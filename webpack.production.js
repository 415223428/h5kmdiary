var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin')

var autoprefixer = require('autoprefixer');
var precss = require('precss');
var values = require('postcss-modules-values');

function absolePath(module) {
  return path.resolve(__dirname, module);
}

module.exports = {
  devtool: false,
  mode: 'production',
  // devtool: "source-map",
  // mode: 'development',
  entry: absolePath('./src/index.tsx'),
  output: {
    path: absolePath('./dist'),
    publicPath: '/',
    filename: 'bundle-[chunkhash].js'
  },
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx'],
    modules: ['node_modules', absolePath('./web_modules')],
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      include: [
        path.resolve(__dirname, './src'),
        path.resolve(__dirname, './web_modules')
      ],
      loader: [
        "babel-loader",
        "ts-loader"
      ]
    },
    {
      test: /\.js$/,
      include: [
        path.resolve(__dirname, "node_modules/plume2"),
        path.resolve(__dirname, "node_modules/webpack-dev-server")
      ],
      loader: "babel-loader"
    },
    {
      test: /\.css/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: "css-loader?modules&localIdentName=[name]-[local]-[hash:base64:5]']",
          options: {
            minimize: true
          }
        }]
      })
    },
    {
      test: /\.css/,
      loader: 'postcss-loader',
      options: {
        plugins: function () {
          return [precss, autoprefixer, values];
        }
      }
    },
    {
      test: /\.(png|ico|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)$/,
      loader: 'url-loader?limit=100000'
    }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].style.css',
      allChunks: true
    }),
    new webpack.DefinePlugin({
      __DEV__: false,
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new HtmlWebpackPlugin({
      env: false,
      filename: 'index.html',
      template: './index.ejs'
    }),
    new webpack.DllReferencePlugin({
      context: __dirname,
      manifest: require('./dist/vendor-manifest.json')
    }),
    // new webpack.optimize.UglifyJsPlugin({
    //   minimize: true
    // }),
    /**
     * ping++微信环境支付宝支付中转页面pay.htm
     * 微信授权文件 *.txt
     */
    new CopyWebpackPlugin(
      [{
        from: absolePath('./web_modules/pay_static/pay.htm'),
        to: absolePath('./dist/pay-online')
      },
      {
        from: absolePath('./web_modules/pay_static'),
        to: absolePath('./dist')
      }, {
        from: absolePath('./web_modules/wmkit/iconfont.css'),
        to: absolePath('./dist')
      }
      ]
    )
  ]
};