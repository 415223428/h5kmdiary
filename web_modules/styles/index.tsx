import React from 'react'

const themeColor = '#3d85cc';  // 主题色
const priceColor = '#cb4255';  // 价格色

export {
  themeColor,
  priceColor
}
