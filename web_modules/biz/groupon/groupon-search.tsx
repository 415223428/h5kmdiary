import React from 'react';
import { Relax } from 'plume2';
import { Link } from 'react-router-dom';
import { history } from 'wmkit';

export default class GrouponSearch extends React.Component<any, any> {
  render() {
    const { keyWords,state } = this.props;
    const styleRecommend = {
      position:'absolute', top:'0.2rem', width:'90%', left: '50%', transform:'translateX(-50%)',background:'rgba(255,255,255,0)',padding:'0'
    }
    const styleRank = {
      background: '#fff'
    }
    const styleRecommendInput = {
      height:'0.56rem',background:'rgba(255,255,255,0.8)',color: '#333'
    }
    const styleRankInput = {
      height:'0.56rem',background:'#f2f2f2',color: '#333',
    }
    return (
      <div className="search-box"  style={state == 'recommend'? styleRecommend : styleRank}>
        <div
          className="input-box"
          onClick={() =>
            history.push({
              pathname: '/search',
              state: { queryString: keyWords, key: 'groupon' }
            })
          }
          style={state == 'recommend'? styleRecommendInput : styleRankInput}
        >
          {/* <i className="iconfont icon-sousuo" /> */}
          <img src={require('./img/search.png')} style={{width:'0.33rem',height:'0.33rem'}} />
          <input type="text" value={keyWords} placeholder="搜索" style={{fontSize:'0.26rem', color: '#333'}}/>
        </div>
        <div />
      </div>
    );
  }
}
