import React, { Component } from 'react';
import { history, WMkit } from 'wmkit';

export default class Blank extends Component<any, any> {
  props: {
    tips?: string;
    content?: string;
    img?: string;
    isToGoodsList?: boolean;
    style?: any;
  };

  static defaultProps = {
    content: '',
    img: 'http://pic.qianmi.com/astore/d2c-wx/images/list-none.png',
    isToGoodsList: false,
    styles: {},
  };

  render() {
    const { tips, content, img, isToGoodsList, style } = this.props;
    return (
      <div className="list-none" style={style}>
        <img src={img} style={{marginTop:'0.8rem'}}/>
        <p style={{
          marginTop:'.7rem',
          color:"rgba(153,153,153,1)"
        }}>{content}</p>
        <p className="grey-tips">{tips}</p>
        {isToGoodsList && (
          <div className="half" style={{display:'flex',justifyContent:'center'}}>
            <button className="btn btn-ghost" style={{border:"1px solid rgba(51,51,51,1)",borderRadius:"35px",lineHeight:'0.01rem'}} onClick={() => this.go()}>
              逛逛商品
            </button>
          </div>
        )}
      </div>
    );
  }
  /**
   * 根据分销渠道跳转
   */
  go = () => {
    if (WMkit.isShop()) {
      history.push('/shop-index-c');
    } else {
      history.push('/goodsList');
    }
  };
}
