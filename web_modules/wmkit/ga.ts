import { WMkit } from 'wmkit';
/**
 * 事件跟踪
 */
interface IEventFieldsObject {
  eventCategory: string; //通常是用户与之互动的对象（例如 'Video'）
  eventAction: string; //互动类型（例如 'play'）
  eventLabel?: string; //用于对事件进行分类（例如 'Fall Campaign'）
  eventValue?: string; //与事件相关的数值（例如 42）
}

/**
 * 事件跟踪
 * https://developers.google.com/analytics/devguides/collection/analyticsjs/events
 * Analytics（分析）中跟踪对出站链接点击的函数。
 * 此函数会将有效网址字符串作为参数，并将该网址字符串
 * 用作事件标签。通过将 transport 方法设置为“beacon”来发送匹配
 * 在支持“navigator.sendBeacon”的浏览器中使用该方法。
 */
export function eventTrack(fieldsObject: IEventFieldsObject) {
  let transport = 'beacon';
  window['ga'] &&
    window['ga'](
      'send',
      'event',
      Object.assign({}, { transport }, fieldsObject)
    );
}

interface IPageViewFieldsCommon {
  title?: string;
}

interface ILocation {
  location?: string;
}

interface IPage {
  page?: string;
}

/**
 * 网页跟踪
 * location和page 必须传递一个
 */
type TPageViewFieldsObject = IPageViewFieldsCommon & (ILocation | IPage);

/**
 * 网页跟踪
 * https://developers.google.com/analytics/devguides/collection/analyticsjs/pages
 * @param fieldsObject 不传就是用默认的 history.loaction.pathname
 */
export function pageViewTrack(fieldsObject?: TPageViewFieldsObject) {
  const ga = window['ga'];
  if (ga) {
    fieldsObject
      ? ga('send', 'pageview', fieldsObject)
      : ga('send', 'pageview');
  }
}

/**
 * https://developers.google.com/analytics/devguides/collection/analyticsjs/single-page-applications
 * 单页面加上page
 * @param page 
 */
export function setPage(page: string) {
  //1000.com需要区分商户
  if (location.origin.indexOf('.1000.com') != -1) {
    page = `${WMkit.getAdminId()}/${page}`;
  }
  window['ga'] && window['ga']('set', 'page', page);
}
