import { Fetch } from 'wmkit'

type TResult = { code: string; message: string; context: any }

/**
 * 批量加入购物车
 */
export const purchase = (buyGoodsInfos) => {
  return Fetch<TResult>('/site/batchAdd',{
    method: 'POST',
    body: JSON.stringify({goodsInfos:buyGoodsInfos.toJS()})
  })
};


/**
 * 立即购买
 */
export const immediateBuy = (buyGoodsInfo) => {
  console.log('%c12211111','color:red');
  console.log(buyGoodsInfo.get('goodsInfoId'));
  console.log(buyGoodsInfo.get('num'));
  console.log('立即购买1立即购买');
  return Fetch<TResult>('/trade/immediate-buy', {
    method: 'POST',
    body: JSON.stringify({
      goodsInfoId: buyGoodsInfo.get('goodsInfoId'),
      buyCount: buyGoodsInfo.get('num')
    })
  });
};

