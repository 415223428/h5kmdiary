import React, { Component } from 'react';
import { Map, List, fromJS, is } from 'immutable';
import { WMImage, NumberInput, Button } from 'wmkit';
import {
  createImmutableData,
  calculateSpeInfo,
  changeSpecDetail,
  returnStockFlag,
  changeNum,
  exchange
} from './state-change';
import * as _ from 'wmkit/common/util';

const SubmitButton = Button.Submit;
const styles = require('../css/style.css');

/**
 * 积分商品-规格选择弹框
 */
export default class WMPointsChoose extends Component<any, any> {
  props: {
    data: any;
    visible: boolean;
    changeSpecVisible: Function;
    dataCallBack?: Function;
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {
    const goods = nextProps.data || {};
    const thisGoods = this.props.data || {};
    if (
      !is(fromJS(goods), fromJS(thisGoods)) &&
      (nextProps.data && nextProps.data.skuId && nextProps.data.pointsGoodsId)
    ) {
      // 组装层级结构的规格数据
      const dataIm = createImmutableData(nextProps.data);
      // 主要是计算每个规格值是否灰化不可点击, 以及计算得出当前的sku
      this.setState({
        ...dataIm,
        ...calculateSpeInfo(dataIm)
      });
    }
  }

  render() {
    const { visible, changeSpecVisible } = this.props;
    const {
      goods = Map(),
      goodsInfo = Map(),
      pointsGoods = Map(),
      goodsInfoCache = Map(), //缓存之前选择的sku,防止用户取消选中规格时,无信息展示的问题
      goodsSpecs = List()
    } = this.state;
    const noSpecStockFlag = returnStockFlag(pointsGoods.get('stock'));

    return (
      <div style={{ display: !visible || !goods ? 'none' : 'block' }}>
        <div
          className="choose-mask"
          onClick={() => {
            changeSpecVisible(false);
          }}
        />
        <div className="choose-body">
          <div className="choose-content">
            {/*弹窗关闭按钮*/}
            <div
              className="close"
              onClick={() => {
                changeSpecVisible(false);
              }}
            >
              <i className="iconfont icon-guanbi" />
            </div>

            {/*sku图文信息*/}
            <div className="choose-top b-1px-b">
              <WMImage
                src={goodsInfoCache.get('goodsInfoImg')}
                alt=""
                width="100"
                height="100"
              />
              <div className="title-box">
                <p className="title">{goods ? goods.get('goodsName') : ' '}</p>
                <p className="price">{pointsGoods.get('points')}积分</p>
                <p className="mkt-price">
                  市场价￥{_.addZero(goodsInfo.get('marketPrice'))}
                </p>
              </div>
            </div>

            {/*sku中间滚动区域*/}
            <div className="choose-center">
              {/*sku选择规格*/}
              {goodsSpecs &&
                goodsSpecs.size > 0 &&
                goodsSpecs.toJS().map((spec, index) => {
                  return (
                    <div className="spec-box" key={spec.specId}>
                      <label className="spec-title">{spec.specName}</label>
                      <div className="spec-item">
                        {spec.specDetails.map((det) => {
                          return (
                            <span
                              key={det.specDetailId}
                              onClick={
                                spec.defaultVal != det.specDetailId &&
                                det.disabled
                                  ? () => {}
                                  : () => {
                                      this._changeSpecDetail(
                                        spec.defaultVal == det.specDetailId
                                          ? null
                                          : det.specDetailId,
                                        index
                                      );
                                    }
                              }
                              className={
                                spec.defaultVal == det.specDetailId
                                  ? 'spec-items checked'
                                  : det.disabled
                                    ? 'spec-items invalid'
                                    : 'spec-items'
                              }
                            >
                              {det.detailName}
                            </span>
                          );
                        })}
                      </div>
                    </div>
                  );
                })}

              {/*sku选择数量*/}
              <div className="sku-num retail-num">
                <div className="sku-bottom">
                  <label className="spec-title">数量</label>
                  <div className="row-flex">
                    <span className="other-text">
                      库存{pointsGoods.get('stock') || 0}
                      {goods.get('goodsUnit')}&nbsp;&nbsp;
                    </span>
                    <NumberInput
                      disableNumberInput={noSpecStockFlag}
                      value={noSpecStockFlag ? 0 : pointsGoods.get('num')}
                      max={pointsGoods.get('stock') || 0}
                      onChange={(value) =>
                        this._changeNum(
                          value,
                          pointsGoods.get('stock'),
                          pointsGoods.get('pointsGoodsId')
                        )
                      }
                      min={1}
                      error={goodsInfo.get('error')}
                    />
                  </div>
                </div>
              </div>
            </div>

            {/*sku底部立即兑换*/}
            <div className="bottom b-1px-t">
              <span className="normal-text">
                已选{pointsGoods.get('num') || 0}
                {goods.get('goodsUnit')}
              </span>
              <SubmitButton
                defaultStyle={{ height: '0.9rem' }}
                disabled={!pointsGoods.get('num')}
                text="立即兑换"
                onClick={() => this._exchange()}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 切换选中前n-1个规格项的规格值
   * @param specDetailId
   * @param index
   * @private
   */
  _changeSpecDetail = (specDetailId, index) => {
    this.setState(changeSpecDetail(this.state, specDetailId, index), () => {
      if (this.props.dataCallBack) {
        this.props.dataCallBack(this.state.goodsInfo, this.state.pointsGoods);
      }
    });
  };

  /**
   * 用户改变sku购买数量
   * @param num 数量
   * @param stock 库存
   * @param goodsInfoId sku标识
   * @private
   */
  _changeNum = (savedNum, stock, pointsGoodsId) => {
    savedNum = stock > 0 ? (savedNum < stock ? savedNum : stock) : 0;
    this.setState(changeNum(this.state, { num: savedNum, pointsGoodsId }));
  };

  /**
   * 立即兑换
   * @private
   */
  _exchange = async () => {
    await exchange(this.state);
    this.props.changeSpecVisible(false);
  };
}
