import { Fetch } from 'wmkit';

/**
 * 立即兑换
 * @param pointsGoodsId
 */
export const exchange = (params) => {
  return Fetch<Result<any>>('/pointsTrade/exchange', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};
