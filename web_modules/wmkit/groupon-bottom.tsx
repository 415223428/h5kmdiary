import React, { Component } from 'react';
import { history, WMkit } from 'wmkit';
import { msg } from 'plume2';
// 引入图片
const dates = [
  {
    icon: '../images/purchasing.png',
    iconActive: '../images/purchasing-active.png',
    text: '拼购',
    press: '/groupon-center'
  },
  {
    icon: '../images/purchasing.png',
    text: '热拼排行',
    press: '/groupon-selection'
  },
  {
    icon: '../images/purchasing.png',
    text: '玩法介绍',
    press: '/groupon-rule'
  },
  {
    icon: '../images/purchasing.png',
    text: '我的拼购',
    press: '/group-order-list'
  }
];
export default class GrouponBottom extends Component<any, any> {
  props: {
    currTab?: string;
  };

  render() {
    const { currTab } = this.props;
    return (
      <div className="groupon-bottom">
        <ul>
          {dates.map((value, index) => {
            return (
              <li
                key={index}
                onClick={() => {
                  if (value.text == '我的拼购' && !WMkit.isLogin()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        // history.push(value.press);
                        history.replace(value.press);
                      }
                    });
                  } else {
                    // history.push(value.press);
                    history.replace(value.press);
                  }
                }}
                className={currTab == value.text ? 'curr' : ''}
              >
                {/* <i className={`iconfont ${value.icon}`} /> */}
                {index == 0? <img src={currTab == value.text ? require('../images/purchasing-active.png') : require('../images/purchasing.png')}/> : null}
                {index == 1? <img src={currTab == value.text ? require('../images/hot-rank-active.png') : require('../images/hot-rank.png')}/> : null}
                {index == 2? <img src={currTab == value.text ? require('../images/introduce-active.png') : require('../images/introduce.png')}/> : null}
                {index == 3? <img src={currTab == value.text ? require('../images/my-purchase-active.png') : require('../images/my-purchase.png')}/> : null}
                <span>{value.text}</span>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
