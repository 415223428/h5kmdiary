# 公共组件目录 #
### * 工具类 ###
**详情**: 多种常用公共方法

**位置**: wmkit/common/util.ts
```sh
用法
 1. 加 _.add(number1, number2)

 2. 减 _.sub(number1, number2)

 3. 乘 _.mul(number1, number2)

 4. 除 _.div(number1, number2)

 5. 格式化时间(yyyy-MM-dd hh:mm:ss) _.formatDate()

 6. 是否微信中打开  _.isWeixin()

 7. 是否是空对象 _.isEmptyObject(obj)

 8. 精确两位小数(四舍五入) _.addZero(number)

 9. 密码安全级别判断 _.getLevel(pwd)

 10.格式化金额(s:金额,n:小数位(可选),fp:标准金额展示(可选)) _.fmoney(s, n, fp)

 11.展示两位小数(不四舍五入) _.toFixed2(number)

 12.异步加载js _.loaderJs(jsSrc)

 13.手机号码安全展示 _.safeMobile(mobile)
```

### * 轮播图组件 ###
**详情**: 渲染一个轮播图

**位置**: wmkit/slider.tsx

**相关组件**: wmkit/image-util/*
```sh
使用 <Slider data={图片地址数组} height={高} width={宽} zoom={是否可点击放大}
            slideInterval={轮播间隔毫秒数，等于0时不自动轮播}/>
```

### * 图片组件 ###
**详情**: 1. 判断是否支持webp 2. 兼容

**位置**: wmkit/image-util/image.tsx

**相关组件**: wmkit/image-util/wrap-url.ts
```sh
使用 <WMImage style={样式}
             src={图片路径，为空会使用组件内部默认图}
             errorSrc={图片加载失败时使用的图片}
             alt={alt}
             width="100%"
             height="100%"/>
```

### * 单选按钮(圆形) ###
**详情**: 一组只能单选的按钮

**位置**: wmkit/radio/index.tsx

**数据源格式**:
```sh
[{name: 'name0', id: 0},{name: 'name1', id: 1},...]
```

```sh
用法
<Radio
    data={数据源}
    beforeTxt={按钮是否在文本前展示}
    checked={选中数据源的值}
    onCheck={选择方法}
/>
```

**位置**: wmkit/switch/index.tsx
```用法
import {Switch} from 'wmkit'
<Switch switched={false}/>关闭
<Switch switched={true}/>打开
```

### * check选择按钮如购物车 ###
示例
```javascript
    <Check checked={false} style={styles.check}/>
```

### * 商品数量加减 ###
示例
```javascript
    <NumberInput/>
```
##### 传参
```javascript
    export interface NumberInputProps{
      //组件禁用
      disableNumberInput?: boolean
      //禁用 +
      disableAdd?: boolean
      //禁用 -
      disableSubtraction?: boolean
      //值
      value?: number
      //组件错误
      error?: string
      //选择最大值
      min?: number
      //选择最小值
      max?: number
      //值改变时回调的方法，值发生变化立即触发
      onChange?: Function
      // 值改变时回调，延时触发，400ms内多次连续操作只触发一次
      onDelayChange?: Function
      // 点击加号回调，两个参数：1.加号是否已禁用 2.this.state.value+1
      onAddClick?: Function,
      //库存提醒
      stockAlert?: boolean
    }
```

### * 表单输入 ###
实例

```javascript
    <FormInput label="验证码" placeHolder="请输入验证码" other={<a className="send-code">发送验证码</a>}/>
```
##### 传参
```javascript
   export interface IInputProps{
     label?: string,
     placeHolder?: string
     //input onchange
     onChange?: Function,
     //其它节点
     other?: ReactNode,
     //是否必填
     required?: boolean
   }
```
##### 说明：用于表单input输入框,更完全的功能有待优化

### * 表单项展示 ###
实例

```javascript
    <FormItem label="退货说明" placeHolder="有色差" textStyle={{color: 'red'}}/>

    <FormItem labelName="商家退款账户" placeholder={
              <div>
                <div>南京银行玄武支行</div>
                <div>1234 5678 9098 3222 6666</div>
              </div>
            }/>
```
##### 传参
```javascript
   export interface IInputProps{
     labelName?: string
     placeholder?: string | React.ReactElement<any>
     // 输入框样式
     textStyle?: Object
   }
```
##### 说明：没有输入、选择等功能，仅展示一行内容


### * 图片上传 ###

**详情**: 图片上传公共组件

**位置**: wmkit/upload/upload-image.tsx

示例
```javascript
    <UploadImage/>
```
##### 传参
```javascript
  props: {
    Tag?: string; //父元素标签 默认span
    style?: object;
    className?: string;
    disabled?: boolean; //true，隐藏父元素事件
    children?; //子元素
    action?: string; //浏览器请求上传地址
    compressLowerlimitMB?: number; //压缩下限大小(只针对浏览器上传，微信上传不限制)
    quality?: number; //压缩比例 1~100
    onSuccess: (result: { res; err }, file?) => void; //参数是promise，请注意
    onLoadChecked?: () => boolean; //上传验证，为false时不进行上传
    onProgress?: (Event) => void;
    onLoadStart?: (Event) => void;
    onLoadEnd?: (Event) => void;
    onAbort?: (Event) => void;
    onError?: (Event) => void;
    repeat?: boolean; //是否可以重复上传同一张图片
    fileType?: string; //图片类型
    size?: number; //图片大小M,默认10M
  }
```

### * switch开关 ###
**相关组件**: wmkit/switch/index.tsx
```
使用 <Switch switched={true} 或者 <Switch switched={false}默认开关的打开和关闭状态
```

### * 标签页 ###
**详情**: 选项卡切换组件

**位置**: wmkit/tab-item/index.tsx

**数据格式**:
```sh
data={'label':'显示内容','tabKey':'唯一的自定义key','display':'是否显示true显示false不显示'}
tabItemStyles={tabs:'tabs样式'tabItem: 'tabItem样式',activity: '选中的tabItem样式'}
```
**传参**
```sh
props: {
        data: List<TabItemProps> //数据源
        activeKey: string //选中的relaxprops:key
        onTabClick: Function //设置选中的relaxprops:key
        tabItemStyles: TabItemStyles //样式

    }
```

**示例**
```sh
<Tabs data={数据源} activeKey={选中的key} onTabClick={设置选中的key} tabItemStyles={样式} />
```


### * 接口请求 ###

**详情**: 调用后台接口

**位置**: wmkit/fetch.ts

格式
```javascript
    fetch请求:
      Fetch(
        input: RequestInfo, // url不需要 host 域名
        init?: RequestInit, // header数据等
        opts: IFetch = { insertHost: true, showTip: true }
      )
```

示例
```javascript
    引用:
      import { Fetch } from 'wmkit'
    get请求:
      Fetch('/login?name=name&pwd=pwd')
    post请求:
      Fetch('/login', {method: 'POST', body: JSON.stringify({name: 'name', pwd: 'pwd'})})
```

### * 弹窗 ###

**详情**: 各种弹窗，Alert  Confirm  Modal

**位置**: wmkit/modal/index.tsx

格式
```javascript
    Alert:
      Alert(
        {
          text: any, // 内容
          cb?: () => void, // 回调函数
          close?: () => void, // 关闭弹窗
          time?: number // 显示时长
        }
      )
    Confirm:
      Confirm(
        {
          title?: String; // 标题
          text: any; // 内容 (必传)
          confirmCb?: () => void; //点击确认回调函数
          cancel?: () => void; // 点击取消回调函数
          close?: () => void; //关闭弹窗
          okBtn?: string; // 确认按钮 (不传则不显示该按钮)
          cancelBtn?: string; // 取消按钮 (不传则不显示该按钮)
        }
      )
    Modal:
      Modal(
        {
          title?: String, // 标题
          content?: any, // 内容
          btnText?: String, // 按钮
          btnCb?: () => void, // 回调函数
          close?: () => void // 关闭
        }
      )
```
示例
```javascript
    引用:
      import { Alert, Confirm, Modal } from 'wmkit';
    Alert:
      Alert({ text: '添加购物车成功' });

    Confirm:
      Confirm({
            title: '您确定要删除吗？',
            text: '',
            confirmCb: () => {

            }
          })

    Modal:
      Modal({
            title: '删除',
            content: '您确定要删除吗？',
            btnText: '确定',
            close: () => {

            }
          })
```

### * 单选按钮(方形) ###
**详情**: 一组只能单选的按钮

**位置**: wmkit/radio-box/index.tsx

**数据源格式**:
```sh
[{name: 'name0', id: 0},{name: 'name1', id: 1},...]
```

```sh
用法
<RadioBox
    data={数据源}
    checked={选中数据源的值}
    onCheck={选择方法}
/>
```

### * 页面提示条 ###

**位置**: wmkit/tips/index.tsx

```sh
用法
<Tips
    iconName={图标class名}
    border={是否需要下边框}
    text={提示内容}
/>
```

### * 按钮 ###

**位置**: wmkit/button/index.tsx

```sh
用法

text: 按钮文字
onClick: 按钮事件。如果返回的是Promise对象，回调前按钮不可点击，用来防止重复提交
disabled: 按钮是否禁用
defaultStyle: 自定义按钮默认样式
clickStyle: 自定义按钮点击样式
shouldStartCountDown: 返回布尔值的函数，为false时不倒计时
resetWhenError: click方法如果是结果是Promise，catch异常情况是否重置Timer

import {Button} from 'wmkit'

const LongBlueButton = Button.LongBlue
const LongButton = Button.Long
const SubmitButton = Button.Submit
const BlueButton = Button.Blue
const RedButton = Button.Red
const SmallBlueButton = Button.SmallBlue
const SmallRedButton = Button.SmallRed
const Timer = Button.Timer

<LongButton onClick={() => {}} text="button1"/>

<LongBlueButton onClick={() => {}} text="button2"/>

<SubmitButton onClick={() => {}} text="button3"
              defaultStyle={{backgroundColor: 'green', color: 'white'}}
              clickStyle={{backgroundColor: 'yellow'}}
/>

<RedButton onClick={() => {}} text="button5" disabled/>

<BlueButton onClick={() => {}} text="button6"/>

<SmallRedButton onClick={() => {}} text="button6"/>

<SmallBlueButton text="填写物流"
            onClick={() => {
              return webapi.logistic()
            }}/>

<Timer text="发送验证码" onClick={} countDownText="倒计时中展示内容" countDownTime={倒计时时间，单位秒}
       shouldStartCountDown={返回布尔值的函数，为false时不倒计时}
       resetWhenError={true}/>
```

### * 勾型单选按钮(√) ###
**详情**: 勾型单选按钮组件

**位置**: wmkit/radio-hook/index.tsx

**数据格式**:
```sh
data = [{id:'唯一id',name:'显示内容'}...]
```
**传参**
```sh
props: {
        data: [],//数据源
        checked: '0', //默认选中id
        onCheck: noop //点击事件处理方法

    }
```

**示例**
```sh
<RadioHook data={数据源} checked={默认选中的id} onCheck={点击事件处理方法}/>
```

### * 列表（带分页） ###

**详情**: 带分页列表

**位置**: wmkit/list-view/index.tsx

格式
```sh
    <ListView
        //请求的url
        url: '',
        //样式
        style: {},
        //http参数
        params: {},
        //默认当前页
        pageNum: 0,
        //默认每页展示的数量
        pageSize: 10,
        //提前多少px，发出ajax请求
        scrollRenderAheadDistance: 100,
        //当前的数据
        dataSource: [],
        //是否展示showLoadingMore
        isShowLoadingMore: true,
        //是否分页
        isPagination: true,
        //显示头部
        renderHeader: null,
        //展示每列
        renderRow: null,
        //展示页脚
        renderFooter: null,
        //显示空
        renderEmpty: null,
        //显示loading效果
        renderLoading: null,
        //收到数据后的回调
        onDataReached: noop,
        //可接受类名扩展
        className: ''
      />
```
示例
```sh
    <ListView
      url='/goods/skus'
      renderRow={(goodsItem: any, index: number) => <GoodsItem goodsItem={fromJS(goodsItem)} listView={listView} />}
      renderEmpty={() => <Blank content="没有搜到任何商品～" />}
      ref={(_listView) => this._listView = _listView}
    />
```

### * 商品清单 ###
**详情**: 只展示商品列表的清单,不做分页

**位置**: wmkit/goods-list/index.tsx

**数据格式**:
```sh
data = [{ imgUrl: '图片地址', skuName: 'sku名称',
          specDetails: '规格详情', price: 'sku价格', num: 'sku数量' }...]
```
**传参**
```sh
props: {
        data: [],//数据源
    }
```

**示例**
```sh
<SkuList data={数据源}/>
```
### * 文本输入(自动换行) ###
**详情**: 用于文本输入,支持超过一行自动换行,内容到达最大长度禁止输入

**位置**: wmkit/form/form-text/index.tsx
```sh
用法
    <FormText
          label?: string,        //输入项名称
          placeholder?: string   //预留文案
          onChange?: Function,   //input onchange
          other?: ReactNode,     //其它节点
          required?: boolean,    //是否必填
          value?:string,         //默认值
          maxLength?: number     //最大长度
    />
```

### * 图片列表可滑动显示 ###

**详情**: 图片列表可以左右滑动

**位置**: wmkit/image-list-scroll/index.tsx

格式
```sh
     <ImageListScroll
        imageList: Array<any>, // 图片列表
        size?: number, // 图片尺寸大小
        labelName?: string // 列表名称
      />
```
示例
```sh
    引用:
      import {ImageListScroll} from 'wmkit';

      <ImageListScroll imageList={attachments} labelName="退单附件"/>
```

### * 表单验证 ###
**详情**: 提交之前，对表单某个字段进行格式校验，自动弹出Alert框提示，校验不通过返回false，更详细见源文件

**位置**: wmkit/FormRegexUtil/index.tsx

示例
```sh
    引用:
      import {FormRegexUtil} from "wmkit";

      if (!FormRegexUtil(invoiceBean.get("bankNo"), "银行基本户号", {required: true, regexType: 'accountNo'}))return;
```