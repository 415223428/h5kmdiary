import { Actor, Action, IMap } from 'plume2'

/**
 * 通用的loadingActor，省的每个组件里面都写，
 * store里使用this.dispatch('common:loading', bollean); 来显是或隐藏
 */
export default class LoadingActor extends Actor {
  defaultState() {
    return {
      loading: true
    }
  }

  @Action('common:loading')
  loading(state: IMap, loading: boolean) {
    return state.set('loading', loading);
  }
}