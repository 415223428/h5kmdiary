/**
 *创建HashHistroy
 */
const createHistory = require("history").createBrowserHistory

const history = createHistory();

(window as any).apiready = () => {
  //这里的m在后面才创建，但是能成功地更改isApicloud，后续需继续观察
  (window as any).getUrl = (newValue) => {
    (window as any).api.sendEvent({
      name: 'getUrl',
      extra: { //发送的数据
        url: newValue,
      }
    });
  };
  (window as any).paySuccess = (price, tradeNo, type) => {
    (window as any).api.sendEvent({
      name: 'paySuccess',
      extra: { //发送的数据
        price: price,
        tradeNo: tradeNo,
        type: type
      }
    });
  };
  (window as any).openqiyu = () => {
    (window as any).api.sendEvent({
      name: 'openqiyu',
      extra: { //发送的数据
        // context:context,
        
      }
    });
  };

  (window as any).alipay = (price, tradeNo, type,appEncrypted) => {
    // console.log('price');
    // console.log(price);
    // console.log('tradeNo');
    // console.log(tradeNo);
    (window as any).api.sendEvent({
      name: 'alipay',
      extra: { //发送的数据
        price: price,
        tradeNo: tradeNo,
        type: type,
        appEncrypted:appEncrypted
      }
    });
  };

  //goto 订单
  (window as any).gotoOrderList = (tradeId, type) => {
    // // history.push('/main');
    // // history.push('/order-list');
    // console.log(tradeId);
    // console.log(type);

    let url = '';
    if (type === 'tradeId') {
      url = `/pay-success/${tradeId}/undefined`;
    } else {
      url = `/pay-success/undefined/${tradeId}`;
    }

    history.push(url);

    location.reload();
  };

  (window as any).shareImg = (base64) => {
    (window as any).api.sendEvent({
      name: 'shareImg',
      extra: { //发送的数据
        // context:context,
        base64: base64,
      }
    });
  };
}

history.listen((location, action) => {
  const { pathname } = location
  // if(action==="PUSH"){
  if ((window as any).getUrl !== undefined) {
    (window as any).getUrl(pathname)
  }
  //   }
})

export default history;