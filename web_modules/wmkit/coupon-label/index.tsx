import React from 'react';
require('./style.css')

export default class CouponLabel extends React.PureComponent<any, any> {
  static defaultProps = {
    //券值
    text: '',
    style: {},
    pStyle: {}
  };
  render() {
    const { text,style,pStyle } = this.props;
    return (
      <div className="coupon-label" style={style}>
        <p style={pStyle}>{text}</p>
      </div>
    );
  }
}
