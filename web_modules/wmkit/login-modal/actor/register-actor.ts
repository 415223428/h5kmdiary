import { Actor, Action, IMap } from 'plume2';
import { IList } from 'typings/globalType';
import { StateType } from 'rmc-tabs/lib/Tabs';
export default class RegisterActor extends Actor {
  defaultState() {
    return {
      //注册账号
      registerAccount: '',
      //注册密码
      registerPass: '',
      //注册验证码
      registerCode: '',
      //注册时是否显示密码
      showRegisterPass: false,
      //显示注册协议
      showAgreement: false,
      // 邀请人
      inviteeCustomerName: '',
      //logo
      pcLogo: [],
    };
  }

  // /**
  //  * logo
  //  * @param state
  //  * @param config
  //  */
  // @Action('register:init')
  // init(state, config: IList) {
  //   return state
  //     .set('pcLogo', config)
  // }

  // 邀请人
  @Action('change:inviteeCustomerName')
  changeInviteeCustomerName(state, inviteeCustomerName) {
    return state.set('inviteeCustomerName', inviteeCustomerName);
  }

  @Action('field:value')
  setFieldValue(state, { field, value }) {
    return state.set(field, value);
  }

  @Action('loginModal:toggleShowAgreement')
  toggleShowAgreement(state) {
    return state.set('showAgreement', !state.get('showAgreement'));
  }

  @Action('modal:clear:register')
  clearRegister(state) {
    return state
      .set('registerAccount', '')
      .set('registerPass', '')
      .set('registerCode', '')
      .set('showRegisterPass', false)
      .set('showAgreement', false);
  }

}
