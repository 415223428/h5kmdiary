import React, { Component } from 'react';
import { Relax } from 'plume2';
import {
  FormInput,
  Button,
  noop,
  Picker,
  FormSelect,
  ValidConst,
  Alert,
  FindArea
} from 'wmkit';
import AppStore from '../store'

const LongBlueButton = Button.LongBlue;
const SubmitButton = Button.LongBlue;
const TimerButton = Button.Timer;

const icon = require('../img/icon.png');
const failed = require('../img/failed.png');
const login=require('../img/login.png')
const logout=require('../img/logout.png')
const logo=require('../img/logo1.png')
const logo2 = require('../img/logo2.png');

@Relax
export default class Register extends Component<any, any> {
  store: AppStore
  static relaxProps = {
    toggleVisible: noop,
    initPage: noop,
    setFieldValue: noop,
    sendCode: noop,
    doRegister: noop,
    doPerfect: noop,
    toggleShowAgreement: noop,
    changeCustomerDetailField: noop,
    getArea: noop,
    registerAccount: 'registerAccount',
    registerPass: 'registerPass',
    registerCode: 'registerCode',
    showRegisterPass: 'showRegisterPass',
    //显示完善账户信息
    showImproveInfo: 'showImproveInfo',
    //会员基本信息
    customerDetail: 'customerDetail',
    //邀请人
    inviteeCustomerName: 'inviteeCustomerName',
    pcLogo:'pcLogo'
  };

  render() {
    const {
      registerAccount,
      registerPass,
      registerCode,
      setFieldValue,
      showRegisterPass,
      sendCode,
      doRegister,
      showImproveInfo,
      customerDetail,
      inviteeCustomerName,
      changeCustomerDetailField,
      getArea,
      doPerfect,
      toggleShowAgreement,
      pcLogo
    } = this.props.relaxProps;
    const customerName = customerDetail.get('customerName');
    const customerAddress = customerDetail.get('customerAddress');
    const contactName = customerDetail.get('contactName');
    const contactPhone = customerDetail.get('contactPhone');
    let provinceId = customerDetail.get('provinceId');
    let cityId = customerDetail.get('cityId');
    let areaId = customerDetail.get('areaId');
    let area = provinceId
      ? cityId
        ? [provinceId.toString(), cityId.toString(), areaId.toString()]
        : [provinceId.toString()]
      : [];
    //拼接省市区名字
    let areaName = FindArea.addressInfo(provinceId, cityId, areaId);
    //对应显示的样式
    let textStyle = provinceId ? { color: '#333' } : {};
    const { toggleVisible, initPage } = this.props.relaxProps;
    //logo
    // const pcLogo = this.store.state().toJS().pcLogo;
    // let pcLogo = this.props.logo;
    return (
      <div className="registerContainer">
        <a
          href="javascript:void(0);"
          onClick={() => {
            toggleVisible({ visible: false });
            initPage();
          }}
          className="closeBox"
        >
          <i className="iconfont icon-guanbi" />
        </a>
        {!showImproveInfo ? (
          <div style={{ width: '90%' }}>
            <div className="logo1">
              <img
                // src={pcLogo && pcLogo.length >= 1 ? pcLogo[0].url : logo}
                // src={pcLogo ? pcLogo : logo}
                src={logo2}
                className="logoImg1"
              />
            </div>
            {/* <h4 className="registerTitle">请使用有效手机号注册</h4> */}
            {inviteeCustomerName && (
              <p className="invite-text">邀请人：{inviteeCustomerName}</p>
            )}
            <div className="registerBox">
              {/* <FormInput
                label=""
                type="tel"
                placeHolder="请输入您的手机号"
                pattern="[0-9]*"
                defaultValue={registerAccount}
                maxLength={11}
                onChange={(e) =>
                  setFieldValue({
                    field: 'registerAccount',
                    value: e.target.value
                  })
                }
              /> */}
              <div className="row form-item">
                <div className="inputBox eyes-box inputBoxMy">
                  <img src={require('../img/phone.png')} alt="" />
                  <input
                    className="formInput"
                    type="tel"
                    pattern="[0-9]*"
                    placeholder="请输入您的手机号"
                    value={registerAccount}
                    maxLength={11}
                    onChange={(e) =>
                      setFieldValue({
                        field: 'registerAccount',
                        value: e.target.value
                      })}
                  />
                </div>
              </div>

              <div className="row form-item">
                <div className="inputBox eyes-box inputBoxMy" style={{ paddingRight: 0 }}>
                  <img src={require('../img/code.png')} alt="" />
                  <input
                    className="formInput sendCodeInput"
                    type="text"
                    placeholder="请输入手机验证码"
                    pattern="/^[0-9]{6}$/"
                    value={registerCode}
                    onChange={(e) =>
                      setFieldValue({
                        field: 'registerCode',
                        value: e.target.value
                      })}
                    maxLength={6}
                  />
                  <TimerButton
                    text="获取验证码"
                    resetWhenError={true}
                    shouldStartCountDown={() => this._sendCode()}
                    onClick={() => sendCode(registerAccount)}
                    defaultStyle={{
                      fontSize: '0.26rem',
                      padding: '0.11rem',
                      fontFamily: 'PingFang SC',
                      fontWeight: '500',
                      color: 'rgba(255,77,77,1)',
                      border: '1px solid rgba(255, 77, 77, 1)',
                      borderRadius: '4px',
                      margin: 'auto 0 auto auto',
                    }}
                  />
                </div>
              </div>

              {/* <FormInput
                label=""
                type="number"
                placeHolder="请输入验证码"
                pattern="[0-9]*"
                defaultValue={registerCode}
                maxLength={6}
                onChange={(e) =>
                  setFieldValue({
                    field: 'registerCode',
                    value: e.target.value
                  })
                }
                other={
                  <TimerButton
                    text="发送验证码"
                    resetWhenError={true}
                    shouldStartCountDown={() => this._sendCode()}
                    onClick={() => sendCode(registerAccount)}
                    defaultStyle={{
                      color: '#000',
                      borderColor: '#000'
                    }}
                  />
                }
              /> */}
              <div className="row form-item">
                <div className="eyes-box inputBoxMy">
                  <img src={require('../img/pwd.png')} alt="" />
                  <input
                    placeholder="请输入6-16位字母或数字密码"
                    type={showRegisterPass ? 'text' : 'password'}
                    value={registerPass}
                    pattern="/^[A-Za-z0-9]{6,16}$/"
                    className="form-input formInput"
                    maxLength={16}
                    onChange={(e) =>
                      setFieldValue({
                        field: 'registerPass',
                        value: e.target.value
                      })
                    }
                  />
                  <i
                    onClick={() =>
                      setFieldValue({
                        field: 'showRegisterPass',
                        value: !showRegisterPass
                      })
                    }
                    className={`iconfont icon-${
                      showRegisterPass ? 'yanjing' : 'iconguanbiyanjing'
                      }`}
                  />
                </div>
              </div>
              <div className="registerBtn" style={{ margin: '1.2rem auto auto' }}>
                {/* <LongBlueButton text="注册" onClick={() => doRegister()} /> */}
                <img src={logout} alt="" style={{width:'5rem'}} onClick={() => doRegister()}/>
              </div>
              <div className="registerTips">
                点击注册代表您已阅读并接受<a
                  href="javascript:;"
                  onClick={() => toggleShowAgreement()}
                >
                  <span>《会员注册协议》</span>
                </a>
              </div>
            </div>
            <div className="footer-copyright">
              <p>粤ICP备19120716号-1</p>
            </div>
          </div>
        ) : (
            <div className="improve-info">
              <div className="tip-box">
                <img src={icon} className="img" />
                <span className="text">
                  您还需完善账户信息才可正常访问商品信息
              </span>
              </div>
              <div className="row form-item">
                <span className="form-text">
                  <i>*</i>称呼/公司名称
              </span>
                <input
                  placeholder="请填写您的称呼或您公司的名称"
                  type="text"
                  className="form-input"
                  value={customerName}
                  maxLength={15}
                  onChange={(e) =>
                    changeCustomerDetailField({
                      field: 'customerName',
                      value: e.target.value
                    })
                  }
                />
              </div>
              <Picker
                extra="所在地区"
                title="选择地址"
                format={(values) => {
                  return values.join('/');
                }}
                value={area}
                className="addr-picker"
                onOk={(val) => getArea(val)}
              >
                <FormSelect
                  labelName="选择地址"
                  placeholder="请选择您所在的地区"
                  textStyle={textStyle}
                  selected={{ key: area, value: areaName }}
                />
              </Picker>
              <div className="row form-item">
                <span className="form-text">详细地址</span>
                <input
                  placeholder="请填写您的详细地址"
                  type="text"
                  className="form-input"
                  value={customerAddress}
                  maxLength={60}
                  onChange={(e) =>
                    changeCustomerDetailField({
                      field: 'customerAddress',
                      value: e.target.value
                    })
                  }
                />
              </div>
              <div className="row form-item">
                <span className="form-text">
                  <i>*</i>常用联系人
              </span>
                <input
                  placeholder="请填写一位常用联系人"
                  type="text"
                  className="form-input"
                  value={contactName}
                  maxLength={15}
                  onChange={(e) =>
                    changeCustomerDetailField({
                      field: 'contactName',
                      value: e.target.value
                    })
                  }
                />
              </div>
              <div className="row form-item">
                <span className="form-text">
                  <i>*</i>常用联系人手机号
              </span>
                <input
                  placeholder="请填写联系人常用手机号"
                  type="text"
                  className="form-input"
                  value={contactPhone}
                  maxLength={11}
                  onChange={(e) =>
                    changeCustomerDetailField({
                      field: 'contactPhone',
                      value: e.target.value
                    })
                  }
                />
              </div>
              <div className="improve-btn">
                <SubmitButton text="提交" onClick={() => doPerfect()}>
                  提交
              </SubmitButton>
              </div>
            </div>
          )}
      </div>
    );
  }

  _sendCode = () => {
    const { registerAccount } = this.props.relaxProps;
    const regex = ValidConst.phone;
    if (!registerAccount) {
      Alert({
        text: '手机号不能为空！'
      });
      return false;
    } else if (!regex.test(registerAccount)) {
      Alert({
        text: '手机号格式有误!'
      });
      return false;
    } else {
      return true;
    }
  };
}
