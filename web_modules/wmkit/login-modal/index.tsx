import React from 'react';
import { msg, StoreProvider } from 'plume2';
import AppStore from './store';
import Register from './components/register';
import LoginTab from './components/login-tab';
import QuickLogin from './components/quick-login';
import AccountFrom from './components/account-form';
import DynamicFrom from './components/dynamic-from';
import Agreement from './components/agreement';
import { noop, _ } from 'wmkit';
import './css/style.css';


const logo = require('./img/logo.png');
const logo2 = require('./img/logo2.png');

let u = navigator.userAgent;
let isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //这个判断 是不是ios手机

@StoreProvider(AppStore, { debug: __DEV__ })
export default class WMLoginModal extends React.Component<any, any> {
  store: AppStore;

  //控制弹框显示隐藏
  componentWillMount() {
    msg.on('loginModal:toggleVisible', this.toggleVisible);
  }

  //组件销毁事件解绑
  componentWillUnmount() {
    msg.off('loginModal:toggleVisible', this.toggleVisible);
  }

  componentDidUpdate() {
    if (isiOS) {
      //ios手机上才会出现input失去焦点的时候页面被顶起
      window.addEventListener('scroll', this._initScroll);
    }
  }

  constructor(props) {
    super(props);
  }

  render() {
    const showLogin = this.store.state().get('showLogin');
    const isALogin = this.store.state().get('isALogin');
    const pcLogo = this.store.state().get('pcLogo');
    const wxFlag = this.store.state().get('wxFlag');
    const showAgreement = this.store.state().get('showAgreement');
    return (
      this.store.state().get('modalVisible') && (
        <div className="login-modal" id="login-modal">
          {showLogin ? (
            <div className="loginContainer">
              <a
                // href="javascript:void(0);"
                className="closeBox"
                onClick={() =>
                  this.store.toggleVisible({ visible: false, callBack: null })
                }
              >
                <i className="iconfont icon-guanbi" />
              </a>
              <div className="l-logo">
                <img 
                  // src={pcLogo ? pcLogo : logo} 
                  src={logo2}
                  style={{width: '1.65rem',height: '1.65rem'}}
                  className="logoImg" />
              </div>
              <LoginTab />
              {isALogin ? <AccountFrom /> : <DynamicFrom />}
              <div className="register-account">
                <a onClick={() => this.store.toggleLogin()}>
                  <span className="dark-text">注册账号</span>
                  {/* <i
                    style={{ marginLeft: 5, fontSize: 12 }}
                    className="iconfont icon-xiayibu1 dark-text"
                  /> */}
                </a>
                <a href="/user-safe-password"><span className="dark-text">忘记密码</span></a>
              </div>
              {_.isWeixin() &&
                < div style={{ width: '100%', marginTop: '1rem' }}>
                  <div className="footTitle">
                    <p>其他登录方式</p>
                  </div>
                  {wxFlag && <QuickLogin />}
                  <div className="footer-copyright">
                  </div>
                </div>
              }

              {/* <div className="bottomBar"> */}
              {/* {wxFlag && <QuickLogin />} */}
              <div className="footer-copyright">
                <p>粤ICP备19120716号-1</p>
              </div>
              {/* </div> */}
            </div>
          ) : (
              <Register logo={pcLogo}/>
            )
          }
          {showAgreement && <Agreement />}
        </div >
      )
    );
  }

  toggleVisible = ({ callBack }) => {
    this.store.init();
    this.store.toggleVisible({ visible: true, callBack: callBack });
  };

  /**
   * IOS端点击input键盘收起的事件处理
   * @private
   */
  _initScroll = () => {
    let loginModal = document.getElementById('login-modal');
    if (loginModal) {
      loginModal.addEventListener('focusout', () => {
        //软键盘收起的事件处理
        setTimeout(window.scrollTo(0, 0), 500)
      });
    }
  };
}
