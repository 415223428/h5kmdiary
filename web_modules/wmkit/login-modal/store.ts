import { Store, msg } from 'plume2';

import { _, Alert, history, storage, ValidConst, WMkit, wxAuth,Confirm } from 'wmkit';
import { getShareUserId, mergePurchase } from 'biz';
import { cache, config } from 'config';
import LoginActor from './actor/login-actor';
import RegisterActor from './actor/register-actor';
import InfoActor from './actor/info-actor';
import AgreeActor from './actor/agree-actor';
import * as webapi from './webapi';
import { isNullOrUndefined } from 'util';

/**校验名称和联系人*/
function _testName(name: string) {
  if (name == '') {
    Alert({
      text: '请填写名称！'
    });
    return false;
  } else if (name.length < 2 || name.length > 15) {
    Alert({
      text: '名称为2-15个字符！'
    });
    return false;
  }
  return true;
}

function _testContact(contact: string) {
  if (contact == '') {
    Alert({
      text: '请填写常用联系人！'
    });
    return false;
  } else if (contact.length < 2 || contact.length > 15) {
    Alert({
      text: '联系人名称为2-15个字符！'
    });
    return false;
  }
  return true;
}

/**校验地址详情*/
function _testAddress(address: string) {
  if (address == '') {
    Alert({
      text: '请填写详细地址！'
    });
    return false;
  } else if (address.length < 5 || address.length > 60) {
    Alert({
      text: '地址长度为5-60个字符！'
    });
    return false;
  }
  return true;
}

/**校验手机号码*/
function _testTel(tel: string) {
  const regex = ValidConst.phone;
  if (tel == '') {
    Alert({
      text: '请填写联系人手机号！'
    });
    return false;
  } else if (!regex.test(tel)) {
    Alert({
      text: '无效的手机号！'
    });
    return false;
  } else {
    return true;
  }
}

export default class AppStore extends Store {
  bindActor() {
    return [
      new LoginActor(),
      new RegisterActor(),
      new InfoActor(),
      new AgreeActor()
    ];
  }

  constructor(props) {
    super(props);
    (window as any)._store = this;
  }

  init = async () => {
    // 初始化基本信息和微信授权登录开关
    const res = await Promise.all([
      webapi.fetchBaseConfig(),
      webapi.fetchWxLoginStatus()
    ]);
    if (
      res[0].code == config.SUCCESS_CODE &&
      res[1].code == config.SUCCESS_CODE
    ) {
      this.dispatch(
        'login:init',
        (res[0].context as any).pcLogo
          ? JSON.parse((res[0].context as any).pcLogo)[0]
          : { url: '' }
      );
      // this.dispatch('register:init', JSON.parse((res[0].context as any).pcLogo));
      // 注册协议
      this.dispatch('agree:registerContent', res[0].context as any);
      //清除注册弹框信息
      this.dispatch('modal:clear:register');
      //清除完善账户弹框信息
      this.dispatch('modal:clear:info');
      this.dispatch('login:wxFlag', res[1].context);
      
      if (!isNullOrUndefined(WMkit.inviteeId())) {
        console.log('登录注册人的ID');
        // 查询邀请人姓名
        const { code, context } = await webapi.getCustomerInfo(
          WMkit.inviteeId()
        );
        if (code == config.SUCCESS_CODE) {
          if (context) {
            //获取邀请人信息
            this.dispatch(
              'change:inviteeCustomerName',
              (context as any).customerName
            );
          }
        }
      }
    }

    if (__DEV__ || !_.isWeixin()) {
      return;
    }
  };

  showPass = () => {
    const showpass = this.state().get('isShowpwd');
    this.dispatch('login:showPass', showpass);
  };

  doLogin = async () => {
    // true：账号密码登录  false：验证码登录
    const isALogin = this.state().get('isALogin');
    let result = null;
    if (isALogin) {
      /**
       * 获取用户名和密码，并去除所有空格*/
      const account = this.state()
        .get('account')
        .trim()
        .replace(/\s/g, '');
      const password = this.state()
        .get('password')
        .trim()
        .replace(/\s/g, '');
      // if (WMkit.testTel(account) && WMkit.testPass(password)) {
      if (WMkit.testPass(password)) {
        const base64 = new WMkit.Base64();
        const res = await webapi.login(
          base64.urlEncode(account),
          base64.urlEncode(password)
        );
        result = res;
      }
    } else {
      /**
       * 获取用户名和验证码，并去除所有空格*/
      const account = this.state()
        .get('account')
        .trim()
        .replace(/\s/g, '');
      const verificationCode = this.state()
        .get('verificationCode')
        .trim()
        .replace(/\s/g, '');
      if (
        WMkit.testTel(account) &&
        WMkit.testVerificationCode(verificationCode)
      ) {
        const res = await webapi.loginWithVerificationCode(
          account,
          verificationCode
        );
        result = res;
      }
    }
    const { context } = result;
    /**
     * 登录成功时*/
    if ((result as any).code == config.SUCCESS_CODE) {
      //获取审核状态
      this.switchLogin(context);
    }
    //密码输错5次，按钮设灰色
    else if ((result as any).code == 'K-010004') {
      this.transaction(() => {
        this.dispatch('login:buttonstate');
        this.dispatch('login:buttonvalue', result.message);
      });
      Alert({
        text: result.message
      });
      return;
    } else {
      /**
       * 登录失败，提示各类出错信息*/
      if (result.message === '参数不正确') {
        Alert({
          text: '请输入手机号'
        });
      } else {
        Alert({
          text: result.message
        });
      }
      return false;
    }

    // this.dispatch('login:loginAuto',taken);
  };

  setAccount = (account) => {
    this.transaction(() => {
      //登录按钮恢复可点击状态
      this.dispatch('login:enableButton');
      this.dispatch('login:account', account);
      this.dispatch('login:accountChange', this.state().get('accountChange'));
    });
  };

  setPressError = (errorPress: boolean) => {
    this.dispatch('login:errorPress', errorPress);
  };

  setPassword = (pass) => {
    this.dispatch('login:password', pass);
  };

  /**
   * 输入验证码
   * @param code
   */
  setVerificationCode = (code) => {
    this.dispatch('login:verificationCode', code);
  };

  /**忘记密码*/
  forgetPass = async () => {
    history.push('/user-safe-password');
  };

  /*切换登录方式*/
  loginChange = () => {
    this.dispatch('login:loginChange');
  };

  /**
   * 微信登录授权
   */
  weChatQuickLogin = async () => {
    const url = location.href;
    //弹框隐藏(因为重定向以后页面不刷新)
    this.dispatch('loginModal:toggleVisible', {
      visible: false,
      callBack: this.state().get('callBack')
    });
    wxAuth.getAuth(url.split('?')[0], 'login');
  };

  /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
  /** ** ** ** ** ** ** ** ** ** ** ** ** * 验证码登录 * ** ** ** ** ** ** ** ** ** ** ** ** **/
  /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

  /**
   * 发送验证码给手机号码
   * @returns {Promise<Result<ReturnResult>>}
   */
  sendCode = (mobile) => {
    //登录发送验证码还是注册发送验证码
    const showLogin = this.state().get('showLogin');
    return webapi.sendCode(mobile, showLogin).then((res) => {
      if (res.code === config.SUCCESS_CODE) {
        Alert({
          text: '验证码已发送，请注意查收！',
          time: 3000
        });
      } else {
        Alert({
          text: res.message,
          time: 3000
        });
        return Promise.reject(res.message);
      }
    });
  };

  //弹框显示隐藏
  toggleVisible = ({ visible, callBack }) => {
    this.dispatch('loginModal:toggleVisible', { visible, callBack });
  };

  /**
   * 根据登录后的返回参数跳转到相应页面
   * @param context
   */
  switchLogin = async (context: any) => {
    // 判断是否编号还是手机号 1手机 2编号 null:不需要绑定
    if (context.accountTypt == "2") {
      this.dispatch('loginModal:toggleVisible', {
        visible: false,
        callBack: null
      });
      sessionStorage.setItem('customerId', context.customerId)
      history.push('/bangDingPhone');
    } else if (context.accountTypt == "1") {
      this.dispatch('loginModal:toggleVisible', {
        visible: false,
        callBack: null
      });
      sessionStorage.setItem('accountName', context.accountName)
      history.push('/bangDingVipNo');
    } else {
      switch (context.checkState) {
        /**审核中*/
        case 0:
          WMkit.clearLoginCache();
          //在完善信息开关打开的时候，且注册了以后没有完善账户信息
          if (!context.isLoginFlag && !context.customerDetail.customerName) {
            //存储customerId
            this.dispatch('change:customerDetailField', {
              field: 'customerId',
              value: (context as any).customerId
            });
            //关闭登录弹框
            this.toggleLogin();
            //显示完善信息的弹框
            this.dispatch('show:showImproveInfo');
          } else {
            //将审核中的账户信息存入本地缓存
            storage('local').set(
              cache.PENDING_AND_REFUSED,
              JSON.stringify(context)
            );
            //关闭弹框，调到完善账户信息
            this.dispatch('loginModal:toggleVisible', {
              visible: false,
              callBack: null
            });
            history.push(`/improve-information/${context.customerId}`);
          }
          break;
        /**审核通过，成功登录*/
        case 1:
          // a.设置登陆后token以及登陆信息缓存
          window.token = context.token;
          localStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
          sessionStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
          //存入直销会员数据
          localStorage.setItem('loginSaleType', context.sourceType)
          WMkit.setIsDistributor();
          // b.登陆成功,需要合并登录前和登陆后的购物车,传入回调函数
          mergePurchase(this.state().get('callBack'));
          // c.登陆成功,关闭弹框
          const lo = WMkit.messAlter(context);
          if (await lo===1) {
            Confirm({
              text: '您的手机号码未绑定会员编号，请通过会员编号登录进行手机号码绑定，后续方可通过手机号码登录',
              okBtn: '确定',
              maskClose:false,
              confirmCb: ()=>{
                console.log('已经记录了');
                WMkit.recordLogin(context.customerId,context.customerAccount)
                WMkit.clearLoginCache();
              },
            });
            // WMkit.clearLoginCache();
            return
          }
          Alert({ text: '登录成功' });
          // 登录成功，发送消息，查询分销员信息 footer/index.tsx
          msg.emit('userLoginRefresh');
          this.dispatch('loginModal:toggleVisible', {
            visible: false,
            callBack: null
          });

          // if(context.sourceType === null){
          //   const checkPhone = await webapi.checkPhone(context.customerAccount);
          //   if (checkPhone.code === 'K-000001') {
          //       Alert({
          //         text: '您的手机号码未绑定会员编号，请通过会员编号登录进行手机号码绑定，后续方可通过手机号码登录'
          //       })
          //   }
          // }
          _.showRegisterModel((context as any).couponResponse, true);
          break;
        /**审核未通过*/
        default:
          WMkit.clearLoginCache();
          this.dispatch('loginModal:toggleVisible', {
            visible: false,
            callBack: null
          });
          //将审核中的账户信息存入本地缓存
          storage('local').set(
            cache.PENDING_AND_REFUSED,
            JSON.stringify(context)
          );
          history.push(`/improve-information/${(context as any).customerId}`);
      }
    }
  };

  toggleLogin = () => {
    this.dispatch('loginModal:toggleLogin');
  };

  //还原设置
  initPage = () => {
    this.dispatch('loginModal:initPage');
  };

  //改变指定的state值
  setFieldValue = ({ field, value }) => {
    this.dispatch('field:value', { field, value });
  };

  //注册
  doRegister = async () => {
    //参数准备
    const mobile = this.state().get('registerAccount');
    const sendCode = this.state().get('registerCode');
    const password = this.state().get('registerPass');
    const employeeId = this.state().get('employeeId');
    const shareUserId = getShareUserId();
    const inviteeId = WMkit.inviteeId();

    if (
      WMkit.testTel(mobile) &&
      WMkit.testPass(password) &&
      WMkit.testVerificationCode(sendCode)
    ) {
      const fromPage = 1;

      const { code, message } = await webapi.checkRegister(
        mobile,
        sendCode,
        fromPage
      );
      if (code == config.SUCCESS_CODE) {
        const registerInfo = {
          customerAccount: mobile,
          customerPassword: password,
          verifyCode: sendCode,
          inviteeId: inviteeId,
          shareUserId: shareUserId,
          fromPage: fromPage //0:注册页面，1：注册弹窗
        };
        sessionStorage.setItem(
          cache.REGISTER_INFO,
          JSON.stringify(registerInfo)
        );
        this.dispatch('loginModal:toggleVisible', {
          visible: false,
          callBack: null
        });
        history.push(`/distribution-register`);
        return true;
      } else {
        Alert({
          text: message
        });
      }
    } else {
      return false;
    }
  };

  //查询会员基本信息
  queryCustomerDetal = async (id: string) => {
    const res = storage('local').get(cache.PENDING_AND_REFUSED);
    //如果存在，更改用户ID的状态值
    this.dispatch('field:value', { field: 'customerId', value: id });
    if (res) {
      const customer = JSON.parse(res).customerDetail;
      this.transaction(() => {
        this.dispatch('detail:customer', customer);
        this.dispatch('detail:checked', JSON.parse(res).checkState);
      });
    }
  };



  toDoLogin = async () => {
    // true：账号密码登录  false：验证码登录
    const isALogin = this.state().get('isALogin');
    let result = null;
    const res = await webapi.appAutoLogin(
    );
    result = res;
    const { context } = result;
    /**
     * 登录成功时*/
    if ((result as any).code == config.SUCCESS_CODE) {
      //获取审核状态
      this.switchLogin(context);
    }
    //密码输错5次，按钮设灰色
    else if ((result as any).code == 'K-010004') {
      this.transaction(() => {
        this.dispatch('login:buttonstate');
        this.dispatch('login:buttonvalue', result.message);
      });
      Alert({
        text: result.message
      });
      return;
    } else {
      /**
       * 登录失败，提示各类出错信息*/
      Alert({
        text: result.message
      });
      return false;
    }


  };



  //填写会员信息
  changeCustomerDetailField = ({ field, value }) => {
    this.dispatch('change:customerDetailField', { field, value });
  };

  getArea = (value: number[]) => {
    this.dispatch('detail:area', value);
  };

  /**完善账户信息*/
  doPerfect = async () => {
    const customer = this.state()
      .get('customerDetail')
      .toJS();
    /**先校验必填项*/
    if (
      _testName(customer.customerName) &&
      _testContact(customer.contactName) &&
      _testTel(customer.contactPhone)
    ) {
      //不为空，且不是不限
      if (customer.provinceId) {
        if (!_testAddress(customer.customerAddress)) {
          return false;
        }
      }
      if (customer.customerAddress != '') {
        //未选择省份或者选择不限时，都视为未选择
        if (customer.provinceId == '' || customer.provinceId == null) {
          Alert({
            text: '请选择所在地区！'
          });
          return false;
        }
      }
      const { code, message, context } = await webapi.doPerfect(customer);
      if (code == config.SUCCESS_CODE) {
        //清除缓存
        localStorage.removeItem(cache.PENDING_AND_REFUSED);
        //是否直接可以登录 0 否 1 是
        if (!(context as any).isLoginFlag) {
          Alert({
            text: '提交成功，将会尽快给您审核！'
          });
          //弹框关闭
          this.dispatch('loginModal:toggleVisible', {
            visible: false,
            callBack: null
          });
          _.showRegisterModel((context as any).couponResponse, false);
        } else {
          // a.设置登陆后token以及登陆信息缓存
          window.token = (context as any).token;
          localStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
          sessionStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
          // 登录成功，发送消息，查询分销员信息 footer/index.tsx
          msg.emit('userLoginRefresh');
          // b.登陆成功,需要合并登录前和登陆后的购物车,传入回调函数
          mergePurchase(this.state().get('callBack'));
          // c.登陆成功,关闭弹框
          Alert({ text: '登录成功' });
          //关闭弹框
          this.dispatch('loginModal:toggleVisible', {
            visible: false,
            callBack: null
          });
          _.showRegisterModel((context as any).couponResponse, true);
        }
      } else {
        Alert({
          text: message
        });
        return false;
      }
    }
  };

  //切换注册协议登录显示
  toggleShowAgreement = () => {
    this.dispatch('loginModal:toggleShowAgreement');
  };
}

