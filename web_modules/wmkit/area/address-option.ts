import provinces from './provinces.json'
import cities from './cities.json'
import areas from './areas.json'

areas.forEach((area) => {
  const matchCity = cities.filter(city => city.code === area.parent_code)[0];
  if (matchCity) {
    matchCity.c = matchCity.c || [];
    matchCity.c.push({
      l: area.name,
      v: area.code,
    });
  }
});

cities.forEach((city) => {
  const matchProvince = provinces.filter(province => province.code === city.parent_code)[0];
  if (matchProvince) {
    matchProvince.c = matchProvince.c || [];
    matchProvince.c.push({
      l: city.name,
      v: city.code,
      c: city.c,
    });
  }
});

const options = provinces.map(province => ({
  l: province.name,
  v: province.code,
  c: province.c,
}));

export default options;