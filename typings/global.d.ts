declare const __DEV__: boolean;
declare const IMAGE_SERVER: string;
declare const BFF_HOST: string;

declare const System: {
  import(module: string): Promise<any>;
};

declare interface AsyncResult<T> {
  res: T;
  err: Error;
}

//返回结果泛型
declare interface Result<T> {
  code: string;
  message: string;
  context: T;
}

interface Window {
  token: any | string;
  xDllContext: any;
  XSite_Info: any;
  wechatConfig: any;
  needLogin: boolean;
  distributor: boolean;
}

interface Date {
  Format: any;
}
