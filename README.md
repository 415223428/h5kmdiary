## b2b-wechat

## 准备开始

```sh
#使用yarn作为npm的管理工具
npm install -g yarn
#clone项目到本地
git clone https://git.coding.net/wmzhangjin/b2b-wechat.git
git clone http://47.244.51.250:10180/root/ks-wechat

cd b2b-wechat
#安装依赖
# yarn
yarn --registry=http://119.23.201.142:7001

#本地测试
yarn start

# build dll
yarn run build:dll

# build 项目
yarn run build

# 上线
yarn run build:all
```

## 技术栈 -- 单页面应用

项目构建:
webpack 上线打包时使用
webpack-dev-server 开发人员平日开发使用
babel 编译

规范/格式化:
tslint 约定/检查前端规范
prettier 代码格式化工具

基础架构:
react 前端基础框架
ES6 增加了便捷的语法糖以及 API
typescript 相当于对 javascript 封装,api 更清晰,有强语言倾向

状态管理:
plume2 自研的状态管理框架-类似 redux

工具 util:
moment 日期/时间组件
immutable 不可变对象工具
...

UI 组件:
ant 阿里的 react-ui 组件
css/less 混用

## 统一使用 prettier 作为项目的格式化工具，it's amazing.

> 编码规范不该是一种束缚，而是一种便于团队协作沟通的约束。

编码其实很多的时候是一种很自我的一种创造性地表达，为了磨平这种过分的自我，我们采用
互不迁就融入社区的做法。我们使用社区最流行的编码规范来约束彼此。

## The most popular javascript Guide

https://github.com/airbnb/javascript

## React?

1.  模块的命名以中划线分割，如：user-guide, goods-list

2.  使用 space 代替 tab，保持缩进为两个空格，代码更紧凑明快

3.  引入模块依赖，尽量使用 ES6 的 import(尽管还不完美)，但是未来以来

```javascript
  import React from 'react'
  import {Relax} from 'plume2'

  @Relax
  export default class XXX extends React.Component<any, any> {
    props: {
      form?: any
      relaxProps?: {
      }
    }

    static relaxProps = {

    }

    constructor(props) {
      super(props)
    }

    render() {
      return (
        <div></div>
      )
    }
  }


  const styles = {
    container: {
      display: 'flex',
    }
  }
```

5.  引入模块的顺序，先引入系统级别，再引入库级别，然后是我们自己的模块(参考 python)，如：

```javascript
import fs from 'fs'; //nodejs
import path from 'path'; //nodejs

import React from 'react-native'; //libary
import { Relax } from 'plume2'; //libary

import wmkit from 'wmkit'; // our module
```

6.  在 React 的 class 中，在 render 方法之前都是 React 的生命周期方法，在 render 方法之后才是我们的业务方法，如果是私有方法以下划线开头。

```javascript
import React from 'react';
import { Relax } from 'plume2';

export default class MyComponent extends React.Component<any, any> {
  static defaultProps = {};

  defaultState() {
    return {};
  }

  constructor(props) {
    super(props);
  }

  /**
   * 组件渲染完成，一般用于监控资源
   */
  componentDidMount() {}

  /**
   * 组件更新完成
   */
  componentDidUpdate() {}

  /**
   * 组件被从scene中remove了
   */
  componentWillUnmount() {}

  /**
   * virtualdom
   */
  render() {}

  /**
   * 处理用户登录
   * @private
   */
  _handleUserLogin() {}

  /**
   * 处理按钮的点击
   * @private
   */
  _handleBtnTouch() {}

  getUserInfo() {}
}

const styles = {
  container: {
    display: 'flex'
  }
};
```

7.  讨论： 建议每个方法之间空两行，方法之间不要那么紧凑，好吧，再次暴露了，我是 python 脑残粉

标准 plume2 项目结构

```sh
➜  task-list git:(master) tree -L 2
.
├── components
│   ├── header.tsx
│   ├── index.tsx
│   └── task-list.tsx
├── index.tsx
├── store.ts
└── webapi.ts

1 directory, 6 files
```

## 自动化构建

在原来的启动命令后面加入类型参数，仅支持 dev,test1,test2 和 online 四个类型
例如：yarn start:dev:链接本地后台
yarn start:test1:链接 118 后台
yarn start:test2:链接 120 后台
yarn start:online:链接线上后台
部署生产环境同理
例如:yarn build:all:test1,部署 118 环境
yarn build:all:test2,部署 120 环境
yarn build:all:online,部署线上环境
原来的 yarn start 和 build:all 依然能够正常运行，但是不会自动覆盖 config 文件
