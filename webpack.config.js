var path = require("path");
var webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require("copy-webpack-plugin");

var autoprefixer = require("autoprefixer");
var precss = require("precss");
var values = require("postcss-modules-values");

function absolePath(module) {
    return path.resolve(__dirname, module);
}

module.exports = {
    cache: true,
    entry: absolePath("./src/index.tsx"),
    mode: 'development',
    output: {
        path: absolePath("./build"),
        publicPath: "/",
        filename: "bundle-[chunkhash].js"
    },
    resolve: {
        extensions: [".js", ".json", ".ts", ".tsx"],
        modules: ["node_modules", absolePath("./web_modules")],
    },
    module: {
        rules: [{
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loaders: ["babel-loader?cacheDirectory=true", "ts-loader"]
            },
            {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, "node_modules/plume2"),
                    path.resolve(__dirname, "node_modules/webpack-dev-server")
                ],
                loader: "babel-loader"
            },
            {
                test: /\.css/,
                use: [{
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            plugins: function() {
                                return [precss, autoprefixer, values];
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(png|ico|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)$/,
                loader: "url-loader?limit=100000"
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "[name].style.css",
            allChunks: true
        }),
        new webpack.DefinePlugin({
            __DEV__: true
        }),
        new HtmlWebpackPlugin({
            env: true,
            filename: "index.html",
            template: "./index.ejs"
        }),
        new webpack.SourceMapDevToolPlugin({
            filename: "[name].js.map",
            exclude: ["vendor.js"]
        }),
        // new webpack.optimize.LimitChunkCountPlugin({
        //   maxChunks: 15, // Must be greater than or equal to one
        //   minChunkSize: 6
        // }),

        /**
         * ping++微信环境支付宝支付中转页面pay.htm
         * 微信授权文件 *.txt
         */
        new CopyWebpackPlugin([{
                from: absolePath("./web_modules/pay_static/pay.htm"),
                to: absolePath("./build/pay-online")
            },
            {
                from: absolePath("./web_modules/pay_static"),
                to: absolePath("./build")
            },
            {
                from: absolePath("./web_modules/wmkit/iconfont.css"),
                to: absolePath("./build")
            }
        ])
    ],
    devServer: {
        port: 4000,
        host: "0.0.0.0",
        disableHostCheck: true,
        historyApiFallback: true
    }
};