var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin')

function absolePath(module) {
  return path.resolve(__dirname, module);
}

module.exports = {
  mode: 'production',
  entry: {
    vendor: [
      'plume2',
      'antd-mobile',
      'hammerjs',
      'moment',
      'react',
      'rc-dialog',
      'url-parse',
      'swiper',
      'zscroller',
      'react-router-dom',
      'whatwg-fetch',
      'classnames'
    ]
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    filename: '[name].dll.js',
    /**
     * output.library
     * 将会定义为 window.${output.library}
     * 在这次的例子中，将会定义为`window.vendor_library`
     */
    library: '[name]_library'
  },
  module: {
    rules: [
      {
        test: /.js$/,
        include: [
          path.resolve(__dirname, 'node_modules/plume2')
        ],
        loader: 'babel-loader?cacheDirectory=true'
      }
    ]
  },
  optimization: {
    runtimeChunk: true,
    splitChunks: {
      chunks: 'all', // 进行模块拆分 提取公共代码 相当于webpack2 的 CommonsChunkPlugin
    },

    // minimizer: true, // 美化代码 相当于webpack2 的 UglifyJsPlugin mode 为 production 自动 true
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.DllPlugin({
      /**
       * path
       * 定义 manifest 文件生成的位置
       * [name]的部分由entry的名字替换
       */
      path: path.join(__dirname, './dist', '[name]-manifest.json'),
      /**
       * name
       * dll bundle 输出到那个全局变量上
       * 和 output.library 一样即可。
       */
      name: '[name]_library'
    }),
    // new webpack.optimize.UglifyJsPlugin({
    //   compress: {
    //     warnings: false
    //   }
    // }),
    /**
     * ping++微信环境支付宝支付中转页面pay.htm
     * 微信授权文件 *.txt
     */
    new CopyWebpackPlugin(
      [{
        from: absolePath('./web_modules/pay_static/pay.htm'),
        to: absolePath('./dist/pay-online')
      },
      {
        from: absolePath('./web_modules/pay_static'),
        to: absolePath('./dist')
      }, {
        from: absolePath('./web_modules/wmkit/iconfont.css'),
        to: absolePath('./dist')
      }]
    )
  ]
};
