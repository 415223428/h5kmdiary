import { Action, Actor } from 'plume2';

export default class UserSafeActor extends Actor {
  defaultState() {
    return {
      wxFlag: false
    };
  }

  @Action('INIT')
  init(state, { wxFlag }) {
    return state.set('wxFlag', wxFlag);
  }
}
