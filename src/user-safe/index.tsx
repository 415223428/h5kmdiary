import React from 'react';
import { StoreProvider } from 'plume2';
import { Link } from 'react-router-dom';
import { history, Alert } from 'wmkit';
import { cache } from 'config';

import AppStore from './store';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class UserSafe extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    const accountName = JSON.parse(localStorage.getItem(cache.LOGIN_DATA))
      .accountName;

    return (
      <div>
        <div
          className="row form-item"
          style={{ alignItems: 'center' }}
          onClick={() => history.push('/user-safe-password')}
        >
          <span className="form-text">登录密码</span>
          <i className="iconfont icon-jiantou1" />
        </div>
        <div
          className="row form-item"
          style={{ alignItems: 'center' }}
          onClick={() => history.push(`/balance-pay-password`)}
        >
          <span className="form-text">支付密码</span>
          <i className="iconfont icon-jiantou1" />
        </div>
        <div
          className="row form-item"
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
          onClick={() => {
            Alert({
              text: '修改绑定手机将会同时修改您的登录手机，请您谨慎操作！',
              time: 3000,
              cb: () => history.push('/user-safe-mobile')
            });
          }}
        >
          <span className="form-text">手机绑定</span>
          <div>
            <span className="form-text" style={{ marginRight: 5 }}>
              {accountName}
            </span>
            <i className="iconfont icon-jiantou1" />
          </div>
        </div>
        <div
          className="row form-item"
          style={{ alignItems: 'center' }}
          onClick={() => history.push('/linked-account')}
        >
          <span className="form-text">关联账号</span>
          <i className="iconfont icon-jiantou1" />
        </div>
      </div>
    );
  }
}
