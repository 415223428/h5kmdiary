import { Store, IOptions } from 'plume2';

import { config } from 'config';

import UserSafeActor from './actor/user-safe-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new UserSafeActor()];
  }

  init = async () => {
    const res = await webapi.fetchWxLoginStatus();
    if (res.code == config.SUCCESS_CODE) {
      this.dispatch('INIT', res.context);
    }
  };
}
