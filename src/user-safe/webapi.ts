import { Fetch } from 'wmkit';

/**
 * 获取微信授权登录开关
 */
export const fetchWxLoginStatus = () => {
  return Fetch('/third/login/wechat/status/MOBILE');
};
