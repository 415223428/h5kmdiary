import React from 'react';
import {StoreProvider} from 'plume2';
import AppStore from './store';
import EmailList from "./component/email-list";


@StoreProvider(AppStore, {debug: __DEV__})
export default class UserEmail extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    return (
      <EmailList/>
    )
  }

}
