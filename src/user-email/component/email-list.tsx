import React from 'react';
import { Link } from 'react-router-dom';

import { IMap, Relax } from 'plume2';
import { Alert, Confirm, noop } from 'wmkit';
import { IList } from 'typings/globalType';
require('../css/style.css')
const add=require('../img/add.png')
const del=require('../img/del.png')
const edit=require('../img/edit.png')
@Relax
export default class EmailList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      emailList: IList;
      emailInfo: IMap;
      getEmailCount: Function;
      onShowModal: Function;
      onCancelModal: Function;
      checkParameter: Function;
      setParameter: Function;
      doSaveCustomerEmail: Function;
      deleteCustomerEmailById: Function;
    };
  };

  static relaxProps = {
    emailList: 'emailList',
    emailInfo: 'emailInfo',
    getEmailCount: noop,
    onShowModal: noop,
    onCancelModal: noop,
    checkParameter: noop,
    setParameter: noop,
    doSaveCustomerEmail: noop,
    deleteCustomerEmailById: noop
  };

  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    window.addEventListener('scroll', this._initScroll);
  }

  render() {
    const { emailList } = this.props.relaxProps;
    return (
      <div style={{paddingTop:' 0.1rem',background: '#fafafa',paddingBottom:' calc(100vh - 4rem)'}}>
        {emailList &&
          emailList.toJS().map((o) => {
            return (
              <div key={o.customerEmailId} className='mail-list'>
                <p style={{ display: 'flex', flex: 1 }}>{o.emailAddress}</p>
                <div className='btn-img'>
                  {/* <i
                    className="iconfont icon-bianji"
                    style={{ paddingRight: 10 }}
                    onClick={() => {
                      this._showConfirmModal(o);
                    }}
                  />
                  <i
                    className="iconfont icon-shanchu"
                    onClick={() => {
                      this._handleDelete(o.customerEmailId);
                    }}
                  /> */}
                  <img src={edit} alt="" onClick={() => {
                       this._showConfirmModal(o);
                    }}/>
                    <img src={del} alt="" onClick={() => {
                     this._handleDelete(o.customerEmailId);
                    }}/>
                </div>
              </div>
            );
          })}
          <div className="grey-tips">
            <p>
              提示：您最多可以添加5条财务邮箱信息,银联企业付款时，系统自动发送付款通知至财务邮箱
            </p>
          </div>
        <div className="register-btn" style={{padding:'0'}}>
          
          {/* <button
            className="btn btn-primary"
            style={{background: 'linear-gradient(to right, #FF6A4D, #FF1A1A)'}}
            onClick={() => this._addCustomerEmail()}
          >
            新增
          </button> */}
          <img src={add} alt="" style={{width:'100%'}} onClick={() => this._addCustomerEmail()}/>
        </div>
      </div>
    );
  }

  /**
   * 新增财务邮箱信息
   * @private
   */
  _addCustomerEmail = () => {
    const { getEmailCount } = this.props.relaxProps;
    const accountCount = getEmailCount();
    if (accountCount < 5) {
      const emailInfo = {
        emailAddress: ''
      };
      this._showConfirmModal(emailInfo);
    } else {
      Alert({ text: '您最多可以添加5条财务邮箱信息' });
    }
  };

  /**
   * IOS端点击input键盘收起的事件处理
   * @private
   */
  _initScroll = () => {
    document.body.addEventListener('focusout', () => {
      //软键盘收起的事件处理
      window.scrollTo(0, 0);
    });
  };

  /**
   * 显示修改弹窗
   * @param emailInfo
   * @private
   */
  _showConfirmModal = (emailInfo) => {
    const {
      onShowModal,
      onCancelModal,
      checkParameter,
      setParameter,
      doSaveCustomerEmail
    } = this.props.relaxProps;
    onShowModal(emailInfo);
    Confirm({
      text: (
        <input
          placeholder="邮箱地址"
          defaultValue={emailInfo.emailAddress}
          maxLength={32}
          onChange={(val) => {
            setParameter('emailAddress', val.target.value);
          }}
          style={styles.modalInput}
        />
      ),
      okBtn: '确定',
      cancelBtn: '取消',
      maskClose: false,
      confirmCb: () => doSaveCustomerEmail(emailInfo),
      cancel: () => {
        onCancelModal();
      }
    });
  };

  /**
   * 修改邮箱信息
   * @param value
   * @private
   */
  _setEmailAddress(value) {
    const { checkParameter, setParameter } = this.props.relaxProps;
    checkParameter(value);
    setParameter('emailAddress', value);
  }

  /**
   * 删除财务邮箱信息
   */
  _handleDelete(customerEmailId) {
    const { deleteCustomerEmailById } = this.props.relaxProps;
    Confirm({
      text: '确定删除该财务邮箱信息吗？',
      okBtn: '确定',
      cancelBtn: '取消',
      confirmCb: function() {
        deleteCustomerEmailById(customerEmailId);
      }
    });
  }
}

const styles = {
  listItem: {
    paddingTop: 30,
    paddingLeft: 20,
    paddingRight: 20,
    fontSize: 16,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  } as any,
  modalInput: {
    height: 35,
    width: '100%',
    border: '1px solid #999',
    borderRadius: 3,
    paddingLeft: 10
  }
};
