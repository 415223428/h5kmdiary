import { Alert, ValidConst } from 'wmkit';
import { IOptions, Store } from 'plume2';

import * as webapi from './webapi';
import UserEmail from './actor/user-email';
import { config } from 'config';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new UserEmail()];
  }

  /**
   * 初始化数据
   */
  init = async () => {
    const { context, code, message } = await webapi.fetchCustomerEmailList();
    if (code === config.SUCCESS_CODE) {
      this.dispatch('customer:emailList', context);
    } else {
      Alert({ text: message });
    }
  };

  /**
   * 保存邮箱
   * @returns {Promise<void>}
   */
  doSaveCustomerEmail = () => {
    const formData = this.state().get('emailInfo');
    const checkRes = this.checkParameter(formData.get('emailAddress'));
    if (checkRes) {
      let msgStr = '操作成功!';
      let resultPromise;
      if (formData.get('customerEmailId')) {
        resultPromise = webapi.updateCustomerEmail(formData);
        msgStr = '编辑成功!';
      } else {
        resultPromise = webapi.addCustomerEmail(formData);
        msgStr = '新增成功!';
      }
      resultPromise.then((result) => {
        if (result.code === config.SUCCESS_CODE) {
          Alert({ text: msgStr });
          this.init();
        } else {
          Alert({ text: result.message });
        }
      });
    }
    return checkRes;
  };

  /**
   * 删除会员财务邮箱
   */
  deleteCustomerEmailById = async (customerEmailId) => {
    const { code } = await webapi.deleteCustomerEmailById(customerEmailId);
    if (code == config.SUCCESS_CODE) {
      Alert({ text: '删除成功' });
      this.init();
    }
  };

  /**
   * 获取会员财务邮箱的数量
   */
  getEmailCount = () => {
    if (this.state().get('emailList')) {
      return this.state()
        .get('emailList')
        .toJS().length;
    } else {
      return 0;
    }
  };

  /**
   * 显示弹窗
   */
  onShowModal = (emailInfo) => {
    this.setParameter('customerEmailId', emailInfo.customerEmailId);
    this.setParameter('customerId', emailInfo.customerId);
    this.setParameter('emailAddress', emailInfo.emailAddress);
  };

  /**
   * 弹框取消
   */
  onCancelModal = () => {
    this.setParameter('customerEmailId', '');
    this.setParameter('customerId', '');
    this.setParameter('emailAddress', '');
  };

  /**
   * 值改变
   * @param key
   * @param value
   */
  setParameter = (key, value) => {
    this.dispatch('customer:email:edit', { key, value });
  };

  /**
   * 邮箱格式校验
   * @param emailAddress
   * @returns {boolean}
   */
  checkParameter = (emailAddress) => {
    // 正则校验邮箱格式
    // const re = ValidConst.email;
	  const re = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;
    if (emailAddress.trim() == '') {
      Alert({ text: '请输入邮箱地址' });
      return false;
    } else if (!re.test(emailAddress)) {
      Alert({ text: '您填写的邮箱格式有误，请检查后重新输入' });
      return false;
    } else {
      return true;
    }
  };
}
