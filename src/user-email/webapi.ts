import {Fetch} from 'wmkit';
import {IMap} from 'typings/globalType';

/**
 * 查询会员的财务邮箱信息列表
 */
export const fetchCustomerEmailList = () => {
  return Fetch<Result<any>>('/customer/emailList/', {
    method: 'GET'
  })
}

/**
 * 新增会员财务邮箱
 */
export const addCustomerEmail = (formData: IMap) => {
  return Fetch<Result<any>>('/customer/email/', {
    method: 'POST',
    body: JSON.stringify(formData)
  })
};


/**
 * 修改会员财务邮箱
 */
export const updateCustomerEmail = (formData: IMap) => {
  return Fetch<Result<any>>('/customer/email/', {
    method: 'PUT',
    body: JSON.stringify(formData)
  })
};

/**'
 *
 * 删除会员财务邮箱
 */
export const deleteCustomerEmailById = (customerEmailId) => {
  return Fetch<Result<any>>('/customer/email/' + customerEmailId, {
    method: 'DELETE'
  })
}