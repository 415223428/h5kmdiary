import {fromJS} from 'immutable'

import {Actor, Action} from 'plume2'
import {IMap} from 'typings/globalType'

export default class UserEmail extends Actor {
  defaultState() {
    return {
      // 用户财务邮箱列表
      emailList: [],
      // 弹窗邮箱内容
      emailInfo: {
        customerEmailId: '',
        customerId: '',
        emailAddress: ''
      },
    }
  }

  /**
   * 邮箱列表
   */
  @Action('customer:emailList')
  emailList(state: IMap, res) {
    return state.set('emailList', fromJS(res));
  }

  /**
   * 修改表单值
   */
  @Action('customer:email:edit')
  setParameter(state, {key, value}) {
    return state.setIn(['emailInfo', key], value);
  }

}