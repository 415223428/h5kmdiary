import { Action, Actor, IMap } from 'plume2';

export default class GrouponRuleActor extends Actor {
  defaultState() {
    return {
      context: ''
    };
  }

  /**
   * 拼团规则
   *
   * @param {IMap} state
   * @param {string} context
   * @returns
   * @memberof GrouponRuleActor
   */
  @Action('init')
  init(state: IMap, context) {
    return state.set('context', context);
  }
}
