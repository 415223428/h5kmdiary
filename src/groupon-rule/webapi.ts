import { Fetch } from 'wmkit';
/**
 * 获取拼团设置
 * @type {Promise<AsyncResult<T>>}
 */
export const fetchGrouponSetting = () => {
  return Fetch('/groupon/setting/info');
};
