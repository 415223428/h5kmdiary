import React from 'react';
import { StoreProvider } from 'plume2';
import { GrouponBottom } from 'wmkit';
import AppStore from './store';
const style = require('./css/style.css');

@StoreProvider(AppStore)
export default class AboutUs extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '玩法介绍';
    this.store.init();
  }

  render() {
    return (
      <div className="groupon-rule">
        <div className="select-list-title">
          <img src={require('./img/title.png')} alt="" />
          <span>玩法介绍</span>
          <img src={require('./img/title.png')} alt="" />
        </div>
        <div
          className="about-box"
          dangerouslySetInnerHTML={{
            __html: this.store.state().get('context')
          }}
        >
        </div>
        {/* <img src={require('./img/howToPlay.png')} alt="howToPlay" style={{display: 'block',margin:'0 auto',width:'90%'}}/> */}
        <GrouponBottom currTab={'玩法介绍'} />
      </div>
    );
  }
}
