import { Store } from 'plume2';
import { config } from 'config';
import * as webapi from './webapi';
import GrouponRuleActor from './actor/groupon-rule-actor';
export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new GrouponRuleActor()];
  }

  init = async () => {
    const res = (await webapi.fetchGrouponSetting()) as any;
    const { code, context } = res;
    if (code == config.SUCCESS_CODE) {
      let rules =
        context && context.grouponSettingVO
          ? context.grouponSettingVO.rule
          : '';
      this.dispatch('init', rules);
    }
  };
}
