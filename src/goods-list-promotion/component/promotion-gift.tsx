import React, { Component } from "react";
import { Relax } from "plume2";

import { noop } from 'wmkit'
import moment from 'moment'
import { Const } from 'config'

const styles = require('../css/style.css')

@Relax
export default class PromotionGift extends Component<any, any> {

  props: {
    relaxProps?: {
      marketing: any

      changeGiftShow: Function
    }
  }


  static relaxProps = {
    marketing: 'marketing',

    changeGiftShow: noop,
  }

  render() {
    const { marketing, changeGiftShow } = this.props.relaxProps
    const { beginTime, endTime, subType, fullGiftLevelList } = marketing

    let text = ''
    if (subType == 4) {
      text = fullGiftLevelList.map(m => m.fullAmount).toString()
    } else if (subType == 5) {
      text = fullGiftLevelList.map(m => m.fullCount).toString()
      if (text.length > 0) {
        text = text + '件'
      }
    }

    return (
      <div style={{ height: '1.6rem'}}>
        <div 
          className={styles.headerBg}
          style={{
           height:'1.6rem',
            color:'#fff', fontSize:'0.26rem',
            padding:'0 0.3rem 0.3rem', display:'flex', justifyContent:'space-between', alignItems:'center'
          }}
        >
          {
            beginTime && endTime &&
            <div className="info" style={{width:'60%',height:'1.0rem'}}>
              <p> {beginTime && moment(beginTime).format(Const.DATE_FORMAT)}至
              {endTime && moment(endTime).format(Const.DATE_FORMAT)}&nbsp;以下商品可参加满赠活动</p>
              <p>赠品赠完为止</p>
            </div>
          }
          {/* <div className="right" onClick={() => changeGiftShow()}>
            <i className="iconfont icon-cc-gift"></i>
            <span>查看赠品</span>
          </div> */}
          <div className="right">
            <p>满{text}获赠品</p>
          </div>
        </div>
      </div>

    )
  }
}
