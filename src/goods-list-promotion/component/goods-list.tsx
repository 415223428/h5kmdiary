import React, { Component } from 'react';
import { fromJS } from 'immutable';
import { IMap, Relax } from 'plume2';

import { Blank, ListView, noop, WMkit } from 'wmkit';
import { IList } from 'typings/globalType';

import GoodsItem from './goods-item';

const noneImg = require('../img/none.png');

@Relax
export default class GoodsList extends Component<any, any> {
  _listView: any;

  props: {
    relaxProps?: {
      marketingId: any;
      initialEnd: boolean;
      cateIds: IList;
      brandIds: IList;
      sortType: IMap;
      type: number;

      changeGoodsNum: Function;
      query: (data: any) => void;
      goodsMarketing: IMap;
      isShow: boolean;
    };
  };

  static relaxProps = {
    marketingId: 'marketingId',
    initialEnd: 'initialEnd',
    cateIds: 'cateIds',
    brandIds: 'brandIds',
    sortType: 'sortType',
    type: 'type',

    changeGoodsNum: noop,
    query: noop,

    goodsMarketing: 'goodsMarketing',
    isShow: 'isShow'
  };

  constructor(props) {
    super(props);
    this.state = {
      // 防止修改商品数量,引起参数变化,重新查询商品列表(刷新页面,体验不好)
      dtoListParam: JSON.parse(localStorage.getItem(WMkit.purchaseCache())) || []
    };
  }

  render() {
    let {
      marketingId,
      changeGoodsNum,
      cateIds,
      brandIds,
      sortType,
      query,
      goodsMarketing,
      isShow
    } = this.props.relaxProps;

    // 排序标识
    let sortFlag = 0;
    if (sortType.get('type') == 'dateTime' && sortType.get('sort') == 'desc') {
      sortFlag = 1;
    } else if (
      sortType.get('type') == 'dateTime' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 1;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'desc'
    ) {
      sortFlag = 2;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 3;
    } else if (sortType.get('type') == 'salesNum') {
      sortFlag = 4;
    } else if (sortType.get('type') == 'evaluateNum') {
      sortFlag = 5;
    } else if (sortType.get('type') == 'praise') {
      sortFlag = 6;
    } else if (sortType.get('type') == 'collection') {
      sortFlag = 7;
    }
    let skuUrl = '/goods/skuListFront';
    if (WMkit.isLoginOrNotOpen()) {
      skuUrl = '/goods/skus';
    }
    return (
      <ListView
        url={skuUrl}
        style={{ height: window.innerHeight - 160 }}
        params={{
          cateIds: cateIds,
          brandIds: brandIds,
          sortFlag: sortFlag,
          pageNum: 0,
          marketingId,
          cateAggFlag: true,
          esGoodsInfoDTOList: this.state.dtoListParam
        }}
        otherProps={['goodsIntervalPrices']}
        renderRow={(goodsItem: IMap) => {
          return (
            <GoodsItem
              goodsItem={fromJS(goodsItem)}
              changeGoodsNum={changeGoodsNum}
              goodsMarketing={goodsMarketing}
              marketingId={marketingId}
              isShow={isShow}
            />
          );
        }}
        renderEmpty={() => <Blank img={noneImg} content="没有搜到任何商品" />}
        ref={(_listView) => (this._listView = _listView)}
        onDataReached={query}
      />
    );
  }
}
