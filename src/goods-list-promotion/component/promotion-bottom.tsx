import React, { Component } from "react";
import { Relax } from "plume2";
import { history, _ } from 'wmkit';
import { Check } from 'wmkit';

const styles = require('../css/style.css')
const purCar=require('../img/pur-car.png')

@Relax
export default class PromotionBottom extends Component<any, any> {

  props: {
    relaxProps?: {
      calc: any
      marketing: any
    }
  }

  static relaxProps = {
    calc: 'calc',
    marketing: 'marketing',
  }

  render() {

    const { calc, marketing } = this.props.relaxProps
    let lackDesc = ''
    const { subType } = marketing
    if (subType === 0 || subType === 2 || subType === 4) {
      lackDesc = '￥' + (calc.lack ? calc.lack : 0)
    } else if (subType === 1 || subType === 3 || subType === 5) {
      lackDesc = (calc.lack ? calc.lack : 0) + '件'
    }
    let desc = '';
    if (marketing.marketingType === 0) {
      desc = '可减￥' + calc.discount;
    } else if (marketing.marketingType === 1) {
      desc = '可享' + _.mul(calc.discount || 0, 10) + '折';
    } else if (marketing.marketingType === 2) {
      desc = '可获赠品';
    }

    return (
      <div style={{ height: '0.98rem' }}>
        <div className={styles.proTotal}>
          <div className="info" style={{display:'flex',alignItems:'center'}}>
            <div className="check-left" style={{display:'flex',alignItems:'center'}}>
              {/* 全选方法未实现 */}
              <Check
                checkStyle={{width:'0.38rem',height:'0.38rem',lineHeight:'0.38rem'}}
              />
              <span className="grey" style={{fontSize:'0.28rem',color:'#999',marginLeft:'0.1rem'}}>全选</span>
            </div>
            <div className="right" style={{marginLeft:'0.2rem'}}>
              <div style={{display:'flex'}}>
                <span style={{fontSize:'0.28rem',color:'#333',fontWeight:'bold'}}>商品总额:
                  <i className="iconfont icon-qian" style={{color:'#FF4D4D'}}></i>
                  <span style={{color:'#FF4D4D'}}>{calc.totalAmount || 0}</span>
                </span>
                <span className="criteria" style={{fontSize:'0.2rem',marginLeft:'0.1rem'}}>已选
                  {/* {calc.goodsInfoList ? calc.goodsInfoList.filter(goodsInfo => goodsInfo.buyCount).length : 0}
                  &nbsp;种&nbsp; */}
                  <span style={{color:'#FF4D4D'}}>{calc.totalCount}</span>件</span>
              </div>
              {
                calc.lack === 0 ?
                  <div className="bottom" style={{fontSize:'0.2rem'}}>
                    {/* <i className="iconfont icon-success"></i> */}
                    <span>满足条件&nbsp;{desc}</span>
                  </div>
                  :
                  <div className="bottom" style={{fontSize:'0.2rem'}}>
                    {/* <i className="iconfont icon-jinggao"></i> */}
                    <span>未满足条件&nbsp;还差{lackDesc}</span>
                  </div>
              }
            </div>
          </div>
          {/* <button 
            onClick={() => history.push('/purchase-order')}
            style={{
              background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))',color:'#fff',fontSize:'0.28rem',fontWeight:'bold',
              width:'1.9rem'
            }}
          >去购物车</button> */}
          <img src={purCar} alt=""  style={{width:'1.9rem'}} onClick={() => history.push('/purchase-order')}/>
        </div>
      </div>

    )

  }


}
