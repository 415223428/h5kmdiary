import React, { Component } from 'react';
import { Relax } from 'plume2';
import moment from 'moment'
import { Const } from 'config'
import { _ } from 'wmkit'

const styles = require('../css/style.css')
const _TYPE = {
  '0': { text: '减', color: '' },
  '2': { text: '赠', color: 'red' },
  '1': { text: '折', color: 'orange' },
}
@Relax
export default class PromotionDiscount extends Component<any, any> {

  props: {
    relaxProps?: {
      marketing: any
    }
  }

  static relaxProps = {
    marketing: 'marketing'
  }


  render() {
    const { marketing } = this.props.relaxProps
    const { beginTime, endTime, subType, fullReductionLevelList, marketingType, fullDiscountLevelList } = marketing
    let text = ''
    if (marketingType == 0) {
      if (subType == 0) {
        text = fullReductionLevelList.map(m => `满${m.fullAmount}减${m.reduction}`).toString()
      } else if (subType == 1) {
        text = fullReductionLevelList.map(m => `满${m.fullCount}件减${m.reduction}`).toString()
      }
    } else if (marketingType == 1) {
      if (subType == 2) {
        text = fullDiscountLevelList.map(m => `满${m.fullAmount}享${_.mul(m.discount, 10)}折`).toString()
      } else if (subType == 3) {
        text = fullDiscountLevelList.map(m => `满${m.fullCount}件享${_.mul(m.discount, 10)}折`).toString()
      }
    }

    return (
      <div style={{ height: '1.6rem' }}>
        <div 
          className={styles.headerBg}
          style={{
            height:'1.6rem',
            color:'#fff', fontSize:'0.26rem',
            padding:'0 0.3rem 0.3rem'
          }}
        >
          {
            beginTime && endTime &&
            <div className="info" style={{display:'flex',alignItems:'center',justifyContent:'space-between',flexDirection:'row'}}>
              <p style={{width:'50%',height:'1.0rem',lineHeight:'0.5rem'}}>{moment(beginTime).format(Const.DATE_FORMAT)}至
              {moment(endTime).format(Const.DATE_FORMAT)}&nbsp;以下商品可参加满
              {_TYPE[marketingType] && _TYPE[marketingType].text}活动</p>
              <p style={{fontSize:'0.32rem', fontWeight:'bold'}}>{text}</p>
            </div>
          }
        </div>
      </div>
    )

  }


}
