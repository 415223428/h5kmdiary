import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';
import { IList } from 'typings/globalType';
import { fromJS } from 'immutable';

const styles = require('../css/style.css');
const reset=require('../img/reset.png')
const confirm=require('../img/confirm.png')
@Relax
export default class GoodsTabBrand extends Component<any, any> {
  props: {
    relaxProps?: {
      brands: IList;

      closeShade: Function;
      chooseBrand: Function;
      clearBrands: Function;
      okBrands: Function;
    };
    hide: boolean;
  };

  static relaxProps = {
    brands: 'brands',

    closeShade: noop,
    chooseBrand: noop,
    clearBrands: noop,
    okBrands: noop
  };

  render() {
    const {
      closeShade,
      brands,
      chooseBrand,
      clearBrands,
      okBrands
    } = this.props.relaxProps;
    const brandList = (brands || fromJS([])).toJS();
    return (
      !this.props.hide && (
        <div
          style={{ width: '100%', display: 'flex', flexDirection: 'column' }}
        >
          <div className={styles.tabTtems}>
            <div className="items" style={{padding:'0.2rem'}}>
              {brandList.map((v, k) => {
                return (
                  <span
                    onClick={() => chooseBrand(v.brandId)}
                    key={k}
                    className={v.checked ? 'item select-item-checked' : 'item'}
                  >
                    {v.brandName + (v.nickName ? '(' + v.nickName + ')' : '')}
                    {v.checked && <i className="iconfont icon-gou" />}
                  </span>
                );
              })}
            </div>
            <div className="items-btn"
              style={{padding:'0.3rem',display:'flex',alignItems:'center',justifyContent:'space-between'}}
            >
              {/* <button 
                onClick={() => clearBrands()} className="reset-btn"
                style={{
                  width:'3.25rem',height:'0.7rem',background:'linear-gradient(135deg,rgba(255,139,8,1),rgba(247,187,88,1))',borderRadius:'0.1rem',
                  fontSize:'0.28rem',color:'#fff',lineHeight:'0.7rem', flex:'none'
                }}
              >
                重置
              </button> */}
              <img src={reset} alt="" onClick={() => clearBrands()} style={{ width:'3.25rem'}}/>
              {/* <button 
                onClick={() => okBrands()} className="check-btn"
                style={{
                  width:'3.25rem',height:'0.7rem',background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))',borderRadius:'0.1rem',
                  fontSize:'0.28rem',color:'#fff',lineHeight:'0.7rem', flex:'none'
                }}
              >
                确定
              </button> */}
              <img src={confirm} alt="" onClick={() => okBrands()} style={{ width:'3.25rem'}}/>
            </div>
          </div>
          <div
            style={{ width: '100%', height: '100%' }}
            onClick={() => closeShade()}
          />
        </div>
      )
    );
  }
}
