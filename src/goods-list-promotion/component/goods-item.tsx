import React from 'react';
import { _, history, WMImage } from 'wmkit';
import { GoodsNum, MarketingLabel } from 'biz';

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const { goodsItem, goodsMarketing, marketingId,isShow } = this.state;
    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 起订量
    const count = goodsInfo.get('count') || 0;
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const buyCount =
      invalid || (goodsMarketing && goodsMarketing.get(id) !== marketingId)
        ? 0
        : goodsInfo.get('buyCount') || 0;
    // 营销标签
    const marketingLabels = goodsInfo.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsInfo.get('couponLabels');

    //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
    //好评率
    let favorableRate = '100';
    if (goodsInfo.get('goodsEvaluateNum') && goodsInfo.get('goodsEvaluateNum') != 0){
      favorableRate = _.mul(_.div(goodsInfo.get('goodsFavorableCommentNum'),goodsInfo.get('goodsEvaluateNum')),100).toFixed(0);
    }

    //评论数
    let evaluateNum = '暂无';
    const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
    if (goodsEvaluateNum) {
      if (goodsEvaluateNum<10000){
        evaluateNum = goodsEvaluateNum;
      } else {
        const i = _.div(goodsEvaluateNum,10000).toFixed(1);
        evaluateNum =  i+'万+';
      }
    }

    //销量
    let salesNum='暂无';
    const goodsSalesNum = goodsInfo.get('goodsSalesNum');
    if (goodsSalesNum){
      if (goodsSalesNum<10000){
        salesNum = goodsSalesNum;
      } else {
        const i = _.div(goodsSalesNum,10000).toFixed(1);
        salesNum =  i+'万+';
      }
    }
    // 预估点数
    let pointNum = goodsInfo.get('kmPointsValue');
    //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆

    return (
      <div
        className="goods-list"
        onClick={() => history.push('/goods-detail/' + goodsItem.get('id'))}
      >
        <div className="img-box">
          <WMImage
            src={goodsInfo.get('goodsInfoImg')}
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail">
          <div className="title" style={{color:'#333',fontSize:'0.26rem',fontWeight:'bold'}}>{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>

          {/* 评价 */}
          {
            isShow?
              (
                <div className="goods-evaluate" style={{color:'#999',fontSize:'0.22rem'}}>
                  <span className="goods-evaluate-spn">{salesNum}销量</span>
                  <span className="goods-evaluate-spn mar-lr-28">{evaluateNum}评价</span>
                  <span className="goods-evaluate-spn">{favorableRate}%好评</span>
                  {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
                </div>
              )
              :
              (
                <div className="goods-evaluate">
                  <span className="goods-evaluate-spn">{salesNum}销量</span>
                </div>
              )
          }

          <div className="marketing">
            {goodsInfo.get('companyType') == 0 && (
              <div className="self-sales">自营</div>
            )}
            {(marketingLabels || couponLabels) && (
              <MarketingLabel
                marketingLabels={marketingLabels}
                couponLabels={couponLabels}
              />
            )}
            {invalid && <div className="out-stock">缺货</div>}
          </div>
          <div className="bottom">
            <span className="price" style={{fontSize:'0.32rem',color:'#FF4D4D'}}>
              <i className="iconfont icon-qian" style={{fontWeight:'normal',fontSize:'0.32rem'}} />
              <span style={{fontWeight:'bold'}}>{this._calShowPrice(goodsItem, buyCount)}</span>
            </span>
            <GoodsNum
              value={buyCount}
              max={stock}
              disableNumberInput={invalid}
              goodsInfoId={id}
              onAfterClick={(num) =>
                this._afterChangeNum(id, marketingId, buyCount, num)
              }
            />
          </div>
        </div>
      </div>
    );
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (id, marketingId, buyCount, num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
    this.props.changeGoodsNum(id, marketingId, buyCount, num);
  };
}
