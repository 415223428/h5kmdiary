import React, { Component } from 'react';
import { Relax } from 'plume2';

import { fromJS, Map, List } from 'immutable';
import { Check, Alert, noop } from 'wmkit';

// 定义是否有底部确定按钮，这边牵扯到底部商品的滚动距离
const hasBottom = false;

/**
 * 通用促销底部弹出
 */
@Relax
export default class PanelBottom extends Component<any, any> {
  props: {
    relaxProps?: {
      giftShow: boolean;
      marketing: any;
      gift: any;

      changeGiftShow: Function;
    };
  };

  static relaxProps = {
    giftShow: 'giftShow',
    marketing: 'marketing',
    gift: 'gift',

    changeGiftShow: noop
  };

  render() {
    const { giftShow, changeGiftShow, marketing, gift } = this.props.relaxProps;
    const { fullGiftLevelList } = marketing;
    const { goodsInfos } = gift;
    return (
      giftShow && (
        <div style={{ display: 'block' }}>
          <div
            style={{
              position: 'fixed',
              left: 0,
              top: 0,
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(0,0,0,.5)',
              zIndex: 9999
            }}
            onClick={() => changeGiftShow()}
          />
          <div className="panel-bottom">
            <div className="panel-title b-1px-b">
              <h1>赠品</h1>
              <i
                className="iconfont icon-guanbi"
                onClick={() => changeGiftShow()}
              />
            </div>
            <div className="panel-body" style={styles.panelContent}>
              {(fullGiftLevelList || []).map((level, index) => {
                let rule =
                  marketing.subType === 4
                    ? level.fullAmount
                    : level.fullCount + '件';
                return (
                  <div className="gift b-1px-b">
                    <span className="gift-title">
                      满{rule}可获以下赠品，可选{level.giftType === 0
                        ? '全部'
                        : '一种'}
                    </span>
                    {goodsInfos &&
                      goodsInfos.map((v, k) => {
                        const detail = level.fullGiftDetailList.find(
                          (f) => f.productId == v.goodsInfoId
                        );
                        const stock = v.stock;
                        // 起订量
                        const count = v.count || 0;
                        // 库存等于0或者起订量大于剩余库存
                        const noneStock = stock <= 0;
                        if (detail) {
                          return (
                            <div
                              key={k}
                              className="goods-list"
                              onClick={() =>
                                v.goodsStatus != '2'
                                  ? (window.location.href = `/goods-detail/${
                                      v.goodsInfoId
                                    }`)
                                  : null
                              }
                            >
                              <img className="img-box" src={v.goodsInfoImg} />
                              <div className="detail">
                                <a className="title">{v.goodsInfoName}</a>
                                <p className="gec">{v.specText}</p>
                                <div className="bottom">
                                  <div className="bottom-state">
                                    <span className="price">
                                      <i className="iconfont icon-qian" />
                                      {v.marketPrice}
                                    </span>
                                    {v.goodsStatus == '2' && (
                                      <div className="out-stock">失效</div>
                                    )}
                                    {v.goodsStatus != '2' &&
                                      noneStock && (
                                        <div className="out-stock">缺货</div>
                                      )}
                                  </div>
                                  <span className="num">
                                    ×{detail.productNum}
                                    {v.goodsUnit}
                                  </span>
                                </div>
                              </div>
                            </div>
                          );
                        }
                      })}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )
    );
  }
}

const styles = {
  panelContent: {
    paddingBottom: hasBottom ? 48 : 0
  }
};
