import React from 'react';
import { StoreProvider } from 'plume2';

import AppStore from './store';
import GoodsList from './component/goods-list';
import GoodsTab from './component/goods-tab';
import GoodsTabShade from './component/goods-tab-shade';
import PromotionGift from './component/promotion-gift';
import PromotionDiscount from './component/promotion-discount';
import PromotionBottom from './component/promotion-bottom';
import PanelBottom from './component/panel-bottom';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsListPromotion extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    // 解析url中的参数
    const { mid } = this.props.match.params;
    this.store.init(Number(mid));
  }

  render() {
    const type = this.store.state().get('type');
    return (
      <div>
        {/*顶部促销*/}
        {type === 2 ? <PromotionGift /> : <PromotionDiscount />}

        {/*顶部的tab*/}
        <GoodsTab />

        {/*tab遮罩*/}
        <GoodsTabShade />

        {/*商品列表*/}
        <GoodsList />
        <PromotionBottom />
        <PanelBottom />
      </div>
    );
  }
}
