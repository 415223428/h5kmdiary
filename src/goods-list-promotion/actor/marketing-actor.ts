import { Action, Actor, IMap } from 'plume2'

import { fromJS } from 'immutable'

export default class MarketingActor extends Actor {
  defaultState() {
    return {
      // 营销信息
      marketing: {},
      // 计算结果
      calc: {},
      // 赠品信息
      gift: []
    }
  }

  /**
   * 获取开始结束时间
   */
  @Action('marketing: detail')
  fetchTime(state: IMap, marketing) {
    return state.set('marketing', marketing)
  }


  /**
   * 获取营销计算价格
   * @param {*} state 
   * @param {*} result 
   */
  @Action('marketing: calc')
  calc(state: IMap, result) {
    return state.set('calc', result)
  }


  /**
   * 获取赠品详情
   * @param {*} state 
   * @param {*} gift 
   */
  @Action('marketing: gift')
  gift(state: IMap, gift) {
    return state.set('gift', gift)
  }
}