import { Actor, Action, IMap } from 'plume2'

export default class LoadingActor extends Actor {
  defaultState() {
    return {
      loading: true
    }
  }

  @Action('goods: loading')
  loading(state: IMap, loading) {
    return state.set('loading', loading)
  }
}