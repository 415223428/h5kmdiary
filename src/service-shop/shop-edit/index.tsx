import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import { Tips } from 'wmkit';
import ShopGoodsList from './component/shop-goods-list';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class ShopEdit extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {}

  componentWillUnmount() {
    // console.log('--------组件被销毁-------');
    // this.store.save();

  }

  render() {
    const isLarge = this.store.state().get('isLarge');
    return (
      <div className="content shop">
        <div className="shop-tip-box scroll-fixed-box">
          <Tips
            styles="hint-box"
            iconName="icon-jinggao"
            text="长按商品并拖动排序"
          />
          <div className="list-tab" onClick={() => this.store.setIsLarge()}>
            <i
              className={'iconfont ' + (isLarge ? 'icon-fenlei' : 'icon-datu')}
            />
            <span>{isLarge ? '列表' : '大图'}</span>
          </div>
        </div>
        <ShopGoodsList />
      </div>
    );
  }
}
