import React from 'react';
import { config } from 'config';
import { Alert } from 'wmkit';
const delImg = require('../img/good-item-del.png');

/**
 *
 */
export default class ShopGoodsItemSmall extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const { index,handleDelete } = this.props;
    const goodsItem = this.state.goodsItem;
    return goodsItem? (
      <div className="small-goods-box b-1px invalid">
        <img
          src={goodsItem.getIn(['goodsInfo', 'goodsInfoImg'])}
          style={{ width: '100%', height: '100%', opacity: 1 }}
        />
        <div className="small-goods-del" onClick={() => handleDelete(index,goodsItem.getIn(['goodsInfo', 'goodsInfoId']))}>
          <img src={delImg} alt="" />
        </div>
        {/*<div className="small-goods-mask">*/}
          {/*<span>失效</span>*/}
        {/*</div>*/}
      </div>
    ):
      null;
  }

  // _del = async (goodsInfoId) => {
  //   const { handleDelete } = this.props;
  //   const res = await handleDelete(goodsInfoId);
  //   if (res == config.SUCCESS_CODE){
  //     this.setState({'goodsItem':''},()=>{
  //       Alert({ text: '删除成功' });
  //     });
  //   } else {
  //     Alert({ text: res });
  //   }
  // };
}
