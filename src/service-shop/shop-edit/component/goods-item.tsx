import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMImage, _, Alert } from 'wmkit';
import { GoodsNum, MarketingLabel } from 'biz';
import { config } from 'config';

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const { index, handleDelete } = this.props;
    const goodsItem = this.state.goodsItem;

    //是否失效
    const invalid = false;
    // 社交电商相关内容显示与否
    const social = true;
    //禁用分享赚
    const socialDisabled = false;

    let goodsInfo;
    if (goodsItem) {
      // sku信息
      goodsInfo = goodsItem.get('goodsInfo');
    }

    var data = {
      // 要发送到七鱼的商品或者订单的数据对象
      picture: goodsInfo.get('goodsInfoImg'),
      title: goodsInfo.get('goodsInfoName'),
      price: _.addZero(goodsInfo.get('marketPrice')),
      url: `https://m.kmdiary.com/goods-detail/ + ${goodsItem.get('id')}`,
      showCustomMsg: 1,
    }
    return goodsItem ? (
      <div
        className="goods-list"
        // onClick={() => history.push('/goods-detail/' + 'id')}
        onClick={() => window.parent.postMessage(data, '*')}
      >
        <div className="img-box">
          <WMImage src={goodsInfo.get('goodsInfoImg')} width="100%" height="100%" />
        </div>
        <div className="detail b-1px-b">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>


          <div className="marketing">
            {goodsInfo.get('companyType') == 0 && (
              <div className="self-sales">自营</div>
            )}
            {/*{(marketingLabels || couponLabels) && (*/}
            {/*<MarketingLabel*/}
            {/*marketingLabels={marketingLabels}*/}
            {/*couponLabels={couponLabels}*/}
            {/*/>*/}
            {/*)} */}
          </div>

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {/* {this._calShowPrice(goodsItem, buyCount)} */}
                {_.addZero(goodsInfo.get('marketPrice'))}
              </span>
              {social && (
                <span
                  className={
                    invalid ? 'commission commission-disabled' : 'commission'
                  }
                >
                  /&nbsp;赚{_.addZero(goodsInfo.get('distributionCommission'))}
                </span>
              )}
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            {social ? (
              !invalid && (
                <div className="social-btn-box">
                  <div
                    className={
                      socialDisabled
                        ? 'social-btn social-btn-disabled'
                        : 'social-btn'
                    }
                    onClick={(e) => {
                      handleDelete(index, goodsInfo.get('goodsInfoId'));
                      e.stopPropagation();
                    }}
                  >
                    删除
                  </div>
                </div>
              )
            ) : (
                <GoodsNum
                  value={1}
                  max={3}
                  disableNumberInput={invalid}
                  goodsInfoId={'id'}
                  onAfterClick={this._afterChangeNum}
                />
              )}
          </div>
        </div>
      </div>
    ) : null;
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
  };

  // _del = async (goodsInfoId) => {
  //   const { handleDelete } = this.props;
  //   const res = await handleDelete(goodsInfoId);
  //   if (res == config.SUCCESS_CODE){
  //     this.setState({'goodsItem':''},()=>{
  //       Alert({ text: '删除成功' });
  //     });
  //   } else {
  //     Alert({ text: res });
  //   }
  // };
}
