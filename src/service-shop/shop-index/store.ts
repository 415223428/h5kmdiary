import { Store, IOptions } from 'plume2';
import { fromJS } from 'immutable';

import ShopIndexActor from './actor/shop-index-actor';
import * as webapi from './webapi';
import { config, cache } from 'config';
import {Alert, Confirm, history} from 'wmkit';

export default class AppStore extends Store {
  bindActor() {
    return [new ShopIndexActor()];
  }

  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  init = async () => {
    const res = await Promise.all([
      webapi.queryDistributeInfo(),
      webapi.queryDistributeSetting()
    ]);
    if (
      res[0].code == config.SUCCESS_CODE &&
      res[1].code == config.SUCCESS_CODE
    ) {
      this.dispatch(
        'InvitActor:baseInfo',{customerInfo: fromJS(res[0].context).get('distributionCustomerVO'),
          settingInfo:  fromJS(res[1].context).get('distributionSettingSimVO')}
      );
    }
  };

  /**
   * 邀请好友弹层
   */
  changeInvitState = async () => {
    this.dispatch('InvitActor:changeInvitState');
  };

  changeShareVisible = () => {
    this.dispatch('InvitActor:changeShareVisible');
  };

  saveCheckedSku = (sku) => {
    this.dispatch('InvitActor:saveCheckedSku', sku);
  };

  fetchDistributionSetting = async () => {
    let inviteeId =
      JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '';
    if (inviteeId) {
      const { code, context, message } = await webapi.fetchDistributionSetting(
        inviteeId
      );
      //存储分享店铺的图片
      if (code == config.SUCCESS_CODE) {
        this.dispatch(
          'InvitActor:shopShareImg',
          (context as any).distributionSettingSimVO.shopShareImg
        );
      }
    }
  };
}
