import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
@Relax
export default class InvitHead extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeInvitState: Function;
    };
  };

  static relaxProps = {
    changeInvitState: noop
  };

  render() {
    const { changeInvitState } = this.props.relaxProps;
    return (
      <div className="pop-div">
        <div className="store-informat">
          <img className="heard-pointer" src={require('../img/s2b.jpg')} />
          <div className="store-container">
            <span className="store-name">XXXXXX的店铺</span>
            <span className="store-daren">达人店主</span>
          </div>
        </div>
        <div className="close" onClick={() => changeInvitState()}>
          <i className="iconfont icon-Close f-size" />
        </div>
        <div className="div-box">
          <div className="invit-img" />
          <span className="invit-title">我的店铺 欢迎浏览</span>
          <span className="invit-text1">店铺所有商品特价</span>
          <img className="invit-icon" src={require('../img/s2b.jpg')} />
          <span className="invit-text2">长按立即购买</span>
        </div>
        <ul className="bottom-ul">
          <li>
            <div className="wecat-icon" />
            <span>微信好友</span>
          </li>
          <li>
            <div className="circle-icon" />
            <span>朋友圈</span>
          </li>
          <li>
            <div className="img-icon" />
            <span>保存到相册</span>
          </li>
        </ul>
      </div>
    );
  }
}
