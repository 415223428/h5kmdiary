import React from 'react';
import { IMap, Relax } from 'plume2';
import { history } from 'wmkit';
const shopHeadBg = require('../img/shop-head-bg.png');
const defaultImg = require('../../../../web_modules/images/default-img.png');

/**
 *
 */
@Relax
export default class ShopHead extends React.Component<any, any> {
  props: {
    relaxProps?: {
      customerInfo: IMap;
      settingInfo: IMap;
    };
  };

  static relaxProps = {
    customerInfo: 'customerInfo',
    settingInfo: 'settingInfo'
  };

  render() {
    const { customerInfo, settingInfo } = this.props.relaxProps;
    let headImg = customerInfo.get('headImg')
      ? customerInfo.get('headImg')
      : defaultImg;
    return (
      <div
        className="shop-head scroll-fixed-box"
        style={{
          background: 'url(' + shopHeadBg + ') 0 0 no-repeat',
          backgroundSize: '100% 100%'
        }}
      >
        <div>
          <i
            className="iconfont icon-jiantou"
            style={{ color: 'transparent' }}
          />
        </div>
        <div className="shop-head-bottom">
          <div className="shop-head-info">
            <div className="shop-head-info-img">
              <img src={headImg} alt="" />
            </div>
            <div>
              <p className="shop-head-info-name">
                {customerInfo && customerInfo.get('customerName')}的{settingInfo &&
                  settingInfo.get('shopName')}
              </p>
              <span className="shop-head-info-tag">
                {customerInfo && customerInfo.get('distributorLevelName')}
              </span>
            </div>
          </div>
          <div className="shop-head-btns">
            <a
              className="shop-head-btn"
              href="javascript:;"
              onClick={() => history.push('/shop-edit')}
            >
              <i className="iconfont icon-bianji" />
              <span>编辑</span>
            </a>
            <a
              className="shop-head-btn"
              href="javascript:;"
              onClick={() => history.push('/shop-goods')}
            >
              <i className="iconfont icon-xuanpin" />
              <span>选品</span>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
