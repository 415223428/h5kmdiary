import React from 'react';
import { msg } from 'plume2';
import { Link } from 'react-router-dom';
import { history, WMImage, _, WMkit, Alert } from 'wmkit';
import { GoodsNum } from 'biz';
import { cache } from 'config';

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    let { changeShareVisible, goodsItem } = this.props;
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    //是否失效
    const stock = goodsInfo.get('stock');
    const count = goodsInfo.get('count') || 0;
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const inviteeId =
      JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '';
    // 社交电商相关内容显示与否
    const social = true;
    //禁用分享赚
    const socialDisabled = false;

    var data = {
      // 要发送到七鱼的商品或者订单的数据对象
      picture: goodsInfo.get('goodsInfoImg'),
      title: goodsInfo.get('goodsInfoName'),
      price: _.addZero(goodsInfo.get('marketPrice')),
      url: `https://m.kmdiary.com/goods-detail/ + ${goodsItem.get('id')}`,
      showCustomMsg: 1,
    }
    return (
      <div
        className={`goods-list ${invalid ? 'invalid-goods' : ''}`}
        // onClick={() =>
        //   this.goToGoodsDetail(goodsInfo.get('goodsId'), goodsInfo.get('goodsInfoId'))
        // }
        onClick={() => window.parent.postMessage(data, '*')}
      >
        <div className="img-box">
          <WMImage
            src={goodsInfo.get('goodsInfoImg')}
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail b-1px-b">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>

          <div className="marketing">
            {goodsInfo.get('companyType') == 0 && (
              <div className="self-sales">自营</div>
            )}
            {/*{(marketingLabels || couponLabels) && (*/}
            {/*<MarketingLabel*/}
            {/*marketingLabels={marketingLabels}*/}
            {/*couponLabels={couponLabels}*/}
            {/*/>*/}
            {/*)}*/}
          </div>

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {/* {this._calShowPrice(goodsItem, buyCount)} */}
                {_.addZero(goodsInfo.get('marketPrice'))}
              </span>
              {social && (
                <span
                  className={
                    invalid ? 'commission commission-disabled' : 'commission'
                  }
                >
                  /&nbsp;赚{_.addZero(goodsInfo.get('distributionCommission'))}
                </span>
              )}
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            {social ? (
              !invalid && (
                <div className="social-btn-box">
                  <div
                    className="social-btn social-btn-ghost"
                    onClick={async (e) => {
                      e.stopPropagation();
                      const result = await WMkit.getDistributorStatus();
                      if ((result.context as any).distributionEnable) {
                        history.push(
                          `/graphic-material/${goodsInfo.get('goodsInfoId')}`
                        );
                      } else {
                        let reason = (result.context as any).forbiddenReason;
                        msg.emit('bStoreCloseVisible', {
                          visible: true,
                          reason: reason
                        });
                      }
                    }}
                  >
                    发圈素材
                  </div>
                  <div
                    className={
                      socialDisabled
                        ? 'social-btn social-btn-disabled'
                        : 'social-btn'
                    }
                    onClick={async (e) => {
                      e.stopPropagation();
                      const result = await WMkit.getDistributorStatus();
                      if ((result.context as any).distributionEnable) {
                        if (!socialDisabled && (window as any).isMiniProgram) {
                          changeShareVisible();
                          this.props.saveSku(goodsInfo);
                        } else {
                          Alert({
                            text: '请到小程序端进行该操作!'
                          });
                        }
                      } else {
                        let reason = (result.context as any).forbiddenReason;
                        msg.emit('bStoreCloseVisible', {
                          visible: true,
                          reason: reason
                        });
                      }
                    }}
                  >
                    分享赚
                  </div>
                </div>
              )
            ) : (
                <GoodsNum
                  value={1}
                  max={3}
                  disableNumberInput={invalid}
                  goodsInfoId={'id'}
                  onAfterClick={this._afterChangeNum}
                />
              )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  goToGoodsDetail = (goodsId, goodsInfoId) => {
    //小程序打开，去邀请
    if ((window as any).isMiniProgram) {
      history.push(
        `/shop-index/goods-detail/0/${goodsId}/${goodsInfoId}`
      )
    } else {
      Alert({
        text: '请到小程序端进行该操作!'
      });
    }
  }

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
  };
}
