import React from 'react';
import { Relax } from 'plume2';
import GoodsItem from './goods-item';
import { Blank, ListView, noop } from 'wmkit';
import { fromJS } from 'immutable';

const noneImg = require('../img/list-none.png');

/**
 *
 */
@Relax
export default class ShopGoodsList extends React.Component<any, any> {
  _listView: any;
  props: {
    relaxProps?: {
      changeShareVisible: Function;
      saveCheckedSku: Function;
    };
  };

  static relaxProps = {
    changeShareVisible: noop,
    saveCheckedSku: noop
  };

  render() {
    let { changeShareVisible, saveCheckedSku } = this.props.relaxProps;
    return (
      <ListView
        url={'/goods/shop/sku-list'}
        style={{ height: 'calc(100vh - 88px)' }}
        dataPropsName={'context.esGoodsInfoPage.content'}
        otherProps={['goodsIntervalPrices']}
        renderRow={(item: any) => {
          return (
            <GoodsItem
              saveSku={(sku) => saveCheckedSku(sku)}
              goodsItem={fromJS(item)}
              changeShareVisible={() => changeShareVisible()}
              listView={true}
            />
          );
        }}
        renderEmpty={() => (
          <Blank img={noneImg} content="店主比较懒，还没有添加商品哦~" />
        )}
        ref={(_listView) => (this._listView = _listView)}
      />
    );
  }
}
