import { IOptions, Store, IMap, msg } from 'plume2';
import { fromJS } from 'immutable';
import { config } from 'config';
import { Alert, history } from 'wmkit';

import * as webapi from './webapi';
import GoodsActor from './actor/goods-actor';
import GoodsTabActor from './actor/goods-tab-actor';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new GoodsActor(), new GoodsTabActor()];
  }

  /**
   * 初始化数据
   */
  init = async (cateId, cateName, queryString) => {
    // const { code, context } = await webapi.queryGoodsShowType();
    // if (code == config.SUCCESS_CODE) {
    // goodsShowType(0:sku列表,1:spu列表)  imageShowType(0:小图,1:大图)
    // const { goodsShowType, imageShowType } = context as any;
    let params = {};
    let props = [];
    if (cateId) {
      params['selectedCate'] = { cateId, cateName };
      props = await this._getProps(cateId);
    }
    if (queryString) {
      params['queryString'] = queryString;
    }

    this.transaction(() => {
      // this.dispatch('goodsActor:changeGoodsShowType', goodsShowType); // sku/spu视图模式
      // this.dispatch('goodsActor:changeImageShowType', imageShowType == 0); // 小图/大图模式
      this.dispatch('goods-list:props', fromJS(props));
      this.dispatch('goods-list:searchParams', fromJS(params));
      this.dispatch('goods-list:initialEnd');
      if (cateName != null && cateName != undefined && cateName != '') {
        this.dispatch('goods-list:setCateId', { cateId, cateName });
      }
    });
    // }
  };

  /**
   * 根据分类id查询属性与属性值
   */
  _getProps = async (cateId) => {
    // 查询该分类的属性信息
    const propsRes = (await webapi.queryProps(cateId)) as any;
    if (propsRes.code == config.SUCCESS_CODE) {
      propsRes.context.forEach((prop) => {
        (prop.goodsPropDetails as Array<any>).push({
          detailId: 0,
          propId: prop.propId,
          detailName: '其他'
        });
      });
      return propsRes.context;
    }
    return [];
  };

  /**
   * 显示遮罩
   */
  openShade = (tabName) => {
    this.dispatch('goods-list:openShade', tabName);
  };

  /**
   * 隐藏遮罩
   */
  closeShade = () => {
    this.dispatch('goods-list:closeShade');
  };

  /**
   * 设置选中的类目信息
   */
  setCateId = async (cateId, cateName) => {
    const props = await this._getProps(cateId);

    this.transaction(() => {
      this.dispatch('goods-list:props', fromJS(props));
      this.dispatch(
        'goods-list:searchParams',
        fromJS({ selectedCate: { cateId, cateName }, queryString: '' })
      );
      this.dispatch('goods-list:clearFilters', {});
      // 设置筛选项状态为待重绘
      this.dispatch('goods-list:drawFilter', true);
      this.closeShade();
    });
  };

  /**
   * 设置排序
   */
  setSort = (type) => {
    let newType = type;
    let newSort = '';
    const sortType = this.state().get('sortType');

    // 是否切换排序类型？
    if (newType !== sortType.get('type')) {
      if (newType === 'default') {
        newSort = '';
      } else if (newType === 'dateTime') {
        newSort = 'desc';
      } else if (newType === 'price') {
        newSort = 'asc';
      }
    } else if (newType !== 'default') {
      // 同一种排序类型，切换升降顺序，默认排序无顺序

      if (sortType.get('sort') === 'asc') {
        newSort = 'desc';
      } else if (sortType.get('sort') === 'desc') {
        newSort = 'asc';
      }
    }

    this.dispatch('goods-list:setSort', { type: newType, sort: newSort });
    this.closeShade();
  };

  /**
   * 切换商品列表视图模式
   */
  changeLayout = () => {
    this.dispatch('goodsActor:changeLayout');
  };

  /**
   * 切换批发类型-规格选择框显示与否
   */
  changeWholesaleVisible = (value) => {
    this.dispatch('goodsActor:changeWholesaleVisible', value);
  };

  /**
   * 切换零售类型-规格选择框显示与否
   */
  changeRetailVisible = (value) => {
    this.dispatch('goodsActor:changeRetailVisible', value);
  };

  /**
   * 切换分享赚弹框显示与否
   */
  changeShareVisible = () => {
    this.dispatch('goodsActor:changeShareVisible', true);
  };

  /**
   * 根据skuId查询spu相关信息,并打开对应销售类型的规格选择弹框
   * @param skuId
   */
  querySpuAndOpenSpec = async (skuId) => {
    // 若再次打开的spu与之前不一致,则重新查询后打开弹框
    if (skuId != this.state().get('chosenSpu').skuId) {
      const { code, context, message } = (await webapi.querySpu(skuId)) as any;
      if (code == config.SUCCESS_CODE) {
        let goodsInfos = context.goodsInfos;
        if (goodsInfos && goodsInfos.length > 0) {
          context.skuId = skuId;
          this.transaction(() => {
            this.dispatch('goodsActor:setSpuContext', context);
            if (context.goods.saleType == 0) {
              this.changeWholesaleVisible(true);
            } else {
              this.changeRetailVisible(true);
            }
          });
        } else {
          Alert({ text: '该商品已失效,请刷新列表' });
        }
      } else {
        Alert({ text: message });
      }
    } else {
      // 若打开的spu与之前一致,直接打开弹框
      if (this.state().get('chosenSpu').goods.saleType == 0) {
        this.changeWholesaleVisible(true);
      } else {
        this.changeRetailVisible(true);
      }
    }
  };

  /**
   * 商品列表ListView查询得到的数据返回处理
   * @param data
   */
  handleDataReached = (data: any) => {
    if (data.code !== config.SUCCESS_CODE) {
      return;
    }

    const { goodsList } = data.context;

    this.dispatch('goods-list:isNotEmpty', !!goodsList);

    // 是否需要重绘筛选项
    if (this.state().get('drawFilter')) {
      // 设置状态为已重绘
      this.dispatch('goods-list:drawFilter', false);

      // 搜索结果对应的品牌聚合结果
      let { brands } = data.context;

      // 展示品牌筛选条件
      this.dispatch('goods-list:initFilter', fromJS({ brands }));
    }
  };

  // 品牌和属性筛选条件选中变化
  handleFilterChange = (
    selectSelfCompany,
    selectedItem,
    selectedBrandIds,
    goodsProps,
    brandExpanded: boolean,
    expandProp: IMap
  ) => {
    this.dispatch('goods-list:filterChange', {
      selectSelfCompany,
      selectedItem,
      selectedBrandIds,
      goodsProps,
      brandExpanded,
      expandProp
    });
  };

  //分销员添加分销商品
  addCommodityDistribution = async (goodsId: String, goodsInfoId: String, storeId: String) => {
    const { code, context, message } = await webapi.addCommodityDistribution(
      goodsId,
      goodsInfoId,
      storeId
    );
    if (code == config.SUCCESS_CODE) {
      Alert({ text: '添加成功' });
      return true;
    } else {
      Alert({ text: message });
      return false;
    }
  };

  //查询店铺已经添加的分销商品数
  getCountsByCustomerId = async (storeId: String) =>{
    const { code, message } = await webapi.getCountsByCustomerId(storeId) as any;
    if (code == config.SUCCESS_CODE) {
      return true;
    } else {
      Alert({ text: message });
      return false;
    }
  };

  //删除分销商品
  delCommodityDistribution = async (goodsInfoId: String) => {
    const { code, context, message } = await webapi.delCommodityDistribution(
      goodsInfoId
    ) ;
    if (code == config.SUCCESS_CODE) {
      Alert({ text: '删除成功' });
      return true;
    } else {
      Alert({ text: message });
      return false;
    }
  };
}
