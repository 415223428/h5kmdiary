import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class GoodsTabSort extends Component<any, any> {
  props: {
    relaxProps?: {
      closeShade: Function;
      setSort: Function;
      sortType: IMap;
    };
  };

  static relaxProps = {
    closeShade: noop,
    setSort: noop,
    sortType: 'sortType'
  };

  render() {
    const { closeShade, setSort, sortType } = this.props.relaxProps;

    // 排序字段
    const type = sortType.get('type');
    // 排序方式：升序 降序
    const sort = sortType.get('sort');

    return (
      <div style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
        <div style={{ backgroundColor: '#ffffff', width: '100%' }}>
          <div
            className={
              type == 'default' || type == ''
                ? 'sort-item new-theme-text'
                : 'sort-item'
            }
            onClick={() => setSort('default')}
          >
            默认
          </div>
          <div className="sort-item" onClick={() => setSort('dateTime')}>
            <div className={type == 'dateTime' ? 'new-theme-text' : ''}>
              最新
            </div>
            <div
              className={
                type == 'dateTime' && sort == 'asc'
                  ? 'sort-icon-box new-theme-text'
                  : 'sort-icon-box'
              }
            >
              <i className="iconfont icon-xiangshangpai" />
            </div>
            <div
              className={
                type == 'dateTime' && sort == 'desc' ? 'new-theme-text' : ''
              }
            >
              <i className="iconfont icon-xiangxiapai" />
            </div>
          </div>
          {/*<div className="sort-item" onClick={() => setSort('price')}>*/}
          {/*<div style={type == 'price' ? {color: '#3d85cc'} : {}}>价格</div>*/}
          {/*<div style={type == 'price' && sort == 'asc' ? {color: '#3d85cc'} : {}}>*/}
          {/*<i className="iconfont icon-xiangshangpai"></i>*/}
          {/*</div>*/}
          {/*<div style={type == 'price' && sort == 'desc' ? {color: '#3d85cc'} : {}}>*/}
          {/*<i className="iconfont icon-xiangxiapai"></i>*/}
          {/*</div>*/}
          {/*</div>*/}
        </div>

        <div
          style={{ width: '100%', height: '100vh' }}
          onClick={() => closeShade()}
        />
      </div>
    );
  }
}
