import * as React from 'react';
import { Relax } from 'plume2';
import { fromJS } from 'immutable';
import { ListView, Blank } from 'wmkit';
import { div } from 'wmkit/common/util';
import { GrouponListItem } from 'biz';

@Relax
export default class SelectList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      toRefresh: boolean;
      //请求参数
      keyWords: string;
    };
  };

  static relaxProps = {
    toRefresh: 'toRefresh',
    //请求参数
    keyWords: 'keyWords'
  };

  render() {
    const { keyWords, toRefresh } = this.props.relaxProps;
    // 组件刚执行完mount，搜索条件没有注入进来，先不加载ListView，避免先进行一次无条件搜索，再立刻进行一次有条件搜索
    if (!toRefresh) {
      return null;
    }

    return (
      <div>
        <ListView
          url="/groupon/center/list"
          params={{
            goodsName: keyWords
          }}
          isPagination={true}
          renderRow={(item) => {
            return (
              <GrouponListItem
                goodInfo={fromJS(item)}
                key={fromJS(item).get('goodsId')}
              />
            );
          }}
          renderEmpty={() => (
            <Blank
              img={require('../img/empty.png')}
              content="啊哦，暂时没有拼团活动~"
            />
          )}
        />
      </div>
    );
  }
}
