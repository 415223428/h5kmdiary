import { Action, Actor, IMap } from 'plume2';

export default class GrouponCenterActor extends Actor {
  defaultState() {
    return {
      // 关键字搜索
      keyWords: '',

      //放入state,为了分页组件局部刷新
      toRefresh: false
    };
  }

  /**
   * 数据变化
   * @param state
   * @param context
   * @returns {*}
   */
  @Action('group: field: change')
  initList(state: IMap, { field, value }) {
    return state.set(field, value);
  }
}
