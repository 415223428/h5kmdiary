/**
 * Created by chenpeng on 2017/8/3.
 */
import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class PayOnlineActor extends Actor {
  defaultState() {
    return {
      trade: {
        tradePrice: '', //订单金额
        tradeNo: '', //订单号
        orderTimeOut: '' //订单过期时间
      },
      payItems: [], //支付方式
      payOrder: {}, //付款记录
      wxPayMwebUrl: '', //微信支付h5支付，支付福跳转链接
      visible: false, //支付密码弹框展示
      payPwd: '', //支付密码
      checkPayPwdRes: true,
      payPwdTime: 0,
      channelItemId: 0, //支付渠道
      balance: 0 // 余额
    };
  }

  /**
   * 设置订单金额和订单号
   */
  @Action('pay-online-actor:setTrade')
  setTradePrice(state: IMap, trade: any) {
    return state.set('trade', fromJS(trade));
  }

  /**
   * 设置支付方式
   */
  @Action('pay-online-actor:setPayItems')
  setPayItems(state: IMap, payItems: any) {
    return state.set('payItems', fromJS(payItems));
  }

  /**
   * 设置用户可用余额
   */
  @Action('balance-pay:setBalance')
  setBalance(state, val) {
    return state.set('balance', val);
  }

  /**
   * 设置付款记录
   */
  @Action('pay-online-actor:setPayOrder')
  setPayOrder(state: IMap, payOrder: any) {
    return state.set('payOrder', fromJS(payOrder));
  }

  /**
   * 微信支付h5支付支付跳转链接
   * @param state
   * @param payOrder
   */
  @Action('pay-online-actor:setWXPayMwebUrl')
  setWXPayMwebUrl(state: IMap, wxPayMwebUrl: string) {
    return state.set('wxPayMwebUrl', wxPayMwebUrl);
  }

  /**
   * 支付密码输入框是否显示
   */
  @Action('balance-pay:showPassword')
  showPassword(state, val) {
    return state.set('visible', val);
  }

  /**
   * 支付密码改变
   */
  @Action('balance-pay:changePayPwd')
  changePayPwd(state, val) {
    return state.set('payPwd', val);
  }

  /**
   * 校验密码结果
   */
  @Action('balance-pay:checkPayPwdRes')
  checkPayPwdRes(state, checkPayPwdRes) {
    return state.set('checkPayPwdRes', checkPayPwdRes);
  }

  /**
   * 密码错误次数
   */
  @Action('balance-pay:countPayPwdTime')
  countPayPwdTime(state, payPwdTime) {
    return state.set('payPwdTime', payPwdTime);
  }

  /**
   * 支付渠道
   */
  @Action('balance-pay:setChannelItemId')
  setChannelItemId(state, channelItemId) {
    return state.set('channelItemId', channelItemId);
  }
}
