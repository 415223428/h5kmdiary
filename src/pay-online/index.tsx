import React, { Component } from 'react';
import { StoreProvider } from 'plume2';

import { history, Confirm } from 'wmkit';

import PayType from './component/pay-type';
import AppStore from './store';
import PaymentPassword from './component/payment-password';

const style = require('./css/style.css');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class PayOnline extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    if (history.action == 'POP') {
      Confirm({
        title: '请您在新打开的网页上完成付款',
        text:
          '付款完成前请不要关闭此窗口完成付款后请根据您的情况点击下面的按钮',
        okBtn: '已完成付款',
        confirmCb: () => {
          history.push('/order-list');
        },
        cancelBtn: '更换支付方式'
      });
    }

    const { tid } = this.props.match.params;
    const state = this.props.location.state;
    this.store.init(tid, state && state.results);
  }

  render() {
    return (
      <div>
        <PayType />
        {/* 输入支付密码 */}
        {this.store.state().get('visible') && <PaymentPassword />}
      </div>
    );
  }
}
