/**
 * Created by chenpeng on 2017/8/3.
 */
import { Fetch } from 'wmkit';

export const fetchOrder = (tid: string) => {
  return Fetch<Result<any>>(`/trade/show/${tid}`);
};

/**
 * 获取支付对象
 */
export const fetchCharge = (payRequest: any) => {
  return Fetch('/pay/create', {
    method: 'POST',
    body: JSON.stringify(payRequest)
  });
};

/**
 * 获取可用支付项
 */
export const fetchPayItems = () => {
  return Fetch<Result<any>>('/pay/items/H5');
};

/**
 * 获取用户余额
 */
export const fetchBalance = () => {
  return Fetch('/customer/funds/statistics');
}

/**
 * 订单付款记录
 */
export const fetchPayOrder = (tid: string) => {
  return Fetch(`/trade/payOrder/${tid}`);
};

/**
 * 批量订单付款记录
 */
export const fetchPayOrders = (parentTid: string) => {
  return Fetch(`/trade/payOrders/${parentTid}`);
};

/**
 * 查询会员的财务邮箱信息列表
 */
export const fetchCustomerEmailList = () => {
  return Fetch<Result<any>>('/customer/emailList/', {
    method: 'GET'
  });
};

/**
 * 发送邮件
 */
export const doSendEmails = (tradeId: string) => {
  return Fetch(`/customer/email/sendToFinance/${tradeId}`, {
    method: 'POST'
  });
};

/**
 * 微信支付h5支付获取支付跳转链接
 * @param params
 */
export const getWXPayMwebUrl = (params) => {
  return Fetch<Result<any>>('/pay/wxPayUnifiedorderForMweb', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 微信浏览器内JSApi支付,获取prepay_id
 * @param params
 */
export const getWXPayJSApi = (params) => {
  return Fetch<Result<any>>('/pay/wxPayUnifiedorderForJSApi', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 支付宝支付前支付状态校验
 * @param params
 */
export const getPayState = (tid) => {
  // 没有tid的情况直接不做校验
  if (!tid) return Promise.resolve({ context: 0 });
  return Fetch(`/pay/aliPay/check/${tid}`);
};

/**
 * 验证拼团订单是否可支付
 * @param tid
 * @returns {Promise<Result<any>>}
 */
export const checkGrouponOrder = (tid) => {
  // 没有tid的情况直接不做校验
  if (!tid) return Promise.resolve({ context: { status: 0 } });
  return Fetch(`/groupon/order/check/${tid}`, {
    method: 'POST'
  });
};

/**
 * 校验输入支付密码
 * @param payPassword
 */
export const checkCustomerPayPwd = (payPassword) => {
  return Fetch<Result<any>>('/checkCustomerPayPwd', {
    method: 'POST',
    body: JSON.stringify({ payPassword: payPassword })
  });
};

/**
 * 获取当前登陆人信息
 */
export const getLoginCustomerInfo = () => {
  return Fetch<Result<any>>('/customer/getLoginCustomerInfo', {
    method: 'GET'
  });
};

/**
 * 校验会员支付密码是否可用
 */
export const isPayPwdValid = () => {
  return Fetch<Result<any>>('/isPayPwdValid', {
    method: 'POST'
  });
};

/**
 * 余额支付
 */
export const balancePay = (payMobileRequest) => {
  return Fetch<Result<any>>('/pay/balancePay', {
    method: 'POST',
    body: JSON.stringify(payMobileRequest)
  });
};

/**
  *
 * */
export const getPrepayId = (payOrder, payPrice) => {
  return Fetch<Result<any>>('/system/advertisingConfig', {
    method: 'POST',
    body: JSON.stringify({
      totalFee: payPrice * 100,
      outTradeNo: payOrder
    })
  });
};
