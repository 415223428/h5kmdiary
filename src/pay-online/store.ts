/**
 * Created by chenpeng on 2017/8/3.
 */
import { IOptions, Store } from 'plume2';
import { config } from 'config';
import { _, Alert, Confirm, history, WMkit } from 'wmkit';
import * as webapi from './webapi';

import {
  balancePay,
  checkGrouponOrder,
  doSendEmails,
  fetchBalance,
  fetchCharge,
  fetchCustomerEmailList,
  fetchOrder,
  fetchPayItems,
  fetchPayOrder,
  fetchPayOrders,
  getPayState,
  getWXPayJSApi,
  getWXPayMwebUrl
} from './webapi';
import PayOnlineActor from './actor/pay-online-actor';
import * as webApi from '../points-mall/webapi';
import { init } from '../balance/home/webapi';

declare const WeixinJSBridge: any;

const zhi = require('./img/zhi.png');
const wechat = require('./img/wechat.png');
const unipay = require('./img/unipay.png');
const balance = require('./img/balance.png');

/**
 *
 * @type {List<[{label: '内容'; channel: ''; imgSrc: '图片'}]>}
 */
const payData = [
  { label: '支付宝', code: 'alipay_wap', imgSrc: zhi },
  { label: '微信支付', code: 'wx_pub', imgSrc: wechat },
  { label: '银联支付', code: 'upacp_wap', imgSrc: unipay },
  { label: '企业银联支付', code: 'unionpay_b2b_wx', imgSrc: unipay },
  { label: '支付宝 ', code: 'alipay_h5', imgSrc: zhi },
  // 非微信浏览器h5支付
  { label: '微信支付', code: 'wx_mweb', imgSrc: wechat },
  // 微信浏览器jsApi支付
  { label: '微信支付', code: 'js_api', imgSrc: wechat },
  { label: '余额支付', code: 'balance_h5', imgSrc: balance }
];

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new PayOnlineActor()];
  }

  /**
   * 初始化订单支付成功
   */
  initSuccess = async (tid: string, parentTid) => {
    let resPromise;
    if (!(tid === 'undefined' || tid === 'null')) {
      resPromise = fetchPayOrder(tid);
    } else {
      resPromise = fetchPayOrders(parentTid);
    }
    let { code, context, message } = (await resPromise) as any;
    if (code === config.SUCCESS_CODE) {
      let payOrders;
      if (context.payOrders instanceof Array) {
        payOrders = context.payOrders;
      } else {
        payOrders = [context];
      }
      this.dispatch('pay-online-actor:setPayOrder', payOrders);
    } else {
      Alert({ text: message });
    }
  };

  /**
   * 初始化订单支付
   */
  init = async (tid: string, results) => {
    // 设置支付单的价格、超时时间、母订单id
    let price, orderTimeOut, parentTid;
    if (tid) {
      // 通过订单列表进来的单笔订单支付
      const { code, context, message } = (await fetchOrder(tid)) as any;
      if (code === config.SUCCESS_CODE) {
        price = context.price;
        orderTimeOut = context.orderTimeOut;
      } else {
        Alert({ text: message });
      }
    } else {
      // 提交订单合并支付
      parentTid = results[0].parentTid;
      orderTimeOut = results[0].orderTimeOut;
      price = results.reduce((a, b) => a + b.price, 0);
    }

    //获取支付方式
    const payInfo = await fetchPayItems();
    if (__DEV__) {
      console.log('....', payInfo);
    }

    //zip
    let payItems = payInfo.context;
    // 获取用户余额
    const balanceData = (await fetchBalance()) as any;
    this.dispatch(
      'balance-pay:setBalance',
      _.addZero(balanceData.context.withdrawAmountTotal)
    );
    payItems = (payItems as any)
      .filter((payItem1) => {
        if (_.isWeixin()) {
          // 微信浏览器,展示jsApi支付方式
          if (payItem1.channel == 'WeChat') {
            return payItem1.code == 'js_api';
          } else {
            return payItem1.channel != 'Alipay' && payItem1.channel != 'alipay';
          }
        } else {
          // 非微信浏览器,展示h5支付方式
          if (payItem1.channel == 'WeChat') {
            return payItem1.code == 'wx_mweb';
          } else {
            // 不展示ping++微信支付
            return payItem1.channel != 'WeChat';
          }
        }
      })
      .map((payItem) => {
        let pays = payData.filter((pay) => pay.code == payItem.code);

        if (pays.length > 0) {
          if (
            Number(balanceData.context.withdrawAmountTotal) < Number(price) &&
            payItem.gateway.id == 5
          ) {
            payItem['src'] = require('./img/balance_dark.png');
          } else {
            payItem['src'] = pays[0].imgSrc;
          }
          payItem['label'] = pays[0].label;

          //ping++ 支付
          let gateway = payItem.gateway;
          if (gateway && gateway.type) {
            payItem['sdkType'] = 'pingpp';
          } else {
            payItem['sdkType'] = payItem.code;
          }
        }

        return payItem;
      });

    this.transaction(() => {
      //订单
      this.dispatch('pay-online-actor:setTrade', {
        tradeNo: tid,
        parentTid: parentTid,
        tradePrice: _.addZero(price),
        orderTimeOut: orderTimeOut
      });
      //支付方式
      this.dispatch('pay-online-actor:setPayItems', payItems);
    });
  };

  /**
   * 1.获取支付对象
   */
  getPayCharge = async (channelItemId: string) => {
    const tid = this.state().getIn(['trade', 'tradeNo']);
    const parentTid = this.state().getIn(['trade', 'parentTid']);
    const { openid } = WMkit.wechatOpenId();
    //支付request
    const payRequest = {
      tid: tid,
      parentTid,
      successUrl: window.location.origin + `/pay-success/${tid}/${parentTid}`,
      channelItemId: channelItemId,
      openId: openid
    };

    return await fetchCharge(payRequest);
  };

  /**
   * 2.ping++sdk支付
   */
  pingppSdk = (charge: Promise<any>, tid: string) => {
    const parentTid = this.state().getIn(['trade', 'parentTid']);
    charge
      .then((val) => {
        if (val.code === config.SUCCESS_CODE) {
          System.import('pingpp-js').then((pingpp) => {
            pingpp.createPayment(val.context, function (result, err) {
              if (result == 'success') {
                // 只有微信公众账号 wx_pub 支付成功的结果会在这里返回，其他的支付结果都会跳转到 extra 中对应的 URL。
                history.push(`/pay-success/${tid}/${parentTid}`);
              } else if (result == 'fail') {
                // charge 不正确或者微信公众账号支付失败时会在此处返回
                Alert({ text: '支付失败', time: 1000 });
                /*Confirm({
                  title: "",
                  text: ""
                })*/
                history.push('/order-list');
              } else if (result == 'cancel') {
                // 微信公众账号支付取消支付
                Confirm({
                  title: '请您在新打开的网页上完成付款',
                  text:
                    '付款完成前请不要关闭此窗口完成付款后请根据您的情况点击下面的按钮',
                  okBtn: '已完成付款',
                  confirmCb: () => {
                    history.push('/order-list');
                  },
                  cancelBtn: '更换支付方式'
                });
              }
            });
          });
        } else if (val.code == 'K-050206') {
          Alert({ text: '订单状态已改变，请关闭页面后重试!' });
        } else if (val.code == 'K-100208') {
          history.push(`/pay-success/${tid}/${parentTid}`);
        } else {
          Alert({ text: val.message });
        }
      })
      .catch(() => {
        Alert({ text: '支付失败' });
      });
  };

  /**
   * 1.获取支付对象
   * 2.调用createPayment后跳入第三方支付界面
   */
  toPay = (getPayCharge: Function, paySdk: Function) => {
    return (channelItemId: string, tid: string) => {
      paySdk(getPayCharge(channelItemId), tid);
    };
  };

  /**
   * 给财务部门发送通知邮件
   */
  sendMailToFinance = async () => {
    // 查询当前会员财务邮箱
    const res: any = await fetchCustomerEmailList();
    // 判断财务邮箱数量
    if (res.context.length > 0) {
      // 已有财务邮箱，发送邮件，跳转订单列表
      const tradeId = this.state().getIn(['trade', 'tradeNo']);
      doSendEmails(tradeId);
      history.push('/order-list');
    } else {
      // 无邮箱，提示请维护邮箱，跳转财务邮箱维护页面
      Confirm({
        text: '请维护财务邮箱',
        okBtn: '确定',
        maskClose: false,
        confirmCb: function () {
          history.push('/user-email');
        }
      });
    }
  };

  /**
   * 获取微信支付h5支付统一下单接口的返回支付跳转链接
   */
  getWXPayMwebUrl = async () => {
    const tradeId = this.state().getIn(['trade', 'tradeNo']);
    const parentTid = this.state().getIn(['trade', 'parentTid']);
    const params = { tid: tradeId, parentTid };
    // 0:未支付  1：已支付 2:超时未支付，订单已作废
    const { context } = await getPayState(tradeId);
    if (context == 1) {
      Confirm({
        okBtn: '确定',
        text: '请勿重复提交订单',
        confirmCb: () => {
          history.push('/order-list');
        }
      });
    } else if (context == 2) {
      //订单作废或超时的提示
      Confirm({
        okBtn: '确定',
        text: '支付失败，请重新下单支付',
        confirmCb: () => {
          history.push('/order-list');
        }
      });
    } else {
      if ((window as any).paySuccess !== undefined) {
        (window as any).paySuccess(this.state().get('trade').get('tradePrice'), tradeId ? tradeId : parentTid, tradeId ? 'tradeId' : 'parentTid')
        return;
      }
      const res: any = await getWXPayMwebUrl(params);
      if (res.code == config.SUCCESS_CODE) {
        let context = res.context;

        if (
          context.return_code == 'SUCCESS' &&
          context.result_code == 'SUCCESS'
        ) {
          this.dispatch('pay-online-actor:setWXPayMwebUrl', context.mweb_url);
          console.log(context.mweb_url);

          // console.log(this.state().get('trade').get('tradePrice'));
          // console.log(this.state().get('trade').get('parentTid'));
          // console.log(tradeId);
          setTimeout(() => {
            Confirm({
              okBtn: '确定',
              text: '请勿重复提交订单',
              confirmCb: () => {
                history.push('/main');
              }
            })
          }, 3000);

          window.location.href = context.mweb_url;
        } else {
          Confirm({
            okBtn: '确定',
            text: context.err_code_des,
            confirmCb: () => {
              history.push('/order-list');
            }
          });
        }
      } else {
        Confirm({
          okBtn: '确定',
          text: res.message,
          confirmCb: () => {
            history.push('/order-list');
          }
        });
      }
    }
  };

  /**
   * 获取微信支付h5支付统一下单接口的返回支付跳转链接
   */
  getWXPayJSApi = async () => {
    const tradeId = this.state().getIn(['trade', 'tradeNo']);
    const parentTid = this.state().getIn(['trade', 'parentTid']);
    // 0:未支付  1：已支付
    const { context } = await getPayState(tradeId);
    if (context == 1) {
      Confirm({
        okBtn: '确定',
        text: '请勿重复提交订单',
        confirmCb: () => {
          history.push('/main');
        }
      });
    } else {
      const { openid } = WMkit.wechatOpenId();
      const params = { tid: tradeId, openid, parentTid };
      const res: any = await getWXPayJSApi(params);
      if (res.code == config.SUCCESS_CODE) {
        const context = res.context;
        function onBridgeReady() {
          WeixinJSBridge.invoke('getBrandWCPayRequest', context, function (res) {
            if (res.err_msg == 'get_brand_wcpay_request:ok') {
              // 使用以上方式判断前端返回,微信团队郑重提示：
              //res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
              history.push(`/pay-success/${tradeId}/${parentTid}`);
            } else {
              Alert({ text: '支付失败' });
            }
          });
        }
        if (typeof WeixinJSBridge == 'undefined') {
          if (document.addEventListener) {
            document.addEventListener(
              'WeixinJSBridgeReady',
              onBridgeReady,
              false
            );
          } else if ((document as any).attachEvent) {
            (document as any).attachEvent('WeixinJSBridgeReady', onBridgeReady);
            (document as any).attachEvent(
              'onWeixinJSBridgeReady',
              onBridgeReady
            );
          }
        } else {
          onBridgeReady();
        }
      } else {
        Confirm({
          okBtn: '确定',
          text: res.message,
          confirmCb: () => {
            history.push('/order-list');
          }
        });
      }
    }
  };

  zhifubaoPay = async (channelItemId: string, tid: string) => {
    const parentTid = this.state().getIn(['trade', 'parentTid']);
    const successUrl =
      window.location.origin + `/pay-success/${tid}/${parentTid}`;
    //支付request
    const token = (window as any).token;
    const payRequest = {
      terminal: 1,
      tid: tid,
      parentTid,
      successUrl: successUrl,
      channelItemId: channelItemId,
      openId: ''
    };
    const ApppayRequest = {
      terminal: 2,
      tid: tid,
      parentTid,
      successUrl: successUrl,
      channelItemId: 19,
      openId: ''
    };
    if (token) {
      // 0:未支付  1：已支付
      const { context } = await getPayState(tid);
      if (context == 1) {
        Confirm({
          okBtn: '确定',
          text: '订单已支付，请勿重复提交',
          confirmCb: () => {
            history.push('/order-list');
          }
        });
      } else {
        let result = JSON.stringify({ ...payRequest, token: token });
        const base64 = new WMkit.Base64();
        let encrypted = base64.urlEncode(result);
        const exportHref = config.BFF_HOST + `/pay/aliPay/${encrypted}`;
        console.log('');

        console.log(encrypted);
        console.log(token);

        if ((window as any).alipay !== undefined) {
          let appResult = JSON.stringify({ ...ApppayRequest, token: token });
          const appBase64 = new WMkit.Base64();
          let appEncrypted = appBase64.urlEncode(appResult);
          (window as any).alipay(this.state().get('trade').get('tradePrice'), tid ? tid : parentTid, tid ? 'tradeId' : 'parentTid',appEncrypted)
          return;
        }


        window.location.href = exportHref;
        // window.open(exportHref);

        // Confirm({
        //   title: '请您在新打开的网页上完成付款',
        //   text:
        //     '付款完成前请不要关闭此窗口完成付款后请根据您的情况点击下面的按钮',
        //   okBtn: '已完成付款',
        //   confirmCb: () => {
        //     history.push('/order-list');
        //   },
        //   cancelBtn: '更换支付方式'
        // });
      }
    } else {
      Alert({ text: '请登录！' });
    }
  };

  /**
   * 余额支付
   */
  balancePay = WMkit.onceFunc(async (tid: string, channelItemId: string) => {
    const token = (window as any).token;
    if (token) {
      //校验输入密码
      let payPassword = this.state().get('payPwd');
      if (payPassword == '') {
        Alert({
          text: '请输入支付密码！'
        });
        return;
      }
      let checkPayPwd = await this.checkCustomerPayPwd(payPassword);
      if (!checkPayPwd) {
        this.dispatch('balance-pay:checkPayPwdRes', false);
        await this.getLoginCustomerInfo();
        return;
      } else {
        this.dispatch('balance-pay:checkPayPwdRes', true);
      }
      const parentTid = this.state().getIn(['trade', 'parentTid']);
      const payMobileRequest = {
        tid: tid,
        parentTid,
        channelItemId: channelItemId
      };
      const res: any = await balancePay(payMobileRequest);
      if (res && res.code == config.SUCCESS_CODE) {
        Alert({ text: '支付成功' });
        // 1.关闭支付密码弹窗
        this.showPassword(false, channelItemId);
        // 2.重置支付密码
        this.dispatch('balance-pay:changePayPwd', '');
        history.push('/order-list');
      } else {
        Alert({ text: res.message });
      }
    } else {
      Alert({ text: '请登录！' });
    }
  }, 3000);

  /**
   * 验证拼团订单是否可支付
   * @param {string} tid
   * @returns {Promise<boolean>}
   */
  checkGrouponOrderPay = async (tid: string) => {
    const token = (window as any).token;
    if (token) {
      const { context } = await checkGrouponOrder(tid);
      return context;
    } else {
      Alert({ text: '请登录！' });
    }
  };

  /**
   * 校验会员支付密码是否可用
   */
  isPayPwdValid = async () => {
    const res = await webApi.isPayPwdValid();
    if (res.code == 'K-000000') {
      return true;
    } else if (res.code == 'K-010206') {
      Confirm({
        text: '您还没有设置支付密码，暂时无法使用余额支付',
        okBtn: '设置支付密码',
        confirmCb: () => history.push('/balance-pay-password')
      });
      return false;
    } else if (res.code == 'K-010207') {
      Alert({
        text: res.message,
        time: 1000
      });
      return false;
    }
  };

  /**
   * 校验输入支付密码
   * @param payPassword
   */
  checkCustomerPayPwd = async (payPassword) => {
    const res = await webApi.checkCustomerPayPwd(payPassword);
    if (res.code == 'K-000000') {
      return true;
    } else if (res.code == 'K-010206') {
      Confirm({
        text: '您还没有设置支付密码，暂时无法使用余额付款',
        okBtn: '设置支付密码',
        confirmCb: () => history.push('/balance-pay-password')
      });
    } else {
      Alert({
        text: res.message,
        time: 1000
      });
      return false;
    }
  };

  /**
   * 获取当前登陆人信息
   */
  getLoginCustomerInfo = async () => {
    const res = (await webApi.getLoginCustomerInfo()) as any;
    if (res.code == 'K-000000') {
      this.dispatch('balance-pay:countPayPwdTime', res.context.payErrorTime);
    }
  };

  /**
   * 支付密码弹框显示
   */
  showPassword = (flag: Boolean, channelItemId) => {
    this.transaction(() => {
      this.dispatch('balance-pay:showPassword', flag);
      this.dispatch('balance-pay:setChannelItemId', channelItemId);
    });
  };

  /**
   * 输入密码修改
   * @param payPwd
   */
  changePayPwd = async (payPwd) => {
    this.dispatch('balance-pay:changePayPwd', payPwd);
  };

  /**
   *
   * @param payPwd
   */
  wxPrepay = async (tradeId, price) => {
    webapi.getPrepayId(tradeId, price).then((res) => {
      return res
    })
  };


}
