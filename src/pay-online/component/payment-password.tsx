import React from 'react';
import { history, noop } from 'wmkit';
import {IMap, Relax} from 'plume2';

@Relax
export default class PaymentPassword extends React.Component<any, any> {
  props: {
    relaxProps?: {
      trade: IMap;
      showPassword: Function;
      balancePay: Function;
      changePayPwd: Function;
      checkPayPwdRes: boolean;
      payPwdTime: number;
      channelItemId: number;
    };
  };

  static relaxProps = {
    trade: 'trade',
    showPassword: noop,
    balancePay: noop,
    doFetchCoupon: noop,
    changePayPwd: noop,
    checkPayPwdRes: 'checkPayPwdRes',
    payPwdTime: 'payPwdTime',
    channelItemId: 'channelItemId'
  };
  render() {
    const {
      trade,
      showPassword,
      balancePay,
      changePayPwd,
      checkPayPwdRes,
      payPwdTime,
      channelItemId
    } = this.props.relaxProps;
    let payPwdErrorTime = 3 - payPwdTime;
    const tid = trade.get('tradeNo');
    return (
      <div className="black-layer">
        <div className="white-layer">
          <span className="payment-title">请输入支付密码</span>
          <div className="input-div">
            <input
              type="password"
              className="int-pass"
              onChange={(e) => {
                changePayPwd(e.target.value);
              }}
            />
          </div>
          <div className="forget-pass">
            {checkPayPwdRes ? (
              ''
            ) : (
              <span className="left-text">
                {payPwdTime == 3
                  ? '账户已冻结，请30分钟后重试'
                  : '密码错误，还有' + payPwdErrorTime + '次机会'}
              </span>
            )}
            <span className="right-text" onClick={() => this.forgetPayPwd()}>
              忘记密码？
            </span>
          </div>
          <div className="down-btn">
            <div className="cancel-btn" onClick={() => showPassword(false, '')}>
              取消
            </div>
            {checkPayPwdRes ? (
              <div className="submit-btn" onClick={() => balancePay(tid, channelItemId)}>
                提交
              </div>
            ) : payPwdTime == 3 ? (
              <div className="no-submit-btn">提交</div>
            ) : (
              <div className="submit-btn" onClick={() => balancePay(tid, channelItemId)}>
                提交
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 忘记密码
   */
  forgetPayPwd() {
    history.push({
      pathname: '/balance-pay-password',
      state: { forget: true }
    });
  }
}
