import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { IList } from '../../../typings/globalType';
import { noop, Confirm, _, Alert } from 'wmkit';
import wx from 'weixin-js-sdk';
import '../css/style.css'
import { cache } from 'config';

@Relax
export default class PayType extends Component<any, any> {
  props: {
    relaxProps?: {
      trade: IMap;
      payItems: IList;
      toPay: Function;
      pingppSdk: Function;
      getPayCharge: Function;
      sendMailToFinance: Function;
      getWXPayMwebUrl: Function;
      getWXPayJSApi: Function;
      zhifubaoPay: Function;
      checkGrouponOrderPay: Function;
      showPassword: Function;
      isPayPwdValid: Function;
      balance: number;
    };
  };

  static relaxProps = {
    trade: 'trade',
    payItems: 'payItems',
    toPay: noop,
    pingppSdk: noop,
    getPayCharge: noop,
    sendMailToFinance: noop,
    getWXPayMwebUrl: noop,
    getWXPayJSApi: noop,
    zhifubaoPay: noop,
    checkGrouponOrderPay: noop,
    showPassword: noop,
    isPayPwdValid: noop,
    balance: 'balance'
  };

  state = {
    payFlag: false
  };
  render() {
    const { trade, payItems, balance } = this.props.relaxProps;
    const tid = trade.get('tradeNo');
    const timeOut = trade.get('orderTimeOut');
    const tradePrice = trade.get('tradePrice');
    return (
      <div className="register-box pay-omline-box">
        <div className="row form-item pay-title">
          <span className="form-text pay-title" style={{ fontSize: '.28rem' }}>订单金额</span>
          <span className="select-text price">￥{tradePrice}</span>
        </div>
        {payItems.toJS().map((pay) => {
          return (
            <div
              className="row form-item pay-title "
              style={{ lineHeight: '0' }}
              onClick={() => this._toPay(pay.sdkType, pay.id, tid)}
            >
              <div className="form-img ">
                {/* <img src={pay.src} /> */}
                <span
                  className={
                    Number(balance) < Number(tradePrice) && pay.gateway.id == 5
                      ? 'balance-text'
                      : 'form-text'
                  }
                >
                  {pay.label}
                </span>

              </div>
              {pay.gateway.id == 5 ? (
                <span
                  className={
                    Number(balance) >= Number(tradePrice)
                      ? 'form-context balance-style'
                      : 'balance-context balance-style'
                  }
                >
                  <span >
                    当前余额:<span style={{ color: "rgba(255, 77, 77, 1)" }}>￥{balance}</span>
                  </span>
                </span>
              ) : null}
              <i className="iconfont icon-jiantou1" />
            </div>
          );
        })}
        {timeOut ? (
          <div>
            <p className="hint-msg" style={{ color: "rgba(153,153,153,1)" }}>请在 {_.formatDate(timeOut)} 前完成支付,否则将会自动取消</p>
            {/* <p className="hint-msg">否则将会自动取消</p> */}
          </div>
        ) : null}
      </div>
    );
  }

  /**
   * 根据sdkType,选择支付sdk
   */
  _toPay = async (type: string, id: string, tid: string) => {
    let timeFlag = null
    if (this.state.payFlag) {
      return;
    } else {
      this.setState({
        payFlag: true
      });
      timeFlag = setInterval(() => {
        this.setState({
          payFlag: false
        });
        clearInterval(timeFlag)
      }, 3000)
    }
    const {
      toPay,
      pingppSdk,
      sendMailToFinance,
      getPayCharge,
      getWXPayMwebUrl,
      getWXPayJSApi,
      zhifubaoPay,
      checkGrouponOrderPay,
      trade
    } = this.props.relaxProps;
    const tradePrice = trade.get('tradePrice');
    const parentTid = trade.get('parentTid');
    const result = await checkGrouponOrderPay(tid);
    const status = result.status;

    if (status == 0 || status == 2) {
      if (type == 'pingpp') {
        toPay(getPayCharge, pingppSdk)(id, tid);
      } else if (type == 'alipay_h5') {
        zhifubaoPay(id, tid);
      } else if (type == 'wx_pub') {
      } else if (type == 'upacp_wap') {
      } else if (type == 'unionpay_b2b_wx') {
        Confirm({
          title: '银联企业付款需在PC上进行完成',
          text: '系统会同时发送邮件至财务邮箱',
          cancelBtn: '更换支付方式',
          okBtn: '通知财务',
          maskClose: false,
          confirmCb: () => {
            //通知财务
            //通知后跳转到订单中心
            sendMailToFinance();
          }
        });
      } else if (type == 'wx_mweb') {
        wx.miniProgram.getEnv(function (res) {
          if (res.miniprogram) {
            // 小程序内--跳转小程序wxpay页面发起支付
            let payData = {
              tradeId: tid,
              parentTid: parentTid,
              price: tradePrice
            };
            let paramStr = JSON.stringify(payData);
            wx.miniProgram.navigateTo({
              url: `/pages/wxpay/wxpay?paramStr=${paramStr}`
            });
            return;
            // this.store.wxPrepay(tradeId, tradePrice).then((res) => {
            //   console.log(res)
            //   return;
            //   paramStr = JSON.stringify(payData);
            //   wx.miniProgram.navigateTo({
            //     url: `/pages/wxpay/wxpay?paramStr=${paramStr}`
            //   });
            // })
            // return;
          }
        });
        //获取微信支付h5支付统一下单接口
        getWXPayMwebUrl();
      } else if (type == 'js_api') {
        wx.miniProgram.getEnv(function (res) {
          if (res.miniprogram) {
            let payData = {
              tradeId: tid,
              parentTid: parentTid,
              price: tradePrice
            };
            let paramStr = JSON.stringify(payData);
            wx.miniProgram.navigateTo({
              url: `/pages/wxpay/wxpay?paramStr=${paramStr}`
            });

            // 小程序内--跳转小程序wxpay页面发起支付
            // wx.miniProgram.navigateTo({
            //   url: `/pages/wxpay/wxpay?tid=${tid ? tid : ''}&parentTid=${
            //     parentTid ? parentTid : ''
            //   }&token=${window.token}`
            // });
          } else {
            // 微信浏览器内--获取微信支付h5支付统一下单接口
            getWXPayJSApi();
          }
        });
      } else if (type == 'balance_h5') {
        // 余额支付
        this._showPassword(id);
      }
    } else if (
      status == 1 ||
      status == 3 ||
      status == 4 ||
      status == 5 ||
      status == 9
    ) {
      Alert({ text: '参数信息不正确，请检查参数！' });
    } else if (status == 6) {
      Alert({ text: '拼团活动已失效！' });
    } else if (status == 7) {
      Alert({ text: '存在同一个拼团营销活动处于待成团状态，不可开团！' });
    } else if (status == 8) {
      Alert({ text: '已参同一团活动，不可再参团！' });
    } else if (status == 10) {
      Alert({ text: '已超团结束时长24小时！' });
    } else if (status == 11) {
      Alert({ text: '未达起购数！' });
    } else if (status == 12) {
      Alert({ text: '超出限购数！' });
    } else if (status == 13) {
      Alert({ text: '团长订单-已成团/已作废！' });
    }
  };

  /**
   * 支付密码弹窗
   */
  _showPassword = async (id) => {
    const {
      isPayPwdValid,
      showPassword,
      balance,
      trade
    } = this.props.relaxProps;
    if (Number(balance) < Number(trade.get('tradePrice'))) {
      Alert({
        text: '可用余额不足！'
      });
    } else {
      const result = await isPayPwdValid();
      if (result) {
        showPassword(true, id);
      }
    }
  };
}
