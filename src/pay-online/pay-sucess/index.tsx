import React from 'react';
import { StoreProvider } from 'plume2';
import '../css/style.css'

import { WMImage, history, _ } from 'wmkit';

import AppStore from '../store';

/**
 * 支付成功
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class PaySuccess extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { tid, parentTid } = this.props.match.params;
    // 防止网关没有回调，这边简单处理为延迟请求两次
    setTimeout(() => {
      this.store.initSuccess(tid, parentTid);
    }, 1000);
    setTimeout(() => {
      this.store.initSuccess(tid, parentTid);
    }, 3000);
  }

  render() {
    const payOrder = this.store.state().get('payOrder');
    const orderCode = payOrder.get('orderCode');
    const grouponNo = payOrder.get('grouponNo');
    return (
      <div className="list-none" style={{ alignItems: "normal" }}>
        {payOrder &&
          payOrder.map((item) => (
            <div style={{display:"flex",flexDirection:"column"}}>
            <WMImage
              src={require('./img/result-suc.png')}
              width="200px"
              height="200px"
              style={{ alignSelf: "center" }}
            />
            <div>
              <p className={'text-al-cen'} style={{textAlign:"center",marginTop:".2rem"}}>订单支付成功！</p>
            </div>
            </div>))}
            <div>
              <div className="bot-box" style={{ border: "0px solid #ebebeb", boxShadow: "1px 2px 7px 0px rgba(4,0,0,0.1)", borderRadius: "20px", margin: ".4rem .2rem .2rem;" }}>
                {payOrder &&
                  payOrder.map((item) => (
                    <div key={item.get('orderCode')}>
                      {/* <div className="line" /> */}
                      <div className="item" style={{ marginTop: "15px" }}>
                        订单编号
                    <span>{item.get('orderCode')}</span>
                      </div>
                      <div className="item">
                        订单金额
                    <span>
                          <i className="iconfont icon-qian" />
                          {_.addZero(item.get('payOrderPrice'))}
                        </span>
                      </div>
                      <div className="item">
                        付款流水号
                    <span>{item.get('receivableNo')}</span>
                      </div>
                    </div>
                  ))}
              </div>
              {/* <div className="bot-line" /> */}
            </div>
            <div className="bottom-btn" style={{ bottom: "14.7%" }}>
              <div className="half">
                {grouponNo ? (
                  <button
                    className="btn btn-ghost"
                    onClick={() => history.push(`/group-buy-detail/${grouponNo}`)}
                  >
                    查看团详情
              </button>
                ) : (
                    <button
                      style={{ background: "rgba(255,255,255,1)", border: "1px solid rgba(102,102,102,1)", borderRadius: "35px" }}
                      className="btn btn-ghost"
                      onClick={() => {
                        const payOrder = this.store.state().get('payOrder');
                        if (payOrder.size > 1) {
                          history.push('/order-list');
                        } else {
                          history.push(
                            `/order-detail/${payOrder.getIn([0, 'orderCode'])}`
                          );
                        }
                      }}
                    >
                      查看订单
              </button>
                  )}
              </div>
              <div className="half" >
                <button className="btn btn-ghost" style={{ background: "rgba(255,255,255,1)", border: "1px solid rgba(102,102,102,1)", borderRadius: "35px" }} onClick={() => history.push('/')}>
                  返回首页
            </button>
              </div>
            </div>
      </div>
    );
  }
}
