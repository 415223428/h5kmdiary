import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import Status from './component/status';
import Logistics from './component/logistics';
import './css/style.css'
const icon = require('./component/img/none.png');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class LogisticsInfo extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { oid } = this.props.match.params;
    const { tid, type } = this.props.match.params;
    this.store.fetchBasicInfo(oid, tid, type);
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Status />
        {/* <div
          className={
            this.store.state().get('result')
              ? 'list-none hide'
              : 'list-none showblock'
          }
        >
          <img src={icon} />
          <span style={styles.darktext}>暂无相关物流信息</span>

        </div> */}

        {
          this.store.state().get('result') ?
            "" :
            <div className="address-box">
              <ul className="box">
                <h2 className="order-title" style={{ background: "rgba(255,255,255,1)", marginTop: ".2rem" }}>物流信息</h2>
                <div style={{ display: "flex" }}>
                  <div className="line-item-express">
                    <img src={require("./component/img/green-radio.png")} alt="" className="radio-icon" style={{ marginBottom: ".72rem" }}></img>

                    {/* <img src={require("./img/line.png")} alt="" className="gray-line"></img> */}
                  </div>
                  <div className="line-between-content" style={{ marginBottom: ".29rem" }}>
                    <div style={{ fontSize: ".28rem", lineHeight: ".42rem" }}><span></span> 您的商品已出库</div>
                    <p className="time" style={{ fontSize: ".24rem", lineHeight: ".42rem", color: "rgba(153,153,153,1)" }}>{this.store.state().get('goodList')._root.entries[0][1]}</p>
                  </div>
                </div>
              </ul>
            </div>
        }


        {this.store.state().get('result') && <Logistics />}
      </div>
    );
  }
}

const styles = {
  darktext: {
    color: ' #333',
    fontSize: '0.28rem',
    margin: '0px',
    lineHeight: '1.8'
  }
};
