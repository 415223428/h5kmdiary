import React from 'react'
import {IMap, Relax} from 'plume2'
import AppStore from '../store'
@Relax
export default class Status extends React.Component<any, any> {

  store: AppStore


  props: {
    relaxProps?: {
      goodList: IMap
    }
  }


  static relaxProps = {
    goodList: 'goodList'
  }


  render() {
    const company = this.props.relaxProps.goodList.get('logistics')
    return (
      <div className="order-status address-box">
        <div className="ship-status ">
          <p>
            <span className="name">发货日期</span>
            <span className="grey">{this.props.relaxProps.goodList.get('deliveryTime')?this.props.relaxProps.goodList.get('deliveryTime').split(' ')[0]:"物流更新中"}</span>
          </p>
          <p>
            <span className="name">物流公司</span>
            <span className="grey">{company.get('logisticCompanyName')}</span>
          </p>
          <p>
            <span className="name">物流单号</span>
            <span className="grey">{company.get('logisticNo')}</span>
          </p>
        </div>
      </div>
    )
  }
}
