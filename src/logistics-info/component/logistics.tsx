import React from 'react'
import { Relax } from 'plume2'
import AppStore from '../store'
const styles = require('../css/style.css')

@Relax
export default class Logistics extends React.Component<any, any> {

  store: AppStore


  props: {
    relaxProps?: {
      detail: any
      goodList: any
    }
  }


  static relaxProps = {
    detail: 'detail',
    goodList: 'goodList'
  }


  render() {
    // console.log(this.props.relaxProps.goodList._root.entries[0][1])
    return (
      <div className="address-box" >
        <ul className={styles.box}>
          <h2 className="order-title" style={{ background: "rgba(255,255,255,1)", marginTop: ".2rem" }}>物流信息</h2>
          {
            this.props.relaxProps.detail.map((v, k) => {
              return (
                // <li className={styles.cur}>
                //   <div><span></span>{v.context}</div>
                //   <p className="time">{v.time}</p>
                // </li>
                <div style={{ display: "flex" }} key={k}>
                  <div className="line-item-express">
                    {k == 0 ?
                      <img src={require("./img/green-radio.png")} alt="" className="radio-icon"></img> :
                      <img src={require("./img/gray-radio.png")} alt="" className="radio-icon" ></img>
                    }
                    <img src={require("./img/line.png")} alt="" className="gray-line"></img>
                  </div>
                  <div className="line-between-content">
                    <div style={{ fontSize: ".28rem", lineHeight: ".42rem" }}><span></span>{v.context}</div>
                    <p className="time" style={{ fontSize: ".24rem", lineHeight: ".42rem", color: "rgba(153,153,153,1)" }}>{v.time}</p>
                  </div>
                </div>
              )
            })
          }
          <div style={{ display: "flex" }}>
            <div className="line-item-express">
              <img src={require("./img/gray-radio.png")} alt="" className="radio-icon" style={{ marginBottom: ".72rem" }}></img>

              {/* <img src={require("./img/line.png")} alt="" className="gray-line"></img> */}
            </div>
            <div className="line-between-content" style={{marginBottom:".29rem"}}>
              <div style={{ fontSize: ".28rem", lineHeight: ".42rem" }}><span></span> 您的商品已出库</div>
              <p className="time" style={{ fontSize: ".24rem", lineHeight: ".42rem", color: "rgba(153,153,153,1)" }}>{this.props.relaxProps.goodList._root.entries[0][1]}</p>
            </div>
          </div>
        </ul>
      </div>
    )
  }
}
