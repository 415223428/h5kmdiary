/**
 * Created by feitingting on 2017/8/3.
 */
import {Store} from 'plume2'
import {Alert} from 'wmkit'
import LogisticsInfoActor from './actor/logistic-info-actor'
import * as webapi from './webapi'
export default class AppStore extends Store {

  bindActor() {
    return [
      new LogisticsInfoActor
    ]
  }


  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  /**
   * 获取跟踪到的物流信息
   * @param id
   * @returns {Promise<void>}
   */
  init = async (id:string) => {
    const code = this.state().getIn(['goodList', 'logistics']).toJS().logisticStandardCode
    const res = await  webapi.fetchDeliveryDetail(code, id)
    if ((res as any).length > 0) {
      this.dispatch('logistics:init', res)
    } else {
      //显示图片tips
      this.dispatch('logistics:result')
/*      Alert({
        text:"暂无相关物流信息！",
        time:2000
      })*/
    }
  }


  /**
   * 获取快递公司基本信息
   * @param id
   * @returns {Promise<void>}
   */
  fetchBasicInfo = async (orderId:string,id: string,type: string) => {
    const res = await webapi.fetchCompanyInfo(orderId,id,type);
    if (res.code == 'K-000000') {
      this.dispatch('logistics:info', res.context)
      this.init((res.context as any).logistics.logisticNo)
    }
  }
}