import { Store } from 'plume2';
import { fromJS } from 'immutable';
import ChoseServiceActor from './actor/chose-service-actor';
import * as webapi from './webapi';
import { config } from 'config';

export default class AppStore extends Store {
  
  bindActor() {
    return [ new ChoseServiceActor];
  }

  /**
   * 初始化，查询并设置平台客服列表
   */
  init = async (storeId) => {
    const {code, context } = await webapi.fetchOnlineServiceList(storeId) as any;
    if (code == config.SUCCESS_CODE) {
      let onlineServices = context ? context.qqOnlineServerItemRopList : [];
      onlineServices = fromJS(onlineServices.map((item) => {
        return {
          customerServiceName: item.customerServiceName,
          customerServiceAccount: item.customerServiceAccount
        }
      }));
      this.dispatch('choseService:setOnlineServices', onlineServices);
    }
  }

}
