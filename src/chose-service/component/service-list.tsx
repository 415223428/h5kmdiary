import React from 'react';
import { history, _ } from 'wmkit';
import { Relax } from 'plume2';


@Relax
export default class ServiceList extends React.Component<any, any> {

  props: {
		relaxProps ? : {
			onlineServices: any
		}
	}

	static relaxProps = {
		onlineServices: 'onlineServices',
	}

  render() {
    const { onlineServices } = this.props.relaxProps;
    return (
      <div>
        <div className="service-title">选择客服</div>
        <div className="service-list">
          {onlineServices.toJS().map(item => 
            <div key={item.customerServiceAccount} className="item" onClick={() => {
              _.qqCustomerService(item.customerServiceAccount);
            }}>
            <i className="iconfont icon-qq icon-item" />
            <span>{item.customerServiceName}</span>
            <i className="iconfont icon-jiantou1 arrow" />
          </div>
          )}
        </div>
      </div>
    );
  }
}
