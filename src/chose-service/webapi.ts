import { Fetch } from 'wmkit';

/**
 * 获取客服信息
 */
export const fetchOnlineServiceList = (storeId) => {
  return Fetch(`/customerService/qq/detail/${storeId}/1`);
}