import {Action, Actor, IMap} from "plume2";

export default class ChoseServiceActor extends Actor {
	defaultState() {
		return {
			// 平台客服列表
      onlineServices: []
		}
	}

	/**
	 * 设置平台客服列表
	 */
	@Action('choseService:setOnlineServices')
	setOnlineServices(state: IMap, onlineServices) {
		return state.set('onlineServices', onlineServices);
	}
}