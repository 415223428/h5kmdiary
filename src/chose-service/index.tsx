import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import ServiceList from './component/service-list';
const style = require('./css/style.css');

@StoreProvider(AppStore)
export default class ChoseService extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    let { sid } = this.props.match.params;
    this.store.init(sid);
  }

  render() {
    return (
      <div className="service-main">
        <ServiceList />
      </div>
    );
  }
}
