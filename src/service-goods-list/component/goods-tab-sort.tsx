import React, {Component} from 'react';
import {IMap, Relax} from 'plume2';
import {noop} from 'wmkit';

@Relax
export default class GoodsTabSort extends Component<any, any> {
  props: {
    relaxProps?: {
      closeShade: Function;
      setSort: Function;
      sortType: IMap;
      isShow: boolean;
    };
  };

  static relaxProps = {
    closeShade: noop,
    setSort: noop,
    sortType: 'sortType',
    isShow: 'isShow'
  };

  render() {
    const { closeShade, setSort, sortType, isShow } = this.props.relaxProps;

    // 排序字段
    const type = sortType.get('type');
    // 排序方式：升序 降序
    const sort = sortType.get('sort');

    return (
      <div style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
        <div style={{ backgroundColor: '#ffffff', width: '100%' }}>
          <div
            className={
              type == 'default' || type == ''
                ? 'sort-item new-theme-text'
                : 'sort-item'
            }
            onClick={() => setSort('default')}
          >
            综合
          </div>

          <div className="sort-item" onClick={() => setSort('dateTime')}>
            <div className={type == 'dateTime' ? 'new-theme-text' : ''}>
              最新
            </div>
            <div
              className={
                type == 'dateTime'
                  ? 'sort-icon-box new-theme-text'
                  : 'sort-icon-box'
              }
            />
          </div>

          <div
            className={
              type == 'salesNum' ? 'sort-item new-theme-text' : 'sort-item'
            }
            onClick={() => setSort('salesNum')}
          >
            销量
          </div>

          {/* <div className="sort-item" onClick={() => setSort('price')}>
            <div className={type == 'price' ? 'new-theme-text' : ''}>价格</div>
            <div
              className={
                type == 'price' && sort == 'asc'
                  ? 'sort-icon-box new-theme-text'
                  : 'sort-icon-box'
              }
            >
              <i className="iconfont icon-xiangshangpai" />
            </div>
            <div
              className={
                type == 'price' && sort == 'desc' ? 'new-theme-text' : ''
              }
            >
              <i className="iconfont icon-xiangxiapai" />
            </div>
          </div> */}

          {isShow ? (
            <div
              className={
                type == 'evaluateNum' ? 'sort-item new-theme-text' : 'sort-item'
              }
              onClick={() => setSort('evaluateNum')}
            >
              评论数
            </div>
          ) : null}

          {isShow ? (
            <div
              className={
                type == 'praise' ? 'sort-item new-theme-text' : 'sort-item'
              }
              onClick={() => setSort('praise')}
            >
              好评
            </div>
          ) : null}

          <div
            className={
              type == 'collection' ? 'sort-item new-theme-text' : 'sort-item'
            }
            onClick={() => setSort('collection')}
          >
            收藏
          </div>
        </div>

        <div
          style={{ width: '100%', height: '100vh' }}
          onClick={() => closeShade()}
        />
      </div>
    );
  }
}
