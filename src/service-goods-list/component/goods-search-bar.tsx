import React from 'react'
import {Link} from 'react-router-dom'
import {Relax} from 'plume2'
import {noop, history} from 'wmkit'

@Relax
export default class GoodsSearchBar extends React.Component<any, any> {


  props: {
    relaxProps?: {
      changeLayout: Function,
      listView: boolean,
      queryString: string,
    }
  }


  static relaxProps = {
    changeLayout: noop,
    listView: 'listView',
    queryString: 'queryString',
  }


  render() {

    const {changeLayout, listView, queryString} = this.props.relaxProps;

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div className="input-box" onClick={() => history.push({pathname: '/serviceSearch', state: {queryString}})}>
              <i className="iconfont icon-sousuo"></i>
              <input type="text" value={queryString} placeholder="搜索商品"/>
            </div>

            <div>
              <a href="javascript:void(0)" className="list-tab" onClick={() => changeLayout()}>
                {/* <i className={`iconfont ${ listView ? 'icon-datu' : 'icon-fenlei' }`} style={{fontSize:".34rem"}}></i> */}
                { listView? 
                <img src={require("../img/classfiy.png")} alt="" style={{width:".34rem",height:".34rem"}}/> :
                 <img src={require("../img/list.png")} alt="" style={{width:".34rem",height:".34rem"}}/>}
                {/* <span>{listView ? '大图' : '列表'}</span> */}
              </a>
            </div>

          </div>
        </div>
      </div>
    )
  }
}
