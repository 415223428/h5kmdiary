import React from 'react';
import { msg } from 'plume2';
import { _, AddCart, Alert, history, WMImage, WMkit } from 'wmkit';
import { MarketingLabel } from 'biz';
import {cache} from 'config'
export default class SpuItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      goods,
      listView,
      spuAddCartFunc,
      changeShareVisible,
      isDistributor,
      saveCheckedSku,
      isShow
    } = this.props;
    // spu下第一个上架的sku信息
    const goodsInfo = goods
      .get('goodsInfos')
      .find((skuInfo) => skuInfo.get('addedFlag') === 1);
    // 商品是否要设置成无效状态(spu下所有sku的库存等于0 或者 起订量大于剩余库存)
    const invalid = goods.get('goodsInfos').every((sku) => {
      const stock = sku.get('stock'); // 库存
      const count = sku.get('count') || 0; // 起订量
      return stock <= 0 || (count > 0 && count > stock);
    });
    // 会员价
    const salePrice = goodsInfo.get('salePrice') || 0;
    // 最低的区间价
    const intervalMinPrice = goodsInfo.get('intervalMinPrice') || 0;
    // 营销标签
    const marketingLabels = goodsInfo.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsInfo.get('couponLabels');
    const id = goodsInfo.get('goodsInfoId');
    let containerClass = listView
      ? invalid
        ? 'goods-list spu-goods invalid-goods'
        : 'goods-list spu-goods'
      : invalid
        ? 'goods-box spu-goods invalid-goods'
        : 'goods-box spu-goods';

    // 社交电商相关内容显示与否
    const social = goodsInfo.get('distributionGoodsAudit') == 2 ? true : false;
    //禁用分享赚
    const socialDisabled = false;
    const distributionCommission = goodsInfo.get('distributionCommission');
    const marketPrice = goodsInfo.get('marketPrice');
    const isdistributor = WMkit.isShowDistributionButton();
    const inviteeId =isdistributor? JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId :'';
    const inviteCode=isdistributor?sessionStorage.getItem('inviteCode'):'';
    const url = `goods-detail/${goodsInfo.get('goodsInfoId')}/?channel=mall&inviteeId=${inviteeId}&inviteCode=${inviteCode}`;
    //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
    //好评率
    let favorableRate = '100';
    if (
      goodsInfo.get('goodsEvaluateNum') &&
      goodsInfo.get('goodsEvaluateNum') != 0
    ) {
      favorableRate = _.mul(
        _.div(
          goodsInfo.get('goodsFavorableCommentNum'),
          goodsInfo.get('goodsEvaluateNum')
        ),
        100
      ).toFixed(0);
    }

    //评论数
    let evaluateNum = '暂无';
    const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
    if (goodsEvaluateNum) {
      if (goodsEvaluateNum < 10000) {
        evaluateNum = goodsEvaluateNum;
      } else {
        const i = _.div(goodsEvaluateNum, 10000).toFixed(1);
        evaluateNum = i + '万+';
      }
    }

    //销量
    let salesNum = '暂无';
    const goodsSalesNum = goodsInfo.get('goodsSalesNum');
    if (goodsSalesNum) {
      if (goodsSalesNum < 10000) {
        salesNum = goodsSalesNum;
      } else {
        const i = _.div(goodsSalesNum, 10000).toFixed(1);
        salesNum = i + '万+';
      }
    }
    // 预估点数
    let pointNum = goodsInfo.get('kmPointsValue');
    //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆

    return (
      <div
        className={containerClass}
        style={{paddingTop:'.16rem'}}
        onClick={() =>
          history.push('/goods-detail/' + goodsInfo.get('goodsInfoId'))
        }
      >
        <div className="img-box">
          <WMImage
            mode="pad"
            src={goodsInfo.get('goodsInfoImg')}
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail b-1px-b">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>

          {/* 评价 */}
          {isShow ? (
            <div className="goods-evaluate">
              <span className="goods-evaluate-spn">{salesNum}销量</span>
              <span className="goods-evaluate-spn mar-lr-28">
                {evaluateNum}评价
              </span>
              <span className="goods-evaluate-spn">{favorableRate}%好评</span>
              {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
            </div>
          ) : (
            <div className="goods-evaluate">
              <span className="goods-evaluate-spn">{salesNum}销量</span>
            </div>
          )}

          <div className="marketing">
            {goodsInfo.get('companyType') == 0 && (
              <div className="self-sales">自营</div>
            )}
            {!social &&
              (marketingLabels || couponLabels) && (
                <MarketingLabel
                  marketingLabels={marketingLabels}
                  couponLabels={couponLabels}
                />
              )}
          </div>

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {social
                  ? _.addZero(marketPrice)
                  : goodsInfo.get('priceType') == 1
                    ? _.addZero(intervalMinPrice)
                    : _.addZero(salePrice)}
              </span>
              {social &&
                !WMkit.inviteeId() &&
                isDistributor && (
                  <span
                    className={
                      invalid ? 'commission commission-disabled' : 'commission'
                    }
                  >
                    /&nbsp;赚{_.addZero(distributionCommission)}
                  </span>
                )}
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            {social && !WMkit.inviteeId() && isDistributor
              ? !invalid && (
                  <div className="social-btn-box">
                    <div
                      className="social-btn social-btn-ghost"
                      onClick={async (e) => {
                        e.stopPropagation();
                        const result = await WMkit.getDistributorStatus();
                        if ((result.context as any).distributionEnable) {
                          history.push(
                            '/graphic-material/' + goodsInfo.get('goodsInfoId')
                          );
                        } else {
                          let reason = (result.context as any).forbiddenReason;
                          msg.emit('bStoreCloseVisible', {
                            visible: true,
                            reason: reason
                          });
                        }
                      }}
                    >
                      发圈素材
                    </div>
                    <div
                      className={
                        socialDisabled
                          ? 'social-btn social-btn-disabled'
                          : 'social-btn'
                      }
                      onClick={async (e) => {
                        e.stopPropagation();
                        history.push({
                          pathname: '/share',
                          state: {
                            goodsInfo ,
                            url,
                            id
                          }
                        });
                      }}
                    >
                      分享赚
                    </div>
                  </div>
                )
              : null}

            {/*小图展示模式*/}
            {listView &&
              ((social && !isDistributor) || !social || WMkit.inviteeId()) && (
                <AddCart
                  disableAdd={invalid}
                  skuId={goodsInfo.get('goodsInfoId')}
                  spuAddCartFunc={spuAddCartFunc}
                />
              )}
            {/*大图展示模式*/}
            {!listView && (
              <AddCart
                disableAdd={invalid}
                skuId={goodsInfo.get('goodsInfoId')}
                spuAddCartFunc={spuAddCartFunc}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}
