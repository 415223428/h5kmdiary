import React from 'react';
import { msg } from 'plume2';
import { _, Alert, history, WMImage, WMkit } from 'wmkit';
import { GoodsNum, MarketingLabel } from 'biz';
import GoodsShareImg from './goods-share-img'
import { cache, config } from 'config'; 
require('../css/style.css')

const faquan=require('../img/faquan.png')
const share=require('../img/share.png')

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const {
      goodsItem,
      listView,
      changeShareVisible,
      isDistributor,
      saveCheckedSku,
      changefenxiang,
      sendId,
      isfenxiang,
      customer,
      cloasefenxiang,
      closeImg,
      iscloseImg,
      isopenImg,
      fenxiangId,
      isShow,
      closeChoose,
      openWechatShare,
      isWechatShare,
    } = this.state;
    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    let params=goodsInfo.get('goodsInfoId');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 起订量
    const count = goodsInfo.get('count') || 0;
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;
    // 营销标签
    const marketingLabels = goodsInfo.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsInfo.get('couponLabels');

    let containerClass = listView
      ? invalid
        ? 'goods-list invalid-goods'
        : 'goods-list'
      : invalid
        ? 'goods-box invalid-goods'
        : 'goods-box';

    // 社交电商相关内容显示与否
    const social = goodsInfo.get('distributionGoodsAudit') == 2 ? true : false;
    const distributionCommission = goodsInfo.get('distributionCommission');
    const marketPrice = goodsInfo.get('marketPrice');
    //禁用分享赚
    const socialDisabled = false;
    const isdistributor = WMkit.isShowDistributionButton();
    const inviteeId =isdistributor? JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId :'';
    const inviteCode=isdistributor?sessionStorage.getItem('inviteCode'):'';
    const url = `goods-detail/${goodsInfo.get('goodsInfoId')}/?channel=mall&inviteeId=${inviteeId}&inviteCode=${inviteCode}`;
    //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
    //好评率
    let favorableRate = '100';
    if (
      goodsInfo.get('goodsEvaluateNum') &&
      goodsInfo.get('goodsEvaluateNum') != 0
    ) {
      favorableRate = _.mul(
        _.div(
          goodsInfo.get('goodsFavorableCommentNum'),
          goodsInfo.get('goodsEvaluateNum')
        ),
        100
      ).toFixed(0);
    }

    //评论数
    let evaluateNum = '暂无';
    const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
    if (goodsEvaluateNum) {
      if (goodsEvaluateNum < 10000) {
        evaluateNum = goodsEvaluateNum;
      } else {
        const i = _.div(goodsEvaluateNum, 10000).toFixed(1);
        evaluateNum = i + '万+';
      }
    }

    //销量
    let salesNum = '暂无';
    const goodsSalesNum = goodsInfo.get('goodsSalesNum');
    if (goodsSalesNum) {
      if (goodsSalesNum < 10000) {
        salesNum = goodsSalesNum;
      } else {
        const i = _.div(goodsSalesNum, 10000).toFixed(1);
        salesNum = i + '万+';
      }
    }
    // 预估点数
    let pointNum = goodsInfo.get('kmPointsValue');
    //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆

    return (
      <div>
      {/* <GoodsShareImg
        goodsItem={goodsItem}
        isfenxiang={isfenxiang}
        customer={customer}
        cloasefenxiang={cloasefenxiang}
        closeImg={closeImg}
        iscloseImg={iscloseImg}
        isopenImg={isopenImg}
        fenxiangId={fenxiangId}
        params={params}
        isWechatShare={isWechatShare}
        openWechatShare={openWechatShare}
      /> */}
      {!invalid?
        <div
          key={id}
          className={containerClass + ' shop-goods-list-new'}
          style={{borderRadius: '0.1rem',padding: '0.16rem 0 0 0.16rem'}}
          onClick={() => history.push('/goods-detail/' + goodsItem.get('id'))}
        >
          <div className="img-box" style={{margin:'.15rem .2rem .2rem .15rem'}}>
            <WMImage
              mode="pad"
              src={goodsInfo.get('goodsInfoImg')}
              width="100%"
              height="100%"
            />
          </div>
          <div className="detail b-1px-b" style={{marginTop: '0.1rem',padding:'.16rem .16rem'}}>
            <div className="title" >{goodsInfo.get('goodsInfoName')}</div>
            <p className="gec">{goodsInfo.get('specText')}</p>

            {/* 评价 */}
            {isShow ? (
              <div className="goods-evaluate" style={{width:'100%'}}>
                <span className="goods-evaluate-spn">{salesNum}销量</span>
                <span className="goods-evaluate-spn mar-lr-28">
                  {evaluateNum}评价
                </span>
                <span className="goods-evaluate-spn">{favorableRate}%好评</span>
                {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
              </div>
            ) : (
              <div className="goods-evaluate">
                <span className="goods-evaluate-spn">{salesNum}销量</span>
              </div>
            )}

            <div className="marketing">
              {goodsInfo.get('companyType') == 0 && (
                <div className="self-sales">自营</div>
              )}

              {!social &&
                (marketingLabels || couponLabels) && (
                  <MarketingLabel
                    marketingLabels={marketingLabels}
                    couponLabels={couponLabels}
                  />
                )}
            </div>

            <div className="bottom" style={social && !WMkit.inviteeId() && isDistributor?{flexDirection:'column',alignItems:'flex-start'}:{}}>
              <div className="bottom-price">
                <span className="price">
                  <i className="iconfont icon-qian" />
                  {social
                    ? _.addZero(marketPrice)
                    : this._calShowPrice(goodsItem, buyCount)}
                </span>
                {social &&
                  !WMkit.inviteeId() &&
                  isDistributor && (
                    <span
                      className={
                        invalid ? 'commission commission-disabled' : 'commission'
                      }
                    >
                      &nbsp;赚
                      <span style={invalid ? { color: '#999', fontSize: '0.25rem' } : { color: '#FF4D4D', fontSize: '0.25rem' }}>
                        <i className="iconfont icon-qian" />
                        {_.addZero(distributionCommission)}
                      </span>
                    </span>
                  )}
                {invalid && <div className="out-stock">缺货</div>}
              </div>
              {social && !WMkit.inviteeId() && isDistributor
                ? !invalid && (
                    <div className="social-btn-box">
                      <div
                        // className="social-btn social-btn-ghost"
                        onClick={async (e) => {
                          e.stopPropagation();
                          const result = await WMkit.getDistributorStatus();
                          if ((result.context as any).distributionEnable) {
                            history.push(
                              '/graphic-material/' + goodsItem.get('id')
                            );
                          } else {
                            let reason = (result.context as any).forbiddenReason;
                            msg.emit('bStoreCloseVisible', {
                              visible: true,
                              reason: reason
                            });
                          }
                        }}
                      >
                        <img src={faquan} alt=""/>
                      </div>
                      <div
                        onClick={async (e) => {
                          e.stopPropagation();
                          // sendId(goodsItem.get('id'));
                          //   changefenxiang();
                          history.push({
                            pathname: '/share',
                            state: {
                              goodsInfo ,
                              url,
                              id
                            }
                          });
                        }}
                      >
                        <img src={share} alt=""/>
                      </div>
                    </div>
                  )
                : null}

              {listView &&
                ((social && !isDistributor) || !social || WMkit.inviteeId()) && (
                  <GoodsNum
                    value={buyCount}
                    max={stock}
                    disableNumberInput={invalid}
                    goodsInfoId={id}
                    onAfterClick={this._afterChangeNum}
                  />
                )}

              {!listView && (
                <GoodsNum
                  value={buyCount}
                  max={stock}
                  disableNumberInput={invalid}
                  goodsInfoId={id}
                  onAfterClick={this._afterChangeNum}
                />
              )}
            </div>
          </div>
        </div>:null}
      </div>
    );
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
  };
}
