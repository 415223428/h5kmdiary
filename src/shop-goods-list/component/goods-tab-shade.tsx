import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';

import GoodsTabCate from './goods-tab-cate';
import GoodsTabSort from './goods-tab-sort';

/**
 * 商品列表遮罩
 */
@Relax
export default class GoodsTabShade extends Component<any, any> {
  props: {
    storeId: number;
    relaxProps?: {
      showShade: boolean;
      tabName: string;
      closeShade: Function;
    };
  };

  static relaxProps = {
    showShade: 'showShade',
    tabName: 'tabName',
    closeShade: noop
  };

  render() {
    const { showShade, tabName } = this.props.relaxProps;

    const top = tabName == 'goodsFilter' ? 0 : 88;

    return (
      <div style={showShade ? {} : { display: 'none' }}>
        <div
          style={{
            position: 'fixed',
            left: 0,
            top: top,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,.5)',
            zIndex: 9999
          }}
        >
          {/*分类只mount一次，只请求一次分类数据*/}
          <GoodsTabCate
            storeId={this.props.storeId}
            hide={tabName !== 'goodsCate'}
          />
          {/*排序*/
          tabName == 'goodsSort' ? <GoodsTabSort /> : null}
        </div>
      </div>
    );
  }
}
