import React, { Component } from 'react';
import { fromJS, List } from 'immutable';
import { IMap, Relax } from 'plume2';

import { Blank, ListView, Loading, noop, WMkit } from 'wmkit';
import GoodsItem from './goods-item';
import SpuItem from './spu-item';

const noneImg = require('../img/none.png');

@Relax
export default class GoodsList extends Component<any, any> {
  _listView: any;

  props: {
    relaxProps?: {
      customer: any;
      storeId: number;
      searchState: IMap;
      listView: boolean; // 小图,大图
      goodsShowType: number; // sku列表,spu列表
      loading: boolean;
      changeShareVisible: Function;
      // 选中的类目
      selectedCate: IMap;
      sortType: IMap;
      selectedBrandIds: any;
      specs: any;
      selectedSpecDetails: IMap;
      queryString: string;
      initialEnd: boolean;
      handleDataReached: (data: Object) => void;
      querySpuAndOpenSpec: (skuId) => void;
      isNotEmpty: boolean;
      isDistributor: string;
      saveCheckedSku: Function;
      //评价相关信息是否展示
      isShow: boolean;
      changefenxiang: Function;
       sendId: Function;
       isfenxiang: boolean;
       cloasefenxiang: Function;
       closeImg: boolean;
       iscloseImg: Function;
       isopenImg: Function;
       fenxiangId: String;
       closeChoose:String;
       openWechatShare:Function;
       isWechatShare:boolean;
    };
  };

  static relaxProps = {
    storeId: 'storeId',
    searchState: 'searchState',
    listView: 'listView',
    goodsShowType: 'goodsShowType',
    loading: 'loading',
    selectedCate: 'selectedCate',
    sortType: 'sortType',
    selectedBrandIds: 'selectedBrandIds',
    specs: 'specs',
    selectedSpecDetails: 'selectedSpecDetails',
    queryString: 'queryString',
    initialEnd: 'initialEnd',
    handleDataReached: noop,
    querySpuAndOpenSpec: noop,
    changeShareVisible: noop,
    isNotEmpty: 'isNotEmpty',
    isDistributor: 'isDistributor',
    saveCheckedSku: noop,
    isShow: 'isShow',
    changefenxiang: noop,
    sendId: noop,
    isfenxiang: 'isfenxiang',
    customer: 'customer',
    cloasefenxiang: noop,
    iscloseImg: noop,
    closeImg: 'closeImg',
    isopenImg: noop,
    fenxiangId: 'fenxiangId',
    closeChoose:'closeChoose',
    openWechatShare:noop,
    isWechatShare:"isWechatShare",
  };
  constructor(props) {
    super(props);
    this.state = {
      // 防止修改商品数量,引起参数变化,重新查询商品列表(刷新页面,体验不好)
      dtoListParam:
        JSON.parse(localStorage.getItem(WMkit.purchaseCache())) || []
    };
  }

  render() {
    let {
      listView,
      goodsShowType,
      selectedCate,
      sortType,
      queryString,
      initialEnd,
      selectedBrandIds,
      specs,
      selectedSpecDetails,
      handleDataReached,
      querySpuAndOpenSpec,
      changeShareVisible,
      isNotEmpty,
      storeId,
      isDistributor,
      saveCheckedSku,
      isShow,
      changefenxiang,
      sendId,
      isfenxiang,
      customer,
      cloasefenxiang,
      closeImg,
      iscloseImg,
      isopenImg,
      fenxiangId,
      closeChoose,
      openWechatShare,isWechatShare
    } = this.props.relaxProps;

    // 组件刚执行完mount，搜索条件没有注入进来，先不加载ListView，避免先进行一次无条件搜索，再立刻进行一次有条件搜索
    if (!initialEnd) {
      return <Loading />;
    }

    let layoutClassName = listView && isNotEmpty ? null : 'view-box';

    // 搜索条件
    // 关键字
    const keyword = queryString;

    // 目录编号
    const cateId =
      selectedCate.getIn(['storeCateIds', 0]) == 0
        ? null
        : selectedCate.getIn(['storeCateIds', 0]);

    // 排序标识
    let sortFlag = 0;
    if (sortType.get('type') == 'dateTime' && sortType.get('sort') == 'desc') {
      sortFlag = 1;
    } else if (
      sortType.get('type') == 'dateTime' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 1;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'desc'
    ) {
      sortFlag = 2;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 3;
    } else if (sortType.get('type') == 'salesNum') {
      sortFlag = 4;
    } else if (sortType.get('type') == 'evaluateNum') {
      sortFlag = 5;
    } else if (sortType.get('type') == 'praise') {
      sortFlag = 6;
    } else if (sortType.get('type') == 'collection') {
      sortFlag = 7;
    }

    // 如果使用关键字搜索，默认排序要传null，为了让更匹配关键字的排在前面(ES默认排序)
    if (
      keyword &&
      (sortType.get('type') === '' || sortType.get('type') === 'default')
    ) {
      sortFlag = null;
    }

    // 品牌
    const brandIds = (selectedBrandIds && selectedBrandIds.toJS()) || null;

    // 规格值
    let selectedSpecs = null;
    if (selectedSpecDetails && selectedSpecDetails.count() > 0) {
      selectedSpecs = List();
      selectedSpecDetails.keySeq().forEach((k, index, iter) => {
        let spec = specs.find((spec) => spec.get('specId') == k);
        selectedSpecs = selectedSpecs.set(
          index,
          fromJS({
            name: spec.get('specName'),
            values: selectedSpecDetails.get(k)
          })
        );
      });
    }
    let dataUrl;
    if (WMkit.isLoginOrNotOpen()) {
      if (goodsShowType == 0) {
        dataUrl = '/goods/skus'; //登陆后的sku列表
      } else {
        dataUrl = '/goods/spus'; //登陆后的spu列表
      }
    } else {
      if (goodsShowType == 0) {
        dataUrl = '/goods/skuListFront'; //开放访问的sku列表
      } else {
        dataUrl = '/goods/spuListFront'; //开放访问的spu列表
      }
    }
    return (
      <ListView
        className={layoutClassName}
        url={dataUrl}
        style={{ height: window.innerHeight - 88 }}
        params={{
          keywords: keyword,
          storeCateIds: cateId ? [cateId] : [],
          brandIds: brandIds,
          specs: selectedSpecs,
          sortFlag: sortFlag,
          storeId,
          esGoodsInfoDTOList: this.state.dtoListParam
        }}
        dataPropsName={
          goodsShowType == 0
            ? 'context.esGoodsInfoPage.content'
            : 'context.esGoods.content'
        }
        otherProps={['goodsIntervalPrices']}
        renderRow={(item: any) => {
          if (goodsShowType == 0) {
            return (
              <GoodsItem
                changeShareVisible={() => changeShareVisible()}
                goodsItem={fromJS(item)}
                listView={listView}
                key={item.id}
                isDistributor={isDistributor}
                saveCheckedSku={saveCheckedSku}
                isShow={isShow}
                sendId={sendId}
                changefenxiang={() => changefenxiang()}
                isfenxiang={isfenxiang}
                customer={customer}
                cloasefenxiang={cloasefenxiang}
                closeImg={closeImg}
                iscloseImg={iscloseImg}
                isopenImg={isopenImg}
                fenxiangId={fenxiangId}
                closeChoose={closeChoose}
                openWechatShare={openWechatShare}
                isWechatShare = {isWechatShare}
              />
            );
          } else {
            return (
              <SpuItem
                changeShareVisible={() => changeShareVisible()}
                goods={fromJS(item)}
                listView={listView}
                key={item.id}
                spuAddCartFunc={querySpuAndOpenSpec}
                isDistributor={isDistributor}
                saveCheckedSku={saveCheckedSku}
                isShow={isShow}
              />
            );
          }
        }}
        renderEmpty={() => <Blank img={noneImg} content="没有搜到任何商品～" />}
        ref={(_listView) => (this._listView = _listView)}
        onDataReached={handleDataReached}
      />
    );
  }
}
