import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop, history } from 'wmkit';
import GoodsCate from '../../shop-goods-cate';

@Relax
export default class GoodsTabCate extends Component<any, any> {
  props: {
    relaxProps?: {
      setCateId: Function;
      selectedCate: any;
    };
    hide: boolean;
    storeId: number;
  };

  static relaxProps = {
    setCateId: noop,
    selectedCate: 'selectedCate'
  };

  render() {
    const { setCateId, selectedCate } = this.props.relaxProps;

    return (
      <div>
        <GoodsCate
          source="goodsList"
          storeId={this.props.storeId}
          hide={this.props.hide}
          storeCateId={selectedCate.getIn(['storeCateIds', 0])}
          handleClick={(storeCateId, cateName) => {
            let { pathname, search } = history.location;
            let path = pathname + '?cid=' + storeCateId + '&cname=' + cateName;
            history.replace(path, {});
            setCateId(storeCateId, cateName);
          }}
        />
      </div>
    );
  }
}
