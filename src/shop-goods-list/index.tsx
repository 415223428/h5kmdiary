import React from 'react';
import { StoreProvider, msg } from 'plume2';

import { history, WMkit, WMWholesaleChoose, WMRetailChoose } from 'wmkit';
import { ShareModal } from 'biz';
import { cache, config } from 'config';
import AppStore from './store';
import GoodsList from './component/goods-list';
import GoodsTab from './component/goods-tab';
import GoodsTabShade from './component/goods-tab-shade';
import GoodsSearchBar from './component/goods-search-bar';
import * as webapi from './webapi';
import Search from '../shop-search';
const car =require('./img/car.png')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsListIndex extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '商品列表';
    msg.on('purchaseNum', this._fetchPurChaseNum);
    this._fetchPurChaseNum();
  }

  componentWillUnmount() {
    msg.off('purchaseNum', this._fetchPurChaseNum);
  }

  state: {
    //sku种类数量
    purchaseNum: 0;
  };

  componentDidMount() {
    // 目录编号
    let cateId = '';
    // 目录名称
    let cateName = '';
    // 搜索关键字
    let queryString = '';

    let search = this.props.location.search;
    search = search.substring(1, search.length);
    let searchArray = search.split('&');
    searchArray.forEach((kv) => {
      const split = kv.split('=');
      if (split[0] === 'cid') {
        cateId = split[1];
      } else if (split[0] === 'cname') {
        cateName = decodeURIComponent(split[1]) || '';
      } else if (split[0] === 'q') {
        queryString = decodeURIComponent(split[1]) || '';
      }
    });
    // 解析url中的参数
    const { sid } = this.props.match.params;
    this.store.memberInfo();
    this.store.init(sid, cateId, cateName, queryString);
  }

  render() {
    const { sid } = this.props.match.params;
    return (
      <div>
        {/*顶部搜索框*/}
        <GoodsSearchBar storeId={sid} />

        {/*顶部的tab*/}
        <GoodsTab />

        {/*tab遮罩*/}
        <GoodsTabShade storeId={sid} />

        {/*商品列表*/}
        <GoodsList />

        {/* 购物车 */}
        <div
          className="bottom-cart show"
          onClick={() => history.push('/purchase-order')}
        >
          {/* <i className="iconfont icon-gouwuche" /> */}
          <img src={car} alt="" style={{width:'120%'}}/>
          {this.state.purchaseNum > 0 && (
            <div className="tag" style={{background:'#fff',color:'#FF4D4D',borderRadius:'50%'}}>{this.state.purchaseNum}</div>
          )}
        </div>

        {/*批发销售类型-规格选择弹框*/}
        <WMWholesaleChoose
          data={this.store.state().get('chosenSpu')}
          visible={this.store.state().get('wholesaleVisible')}
          changeSpecVisible={this.store.changeWholesaleVisible}
        />

        {/*零售销售类型-规格选择弹框*/}
        <WMRetailChoose
          data={this.store.state().get('chosenSpu')}
          visible={this.store.state().get('retailVisible')}
          changeSpecVisible={this.store.changeRetailVisible}
        />

        {/*分享赚弹框*/}
        <ShareModal
          visible={this.store.state().get('shareVisible')}
          changeVisible={this.store.changeShareVisible}
        />

        {false && <Search />}
      </div>
    );
  }

  //查询购物车数量
  _fetchPurChaseNum = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      const { context, message, code } = await webapi.fetchPurchaseCount();
      if (code == config.SUCCESS_CODE) {
        this.setState({ purchaseNum: context });
      }
    } else {
      const purCache =
        JSON.parse(localStorage.getItem(WMkit.purchaseCache())) || [];
      this.setState({ purchaseNum: purCache.length });
    }
  };
}
