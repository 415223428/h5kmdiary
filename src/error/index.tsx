import React, { Component } from 'react';
import { WMImage, history } from 'wmkit';

export default class ErrorPage extends Component<any, any> {
  render() {
    return (
      <div className="list-none">
        <WMImage src={require('./img/none.png')} width="200px" height="200px" />
        <p>啊哦，页面不见了</p>
        <div className="half">
          <button
            className="btn btn-ghost"
            onClick={() => history.replace('/')}
          >
            去首页
          </button>
        </div>
      </div>
    );
  }
}
