import React from 'react';
import { Link } from 'react-router-dom';

export default class NotData extends React.Component<any, any> {
  render() {
    return (
      <div className="not-data">
        <img className="not-img" src={require('../img/not.png')} />
        <span className="not-text1">更多功能正在开发中...</span>
        <span className="not-text2">敬请期待</span>
      </div>
    );
  }
}
