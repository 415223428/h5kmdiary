import React, {Component} from "react"
import {StoreProvider} from "plume2";
import {SkuList, GiftList} from 'wmkit'
import AppStore from './store'


@StoreProvider(AppStore)
export default class ReturnSkuList extends Component<any, any> {
  store: AppStore


  componentWillMount() {
    const {rid} = this.props.match.params

    this.store.init(rid)
  }


  render() {
    const skuList = this.store.state().get('returnSkus').map(sku => {
      sku.imgUrl = sku.pic
      return sku
    })
    const giftList = this.store.state().get('returnGifts')

    return (
      <div style={{background: '#fafafa'}}>
        <SkuList data={skuList}/>
        <GiftList data={giftList}/>
      </div>
    )
  }
}
