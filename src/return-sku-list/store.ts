import {Store} from "plume2";
import { Alert,_ } from 'wmkit'
import * as webApi from './webapi'
import SkuActor from './actor/sku-actor'


export default class AppStore extends Store {
  bindActor() {
    return [
      new SkuActor
    ]
  }


  constructor(props) {
    super(props);
    if (__DEV__) {
      //debug
      (window as any)._store = this;
    }
  }


  /**
   * 退单商品初始化
   */
  init = async (rid) => {
    const skuRes = await webApi.fetchReturnDetail(rid) as any;
    if(skuRes.context.returnType == 'RETURN'){
      skuRes.context.returnItems.forEach(item => {
        item.price = _.addZeroFloor(item.splitPrice);//初始化每个商品的均摊平均价格,向下截取金额
      });
    }
    if(skuRes.code == 'K-000000') {
      this.transaction(() => {
        this.dispatch('sku: list: fetch', (skuRes.context as any).returnItems)
        this.dispatch('gift: list: fetch', (skuRes.context as any).returnGifts)
      })
    }else {
      Alert({text: skuRes.message})
    }
  }

}