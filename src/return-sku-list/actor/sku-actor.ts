import {Action, Actor, IMap} from "plume2";


export default class SkuActor extends Actor {
  defaultState() {
    return {
      returnSkus: [], // 退单商品
      returnGifts: [], //退单赠品
    }
  }


  /**
   * 退单商品集合
   * @param state
   * @param orderSkus
   * @returns {Map<string, V>}
   */
  @Action('sku: list: fetch')
  fetchOrderSkus(state: IMap, returnSkus) {
    return state.set('returnSkus', returnSkus)
  }

  /**
   * 退单赠品集合
   */
  @Action('gift: list: fetch')
  fetchOrderGifts(state: IMap, returnGifts) {
    return state.set('returnGifts', returnGifts)
  }
}
