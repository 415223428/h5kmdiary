import {Store, IOptions} from 'plume2';
import {Alert, history, ValidConst, WMkit} from 'wmkit'
import {config, cache} from 'config';
import * as webapi from './webapi'
import SafeMobileActor from './actor/safe-mobile-actor'


/**
 * 手机号码校验*/
function _testTel(tel: string) {
  const regex = ValidConst.phone
  if (tel == "") {
    Alert({
      text: '请填写手机号'
    })
    return false
  }
  else if (!regex.test(tel)) {
    Alert({
      text: '无效的手机号！'
    })
    return false
  } else {
    return true
  }
}

export default class AppStore extends Store {
  bindActor() {
    return [
      new SafeMobileActor
    ]
  }


  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }


  /**
   * 获取手机号码
   * @param value
   */
  getMobile = (value: string) => {
    this.dispatch('mobile:newMobile', value)
  }


  /**
   * 获取验证码
   * @param value
   */
  getCode = (value: string) => {
    this.dispatch('mobile:code', value)
  }


  /**
   * 发送验证码给新手机号码
   * @returns {Promise<Result<ReturnResult>>}
   */
  sendCode = async (mobile) => {
    return webapi.sendCode(mobile).then(res => {
      const {code, message} = res
      if (code == config.SUCCESS_CODE) {
        this.dispatch('mobile:isValid', true)
        Alert({
          text: '验证码已发送，请注意查收！',
          time: 1000
        })
      } else {
        Alert({
          text: message,
          time: 1000
        })
        return Promise.reject(message)
      }
    })
  }


  /**
   * 提交 验证输入的验证码对不对
   * @returns {Promise<boolean>}
   */
  changeMobile = async () => {
    const mobile = this.state().get('mobile')
    const verifiedCode = this.state().get('code')
    const oldVerifyCode = localStorage.getItem(cache.OLD_VERIFY_CODE)
    if (_testTel(mobile)) {
      if (verifiedCode == "") {
        Alert({
          text: "请填写验证码！",
          time: 1000
        })
        return false
      } else {
        const regex = /^\d{6}$/
        if (!regex.test(verifiedCode)){
          Alert({
            text:"验证码错误！",
            time:1000
          })
          return
        }

        const {code, message} = await webapi.testCode(oldVerifyCode, mobile, verifiedCode)
        if (code == config.SUCCESS_CODE) {      
          WMkit.clearLoginCache()
          Alert({
            text: "修改绑定手机号成功！",
            time: 1000
          })          
          setTimeout(() => {
            history.push({
              pathname: '/login'
            })
          }, 1000)          
        } else {
          Alert({
            text: message,
            time: 1000
          })
        }
      }
    }
  }
}
