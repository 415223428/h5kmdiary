import {Actor, Action, IMap} from "plume2";

export default class SafeMobileActor extends Actor {
  defaultState() {
    return {
      mobile: "",
      code: "",
      buttonValue: "发送验证码",
      //新手机号是否验证通过
      isValid: false
    }
  }


  /**
   * 新手机号是否验证通过
   * @param state
   * @param code
   */
  @Action('mobile:isValid')
  getIsValid(state: IMap, value: boolean) {
    return state.set('isValid', value)
  }


  /**
   * 获取手机号码
   * @param state
   * @param code
   */
  @Action('mobile:newMobile')
  getMobile(state: IMap, mobile: string) {
    return state.set('mobile', mobile)
  }


  /**
   * 获取验证码
   * @param state
   * @param code
   */
  @Action('mobile:code')
  getCode(state: IMap, code: string) {
    return state.set('code', code)
  }
}