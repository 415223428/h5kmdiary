import React from 'react'
import {StoreProvider} from 'plume2';
import AppStore from './store'
import SafeMobileItem from './component/safe-mobile-item'
const submit=require('./img/submit.png')
@StoreProvider(AppStore, {debug: __DEV__})
export default class UserSafeMobile extends React.Component<any, any> {
  store: AppStore;

  render() {
    return (
      <div className="content register-content">
        <SafeMobileItem/>
        <div className="register-btn" style={{padding:'0'}}>
          {/* <button className="btn btn-primary" onClick={() => {
            this.store.changeMobile()
          }}>提交
          </button> */}
          <img src={submit} alt="" style={{width:'100%'}} 
               onClick={() => {
                this.store.changeMobile()
          }}/>
        </div>
      </div>
    )
  }
}
