import React from 'react'
import {Relax} from 'plume2'
import {Link} from 'react-router-dom'
import {noop, FormInput, Button, Alert, ValidConst} from 'wmkit';

const TimerButton = Button.Timer

@Relax
export default class SafeMobileItem extends React.Component<any, any> {
  props: {
    relaxProps?: {
      code: string
      mobile: string
      isValid: boolean
      buttonValue: string
      getMobile: Function
      getCode: Function
      sendCode: Function
    }
  }


  static relaxProps = {
    code: 'code',
    mobile: 'mobile',
    isValid: 'isValid',
    buttonValue: 'buttonValue',
    getMobile: noop,
    getCode: noop,
    sendCode: noop
  }

  render() {
    const {mobile, code, isValid, getMobile, getCode, buttonValue, sendCode} = this.props.relaxProps

    return (
      <div className="register-box">
        <FormInput label="手机号" type="number" placeHolder="请输入新的手机号"
                   defaultValue={mobile} pattern="[0-9]*" maxLength={11}
                   onChange={(e) => getMobile(e.target.value)}/>
        <FormInput label="验证码" placeHolder="请输入验证码" pattern="[0-9]*" maxLength={6}
                   defaultValue={code} type="number"
                   onChange={(e) => getCode(e.target.value)}
                   other={
                     <TimerButton text={buttonValue} resetWhenError={true}
                                  shouldStartCountDown={() => this._sendCode(mobile)}
                                  onClick={() => sendCode(mobile)}/>}/>
        <div className="register-tips">
          <p>提示：</p>

          <p>1.修改绑定手机将会同时修改您的登录手机，请您谨慎操作！</p>

          <p>2.如出现收不到短信的情况，可能是由于通信网络异常造成，请您稍后重新尝试操作！</p>
        </div>
      </div>
    )
  }


  _sendCode = (tel: string) => {
    const regex = ValidConst.phone
    if (tel == "") {
      Alert({
        text: "请填写手机号！"
      })
      return false
    } else if (!regex.test(tel)) {
      Alert({
        text: "无效的手机号！"
      })
      return false
    } else {
      return true
    }
  }
}
