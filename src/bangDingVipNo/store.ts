import { Store } from 'plume2';
import parse from 'url-parse';

import { Alert, history, WMkit, storage, _, wxAuth } from 'wmkit';
import { cache, config } from 'config';
import bangDingVipNoActor from './actor/bangDingVipNo-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new bangDingVipNoActor()];
  }

  constructor(props) {
    super(props);
    (window as any)._store = this;
  }

  init = () => {
    // this.dispatch('bangDingVipNo:init');
    this.fetchList()
  };

  showPass = () => {
    const showpass = this.state().get('isShowpwd');
    this.dispatch('bangDingVipNo:showPass', showpass);
  };

  checkStore = (storeId, storeChecked) => {

    // 初始化stores,将check设为false
    const newarr = this.state().get('stores').toJS()
    for (var i = 0; i < newarr.length; i++) {
      newarr[i].storeChecked = false;
    }
    this.dispatch('bangDingVipNo:stores', newarr);

    this.dispatch('bangDingVipNo:checkVipNo', { storeId: storeId, storeChecked: storeChecked });
  }
  
  checkBoundVip = () => {
    this.dispatch('bangDingVipNo:checkBoundVip');
  }


  // 提交绑定的编号
  submitNo = () => {
    console.log("%csubmitNo", "color:blue;font-size:50px");
    // 判断是否有勾选
    let hasCheck = false
    this.state().get('stores').map((store)=>{
      if(store.toJS().storeChecked === true){
        hasCheck = true
      }
    })
    if(!hasCheck){
      Alert({
        text: "请选择一个会员编号进行绑定"
      });
    }else{
      this.dispatch('bangDingVipNo:toggleModalPwd');
    }
  };

  // 提交Confirm
  submitConfirm = async () => {
    console.log("%csubmitConfirmsubmitConfirmsubmitConfirmsubmi", "color:red;font-size:50px");
    console.log(this.state().get('stores').toJS());
    console.log(this.state().get('password'));
    console.log(sessionStorage.getItem("accountName"));
    if (sessionStorage.getItem("accountName") == null) {
      console.log("%csessionStorage找不到accountName", "color:red;font-size:50px");
      return
    }
    const res = await webapi.authenKM(
      {
        kangMeiCustomerMobileRequestList: this.state().get('stores').toJS(),
        password: this.state().get('password'),
        // accountTypt: 1,
        customerAccount:sessionStorage.getItem("accountName")
      }
    );
    console.log("%c"+res.context, "color:red;font-size:50px");
    console.log("%c"+res.message, "color:red;font-size:50px");
    if(res.code=="K-000000"){
      Alert({
        text: res.context
      });
      history.push('/login');
      this.dispatch('bangDingVipNo:hiddenModal');
    }else{
      Alert({
        text: "密码错误"
      });
    }
  };

  setPassword = (pass) => {
    this.dispatch('bangDingVipNo:password', pass);
  };

  // 提交密码
  submitPwd = () => {
    console.log('submitPwd');
    if ( WMkit.testPass(this.state().get('password'))) {
    this.submitConfirm()
    // this.dispatch('bangDingVipNo:hiddenModal');
    // this.dispatch('bangDingVipNo:toggleModalConfirm');
  }
  };
  
  // hiddenModal
  hiddenModal = () => {
    console.log('hiddenModal');
    this.dispatch('bangDingVipNo:hiddenModal');
  };

  // 请求vip No
  fetchList = async () => {
    console.log("%c+++++++++++++", "color:blue;font-size:50px");
    console.log(sessionStorage.getItem("accountName"));
    if (sessionStorage.getItem("accountName") == null) {
      console.log("%csessionStorage找不到accountName", "color:red;font-size:50px");
      return
    }
    const res = await webapi.getList(sessionStorage.getItem("accountName"));
    var obj = [];
    var newObj = [];

    if (res.code=="K-000000") {
      obj = JSON.parse(JSON.stringify(res.context))
      console.log(obj);
      console.log(newObj);
      obj.forEach((item) => {
        newObj.push({
          storeId: item.customerId,
          storeNo: item.customerId,
          storeChecked: false
        });
      });
      console.log(newObj);
      console.log("%c+++++++++++++", "color:blue;font-size:50px");
      this.dispatch('bangDingVipNo:stores', newObj);
    }else{
      Alert({
        text: res.message
      });
    }
  };

}


