import { Actor, Action, IMap } from 'plume2';

import { fromJS, Map } from 'immutable';
export default class bangDingVipNoActor extends Actor {
  defaultState() {
    return {
      fullBannerImages: require('../img/none.png'),

      isCheckBoundVip:false,
      showModalConfirm: false,
      showModalPwd: false,
      password: "",
      isShowpwd: false,
      stores: []
    };
  }
  //     stores:[
  //         {
  //             storeId: 123,
  //             storeNo:"编号12",
  //             storeChecked: false
  //           },
  //     ],

  /**
   * 页面初始化
   * @param state
   */
  @Action('bangDingVipNo:init')
  init(state) {
    // return state
    //   .set('fullBannerImages', state.get('fullBannerImages'))
    // .set('time', state.get('time'))
    // .set('fullBannerUrl', state.get('fullBannerUrl'))
    // .set('fullBannerSwitchFlag', state.get('fullBannerSwitchFlag'))
  }

  /**
    * 是否显示密码
    * @param state
    * @param showpass
    */
  @Action('bangDingVipNo:showPass')
  showPass(state, showpass: boolean) {
    return state.set('isShowpwd', !showpass);
  }

  // toggleModalPwd
  @Action('bangDingVipNo:toggleModalPwd')
  toggleModalPwd(state) {
    return state
      .set('showModalPwd', !state.get('showModalPwd'))
  }

  // toggleModalConfirm
  @Action('bangDingVipNo:toggleModalConfirm')
  toggleModalConfirm(state) {
    return state
      .set('showModalConfirm', !state.get('showModalConfirm'))
  }

  // hiddenModal
  @Action('bangDingVipNo:hiddenModal')
  hiddenModal(state) {
    return state
      .set('showModalConfirm', false)
      .set('showModalPwd', false)
      // 产品要modal消失后清除密码，取消勾选
      .set('isCheckBoundVip', false)
      .set('password', "")
  }

  // 初始化vip no. list,set false
  @Action('bangDingVipNo:stores')
  storeList(state: IMap, storeList) {
    return state.set('stores', fromJS(storeList));
  }

  //勾选
  @Action('bangDingVipNo:checkVipNo')
  checkVipNo(state,stores) {
    const storesIndex = state
      .get('stores')
      .findIndex((store) => store.get('storeId') === stores.storeId);
    return state.setIn(['stores', storesIndex, 'storeChecked'], !stores.storeChecked);
  }
  //勾选checkBoundVipConfirm
  @Action('bangDingVipNo:checkBoundVip')
  checkBoundVip(state) {
    return state
      .set('isCheckBoundVip', !state.get('isCheckBoundVip'))
  }

  /**
  * 监听密码状态值
  * @param state
  * @param pass
  */
  @Action('bangDingVipNo:password')
  changePsw(state, pass: string) {
    return state.set('password', pass);
  }
}
