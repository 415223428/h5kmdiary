import React from 'react';
import { Link } from 'react-router-dom';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const styles = require('./css/style.css');
import { Check, WMkit, history } from 'wmkit';
const confirmPng = require('./img/confirm.png');
const modelComfirm = require('./img/modelComfirm.png');


@StoreProvider(AppStore, { debug: __DEV__ })
export default class bangDingVipNo extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '绑定会员编号-康美日记';
    this.store.init();
  }

  constructor(props: any) {
    super(props);
  }

  render() {
    const showModalConfirm = this.store.state().get('showModalConfirm')
    const showModalPwd = this.store.state().get('showModalPwd')
    const isCheckBoundVip = this.store.state().get('isCheckBoundVip')
    const stores = this.store.state().get('stores').toJS()
    const password = this.store.state().get('password')
    const isShowpwd = this.store.state().get('isShowpwd')

    // console.log("%c" + showModalConfirm, "color:red;font-size:50px");
    return (
      <div>
        {
          <div className="bangDingVipNo">

            {
              showModalConfirm ? (
                <span>
                  <div className="modalBg" onClick={() => {
                    this.store.hiddenModal()
                  }} ></div>
                  <div className="myModal">
                    <div className="myTitle">一经绑定无法解绑</div>
                    <div className="myCheckBox" style={{
                      marginBottom: '.85rem'
                    }}>
                      <Check checked={isCheckBoundVip}
                        onCheck={() =>
                          this.store.checkBoundVip()
                        }
                        checkStyle={{ width: '0.38rem', height: '0.38rem' }} />
                      <span>我同意</span>
                    </div>
                    <div className="myFooter">
                      {
                        isCheckBoundVip ? (
                          <img src={modelComfirm} alt="" onClick={() => {
                            this.store.submitConfirm()
                          }} />) : (
                            <button style={{
                              background: '#dcdcdc',
                              marginRight: '.85rem',
                              color: 'white'
                            }}>确定</button>
                          )
                      }
                      <button onClick={() => {
                        this.store.hiddenModal()
                      }} >取消</button>
                    </div>
                  </div>
                </span>
              ) : null
            }

            {
              showModalPwd ? (
                <span>
                  <div className="modalBg" onClick={() => {
                    this.store.hiddenModal()
                  }} ></div>
                  <div className="myModal">
                    <div className="myTitle">请输入密码</div>
                    <div className="myCheckBox" style={{
                      marginLeft: ".85rem",
                      marginRight: ".85rem",
                    }}>
                      <span style={{
                        borderBottom: "1px solid #e0e0e0",
                        width:'100%',
                        marginLeft:0
                      }}>
                        <input
                          type={isShowpwd ? 'text' : 'password'}
                          value={password}
                          pattern="/^[A-Za-z0-9]{6,16}$/"
                          onChange={(e) => this.store.setPassword(e.target.value)}
                          style={{
                            // width: "100%",
                            // borderBottom: "1px solid #e0e0e0",
                            width: "calc(100% - 20px)",
                            padding: '.1rem'
                          }}
                        />
                        <i
                          onClick={() => this.store.showPass()}
                          className={`iconfont icon-${
                            isShowpwd ? 'yanjing' : 'iconguanbiyanjing'
                            }`} />
                      </span>
                    </div>
                    <div className="myCheckBox" style={{
                      display:"none"
                    }}>
                      <Check checked={isCheckBoundVip}
                        onCheck={() =>
                          this.store.checkBoundVip()
                        }
                        checkStyle={{ width: '0.38rem', height: '0.38rem' }} />
                      <span>一经绑定无法解绑</span>
                    </div>
                    <div className="myFooter">
                      {
                        // (password && isCheckBoundVip) ? (
                        (password ) ? (
                          <img src={modelComfirm} alt="" onClick={() => {
                            this.store.submitPwd()
                          }} />) : (
                            <button style={{
                              background: '#dcdcdc',
                              marginRight: '.85rem',
                              color: 'white'
                            }}>确定</button>
                          )
                      }

                      <button onClick={() => {
                        this.store.hiddenModal()
                      }} >取消</button>
                    </div>
                  </div>
                </span>
              ) : null
            }

            <div className="myHeader">
              绑定会员编号
            </div>
            <div className="myList">
              <ul>
                {
                  stores && stores.map((store) => {
                    return (<li key={store.storeId}>
                      <span>
                        <Check checked={store.storeChecked}
                          onCheck={() =>
                            this.store.checkStore(store.storeId, store.storeChecked)
                          }
                          checkStyle={{ width: '0.38rem', height: '0.38rem' }} />
                      </span>
                      {store.storeNo}
                    </li>)
                  })
                }
              </ul>
            </div>
            <div className="myFooterBtn">
              <img src={confirmPng} alt="" onClick={() => {
                this.store.submitNo()
              }} />
            </div>
          </div>
        }
      </div>
    );
  }
}
