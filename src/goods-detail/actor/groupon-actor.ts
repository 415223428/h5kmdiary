import { Actor, Action, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class GrouponActor extends Actor {
  defaultState() {
    return {
      //spu信息
      grouponFlag: false
    };
  }
  /**
   * 拼团标签是否显示
   * @param state
   * @param num
   * @returns {IMap}
   */
  @Action('groupon-label: info')
  changeNum(state: IMap, grouponFlag) {
    return state.set('grouponFlag', grouponFlag);
  }
}
