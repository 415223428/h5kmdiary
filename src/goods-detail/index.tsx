import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit, WMRetailChoose, WMWholesaleChoose } from 'wmkit';
import { BStoreClose } from 'biz';
import { cache } from 'config';
import AppStore from './store';
import ImgSlider from './component/img-slider';
import ImgRetail from './component/img-retail';
import Title from './component/goods-title';
import GoodsPriceWholesale from './component/goods-price-wholesale';
import GoodsPriceRetail from './component/goods-price-retail';
import Spec from './component/goods-spec';
import Desc from './component/goods-desc';
import Bottom from './component/goods-bottom';
import StoreName from './component/store-name';
import Promotion from './component/goods-promotion';
import PromotionShade from './component/promotion-shade';
import './css/style.css';
import GoodsCoupon from './component/goods-coupon';
import CouponMask from './component/coupon-mask';
import HeaderSocial from './component/header-social';
import GoodsShare from './component/goods-share';
import GoodsShareImg from './component/goods-share-img';
import NowSpellGroup from './component/now-spell-group';
import GoodsRushBuy from './component/goods-rush-buy';
// import GoodsEvaluation from './component/goods-evaluation';
import GoodsEvaluation from './component/goods-evaluation';
import wx from 'weixin-js-sdk';
import GoodsDetailPricePoints from './component/goods-price-points';
import FindBigPicture from './component/find-big-picture';
import WMPointsChoose from 'wmkit/goods-choose/points-goods-choose';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsDetail extends React.Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
    this.state = props;
    //这边判断是否是微信小程序环境
    wx.miniProgram.getEnv(function (res) {
      (window as any).isMiniProgram = res.miniprogram;
    });
    //从分销链接进来且为店铺外分享，展示全站商品规格
    if (
      location.href.indexOf('inviteeId') != -1 &&
      location.href.indexOf('mall') != -1
    ) {
      let searchObj = WMkit.searchToObj(this.props.location.search);
      //并分销员会员ID和分销渠道存入缓存，分销渠道为商城
      WMkit.setShareInfoCache((searchObj as any).inviteeId, '1');
    }
  }
  _parseQueryString = (url) => {
    var obj = {};
    var keyvalue = [];
    var key = "",
      value = "";
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    for (var i in paraString) {
      keyvalue = paraString[i].split("=");
      key = keyvalue[0];
      value = keyvalue[1];
      obj[key] = value;
    }
    return obj;
  }
  componentWillMount() {
    window.scroll(0, 0);
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    const pointsGoodsId =
      this.props.location.state && this.props.location.state.pointsGoodsId;
    //已登录或未开放，调用原来的
    if (WMkit.isLoginOrNotOpen()) {
      this.store.memberInfo();
      this.store.init(id, this.props.match.url, pointsGoodsId).then((res) => {
        document.title = this.store
          .state()
          .getIn(['goodsInfo', 'goodsInfoName']);
      });
    } else {
      //未登录并且已经开放的，调用新的
      this.store
        .initWithOutLogin(id, this.props.match.url, pointsGoodsId)
        .then((res) => {
          document.title = this.store
            .state()
            .getIn(['goodsInfo', 'goodsInfoName']);
        });
    }
  }

  componentWillUnmount(){
    const picture = document.getElementsByClassName('view-large')[0];
    {
      picture?
      picture.remove():null
    }
  }

  render() {
    //商品名称传递到小程序里
    if (this.store.state().get('goods').size > 0) {
      wx.miniProgram.getEnv((res) => {
        if (res.miniprogram) {
          wx.miniProgram.postMessage({
            data: {
              type: 'goodsShare',
              goodsName: this.store
                .state()
                .get('goods')
                .get('goodsName'),
              skuId: this.store
                .state()
                .get('spuContext')
                .get('skuId'),
              shareUserId: WMkit.getUserId(),
              token: window.token
            }
          });
        }
      });
    }
    const saleType = this.store.state().getIn(['goods', 'saleType']);
    //当商品允许分销且分销审核通过，视为分销商品，不展示促销和优惠券
    const distributionFlag =
      this.store
        .state()
        .get('goodsInfo')
        .get('distributionGoodsAudit') == '2';
    const pointsGoodsId = this.store.state().get('pointsGoodsId');

    const grouponFlag = this.store
      .state()
      .get('goodsInfo')
      .get('grouponLabel');

    //当前选中商品是否为正在进行的抢购活动
    let flashsaleGoodsFlag = false;
    let flashsalePrice;
    let flashsaleGoods;
    let flashSaleGoodsId;
    const selectdGoodsInfoId = this.store
      .state()
      .getIn(['goodsInfo', 'goodsInfoId']);
    this.store
      .state()
      .get('flashsaleGodos')
      .map((v) => {
        if ((v.get('goodsInfoId') == selectdGoodsInfoId) && (v.get('stock') >= v.get('minNum'))) {
          flashsaleGoodsFlag = true;
          flashSaleGoodsId = v.get('id');
          flashsalePrice = v.get('price');
          flashsaleGoods = v;
        }
      });
    // const {isDistributor}=this.state;
    const isDistributor = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
    const search = this.props.location.search;
    const { inviteCode }: any = this._parseQueryString(search);
    // sessionStorage.getItem('inviteCode');
    { inviteCode ? sessionStorage.setItem('otherinviteCode', inviteCode) : null }
    return (
      <div>
        {/* 分享图片 */}
        {/* {isDistributor ?
          <GoodsShareImg
            flashsalePrice={flashsalePrice}
            flashsaleGoodsFlag={flashsaleGoodsFlag} /> : null} */}
        {!this.store.state().get('findBigImage') ? (
          <div className="goods-detail spell-box" style={{ position: 'relative' }}>
            {/*<GoodsEvaluationHead />*/}
            {/*是分销员且不是通过分享链接进来的且当前商品为分销商品才展示发圈素材和分享*/}
            {WMkit.isDistributor() &&
              !flashsaleGoodsFlag && !pointsGoodsId &&
              !sessionStorage.getItem(cache.INVITEE_ID) &&
              distributionFlag && (
                <HeaderSocial skuId={this.props.match.params.id} />
              )}

            {/* 非分销规格才展示优惠券 */}
            {!flashsaleGoodsFlag && <CouponMask />}

            {/*非分销商品才有促销*/}
            {!flashsaleGoodsFlag && <PromotionShade />}

            {/* <ShareModal
          visible={this.store.state().get('shareVisible')}
          changeVisible={this.store.changeShareVisible}
        /> */}

            {/*轮播动图*/}
            {saleType === 0 ? <ImgSlider /> : <ImgRetail />}

            {/*价格信息*/}
            {pointsGoodsId && !flashsaleGoodsFlag ? (
              <GoodsDetailPricePoints />
            ) : saleType === 0 ? (
              <GoodsPriceWholesale />
            ) : (
                  <GoodsPriceRetail
                    flashsalePrice={flashsalePrice}
                    flashsaleGoodsFlag={flashsaleGoodsFlag}
                  />
                )}

            {/*抢购状态 距结束还剩 style样式定义在该组件底部*/}
            {flashsaleGoodsFlag && <GoodsRushBuy flashsaleGoods={flashsaleGoods} />}

            {/*商品名称,收藏,分享*/}
            <Title />

            {//该商品有促销活动时，展示促销活动详情
              !distributionFlag &&
                !pointsGoodsId &&
                !flashsaleGoodsFlag &&
                this.store.state().get('goodsInfo').size > 0 ? (
                  this.store
                    .state()
                    .get('goodsInfo')
                    .get('marketingLabels').size == 0 ? null : (
                      <Promotion />
                    )
                ) : null}

            {/*优惠券信息*/}
            {!distributionFlag &&
              !pointsGoodsId &&
              !flashsaleGoodsFlag &&
              this.store.state().get('goodsInfo').size > 0 ? (
                this.store
                  .state()
                  .get('goodsInfo')
                  .get('couponLabels').size == 0 ? null : (
                    <GoodsCoupon />
                  )
              ) : null}

            {/*规格*/}
            <Spec />

            {/*店铺*/}
            <StoreName />

            {/* 评价 */}
            {this.store.state().get('isShow') &&
              !this.store
                .state()
                .get('top3Evaluate')
                .isEmpty() && <GoodsEvaluation />
            }

            {/*商品详情*/}
            <Desc />

            {/*批发销售类型-规格选择弹框*/}
            <WMWholesaleChoose
              data={this.store
                .state()
                .get('spuContext')
                .toJS()}
              visible={this.store.state().get('wholesaleVisible')}
              isImmediate={this.store.state().get('isImmediate')}
              changeSpecVisible={this.store.changeWholesaleVisible}
            />
            {/*零售销售类型-规格选择弹框*/}
            <WMRetailChoose
              data={this.store
                .state()
                .get('spuContext')
                .toJS()}
              visible={this.store.state().get('retailSaleVisible')}
              isImmediate={this.store.state().get('isImmediate')}
              changeSpecVisible={this.store.changeRetailSaleVisible}
              dataCallBack={this.store.changeSpecVisibleAndRender}
              flashsaleGoodsFlag={flashsaleGoodsFlag}
              flashsaleGoods={flashsaleGoods}
            />

            {/*立即购买-规格选择弹框*/}
            {/* <WMRetailChoose
              data={this.store
                .state()
                .get('spuContext')
                .toJS()}
              visible={this.store.state().get('retailSaleVisible')}
              changeSpecVisible={this.store.changeRetailSaleVisible}
              dataCallBack={this.store.changeSpecVisibleAndRender}
              flashsaleGoodsFlag={flashsaleGoodsFlag}
              flashsaleGoods={flashsaleGoods}
            /> */}

            {/*积分商品-规格选择弹框*/}
            <WMPointsChoose
              data={this.store
                .state()
                .get('spuContext')
                .toJS()}
              visible={this.store.state().get('pointsExchangeVisible')}
              changeSpecVisible={this.store.changePointsExchangeVisible}
              dataCallBack={this.store.changeSpecVisibleAndRender}
            />

            {/* 正在拼团 */}
            {!distributionFlag && grouponFlag && <NowSpellGroup />}

            {/*底部加入购物车、立即购买等按钮*/}
            <Bottom
              flashsaleGoodsFlag={flashsaleGoodsFlag}
              flashsaleGoods={flashsaleGoods}
            />
            {/*如果是微信小程序环境,就显示分享按钮*/}
            {/* {(window as any).isMiniProgram && <GoodsShare />} */}

            {/*分销员禁用状态提示*/}
            <BStoreClose />
          </div>
        ) : (
            <FindBigPicture />
          )}

      </div>
    );
  }
}
