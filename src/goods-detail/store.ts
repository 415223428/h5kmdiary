import { IOptions, msg, Store } from 'plume2';

import { fromJS } from 'immutable';
import { cache, config } from 'config';
import { Alert, Confirm, history, WMkit, wxShare } from 'wmkit';
import DescActor from './actor/desc-actor';
import GoodsActor from './actor/goods-actor';
import { evaluateWebapi, putPurchase } from 'biz';
import * as webapi from './webapi';
import CouponActor from './actor/coupon-actor';
import EvaluateActor from './actor/evaluate-actor';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [
      new DescActor(),
      new GoodsActor(),
      new CouponActor(),
      new EvaluateActor()
    ];
  }

  /**
   * sku初始化
   * @param id
   */
  init = async (id, url, pointsGoodsId) => {
    const res = await Promise.all([
      evaluateWebapi.isShow(),
      webapi.init(id, pointsGoodsId)
    ]);
    this.dispatch('evaluateActor:isShow', res[0]);

    const code = res[1].code,
      context: any = res[1].context;
    //检查这个店铺是否是有效状态
    const status = (await webapi.fetchShopStatus()) as any;
    if (!status.context) {
      Confirm({
        text: '该店铺已失效，不可在店铺内购买商品',
        okBtn: '查看个人中心',
        confirmCb: () => history.push('/user-center')
      });
      return;
    }
    if (code == config.SUCCESS_CODE) {
      await this._initFlashSaleGoods((context as any).goods.goodsId);
      /**商品详情pv/uv埋点*/
      (window as any).myPvUvStatis(id, context.goods.companyInfoId);
      //批发类型按照以往逻辑取第一个显示状态，零售类型则是按照列表页点选进来的sku进行展示
      let saleType = context.goods.saleType;
      //获取第一个sku的关注情况
      const followRes = (await webapi.fetchFollow(
        saleType == 0 ? context.goodsInfos[0].goodsInfoId : id
      )) as any;
      if (followRes.code == config.SUCCESS_CODE) {
        this.dispatch(
          'goods-detail: follow',
          followRes.context.goodsInfos.totalElements > 0
        );
      }
      // 设置在线客服显示状态
      const onlineRes = (await webapi.fetchOnlineServiceList(
        context.goodsInfos[0].storeId
      )) as any;
      if (onlineRes.code == config.SUCCESS_CODE) {
        this.dispatch(
          'goods-detail:setOnlineServiceFlag',
          onlineRes.context != null
        );
      }
      //赋予skuId
      context.skuId = id;
      //赋予积分商品id
      context.pointsGoodsId = pointsGoodsId;
      let goodsInfos = (context as any).goodsInfos;

      //无内容的storeGoodsTabs需要过滤出来
      let storeGoodsTabs = [];
      context.storeGoodsTabs.map((v) => {
        if (
          context.goodsTabRelas.length > 0 &&
          context.goodsTabRelas.find((item) => item.tabId == v.tabId) &&
          context.goodsTabRelas.find((item) => item.tabId == v.tabId).tabDetail
        ) {
          storeGoodsTabs.push(v);
        }
      });
      this.updatePurchaseCount(); // 购物车数量
      this.transaction(() => {
        this.dispatch(
          'goods-list:isDistributor',
          WMkit.isShowDistributionButton()
        );
        this.dispatch('goods-detail: info', context);
        // 是否拼团
        this.dispatch(
          'groupon-label: info',
          fromJS((context as any).grouponFlag)
        );
        this.dispatch('desc:data', context.goods.goodsDetail);
        this._initProps((context as any).goods.cateId);
        this._initBrand((context as any).goods.brandId);
        this._initEvaluate((context as any).goods.goodsId);
        //存储url
        this.dispatch('goods-detail:url', url);
        //图文详情tab
        this.dispatch('goods-detail:storeGoodsTabs', storeGoodsTabs);
        //tab关联内容
        if (context.goodsTabRelas) {
          this.dispatch(
            'goods-detail:storeGoodsTabContent',
            context.goodsTabRelas
          );
        }
      });
      //判断商品所属店铺是否关闭
      const storeRes = (await webapi.fetchStoreInfo(
        goodsInfos[0].storeId
      )) as any;
      if (storeRes.code != config.SUCCESS_CODE) {
        msg.emit('storeCloseVisible', true);
      } else {
        this.dispatch('goodsDetail:initStore', storeRes.context);
      }
      let data = {
        url,
        title: context.goodsInfos[0].goodsInfoName,
        desc: '我发现了一件好货，赶快来看看吧…',
        imgUrl: '',
        afterSuccess: '',
        onCancel: ''
      };
      if (context.goodsInfos[0].goodsIsnfoImg) {
        data.imgUrl = context.goodsInfos[0].goodsInfoImg;
      } else {
        const obj = context.images.filter((f) => f)[0];
        if (obj) {
          data.imgUrl = obj.artworkUrl;
        }
      }

      wxShare(data);
    } else {
      history.replace({
        pathname: '/goods-failure',
        pointsGoodsId: pointsGoodsId
      });
    }
  };
  memberInfo = async () => {
    const res = (await Promise.all([
      webapi.shareInfo()
    ])) as any;
      this.dispatch('member:init', {
        customer: res[0].context,
      });
  };

  /**
   * 未登录且开放访问的情况下
   */
  initWithOutLogin = async (id, url, pointsGoodsId) => {
    const res = await Promise.all([
      evaluateWebapi.isShow(),
      webapi.init(id, pointsGoodsId)
    ]);
    this.dispatch('evaluateActor:isShow', res[0]);

    const code = res[1].code,
      context: any = res[1].context;
    if (code == config.SUCCESS_CODE) {
      await this._initFlashSaleGoods((context as any).goods.goodsId);
      /**商品详情pv/uv埋点*/
      (window as any).myPvUvStatis(id, context.goods.companyInfoId);
      // 设置在线客服显示状态
      const onlineRes = (await webapi.fetchOnlineServiceList(
        context.goodsInfos[0].storeId
      )) as any;
      if (onlineRes.code == config.SUCCESS_CODE) {
        this.dispatch(
          'goods-detail:setOnlineServiceFlag',
          onlineRes.context != null
        );
      }
      context.skuId = id;
      context.pointsGoodsId = pointsGoodsId;
      //获取缓存中的购物车信息
      const buyCartInfo = JSON.parse(
        localStorage.getItem(WMkit.purchaseCache())
      );
      //查询店铺信息
      const storeRes = await webapi.findStore(context.goodsInfos[0].storeId);
      if (storeRes.code == config.SUCCESS_CODE) {
        this.dispatch('goodsDetail:initStore', storeRes.context);
      }

      //无内容的storeGoodsTabs需要过滤出来
      let storeGoodsTabs = [];
      context.storeGoodsTabs.map((v) => {
        if (
          context.goodsTabRelas.length > 0 &&
          context.goodsTabRelas.find((item) => item.tabId == v.tabId) &&
          context.goodsTabRelas.find((item) => item.tabId == v.tabId).tabDetail
        ) {
          storeGoodsTabs.push(v);
        }
      });
      this.updatePurchaseCount(); // 购物车数量
      this.transaction(() => {
        this.dispatch('goods-detail: info', context);
        // 是否拼团
        this.dispatch(
          'groupon-label: info',
          fromJS((context as any).grouponFlag)
        );
        this.dispatch('desc:data', context.goods.goodsDetail);
        //获取缓存的购物车数量
        this.dispatch(
          'goods-detail: purchase: count',
          buyCartInfo ? buyCartInfo.length : 0
        );
        this._initProps((context as any).goods.cateId);
        this._initBrand((context as any).goods.brandId);
        this._initEvaluate((context as any).goods.goodsId);

        //存储url
        this.dispatch('goods-detail:url', url);
        //图文详情tab
        this.dispatch('goods-detail:storeGoodsTabs', storeGoodsTabs);
        //tab关联内容
        if (context.goodsTabRelas) {
          this.dispatch(
            'goods-detail:storeGoodsTabContent',
            context.goodsTabRelas
          );
        }
      });
      //判断商品所属店铺是否关闭
      //const storeRes = (await webapi.fetchStoreInfo(context.storeId)) as any;
      if (storeRes.code != config.SUCCESS_CODE) {
        msg.emit('storeCloseVisible', true);
      }
      let data = {
        url,
        title: context.goodsInfos[0].goodsInfoName,
        desc: '我发现了一件好货，赶快来看看吧…',
        imgUrl: '',
        afterSuccess: '',
        onCancel: ''
      };
      if (context.goodsInfos[0].goodsInfoImg) {
        data.imgUrl = context.goodsInfos[0].goodsInfoImg;
      } else {
        const obj = context.images.filter((f) => f)[0];
        if (obj) {
          data.imgUrl = obj.artworkUrl;
        }
      }
      wxShare(data);
    } else {
      history.replace({
        pathname: '/goods-failure',
        pointsGoodsId: pointsGoodsId
      });
    }
  };

  /**
   * 更新商品详情中的购物车数量
   */
  updatePurchaseCount = () => {
    if (WMkit.isLoginOrNotOpen()) {
      webapi.fetchPurchaseCount().then((res) => {
        if (res.code === config.SUCCESS_CODE) {
          const purchaseNum = res.context || 0;
          this.dispatch('goods-detail: purchase: count', purchaseNum);
        }
      });
    } else {
      const purCache =
        JSON.parse(localStorage.getItem(WMkit.purchaseCache())) || [];
      this.dispatch('goods-detail: purchase: count', purCache.length);
    }
  };

  /**
   * 初始化属性信息
   */
  _initProps = async (cateId) => {
    if (cateId) {
      const goodsPropsRes = (await webapi.fetchPropList(cateId)) as any;
      if (goodsPropsRes.code == config.SUCCESS_CODE) {
        this.dispatch('goods-detail:props', fromJS(goodsPropsRes.context));
      }
    }
  };

  /**
   * 初始化品牌信息
   */
  _initBrand = async (brandId) => {
    if (brandId) {
      const { code, context } = await webapi.fetchBrand(brandId);
      if (code == config.SUCCESS_CODE) {
        this.dispatch('goods-detail:brand', fromJS(context));
      }
    }
  };

  /**
   * 初始化【评价信息
   * @param goodsId
   * @private
   */
  _initEvaluate = (goodsId) => {
    const isShow = this.state().get('isShow');
    if (isShow) {
      webapi.getEvaluateTop3(goodsId).then((res) => {
        if (res.code == config.SUCCESS_CODE) {
          this.dispatch('evaluateActor:top3Evaluate', res.context);
        }
      });
    }
  };

  /**
   * 初始化抢购信息
   * @param goodsId
   * @private
   */
  _initFlashSaleGoods = async (goodsId) => {
    const flashsaleGoods = await webapi.isFlashSale(goodsId);
    this.dispatch('goods-actor:flashsaleGodos', flashsaleGoods.context);
  };

  /**
   * 改变商品数量
   * @param num
   */
  changeNum = (num) => {
    this.dispatch('goods-detail: change: num', num);
  };

  /**
   * 设置分享弹层可见状态
   */
  changeShare = async () => {
    //是分销员且不是通过分享链接进来的，需要获取配置信息
    if (WMkit.isDistributor() && !sessionStorage.getItem(cache.INVITEE_ID)) {
      const result = await WMkit.getDistributorStatus();
      if ((result.context as any).distributionEnable) {
        this.dispatch('change:shareModal');
      } else {
        //禁用原因
        let reason = (result.context as any).forbiddenReason;
        msg.emit('bStoreCloseVisible', {
          visible: true,
          reason: reason
        });
      }
    } else {
      this.dispatch('change:shareModal');
    }
  };

  /**
   * 移除/加入 收藏
   * @param status
   * @param id
   */
  changeFollow = async (status, id) => {
    let res = {};
    if (status) {
      res = await webapi.intoFollow(id);
    } else {
      res = await webapi.outFollow(id);
    }
    if ((res as any).code == 'K-000000') {
      this.dispatch('goods-detail: follow', status);
    } else {
      Alert({
        text: (res as any).message
      });
    }
  };

  /**
   * 加入购物车
   * @param id
   * @param num
   * @param stock
   */
  purchase = async (id, num, stock) => {
    if (num > stock) {
      Alert({
        text: '库存' + stock
      });
      return;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        const { code, message } = await webapi.purchase(id, num);
        if (code == 'K-000000') {
          Alert({
            text: '加入成功!'
          });
          //已登录或者未开放，去查询购物车数量
          if (WMkit.isLoginOrNotOpen()) {
            const purchaseCount = await webapi.fetchPurchaseCount();
            let count = 0;
            if (purchaseCount.code == 'K-000000') {
              count = purchaseCount.context as any;
            }
            this.dispatch('goods-detail: purchase: count', count);
          }
        } else {
          Alert({
            text: message
          });
        }
      } else {
        putPurchase(id, num);
        //更新购物车数量
        const count =
          JSON.parse(localStorage.getItem(WMkit.purchaseCache())).length || 0;
        this.dispatch('goods-detail: purchase: count', count);
      }
    }
  };

  showModal = () => {
    this.dispatch('goods-detail:modal');
  };

  closeModal = () => {
    this.dispatch('goods-detail:modal:close');
  };

  /**
   * 查看促销活动详情
   */
  marketingDetail = async (id: number) => {
    this.showModal();
    if (!id) return;
    const { code, message, context } = await webapi.fetchMarketingDetail(id);
    if (code == 'K-000000') {
      this.dispatch('goods-detail:marketing', context);
    } else {
      Alert({
        text: message
      });
    }
  };

  /**
   * 领券弹框的显示或隐藏
   * @param state
   */
  changeCouponMask = async () => {
    let code = '';
    if (!this.state().get('couponShow')) {
      code = (await this.fetchCouponInfos()) as any;
    }
    if (this.state().get('couponShow') || code === config.SUCCESS_CODE) {
      this.dispatch('change:changeCouponMask');
      msg.emit('app:bottomVisible');
    }
  };

  /**
   * 查询优惠券信息
   */
  fetchCouponInfos = async () => {
    const goodsInfoId = this.state().getIn(['goodsInfo', 'goodsInfoId']);
    const { code, context, message } = (await webapi.fetchCouponInfos(
      goodsInfoId
    )) as any;
    if (code === config.SUCCESS_CODE) {
      this.dispatch('detail: coupon: filed: value', {
        field: 'couponInfos',
        value: context.couponViews
      });
    } else {
      Alert({
        text: message
      });
    }
    return new Promise((resolve) => {
      resolve(code);
    });
  };

  /**
   * 领取优惠券
   */
  receiveCoupon = async (couponId, activityId) => {
    if (window.token) {
      const { code, message } = await webapi.receiveCoupon(
        couponId,
        activityId
      );
      if (code !== config.SUCCESS_CODE) {
        Alert({
          text: message
        });
      }
      this.fetchCouponInfos();
    } else {
      //未登录，先关闭优惠券弹框，再调出登录弹框
      this.dispatch('change:changeCouponMask');
      msg.emit('app:bottomVisible');
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          //callBack:null
        }
      });
    }
  };

  //切换视频暂停播放
  toggleShowVideo = () => {
    this.dispatch('goodsDetail:toggleShowVideo');
  };

  //图文详情tab切换
  changeTabKey = (index) => {
    this.dispatch('goodsDetail:changeTabKey', index);
  };

  //批发销售弹框显示隐藏
  changeWholesaleVisible = () => {
    this.dispatch('goodsDetail:changeWholesaleVisible');
  };

  //零售销售弹框显示隐藏
  changeRetailSaleVisible = (value) => {
    this.dispatch('goodsDetail:changeRetailSaleVisible', value);
  };

  //积分商品规格弹框显示隐藏
  changePointsExchangeVisible = (value) => {
    this.dispatch('goodsDetail:changePointsExchangeVisible', value);
  };

  /**
   * 切换分享赚弹框显示与否
   */
  changeShareVisible = () => {
    this.dispatch('goodsActor:changeShareVisible', true);
  };

  //零售类型弹框操作重新渲染页面
  changeSpecVisibleAndRender = (goodsInfo, pointsGoods) => {
    if (!goodsInfo.isEmpty()) {
      this.dispatch('goodsDetail:closeAndRenderRetail', {
        goodsInfo,
        pointsGoods
      });
      //重新渲染视频
      this.dispatch('goodsDetail:renderVideo');
    }
  };

  //获取图片地址
  showBigImg = (data) => {
    this.dispatch('evaluateActor:findBigPicture', true);
    //图片地址
    this.dispatch('evaluateActor:bigImgIndex', data);
  };

  closeBigImg = () => this.dispatch('evaluateActor:findBigPicture', false);

  curIndex = (index: number) =>
    this.dispatch('evaluateActor:changeIndex', index);

  //拼团商品详情页
  grouponGoodsDetialRender = (goodsInfo) => {};

  /**
   * 立刻抢购
   */
  rushToBuyingFlashSaleGoodsInfo = async(flashSaleGoodsId,num) => {
    if (WMkit.isLogin()) {
      let goodsInfo = this.state().get('goodsInfo');
      let res = await webapi.rushToBuyFlashSaleGoods(flashSaleGoodsId,num);
      if ((res as any).code == 'K-000000') {
        history.push({pathname: '/flash-sale-goods-panic-buying', state: {flashSaleGoodsId:flashSaleGoodsId,flashSaleGoodsNum:num}});
        (window as any).y = '';
      }else if((res as any).code == 'K-140007'){
        // history.push('/flash-sale-order-confirm');
        history.push({pathname: '/flash-sale-goods-panic-buying', state: {flashSaleGoodsId:flashSaleGoodsId,flashSaleGoodsNum:num}});
        (window as any).y = '';
      } else {
        Alert({
          text: (res as any).message
        });
      }
    } else {
      //显示登录弹框
      history.push({
        pathname: '/login'
      });
    }
  };

  isImmediateDo = () => this.dispatch('goods-actor:isImmediate');
  isNotImmediateDo = () => this.dispatch('goods-actor:isNotImmediate');

  reload = () => {
    location.reload();}

  //分享赚
  changefenxiang=()=>{
    this.dispatch('goods-actor:isfenxiang');
  }
  cloasefenxiang=()=>{
    this.dispatch('goods-actor:closefenxiang');
    this.dispatch('goods-actor:closeWechatShare');
  }

  iscloseImg=()=>{
    this.dispatch('goods-actor:closeImg');
  }

  isopenImg=()=>{
    this.dispatch('goods-actor:openImg');
  }
  
  openWechatShare=()=>{
    this.dispatch('goods-actor:openWechatShare');
  }

  // getDistributorInfo = async ()=>{
  //   let {code,context} =  await webapi.fetchDistributorInfo();
  //   if(code == "K-000000"){
  //     if((context as any).distributionCustomerVO){
  //      this.dispatch('goodsActor:getinviteCode',(context as any).distributionCustomerVO.inviteCode) 
  //     }
  //   }
  // }
}


