import React, { PureComponent } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
import { Carousel, WingBlank } from 'antd-mobile';
import EvaluateDetail from './evaluate-detail';

const windowWidth = window.innerWidth.toString() + 'px';

@Relax
export default class GoodsEvaluationHead extends PureComponent<any, any> {
  static relaxProps = {
    bigImgList: 'bigImgList',
    bigImgIndex: 'bigImgIndex',
    curIndex: noop,
    closeBigImg: noop
  };

  constructor(props) {
    super(props);
    this.state = {
      zanFlag: false
    };
  }

  render() {
    const {
      bigImgList,
      bigImgIndex,
      curIndex,
      closeBigImg
    } = this.props.relaxProps;
    return (
      <div className="big-picture">
        <div className="up-black-bj" />
        <div className="center-img" onClick={() => closeBigImg()}>
          <WingBlank>
            <Carousel
              selectedIndex={bigImgIndex}
              dots={false}
              afterChange={(index) => curIndex(index)}
            >
              {bigImgList.map((val) => (
                <div
                  className="slider"
                  style={{ height: windowWidth }}
                  key={val.get('imageId')}
                >
                  <img src={val.get('artworkUrl')}
                       onLoad={() => {
                         window.dispatchEvent(new Event('resize'));
                       }}
                  />
                </div>
              ))}
            </Carousel>
          </WingBlank>
        </div>

        <EvaluateDetail />
      </div>
    );
  }
}
