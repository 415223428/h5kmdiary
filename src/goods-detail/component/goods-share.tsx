import React, {Component} from 'react';
import {IMap, Relax} from 'plume2';
import wx from 'weixin-js-sdk';
import { IList } from 'typings/globalType';
import { noop, _, WMkit, Check, Fetch, Alert } from 'wmkit';
import { config } from 'config';
import { miniProgramShareParams } from '../kit';

@Relax
export default class GoodsShare extends Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.relaxProps.shareVisible != this.props.relaxProps.shareVisible
    ) {
      this.setState({
        checked:
          WMkit.isDistributorLogin() &&
          nextProps.relaxProps.goodsInfo.get('distributionGoodsAudit') == '2'
      });
    }
  }

  props: {
    relaxProps?: {
      shareVisible: boolean;
      changeShare: Function;
      goodsInfo: IMap;
      goods: IMap;
      images: IList;
      goodsInfos: IList;
      spuContext: IMap;
      pointsGoodsId: String;
    };
  };

  static relaxProps = {
    shareVisible: 'shareVisible',
    changeShare: noop,
    goodsInfo: 'goodsInfo',
    goods: 'goods',
    images: 'images',
    goodsInfos: 'goodsInfos',
    spuContext: 'spuContext',
    pointsGoodsId: 'pointsGoodsId'
  };

  render() {
    const { shareVisible, changeShare, goodsInfo,pointsGoodsId } = this.props.relaxProps;
      // 社交电商相关内容显示与否
      const social =
          WMkit.isDistributorLogin() &&
          goodsInfo.get('distributionGoodsAudit') == '2'&&!pointsGoodsId;
    return (
      <div className="goods-share">
        <div className="share-btn" onClick={() => changeShare()}>
          <i className="iconfont icon-fenxiang" />
          {social&& (
              <span>分享赚</span>
            )}
        </div>
        {shareVisible && (
          <div className="poper">
            <div
              className="mask"
              onClick={() => {
                changeShare(false);
              }}
            />
            <div className="poper-content sharingLayer">
              {social ? (
                <div className="title b-1px-b">
                  <p className="price">
                    赚{goodsInfo.get('distributionCommission')}元
                  </p>
                  <p className="infoText">
                    好友通过你分享的链接购买商此商品，你就能赚{goodsInfo.get(
                      'distributionCommission'
                    )}元的利润~
                  </p>
                </div>
              ) : (
                <div className="share-title">分享给好友</div>
              )}
              <div
                className="icon-content"
                style={{ justifyContent: 'center' }}
              >
                <div
                  className="icon"
                  onClick={() => {
                    this.jumpSmallProgram();
                  }}
                >
                  <img src={require('../images/photo-album.png')} alt="" />
                  <p>图文分享</p>
                </div>
              </div>
              {social&& (
                  <div className="checkText">
                    <Check
                      checked={this.state.checked}
                      onCheck={() =>
                        this.setState({
                          checked: !this.state.checked
                        })
                      }
                    />
                    <p>&nbsp;&nbsp;分享此商品后添加至我的店铺</p>
                  </div>
                )}
              <div
                className="share-btn-pro b-1px-t"
                onClick={() => {
                  changeShare(false);
                }}
              >
                取消分享
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }

  /**
   * 跳转到微信小程序图文分享
   */
  jumpSmallProgram = () => {
    const {
      goodsInfos,
      goodsInfo,
      goods,
      images,
      spuContext
    } = this.props.relaxProps;
    const skuId = spuContext.get('skuId');
    //分享类型，0：普通分享，1：分享赚(店铺内) ,2：分享赚（店铺外）3:邀新，4：分享店铺
    let shareType;
    //1.inviteId不存在，即普通登录，如果是小B身份，若是分销商品，则为店铺外分享赚，若为普通商品，则为普通分享
    //2.inviteId不存在，即普通登录，如果是小C身份，则为普通分享
    //3.inviteeId存在，不管小B小C，都是店铺外分享，此处主要考虑二次分享
    if (!WMkit.inviteeId()) {
      if (
        WMkit.isDistributor() &&
        goodsInfo.get('distributionGoodsAudit') == '2'
      ) {
        shareType = 2;
      } else {
        shareType = 0;
      }
    } else {
      shareType = 2;
    }
    const shareUserId = WMkit.getUserId();
    //拼装小程序图文分享需要的参数
    let params = miniProgramShareParams(
      goodsInfos,
      goodsInfo,
      goods,
      images,
      skuId,
      shareType,
      shareUserId
    );
    //先去添加商品到店铺,小C普通分享的时候不调
    if (this.state.checked && shareType == 2) {
      Fetch('/distributor-goods/add', {
        method: 'POST',
        body: JSON.stringify({
          goodsInfoId: goodsInfo.get('goodsInfoId'),
          goodsId: goodsInfo.get('goodsId'),
          storeId: goodsInfo.get('storeId')
        })
      }).then((res) => {
        if (res.code != config.SUCCESS_CODE) {
          Alert({
            text: res.message
          });
        }
      });
    }
    //店铺外分享，还需传inviteeId和token
    wx.miniProgram.navigateTo({
      url: `../canvas/canvas?data=${JSON.stringify(params)}`
    });
    // wx.miniProgram.navigateTo({url: `/pages/canvas/canvas?data=111`})
  };
}
