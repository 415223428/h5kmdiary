import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { cache } from 'config';
import { _, WMkit } from 'wmkit';

/**
 * 零售销售类型-价格展示
 */
@Relax
export default class GoodsDetailPriceRetail extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      goods: IMap;
    };
    //秒杀单价
    flashsalePrice: Number;
    //是否正在秒杀活动中
    flashsaleGoodsFlag: boolean;
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    goods: 'goods'
  };

  render() {
    const { goodsInfo, goods } = this.props.relaxProps;
    const { flashsalePrice, flashsaleGoodsFlag } = this.props;
    //划线价
    const lineShowPrice = this._originPriceInfo(
      goods.get('linePrice'),
      goodsInfo
    );
    //当商品允许分销且分销审核通过，视为分销商品
    const distributionFlag = goodsInfo.get('distributionGoodsAudit') == '2';
    return (
      <div className="market-price" style={{
        background: '#fff',
        height: '.8rem',
        padding: '0 8px'
      }}>
        <div className="b-1px-t" style={{
          height: '100%'
        }}>
          <div className="price-container" style={{
            height: '100%',margin:0,alignItems: 'flex-end',display: 'flex'
          }}>


            <div className="price-box" style={{width:'100%'}}>
              <div
                className="price-item"
                style={
                  flashsaleGoodsFlag
                    ? { color: '#fff', fontSize: '0.36rem'}
                    : {fontSize: '0.36rem',width:'100%'}
                }
              >
                <div style={{display:'flex',justifyContent:'space-between'}}>
                  {/* 价格 */}
                  <div>
                    <i className="iconfont icon-qian" style={{
                      fontSize: '0.36rem'
                    }} />
                    {flashsaleGoodsFlag
                      ? _.addZero(flashsalePrice)
                      : distributionFlag
                        ? _.addZero(goodsInfo.get('marketPrice'))
                        : _.addZero(goodsInfo.get('salePrice'))}
                    {/*{distributionFlag*/}
                    {/*? _.addZero(goodsInfo.get('marketPrice'))*/}
                    {/*: flashsaleGoodsFlag*/}
                    {/*? _.addZero(flashsalePrice)*/}
                    {/*: _.addZero(goodsInfo.get('salePrice'))}*/}
                    &nbsp;&nbsp;
                    {!!lineShowPrice && (
                      <span
                        className="line-price"
                        style={flashsaleGoodsFlag ? styles.colorStyle : null}
                      >
                        ￥{_.addZero(lineShowPrice)}
                      </span>
                    )}
                  </div>
                  {/* 佣金及预估点数 */}
                  <div>
                    {WMkit.isDistributor() &&
                      !flashsaleGoodsFlag &&
                      !sessionStorage.getItem(cache.INVITEE_ID) &&
                      goodsInfo.get('distributionGoodsAudit') == '2' && (
                        <span style={styles.commission}>
                          <span style={{color:'#333'}}>赚</span>{_.addZero(goodsInfo.get('distributionCommission'))}元
                        </span>
                      )
                    }
                    {localStorage.getItem('loginSaleType') == '1'?<span style={styles.commission}><span style={{color:'#333'}}>预估点数</span>{goodsInfo.get('kmPointsValue')}</span>:null}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 获取是否展示划线价,以及划线价
   *   a.若划线价存在,则展示
   *   b.若划线价不存在
   *     b.1.登录前,不展示
   *     b.2.登陆后,展示sku市场价
   * @private
   */
  _originPriceInfo = (linePrice, goodsInfoIm) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        return goodsInfoIm.get('marketPrice');
      } else {
        return null;
      }
    }
  };
}

const styles = {
  commission: {
    marginLeft: '.2rem',
    fontSize: '.26rem',
    color: '#FF4D4D',

  },
  colorStyle: {
    color: '#fff'
  }
};
