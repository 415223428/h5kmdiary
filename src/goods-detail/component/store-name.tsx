import React from 'react';
import { Relax } from 'plume2';
import { _, history } from 'wmkit';

const defaultImg = require('../images/defalutShop.png');

@Relax
export default class StoreName extends React.Component<any, any> {
  props: {
    relaxProps?: {
      store: any;
      isShow: boolean;
    };
  };

  static relaxProps = {
    // 店铺信息
    store: 'store',
    // isShow: 'isShow'
    isShow: 'false'
  };

  render() {
    const { store, isShow } = this.props.relaxProps;
    let follow;
    const followSum = store.get('followSum');
    if (followSum > 10000) {
      follow = _.div(followSum, 10000).toFixed(1) + '万';
    } else {
      follow = followSum;
    }

    let sumCompositeScore: number,
      sumGoodsScore: number,
      sumServerScore: number,
      sumLogisticsScoreScore: number;
    if (store.get('storeEvaluateSumVO')) {
      sumCompositeScore = store.getIn([
        'storeEvaluateSumVO',
        'sumCompositeScore'
      ]);
      sumGoodsScore = store.getIn(['storeEvaluateSumVO', 'sumGoodsScore']);
      sumServerScore = store.getIn(['storeEvaluateSumVO', 'sumServerScore']);
      sumLogisticsScoreScore = store.getIn([
        'storeEvaluateSumVO',
        'sumLogisticsScoreScore'
      ]);
    } else {
      sumCompositeScore = 5;
      sumGoodsScore = 5;
      sumServerScore = 5;
      sumLogisticsScoreScore = 5;
    }

    return (
      <div className="detail-store-style b-1px-tb" onClick={() => history.push(`/store-main/${store.get('storeId')}`)}>
        <div className="up-box">
          <div className="img-box">
            <img
              src={store.get('storeLogo') ? store.get('storeLogo') : defaultImg}
              alt={store.get('storeName')}
            />
          </div>
          <span className="store-name" style={{
            fontSize:'.26rem',
            fontFamily:'PingFang SC',
            fontWeight:'bold',
          }}>{store.get('storeName')}</span>
          {store.get('companyType') === 0 && (
            <div className="self-sales" style={{
              marginLeft:'0.2rem',
              lineHeight: '.36rem'
            }}>自营</div>
          )}
          {/* <div className="right-store-name">
            <span className="store-name">{store.get('storeName')}</span>
            {store.get('companyType') === 0 && (
              <div className="self-sales">自营</div>
            )}
          </div> */}
        </div>
        {isShow ? (
          <div className="center-div">
            <ul className="current-ul">
              <li className="current-li">
                <span className="up-text">{follow}</span>
                <span className="down-text">关注人数</span>
              </li>
              <li className="line1" />
              <li className="current-li">
                <span className="up-text">{store.get('goodsSum')}</span>
                <span className="down-text">全部商品</span>
              </li>
              <li className="line2" />
              <li className="current-li">
                <span className="up-text" style={this._col(sumCompositeScore)}>
                  {sumCompositeScore.toFixed(2)}
                </span>
                <span className="down-text">综合评分</span>
              </li>
            </ul>
            <div className="three-number">
              <span>
                商品质量{' '}
                <span style={this._col(sumGoodsScore)}>
                  {sumGoodsScore.toFixed(2)}
                </span>
              </span>
              <span>
                服务态度{' '}
                <span style={this._col(sumServerScore)}>
                  {sumServerScore.toFixed(2)}
                </span>
              </span>
              <span>
                发货速度{' '}
                <span style={this._col(sumLogisticsScoreScore)}>
                  {sumLogisticsScoreScore.toFixed(2)}
                </span>
              </span>
            </div>
          </div>
        ) : null}
        {/* <div className="bott-btn">
          <a className="current-btn" href={`tel:${store.get('contactMobile')}`}>
            联系卖家
          </a>
          <div
            className="current-btn"
            onClick={() => history.push(`/store-main/${store.get('storeId')}`)}
          >
            进店逛逛
          </div>
        </div> */}
      </div>
    );
  }

  _col = (v: number) => {
    if (v > 3) {
      return { color: '#2DF114' };
    } else {
      return { color: '#f11039' };
    }
  };
}
