import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { history } from 'wmkit';

const windowWidth = window.innerWidth.toString() + 'px';

@Relax
export default class NowSpellGroup extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
    };
  };

  static relaxProps = { goodsInfo: 'goodsInfo' };

  render() {
    const { goodsInfo } = this.props.relaxProps;
    return (
      <div
        className="now-spell-group"
        onClick={() =>
          history.push('/spellgroup-detail/' + goodsInfo.get('goodsInfoId'))
        }
      >
        <span className="text">该商品正在进行拼团</span>
        <div className="see-btn">立即查看</div>
      </div>
    );
  }
}
