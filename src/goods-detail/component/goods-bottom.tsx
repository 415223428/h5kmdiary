import React, { Component } from 'react';
import { IMap, msg, Relax } from 'plume2';

import { Button, history, noop, WMkit } from 'wmkit';

const SubmitButton = Button.Submit2;
const joinCar = require('../images/join-car.png');
const buying = require('../images/buying.png');
const flashsale = require('../images/flashsale.png');
@Relax
export default class GoodsDetailTitle extends Component<any, any> {
  props: {
    relaxProps?: {
      purchaseCount: number;
      store: IMap;
      onlineServiceFlag: boolean;
      goods: IMap;
      updatePurchaseCount: () => {};
      changeWholesaleVisible: Function;
      changeRetailSaleVisible: Function;
      pointsGoodsId: string;
      changePointsExchangeVisible: Function;
      rushToBuyingFlashSaleGoodsInfo: Function;
      // isImmediate: boolean,
      isImmediateDo: Function,
      isNotImmediateDo: Function,
      goodsInfo: IMap,
    };
    flashsaleGoodsFlag: boolean;
    flashsaleGoods: IMap;
  };

  static relaxProps = {
    purchaseCount: 'purchaseCount',
    store: 'store',
    onlineServiceFlag: 'onlineServiceFlag',
    goods: 'goods',
    updatePurchaseCount: noop,
    changeWholesaleVisible: noop,
    changeRetailSaleVisible: noop,
    pointsGoodsId: 'pointsGoodsId',
    changePointsExchangeVisible: noop,
    rushToBuyingFlashSaleGoodsInfo: noop,
    isImmediateDo: noop,
    isNotImmediateDo: noop,
    goodsInfo: 'goodsInfo',
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    // 商品详情中购物车数量角标更新方法
    msg.on('purchaseNum', this.props.relaxProps.updatePurchaseCount);

  }

  componentWillUnmount() {
    msg.off('purchaseNum', this.props.relaxProps.updatePurchaseCount);
  }
  componentDidMount() {
    // console.log(this.props.relaxProps.goods)
    // console.log(this.props.flashsaleGoods)
  }

  render() {
    const {
      purchaseCount,
      store,
      onlineServiceFlag,
      changeWholesaleVisible,
      changeRetailSaleVisible,
      goods,
      pointsGoodsId,
      changePointsExchangeVisible,
      rushToBuyingFlashSaleGoodsInfo,
      isImmediateDo,
      isNotImmediateDo,
      goodsInfo
    } = this.props.relaxProps;
    // (window as any).ysf('product', {
    //   show: 1, // 1为打开， 其他参数为隐藏（包括非零元素）
    //   title: goods.get('goodsName'),
    //   desc: goods.get('goodsSubtitle')||'',
    //   picture: goods.get('goodsImg'),
    //   note: `￥${goodsInfo.get('salePrice')}`,
    //   url: `https://m.kmdiary.com${location.pathname}`
    // });
    // (window as any).openkefuShop = (title,desc,picture,note,url) => {
    //   (window as any).api.sendEvent({
    //     name: 'openkefuShop',
    //     extra: { //发送的数据
    //       title: title,
    //       desc: desc,
    //       picture: picture,
    //       note:note,
    //       url:url
    //     }
    //   });
    // };
    return (
      <div className="detail-bottom-height">
        <div className="detail-bottom" style={{ height: '1rem' }}>
          <div className="detail-bottom-bar" style={{
            height: '100%'
          }}>
            <div className="bar-item-box" style={{
              paddingBottom: '10px'
            }}>
              {/* {onlineServiceFlag &&
                !(window as any).isMiniProgram && ( */}
              <div
                className="bar-item"
                // style={{ minWidth: 70 }}
                onClick={() => {
                  if (WMkit.isLoginOrNotOpen()) {
                    WMkit.getQiyuCustomer();
                    this.configKeFu(goods, goodsInfo);
                    window.location.href = (window as any).ysf('url');
                    (window as any).openqiyu();
                  } else {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        WMkit.getQiyuCustomer();
                        this.configKeFu(goods, goodsInfo)
                        window.location.href = (window as any).ysf('url');
                        (window as any).openqiyu();
                      }
                    });
                  }
                }}
              >
                {!(window as any).isMiniProgram && (
                  <img src={require('../images/service.png')} alt="" style={{
                    width: '0.33rem',
                    height: '0.31rem',
                    margin: ' 0.21rem auto 0.12rem'
                  }} />
                )}
                {!(window as any).isMiniProgram && (
                  <span style={{
                    fontSize: '.22rem',
                    fontFamily: 'PingFang SC',
                    fontWeight: 500,
                    color: 'rgba(102,102,102,1)',
                    // marginBottom:'10px'
                  }}>客服</span>
                )}
              </div>
              {/* )} */}
              <div
                className="bar-item"
                onClick={() => {
                  if (store.get('storeId')) {
                    this._goStore(store.get('storeId'));
                    // history.push(`/store-main/${store.get('storeId')}`)
                  }
                }}
              >
                <img src={require('../images/home.png')} alt="" style={{
                  width: '0.32rem',
                  height: '.32rem',
                  margin: '0.19rem auto 0.14rem'
                }} />
                <span style={{
                  fontSize: '.22rem',
                  fontFamily: 'PingFang SC',
                  fontWeight: 500,
                  color: 'rgba(102,102,102,1)',
                  // marginBottom:'10px'
                }}>首页</span>
              </div>
              <div
                className="bar-item"
                style={{ flexDirection: 'column' }}
                onClick={() => history.push('/purchase-order')}
              >
                <div className="cart-content">
                  {purchaseCount > 0 && (
                    <div className="num-tips" style={{
                      width: '0.28rem',
                      height: '0.28rem',
                      top: '0.1rem',
                      right: '-0.2rem',
                    }}>{purchaseCount}</div>
                  )}
                  {/* <i className="iconfont icon-gouwuche" style={{
                    fontSize: '.35rem'
                  }} /> */}
                  <img src={require('../images/cart.png')} alt="" style={{
                    width: '0.39rem',
                    height: '.33rem',
                    // paddingBottom:'2px'
                    margin: '0.19rem auto 0.14rem'
                  }} />
                </div>
                <span className="red" style={{
                  fontSize: '.22rem',
                  fontFamily: 'PingFang SC',
                  fontWeight: 500,
                  color: 'rgba(102,102,102,1)',
                  // marginBottom:'10px'
                }}>购物车</span>
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
              {this.props.flashsaleGoodsFlag ? null :
                pointsGoodsId ? (
                  <SubmitButton
                    key={pointsGoodsId}
                    defaultStyle={{
                      height: '1rem',
                      lineHeight: '1rem',
                      width: '2.22rem',
                      border: 'none'
                    }}
                    text="立即兑换"
                    onClick={() => {
                      if (WMkit.isLoginOrNotOpen()) {
                        changePointsExchangeVisible(true);
                      } else {
                        msg.emit('loginModal:toggleVisible', {
                          callBack: () => { }
                        });
                      }
                    }}
                  />
                ) : (
                    // <SubmitButton
                    //   key={'addToShopCart'}
                    //   defaultStyle={{
                    //     height: '1rem',
                    //     lineHeight: '1rem',
                    //     backgroundColor: '#000',
                    //     borderColor: '#000',
                    //     width: '2.22rem',
                    //     background: 'linear-gradient(135deg, #F69626, #F8C446)',
                    //     border: 'none'
                    //   }}
                    //   text="加入购物车"
                    //   onClick={() => {
                    //     console.log('%cwwwwwwwwwwwwwww','color:red;fontsize:30px');
                    //     // msg.emit('goods-actor:isNotImmediate', {})
                    //     isNotImmediateDo()
                    //     goods.get('saleType') == 0
                    //       ? changeWholesaleVisible()
                    //       : changeRetailSaleVisible(true)
                    //   }
                    //   }
                    // />
                    <img src={joinCar} alt="" style={{ width: '2.22rem', height: '100%' }}
                      onClick={() => {
                        console.log('%cwwwwwwwwwwwwwww', 'color:red;fontsize:30px');
                        // msg.emit('goods-actor:isNotImmediate', {})
                        isNotImmediateDo()
                        goods.get('saleType') == 0
                          ? changeWholesaleVisible()
                          : changeRetailSaleVisible(true)
                      }
                      } />
                  )}
              {/* <SubmitButton
                key={'immediateBuy'}
                defaultStyle={{ height: '1rem', lineHeight: '1rem', width: '2.22rem', background: 'linear-gradient(135deg, #FF6A4D, #FF1A1A)', border: 'none' }}
                text="立即购买"
                onClick={() =>{
                  console.log('%cqqqqqqqqqqqq','color:red;fontsize:30px');
                  // msg.emit('goods-actor:isImmediate', {})
                  isImmediateDo()
                  goods.get('saleType') == 0
                    ? changeWholesaleVisible()
                    : changeRetailSaleVisible(true)
                }
                }
              /> */}
              {
                pointsGoodsId ? null :
                  this.props.flashsaleGoodsFlag ? (
                    // <SubmitButton
                    //   key={'flashSaleButton'}
                    //   defaultStyle={{
                    //     lineHeight: '1rem',
                    //     width: '2.22rem',
                    //     backgroundColor: '#FF0000',
                    //     borderColor: '#FF0000',
                    //     border: 'none'
                    //   }}
                    //   text="立即抢购"
                    <img src={flashsale}
                      alt=""
                      style={{ width: '2.22rem', height: '100%' }}
                      onClick={() =>
                        rushToBuyingFlashSaleGoodsInfo(
                          this.props.flashsaleGoods.get('id'),
                          this.props.flashsaleGoods.get('minNum')
                        )
                      }
                    />
                  ) : (
                      <img src={buying} alt="" style={{ width: '2.22rem', height: '100%' }}
                        onClick={() => {
                          console.log('%cqqqqqqqqqqqq', 'color:red;fontsize:30px');
                          // msg.emit('goods-actor:isImmediate', {})
                          isImmediateDo()
                          goods.get('saleType') == 0
                            ? changeWholesaleVisible()
                            : changeRetailSaleVisible(true)
                        }
                        } />)}
            </div>
          </div>
        </div>
      </div>
    );
  }

  configKeFu = (goods, goodsInfo) => {
    (window as any).ysf('product', {
      show: 1, // 1为打开， 其他参数为隐藏（包括非零元素）
      title: goods.get('goodsName'),
      desc: goods.get('goodsSubtitle') || '',
      picture: goods.get('goodsImg'),
      note: `￥${goodsInfo.get('salePrice')}`,
      url: `https://m.kmdiary.com${location.pathname}`
    });
    (window as any).openkefuShop = (title, desc, picture, note, url) => {
      (window as any).api.sendEvent({
        name: 'openkefuShop',
        extra: { //发送的数据
          title: title,
          desc: desc,
          picture: picture,
          note: note,
          url: url
        }
      });
    };
  };

  _goStore = (storeId) => {
    //商城范畴，去商城首页,跟小B小C的身份无关
    // history.push(`/store-main/${storeId}`);
    // 首页
    history.push(`/`);
  };
}
