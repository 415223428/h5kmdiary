import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { cache } from 'config';
import { _, WMkit } from 'wmkit';

/**
 * 积分商品-价格展示
 */
@Relax
export default class GoodsDetailPricePoints extends Component<any, any> {
  props: {
    relaxProps?: {
      pointsGoods: IMap;
    };
  };

  static relaxProps = {
    pointsGoods: 'pointsGoods',
  };

  render() {
    const { pointsGoods } = this.props.relaxProps;
    return (
      <div className="market-price" style={{
        background: '#fff',
        height: '.8rem'
      }}>
        <div className="b-1px-t" style={{
          height: '100%'
        }}>
          <div className="price-container" style={{
            height: '100%'
          }}>
            <div className="points-box">
              <div className="price-item">
                {pointsGoods.get('points')}积分
                {localStorage.getItem('loginSaleType') == '1'?
                <span style={{marginLeft: '.26rem',fontSize: '.28rem',color: '#FF4D4D',}}>
                  <span style={{color:'#333'}}>预估点数</span>
                  {pointsGoods.get('goodsInfo').get('kmPointsValue')}
                </span>
                :null}
              </div>
              <div className="mkt-price">
                市场价￥{_.addZero(pointsGoods.get('goodsInfo').get('marketPrice'))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
