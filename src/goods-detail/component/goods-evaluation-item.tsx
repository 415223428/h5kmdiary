import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop, Star, WMImage } from 'wmkit';
import { IList } from '../../../typings/globalType';

const defaultImg = require('../images/default-img.png');
const windowWidth = window.innerWidth.toString() + 'px';
@Relax
export default class GoodsEvaluationItem extends Component<any, any> {
  contentRef;
  static relaxProps = {
    top3Evaluate: 'top3Evaluate',
    showBigImg: noop
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { top3Evaluate } = this.props.relaxProps;
    const evaluateResp = top3Evaluate.getIn([
      'listResponse',
      'goodsEvaluateVOList'
    ]);

    return (
      <div>
        {evaluateResp.map((v, k) => {
          return (
            <div className="evaluation-item b-1px-b" key={v.get('evaluateId')}>
              <div className="evaluation-item-head">
                <div className="evaluation-item-head-user">
                  <img src={defaultImg} alt="" style={{
                    width:'0.7rem',
                    height:'0.7rem'
                  }}/>
                  <span className="evaluation-text" style={{
                    fontWeight:'bold'
                  }}>
                    {v.get('isAnonymous') == 0 ? v.get('customerName') : '匿名'}
                  </span>
                </div>
                <div>
                  <Star star={v.get('evaluateScore')} />
                </div>
              </div>
              <div className="evaluation-item-body">
                <p className="evaluation-text">{v.get('evaluateContent')}</p>
              </div>
              <div className="evaluation-item-img">
                <ul className="evaluation-item-img-list clearfix">
                  {v.get('evaluateImageList') &&
                    this._Img(v.get('evaluateImageList'))}
                </ul>
                {v.get('evaluateImageList') && (
                  <div className="evaluation-item-img-tag">
                    <span>共{v.get('evaluateImageList').size}张</span>
                  </div>
                )}
              </div>
            </div>
          );
        })}
      </div>
    );
  }

  _Img = (imgList: IList) => {
    const { showBigImg } = this.props.relaxProps;
    return imgList.map((v, k) => {
      if (k < 4) {
        return (
          <li
            className="evaluation-item-img-item"
            key={v.get('imageId')}
            onClick={() => showBigImg(v.get('imageId'))}
          >
            <WMImage
              src={v.get('artworkUrl')}
              width={windowWidth}
              height={windowWidth}
            />
          </li>
        );
      }
    });
  };
}
