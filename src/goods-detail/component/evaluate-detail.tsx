import React, { PureComponent } from 'react';
import { Relax } from 'plume2';
import { Star } from 'wmkit';
import { IList } from '../../../typings/globalType';

@Relax
export default class EvaluateDetail extends PureComponent<any, any> {
  props: {
    relaxProps?: {
      bigImgList: IList;
      bigImgIndex: number;
    };
  };

  static relaxProps = {
    bigImgList: 'bigImgList',
    bigImgIndex: 'bigImgIndex'
  };

  constructor(props) {
    super(props);
    this.state = {
      zanFlag: false
    };
  }

  render() {
    const { bigImgList, bigImgIndex } = this.props.relaxProps;
    const evaluate = bigImgList.get(bigImgIndex).get('goodsEvaluate');
    return (
      <div className="down-content">
        <Star star={evaluate.get('evaluateScore')} />
        <span className="specific">{evaluate.get('specDetails')}</span>
        <span className="introduce">{evaluate.get('evaluateContent')}</span>
      </div>
    );
  }

  _clickZan = () => {
    this.setState({
      zanFlag: !this.state.zanFlag
    });
  };
}
