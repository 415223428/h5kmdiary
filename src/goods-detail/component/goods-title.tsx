import React, { Component } from 'react';
import { Relax, IMap, msg } from 'plume2';
import { noop, WMImage, WMkit, history } from 'wmkit';
import { IList } from 'typings/globalType';
import { cache, config } from 'config';
@Relax
export default class GoodsDetailTitle extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      follow: boolean;
      changeFollow: Function;
      init: Function;
      url: String;
      goods: IMap;
      goodsInfos: IList;
      pointsGoodsId: string;
      isfenxiang: boolean;
      changefenxiang: Function;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    follow: 'follow',
    changeFollow: noop,
    init: noop,
    url: 'url',
    goods: 'goods',
    goodsInfos: 'goodsInfos',
    pointsGoodsId: 'pointsGoodsId',
    isfenxiang: 'isfenxiang',
    changefenxiang: noop,
  };
  state: {
    isShare: false
  }
  render() {
    const { goodsInfo, follow, goods, pointsGoodsId, changefenxiang } = this.props.relaxProps;
    const isdistributor = WMkit.isShowDistributionButton();
    return (
      <div className="detail-box" style={{
        background: '#fff'
      }}>
        <div className="title-left">
          {/*商品编号功能产品要删除的*/}
          {/*<div className="sku-title">{goodsInfo.get('goodsInfoNo')}</div>*/}
          <div className="sku-det" style={{
            fontWeight: 'bold'
          }}>{goods.get('goodsName')}</div>
          {goods.get('goodsSubtitle') && (
            <div className="sku-det sec-title">
              {goods.get('goodsSubtitle')}
            </div>
          )}
        </div>
        {
          !pointsGoodsId &&
          <div
            className="right-bar-item b-1px-l"
            style={{ minWidth: 70, color: '#333333' }}
            onClick={() => this.collect()}
          >
            {follow ? (
              <img src={require('../images/heart.png')} alt="" style={{
                width: '0.34rem',
                height: '0.34rem',
              }} />
              // <i className="iconfont icon-xinxing2 loved" />
            ) : (
                <img src={require('../images/heartblank.png')} alt="" style={{
                  width: '0.34rem',
                  height: '0.34rem',
                }} />
                // <i className="iconfont icon-aixin" />
              )}
            <span>{follow ? '已收藏' : '收藏'}</span>
          </div>
        }
        {
          !pointsGoodsId &&
          <div
            className="right-bar-item b-1px-l"
            style={{ minWidth: '1rem', color: '#333333' }}
            onClick={() => this.shareMain()}
          >
            <img src={require('../images/share.png')} alt=""
              style={{
                width: '0.34rem',
                height: '0.34rem',
              }}
            />
            <span>分享</span>
          </div>
        }

        {/* {this.state.isShare&&(
          <div className='goods-share-box'>
            <div>

            </div>
          </div>
        )} */}
      </div>
    );
  }
  save() {
    console.log('%csave', 'color:red');
  }
  collect() {
    const {
      follow,
      changeFollow,
      goodsInfo,
      init,
      url,
      goods,
      goodsInfos
    } = this.props.relaxProps;
    let saleType = goods.get('saleType');
    if (WMkit.isLoginOrNotOpen()) {
      changeFollow(
        !follow,
        saleType == 0
          ? goodsInfos.get(0).get('goodsInfoId')
          : goodsInfo.get('goodsInfoId')
      );
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          init(
            saleType == 0
              ? goodsInfos.get(0).get('goodsInfoId')
              : goodsInfo.get('goodsInfoId'),
            url
          )
        }
        //callBack:null
      });
    }
  }
  shareMain() {
    const {
      goodsInfo,
      init,
      url,
      goods,
      goodsInfos,
      changefenxiang
    } = this.props.relaxProps;
    const lineShowPrice = this._wholeSalePriceInfo(
      goods.get('linePrice'),
      goodsInfos
    );
    let saleType = goods.get('saleType');
    const isdistributor = WMkit.isShowDistributionButton();
    const inviteeId = isdistributor ? JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId : '';
    const inviteCode = isdistributor ? sessionStorage.getItem('inviteCode') : '';
    const newUrl = `goods-detail/${goodsInfo.get('goodsInfoId')}/?channel=mall&inviteeId=${inviteeId}&inviteCode=${inviteCode}`;
    if (WMkit.isLoginOrNotOpen()) {
      history.push({
        pathname: '/share',
        state: {
          goodsInfo,
          url: newUrl,
          lineDrawingPrice: lineShowPrice
        }
      });
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          init(
            saleType == 0
              ? goodsInfos.get(0).get('goodsInfoId')
              : goodsInfo.get('goodsInfoId'),
            url
          )
        }
        //callBack:null
      });
    }
  }

  _wholeSalePriceInfo = (linePrice, goodsInfos) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        // 已登录时,找出最高的市场价
        let maxMarketPrice = null;
        goodsInfos.forEach((info, index) => {
          if (index === 0) {
            maxMarketPrice = info.get('marketPrice');
          } else {
            maxMarketPrice =
              info.get('marketPrice') > maxMarketPrice
                ? info.get('marketPrice')
                : maxMarketPrice;
          }
        });
        return maxMarketPrice;
      } else {
        return null;
      }
    }
  };
}
