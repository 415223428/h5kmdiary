import React from 'react';
import moment from 'moment';
import { IMap, Relax } from 'plume2';
import { _, CountDown, noop } from 'wmkit';

@Relax
export default class GoodsRushBuy extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  props: {
    relaxProps?: {
      serverTime: string;
      reload: Function;
    };
    flashsaleGoods: IMap;
  };

  static relaxProps = {
    serverTime: 'serverTime',
    reload: noop
  };

  render() {
    const { serverTime, reload } = this.props.relaxProps;
    const { flashsaleGoods } = this.props;

    const stock: number = flashsaleGoods.get('stock');
    const volume: number = flashsaleGoods.get('salesVolume');
    const total: number = _.add(stock, volume);
    const num: number = _.div(volume, total);
    return (
      <div style={styles.common}>
        {/* <div style={styles.leftStyle}>
          <span style={styles.rushBuyNum}>
            已抢{_.mul(num, 100).toFixed(0)}%
          </span>
          <div style={styles.showInfo}>
            <div
              className={'progresss'}
              style={{ width: 110 * num, backgroundColor: '#FFEA17' }}
            />
          </div>
        </div> */}
        <div style={styles.goodTime}>
          <p>距离结束还剩</p>
          <CountDown
            visible={true}
            endHandle={reload}
            timeOffset={moment
              .duration(
                moment(flashsaleGoods.get('activityFullTime'))
                  .add(2, 'h')
                  .diff(serverTime)
              )
              .asSeconds()
              .toFixed(0)}
            colorStyle={{fontSize:15}}
          />
        </div>
      </div>
    );
  }
}

let styles = {
  common: {
    position: 'relative',
    height: 65,
    top: -36,
    backgroundColor: '#F91A53',
    marginBottom: -35
  },
  leftStyle: {
    position: 'absolute',
    top: 42
  },
  goodTime: {
    position: 'absolute',
    right: 10,
    top: 15,
    color: '#fff',
    fontSize: 12
  },
  pStyle: {
    paddingTop: 10,
    fontSize: '0.3rem',
    letterSpacing: '1px',
    textAlign: 'center'
  },
  showInfo: {
    position: 'absolute',
    left: 80,
    top: 6,
    width: 110,
    height: 5,
    backgroundColor: '#ebebeb',
    borderRadius: 5
  },
  progresss: {
    position: 'absolute',
    height: 5,
    left: 0,
    backgroundColor: '#FFEA17',
    borderRadius: 5
  },
  rushBuyNum: {
    paddingLeft: 10,
    paddingTop: 5,
    fontSize: 12,
    color: '#fff'
  }
} as any;
