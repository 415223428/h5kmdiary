import React from 'react';
import { Relax, IMap } from 'plume2';
import { Alert, noop } from 'wmkit';
import { marketOne } from '../kit';
//促销类型
const marketingType = ['减', '折', '赠','秒杀'];
//促销类型样式
const _TYPE = {
  '0': { text: '减', color: '' },
  '2': { text: '赠', color: 'red' },
  '1': { text: '折', color: 'orange' },
  '5': { text: '秒杀', color: 'orange' }
};

@Relax
export default class Promotion extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  props: {
    relaxProps?: {
      store: any;
      showModal: Function;
      goodsInfo: IMap;
      marketingDetail: Function;
    };
  };

  static relaxProps = {
    // 店铺信息
    store: 'store',
    showModal: noop,
    goodsInfo: 'goodsInfo',
    marketingDetail: noop
  };

  render() {
    const { showModal, goodsInfo, marketingDetail } = this.props.relaxProps;

    //有促销活动时,返回优先级最高的活动
    const marketing =
      goodsInfo.size > 0 && goodsInfo.get('marketingLabels').size > 0
        ? marketOne(goodsInfo)[0]
        : null;
    //满赠营销
    const giftMarketing = goodsInfo
      .get('marketingLabels')
      .find((marketing) => marketing.get('marketingType') == 2);

    if (marketing) {
      let type = marketing.marketingType;
      return (
        <div
          className="gec-box"
          onClick={() =>
            marketingDetail(
              giftMarketing ? giftMarketing.get('marketingId') : null
            )
          }
          style={{
            background:'#fff'
          }}
        >
          <div className="detail-gec">
            <span className="gec-title">促销</span>
            <div className="gec" style={{
              visibility:'hidden'
            }}>
              <div className={`tag ${_TYPE[type].color}`}>
                {marketingType[type]}
              </div>
              <span>{marketing.marketingDesc}</span>
            </div>
            <i className="iconfont icon-more" />
          </div>
        </div>
      );
    }

    return null;
  }
}
