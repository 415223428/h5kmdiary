import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import Container from './component/container';
const style = require('./css/style.css');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class Settings extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
    this.store.initSuperior();
  }

  render() {
    return (
      <div className="use-setting">
        <Container />
      </div>
    );
  }
}
