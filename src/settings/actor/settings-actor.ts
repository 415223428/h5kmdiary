import { Actor, Action } from 'plume2';

export default class SettingsActor extends Actor {
  defaultState() {
    return {
      // 用户信息
      customer: {},
      customersuperior:{},
    };
  }

  /**
   * 初始化
   */
  @Action('init')
  init(state, { customer }) {
    return state.set('customer', customer);
  }
  
  @Action('customersuperior:init')
  customersuperior(state, value) {
    return state.set('customersuperior', value)
  }
}
