import { Fetch } from 'wmkit';

/**
 * 查询会员信息
 */
export const fetchCustomerCenterInfo = () => {
  return Fetch('/customer/customerCenter');
};

export const fetchCustomersuperior = () => {
  return Fetch(`/customer/get-my-Superior`)
}