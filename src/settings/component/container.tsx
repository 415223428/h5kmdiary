import React, { Component } from 'react';
import { Relax } from 'plume2';
import { Button, history, WMkit } from 'wmkit';

const Long = Button.Long;
const logout = require('../img/logout.png')
@Relax
export default class Container extends Component<any, any> {
  props: {
    relaxProps?: {
      customer: any;
      customersuperior: any;
    };
  };

  static relaxProps = {
    customer: 'customer',
    customersuperior: 'customersuperior',
  };

  render() {
    const { customer, customersuperior } = this.props.relaxProps;
    let mobile = customer.get('customerAccount');
    let headImg = customer.get('headImg') ? customer.get('headImg') : require('../../../web_modules/images/default-headImg.png');
    if (mobile) {
      mobile = mobile.substr(0, 3) + '****' + mobile.substr(7);
    }

    return (
      <div>
        <div className="avatarBox b-1px-b" style={{ height: '1.5rem' }}>
          <div className="border-round">
            <img src={headImg} alt="" style={{ borderRadius: '50%' }} />
          </div>
          <div>
            <p className="use-name">{customer.get('customerName')}</p>
            <p className="use-num">{mobile}</p>
          </div>
          <div className="myOtherAccount">
            <div 
            style={{color: '#169BD5'}}
            onClick={()=>{
              history.push({
                pathname: '/fecth-user-id',
              })
            }}
            >
              我的其他商城账号 >
            </div>
            {
              customersuperior ?
                null
                :
                <div className="invitercode">
                  <img src={require('../img/inviter.png')}
                    style={{ width: "1.4rem" }}
                    onClick={() => {
                      history.push({
                        pathname: '/distribution-register',
                        state: { addInviteCode: true },
                      })
                    }}
                  ></img>
                </div>
            }

          </div>
        </div>
        <div
          className="setting-item b-1px-b"
          onClick={() => history.push('/user-info')}
        >
          <label>基础信息</label>
          <i className="iconfont icon-jiantou1" />
        </div>
        <div
          className="setting-item b-1px-b"
          onClick={() => history.push('/user-finance')}
        >
          <label>财务信息</label>
          <i className="iconfont icon-jiantou1" />
        </div>
        <div
          className="setting-item b-1px-b"
          onClick={() => history.push('/receive-address')}
        >
          <label>收货信息</label>
          <i className="iconfont icon-jiantou1" />
        </div>
        <div
          className="setting-item b-1px-b"
          onClick={() => history.push('/user-safe')}
        >
          <label>账户安全</label>
          <i className="iconfont icon-jiantou1" />
        </div>
        <div
          className="setting-item b-1px-tb"
          onClick={() => history.push('/about-us')}
        >
          <label>关于我们</label>
          <i className="iconfont icon-jiantou1" />
        </div>
        <div
          className="setting-item b-1px-tb"
        >
          <label>版本号 1.3.9 (20200314)</label>
        </div>

        <div className="bottom b-1px-t" style={{ padding: '0', height: '0.98rem' }}>
          {/* <Long 
            text="退出账号" 
            onClick={() => WMkit.logout()} 
            defaultStyle={{background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))',color:'#fff',fontSize:'0.32rem',border:'none'}}
          /> */}
          <img src={logout} alt="" style={{ width: '100%', display: 'block' }} onClick={() => WMkit.logout()} />
        </div>
      </div>
    );
  }
}
