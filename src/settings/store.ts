import { Store } from 'plume2';
import SettingsActor from './actor/settings-actor';
import * as webapi from './webapi';
import { fromJS } from 'immutable';
import { config } from 'config';

export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new SettingsActor()];
  }

  /**
   * 初始化
   */
  init = async () => {
    const res = await webapi.fetchCustomerCenterInfo();
    if (res.code == config.SUCCESS_CODE) {
      this.dispatch('init', {
        customer: fromJS(res.context)
      });
    }
  };

  initSuperior = async()=>{
    const res = await webapi.fetchCustomersuperior();
    if(res.code == 'K-000000'){
      this.dispatch('customersuperior:init',res.context)
    }
  }
}
