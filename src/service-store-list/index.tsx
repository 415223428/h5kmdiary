import React from 'react'
import { StoreProvider } from 'plume2'
import AppStore from './store'

import SearchBar from './component/search-bar'
import List from './component/list'
import Filter from './component/filter'
import './css/style.css'
import Search from '../shop-search'

@StoreProvider(AppStore, { debug: __DEV__ })
export default class StoreList extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    // 搜索关键字
    let queryString = ''

    // 店铺列表带搜索条件目前仅有一个来源：从搜索页面进行搜索
    // 解析url中的参数,形如 ?a=1&b=2&c=3
    let search = this.props.location.search;
    if (search) {
      search = search.substring(1, search.length);//截取后a=1&b=2&c=3
      let searchArray = search.split('&');
      searchArray.forEach(kv => {
        const split = kv.split('=');
        if (split[0] === 'q') {
          queryString = decodeURIComponent(split[1]) || ''
        }
      })
    }
    this.store.init(queryString)
  }

  render() {
    return (
      <div>
        <SearchBar />
        <List />
        {/* 筛选 */}
        <Filter />
        {false && <Search />}
      </div>
    )
  }
}