import React from 'react';
import { Relax } from 'plume2';
import SalesRankItem from './sales-rank-item';
import { IList } from 'typings/globalType';
import { fromJS } from 'immutable';
/**
 *
 */
@Relax
export default class SalesRankList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      rankList: IList;
      tab: string;
    };
  };

  static relaxProps = {
    rankList: 'rankList',
    tab: 'tab'
  };

  render() {
    const { rankList, tab } = this.props.relaxProps;
    return (
      <div className="content">
        {rankList.map((rankItme, i) => {
          return <SalesRankItem rankItme={rankItme} key={i} tab={tab} />;
        })}
      </div>
    );
  }
}
