import React from 'react';
import { Relax } from 'plume2';

import { history, noop } from 'wmkit';
import { render } from 'react-dom';
import * as WMkit from 'wmkit/kit';
import { Tips } from 'wmkit';

@Relax
export default class Content extends React.Component<any, any> {
  props: {
    relaxProps?: {
      tab: string;
      rang: string;
      isDistributor: boolean;
      changeTitle: Function;
    };
  };

  static relaxProps = {
    tab: 'tab',
    rang: 'rang',
    isDistributor: 'isDistributor',
    changeTitle: noop
  };

  render() {
    const { changeTitle, tab, isDistributor, rang } = this.props.relaxProps;
    return (
      <div className="scroll-fixed-box">
        <Tips
          styles="hint-box"
          text={`最近一周内的数据排行 ${rang}`}
        />
        <div className="tag-bar b-1px-b" style={{ padding: 0 }}>
          <ul className="tag-bar-list">
            <li
              className={`tag-bar-list-item ${
                tab == 'inviteCount' ? 'active' : ''
              }`}
              onClick={() => changeTitle('inviteCount')}
            >
              邀新人数
            </li>
            <li
              className={`tag-bar-list-item ${
                tab == 'inviteAvailableCount' ? 'active' : ''
              }`}
              onClick={() => changeTitle('inviteAvailableCount')}
            >
              有效邀新
            </li>
            {isDistributor && (
              <li
                className={`tag-bar-list-item ${
                  tab == 'saleAmount' ? 'active' : ''
                }`}
                onClick={() => changeTitle('saleAmount')}
              >
                销售额
              </li>
            )}
            {isDistributor && (
              <li
                className={`tag-bar-list-item ${
                  tab == 'commission' ? 'active' : ''
                }`}
                onClick={() => changeTitle('commission')}
              >
                预估收益
              </li>
            )}
          </ul>
        </div>
      </div>
    );
  }
}
