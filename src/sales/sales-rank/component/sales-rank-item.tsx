import React from 'react';
import { _ } from 'wmkit';

// 默认头像
const defaultImg = require('../img/default-img.png');
export default class SalesRankItem extends React.Component<any, any> {
  render() {
    const { rankItme, tab } = this.props;
    return (
      <div className="content">
        <div className={`sales-rank-item ${this._active(rankItme)} b-1px-b`}>
          <div className="sales-rank-box">
            <div className="sales-rank-box-img">
              <img src={this._img(rankItme)} alt=""/>
              <div className="sales-rank-box-bg">
                <span>{this._topRank(rankItme)}</span>
              </div>
            </div>
            <div className="sales-rank-box-msg">
              <span className="sales-rank-box-msg-name" style={{fontSize:'0.28rem',color:'#333',fontWeight:'bold'}}>
                {this._name(rankItme)}
              </span>
              <span className="sales-rank-box-msg-num" style={{fontSize:'0.26rem',color:'#333'}}>
                {tab =='inviteCount'?'邀请新人：':
                  tab == 'inviteAvailableCount'?'有效邀新：':
                    tab == 'saleAmount'?'销售额：':
                    tab == 'commission'?'预估收益：':' '}
                <span style={{color:'#FF4D4D'}}>{this._msgNum(rankItme, tab)}</span>
              </span>
            </div>
          </div>
          <div className="sales-rank-num" style={{color:'#FF4D4D',fontSize:'0.28rem'}}>{rankItme.get('ranking')}</div>
        </div>
      </div>
    );
  }

  /**
   * 一二三名
   */
  _active = (rankItme) => {
    let text = '';
    if (rankItme.get('ranking') < 4) {
      text = 'active';
    }
    return `${text}`;
  };

  /**
   * 一二三名
   */
  _imgRank = (rankItme) => {
    let text = '';
    if (rankItme.get('ranking') < 4) {
      text = defaultImg;
    }
    return `${text}`;
  };
  /**
   * 一二三名
   */
  _topRank = (rankItme) => {
    let text = '';
    if (rankItme.get('ranking') < 4) {
      text = rankItme.get('ranking');
    }
    return `${text}`;
  };
  /**
   * 头像
   */
  _img = (rankItme) => {
    let text = '';
    if (!rankItme.get('img')) {
      text = defaultImg;
    } else {
      text = rankItme.get('img');
    }
    return `${text}`;
  };

  /**
   * 名称
   */
  _name = (rankItme) => {
    let text = '';
    if (!rankItme.get('name')) {
      text = '用户';
    } else {
      text = rankItme.get('name');
    }
    // console.log(text.search('^[0-9]*$'),'11111111111');
    
    if(/^[0-9]*$/.test(text)){
      return `${text.substring(0,3)}****${text.substring(7)}`;
    }else{
      return `${text}`;
    }
  };
  /**
   * 排名信息
   */
  _msgNum = (rankItme, tab) => {
    let text = '';
    // 邀新人数
    if (tab == 'inviteCount') {
      text = rankItme.get('inviteCount');
    }
    // 有效邀新
    if (tab == 'inviteAvailableCount') {
      text =  rankItme.get('inviteAvailableCount');
    }
    //销售额
    if (tab == 'saleAmount') {
      text =_.addZero(rankItme.get('saleAmount'));
    }
    // 预估收益
    if (tab == 'commission') {
      text = _.addZero(rankItme.get('commission'));
    }

    return `${text}`;
  };
}
