import React from 'react';
import { Relax, IMap } from 'plume2';
import SalesRankItem from './sales-rank-item';
// 默认头像
const defaultImg = require('../img/default-img.png');
/**
 *
 */
@Relax
export default class SalesRankList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      myRank: IMap;
    };
  };

  static relaxProps = {
    myRank: 'myRank'
  };

  render() {
    const { myRank } = this.props.relaxProps;
    return (
      <div>
        <div className="sales-title-box b-1px-b" style={{background:'#FAFAFA',padding:'0.3rem 0.2rem',fontSize:'0.28rem'}}>
          <span>我的排名</span>
        </div>
        <div
          className="sales-row sales-rank-item b-1px-b"
          style={{ marginBottom: '10px' }}
        >
          <div className="sales-rank-box">
            <div className="sales-rank-box-img">
              <img src={this._img(myRank)} alt="" />
            </div>
            <div className="sales-rank-box-msg">
              <span className="sales-rank-box-msg-name" style={{fontSize:'0.28rem',color:'#333',fontWeight:'bold'}}>
                {this._name(myRank)}
              </span>
            </div>
          </div>
          <div className="sales-rank-num">{this._topRank(myRank)}</div>
        </div>
      </div>
    );
  }

  /**
   * 99+
   */
  _topRank = (rankItme) => {
    let text = rankItme.get('ranking');
    if (!rankItme.get('ranking')) {
      text = '0';
    }
    if (rankItme.get('ranking') > 99) {
      text = '99+';
    }
    return `${text}`;
  };

  /**
   * 头像
   */
  _img = (rankItme) => {
    let text = '';
    if (!rankItme.get('img')) {
      text = defaultImg;
    } else {
      text = rankItme.get('img');
    }
    return `${text}`;
  };

  /**
   * 名称
   */
  _name = (rankItme) => {
    let text = '';
    if (!rankItme.get('name')) {
      text = '用户';
    } else {
      text = rankItme.get('name');
    }
    return `${text}`;
  };
}
