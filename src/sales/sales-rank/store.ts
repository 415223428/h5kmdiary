import { Store, IOptions } from 'plume2';
import { fromJS } from 'immutable';
import { cache } from 'config';
import SalesRankActor from './actor/sales-rank-actor';
import { WMkit } from 'wmkit';
import moment from 'moment';
import { Const } from 'config';
import * as webapi from './webapi';
import { config } from 'config';

export default class AppStore extends Store {
  bindActor() {
    return [new SalesRankActor()];
  }

  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  init = () => {
    let customerInfo = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
    //是否为分销员
    let isDistributor =
      customerInfo && customerInfo.customerDetail.isDistributor;
    if (isDistributor) {
      // 当前用户是分销员
      this.dispatch('tab:isDistributor', true);
    }
    // 最近一周
    let start = moment()
      .subtract(7, 'days')
      .format(Const.DATE_FORMAT);
    let end = moment()
      .subtract(1, 'days')
      .format(Const.DATE_FORMAT);
    this.dispatch('tab:setRang', `${start}~${end}`);

    this.changeTitle('inviteCount');
  };

  /**
   * 获取排行
   * @param res
   */
  fetchRanking = async () => {
    const res = (await webapi.ranking(this.state().get('tab'))) as any;
    if (res.context) {
      this.dispatch('tab:setRanks', fromJS(res.context.rankingVOList));
      this.dispatch(
        'tab:setMyRank',
        fromJS(res.context.distributionCustomerRankingVO)
      );
    }
  };

  /**
   * 切换头部tab
   */
  changeTitle = (id) => {
    this.dispatch('tab:change', id);
    this.fetchRanking();
  };
}
