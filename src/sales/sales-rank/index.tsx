import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';

import SalesRankList from './component/sales-rank-list';
import MyRank from './component/sales-rank-my-rank';
import Tab from './component/tab';
require('./css/style.css')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class SalesRank extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    return (
      <div className="content sales">
        <Tab />
        <div className="sales-list-box">
          <MyRank />
          <SalesRankList />
        </div>
      </div>
    );
  }
}
