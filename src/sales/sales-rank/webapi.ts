import { Fetch, WMkit } from 'wmkit';

/**
 * 查询排行榜
 */
export const ranking = (type) => {
  return Fetch('/distributeRanking/sales/ranking', {
    method: 'POST',
    body: JSON.stringify({
      type: type
    })
  });
};
