import { Action, Actor, IMap } from 'plume2';
import { fromJS, List } from 'immutable';
export default class SalesRankActor extends Actor {
  defaultState() {
    return {
      // 选择的标题inviteCount ：邀新人数 inviteAvailableCount：有效邀新 saleAmount：销售额 commission：预估收益
      tab: 'inviteCount',
      // 是否分销员
      isDistributor: false,
      // 日期范围
      rang: '',
      // 我的排行
      myRank: {},
      // 排行列表
      rankList: []
    };
  }

  /**
   * 当前用户是否是分销员
   * @param state
   * @param isDistributor
   */
  @Action('tab:isDistributor')
  isDistributor(state, isDistributor) {
    return state.set('isDistributor', isDistributor);
  }

  /**
   * 切换tab
   * @param state
   * @param id
   */
  @Action('tab:change')
  changeTab(state, id) {
    return state.set('tab', id);
  }
  /**
   * 时间范围
   * @param state
   * @param id
   */
  @Action('tab:setRang')
  setRang(state, rang) {
    return state.set('rang', rang);
  }
  /**
   * 排行列表
   * @param {IMap} state
   * @param {Array<any>} params
   * @returns
   * {any}
   */
  @Action('tab:setRanks')
  setRanks(state: IMap, params) {
    return state.set('rankList', params);
  }

  /**
   * 我的排行
   * @param state
   * @param totalNum
   */
  @Action('tab:setMyRank')
  myRank(state: IMap, res) {
    return state.set('myRank', res);
  }
}
