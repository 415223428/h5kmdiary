import { Fetch } from 'wmkit'
type TResult = { code: string; message: string ; context: any}


/**
 * 获取该会员作为分销员信息
 */
export const fetchDistributorInfo = () => {
  return Fetch<TResult>('/distribute/distributor-info');
};

/**
 * 获取分销商品素材列表
 * @param filterParams
 */
export function distributionPerformanceDay(filterParams = {}) {
  return Fetch<TResult>('/distribution/performance/day', {
    method: 'POST',
    body: JSON.stringify({
      ...filterParams
    })
  });
}

/**
 * 获取分销商品素材列表
 * @param filterParams
 */
export function distributionPerformanceMonth(filterParams = {}) {
  return Fetch<TResult>('/distribution/performance/month', {
    method: 'POST',
    body: JSON.stringify({
      ...filterParams
    })
  });
}

/**
 * 查询分销设置
 * @param filterParams
 */
export function distributionSetting(filterParams = {}) {
  return Fetch<TResult>('/distribution-setting');
}