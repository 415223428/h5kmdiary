import { Store, IOptions } from 'plume2';

import SalesPerformActor from './actor/sales-perform-actor';
import { WMkit } from 'wmkit';
import * as webapi from './webapi';
import { config } from 'config';
import { fromJS } from 'immutable';

export default class AppStore extends Store {
  bindActor() {
    return [new SalesPerformActor()];
  }

  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  init = async () => {
    if (await !WMkit.isDistributor()) {
      // 当前用户不是分销员
      return;
    }
    this.dispatch('init: data', {dataList:[]});
    this.dispatch('init: length',1);
    //查询分销员信息
    const distributorInfo = await webapi.fetchDistributorInfo() as any;
    if (distributorInfo.code == config.SUCCESS_CODE) {
      //分销员销售业绩
      this.dispatch('init:distributionId', distributorInfo.context.distributionCustomerVO
        .distributionId as any);
    }
    const dayOrMonthFlag = this.state().get('dayOrMonthFlag');
    if (!dayOrMonthFlag) {
      //日销售统计
      //设置日销售统计条件
      const query = this.state()
        .get('dayForm')
        .toJS();
      query.distributionId = this.state().get('distributionId');
      const res = (await webapi.distributionPerformanceDay({
        ...query
      })) as any;

      if (res.code == config.SUCCESS_CODE) {
        this.dispatch('init: data', res.context);
        this.dispatch('init: length', res.context.dataList.length);
      }
      //初始化自然月数据
      this.getMonthData();
    } else {
      const query = this.state()
        .get('monthForm')
        .toJS();
      query.distributionId = this.state().get('distributionId');
      const res = (await webapi.distributionPerformanceMonth({
        ...query
      })) as any;

      if (res.code == config.SUCCESS_CODE) {
        this.dispatch('init: data', res.context);
        this.dispatch('init: length', res.context.dataList.length);
      }
    }

  };

  /**
   * 改变日查询条件
   * @param {any} key
   * @param {any} value
   */
  dayFormFiledChange = ({ key, value }) => {
    this.dispatch('day:form: field', { key, value });
  };

  /**
   * 改变月查询条件
   * @param {any} key
   * @param {any} value
   */
  monthFormFiledChange = ({ key, value }) => {
    this.dispatch('month:form: field', { key, value });
  };

  /**
   * 得到自然月的时间
   */
  getMonthData = () => {
    let date = new Date();
    let monthData = new Array();
    for (let i = 0; i < 6; i++) {
      date.setMonth(date.getMonth() - 1, 1);
      monthData.push(this._formateDate(date));
    }

    this.dispatch('salesPerform:getMonthData', monthData);
  };

  //格式化日期
  _formateDate(date) {
    const dateMap = {} as any
    if (date instanceof Date) {
      dateMap.value = date.getFullYear() + '年' + (date.getMonth() + 1) + '月';
      dateMap.year = date.getFullYear();
      dateMap.month = date.getMonth() + 1;
      return dateMap
    }
  }

  changeOpenLayer = async (isOpenLayer) => {
    this.dispatch('salesPerform:changeOpenLayer', isOpenLayer);
    //查询分销业绩规则说明
    const setting = await webapi.distributionSetting() as any;
    this.dispatch('init: performanceDesc', setting.context.distributionSettingSimVO.performanceDesc);
  };

  /**
   * 改变选择月份下拉框是否显示
   */
  changeShowTagList = (isShowTagList) => {
    this.dispatch('salesPerform:changeShowTagList', isShowTagList);
  };

  /**
   * 改变销售业绩展示方式
   */
  changeDayOrMonthFlag = () => {
    this.dispatch('salesPerform:changeDayOrMonthFlag');
    const dayOrMonthFlag = this.state().get('dayOrMonthFlag');
    if (!dayOrMonthFlag){
      //日维度统计
      this.changeChoiceTab('1');
    }
    this.changeShowTagList(false);
    this.init();
  };

  /**
   * 改变日销售业绩展示方式
   */
  changeChoiceTab = (choiceTabFlag) => {
    if (choiceTabFlag == '1') {
      this.dispatch('salesPerform:changeMonth', '自然月');
      this.dayFormFiledChange({
        key: 'dateCycleEnum',
        value: 0
      });
    } else if (choiceTabFlag == '2') {
      this.dispatch('salesPerform:changeMonth', '自然月');
      this.dayFormFiledChange({
        key: 'dateCycleEnum',
        value: 1
      });
    } else {
      const monthStr = this.state().get('monthStr');
      const year = monthStr.toString().substring()
      this.dayFormFiledChange({
        key: 'dateCycleEnum',
        value: 2
      });

    }
    this.dispatch('salesPerform:changeChoiceTab', choiceTabFlag);
    this.changeShowTagList(false);
    this.init();
  };

  /**
   * 改变选择的自然月
   * @param monthStr
   */
  changeMonth = (monthData) => {
    this.dispatch('salesPerform:changeMonth', monthData.get('value'));
    this.dayFormFiledChange({
      key: 'year',
      value: monthData.get('year')
    });
    this.dayFormFiledChange({
      key: 'month',
      value: monthData.get('month')
    });
    this.changeChoiceTab('3');
  };
}
