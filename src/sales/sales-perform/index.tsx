import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import { history } from 'wmkit';
import SalesTagBar from './component/sales-tag-bar';
import SalesTagBarList from './component/sales-tag-bar-list';
import SalesTable from './component/sales-table';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class SalesPerform extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    let isShowTagList = this.store.state().get('isShowTagList');
    let dayOrMonthFlag = this.store.state().get('dayOrMonthFlag');
    return (
      <div className="content sales">
        <div style={{ height: !dayOrMonthFlag ? '2.4rem' : '1.6rem' }}>
          <div className="scroll-fixed-box">
            <div className="sales-explain">
              <span>
                <span style={{ marginRight: '.1rem' }}>
                  业绩统计规则详见说明
                </span>
                <i
                  className="iconfont icon-help"
                  onClick={() => this.store.changeOpenLayer(true)}
                />
              </span>
              <span
                className="sales-rank"
                onClick={() => history.push('/sales-rank')}
              >
                排行榜
              </span>
            </div>
            <SalesTagBar />
            {isShowTagList && <SalesTagBarList />}
          </div>
        </div>

        <SalesTable />
      </div>
    );
  }
}
