import React from 'react';
import { Relax } from 'plume2';
import { noop, history } from 'wmkit';
import SalesConfirm from './sales-confirm';

/**
 *
 */
@Relax
export default class SalesTagBar extends React.Component<any, any> {
  props: {
    relaxProps?: {
      isOpenLayer: Boolean;
      changeOpenLayer: Function;
      changeShowTagList: Function;
      dayOrMonthFlag: Boolean;
      changeDayOrMonthFlag: Function;
      choiceTabFlag: string;
      changeChoiceTab: Function;
      monthStr: string;
      isShowTagList: Boolean;
      performanceDesc: string;
    };
  };

  static relaxProps = {
    isOpenLayer: 'isOpenLayer',
    changeOpenLayer: noop,
    changeShowTagList: noop,
    dayOrMonthFlag: 'dayOrMonthFlag',
    changeDayOrMonthFlag: noop,
    choiceTabFlag: 'choiceTabFlag',
    changeChoiceTab: noop,
    monthStr: 'monthStr',
    isShowTagList: 'isShowTagList',
    performanceDesc: 'performanceDesc'
  };

  render() {
    const {
      changeOpenLayer,
      isOpenLayer,
      changeShowTagList,
      dayOrMonthFlag,
      changeDayOrMonthFlag,
      choiceTabFlag,
      changeChoiceTab,
      monthStr,
      isShowTagList,
      performanceDesc
    } = this.props.relaxProps;
    return (
      <div className="content">
        <div className="tag-bar b-1px-t">
          <div className="tag-bar-list">
            {dayOrMonthFlag ? (
              <div
                className="tag-bar-list-item"
                onClick={() => changeDayOrMonthFlag()}
              >
                日销售业绩
              </div>
            ) : (
              <div className="tag-bar-list-item active">日销售业绩</div>
            )}
            {dayOrMonthFlag ? (
              <div className="tag-bar-list-item active">月销售业绩</div>
            ) : (
              <div
                className="tag-bar-list-item"
                onClick={() => changeDayOrMonthFlag()}
              >
                月销售业绩
              </div>
            )}
          </div>
        </div>
        {!dayOrMonthFlag && (
          <div className="tag-bar half-tag-bar b-1px-t">
            <div className="tag-bar-list">
              {choiceTabFlag == '1' ? (
                <div className="tag-bar-list-item active">最近7天</div>
              ) : (
                <div
                  className="tag-bar-list-item"
                  onClick={() => changeChoiceTab('1')}
                >
                  最近7天
                </div>
              )}

              {choiceTabFlag == '2' ? (
                <div className="tag-bar-list-item active">最近30天</div>
              ) : (
                <div
                  className="tag-bar-list-item"
                  onClick={() => changeChoiceTab('2')}
                >
                  最近30天
                </div>
              )}
            </div>
            <div className="tag-nav-box">
              {choiceTabFlag == '3' ? (
                <a
                  className="tag-border active"
                  href="javascript:;"
                  onClick={() => changeShowTagList(!isShowTagList)}
                >
                  {monthStr}
                </a>
              ) : (
                <a
                  className="tag-border"
                  href="javascript:;"
                  onClick={() => changeShowTagList(!isShowTagList)}
                >
                  {monthStr}
                </a>
              )}
            </div>
          </div>
        )}

        <SalesConfirm
          visible={isOpenLayer}
          content={
            <div
              dangerouslySetInnerHTML={{
                __html: performanceDesc
              }}
            />
          }
          handleClose={() => changeOpenLayer(false)}
        />
      </div>
    );
  }
}
