import React from 'react';
import { Relax, IMap } from 'plume2';
import { _ } from 'wmkit';
import { fromJS } from 'immutable';
import Empty from './sales-empty';

/**
 *
 */
@Relax
export default class SalesListItem extends React.Component<any, any> {
  props: {
    relaxProps?: {
      data: IMap;
      dayOrMonthFlag: Boolean;
      length: number;
    };
  };

  static relaxProps = {
    data: 'data',
    dayOrMonthFlag: 'dayOrMonthFlag',
    length: 'length',
  };

  render() {
    const { data, dayOrMonthFlag,length } = this.props.relaxProps;
    const dataList = data && data.get('dataList') ? data.get('dataList') : [];
    return (
      <div className="content">
        {dataList && length > 0 ? (
          dataList.map((v, index) => {
            return (
              <div className="sales-list-item b-1px-b">
                <div
                  className="sales-list-item-item"
                  style={{ textAlign: 'left' }}
                >
                  {dayOrMonthFlag ? v.get('targetMonth') : v.get('targetDate')}
                </div>
                <div className="sales-list-item-item active">
                  ￥{_.addZero(v.get('saleAmount'))}
                </div>
                <div className="sales-list-item-item active">
                  ￥{_.addZero(v.get('commission'))}
                </div>
              </div>
            );
          })
        ) : (
          <Empty />
        )}
      </div>
    );
  }
}
