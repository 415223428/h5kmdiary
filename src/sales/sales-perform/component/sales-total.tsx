import React from 'react';
import { IMap, Relax } from 'plume2';
import {_} from 'wmkit';

/**
 *
 */
@Relax
export default class SalesTotal extends React.Component<any, any> {
  props: {
    relaxProps?: {
      data: IMap;
    };
  };

  static relaxProps = {
    data: 'data'
  };

  render() {
    const { data } = this.props.relaxProps;
    const totalSaleAmount = data ? data.get('totalSaleAmount') : 0.00;
    const totalCommission = data ? data.get('totalCommission') : 0.00;
    return (
      <div className="sales-total b-1px-t b-1px-b">
        <div className="sales-total-item" style={{ textAlign: 'left' }}>
          <span className="sales-total-title">&nbsp;</span>
          <span className="sales-total-label">合计业绩</span>
        </div>
        <div className="sales-total-item">
          <span className="sales-total-title">销售额</span>
          <span className="sales-total-num">￥{_.addZero(totalSaleAmount)}</span>
        </div>
        <div className="sales-total-item">
          <span className="sales-total-title">预估收益</span>
          <span className="sales-total-num">￥{_.addZero(totalCommission)}</span>
        </div>
      </div>
    );
  }
}
