import React, { Component } from 'react';
import { WMImage } from 'wmkit';

export default class ShipEmpty extends Component<any, any> {
  render() {
    return (
      <div className="list-none">
        <WMImage
          src={require('../img/none.png')}
          width="200px"
          height="200px"
        />
        <div>
          <p>暂无销售业绩</p>
        </div>
      </div>
    );
  }
}
