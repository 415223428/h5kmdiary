import React from 'react';

export default class SalesConfirm extends React.Component<any, any> {
  static defaultProps = {
    visible: false,
    content: '',
    handleClose: Function
  };
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        {this.props.visible ? (
          <div className="normal-confirm">
            <div className="normal-confirm-content">
              <div className="normal-confirm-content-body">
                {this.props.content}
              </div>
              <div
                className="normal-confirm-content-close"
                onClick={() => this.props.handleClose()}
              >
                <i className="iconfont icon-Close" />
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
