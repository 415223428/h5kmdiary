import React from 'react';
import { Relax } from 'plume2';
import SalesTotal from './sales-total';
import SalesListItem from './sales-list-item';

/**
 *
 */
@Relax
export default class SalesTable extends React.Component<any, any> {
  props: {
    relaxProps?: {};
  };

  static relaxProps = {};

  render() {
    return (
      <div className="content sales-table">
        <SalesTotal />
        <SalesListItem />
      </div>
    );
  }
}
