import React from 'react';
import { Relax } from 'plume2';
import { noop, history } from 'wmkit';
import SalesConfirm from './sales-confirm';
import { IList } from 'typings/globalType';

/**
 *
 */
@Relax
export default class SalesTagBarList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      monthData:IList;
      changeMonth:Function;
    };
  };

  static relaxProps = {
    monthData:'monthData',
    changeMonth:noop,
  };

  render() {
    const {
      monthData,
      changeMonth,
    } = this.props.relaxProps;
    return (
      <div className="tag-list">
        {monthData.map((v,index) =>{
          return <a className="tag-list-item" href="javascript:;" onClick={() => changeMonth(v)}>
              {v.get('value')}
            </a>;
        })}
      </div>
    );
  }
}
