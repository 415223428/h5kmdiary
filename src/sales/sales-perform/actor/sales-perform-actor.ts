import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class SalesPerformActor extends Actor {
  defaultState() {
    return {
      isOpenLayer: false,
      isShowTagList: false,
      dayOrMonthFlag: false, //选择日或者月销售业绩 false:日 true：月
      choiceTabFlag: '1', //日销售业绩下tab选择 1:最近7天 2：最近30天,
      monthData: [], //自然月下拉数据
      monthStr: '自然月',
      dayForm: {
        dateCycleEnum: 0
      }, //日查询条件
      monthForm: {}, //月查询条件
      distributionId: '', //登录的分销员id
      data: {
        // dataList:[]
      }, //展示的数据
      performanceDesc: '' ,//分销业绩规则说明
      length:1,
    };
  }

  @Action('salesPerform:changeOpenLayer')
  changeOpenLayer(state, isOpenLayer) {
    return state.set('isOpenLayer', isOpenLayer);
  }

  /**
   * 改变选择月份下拉框是否显示
   * @param state
   * @returns {any}
   */
  @Action('salesPerform:changeShowTagList')
  changeShowTagList(state, isShowTagList) {
    return state.set('isShowTagList', isShowTagList);
  }

  /**
   * 改变销售业绩展示方式
   *
   * @param state
   * @returns {any}
   */
  @Action('salesPerform:changeDayOrMonthFlag')
  changeDayOrMonthFlag(state) {
    return state.set('dayOrMonthFlag', !state.get('dayOrMonthFlag'));
  }

  /**
   * 改变日销售业绩展示方式
   *
   * @param state
   * @returns {any}
   */
  @Action('salesPerform:changeChoiceTab')
  changeChoiceTab(state, choiceTabFlag) {
    return state.set('choiceTabFlag', choiceTabFlag);
  }

  /**
   * 改变日销售业绩展示方式
   *
   * @param state
   * @returns {any}
   */
  @Action('salesPerform:getMonthData')
  getMonthData(state, monthData) {
    return state.set('monthData', fromJS(monthData));
  }

  /**
   * 改变选择的自然月
   *
   * @param state
   * @returns {any}
   */
  @Action('salesPerform:changeMonth')
  changeMonth(state, monthStr) {
    return state.set('monthStr', monthStr);
  }

  /**
   * 设置日条件
   * @param state
   * @param {any} key
   * @param {any} value
   * @returns {any}
   */
  @Action('day:form: field')
  dayFormFiledChange(state, { key, value }) {
    return state.setIn(['dayForm', key], value);
  }

  /**
   * 设置月条件
   * @param state
   * @param {any} key
   * @param {any} value
   * @returns {any}
   */
  @Action('month:form: field')
  monthFormFiledChange(state, { key, value }) {
    return state.setIn(['monthForm', key], value);
  }

  /**
   * 设置分销员Id
   * @param state
   * @param {any} key
   * @param {any} value
   * @returns {any}
   */
  @Action('init:distributionId')
  initDistributionId(state, distributionId) {
    return state.set('distributionId', distributionId);
  }

  /**
   * 设置展示数据
   * @param state
   * @param {any} key
   * @param {any} value
   * @returns {any}
   */
  @Action('init: data')
  initData(state, data) {
    return state.set('data', fromJS(data));
  }

  /**
   * 设置展示数据
   * @param state
   * @param {any} key
   * @param {any} value
   * @returns {any}
   */
  @Action('init: length')
  initLength(state, length) {
    return state.set('length', length);
  }

  /**
   * 设置分销业绩规则说明
   * @param state
   * @param {any} key
   * @param {any} value
   * @returns {any}
   */
  @Action('init: performanceDesc')
  initPerformanceDesc(state, performanceDesc) {
    return state.set('performanceDesc', performanceDesc);
  }
}
