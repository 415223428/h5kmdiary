/**
 * Created by feitingting on 2017/7/13.
 */
import { Store } from 'plume2';
import { _, Alert, history, WMkit } from 'wmkit';
import { cache, config } from 'config';
import RegisterActor from './actor/register-actor';
import * as webapi from './webapi';
import { isNullOrUndefined } from 'util';
import { getShareUserId } from 'biz';

export default class AppStore extends Store {
  bindActor() {
    return [new RegisterActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  /**
   * 初始化UUID，获取图形验证码
   * @param id
   */
  init = async (id, employeeId) => {
    const res = await Promise.all([webapi.fetchBaseConfig()]);
    if (res[0].code == config.SUCCESS_CODE) {
      this.dispatch('register:init', JSON.parse((res[0].context as any).pcLogo));
    }
    this.dispatch('register:uuid', id);
    if (!isNullOrUndefined(employeeId)) {
      this.dispatch('register:employeeId', employeeId);
    }
    this.state().get('employeeId');
    // this.patchca();
    if (!isNullOrUndefined(WMkit.inviteeId())) {
      console.log('登录注册人的ID');
      // 查询邀请人姓名
      const { code, context } = await webapi.getCustomerInfo(
        WMkit.inviteeId()
      );
      if (code == config.SUCCESS_CODE) {
        if (context) {
          //获取邀请人信息
          this.dispatch(
            'change:inviteeCustomerName',
            (context as any).customerName
          );
        }
      }
    }
  };

  patchca = async () => {
    const uuid = this.state().get('UUID');
    const res = await webapi.patchca(uuid);
    if (res.code == 'K-000000') {
      this.dispatch('register:image', (res as any).context);
    }
  };

  showPwd = () => {
    const showpass = this.state().get('isShowpwd');
    this.dispatch('register:showPass', showpass);
  };

  getMobile = (tel: string) => {
    this.dispatch('register:mobile', tel);
  };

  getPassword = (pass: string) => {
    this.dispatch('register:pass', pass);
  };

  getCode = (code: string) => {
    console.log('getCode');
    
    this.dispatch('register:code', code);
  };

  sendCode = (tel: string) => {
    //加入图片验证码和UUID两个参数
    const uuid = this.state().get('UUID');
    const patcha = this.state().get('patcha');
    return webapi.sendCode(tel, uuid, patcha).then((res) => {
      const { code, message } = res;
      if (code == 'K-000000') {
        Alert({
          text: '发送验证码成功！'
        });
      } else {
        Alert({
          text: message
        });
        return Promise.reject(message);
      }
    });
  };

  doRegister = async () => {
    console.log('doRegister');
    
    /**
     * 参数准备*/
    const mobile = this.state().get('mobile');
    const sendCode = this.state().get('code');
    const password = this.state().get('password');
    const patcha = this.state().get('patcha');
    const uuid = this.state().get('UUID');
    const employeeId = this.state().get('employeeId');
    const shareUserId = getShareUserId();

    if (
      WMkit.testTel(mobile) &&
      WMkit.testPass(password) &&
      WMkit.testVerificationCode(sendCode)
    ) {
      const fromPage = 0;
      const { code, message } = await webapi.checkRegister(mobile, sendCode,fromPage);
      if (code == config.SUCCESS_CODE) {
        const register_info = {
          customerAccount: mobile,
          customerPassword: password,
          verifyCode: sendCode,
          patchca: patcha,
          uuid: uuid,
          employeeId: employeeId,
          shareUserId: shareUserId,
          fromPage: fromPage //0:注册页面，1：注册弹窗
        };
        sessionStorage.setItem(
          cache.REGISTER_INFO,
          JSON.stringify(register_info)
        );
        history.push(`/distribution-register`);
      } else {
        Alert({
          text: message
        });
      }
    } else {
      return false;
    }
  };

  getPatcha = (value) => {
    this.dispatch('register:patcha', value);
  };
}
