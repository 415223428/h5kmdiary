import React from 'react'
import { StoreProvider } from 'plume2'
import { Button,WMkit } from 'wmkit'
import AppStore from './store'
import FormItem from './component/form-item'
const styles = require('./css/style.css');

const logo = require('./img/logo.png');
const logo1 = require('./img/logo1.png');

const LongBlueButton = Button.LongBlue
const UUID = require('uuid-js');
@StoreProvider(AppStore, { debug: __DEV__ })
export default class Register extends React.Component<any, any> {
  store: AppStore

  componentWillMount() {
    document.title = "注册"
    this.store.init(UUID.create().toString(), this.props.match.params.employeeId)
  }


  render() {
    const pcLogo = this.store.state().toJS().pcLogo;
    
    return (
      <div className="content register-content">
        <div className="logo">
          <img
          //  src={pcLogo && pcLogo.length >= 1 ? pcLogo[0].url : logo}
          src={logo1}
           className="logoImg"
          />
        </div>

        <FormItem />
        {/* <div className="register-btn">
          <LongBlueButton text="注册" onClick={() => this.store.doRegister()}/>
        </div> */}
        <div className="footer-copyright">
          <p>粤ICP备19120716号-1</p>
        </div>
      </div>
    )
  }
}
