/**
 * Created by feitingting on 2017/7/13.
 */
import {Fetch} from 'wmkit'

/**
 * 发送验证码
 * @param tel
 * @returns {Promise<Result<T>>}
 */
export const sendCode = (tel: string,uuid:string,patchca:string) => {
  return Fetch('/checkSmsByRegister', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: tel,
      patchca:patchca,
      uuid:uuid
    })
  })
}

/**
 * 获取logo
 * @returns {Promise<Result<T>>}
 */
export const fetchBaseConfig = () => {
  return Fetch('/system/baseConfig', {
    method: 'GET'
  });
};


/**
 * 登录系统
 * @param mobile
 * @param code
 * @param password
 * @returns {Promise<Result<T>>}
 */
export const register = (mobile: string, code: string, password: string,patchca:string,uuid:string,employeeId:string,shareUserId:string) => {
  return Fetch('/register', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: mobile, customerPassword: password,
      verifyCode: code,patchca:patchca,uuid:uuid,employeeId:employeeId,shareUserId:shareUserId
    })
  })
}

/**
 * 根据客户id查询客户信息
 * @param {string} customerId
 * @returns {Promise<Result<any>>}
 */
export const getCustomerInfo = (customerId: string) => {
  return Fetch(`/customer/customerInfoById/${customerId}`, {
    method: 'GET'
  });
};

/**
 * 根据UUID去获取图形验证码
 * @param id
 * @returns {Promise<Result<T>>}
 */
export  const  patchca=(id:string)=>{
  return Fetch(`/patchca/${id}`, {
    method: 'GET',
  })
}

/**
 * 注册验证
 * @param {string} mobile
 * @param {string} code
 * @returns {Promise<Result<any>>}
 */
export const checkRegister = (mobile: string, code: string,fromPage:number) => {
    return Fetch('/register/check', {
        method: 'POST',
        body: JSON.stringify({
            customerAccount: mobile,
            verifyCode: code,
            fromPage:fromPage
        })
    })
}
