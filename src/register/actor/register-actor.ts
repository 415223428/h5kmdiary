/**
 * Created by feitingting on 2017/7/13.
 */
import { Action, Actor } from 'plume2';
import { IList } from '../../../typings/globalType';


export  default class RegisterActor extends Actor {

  defaultState() {
    return {
      pcLogo: [],
      mobile: "",     //手机号
      code: "",       //验证码
      password: "",   //密码
      mobileChange: true, //监听手机号码的变化
      passChange: true,   //监听密码变化
      codeChange: true,   //监听验证码变化
      buttonValue: "发送验证码", //按钮值
      isShowpwd: false,
      image:"",  //图形验证码url
      UUID:"",//UUID
      patcha:"",//图像验证码的值,
      employeeId:'', //业务员id,
      customerName:''
    }
  }

/**
   * logo
   * @param state
   * @param config
   */
  @Action('register:init')
  init(state, config: IList) {
    return state
      .set('pcLogo', config)
  }

  @Action('change:inviteeCustomerName')
  customerName(state, customerName) {
    return state
      .set('customerName', customerName)
  }

  /**
   * 是否显示密码
   * @param state
   * @param show
   */
  @Action('register:showPass')
  showPass(state, show: boolean) {
    return state.set('isShowpwd', !show)
  }


  /**
   * 监听手机号码状态值
   * @param state
   * @param tel
   */
  @Action('register:mobile')
  getMobile(state, tel: string) {
    return state.set('mobile', tel)
  }


  /**
   * 开启倒计时
   * @param state
   * @param start
   */
  @Action('register:setTime')
  setTime(state, start: number) {
    return state.set('buttonValue', start)
  }


  /**
   * 递归
   * @param state
   * @param value
   */
  @Action('register:time')
  time(state, value: number) {
    return state.set('buttonValue', value - 1)
  }


  /**
   * 监听密码状态值
   * @param state
   * @param value
   */
  @Action('register:pass')
  getPass(state, value: string) {
    return state.set('password', value)
  }


  /**
   * 监听验证码的状态值
   * @param state
   * @param value
   */
  @Action('register:code')
  getCode(state, value: string) {
    return state.set('code', value)
  }


  /**
   * 图形验证码
   * @param state
   * @param image
   */
  @Action('register:image')
  image(state,image:string){
    return state.set('image',image)
  }

  @Action('register:uuid')
  UUID(state,id){
    return state.set('UUID',id)
  }


  @Action('register:patcha')
  patcha(state,value){
    return state.set('patcha',value)
  }

  @Action('register:employeeId')
  employeeId(state,id){
    return state.set('employeeId',id)
  }
}