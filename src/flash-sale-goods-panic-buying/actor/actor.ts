import { Actor, Action } from 'plume2';
import { fromJS } from 'immutable';

export default class FlashSaleGoodsPanicBuyingActor extends Actor {
  defaultState() {
    return {
      // 优惠券列表
      goodsInfo: {
        goodImage:'',
        goodName:'',
        goodIntro:'',
        goodPrice:0,
        goodStaus:''
      }
    };
  }

  @Action('setGoodsInfo')
  setGoodsInfo (state,goodsInfo) {
    return state.set('goodsInfo',fromJS(goodsInfo))
  }
}