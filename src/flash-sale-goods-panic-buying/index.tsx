import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import Loading from 'wmkit/loading';
import 'swiper/dist/css/swiper.min.css';
import { Icon, NavBar, Flex } from 'antd-mobile';
const style = require('./css/style.css');
import Good from './component/good';
@StoreProvider(AppStore, { debug: __DEV__ })
export default class FlashSaleGoodsPanicBuying extends React.Component<
  any,
  any
> {
  store: AppStore;

  componentDidMount() {
    const flashSaleGoodsId =
      this.props.location.state && this.props.location.state.flashSaleGoodsId;
    const flashSaleGoodsNum =
      this.props.location.state && this.props.location.state.flashSaleGoodsNum;
    const goodsInfoId =
      this.props.location.state && this.props.location.state.goodsInfoId;
    this.store.init(flashSaleGoodsId, flashSaleGoodsNum,goodsInfoId);
  }



  render() {
    let goodsInfo = this.store.state().get('goodsInfo');
    return (
      <div>
        <NavBar
          mode="light"
          className={'navbarBack'}
        >
          抢购中
        </NavBar>
        <Good
          goodImage = {goodsInfo.get('goodImage')}
          goodName={goodsInfo.get('goodName')}
          goodIntro={goodsInfo.get('goodIntro')}
          goodPrice={goodsInfo.get('goodPrice')}
          goodStaus={goodsInfo.get('goodStaus')}
        />
      </div>
    );
  }
}
