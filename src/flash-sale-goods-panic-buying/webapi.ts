import Fetch from "wmkit/fetch";

export const getFlashSaleGoodsQualifications = (params:any) => {
  return Fetch<Result<any>>('/flashsale/getFlashSaleGoodsQualifications', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 获取秒杀活动详情
 * @param params
 */
export const getFlashSaleInfo = (params:any) =>{
  return Fetch<Result<any>>('/flashsale/getFlashSaleInfo', {
    method: 'POST',
    body: JSON.stringify(params)
  });
}

/**
 * 获取sku详情信息
 * @param skuId
 */
export const getGoodsInfoDetail = (skuId) =>{
    return Fetch<Result<any>>(`/goods/sku/${skuId}`);
}
