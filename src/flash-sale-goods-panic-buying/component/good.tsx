import React from 'react';
import { IMap, Relax } from 'plume2';
import 'swiper/dist/css/swiper.min.css';
import { div } from 'wmkit/common/util';
import { Flex } from 'antd-mobile';
const defaultImg = require('../images/none.png');

@Relax
export default class Good extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }
  render() {
    const { goodName, goodIntro, goodPrice, goodStaus, goodImage } = this.props;
    return (
      <div className="goodDetail">
        <div className="goodIntro">
          <div>
            <img className="goodImage" src={goodImage || defaultImg} alt="" />
          </div>
          <div className="good">
            <p className="good-name">{goodName ? goodName : ''}</p>
            <p>{goodIntro ? goodIntro : ''}</p>
            <p className="good-price">￥{goodPrice ? goodPrice : ''}</p>
          </div>
          <div className="good-status">{goodStaus}</div>
        </div>
        <Flex>
          <Flex.Item>
            <div style={styles.imageBox}>
              <img
                style={{
                  width: '50px',
                  textAlign: 'center',
                  verticalAlign: 'center'
                }}
                src={require('../../../web_modules/images/rushQuene.gif')}
                alt=""
              />
            </div>
          </Flex.Item>
        </Flex>
      </div>
    );
  }
}

const styles = {
  imageBox: {
    width: '1.7rem',
    height: '1.7rem',
    lineHeight: '2.4rem',
    border: '1px solid #ccc',
    borderRadius: '5px',
    verticalAlign: 'middle',
    boxShadow: '3px 3px 10px #888888',
    textAlign: 'center',
    margin: '2.5rem auto'
  }
} as any;
