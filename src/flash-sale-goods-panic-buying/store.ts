import { Store } from 'plume2';
import * as webapi from './webapi';
import history from 'wmkit/history';
import { Alert } from 'wmkit';
import { config } from 'config';
import FlashSaleGoodsPanicBuyingActor from './actor/actor';
import * as WMkit from 'wmkit/kit';

export default class AppStore extends Store {
  timer;

  constructor(props) {
    super(props);
    if (__DEV__) {
      //debug
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new FlashSaleGoodsPanicBuyingActor()];
  }

  init = async (flashSaleGoodsId, flashSaleGoodsNum, goodsInfoId) => {
    await this._getGoodsInfoDetail(flashSaleGoodsId);
    await this.getFlashSaleGoodsQualifications(
      flashSaleGoodsId,
      flashSaleGoodsNum
    );
  };

  /**
   * 获取抢购商品详情
   * @param goodsInfoId
   * @private
   */
  _getGoodsInfoDetail = async (flashSaleGoodsId) => {
    let param = { flashSaleGoodsId: flashSaleGoodsId };
    let goodsInfo = {
      goodImage: '',
      goodName: '',
      goodIntro: '',
      goodPrice: '',
      goodStaus: '积极抢购中'
    };
    let flashSaleInfoRes: any = await webapi.getFlashSaleInfo(param);
    if (flashSaleInfoRes.code == config.SUCCESS_CODE) {
      let goodsInfoRes: any = await webapi.getGoodsInfoDetail(
        flashSaleInfoRes.context.goodsInfoId
      );
      goodsInfo.goodPrice = flashSaleInfoRes.context.price;
      if (goodsInfoRes.code == config.SUCCESS_CODE) {
        goodsInfo.goodImage = goodsInfoRes.context.goods.goodsImg;
        goodsInfo.goodName = goodsInfoRes.context.goodsInfo.goodsInfoName;
        goodsInfo.goodIntro = goodsInfoRes.context.goodsInfo.specText;
      }
    }
    this.dispatch('setGoodsInfo', goodsInfo);
  };

  /**
   * 是否获取抢购资格定时任务查询
   * @param flashSaleGoodsId
   */
  getFlashSaleGoodsQualifications = async (
    flashSaleGoodsId,
    flashSaleGoodsNum
  ) => {
    let res = {};
    this.timer = setInterval(async () => {
      if (WMkit.isLogin()) {
        res = await webapi.getFlashSaleGoodsQualifications({
          flashSaleGoodsId: flashSaleGoodsId,
          flashSaleGoodsNum: flashSaleGoodsNum
        });
        if ((res as any).code == config.SUCCESS_CODE) {
          if ((res as any).context.flashSaleGoodsId != null) {
            history.push('/flash-sale-order-confirm');
            (window as any).y = '';
            //暂停定时任务
            clearInterval(this.timer);
          }
        } else {
          Alert({
            text: (res as any).message
          });
          clearInterval(this.timer);
          setTimeout(() => {
            history.push('/flash-sale');
          }, 1000);
        }
      } else {
        //暂停定时任务
        clearInterval(this.timer);
      }
    }, 1000);
  };
}
