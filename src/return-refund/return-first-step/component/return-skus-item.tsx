import React from 'react';
import { Relax } from 'plume2';
import { noop, Check, NumberInput, _, Alert } from 'wmkit';

const noneImg = require('../../img/none.png');

/**
 * 退货商品
 */
@Relax
export default class ReturnSkusItem extends React.Component<any, any> {
  static relaxProps = {
    //单选
    checkOne: noop,
    changeNum: noop
  };

  static defaultProps = {
    //商品项
    sku: {},
    skuId: -1
  };

  render() {
    const sku = this.props.sku;

    let errorMsg = sku.skuChecked
      ? sku.num > sku.canReturnNum
        ? '可退数量' + sku.canReturnNum
        : sku.num == 0
          ? '退货数量不可小于1'
          : ''
      : '';

    return (
      <div className="refund-item">
        <Check
          checked={sku.skuChecked}
          onCheck={() => this.props.relaxProps.checkOne(sku.skuId)}
          style={{ marginRight: 10 }}
        />
        <div className="goods-list">
          <img src={sku.pic ? sku.pic : noneImg} className="img-box" />
          <div className="detail">
            <div>
              <p className="title">{sku.skuName}</p>
              <p className="gec">{sku.specDetails}</p>
            </div>
            <div className="bottom">
              <span className="price">
                <i className="iconfont icon-qian" />
                {_.addZero(sku.price)}
              </span>
              <NumberInput
                max={sku.canReturnNum}
                min={0}
                error={errorMsg}
                value={sku.num}
                disableSubtraction={sku.skuChecked && sku.num == 1}
                onChange={(buyCount) => {
                  this._handleChange(buyCount);
                }}
                onAddClick={(addDisabled, nextValue) => {
                  const max = sku.canReturnNum;
                  if (addDisabled && nextValue > max) {
                    Alert({ text: `可退数量${max}` });
                  }
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 修改sku数量
   * @param value
   * @private
   */
  _handleChange = (value) => {
    const { canReturnNum } = this.props.sku;

    if (value > canReturnNum) {
      Alert({ text: '可退数量' + canReturnNum });
    }

    const sku = this.props.sku;
    this.props.relaxProps.changeNum({
      skuNum: value,
      skuId: sku.skuId
    });
    this.setState({});
  };
}
