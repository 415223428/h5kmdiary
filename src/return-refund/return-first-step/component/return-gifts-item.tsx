import React from 'react';
import { Check, _ } from 'wmkit';

const noneImg = require('../../img/none.png');

/**
 * 退货赠品
 */
export default class ReturnGiftsItem extends React.Component<any, any> {
  static defaultProps = {
    //赠品项
    sku: {}
  };

  render() {
    const sku = this.props.sku;
    return (
      <div className="refund-item">
        <Check
          checked={sku.giftChecked}
          disable={true}
          style={{ marginRight: 10 }}
        />
        <div className="goods-list">
          <img
            src={sku.pic ? sku.pic : noneImg}
            className="img-box"
            style={{ width: 55, height: 55 }}
          />
          <div className="detail" style={{ minHeight: 50 }}>
            <div>
              <p className="title" style={{ marginBottom: 5 }}>
                <div className="tag-gift">赠品</div>
                {sku.skuName}
              </p>
              <p className="gec" style={{ margin: 0 }}>
                {sku.specDetails}
              </p>
            </div>
            <div className="bottom" style={{ marginTop: 0 }}>
              <span className="price">
                <i className="iconfont icon-qian" />
                {_.addZero(sku.price)}
              </span>
              <span style={{ fontSize: '.28rem' }}>×{sku.num}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
