import React from 'react'
import { Relax } from 'plume2'
import { _, ListView, noop} from 'wmkit'
import RetuenSkusItem from './return-skus-item'


/**
 * 退货商品列表
 */
@Relax
export default class RetuenSkusList extends React.Component<any, any> {
  props:{
    relaxProps?:{
      skus: any,
      saveSkus: Function
    }
  };


  static relaxProps = {
    //退货商品
    skus: 'skus',

    saveSkus: noop
  };


  render() {
    const dataSource = this.props.relaxProps.skus.toJS()

    return (
      <ListView
        isPagination={false}
        dataSource={dataSource}
        style={{height: 'auto'}}
        renderRow={(sku: any, index: number) => {
          //可退数量
          const returnNum = sku.canReturnNum
          if(returnNum > 0) {
            return (<RetuenSkusItem sku={sku} skuId={sku.skuId} skuIndex={index}/>)
          }else {
            return null
          }
        }}
        onDataReached={(res: any) => this.props.relaxProps.saveSkus(res)}
      />
    )
  }
}