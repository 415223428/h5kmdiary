import React from 'react'
import { Relax } from 'plume2'
import { noop, Check } from 'wmkit'
import { checkedAllQL } from '../../ql'
import RetuenSkusList from './return-skus-list'
import ReturnGiftsList from './return-gifts-list'


/**
 * 退货商品box
 */
@Relax
export default class RetuenSkusBox extends React.Component<any, any> {
  static relaxProps = {
    //全选状态
    checkedAll: checkedAllQL,

    //全选方法
    checkAll: noop,
  };


  render() {
    return (
      <div style={{backgroundColor: '#fafafa'}}>
        <div className="refund-title">
          <Check checked={this.props.relaxProps.checkedAll}
                 onCheck={() => this.props.relaxProps.checkAll(this.props.relaxProps.checkedAll)}
          />
          <div className="refund-dec">
            <span style={{marginTop: 2,display: 'block'}}>选择退货商品</span>
            <p><i className="iconfont icon-jinggao"></i>&nbsp;如因退货导致满赠活动失效，您需要返还赠品哦</p>
          </div>
        </div>
        <RetuenSkusList/>
        <ReturnGiftsList/>
      </div>
    )
  }
}
