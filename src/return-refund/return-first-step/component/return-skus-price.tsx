import React from 'react';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';
import { history, noop, _ } from 'wmkit';
import { checkedAllPriceQL, checkedAnyQL } from '../../ql';

const next =require('../img/next.png')
/**
 * 退货商品价格
 */
@Relax
export default class RetuenSkusPrice extends React.Component<any, any> {
  props: {
    history?: object;
    relaxProps?: {
      skus: IList;
      //获取选中退货商品价格
      checkedAllPrice: number;
      //获取按钮是否禁用
      checkedAny: boolean;
      //下一步
      returnSkuSecond: Function;
    };
  };

  static relaxProps = {
    skus: 'skus',
    //获取选中退货商品价格
    checkedAllPrice: checkedAllPriceQL,
    //获取按钮是否禁用
    checkedAny: checkedAnyQL,
    returnSkuSecond: noop
  };

  render() {
    const {price, points} = this.props.relaxProps.checkedAllPrice as any;
    return (
      <div style={{ height: 48 }}>
        <div className="bottom-bar-total">
          <div className="price-box">
            应退金额：
            <label>
              <i className="iconfont icon-qian" />
              {_.addZero(price)}
            </label>
          </div>
          <div className="price-box">
            应退积分：
            <label>
              {Math.floor(points)}
            </label>
          </div>
          <div className="" />
         { this.props.relaxProps.checkedAny?
          <button
            className="btn-submit"
            style={{backgroundColor:'#ccc'}}
            disabled={this.props.relaxProps.checkedAny}
          >
            下一步
          </button>
          :
          <img src={next} alt=""  style={{width: '1.9rem',height: '0.98rem'}}
               onClick={() =>
                this.props.relaxProps.returnSkuSecond(this._nextStep)
              }/>}
        </div>
      </div>
    );
  }

  _nextStep = () => {
    (history as any).push({
      pathname: '/returnsSecond'
    });
  };
}
