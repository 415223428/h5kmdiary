import React from 'react'
import { Relax } from 'plume2'
import { ListView} from 'wmkit'
import ReturnGiftsItem from './return-gifts-item'


/**
 * 退货商品列表
 */
@Relax
export default class ReturnGiftsList extends React.Component<any, any> {
  props:{
    relaxProps?:{
      gifts: any
    }
  };


  static relaxProps = {
    //退货赠品
    gifts: 'gifts'
  };


  render() {
    const {gifts} = this.props.relaxProps;
    if(!gifts || gifts.size == 0){
      return null;
    }
    const dataSource = gifts.toJS();

    return <ListView
        isPagination={false}
        dataSource={dataSource}
        style={{height: 'auto'}}
        renderRow={(sku: any) => {
          //可退数量
          const returnNum = sku.canReturnNum
          if(returnNum > 0) {
            return (<ReturnGiftsItem sku={sku}/>)
          }else {
            return null
          }
        }}
        onDataReached={() => {}}
      />
  }
}