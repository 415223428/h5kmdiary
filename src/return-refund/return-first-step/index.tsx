import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from '../store';
import RetuenSkusBox from './component/return-skus-box';
import RetuenSkusPrice from './component/return-skus-price'
import ReturnRefundHead from '../component/return-refund-head'


/**
 * 退货第一步
 */
@StoreProvider(AppStore)
export default class ReturnsFirst extends React.Component<any, any> {
  store: AppStore;


  constructor(props) {
    super(props)
  }


  componentDidMount() {
    const {tid} = this.props.match.params;

    this.store.init(tid);
  }


  render() {
    return(
      <div style={{backgroundColor:'#fafafa'}}>
        <ReturnRefundHead/>
        <RetuenSkusBox/>
        <RetuenSkusPrice/>
      </div>
    );
  }
}
