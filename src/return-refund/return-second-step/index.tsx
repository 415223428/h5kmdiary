import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from '../store';
import ReturnRefundReason from '../component/return-refund-reason'
import ReturnRefundRemark from '../component/return-refund-remark'
import ReturnType from './component/return-type'
import ReturnRefundFile from '../component/return-refund-file'
import ReturnRefundPrice from '../component/return-refund-price'
const styles = require('../css/style.css')


/**
 * 退货第二步
 */
@StoreProvider(AppStore)
export default class ReturnsSecond extends React.Component<any, any> {
  store: AppStore;


  constructor(props) {
    super(props)
  }


  componentDidMount() {
    this.store.initApplyPage()
  }


  render() {
    return(
      <div style={{backgroundColor:'#fafafa', height: '100%'}}>
        <ReturnRefundReason/>
        <ReturnType/>
        <ReturnRefundRemark/>
        <ReturnRefundFile/>
        <ReturnRefundPrice/>
      </div>
    );
  }
}
