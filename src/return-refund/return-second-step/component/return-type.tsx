import React from 'react'
import { Relax } from 'plume2'
import { RadioBox, noop } from 'wmkit'
import { IList } from 'typings/globalType'


/**
 * 退货方式
 */
@Relax
export default class ReturnType extends React.Component<any, any> {

  props: {
    relaxProps?: {
      // 退货方式
      returnWayList: IList,
      selectedReturnWay: string,
      changeReturnType: Function
    }
  }


  static relaxProps = {
    returnWayList: 'returnWayList',
    selectedReturnWay: 'selectedReturnWay',
    changeReturnType: noop
  }


  render() {
    const {returnWayList, selectedReturnWay, changeReturnType} = this.props.relaxProps

    return (
      <div className="refund-reason-box">
        <h2>退货方式</h2>
        <RadioBox checked={selectedReturnWay}
               data={returnWayList}
               beforeTxt={false}
               onCheck={(value) => changeReturnType(value)}
               style={{ width: '46%'}}
        />
      </div>
    )
  }
}
