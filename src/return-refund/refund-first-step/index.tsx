import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from '../store';
import ReturnRefundReason from '../component/return-refund-reason'
import ReturnRefundRemark from '../component/return-refund-remark'
import ReturnRefundFile from '../component/return-refund-file'
import ReturnRefundPrice from '../component/return-refund-price'

const styles = require('../css/style.css')


/**
 * 退款第一步
 */
@StoreProvider(AppStore)
export default class ReturnsSecond extends React.Component<any, any> {
  store: AppStore;


  constructor(props) {
    super(props)
  }


  componentDidMount() {
    const {tid} = this.props.match.params;

    this.store.init(tid);
  }


  render() {
    return(
      <div style={{backgroundColor:'#fafafa'}}>
        <ReturnRefundReason/>
        <ReturnRefundRemark/>
        <ReturnRefundFile/>
        <ReturnRefundPrice/>
      </div>
    );
  }
}
