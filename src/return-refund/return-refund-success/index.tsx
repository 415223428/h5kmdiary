import React from 'react';
import { StoreProvider } from 'plume2';
import { Alert, WMImage, history, _ } from 'wmkit';
import AppStore from '../store';
import '../css/style.css'

/**
 * 退货退款成功
 */
@StoreProvider(AppStore)
export default class ReturnRefundSuccess extends React.Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { rid } = this.props.match.params;
    if (rid) {
      this.store.returnsOkInit(rid);
    } else {
      Alert({
        text: '退单不存在'
      });
      history.push('/refund-list');
    }
  }

  render() {
    const { rid } = this.props.match.params;

    // 总额
    const totalPrice =
      this.store
        .state()
        .getIn(['returnsResult', 'returnPrice', 'totalPrice']) || 0;
    // 改价金额
    const applyPrice = this.store
      .state()
      .getIn(['returnsResult', 'returnPrice', 'applyPrice']);
    // 应退金额，如果对退单做了改价，使用applyPrice，否则，使用总额totalPrice
    const payPrice = applyPrice || totalPrice;

    return (
      <div className="list-none" style={{ display: "flex", alignItems: 'center', justifyContent: 'center' }}>
        <WMImage
          src={require('./img/return.png')}
          width="2.9rem"
          height="2rem"
        />
        <div style={{ textAlign: 'center', marginTop: ".6rem" ,width:'100%'}}>
          <p className="text-al-c" style={{ fontSize: ".32rem" }}>退货退款提交成功</p>
          <div style={{  marginTop: ".4rem" }}>
            <h5>您的申请已提交审核</h5>
            <h5>您可在我的-退货退款查看处理进度</h5>
          </div>
        </div>
        <div style={{width:'100%'}}>
          <div className="bot-box address-box" style={{ marginTop: ".9rem" ,padding:".2rem"}}>
            {/* <div className="line" /> */}
            <div className="item" style={{ fontSize:".28rem"}}>
              退单编号
              <span>{this.store.state().getIn(['returnsResult', 'id'])}</span>
            </div>
            <div className="item" style={{ fontSize:".28rem",marginTop: ".3rem",marginBottom: ".2rem"}}>
              退单金额
              <span>
                <i className="iconfont icon-qian" />
                {_.addZero(payPrice)}
              </span>
            </div>
          </div>
          {/* <div className="bot-line" /> */}
        </div>
        <div className="bottom-btn-suc" style={{ marginTop: ".4rem" }}>
          <div>
            <button
              className="btn btn-ghost "
              style={{ fontSize: ".28rem", borderRadius: "1rem" ,width:"3rem",marginRight: '.5rem',lineHeight: ".32rem"}}
              onClick={() => this._returnsDetail(rid)}
            >
              查看退货退款详情
            </button>
          </div>
          <div>
            <button
              className="btn btn-ghost "
              style={{ fontSize: ".28rem", borderRadius: "1rem" ,width:"3rem",lineHeight: ".32rem"}}
              onClick={this._backToMain}
            >
              返回首页
            </button>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 退单详情页面
   * @private
   */
  _returnsDetail(rid) {
    if (rid) {
      history.push(`/return-detail/${rid}`);
    } else {
      Alert({
        text: '退单不存在'
      });

      history.push('/refund-list');
    }
  }

  /**
   * 返回首页
   * @private
   */
  _backToMain() {
    history.push('/');
  }
}
