import React from 'react'
import { Relax } from 'plume2'
import { IList } from "typings/globalType"
import { noop, _ } from 'wmkit'

const submit=require('../img/submit.png')
/**
 * 退货退款底部
 */
@Relax
export default class ReturnRefundPrice extends React.Component<any, any> {
  props: {
    relaxProps?: {
      order: object,
      description: string,
      skus: IList,
      totalPrice: number,
      tradePoints: number,
      applyReturns: Function
    }
  }


  static relaxProps = {
    order: 'order',
    description: 'description',
    skus: 'skus',
    totalPrice: 'totalPrice',
    tradePoints: 'tradePoints',
    applyReturns: noop
  }

  constructor(prop){
    super(prop);
    this.state={
      loading:false,
    }
  }

  render() {
    const { totalPrice, tradePoints } = this.props.relaxProps
    return (
      <div style={{ height: 48 }}>
        <div className="bottom-bar-total"style={{justifyContent:"space-between"}} >
          <div style={{ display: "flex", justifyContent: "space-around" ,marginLeft:".2rem"}}>
            <div className="price-box">应退金额：
              <label><i className="iconfont icon-qian" />{_.addZero(totalPrice)}</label>
            </div>
            <div className="price-box">应退积分：
              <label>{tradePoints}</label>
            </div>
          </div>
          <div>
          {/* <button className="btn-submit btn-color-red"
            onClick={() => this.props.relaxProps.applyReturns()}
          >提交
            </button> */}
            <img src={submit} alt="" style={{width: '1.9rem',height: '0.98rem',display:'block'}}
                 onClick={() => this._applyReturns()}/>
          </div>
        </div>
      </div>
    )
  }
  _applyReturns= async ()=>{
    // this.setState({
    //   loading:true
    // })
    this.props.relaxProps.applyReturns()
  }
}
