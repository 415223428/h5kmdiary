import React from 'react';
import { Relax } from 'plume2';
import { UploadImage, noop, WMImage, Alert } from 'wmkit';

/**
 * 退货退款附件
 */
@Relax
export default class ReturnRefundFile extends React.Component<any, any> {
  props: {
    relaxProps?: {
      tid: string;
      addImage: Function;
      removeImage: Function;
      images: any;
    };
  };

  static relaxProps = {
    tid: 'tid',
    addImage: noop,
    removeImage: noop,
    images: 'images'
  };

  render() {
    const { images } = this.props.relaxProps;

    return (
      <div className="refund-reason-box address-box">
        <h2>退单附件</h2>
        <div className="image-scroll">
          {this.props.relaxProps.images.toArray().map((v, index) => {
            return (
              <div className="refund-file delete-img">
                <WMImage
                  src={v.get('fileData') ? v.get('fileData') : v.get('image')}
                  width="53px"
                  height="53px"
                />
                <a
                  href="javascript:void(0)"
                  onClick={() => this.removeImage(index)}
                >
                  ×
                </a>
              </div>
            );
          })}

          {images.count() < 10 ? (
            <div className="refund-file">
              <UploadImage
                onSuccess={this._uploadImage}
                onLoadChecked={this._uploadCheck}
                repeat
                className="upload"
                size={5}
                fileType=".jpg,.png,.jpeg,.gif"
              >
                <div className="upload-item">
                  <div className="upload-content">
                    <i className="iconfont icon-add11" />
                  </div>
                </div>
              </UploadImage>
            </div>
          ) : null}
        </div>
        <p className="file-tips">
          仅支持jpg、jpeg、png、gif文件，最多上传10张，大小不超过5M
        </p>
      </div>
    );
  }

  /**
   * 上传附件
   */
  _uploadImage = (result, file, data) => {
    let { context } = result;

    if (context) {
      this.props.relaxProps.addImage({
        image: context[0],
        status: 'done',
        fileData: data
      });
    }
  };

  /**
   * 删除附件
   */
  removeImage = (index) => {
    this.props.relaxProps.removeImage(index);
  };

  /**
   * 上传前校验
   * @returns {boolean}
   * @private
   */
  _uploadCheck = () => {
    const { images } = this.props.relaxProps;
    if (images.count() >= 10) {
      Alert({ text: '最多只能上传10张图片' });
      return false;
    }
    return true;
  };
}
