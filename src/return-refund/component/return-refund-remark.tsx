import React from 'react'
import { Relax } from 'plume2'
import { noop } from 'wmkit'
import { remarkLengthQL } from '../ql'

/**
 * 退货退款备注
 */
@Relax
export default class ReturnRefundRemark extends React.Component<any, any> {
  static relaxProps = {
    //退单备注
    description: 'description',

    //获取退单备注长度
    remarkLength: remarkLengthQL,

    //输入退货备注
    changReturnRemark: noop,
  }


  render() {
    return (
      <div className="refund-reason-box address-box">
        <h2>退货说明</h2>
        <textarea
          className="bring-area"
          placeholder="点此输入退货说明（100字以内）"
          onChange={(e) => this.props.relaxProps.changReturnRemark(e.target.value) }
          maxLength={100}
          value={this.props.relaxProps.description}
        />
        <p className="words-limit">{this.props.relaxProps.remarkLength}/100</p>
      </div>
    )
  }
}
