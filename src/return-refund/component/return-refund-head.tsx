import React from 'react'
import { Relax } from 'plume2'
const styles = require('../css/style.css')


/**
 * 退货商品头部
 */
@Relax
export default class ReturnRefundHead extends React.Component<any, any> {
  static relaxProps = {
    tid: 'tid'
  }


  render() {
    return (
      <div className="order-num-title mb10">
        订单号：<label>{this.props.relaxProps.tid}</label>
      </div>
    )
  }
}
