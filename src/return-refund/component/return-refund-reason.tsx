import React from 'react'
import { Relax } from 'plume2'
import { RadioBox, noop } from 'wmkit'
import { IList } from 'typings/globalType'


/**
 * 退货退款原因
 */
@Relax
export default class ReturnRefundReason extends React.Component<any, any> {
  props: {
    relaxProps?: {
      // 退货原因
      returnReasonList: IList,
      selectedReturnReason: string,
      changeReturnReason: Function
    }
  }


  static relaxProps = {
    returnReasonList: 'returnReasonList',
    selectedReturnReason: 'selectedReturnReason',
    changeReturnReason: noop
  }


  render() {
    const {returnReasonList, changeReturnReason, selectedReturnReason} = this.props.relaxProps;
    return (
      <div className="refund-reason-box address-box">
        <h2>选择退货原因</h2>
        <RadioBox
          data={ returnReasonList}
          checked={selectedReturnReason}
          onCheck={(value) => changeReturnReason(value)}
          style={{width: '46%',borderRadius:"29px"}}
        />
      </div>
    )
  }
}
