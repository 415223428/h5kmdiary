import { Fetch } from 'wmkit';

/**
 * 获取拼团设置 -- 轮播广告
 * @type {Promise<AsyncResult<T>>}
 */
export const fetchGrouponSetting = () => {
  return Fetch('/groupon/setting/info');
};

/**
 * 查询活动分类
 */
export const getGrouponCateList = () => {
  return Fetch('/groupon/cate/list', {
    method: 'GET'
  });
};

/**
 * 获取热门推荐、精选拼团列表数据
 * @type {Promise<AsyncResult<T>>}
 */
export const fetchGrouponCenterList = (params) => {
  return Fetch('/groupon/center/list', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};
