import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class GrouponCenterActor extends Actor {
  defaultState() {
    return {
      // 拼团设置广告
      grouponAdvert: [],
      // 拼团分类列表
      grouponCates: [],
      // 热门推荐列表
      grouponHotList: [],

      // 选中的拼团分类ID
      chooseCateId: '',
      // 关键字搜索
      keyWords: '',
      // 是否精选分类
      sticky: true,
      //ListView是否可滚动
      isScroll: false,
      //tab的offsetTop
      tabOffsetTop: 0,
      //是否是第一次滚动
      isFirst: true
    };
  }

  /**
   * 数据变化
   * @param state
   * @param context
   * @returns {*}
   */
  @Action('group: field: change')
  initList(state: IMap, { field, value }) {
    return state.set(field, value);
  }

  /**
   * 处理拼团热门推荐数据
   * @param {IMap} state
   * @param data
   * @returns {Map<string, any>}
   */
  @Action('group: hot: list')
  dealHotRecommend(state: IMap, data) {
    if (data.content) {
      // totalPage = (totalRecord + maxResult -1) / maxResult;
      // fromJS([1,2,3,4,5,6,7,8,9,10,11,12,13]).groupBy((_i, index) => parseInt(index/3)).toJS();
      // 共有多少页
      let newArray = fromJS(data.content).groupBy((_i, index) =>
        Math.floor(index / 3)
      );
      return state.set('grouponHotList', newArray);
    }
  }
  /**
   * 存储tab的offsetTop高度
   */
  @Action('saveOffsetTop:tabOffsetTop')
  saveOffsetTop(state: IMap, value) {
    return state.set('tabOffsetTop', value);
  }
  /**
   * 是否是第一次滚动
   */
  @Action('changeIsFirst:isFirst')
  changeIsFirst(state: IMap, value) {
    return state.set('isFirst', value);
  }
}
