import * as React from 'react';
import { Relax } from 'plume2';
import { fromJS, List } from 'immutable';
import { ListView, Blank, noop } from 'wmkit';
import { div } from 'wmkit/common/util';
import { GrouponListItem } from 'biz';
import ImgSlider from '../component/img-slider';
import HotRecommend from '../component/hot-recommend';
import SelectListTop from '../component/select-list-top';
@Relax
export default class SelectList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      //请求参数
      chooseCateId: string;
      keyWords: string;
      sticky: boolean;
      grouponAdvert: List<any>;
      grouponHotList: List<any>;
      _onScroll: Function;
    };
    goodsCount:number;
  };

  static relaxProps = {
    //请求参数
    chooseCateId: 'chooseCateId',
    keyWords: 'keyWords',
    sticky: 'sticky',
    grouponAdvert: 'grouponAdvert',
    grouponHotList: 'grouponHotList',
    _onScroll: noop
  };



  render() {
    const {
      chooseCateId,
      keyWords,
      sticky,
      grouponAdvert,
      grouponHotList,
      _onScroll
    } = this.props.relaxProps;
    const {goodsCount} = this.props;
    let searchBar = document.getElementById('searchBar') ? true : false;
    return (
      <div>
        <ListView
          url="/groupon/center/list"
          params={{
            grouponCateId: chooseCateId,
            goodsName: keyWords,
            sticky: sticky
          }}
          pageSize={goodsCount?goodsCount:10}
          // MAX_CHUNK_PAGE_NUM = {goodsCount}
          isPagination={true}
          renderRow={(item, index) => {
            return <GrouponListItem goodInfo={fromJS(item)} key={index} />;
          }}
          renderEmpty={() => (
            <Blank
              img={require('../img/empty.png')}
              content="啊哦，暂时没有拼团活动~"
            />
          )}
          renderHeader={() => {
            return (
              <div>
                {(!searchBar && grouponAdvert.size > 0) && <ImgSlider />}
                {(!searchBar && grouponHotList.size > 0) && <HotRecommend />}
                <SelectListTop />
              </div>
            );
          }}
          scrollHandle={_onScroll}
        />
      </div>
    );
  }
}
