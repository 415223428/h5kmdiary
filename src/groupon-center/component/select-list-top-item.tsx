import * as React from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';

@Relax
export default class SelectListTopItem extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeTopActive: Function;
    };
    label: string;
    active: boolean;
    tabKey: string;
    defaultCate: number;
  };

  static relaxProps = {
    changeTopActive: noop
  };

  render() {
    const { label, active, tabKey, defaultCate } = this.props;
    const { changeTopActive } = this.props.relaxProps;

    return (
      <div
        className={`layout-item ${active ? 'cur' : null}`}
        onClick={() => {
          changeTopActive(tabKey, defaultCate);
          let layoutTop = document.getElementById('layout-top');
          layoutTop.classList.remove('layout-top-fixed');
        }}
      >
        {label}
      </div>
    );
  }
}
