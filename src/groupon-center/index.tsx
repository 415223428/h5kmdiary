import React from 'react';
import { StoreProvider } from 'plume2';
import { GrouponBottom, Slider } from 'wmkit';
import { GrouponSearch } from 'biz';

import AppStore from './store';
import SelectList from './component/select-list';

import './css/style.css';
import { fromJS } from 'immutable';

@StoreProvider(AppStore)
export default class GroupCenter extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    // document.title = '拼购';
    this.store.init();
  }
  listViewPrt = document.getElementById('listViewPrt');

  // componentDidMount() {
  //   window.addEventListener('scroll', this.store._onScroll.bind(this));
  // }
  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.store._onScroll.bind(this));
  // }
  render() {
    const { page ,goodsCount} = this.props;
    return (
      <div className="groupon-center" style={{paddingTop: '0'}}>
        {page != "main" && <GrouponSearch keyWords={this.store.state().get('keyWords')} state="recommend"/>}
        <SelectList  goodsCount = {goodsCount}/>
        {page != "main" && <GrouponBottom currTab={'拼购'} />}
      </div>
    );
  }
}
