import { Store } from 'plume2';
import { fromJS } from 'immutable';
import { config } from 'config';
import * as webapi from './webapi';
import GrouponCenterActor from './actor/groupon-center-actor';
import { Confirm, history } from 'wmkit';
import Swiper from 'swiper/dist/js/swiper.js';
import 'swiper/dist/css/swiper.min.css';
export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new GrouponCenterActor()];
  }

  /**
   * 初始化加载数据
   * @returns {Promise<void>}
   */
  init = async () => {
    this.getGrouponSetting();
    this.getGrouponCate();
    this.getHotRecommend();
    setTimeout(function() {
      const hotSwiper = new Swiper('#swiper-container2', {
        spaceBetween: 20,
        pagination: {
          type: 'fraction',
          el: '.hot-title-right'
        }
      });
      const bannerSwiper = new Swiper('#swiper-container1', {
        autoplay: true,
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        }
      });
    }, 1000);
  };

  /**
   * 数据变更
   * @param {any} field
   * @param {any} value
   */
  onFieldChange = ({ field, value }) => {
    this.dispatch('group: field: change', { field, value });
  };

  /**
   * 查询拼团设置广告
   * @returns {Promise<void>}
   */
  getGrouponSetting = async () => {
    const res = (await webapi.fetchGrouponSetting()) as any;
    const { code, context } = res;
    if (code == config.SUCCESS_CODE) {
      let setting = context ? context.grouponSettingVO : {};
      if (setting && setting.advert) {
        this.onFieldChange({
          field: 'grouponAdvert',
          value: fromJS(JSON.parse(setting.advert))
        });
      }
    }
  };

  /**
   * 查询拼团分类
   * @returns {Promise<void>}
   */
  getGrouponCate = async () => {
    const res = (await webapi.getGrouponCateList()) as any;
    const { code, context } = res;
    if (code == config.SUCCESS_CODE) {
      let cateList = context ? context.grouponCateVOList : [];
      this.onFieldChange({
        field: 'grouponCates',
        value: fromJS(cateList)
      });
    }
  };

  /**
   * 查询拼团热门推荐列表
   * @returns {Promise<void>}
   */
  getHotRecommend = async (
    { pageNum, pageSize } = { pageNum: 0, pageSize: 15 }
  ) => {
    const res = (await webapi.fetchGrouponCenterList({
      pageSize,
      pageNum
    })) as any;
    const { code, context } = res;
    if (code == config.SUCCESS_CODE) {
      let hotList = context ? context.grouponCenterVOList : [];
      this.dispatch('group: hot: list', hotList);
    }
  };

  /**
   * 拼团分类选中事件
   * @param key tab的key值
   */
  changeTopActive = (key: string, defaultCate: number) => {
    if (key && this.state().get('chooseCateId') === key) {
      return false;
    }
    // keyWords = ''
    this.onFieldChange({ field: 'keyWords', value: '' });
    // 精选分类下的设值
    if (defaultCate == 1) {
      // 精选 = true
      this.onFieldChange({ field: 'sticky', value: true });
      // chooseCateId = ''
      this.onFieldChange({ field: 'chooseCateId', value: '' });
    } else {
      // 精选 = false
      this.onFieldChange({ field: 'sticky', value: false });
      // chooseCateId = key
      this.onFieldChange({ field: 'chooseCateId', value: key });
    }
  };
  /**
   * 存储tab的offsetTop高度
   */
  saveOffsetTop = (value) => {
    this.dispatch('saveOffsetTop:tabOffsetTop', value);
  };
  /**
   * 动态监听滚动
   *
   */
  _onScroll = (scrollY) => {
    const isFirst = this.state().get('isFirst');
    let tabOffsetTop = 0;
    /*tab的offsetTop 获取时机 是所有异步的数据都渲染完成之后才取到 固有的生命周期都去不到 listView没有 渲染完成的回调方法 ~~  */

    if (isFirst) {
      this.dispatch('changeIsFirst:isFirst', false);
      let p = document.getElementById('layout-top');
      this.dispatch('saveOffsetTop:tabOffsetTop', p.offsetTop);
      tabOffsetTop = p.offsetTop;
    }
    /*非第一次滚动 去state中tabOffsetTop*/

    tabOffsetTop =
      tabOffsetTop == 0 ? this.state().get('tabOffsetTop') : tabOffsetTop;
    let layoutTop = document.getElementById('layout-top');
    /**滚动到对应位置 修改tab的浮动样式*/

    if (scrollY >= tabOffsetTop - 48) {
      layoutTop.classList.add('layout-top-fixed');
    } else {
      layoutTop.classList.remove('layout-top-fixed');
    }
  };
}
