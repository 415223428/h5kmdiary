import React from 'react';
import { StoreProvider } from 'plume2';
import { Button } from 'wmkit';
import AppStore from './store';
import Tab from './component/tab';
import CouponList from './component/coupon-list';
import HelpMask from './component/help-mask';
import './css/use-coupon.css'
const LongBlueButton = Button.LongBlue;
const confirm=require('./img/confirm.png')
/**
 * 使用优惠券
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class UseCoupon extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { coupons, couponPageInfo, storeIds } =
      this.props.location.state && this.props.location.state;
    this.store.init(coupons, couponPageInfo, storeIds);
  }

  render() {
    return (
      <div className="container">
        <div style={{ height: '0.8rem' }}>
          <div className="top-box">
            <Tab />
          </div>
        </div>
        <CouponList />
        <HelpMask />
        <div style={{ height: '1.3rem' }}>
          <div className="bottom-btn" style={{padding:'0'}}>
            {/* <LongBlueButton
              text="确定"
              onClick={() => this.store.onSubmit()}
              defaultStyle={{ background: '#000', borderColor: '#000',borderRadius:'0' }}
            /> */}
            <img src={confirm} alt="" style={{width:'100%'}} onClick={() => this.store.onSubmit()}/>
          </div>
        </div>
      </div>
    );
  }
}
