import React from 'react';
import { Relax } from 'plume2';
import { noop, Blank, _ } from 'wmkit';
import { IMap } from 'typings/globalType';

const common = require('../img/common.png');
const store = require('../img/store.png');
const expired = require('../img/expired.png');
@Relax
export default class CouponList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      tabKey: number;
      enableCoupons: IMap;
      disableCoupons: IMap;
      enableCount: number;
      disableCount: number;
      onChooseCoupon: Function;
      onSubmit: Function;
    };
  };

  static relaxProps = {
    tabKey: 'tabKey',
    enableCoupons: 'enableCoupons',
    disableCoupons: 'disableCoupons',
    enableCount: 'enableCount',
    disableCount: 'disableCount',
    onChooseCoupon: noop,
    onSubmit: noop
  };

  render() {
    const {
      tabKey,
      enableCoupons,
      disableCoupons,
      enableCount,
      disableCount,
      onSubmit
    } = this.props.relaxProps;

    return tabKey == 0 ? (
      enableCount != 0 ? (
        <div className="list use-list">
          {enableCoupons.get('stores').map((store) => (
            <div key={store.get('storeId')}>
              <p style={{color:'#333',fontSize:'0.28rem'}}>{`${store.get('storeName')}(${store.get('coupons').size})`}</p>
              {store.get('coupons').map((coupon) => this.renderItem(coupon))}
            </div>
          ))}

          {enableCoupons.get('commonCoupons').size > 0 && (
            <div>
              <p style={{color:'#333',fontSize:'0.28rem'}}>通用券({enableCoupons.get('commonCoupons').size})</p>
              {enableCoupons
                .get('commonCoupons')
                .map((coupon) => this.renderItem(coupon))}
            </div>
          )}
        </div>
      ) : (
        <Blank
          img={require('../img/no-coupon.png')}
          content="啊哦，什么券都没有~"
        />
      )
    ) : disableCount != 0 ? (
      <div className="list use-list">
        {disableCoupons.get('unReachPrice').size > 0 && (
          <div>
            <p style={{color:'#333',fontSize:'0.28rem'}}>未达到使用门槛({disableCoupons.get('unReachPrice').size})</p>
            {disableCoupons
              .get('unReachPrice')
              .map((coupon) => this.renderItem(coupon))}
          </div>
        )}

        {disableCoupons.get('noAvailableSku').size > 0 && (
          <div>
            <p style={{color:'#333',fontSize:'0.28rem'}}>本单商品不可使用({disableCoupons.get('noAvailableSku').size})</p>
            {disableCoupons
              .get('noAvailableSku')
              .map((coupon) => this.renderItem(coupon))}
          </div>
        )}

        {disableCoupons.get('unReachTime').size > 0 && (
          <div>
            <p style={{color:'#333',fontSize:'0.28rem'}}>未达到使用时间({disableCoupons.get('unReachTime').size})</p>
            {disableCoupons
              .get('unReachTime')
              .map((coupon) => this.renderItem(coupon))}
          </div>
        )}
      </div>
    ) : (
      <Blank img={require('../img/no-coupon.png')} content="啊哦，什么券都没有~" />
    );
  }

  /**
   * 渲染可用优惠券
   */
  renderItem = (coupon) => {
    const { onChooseCoupon } = this.props.relaxProps;
    const flag = coupon.get('platformFlag');
    const disabled = coupon.get('disabled');

    return (
      <div
        className={`item ${disabled ? 'gray-item' : flag ? '' : 'store-item'}`}
        key={coupon.get('couponCodeId')}
        onClick={() => {
          if (coupon.get('disabled')) {
            return;
          }
          onChooseCoupon(
            !coupon.get('chosen'),
            coupon.get('couponCodeId'),
            coupon.get('storeId')
          );
        }}
        style={disabled ? {background:`url(${expired}) top center / cover`} : flag ? {background:`url(${common}) top center / cover`} : {background:`url(${store}) top center / cover`}}
      >
        <div className="left">
          <p className="money">
            ￥
            <span className="money-text">{coupon.get('denomination')}</span>
          </p>
          <p className="left-tips">
            {coupon.get('fullBuyType') == 0
              ? '无门槛'
              : `满${coupon.get('fullBuyPrice')}可用`}
          </p>
          {coupon.get('nearOverdue') && (
            <img src={require('../img/expiring.png')} alt="" />
          )}
        </div>
        <div className="right">
          <div className="right-top">
            <i
              style={disabled ? {background:'#ccc'} : flag ? {background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))'} : {background:'linear-gradient(135deg,rgba(255,139,8,1),rgba(247,187,88,1))'}}
            >{flag ? '通用券' : '店铺券'}</i>
            <p>
              <span style={{color:'#333'}}>
                {flag ? '全平台可用' : `仅${coupon.get('storeName')}可用`}
              </span>
            </p>
          </div>
          <div className="range-box" style={{color:'#333'}}>
            限<span>{`${coupon.get('scopeTypeStr')}`}</span>可用
          </div>
          <div className="bottom-box" style={{color:'#999',fontSize:'0.22rem'}}>{`${_.formatDay(
            coupon.get('startTime')
          )}——${_.formatDay(coupon.get('endTime'))}`}</div>
          {coupon.get('chosen') && (
            <img className="check-img" src={require('../img/selected.png')} />
          )}
          {disabled && (<div className="gray-state">
            <img src={require('../img/cant-use.png')} alt="" style={{width:'1.2rem',height:'1.2rem'}} />
          </div>)}
        </div>
      </div>
    );
  };
}
