import React from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class Tab extends React.Component<any, any> {
  props: {
    relaxProps?: {
      tabKey: number;
      enableCount: number;
      disableCount: number;
      changeTabKey: Function;
      changeShow: Function;
    };
  };

  static relaxProps = {
    tabKey: 'tabKey',
    enableCount: 'enableCount',
    disableCount: 'disableCount',
    changeTabKey: noop,
    changeShow: noop
  };
  render() {
    const {
      changeShow,
      tabKey,
      changeTabKey,
      enableCount,
      disableCount
    } = this.props.relaxProps;

    return (
      <div className="tab">
        <div className="tab-left">
          <a
            href="javascript:;"
            className={tabKey == 0 ? 'actived' : ''}
            onClick={() => changeTabKey(0)}
          >
            可用优惠券({enableCount})
          </a>
          <a
            href="javascript:;"
            className={tabKey == 1 ? 'actived' : ''}
            onClick={() => changeTabKey(1)}
          >
            不可用优惠券({disableCount})
          </a>
        </div>
        {/* <i
          className="iconfont icon-help"
          onClick={() => {
            changeShow();
          }}
        /> */}
      </div>
    );
  }
}
