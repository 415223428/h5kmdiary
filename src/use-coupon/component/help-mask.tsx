import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';
/**
 * 使用说明弹窗
 */
@Relax
export default class HelpMask extends Component<any, any> {
  props: {
    relaxProps?: {
      showHelp: boolean;
      changeShow: Function;
    };
  };

  static relaxProps = {
    showHelp: 'showHelp',
    changeShow: noop
  };

  render() {
    const { showHelp, changeShow } = this.props.relaxProps;

    return (
      showHelp && (
        <div className="help-container">
          <div className="help-box">
            <div className="help-top">
              优惠券使用说明：<br />
              1、优惠券有使用期限限制，过了有效期不能使用；<br />
              2、每家店铺只能使用一张店铺券；<br />
              3、每次提交的订单只能使用一张通用券；<br />
              4、每次提交的订单只能使用一张运费券；<br />
              5、商品需在排除满系营销活动的优惠金额后判断是否满足店铺券使用门槛；<br />
              6、商品需在排除满系营销活动以及店铺券的优惠金额后判断是否满足通用券使用门槛；<br />
              7、订单需在排除满系营销活动、店铺券以及通用券的优惠金额后判断是否满足运费券使用门槛；<br />
            </div>
          </div>
          <img
            src={require('../img/close.png')}
            className="close-img"
            onClick={() => changeShow()}
          />
        </div>
      )
    );
  }
}
