import { Fetch } from 'wmkit';

/**
 * 使用优惠券选择时的后台处理
 */
export const checkoutCoupons = (couponCodeIds) => {
  return Fetch('/coupon-code/checkout-coupons', {
    method: 'POST',
    body: JSON.stringify({
      couponCodeIds
    })
  });
};
