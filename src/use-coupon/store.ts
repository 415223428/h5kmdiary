import { fromJS } from 'immutable';
import { Store } from 'plume2';
import CateActor from './actor/cate-actor';
import * as webapi from './webapi';
import { storage, history } from 'wmkit';
import { config, cache } from 'config';

export default class AppStore extends Store {
  bindActor() {
    return [new CateActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }
  /**
   * 弹层的显示隐藏
   */
  changeShow = () => {
    this.dispatch('change:changeShow');
  };

  /**
   * 初始化
   */
  init = (coupons, couponPageInfo, storeIds) => {
    console.log(coupons.toJS());
    console.log('+++++++++++++++++++___');
    this.dispatch('init', { coupons, storeIds, couponPageInfo });
    console.log(this.state().get('enableCoupons').toJS());
    console.log('}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}');
  };

  /**
   * 修改选中的页面
   */
  changeTabKey = (key) => {
    this.dispatch('change:tab:key', key);
  };

  /**
   * 选择优惠券
   */
  onChooseCoupon = async (flag, couponCodeId, storeId) => {
    if (storeId == -1) {
      if (flag) {
        // 1.如果是选择平台券
        this.dispatch('choose:common:coupon', couponCodeId);
      } else {
        // 2.如果是取消选择平台券
        this.dispatch('clean:common:coupon');
      }
    } else {
      // 3.设置店铺券
      this.dispatch('choose:store:coupon', { flag, couponCodeId, storeId });
    }

    // 4.根据已选的券，查询不满足门槛的平台券，以及优惠券优惠总价
    const enableCoupons = this.state()
      .get('enableCoupons')
      .toJS();
    let couponCodeIds = [];
    // 已选平台券
    couponCodeIds = couponCodeIds.concat(
      enableCoupons.commonCoupons
        .filter((c) => c.chosen == true)
        .map((c) => c.couponCodeId)
    );

    let storeCoupons = [];
    enableCoupons.stores.forEach(
      (store) => (storeCoupons = storeCoupons.concat(store.coupons))
    );

    // 已选店铺券
    couponCodeIds = couponCodeIds.concat(
      storeCoupons.filter((c) => c.chosen == true).map((c) => c.couponCodeId)
    );
    const res = (await webapi.checkoutCoupons(couponCodeIds)) as any;
    if (res.code == config.SUCCESS_CODE) {
      // 5.重新设置选择的平台券和优惠券优惠总价
      let { unreachedIds, couponTotalPrice, checkGoodsInfos } = res.context;
      const chosenCommon = this.state()
        .getIn(['enableCoupons', 'commonCoupons'])
        .find((c) => c.get('chosen') == true);
      this.dispatch('clean:common:coupon');
      this.dispatch('set:unreached:ids', {
        unreachedIds: fromJS(unreachedIds),
        couponTotalPrice,
        checkGoodsInfos: fromJS(checkGoodsInfos)
      });
      if (
        chosenCommon &&
        !unreachedIds.includes(chosenCommon.get('couponCodeId'))
      ) {
        this.dispatch('choose:common:coupon', chosenCommon.get('couponCodeId'));
      }
    }
  };

  onSubmit = async () => {
    storage('session').set(
      cache.ORDER_CONFIRM_COUPON,
      JSON.stringify(this.state().toJS())
    );
    history.goBack();
  };
}
