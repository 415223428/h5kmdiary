import { Action, Actor } from 'plume2';
import { fromJS } from 'immutable';
import { IList } from 'typings/globalType';

export default class GoodsActor extends Actor {
  defaultState() {
    return {
      customer: {},
      pcLogo: [],
      invitState: false,
      baseInfo: fromJS({}),
      // 分享弹出显示与否
      shareVisible: false,
      // 分享赚选中的sku
      chosenSku: {},
      //分销员信息
      customerInfo: fromJS({}),
       //分销设置
       settingInfo: fromJS({}),
      isfenxiang:false,
      closeImg:true,
      fenxiangId:'',
      closeChoose:'closeChoose',
      isWechatShare:false,
    };
  }
/**
   * logo
   * @param state
   * @param config
   */
  @Action('logo:init')
  init(state, config: IList) {
    return state
      .set('pcLogo', config)
  }
  /**
   * 切换分享赚弹框显示与否
   */
  @Action('goodsActor:changeShareVisible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }
/**
   * 分享店铺弹层显示
   */
  @Action('goods-actor:changeInvitState')
  changeInvitState(state) {
    return state.set('goods-actor', !state.get('invitState'));
  }
  /**
   * 分享赚选中的sku
   * @param state
   * @param context
   * @returns {any}
   */
  @Action('goods-actor:setSkuContext')
  setSkuContext(state, context) {
    return state.set('chosenSku', fromJS(context));
  }

  @Action('goods-actor:shopShareImg')
  shopShareImg(state, img) {
    return state.setIn(['baseInfo', 'shopShareImg'], img);
  }

    /**
   * 分销员基本信息
   */
  @Action('goodsActor:baseInfo')
  setBaseInfo(state, { customerInfo, settingInfo }) {
    return state
      .setIn(['baseInfo', 'customerName'], customerInfo.get('customerName'))
      .setIn(['baseInfo', 'shopName'], settingInfo.get('shopName'))
      .setIn(['baseInfo', 'headImg'], customerInfo.get('headImg'))
      .setIn(
        ['baseInfo', 'distributorName'],
        settingInfo.get('distributorName')
      )
      .set('customerInfo', customerInfo)
      .set('settingInfo', settingInfo);
  }

    //打开分享页面
    @Action('goods-actor:isfenxiang')
    isfenxiang(state){
      return state.set('isfenxiang',true)
    }
    
  //关闭分享页面
    @Action('goods-actor:closefenxiang')
    closefenxiang(state){
      return state.set('isfenxiang',false)
    }
  
    //关闭img
    @Action('goods-actor:closeImg')
    closeImg(state){
      return state.set('closeImg',false)
      .set('closeChoose',false)
    }
    
    //打开Img
    @Action('goods-actor:openImg')
    openImg(state){
      return state.set('closeImg',true)
      .set('closeChoose',true)
    }
  
    @Action('goods-actor:fenxiangId')
    sendId(state,fenxiangId){
      return state.set('fenxiangId',fenxiangId)
    }

    @Action('goods-actor:openWechatShare')
    openWechatShare(state){
      return state.set('isWechatShare',true)
    }
  
    @Action('goods-actor:closeWechatShare')
    closeWechatShare(state){
      return state.set('isWechatShare',false)
    }
    @Action('member:init1')
    init1(state, { customer }) {
      return state
        .set('customer', fromJS(customer))
    }
}
