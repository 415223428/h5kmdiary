import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class GoodsTabSort extends Component<any, any> {
  props: {
    relaxProps?: {
      closeShade: Function;
      setSort: Function;
      sortType: IMap;
    };
  };

  static relaxProps = {
    closeShade: noop,
    setSort: noop,
    sortType: 'sortType'
  };

  render() {
    const { closeShade, setSort, sortType } = this.props.relaxProps;

    // 排序字段
    const type = sortType.get('type');
    // 排序方式：升序 降序
    const sort = sortType.get('sort');

    return (
      <div style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
        <div style={{ backgroundColor: '#ffffff', width: '100%' }}>
          {/*<div
            className={
              type == 'default' || type == ''
                ? 'sort-item new-theme-text'
                : 'sort-item'
            }
            onClick={() => setSort('default')}
          >
            默认
          </div>*/}
          <div
            className={
              type == 'dateTime' || type == ''
                ? 'sort-item new-theme-text'
                : 'sort-item'
            }
            onClick={() => setSort('dateTime')}
          >
            最新
          </div>
          <div
            className={
              type == 'salesNum' ? 'sort-item new-theme-text' : 'sort-item'
            }
            onClick={() => setSort('salesNum')}
          >
            销量
          </div>

          <div className="sort-item" onClick={() => setSort('commission')}>
            <div className={type == 'commission' ? 'new-theme-text' : ''}>
              佣金
            </div>
            <div
              className={
                type == 'commission' && sort == 'asc'
                  ? 'sort-icon-box new-theme-text'
                  : 'sort-icon-box'
              }
            >
              <i className="iconfont icon-xiangshangpai" />
            </div>
            <div
              className={
                type == 'commission' && sort == 'desc' ? 'new-theme-text' : ''
              }
            >
              <i className="iconfont icon-xiangxiapai" />
            </div>
          </div>

          <div className="sort-item" onClick={() => setSort('price')}>
            <div className={type == 'price' ? 'new-theme-text' : ''}>价格</div>
            <div
              className={
                type == 'price' && sort == 'asc'
                  ? 'sort-icon-box new-theme-text'
                  : 'sort-icon-box'
              }
            >
              <i className="iconfont icon-xiangshangpai" />
            </div>
            <div
              className={
                type == 'price' && sort == 'desc' ? 'new-theme-text' : ''
              }
            >
              <i className="iconfont icon-xiangxiapai" style={{margin: '0 -5px'}}/>
            </div>
          </div>
        </div>

        <div
          style={{ width: '100%', height: '100vh' }}
          onClick={() => closeShade()}
        />
      </div>
    );
  }
}
