import React, { Component } from 'react';
import { List, Map } from 'immutable';
import { IMap, Relax } from 'plume2';
import { noop } from 'wmkit';

const styles = require('../css/style.css');
const reset=require('../img/reset.png')
const confirm=require('../img/confirm.png')
@Relax
export default class GoodsTabFilter extends Component<any, any> {
  props: {
    relaxProps?: {
      closeShade: Function;
      handleFilterChange: Function;
      brandExpanded: boolean;
      expandProp: IMap;
      brandList: any;
      selectedBrandIds: any;
      selectSelfCompany: boolean;
      selectedItem: boolean;
      goodsProps: any;
    };
    hide: boolean;
  };

  static relaxProps = {
    closeShade: noop,
    handleFilterChange: noop,
    brandExpanded: 'brandExpanded',
    expandProp: 'expandProp',
    brandList: 'brandList',
    selectedBrandIds: 'selectedBrandIds',
    selectSelfCompany: 'selectSelfCompany',
    selectedItem: 'selectedItem',
    goodsProps: 'goodsProps'
  };

  state: {
    selectSelfCompany: boolean;
    selectedItem: boolean;
    selectedBrandIds: any;
    goodsProps: any;
    brandExpanded: boolean;
    expandProp: any;
  };

  constructor(props) {
    super(props);

    this.state = {
      // 已选中商城服务列表
      selectSelfCompany: false,
      selectedItem: false,
      // 已选中品牌编号列表.
      selectedBrandIds: List(),
      goodsProps: List(),
      brandExpanded: false,
      expandProp: Map() 
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectSelfCompany: nextProps.relaxProps.selectSelfCompany,
      selectedItem: nextProps.relaxProps.selectedItem,
      selectedBrandIds: nextProps.relaxProps.selectedBrandIds,
      goodsProps: nextProps.relaxProps.goodsProps,
      brandExpanded: nextProps.relaxProps.brandExpanded,
      expandProp: nextProps.relaxProps.expandProp
    });
  }

  render() {
    if (this.props.hide) {
      return null;
    }

    let { closeShade, brandList } = this.props.relaxProps;
    let goodsProps = this.state.goodsProps;

    brandList = (brandList && brandList.toJS()) || [];
    goodsProps = (goodsProps && goodsProps.toJS()) || [];
    const brandExpanded = this.state.brandExpanded;

    return (
      <div style={{ width: '100%' }}>
        <div className="filter-box">
          <div className="filter-content">
            <div className="filter-item">
              <div className="filter-head">
                <h4>商城服务</h4>
              </div>
              <div className="filter-sort">
                {
                  <span
                    // className={`${styles.item} ${this.state.selectSelfCompany &&
                    //   'select-item-checked'}`}
                    className={`${this.state.selectSelfCompany?styles.selected:styles.item} ${this.state.selectSelfCompany &&
                      'select-item-checked'}`}
                    onClick={() => this._handleClickService('companyType')}
                  >
                    自营商品
                  
                  </span>
                }

                {/*<span
                  className={`${styles.item} ${this.state.selectedItem &&
                    'select-item-checked'}`}
                  onClick={() => this._handleClickService('selectedItem')}
                >
                  隐藏已选商品
                
                </span>*/}
              </div>
            </div>

            {/*品牌*/}
            {brandList.length > 0 ? (
              <div className="filter-item">
                <div className="filter-head">
                  <h4>品牌</h4>
                  {brandList.length > 9 && (
                    <i
                      className="iconfont icon-jiantou2"
                      onClick={() =>
                        this.setState({ brandExpanded: !brandExpanded })
                      }
                      style={
                        brandExpanded ? { transform: 'rotate(180deg)' } : {}
                      }
                    />
                  )}
                </div>
                <div className="filter-sort">
                  {brandList.map((v, index) => {
                    if (!brandExpanded && index >= 9) {
                      return null;
                    }

                    // 在已选中品牌编号列表中的索引
                    const selectedIndex = this.state.selectedBrandIds.findIndex(
                      (brandId) => brandId == v.brandId
                    );
                    // 索引大于-1代表已选中
                    const selected = selectedIndex > -1;

                    return selected ? (
                      <span
                        key={`${v.brandId}_1`}
                        className={`${styles.selected} select-item-checked`}
                        onClick={() => this._handleClickBrand(v, selectedIndex)}
                      >
                        {v.brandName}{' '}
                        {v.nickName ? '(' + v.nickName + ')' : null}
                        <i className="iconfont icon-gou" />
                      </span>
                    ) : (
                      <span
                        key={`${v.brandId}_0`}
                        className={styles.item}
                        onClick={() => this._handleClickBrand(v, selectedIndex)}
                      >
                        {v.brandName}{' '}
                        {v.nickName ? '(' + v.nickName + ')' : null}
                      </span>
                    );
                  })}
                </div>
              </div>
            ) : null}

            {/*属性名称与属性值*/}
            {goodsProps.map((prop) => {
              const propId = prop.propId.toString();
              const details = prop.goodsPropDetails || [];

              // 此属性筛选项是否已展开
              const propExpand = this.state.expandProp.get(propId) || false;

              return (
                <div className="filter-item" key={propId}>
                  <div className="filter-head">
                    <h4>{prop.propName}</h4>
                    {details.length > 9 && (
                      <i
                        className="iconfont icon-jiantou2"
                        onClick={() => {
                          let expandProp = this.state.expandProp;
                          expandProp = expandProp.set(propId, !propExpand);
                          this.setState({ expandProp });
                        }}
                        style={
                          propExpand ? { transform: 'rotate(180deg)' } : {}
                        }
                      />
                    )}
                  </div>
                  <div className="filter-sort">
                    {details.map((detail, detailIndex) => {
                      if (!propExpand && detailIndex >= 9) {
                        return null;
                      }

                      const { detailId, detailName, checked } = detail;
                      return checked ? (
                        <span
                          key={`${propId}_${detailId}_1`}
                          className={`${styles.item} select-item-checked`}
                          onClick={() =>
                            this._handleClickProp(propId, detailId, false)
                          }
                        >
                          {detailName}
                          <i className="iconfont icon-gou" />
                        </span>
                      ) : (
                        <span
                          key={`${propId}_${detailId}_0`}
                          className={styles.item}
                          onClick={() =>
                            this._handleClickProp(propId, detailId, true)
                          }
                        >
                          {detailName}
                        </span>
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </div>

          {/*底部按钮*/}
          <div className="filter-btns" style={{display:'flex',justifyContent:'space-around'}}>
            {/* <button
              className="fix-btn reset-btn"
              onClick={() => this._handleReset()}
            >
              重置
            </button> */}
            <img src={reset} alt="" onClick={() => this._handleReset()}/>
            <img src={confirm} alt="" onClick={() => {
                  closeShade();
                  this._handleConfirm();
                }}/>
            {/* <button
              className="check-btn"
              onClick={() => {
                closeShade();
                this._handleConfirm();
              }}
            >
              确定
            </button> */}
          </div>
        </div>

        <div
          style={{ width: '100%', height: '100vh' }}
          onClick={() => closeShade()}
        />
      </div>
    );
  }

  /**
   * 处理点击商城服务
   * @param serviceType 服务类型
   * @private
   */
  _handleClickService = (serviceType: String) => {
    let { selectSelfCompany, selectedItem } = this.state;
    if (serviceType == 'selectedItem') {
      this.setState({
        selectedItem: !selectedItem
      });
    } else {
      this.setState({
        selectSelfCompany: !selectSelfCompany
      });
    }
  };

  /**
   * 处理点击品牌
   * @param brand 品牌对象
   * @param selectedIndex 在已选中品牌编号列表中的位置
   * @private
   */
  _handleClickBrand = (brand: any, selectedIndex: number) => {
    let { selectedBrandIds } = this.state;

    // selectedIndex等于-1时，该品牌没有选中，添加到选中队列，否则删除
    selectedBrandIds =
      selectedIndex > -1
        ? selectedBrandIds.delete(selectedIndex)
        : selectedBrandIds.push(brand.brandId);

    this.setState({
      selectedBrandIds: selectedBrandIds
    });
  };

  /**
   * 处理点击属性值
   */
  _handleClickProp = (propId, detailId, checked) => {
    let { goodsProps } = this.state;
    if (goodsProps && goodsProps.size > 0) {
      const index = goodsProps.findIndex(
        (prop) => prop.get('propId') == propId
      );
      if (index > -1) {
        const goodsPropDetails = goodsProps.get(index).get('goodsPropDetails');
        if (goodsPropDetails && goodsPropDetails.size > 0) {
          if (detailId != null) {
            const detailIndex = goodsPropDetails.findIndex(
              (detail) => detail.get('detailId') == detailId
            );
            if (detailIndex > -1) {
              goodsProps = goodsProps.setIn(
                [index, 'goodsPropDetails', detailIndex, 'checked'],
                checked
              );
              this.setState({ goodsProps: goodsProps });
            }
          }
        }
      }
    }
  };

  /**
   * 清空搜索条件
   * @private
   */
  _handleReset = () => {
    // 清空选中属性
    let { goodsProps } = this.state;
    if (goodsProps && goodsProps.size > 0) {
      goodsProps = goodsProps.map((v) => {
        let details = v.get('goodsPropDetails');
        if (details && details.size > 0) {
          details = details.map((detail) => {
            return detail.get('checked')
              ? detail.set('checked', false)
              : detail;
          });
          v = v.set('goodsPropDetails', details);
        }
        return v;
      });
    }

    this.setState({
      selectSelfCompany: false,
      selectedItem: false,
      selectedBrandIds: List(),
      goodsProps: goodsProps,
      brandExpanded: Map(),
      expandProp: Map()
    });
  };

  /**
   * 点击确认按钮
   * @private
   */
  _handleConfirm = () => {
    let { handleFilterChange } = this.props.relaxProps;
    handleFilterChange(
      this.state.selectSelfCompany,
      this.state.selectedItem,
      this.state.selectedBrandIds,
      this.state.goodsProps,
      this.state.brandExpanded,
      this.state.expandProp
    );
  };
}
