import React from 'react';
import { Link } from 'react-router-dom';
import { msg, Store } from 'plume2';
import { WMImage, _, Alert, history, WMkit } from 'wmkit';
import { GoodsNum } from 'biz';
import GoodsShareImg from './goods-share-img'
import { cache } from 'config';

const noneImg = require('../img/list-none.png');
const faquan=require('../img/faquan.png')
const fenxiang=require('../img/fenxiang.png')
const ziying=require('../img/self-support.png')
const queHuo =require('../img/quehuo.png')

export default class GoodsItem extends React.Component<any, any> {
  
  constructor(props) {
    super(props);
    this.state = props;
  }
  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }
  render() {
    const {
      goodsItem,
      changefenxiang,
      changeShareVisible,
      saveCheckedSku,
      sendId,
      isfenxiang,
      customer,
      cloasefenxiang,
      closeImg,
      iscloseImg,
      isopenImg,
      fenxiangId,
      baseInfo,
      // FIXME 销量、评价、好评展示与否，后期读取后台配置开关
      isShow,
      closeChoose,
      isDistributor,
      openWechatShare,
      isWechatShare,
    } = this.state;
    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    let params=goodsInfo.get('goodsInfoId');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 起订量
    const count = goodsInfo.get('count') || 0;
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;

    let containerClass = invalid ? 'goods-list invalid-goods' : 'goods-list';
    const isdistributor = WMkit.isShowDistributionButton();
    const inviteeId =isdistributor? JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId :'';
    const inviteCode=isdistributor?sessionStorage.getItem('inviteCode'):'';
    const url = `goods-detail/${goodsInfo.get('goodsInfoId')}/?channel=mall&inviteeId=${inviteeId}&inviteCode=${inviteCode}`;
    //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
    //好评率
    let favorableRate = '100';
    if (
      goodsInfo.get('goodsEvaluateNum') &&
      goodsInfo.get('goodsEvaluateNum') != 0
    ) {
      favorableRate = _.mul(
        _.div(
          goodsInfo.get('goodsFavorableCommentNum'),
          goodsInfo.get('goodsEvaluateNum')
        ),
        100
      ).toFixed(0);
    }

    //评论数
    let evaluateNum = '暂无';
    const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
    if (goodsEvaluateNum) {
      if (goodsEvaluateNum < 10000) {
        evaluateNum = goodsEvaluateNum;
      } else {
        const i = _.div(goodsEvaluateNum, 10000).toFixed(1);
        evaluateNum = i + '万+';
      }
    }

    //销量
    let salesNum = '暂无';
    const goodsSalesNum = goodsInfo.get('goodsSalesNum');
    if (goodsSalesNum) {
      if (goodsSalesNum < 10000) {
        salesNum = goodsSalesNum;
      } else {
        const i = _.div(goodsSalesNum, 10000).toFixed(1);
        salesNum = i + '万+';
      }
    }
    // 预估点数
    let pointNum = goodsInfo.get('kmPointsValue');
    //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆

    return (
      <div>
        {/* {isDistributor?
        <GoodsShareImg
          goodsItem={goodsItem}
          isfenxiang={isfenxiang}
          customer={customer}
          cloasefenxiang={cloasefenxiang}
          closeImg={closeImg}
          iscloseImg={iscloseImg}
          isopenImg={isopenImg}
          fenxiangId={fenxiangId}
          baseInfo={baseInfo}
          params={params}
          closeChoose={closeChoose}
          openWechatShare = {openWechatShare}
          isWechatShare={isWechatShare}
        />:null} */}
        <div
          key={id}
          className={containerClass}
          onClick={() =>
            history.push('/goods-detail/' + goodsInfo.get('goodsInfoId'))
          }
        >
          <div className="img-box" style={{margin:'0.2rem 0.2rem 0.2rem 0.1rem'}}>
            <WMImage
              mode="pad"
              src={goodsInfo.get('goodsInfoImg') || noneImg}
              width="100%"
              height="100%"
            />
          </div>
          <div className="detail b-1px-b" style={{marginTop:'0.1rem'}}>
            <div className="title">{goodsInfo.get('goodsInfoName')}</div>
            <p className="gec">{goodsInfo.get('specText')}</p>

            {/* 评价 */}
            {isShow ? (
              <div className="goods-evaluate">
                <span className="goods-evaluate-spn">{salesNum}销量</span>
                <span className="goods-evaluate-spn mar-lr-28">
                  {evaluateNum}
                  评价
                </span>
                <span className="goods-evaluate-spn">
                  {favorableRate}
                  %好评
                </span>
                {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
              </div>
            ) : (
              <div className="goods-evaluate">
                <span className="goods-evaluate-spn">{salesNum}销量</span>
              </div>
            )}

            {goodsInfo.get('companyType') == 0 && (
              <div className="marketing">
                {/* <div className="self-sales">自营</div> */}
                <img src={ziying} alt="" style={{width: '0.6rem'}}/>
              </div>
            )}

            <div className="bottom" style={{flexDirection:'column',alignItems:'flex-start',height:'100%'}}>
              <div className="bottom-price">
                <span className={invalid?'gray-price':'price'} style={{fontSize:'0.4rem'}}>
                  <i className="iconfont icon-qian" style={{marginLeft:'-0.06rem'}}/>
                  {_.addZero(goodsInfo.get('marketPrice'))}
                </span>
                {isDistributor && (
                  <span className="commission">
                    &nbsp;赚
                    <span style={invalid?{color:'#999',fontSize:'0.25rem'}:{color:'#FF4D4D',fontSize:'0.25rem'}}>
                      <i className="iconfont icon-qian" style={{fontSize:'0.25rem'}}/>
                      {_.addZero(goodsInfo.get('distributionCommission'))}
                    </span>
                  </span>
                )}
                {/* {invalid && <div className="out-stock">缺货</div>} */}
                {invalid && <div className="QHuo"><img src={queHuo} style={{padding:0}}/></div>}
              </div>
              {!invalid ? (
                <div>
                  {isDistributor ? (
                    <div style={{ display: 'flex' }}>
                      <div
                        // className="social-btn social-btn-ghost"
                        className='social-btn-box'
                        onClick={async (e) => {
                          e.stopPropagation();
                          const result = await WMkit.getDistributorStatus();
                          if ((result.context as any).distributionEnable) {
                            history.push(
                              '/graphic-material/' + goodsInfo.get('goodsInfoId')
                            );
                          } else {
                            let reason = (result.context as any).forbiddenReason;
                            msg.emit('bStoreCloseVisible', {
                              visible: true,
                              reason: reason
                            });
                          }
                        }}
                      >
                        <img src={faquan} alt="" style={{padding:0}}/>
                      </div>
                      <div
                        // className="social-btn"
                        className='social-btn-box'

                        onClick={
                          async (e)=>{
                            e.stopPropagation();
                            // sendId(goodsItem.get('id'));
                            // changefenxiang();
                            history.push({
                              pathname: '/share',
                              state: {
                                goodsInfo ,
                                url,
                                id,
                              }
                            });
                        }}
                      >
                        <img src={fenxiang} alt="" style={{padding:0}}/>
                      </div>
                    </div>
                  ) : (
                    <GoodsNum
                      value={buyCount}
                      max={stock}
                      disableNumberInput={invalid}
                      goodsInfoId={goodsInfo.get('goodsInfoId')}
                    />
                  )}
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
