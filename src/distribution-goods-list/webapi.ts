import { Fetch } from 'wmkit';

type TResult = { code: string; message: string; context: any };

/**
 * 根据分类id查询商品属性
 * @param params
 */
export const queryProps = (cateId) =>
  Fetch<TResult>(`/goods/props/${cateId}`, { method: 'GET' });
export const shareInfo = () => {
  return Fetch('/customer/customerCenter')
}
