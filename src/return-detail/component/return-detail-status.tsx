import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { Const } from 'config';
import { _ } from 'wmkit';

@Relax
export default class ReturnDetailStatus extends Component<any, any> {
  props: {
    relaxProps?: {
      detail: IMap;
      rejectReason: string;
    };
  };

  static relaxProps = {
    detail: 'detail',
    rejectReason: 'rejectReason'
  };

  render() {
    const { detail, rejectReason } = this.props.relaxProps;
    const returnFlowState = detail.get('returnFlowState');

    let rejectLabel = '';
    switch (returnFlowState) {
      case 'REJECT_RECEIVE':
        rejectLabel = '拒绝收货原因';
        break;
      case 'REJECT_REFUND':
        rejectLabel = '拒绝退款原因';
        break;
      case 'VOID':
        rejectLabel = '作废原因';
        break;
    }

    const labelStatus =
      detail.get('returnType') == 'RETURN'
        ? Const.returnGoodsState[returnFlowState]
        : Const.returnMoneyState[returnFlowState];

    return (
      <div className="order-status address-box" >
        <div className="status">
          {/* <span>退单状态</span> */}
          <span>订单状态</span>
          <span className="result">{labelStatus}</span>
        </div>
        {'REJECT_RECEIVE' == returnFlowState ||
        'REJECT_REFUND' == returnFlowState ||
        'VOID' == returnFlowState ? (
          <div className="status">
            <span className="text">{rejectLabel}</span>
            <span className="text text-wrap">{rejectReason}</span>
          </div>
        ) : null}
        <div className="status">
          <span className="text">订单号</span>
          <span className="text">{detail.get('tid')}</span>
        </div>
        <div className="status">
          <span className="text">退单号</span>
          <span className="text">
            {detail.get('platform') != 'CUSTOMER' && <span>代</span>}
            {detail.get('id')}
          </span>
        </div>
        <div className="status">
          <span className="text">退单时间</span>
          <span className="text">{_.formatDate(detail.get('createTime'))}</span>
        </div>
      </div>
    );
  }
}
