import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import {
  FormSelect,
  FormItem,
  FormText,
  history,
  ImageListScroll,
  WMkit
} from 'wmkit';
import { fromJS } from 'immutable';

const noneImg = require('../img/emptygoods.png');

@Relax
export default class ReturnDetailBody extends Component<any, any> {
  props: {
    relaxProps?: {
      detail: IMap;
      pointsIsOpen: boolean;
    };
  };

  static relaxProps = {
    detail: 'detail',
    pointsIsOpen: 'pointsIsOpen'
  };

  render() {
    const { detail, pointsIsOpen } = this.props.relaxProps;

    const rid = detail.get('id');

    const returnItems = detail.get('returnItems') || fromJS([]);
    const returnGifts = detail.get('returnGifts') || fromJS([]);
    //全部商品(商品+赠品)
    const items = returnItems.concat(returnGifts);

    const returnReason = detail.get('returnReason')
      ? detail.get('returnReason').toJS()
      : {};
    const returnWay = detail.get('returnWay')
      ? detail.get('returnWay').toJS()
      : {};

    // 附件
    const attachments = [];
    detail.get('images') &&
      detail.get('images').map((v) => {
        return attachments.push({ image: JSON.parse(v).url });
      });

    const returnType = detail.get('returnType');

    // 总额
    const totalPrice = detail.getIn(['returnPrice', 'totalPrice']) || 0;
    // 改价金额
    const actualReturnPrice = detail.getIn([
      'returnPrice',
      'actualReturnPrice'
    ]);
    // 应退金额，如果对退单做了改价，使用actualReturnPrice，否则，使用总额totalPrice
    const payPrice = actualReturnPrice == null ? totalPrice : actualReturnPrice;

    const storeId = detail.getIn(['company', 'storeId']);
    const companyType = detail.getIn(['company', 'companyType']);
    const storeName = detail.getIn(['company', 'storeName']);
    const returnFlowState = detail.get('returnFlowState');
    //分销渠道，分销员名称，店铺名称
    //分销渠道
    const channelType = WMkit.channelType();
    const inviteeName = detail.get('distributorName');
    const shopName = detail.get('shopName');
    const inviteeId = detail.get('inviteeId');

    return (
      <div>
        <div className="top-border address-box">
          <FormSelect
            iconVisible={false}
            style={{
              borderRadius: '10px 10px 0 0'
            }}
            labelName={
              WMkit.inviteeId() && WMkit.isShop() ? (
                <div>
                  {inviteeName}的{shopName}
                </div>
              ) : companyType == 0 ? (
                <div>
                  {storeName}
                  <div className="self-sales" style={{
                    marginLeft: '.3rem'
                  }}>自营</div>
                </div>
              ) : (
                    storeName
                  )
            }
            placeholder=""
            onPress={() => {
              WMkit.inviteeId() && WMkit.isShop()
                ? history.push(`/shop-index-c/${inviteeId}`)
                : history.push(`/store-main/${storeId}`);
            }}
          />
          <div
            className="limit-img sku-list-head"
            style={{ borderTop: 0 }}
            onClick={() => {
              history.push(`/return-sku-list/${rid}`);
            }}
          >
            <div className="img-content">
              {items.toJS().map((item, index) => {
                return index < 4 ? (
                  <img
                    className="img-item"
                    src={item.pic ? item.pic : noneImg}
                  />
                ) : null;
              })}
            </div>
            <div className="right-context">
              <div className="total-num">
                <div>共{items.size}种</div>
              </div>
              <i className="iconfont icon-jiantou1" />
            </div>
          </div>
          {/**退货才有物流信息**/
            returnType == 'RETURN' ? (
              <FormSelect
                labelName="退货物流信息"
                placeholder=""
                onPress={() => {
                  history.push(`/return-logistics-info/${detail.get('id')}`);
                }}
              />
            ) : null}
          <div className="top-border mb10 ">
            <FormSelect
              labelName="退款记录"
              placeholder=""
              onPress={() => {
                history.push({
                  pathname: `/finance-refund-record/${rid}`,
                  returnFlowState: returnFlowState
                });
              }}
              style={{ borderRadius: "0px 0px 10px 10px" }}
            />
          </div>
        </div>

        <div className="top-border mb10 address-box">
          <FormItem
            labelName="退货原因"
            placeholder={
              Object.getOwnPropertyNames(returnReason).map(
                (key) => returnReason[key]
              )[0]
            }
            style={{ borderRadius: "10px 10px 0px 0px" }}
          />

          {/**退货才有退货方式**/
            returnType == 'RETURN' ? (
              <FormItem
                labelName="退货方式"
                placeholder={
                  Object.getOwnPropertyNames(returnWay).map(
                    (key) => returnWay[key]
                  )[0]
                }
              />
            ) : null}

          <FormText
            label="退货说明"
            value={detail.get('description')}
            readOnly={true}
          />
        </div>

        <div className="address-box">
          <ImageListScroll imageList={attachments} labelName="退单附件" style={{ borderRadius: "10px" }} />
        </div>

        <div className="address-box">
          <div className="total-price sku-list-head" style={{ borderTop: "0px solid #ebebeb" }}>
            <div className="total-list">
              <span>应退金额</span>
              <span className="price-color">
                <i className="iconfont icon-qian" />
                {totalPrice.toFixed(2)}
              </span>
            </div>
            {returnFlowState == 'COMPLETED' && (
              <div className="total-list"
                style={!pointsIsOpen && ({ borderRadius: "0px 0px 10px 10px" })}
              >
                <span>实退金额</span>
                <span>
                  <i className="iconfont icon-qian" />
                  {payPrice.toFixed(2)}
                </span>
              </div>
            )}
          </div>

          {pointsIsOpen && (
            <div>
              <div className="total-price " style={{ borderTop: "0px solid #ebebeb" }}>
                <div className="total-list">
                  <span>应退积分</span>
                  <span className="price-color">
                    {detail.getIn(['returnPoints', 'applyPoints']) || 0}
                  </span>
                </div>
              </div>
              <div className="total-price sku-list-foot" style={{ borderTop: "0px solid #ebebeb" }}>
                <div className="total-list ">
                  <span>实退积分</span>
                  <span className="price-color">
                    {detail.getIn(['returnPoints', 'actualPoints']) || 0}
                  </span>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
