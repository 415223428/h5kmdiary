import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import { Button, Confirm, history } from 'wmkit';
import AppStore from './store';
import ReturnDetailStatus from './component/return-detail-status';
import ReturnDetailBody from './component/return-detail-body';
const styles = require('./css/style.css');

const SmallBlueButton = Button.SmallBlue1;

/**
 * 退单详情页面
 */
@StoreProvider(AppStore)
export default class ReturnDetail extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { rid } = this.props.match.params;
    this.store.init(rid);
    this.store.basicRules();
  }

  render() {
    const detail = this.store.state().get('detail');

    const rid = detail.get('id');
    // 退单类型 RETURN 退货, REFUND 退款
    const returnType = detail.get('returnType') || 'RETURN';
    const returnFlowState = detail.get('returnFlowState');

    return (
      <div className={styles.container}>
        <ReturnDetailStatus />
        <ReturnDetailBody />
        <div style={{ height: (returnFlowState === 'INIT' || returnFlowState === 'AUDIT' && returnType == 'RETURN') ? "1rem" : "0" }}>
          <div className="order-button-wrap " style={{
            height: (returnFlowState === 'INIT' || returnFlowState === 'AUDIT' && returnType == 'RETURN') ? "1rem" : "0",
            padding: (returnFlowState === 'INIT' || returnFlowState === 'AUDIT' && returnType == 'RETURN') ? "8px" : "0"
          }}>
            {returnFlowState === 'INIT' && (
              <SmallBlueButton
                onClick={() => {
                  Confirm({
                    title: '取消退货退款',
                    text: '您确定要取消该退货退款？',
                    confirmCb: () => {
                      this.store.cancel(rid);
                    },
                    okBtn: '确定',
                    cancelBtn: '取消'
                  });
                }}
                text="取消退订"
              />
            )}

            {/*退货单的已审核状态*/}
            {returnFlowState === 'AUDIT' &&
              returnType == 'RETURN' && (
                <SmallBlueButton
                  onClick={() => {
                    history.push({
                      pathname: `/logistics-input/${rid}`,
                      state: { storeId: detail.get('company').get('storeId') }
                    });
                  }}
                  text="填写物流"
                />
              )}
          </div>
        </div>
      </div>
    );
  }
}
