import { fromJS } from 'immutable';
import { Store } from 'plume2';
import { Alert, history } from 'wmkit';
import * as webapi from './webapi';
import DetailActor from './actor/detail-actor';
import { config } from 'config';

export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new DetailActor()];
  }

  init = async (rid: string) => {
    const { context, message, code } = await webapi.fetchReturnDetail(rid);
    if (code == 'K-000000') {
      this.dispatch('order-return-detail:init', fromJS(context));
      const returnFlowState = this.state().getIn(['detail', 'returnFlowState']);

      if ('REJECT_RECEIVE' == returnFlowState || 'VOID' == returnFlowState) {
        // 拒绝收货 或者 审核驳回
        this.dispatch(
          'order-return-detail:rejectReason',
          this.state().getIn(['detail', 'rejectReason']) || ''
        );
      } else if ('REJECT_REFUND' == returnFlowState) {
        // 拒绝退款
        let res = await webapi.fetchRefundOrderList(rid);

        if (res.code == 'K-000000') {
          const result = fromJS(res.context);
          this.dispatch(
            'order-return-detail:rejectReason',
            result.get('refuseReason') || ''
          );
        }
      }
    } else {
      Alert({ text: message });
      history.replace('/refund-list');
    }
  };

  cancel = async (rid: string) => {
    const { message, code } = await webapi.cancel(rid, '用户取消');

    if (code == 'K-000000') {
      Alert({ text: '成功取消退单' });
      this.init(rid);
    } else {
      Alert({ text: message });
    }
  };

  basicRules = async () => {
    let res: any = await webapi.basicRules();
    if (res && res.code === config.SUCCESS_CODE && res.context.status === 1)
      this.dispatch('userInfo:pointsIsOpen');
  };
}
