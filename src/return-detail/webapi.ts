import { Fetch } from 'wmkit';

/**
 * 获取退单详情
 */
export const fetchReturnDetail = (rid) => {
  return Fetch<Result<any>>(`/return/${rid}`, {
    method: 'POST'
  });
};

/**
 * 取消
 */
export const cancel = (rid: string, reason: string) => {
  return Fetch<Result<any>>(`/return/cancel/${rid}?reason=${reason}`, {
    method: 'POST'
  });
};

/**
 * 查询退款单
 * @param rid
 * @returns {Promise<IAsyncResult<T>>}
 */
export function fetchRefundOrderList(rid: string) {
  return Fetch<Result<any>>(`/account/refundOrders/${rid}`);
}

/**
 * 获取积分设置
 */
export const basicRules = () => {
  return Fetch('/pointsConfig');
};
