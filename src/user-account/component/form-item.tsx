import React from 'react'
import {Link} from 'react-router-dom'

import {Relax} from "plume2";
import {noop, history, Confirm} from 'wmkit'
import {IList} from "typings/globalType";


const styles = require('../css/style.css');
const del=require('../img/del.png')
const edit=require('../img/edit.png')

@Relax
export default class FormItem extends React.Component<any, any> {

  props: {
    relaxProps?: {
      accountList: IList,
      deleteAccountById: Function
    }
  };

  static relaxProps = {
    accountList: 'accountList',
    deleteAccountById: noop
  }

  constructor(props) {
    super(props);
  }

  render() {
    const {accountList} = this.props.relaxProps;
    if (accountList == null) {
      return null;
    }
    return (
      <div className={styles.cardBox}>
        {
          accountList.toJS().map((o) => {
            return (
              <div className={styles.card} key={o.customerAccountId} style={{borderRadius:'0.1rem',background:'#fff'}}>
                <p className={styles.title}>{o.customerBankName}</p>
                <p className={styles.name}>{o.customerAccountName}</p>
                <div className={styles.acount}>
                  <p className={styles.num}>{this._accountNoFormat(o.customerAccountNo)}</p>
                  <div className={styles.btn}>
                    {/* <i className="iconfont icon-bianji" onClick={() => {
                      this._handleEditPage(o.customerAccountId)
                    }}></i>
                    <i className="iconfont icon-shanchu" onClick={() => {
                      this._handleDelete(o.customerAccountId)
                    }}></i> */}
                    <img src={edit} alt="" onClick={() => {
                      this._handleEditPage(o.customerAccountId)
                    }}/>
                    <img src={del} alt="" onClick={() => {
                      this._handleDelete(o.customerAccountId)
                    }}/>
                  </div>
                </div>
              </div>
            )
          })
        }
        <div className="grey-tips" style={{color:'#999'}}>提示：您最多可以添加5条银行账户信息</div>
      </div>
    )
  }


  /**
   * 银行卡每4位空1格
   * @param accountNo
   * @returns {string}
   * @private
   */
  _accountNoFormat(accountNo){
    let res = '';
    while(true){
      if (accountNo.length < 4){
        res += accountNo;
        break;
      }
      res += accountNo.substring(0,4) + " ";
      accountNo = accountNo.substring(4,accountNo.length);
    }
    return res;
  }


  /**
   * 编辑银行卡信息
   * @param accountId
   * @private
   */
  _handleEditPage(accountId) {
    history.push('/user-account-edit/' + accountId);
  }


  /**
   * 删除银行卡信息
   * @param accountId
   * @private
   */
  _handleDelete(accountId) {
    const {deleteAccountById} = this.props.relaxProps;
    Confirm({
      text: "确定删除该银行账户信息吗？",
      okBtn: '确定',
      cancelBtn: '取消',
      confirmCb: function () {
        deleteAccountById(accountId);
      }
    });
  }
}
