import {Fetch} from 'wmkit';

/**
 * 查询会员的银行账户信息列表
 */
export const fetchAccountList = () => {
  return Fetch<Result<any>>('/customer/accountList/', {
    method: 'GET'
  })
}


/**'
 *
 * 删除用户银行账号
 */
export const deleteAccountById = (accountId) => {
  return Fetch<Result<any>>('/customer/account/' + accountId, {
    method: 'DELETE'
  })
}