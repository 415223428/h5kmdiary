import React from 'react';
import { StoreProvider } from 'plume2';

import { history, Alert, Blank } from 'wmkit';

import FormItem from './component/form-item';
import AppStore from './store';
require('./css/style.css');
const add=require('./img/add.png')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class UserAccount extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    const accountCount = this.store.getAccountCount();
    return (
      <div className="content register-content" 
            style={{background: '#fafafa',paddingBottom:' calc(100% - 1rem)'}}
      >
        {accountCount == 0 ? (
          <Blank
            img={require('./img/none.png')}
            content="还没有银行账户，快去新增吧"
            tips="提示：您最多可以添加5条银行账户信息"
          />
        ) : (
          <FormItem />
        )}
        <div className='register-btn' style={{padding:'0'}}>
          {/* <button
            className="btn btn-primary"
            onClick={() => this._checkAccountCount()}
          >
            新增
          </button> */}
          <img src={add} alt="" style={{width:'100%'}} onClick={() => this._checkAccountCount()}/>
        </div>
      </div>
    );
  }

  /**
   * 检查用户
   * @private
   */
  _checkAccountCount = () => {
    const accountCount = this.store.getAccountCount();
    if (accountCount < 5) {
      history.push('/user-account-edit/-1');
    } else {
      Alert({ text: '您最多可以添加5条银行账户信息' });
    }
  };
}
