/**
 * Created by hht on 2017/7/17.
 */
import {fromJS} from 'immutable'

import {Actor, Action} from 'plume2'
import {IMap} from 'typings/globalType'

export default class UserAccount extends Actor {
  defaultState() {
    return {
      accountList: [],
    }
  }

  /**
   * 账号列表
   * @param state
   * @param res
   * @returns {Map<string, V>}
   */
  @Action('customer:accountList')
  accountList(state: IMap, res) {
    return state.set('accountList', fromJS(res));
  }

}