import {Alert} from "wmkit/modal";
import {Store, IOptions} from 'plume2';

import * as webapi from './webapi'
import UserAccount from './actor/user-account'

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [
      new UserAccount
    ]
  }

  /**
   * 初始化数据
   */
  init = async () => {
    const {context} = await webapi.fetchAccountList();
    this.dispatch('customer:accountList', context);
  };


  /**
   * 删除银行账户信息
   * @param accountId
   * @returns {Promise<void>}
   */
  deleteAccountById = async (accountId) => {
    const {code} = await webapi.deleteAccountById(accountId);
    if (code == 'K-000000') {
      Alert({text: "删除成功"});
      this.init();
    }
  };


  /**
   * 获取银行账户数量
   * @returns {number}
   */
  getAccountCount = () => {
    if (this.state().get("accountList")){
      return this.state().get("accountList").toJS().length;
    }else{
      return 0;
    }
  }
}
