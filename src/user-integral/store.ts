import {IOptions, Store} from 'plume2';

import { fromJS } from 'immutable';
import PointsActor from './actor/points-actor';
import {basicRules, queryPointsInfo, queryWillExpirePoints} from './webapi';
import {config} from 'config';

export default class AppStore extends Store {
  bindActor() {
    return [new PointsActor()];
  }

  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  init = async () => {
    const res: any = await queryPointsInfo();
    const willExpirePoints: any = await queryWillExpirePoints();
    if (res.code !== config.SUCCESS_CODE || willExpirePoints.code !== config.SUCCESS_CODE) return;
    this.transaction(() => {
      this.dispatch('points:value', res.context.pointsAvailable);
      this.dispatch('points:willExpire', fromJS(willExpirePoints.context));
    })
  };

  rule = async () => {
    const res: any = await basicRules();
    if (res.code !== config.SUCCESS_CODE) return;
    this.dispatch('points:rule', res.context.remark);
  };

  /**
   * 商品分类的显示隐藏
   */
  changeLayer = () => {
    this.dispatch('change: layerVisible');
  };
}
