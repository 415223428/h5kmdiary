import { Actor, Action, IMap } from 'plume2';

export default class PointsActor extends Actor {
  defaultState() {
    return {
      pointsValue: 0,
      pointsRule: '',
      layerVisible: false,
      //即将过期积分数据
      willExpirePointsData: {
        customerId: '',
        pointsExpireStatus: 0,
        pointsExpireDate: '',
        willExpirePoints: 0
      }
    };
  }

  @Action('points:value')
  pointsValue(state: IMap, pointsValue: number) {
    return state.set('pointsValue', pointsValue);
  }

  @Action('points:willExpire')
  willExpirePointsData(state: IMap, willExpirePointsData) {
    return state.set('willExpirePointsData', willExpirePointsData);
  }

  @Action('points:rule')
  pointsRule(state: IMap, pointsRule: string) {
    return state.set('pointsRule', pointsRule);
  }

  @Action('change: layerVisible')
  changeLayerVisible(state) {
    return state.set('layerVisible', !state.get('layerVisible'));
  }
}
