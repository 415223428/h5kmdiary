import React from 'react';
import { IMap, Relax } from 'plume2';

import { noop } from 'wmkit';

@Relax
export default class ValueLayer extends React.Component<any, any> {
  props: {
    relaxProps?: {
      layerVisible: boolean;
      changeLayer: Function;
      pointsRule: string;
    };
  };

  static relaxProps = {
    layerVisible: 'layerVisible',
    changeLayer: noop,
    pointsRule: 'pointsRule'
  };

  render() {
    const { layerVisible, changeLayer, pointsRule } = this.props.relaxProps;
    return (
      layerVisible && (
        <div className="mask mask-center">
          <div className="valueLayer">
            <div className="headerTitle">积分规则</div>
            <div />
            <div
              className="infoText"
              dangerouslySetInnerHTML={{ __html: pointsRule }}
            />
          </div>
          <a
            href="javascript:;"
            className="la-close"
            onClick={() => changeLayer()}
          >
            <i className="iconfont icon-Close" />
          </a>
        </div>
      )
    );
  }
}
