import React from 'react';
import {Relax} from 'plume2';
import {history, noop} from 'wmkit';
import {Const} from "config";
import moment from 'moment'

const background = require('../img/background.png');
@Relax
export default class CenterMember extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeLayer: Function;
      pointsValue: number;
      willExpirePointsData: any;
    };
  };

  static relaxProps = {
    changeLayer: noop,
    pointsValue: 'pointsValue',
    willExpirePointsData: 'willExpirePointsData'
  };

  render() {
    const { changeLayer, pointsValue, willExpirePointsData } = this.props.relaxProps;
    return (
      <div>
        <div style={{ height: '3.5rem'}}>
          <div className="user-top use-top-fixed" style={{ alignItems: 'center',background:`url(${background}) no-repeat top center`,backgroundSize:'cover' }}>
            <div className="box member-box">
              <div className="growth-context" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                <span style={{color:'#FFE3B2',fontSize:'0.6rem'}}>{pointsValue}</span>
                <span style={{color:'#FFF0D6',fontSize:'0.28rem',marginTop:'0.3rem'}}>当前积分</span>
              </div>
            </div>
            {(willExpirePointsData.get('pointsExpireStatus') == 1 &&
              willExpirePointsData.get('willExpirePoints') > 0) && (
              <div style={{color:'#FFDB29',fontSize:'0.26rem'}}>
                您有{willExpirePointsData.get('willExpirePoints')}积分，将于{moment(willExpirePointsData.get('pointsExpireDate')).format(Const.DATE_FORMAT)}过期，请提前使用
              </div>
            )}
            <div className="growth-rules" onClick={() => changeLayer()}>
              <span>规则</span>
              <i className="icon iconfont icon-jiantou"></i>
            </div>
          </div>
        </div>
        {/* <div className="member-points-mall">
          <img
            src={require('../img/points-mall.jpg')}
            onClick={() => history.push('/points-mall')}
          />
        </div> */}
      </div>
    );
  }
}

{
  /* <div
          className="progress-box"
          style={{ textAlign: 'center', color: '#eee' }}
        >
          2019年12月31日即将过期积分：0
        </div> */
}
