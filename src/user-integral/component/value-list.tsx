import React, {Fragment} from 'react';
import {Blank, ListView} from 'wmkit';
import moment from 'moment';
import {Const} from 'config';

// type 0:普通方式 1:订单相关 2:退单相关
let serviceTypeName = {
  0: { desc: '签到', type: 0 },
  1: { desc: '注册', type: 0 },
  2: { desc: '分享商品', type: 0 },
  3: { desc: '分享注册', type: 0 },
  4: { desc: '分享购买', type: 0 },
  5: { desc: '评论商品', type: 0 },
  6: { desc: '晒单', type: 0 },
  7: { desc: '完善基本信息', type: 0 },
  8: { desc: '绑定微信', type: 0 },
  9: { desc: '添加收货地址', type: 0 },
  10: { desc: '关注店铺', type: 0 },
  11: { desc: '订单完成', type: 1 },
  12: { desc: '订单抵扣', type: 1 },
  13: { desc: '优惠券兑换', type: 1 },
  14: { desc: '积分兑换', type: 1 },
  15: { desc: '退单返还', type: 2 },
  16: { desc: '订单取消返还', type: 1 },
  17: { desc: '过期扣除', type: 0 },
  18: { desc: '客户导入', type:0 }
};

export default class ValueList extends React.Component<any, any> {
  render() {
    return (
      <ul className="pointList b-1px-t" style={{ marginTop: ".72rem"}}>
        <p className="b-1px-b" style={{background:'#fafafa',fontSize:'0.26rem',color:'#333',paddingLeft:'0.3rem'}}>积分明细</p>
        <ListView
          url="/customer/points/page"
          pageSize={20}
          style={{ height: 'calc(100vh - 3.5rem)' }}
          renderRow={this._InfoRow}
          dataPropsName={'context.customerPointsDetailVOPage.content'}
          renderEmpty={() => (
            <Blank
              img={require('../img/list-none.png')}
              content="您暂时还没有明细记录哟~"
              // isToGoodsList={true}
            />
          )}
        />
      </ul>
    );
  }

  /**
   * 列表数据
   */
  _InfoRow = (storeInfo) => {
    let content = storeInfo.content ? JSON.parse(storeInfo.content) : null;
    return (
      <Fragment key={storeInfo.id}>
        <li className="b-1px-b">
          <div>
            <p className="title">
              {serviceTypeName[storeInfo.serviceType].desc}
            </p>
            {this._showPointsDesc(content, storeInfo.serviceType)}
            <p className="time" style={{marginLeft:0}}>
              {moment(storeInfo.opTime).format(Const.SECONDS_FORMAT)}
            </p>
          </div>
          <div className={storeInfo.type ? 'add-text' : 'del-text'}>
            {storeInfo.type ? ' + ' : ' - '}
            {storeInfo.points || 0}
          </div>
        </li>
      </Fragment>
    );
  };

  _showPointsDesc = (content, serviceType) => {
    const info = serviceTypeName[serviceType];
    if (info.type == 0) {
      return null;
    } else if (info.type == 1) {
      return <p className="order-rel">订单编号:{content.orderNo}</p>;
    } else if (info.type == 2) {
      return <p className="order-rel">退单编号:{content.returnOrderNo}</p>;
    }
  };
}
