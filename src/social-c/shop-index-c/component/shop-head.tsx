import React from 'react';
import { Relax } from 'plume2';
import { history, WMkit, noop } from 'wmkit';
const shopHeadBg = require('../img/shop-head-bg.png');
const defaultImg = require('../img/default-img.png');
/**
 *
 */
@Relax
export default class ShopHead extends React.Component<any, any> {
  props: {
    relaxProps?: {
      shopName: string;
      headImg: string;
      toMainPageAndClearInviteCache: Function;
    };
  };

  static relaxProps = {
    shopName: 'shopName',
    headImg: 'headImg',
    toMainPageAndClearInviteCache: noop
  };

  render() {
    let {
      shopName,
      headImg,
      toMainPageAndClearInviteCache
    } = this.props.relaxProps;
    let backToOrginHome = false; //通过链接分享进入精选页提供按钮跳转到原来首页
    if (WMkit.isDistributorLoginForShare()) {
      backToOrginHome = true;
    }
    return (
      <div
        className="shop-head shop-head-c"
        style={{
          background: 'url(' + shopHeadBg + ') 0 0 no-repeat',
          backgroundSize: '100% 100%'
        }}
      >
        <div className="shop-head-info">
          <div className="shop-head-info-img">
            <img src={headImg ? headImg : defaultImg} alt="" />
          </div>
          <p className="shop-head-info-name">{shopName}</p>
        </div>
        {backToOrginHome && (
          <div
            className="head-back-btn"
            onClick={() => toMainPageAndClearInviteCache()}
          >
            <span>返回自己店铺</span>
          </div>
        )}
      </div>
    );
  }
}
