import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop, history } from 'wmkit';

@Relax
export default class GoodsTab extends Component<any, any> {
  props: {
    relaxProps?: { openShade: Function; queryString: string };
  };

  static relaxProps = { openShade: noop, queryString: 'queryString' };

  render() {
    const { openShade, queryString } = this.props.relaxProps;
    return (
      <div className="shop-title-box search-box b-1px-t b-1px-b">
        <div
          className="input-box"
          onClick={() =>
            history.push({ pathname: '/search-c', state: { queryString } })
          }
        >
          <i className="iconfont icon-sousuo" />
          <input type="text" placeholder="搜索商品" value={queryString} />
          {
            !(window as any).isMiniProgram && (
              <button
                onClick={()=>{
                  // window.ysf('open')
                  // window.location.href = window.ysf('url');
                }}
              >客服</button>
          )}
        </div>
        <span
          onClick={() => {
            openShade('goodsFilter');
          }}
        >
          筛选<i className="iconfont icon-shaixuan" />
        </span>
      </div>
    );
  }
}
