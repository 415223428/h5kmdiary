import React, { Component } from 'react';
import { List, Map } from 'immutable';
import { IMap, Relax } from 'plume2';
import { noop } from 'wmkit';

const styles = require('../css/style.css');
const reset=require('../img/reset.png')
const confirm=require('../img/confirm.png')
@Relax
export default class GoodsTabFilter extends Component<any, any> {
  props: {
    relaxProps?: {
      closeShade: Function;
      handleFilterChange: Function;
      brandExpanded: boolean;
      brandList: any;
      selectedBrandIds: any;
    };
  };

  static relaxProps = {
    closeShade: noop,
    handleFilterChange: noop,
    brandExpanded: 'brandExpanded',
    brandList: 'brandList',
    selectedBrandIds: 'selectedBrandIds'
  };

  state: {
    selectedBrandIds: any;
    brandExpanded: boolean;
  };

  constructor(props) {
    super(props);

    this.state = {
      // 已选中品牌编号列表
      selectedBrandIds: List(),
      brandExpanded: false
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedBrandIds: nextProps.relaxProps.selectedBrandIds,
      brandExpanded: nextProps.relaxProps.brandExpanded
    });
  }

  render() {
    let { closeShade, brandList } = this.props.relaxProps;

    brandList = (brandList && brandList.toJS()) || [];
    const brandExpanded = this.state.brandExpanded;

    return (
      <div style={{ width: '100%' }}>
        <div className="filter-box">
          <div className="filter-content">
            {/*品牌*/}
            {brandList.length > 0 ? (
              <div className="filter-item">
                <div className="filter-head">
                  <h4>品牌</h4>
                  {brandList.length > 9 && (
                    <i
                      className="iconfont icon-jiantou2"
                      onClick={() =>
                        this.setState({ brandExpanded: !brandExpanded })
                      }
                      style={
                        brandExpanded ? { transform: 'rotate(180deg)' } : {}
                      }
                    />
                  )}
                </div>
                <div className="filter-sort">
                  {brandList.map((v, index) => {
                    if (!brandExpanded && index >= 9) {
                      return null;
                    }

                    // 在已选中品牌编号列表中的索引
                    const selectedIndex = this.state.selectedBrandIds.findIndex(
                      (brandId) => brandId == v.brandId
                    );
                    // 索引大于-1代表已选中
                    const selected = selectedIndex > -1;

                    return selected ? (
                      <span
                        key={`${v.brandId}_1`}
                        className={`${styles.selected} select-item-checked`}
                        onClick={() => this._handleClickBrand(v, selectedIndex)}
                      >
                        {v.nickName ? v.nickName : v.brandName}
                        <i className="iconfont icon-gou" />
                      </span>
                    ) : (
                      <span
                        key={`${v.brandId}_0`}
                        className={styles.item}
                        onClick={() => this._handleClickBrand(v, selectedIndex)}
                      >
                        {v.nickName ? v.nickName : v.brandName}
                      </span>
                    );
                  })}
                </div>
              </div>
            ) : null}
          </div>

          {/*底部按钮*/}
          <div className="filterBtn"  style={{display:'flex',justifyContent:'space-around'}}>
            {/* <button
              className="fix-btn reset-btn"
              onClick={() => this._handleReset()}
            >
              重置
            </button> */}
            <img src={reset} alt="" onClick={() => this._handleReset()}/>
            {/* <button
              className="check-btn"
              onClick={() => {
                closeShade();
                this._handleConfirm();
              }}
            >
              确定
            </button> */}
            <img src={confirm} alt="" onClick={() => {
                  closeShade();
                  this._handleConfirm();
                }}/>
          </div>
        </div>

        <div
          style={{ width: '100%', height: '100vh' }}
          onClick={() => closeShade()}
        />
      </div>
    );
  }

  /**
   * 处理点击品牌
   * @param brand 品牌对象
   * @param selectedIndex 在已选中品牌编号列表中的位置
   * @private
   */
  _handleClickBrand = (brand: any, selectedIndex: number) => {
    let { selectedBrandIds } = this.state;

    // selectedIndex等于-1时，该品牌没有选中，添加到选中队列，否则删除
    selectedBrandIds =
      selectedIndex > -1
        ? selectedBrandIds.delete(selectedIndex)
        : selectedBrandIds.push(brand.brandId);

    this.setState({
      selectedBrandIds: selectedBrandIds
    });
  };

  /**
   * 清空搜索条件
   * @private
   */
  _handleReset = () => {
    this.setState({
      selectedBrandIds: List(),
      brandExpanded: Map()
    });
  };

  /**
   * 点击确认按钮
   * @private
   */
  _handleConfirm = () => {
    let { handleFilterChange } = this.props.relaxProps;
    handleFilterChange(this.state.selectedBrandIds, this.state.brandExpanded);
  };
}
