import React, { Component } from 'react';
import { fromJS } from 'immutable';
import { Relax } from 'plume2';

import { Blank, ListView, noop, WMkit, Loading } from 'wmkit';

import GoodsItem from './goods-item';

const noneImg = require('../img/list-none.png');

@Relax
export default class GoodsList extends Component<any, any> {
  _listView: any;

  props: {
    relaxProps?: {
      listView: boolean; // 小图,大图
      loading: boolean;
      selectedBrandIds: any;
      queryString: string;
      initialEnd: boolean;
      handleDataReached: (data: Object) => void;
      isNotEmpty: boolean;
      purchase: Function;
    };
  };

  static relaxProps = {
    listView: 'listView',
    loading: 'loading',
    selectedBrandIds: 'selectedBrandIds',
    queryString: 'queryString',
    initialEnd: 'initialEnd',
    handleDataReached: noop,
    isNotEmpty: 'isNotEmpty',
    purchase: noop
  };
  constructor(props) {
    super(props);
    this.state = {
      // 防止修改商品数量,引起参数变化,重新查询商品列表(刷新页面,体验不好)
      dtoListParam:
        JSON.parse(localStorage.getItem(WMkit.purchaseCache())) || []
    };
  }
  render() {
    let {
      listView,
      initialEnd,
      selectedBrandIds,
      handleDataReached,
      isNotEmpty,
      queryString,
      purchase
    } = this.props.relaxProps;
    // 组件刚执行完mount，搜索条件没有注入进来，先不加载ListView，避免先进行一次无条件搜索，再立刻进行一次有条件搜索
    if (!initialEnd) {
      return <Loading />;
    }

    let layoutClassName = listView && isNotEmpty ? null : 'view-box';

    // 品牌
    const brandIds = (selectedBrandIds && selectedBrandIds.toJS()) || null;

    let dataUrl = '/goods/shop/sku-list-to-c';
    return (
      <ListView
        className={layoutClassName}
        url={dataUrl}
        style={{ height: 'calc(100% - 0.96rem)', paddingTop: '2.52rem' }}
        params={{
          customerId: WMkit.inviteeId(),
          brandIds: brandIds,
          likeGoodsName: queryString
        }}
        dataPropsName={'context.esGoodsInfoPage.content'}
        renderRow={(item: any) => {
          return (
            <GoodsItem
              purchase={purchase}
              goodsItem={fromJS(item)}
              listView={listView}
              key={item.id}
            />
          );
        }}
        renderEmpty={() => <Blank img={noneImg} content="没有搜到任何商品～" />}
        ref={(_listView) => (this._listView = _listView)}
        onDataReached={handleDataReached}
      />
    );
  }
}
