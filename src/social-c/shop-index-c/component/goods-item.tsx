import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMImage, WMkit } from 'wmkit';

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const { goodsItem, listView, purchase } = this.state;
    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0;

    let containerClass = listView
      ? invalid
        ? 'goods-list invalid-goods'
        : 'goods-list'
      : invalid
        ? 'goods-box invalid-goods'
        : 'goods-box';
    return (
      <div
        key={id}
        className={containerClass}
        onClick={() => history.push('/shop-index/goods-detail/'+ WMkit.inviteeId() +'/'+ goodsInfo.get('goodsId')+'/'+ id)}
      >
        <div className="img-box">
          <WMImage
            src={goodsInfo.get('goodsInfoImg')}
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail b-1px-b">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {goodsInfo.get('marketPrice')
                  ? goodsInfo.get('marketPrice').toFixed(2)
                  : 0.0}
              </span>
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            <div className="social-btn-box">
              <div
                className={'social-btn'}
                onClick={(e) => {
                  purchase(id, 1, stock);
                  e.stopPropagation();
                }}
              >
                抢购
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
