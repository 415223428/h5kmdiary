import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };

/**
 * 根据skuId查询spu相关信息
 * @param id
 */
export const querySpu = async (id) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  return isLoginOrNotOpen
    ? Fetch<TResult>(`/goods/spu/${id}`)
    : Fetch<TResult>(`/goods/unLogin/spu/${id}`);
};

/**
 * 查询店铺精选-小店名称
 * @param distributorId
 */
export const queryShopInfo = (distributorId) =>
  Fetch<TResult>(`/goods/shop-info/${distributorId}`, { method: 'GET' });

/**
 * 查询购物车数量
 * @returns {Promise<Result<T>>}
 */
export const fetchPurchaseCount = () => {
  return Fetch('/site/countGoods');
};

/**
 * 查询店铺是否是有效状态
 * @returns {Promise<Result<T>>}
 */
export const fetchShopStatus = () => {
  return Fetch('/distribute/check/status');
};

/**
 * 加入购物车
 * @param id
 * @param num
 * @returns {Promise<Result<TResult>>}
 */
export const purchase = (id, num) => {
  return Fetch<Result<any>>('/site/purchase', {
    method: 'POST',
    body: JSON.stringify({
      goodsInfoId: id,
      goodsNum: num
    })
  });
};

/**
 * 获取从小程序端进行登录的登录信息
 */
export const getLoginInfo = (customerId) => {
  return Fetch<TResult>(`/third/login/wechat/logininfo/${customerId}`, {
    method: 'GET',
  });
};
