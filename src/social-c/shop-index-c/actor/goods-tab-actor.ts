import { Actor, Action, IMap } from 'plume2';
import { fromJS, List, Map } from 'immutable';

export default class GoodsTabActor extends Actor {
  defaultState() {
    return {
      // 是否显示tab遮罩
      showShade: false,
      // 筛选tab里的品牌列表
      brandList: [],
      // 选中的品牌编号列表
      selectedBrandIds: [],
      brandExpanded: false, // 品牌筛选项是否全部展开？
      // 搜索关键字
      queryString: '',
      // 是否初始化解析参数完毕
      initialEnd: false,
      // 是否需要重绘品牌／属性筛选项
      drawFilter: true,
      // 列表是否不为空 true 不为空 false 为空
      isNotEmpty: true,
      //小店名称
      shopName: '',
      headImg:'',
      //购物车数量
      purchaseCount: 0
    };
  }

  @Action('goods-list:shopInfo')
  queryShopInfo(state, context) {
    return state.set('shopName', context.get('shopName')).set('headImg',context.get('headImg'));
  }

  /**
   * 解析参数完毕
   */
  @Action('goods-list:initialEnd')
  initialEnd(state) {
    return state.set('initialEnd', true);
  }

  /**
   * 搜素条件修改
   */
  @Action('goods-list:searchParams')
  searchParams(state, params) {
    return state.mergeDeep(params);
  }

  /**
   * 显示遮罩
   */
  @Action('goods-list:openShade')
  openShade(state) {
    return state.set('showShade', true);
  }

  /**
   * 隐藏遮罩
   */
  @Action('goods-list:closeShade')
  closeShade(state) {
    return state.set('showShade', false);
  }

  @Action('goods-list:drawFilter')
  drawFilter(state: IMap, drawFilter: boolean) {
    return state.set('drawFilter', drawFilter);
  }

  /**
   * 初始化品牌筛选项
   */
  @Action('goods-list:initFilter')
  initBrand(state, filters) {
    return state.set('brandList', filters.get('brands'));
  }

  /**
   * 品牌,属性筛选项选中内容变更
   */
  @Action('goods-list:filterChange')
  filterChange(state, { selectedBrandIds, brandExpanded }) {
    return state.withMutations((state) => {
      state
        .set('selectedBrandIds', selectedBrandIds)
        .set('brandExpanded', brandExpanded);
    });
  }

  /**
   * 列表是否不为空 true 不为空  false 为空
   */
  @Action('goods-list:isNotEmpty')
  isNotEmpty(state, isNotEmpty) {
    return state.set('isNotEmpty', isNotEmpty);
  }

  /**
   * 更新品牌筛选项的展开状态
   */
  @Action('goods-list:brandExpanded')
  brandExpanded(state, brandExpanded) {
    return state.set('brandExpanded', brandExpanded);
  }

  /**
   * 清空选中的 /品牌信息
   */
  @Action('goods-list:clearFilters')
  clearFilters(state, {}) {
    return state.withMutations((state) => {
      state.set('selectedBrandIds', List()).set('brandExpanded', Map());
    });
  }

  /**
   * 购物车数量
   * @param state
   * @param count
   * @returns {Map<string, V>}
   */
  @Action('goods-list: purchase: count')
  purchaseCount(state: IMap, count) {
    return state.set('purchaseCount', count);
  }
}
