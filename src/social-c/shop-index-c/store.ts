import { IOptions, Store, msg } from 'plume2';
import { fromJS } from 'immutable';
import { config, cache } from 'config';
import { Alert, Confirm, history, WMkit } from 'wmkit';

import * as webapi from './webapi';
import GoodsActor from './actor/goods-actor';
import GoodsTabActor from './actor/goods-tab-actor';
import { putPurchase, mergePurchase } from 'biz';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new GoodsActor(), new GoodsTabActor()];
  }

  /**
   * 查询店铺精选-小店名称
   * @param distributorId
   * @returns {Promise<void>}
   */
  queryShopInfo = async (distributorId) => {
    const { code, context } = (await webapi.queryShopInfo(
      distributorId
    )) as any;
    if (code == config.SUCCESS_CODE) {
      this.dispatch('goods-list:shopInfo', fromJS(context));
    }
  };

  /**
   * 初始化数据
   */
  init = async (queryString) => {
    //检查这个店铺是否是有效状态
    const status = (await webapi.fetchShopStatus()) as any;
    if (!status.context) {
      Confirm({
        text: '该店铺已失效，不可在店铺内购买商品',
        okBtn: '查看个人中心',
        confirmCb: () => history.push('/user-center')
      });
      return;
    }
    // const { code, context } = await webapi.queryGoodsShowType();
    //if (code == config.SUCCESS_CODE) {
    // goodsShowType(0:sku列表,1:spu列表)  imageShowType(0:小图,1:大图)
    // const { goodsShowType } = context as any;
    let params = {};
    if (queryString) {
      params['queryString'] = queryString;
    }

    //  this.transaction(() => {
    //this.dispatch('goodsActor:changeGoodsShowType', goodsShowType); // sku/spu视图模式
    // this.dispatch('goodsActor:changeImageShowType', imageShowType == 0); // 小图/大图模式
    this.dispatch('goods-list:searchParams', fromJS(params));
    this.dispatch('goods-list:initialEnd');
    // if (cateName != null && cateName != undefined && cateName != '') {
    //   this.dispatch('goods-list:setCateId', { cateId, cateName });
    // }
    //});
    //}
  };

  /**
   * 显示遮罩
   */
  openShade = () => {
    this.dispatch('goods-list:openShade');
    //无品牌或者品牌列表长度为0，给出提示信息
    if (
      !this.state().get('brandList') ||
      (this.state().get('brandList') &&
        this.state()
          .get('brandList')
          .toJS().lenght == 0)
    ) {
      Alert({
        text: '当前商品暂无可筛选项!',
        time: 2000
      });
    }
  };

  /**
   * 隐藏遮罩
   */
  closeShade = () => {
    this.dispatch('goods-list:closeShade');
  };

  /**
   * 加入购物车
   * @param id
   * @param num
   * @param stock
   */
  purchase = async (id, num, stock) => {
    if (num > stock) {
      Alert({
        text: '库存' + stock
      });
      return;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        const { code, message } = await webapi.purchase(id, num);
        if (code == 'K-000000') {
          Alert({
            text: '加入成功!'
          });
          //更新购物车全局数量
          msg.emit('purchaseNum');
        } else {
          Alert({
            text: message
          });
        }
      } else {
        if (putPurchase(id, num)) {
          //更新购物车全局数量
          msg.emit('purchaseNum');
          Alert({
            text: '加入成功!'
          });
        }
      }
    }
  };

  /**
   * 商品列表ListView查询得到的数据返回处理
   * @param data
   */
  handleDataReached = (data: any) => {
    if (data.code !== config.SUCCESS_CODE) {
      return;
    }

    const { goodsList } = data.context;

    this.dispatch('goods-list:isNotEmpty', !!goodsList);

    // 是否需要重绘筛选项
    if (this.state().get('drawFilter')) {
      // 设置状态为已重绘
      this.dispatch('goods-list:drawFilter', false);

      // 搜索结果对应的品牌聚合结果
      let { brands } = data.context;

      // 展示品牌筛选条件
      this.dispatch('goods-list:initFilter', fromJS({ brands }));
    }
  };

  // 品牌筛选条件选中变化
  handleFilterChange = (selectedBrandIds, brandExpanded: boolean) => {
    this.dispatch('goods-list:filterChange', {
      selectedBrandIds,
      brandExpanded
    });
  };
  //退出分享链接到商城首页,并更新邀请人和分销渠道缓存(分销渠道变为商城)
  toMainPageAndClearInviteCache = () => {
    WMkit.toMainPageAndClearInviteCache();
  };

  getLoginInfo = async (customerId) => {
    const { code, context } = await webapi.getLoginInfo(customerId);
    if (code == 'K-000000') {
      window.token = (context as any).token;
      localStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context as any));
      sessionStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context as any));
      WMkit.setIsDistributor();
      // 登录成功，发送消息，查询分销员信息 footer/index.tsx
      msg.emit('userLoginRefresh');
      // b.登陆成功,需要合并登录前和登陆后的购物车
      mergePurchase(null);
      // c.登陆成功,跳转拦截前的路由s
      Alert({ text: '登录成功' });
    }
  };
}
