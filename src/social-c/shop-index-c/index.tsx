import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit } from 'wmkit';

import AppStore from './store';
import ShopHead from './component/shop-head';
import GoodsList from './component/goods-list';
import GoodsTab from './component/goods-tab';
import GoodsTabShade from './component/goods-tab-shade';
import Footer from '../../footer';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsListIndex extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '店铺精选页';
    let { inviteeId } = this.props.match.params;
    // 搜索关键字
    let queryString = '';
    let search = this.props.location.search;
    search = search.substring(1, search.length);

    let searchArray = search.split('&');
    searchArray.forEach((kv) => {
      const split = kv.split('=');
      if (split[0] === 'q') queryString = decodeURIComponent(split[1]) || '';
    });
    let searchObj = WMkit.searchToObj(this.props.location.search);
    if (searchObj) {
      //登录返回的登录人ID，传到H5这边
      if ((searchObj as any).customerId) {
        this.store.getLoginInfo((searchObj as any).customerId);
      }
    }
    if (inviteeId) {
      WMkit.setShareInfoCache(inviteeId, 2);
    } else {
      inviteeId = WMkit.inviteeId();
    }
    this.store.queryShopInfo(inviteeId);
    this.store.init(queryString);
  }

  render() {
    return (
      <div className="content shop shop-c">
        <div className="scroll-fixed-box">
          {/*顶部搜索框*/}
          <ShopHead />

          {/*顶部的tab*/}
          <GoodsTab />
        </div>

        {/*tab遮罩*/}
        <GoodsTabShade />

        {/*商品列表*/}
        <GoodsList />
        <Footer path={'/shop-index-c/:inviteeId?'} />
      </div>
    );
  }
}
