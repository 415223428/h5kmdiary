/**
 * Created by qiaokang on 2019/3/11.
 */
import { Fetch } from 'wmkit';


export const getDistributionSetting = () => {
  return Fetch(`/customer/get-distribution-setting`, {
    method: 'GET'
  });
};

export const getmysubordinate = () => {
  // const params = {
  //   customerId
  // }
  return Fetch(`/customer/get-my-subordinate`)
  // , {
  //   method: 'POST',
  //   body: JSON.stringify(params)
  // });
};

