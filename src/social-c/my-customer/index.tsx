import React from 'react';
import { StoreProvider } from 'plume2';
import { cache, config } from 'config';
import AppStore from './store';
import Content from './component/content';
import Header from './component/header';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsListIndex extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    // 解析url中的参数
    const { id,i } = this.props.match.params;
    const customerId = 'a20005168';
    this.store.init(id,i);
    this.store.initMySubordinate();
    // console.log(customerId);
    
  }

  render() {
    return (
      <div className="content sales" style={{ backgroundColor: '#fff' }}>
        {/* <Header /> */}
        <Content />
      </div>
    );
  }
}
