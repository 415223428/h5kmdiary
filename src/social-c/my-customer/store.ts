import { IOptions, Store } from 'plume2';
import { fromJS } from 'immutable';
import { cache } from 'config';
import MyCustomerActor from './actor/my-customer-actor';
import * as webApi from './webapi';
export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new MyCustomerActor()];
  }

  init = async (id,i) => {
    let customerInfo = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
    //是否为分销员
    let isDistributor =
      customerInfo && customerInfo.customerDetail.isDistributor;
    if (isDistributor) {
      // 当前用户是分销员
      this.dispatch('tab:isDistributor', true);
    }

    if (id) {
      this.changeTitle(id);
      if(id==4){
        this.changeT(i);
      }
    }


  };

  initMySubordinate = async ()=>{
    const res = await webApi.getmysubordinate();
    if(res.code == 'K-000000'){
      this.dispatch('mySubordinate:init',res.context);
    }
  }

  /**
   * 设置邀请好友列表
   * @param res
   */
  fetchFriends = (res: any) => {
    // 邀请人数
    let totalNum = 0;
    if (res.context) {
      if (res.context.totalElements) {
        totalNum = res.context.totalElements;
      }

      this.dispatch('tab:setFriends', fromJS(res.context.content));
    }
    this.dispatch('tab:setTotalNum', totalNum);
  };

  /**
   * 切换头部tab
   */
  changeTitle = (id) => {
    this.dispatch('tab:change', id);
  };

  /**
   * 切换头部t
   */
  changeT = (i) => {
    this.dispatch('t:change', i);
  };
}
