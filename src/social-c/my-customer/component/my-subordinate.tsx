import React from 'react';
import { Relax } from 'plume2';
import { noop, history } from 'wmkit';

require('../css/style.css');

@Relax
export default class InviteList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      t: string;
    }
    val: any
  }
  static relaxProps = {
    t: 't',
  };
  render() {
    let { t } = this.props.relaxProps;
    let Subordinate = this.props.val;
    let subordinateType;
    let inviteCustomerNames;
    let newSubordinates = []//类别筛选

    if (t == '2') {
      Subordinate ?
        Subordinate.forEach(i => {
          if (i.inviteLevel !== 1) {
            newSubordinates.push(i)
          }
        }) : null
    } else if (t == '3') {
      Subordinate ?
        Subordinate.forEach(i => {
          if (i.inviteLevel !== 0) {
            newSubordinates.push(i)
          }
        }) : null
    } else {
      newSubordinates = Subordinate ? Subordinate : ''
    }

    return (
      <div className='subordinate-list'>
        <div className="ul-tab">
          <span>用户名</span>
          <span>注册时间</span>
          <span>级别</span>
          <span>推荐人</span>
          <span>累计佣金</span>
        </div>
        <div className='ul-list'>
          <ul className="ul-list-li">
            {newSubordinates ?
              newSubordinates.map((i, index) => {
                if (i.inviteLevel == 0) {
                  subordinateType = '直接分销员'
                } else if (i.inviteLevel == 1) {
                  subordinateType = '间接分销员'
                }
                inviteCustomerNames = i.inviteCustomerNames
                  .reduce((a, b) => {
                    if (a != '') {
                      a = a + ',' + b;
                    } else {
                      a = a + `${b}`
                    }
                    return a
                  }, '') || '';
                return (
                  <li key={index}>
                    <div className='ul-list-div-name'
                      onClick={() => {
                        history.push({
                          pathname: '/social-c/my-subordinates',
                          state: {
                            customerId: i.customerId,
                            inviteLevel: i.inviteLevel,
                          }
                        });
                      }}
                    >{i.customerName}</div>
                    <div>{i.createTime.substring(0, 10)}</div>
                    <div>{subordinateType}</div>
                    <div>
                      {inviteCustomerNames}
                    </div>
                    <div>
                      <i className="iconfont icon-qian" />
                      {i.countCommission}
                    </div>
                  </li>
                )
              }) : null
            }
          </ul>
        </div>
      </div>
    )
  }
}