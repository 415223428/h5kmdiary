import React from 'react';
import { Relax, IMap } from 'plume2';
import { fromJS } from 'immutable';
import Friend from './friend';
import { ListView, Blank, noop } from 'wmkit';

const style = require('../css/style.css');

@Relax
export default class FriendList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      fetchFriends: Function;
      tab: string;
    };
  };

  static relaxProps = {
    fetchFriends: noop,
    tab: 'tab'
  };

  render() {
    const { fetchFriends, tab } = this.props.relaxProps;
    let form = fromJS([]).toJS();
    let tipMsg = '您还没有邀请好友哦';
    // 分页查询我邀请的客户url
    let url = '/customer/page-invite-customer';
    if ('2' == tab) {
      // 分页查询我的有效邀请客户url
      url = '/customer/page-valid-invite-customer';
      tipMsg = '您还没有有效邀新哦';
    } else if ('3' == tab) {
      // 分页查询我的客户url
      url = '/customer/page-my-customer';
      tipMsg = '您还没有顾客哦';
    }

    return (
      <div style={{width:"100%"}}>
        <ListView
          url={url}
          params={form}
          style={{ height: 'calc(100vh - 2.65rem)',background:'#fff',borderTop:'0.2rem solid #fafafa' }}
          renderRow={(friend: IMap, index: number) => {
            return <Friend key={index} friend={friend} index={index} />;
          }}
          renderEmpty={() => (
            <Blank img={require('../img/list-none.png')} content={tipMsg} />
          )}
          onDataReached={(res) => fetchFriends(res)}
        />
      </div>
    );
  }
}
