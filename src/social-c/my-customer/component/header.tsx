import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { history, noop } from 'wmkit';

@Relax
export default class Header extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeDrapMenu: Function;
      showDrapMenu: boolean;
    };
  };

  static relaxProps = {
    changeDrapMenu: noop,
    showDrapMenu: 'showDrapMenu'
  };

  render() {
    const { changeDrapMenu, showDrapMenu } = this.props.relaxProps;

    return (
      <div>
        <div className="headerBox"
             style={{fontWeight:'bold'}}>
          <a className="header-title" href="javascript:;">
            我的用户
          </a>
          <a
            className="header-right"
            style={{ color: '#FF4D4D',fontWeight:'bold'}}
            onClick={() => {
              history.push('/sales-rank');
            }}
          >
            排行榜
          </a>
        </div>
      </div>
    );
  }
}
