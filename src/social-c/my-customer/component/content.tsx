import React from 'react';
import { Relax } from 'plume2';

import { history, noop } from 'wmkit';
import InviteList from './invite-list';
import MyCustomerList from './my-customer-List';
import ValidInviteList from './valid-invite-list';
import MySubordinateList from './my-subordinate-list'
import { render } from 'react-dom';
import * as WMkit from 'wmkit/kit';

@Relax
export default class Content extends React.Component<any, any> {
  props: {
    relaxProps?: {
      tab: string;
      changeTitle: Function;
      isDistributor: boolean;
    };
  };

  static relaxProps = {
    tab: 'tab',
    changeTitle: noop,
    isDistributor: 'isDistributor'
  };

  render() {
    const { changeTitle, tab, isDistributor } = this.props.relaxProps;
    //如果小b在小b的分享链接内则不展示我的顾客
    let linkflag = WMkit.inviteeId();
    return (
      <div className='head-tab' style={{ height: '100%',background:'#fff'}}>
        <div className="tag-bar b-1px-t b-1px-b">
          <ul className="tag-bar-list">
            <li
              className={`tag-bar-list-item ${tab == '1' ? 'active' : ''}`}
              onClick={() => changeTitle('1')}
            >
              邀新人数
            </li>
            <li
              className={`tag-bar-list-item ${tab == '2' ? 'active' : ''}`}
              onClick={() => changeTitle('2')}
            >
              有效邀新
            </li>
            {isDistributor &&
              !linkflag && (
                <li
                  className={`tag-bar-list-item ${tab == '3' ? 'active' : ''}`}
                  onClick={() => changeTitle('3')}
                >
                  我的顾客
                </li>
              )}
              {isDistributor &&
              !linkflag && (
                <li
                  className={`tag-bar-list-item ${tab == '4' ? 'active' : ''}`}
                  onClick={() => changeTitle('4')}
                >
                  我的下级
                </li>
              )}
          </ul>
          {/*<div className="tag-bar-icon-box">*/}
          {/*<a*/}
          {/*className="tag-bar-icon"*/}
          {/*href="javascript:;"*/}
          {/*onClick={() => history.push('/sales-rank')}*/}
          {/*>*/}
          {/*<i className="iconfont icon-paihang"/>*/}
          {/*<span>排行榜</span>*/}
          {/*</a>*/}
          {/*</div>*/}
        </div>

        {tab == '1' && <InviteList />}
        {tab == '2' && <ValidInviteList />}
        {tab == '3' && isDistributor && <MyCustomerList />}
        {tab == '4' && isDistributor && <MySubordinateList />}
        <div className='white' style={{}}></div>
      </div>
    );
  }
}
