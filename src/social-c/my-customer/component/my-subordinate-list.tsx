import React from 'react';
import { Relax, IMap } from 'plume2';
import { history, noop } from 'wmkit';
import MySubordinate from './my-subordinate';
import { DatePicker, List } from 'antd-mobile';
import enUs from 'antd-mobile/lib/date-picker/locale/en_US';
import { IList } from 'typings/globalType';
const style = require('../css/style.css');
const date = require('../img/date.png')
const queding = require('../img/confirm.png')
const reset = require('../img/reset.png')
const CustomChildren = ({ extra, onClick, children }: any) => (
  <div
    onClick={onClick}
    style={{ backgroundColor: '#fff', display: 'flex', alignItems: 'baseline' }}
  >
    {children}
    <span style={{ float: 'right', color: '#999', fontSize: '0.26rem', margin: '0.06rem' }}>{extra}</span>
  </div>
);

@Relax
export default class InviteList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      t: string;
      changeT: Function;
      mySubordinate:any;
    };
  };
  static relaxProps = {
    t: 't',
    changeT: noop,
    mySubordinate:'mySubordinate',
  };
  state = {
    isreset: '',
    indirect: 0,
    direct: 0,
    customChildValueS: null,
    customChildValueE: null,
    STime: {
      year: null,
      month: null,
      date: null
    },
    ETime: {
      year: null,
      month: null,
      date: null
    },

  }
  reset = () => {
    this.setState({
      isreset: 'reset',
      customChildValueS: null,
      customChildValueE: null,
    })
  }
  sortSubordinate = () => {
    let t = new Date();
    let s = this.state.customChildValueS, e = this.state.customChildValueE;
    console.log(s);
    console.log(e);
    
    let STime = {
      year: s.getFullYear(),
      month: s.getMonth() + 1,
      date: s.getDate()
    },
      ETime = {
        year: e.getFullYear(),
        month: e.getMonth() + 1,
        date: e.getDate()
      };
    this.setState({
      STime,
      ETime,
      isreset: ''
    })
  }
  subNumCount = (Subordinate) => {
    let directSub = 0, indirectSub = 0;
    Subordinate.forEach(i => {
      if (i.type == '直接分销员') {
        directSub += 1;
      }
      if (i.type == '间接分销员') {
        indirectSub += 1;
      }
    })
    return [directSub, indirectSub]
  }
  render() {
    const { changeT, t,mySubordinate } = this.props.relaxProps;
    let { isreset, STime, ETime} = this.state;
    //
    const customerSubordinate =mySubordinate?mySubordinate.distributionCustomerSubordinateVOS:'';
    const inredirectCusCount = mySubordinate?mySubordinate.inredirectCusCount:'';
    const redirectCusCount = mySubordinate?mySubordinate.redirectCusCount:'';
    let Subordinate = [];
    if (isreset != 'reset') {
      Subordinate = this._timesort(STime, ETime, customerSubordinate);
    } else {
      Subordinate = customerSubordinate
    }

    //分销员数量
    // let subordinateNum = this.subNumCount(Subordinate)
    return (
      <div className='subordinate-box'>
        <div className='subordinate-box-head b-1px-b'>
          <div className='subordinate-box-head-sort'>
            <div style={{ display: 'flex' }}>
              <img src={date} alt="" />

            </div>
            <div className='subordinate-box-head-sort-date'>
              <div className='s-time'
                style={{
                  height: '0.9rem',
                  lineHeight: '0.695rem',
                  display: 'flex',
                  alignItems: 'center',
                  fontSize: '0.28rem'
                }}>
                <DatePicker

                  mode="date"
                  value={this.state.customChildValueS}
                  title="开始日期"
                  onChange={v => this.setState({ customChildValueS: v, customChildValueE: v })}
                  extra="开始日期"
                >
                  <CustomChildren>日期筛选</CustomChildren>
                </DatePicker>
              </div>
              <div className='e-time'>
                <DatePicker
                  value={this.state.customChildValueE}
                  mode="date"
                  minDate={this.state.customChildValueS}
                  title="结束日期"
                  onChange={v => this.setState({ customChildValueE: v })}
                  extra="结束日期"
                >
                  <CustomChildren></CustomChildren>
                </DatePicker>
              </div>
            </div>

            <div style={{ display: 'flex', alignItems: 'center' }}>
              <div onClick={this.reset}>
                <img src={reset} alt="" style={{ width: '1.1rem', height: '0.5rem', margin: '0 0.1rem' }} />
              </div>
              <div
                onClick={this.sortSubordinate}>
                <img src={queding} alt="" style={{ width: '1.1rem', height: '0.5rem' }} />
              </div>

            </div>
          </div>
          <div className='distributer'>
            直接分销员数量(
                <span style={{ color: '#FF4D4D' }}>
              {redirectCusCount}
            </span>
            ) | 间接分销员数量(
                <span style={{ color: '#FF4D4D' }}>{inredirectCusCount}</span>
            )
            </div>
        </div>
        <div style={{ height: '100%' }}>
          <div className="tag-bar b-1px-b">
            <ul className="tag-bar-list">
              <li
                className={`tag-bar-list-item ${t == '1' ? 'active' : ''}`}
                onClick={() => changeT('1')}
              >
                全部
                </li>
              <li
                className={`tag-bar-list-item ${t == '2' ? 'active' : ''}`}
                onClick={() => changeT('2')}
              >
                直接分销员
                </li>
              <li
                className={`tag-bar-list-item ${t == '3' ? 'active' : ''}`}
                onClick={() => changeT('3')}
              >
                间接分销员
                </li>
            </ul>
          </div>

          {t == '1' && <MySubordinate val={Subordinate} />}
          {t == '2' && <MySubordinate val={Subordinate} />}
          {t == '3' && <MySubordinate val={Subordinate} />}
        </div>
      </div>
    )
  }

  _timesort = (STime, ETime, subordinates) => {
    let newsubordinates = [];//日期筛选
    let sy = parseInt(STime.year),
      sm = parseInt(STime.month),
      sd = parseInt(STime.date),
      ey = parseInt(ETime.year),
      em = parseInt(ETime.month),
      ed = parseInt(ETime.date);
    if (STime.year) {
      subordinates.forEach(i => {
        let y = parseInt(i.createTime.slice(0, 4)), m = parseInt(i.createTime.slice(5, 7)), d = parseInt(i.createTime.slice(8, 10));
        if (y >= sy && y <= ey) {
          if (y == sy || y == ey) {
            if (y == sy && y == ey) {
              if (m >= sm && m <= em) {
                if (d >= sd && d <= ed) {
                  newsubordinates.push(i);
                }
              }
            } else if (y == sy) {
              if (m >= sm) {
                if (m == sm) {
                  if (d >= sd) {
                    newsubordinates.push(i);
                  }
                } else {
                  newsubordinates.push(i);
                }
              }
            }
            else if (y == ey) {
              if (m <= em) {
                if (m == em) {
                  if (d <= ed) {
                    newsubordinates.push(i);
                  }
                } else {
                  newsubordinates.push(i);
                }
              }
            }
          }
          else {
            newsubordinates.push(i);
          }
        }
      });
    } else {
      newsubordinates = subordinates;
    }
    return newsubordinates;
  }
}