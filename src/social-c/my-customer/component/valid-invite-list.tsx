import React from 'react';
import { Relax } from 'plume2';
import FriendList from './friend-list';
const style = require('../css/style.css');

@Relax
export default class ValidInviteList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      totalNum: number;
    };
  };

  static relaxProps = {
    totalNum: 'totalNum',
  };

  render() {
    const { totalNum } = this.props.relaxProps;

    return (
      <div className="friend-box">
        <div className="friend-list">
          <div className="friend-list-total b-1px-b"
               style={{display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                margin:' 0.11rem auto 0.6rem'}}>
            <span className="friend-list-total-num" style={{fontSize: '0.6rem',color:' #ff4d4d'}}>{totalNum}</span>
            <span className="friend-list-total-text" style={{marginTop: '0.39rem',fontSize: '0.26rem',color: '#ff4d4d'}}>有效邀新</span>
          </div>
          {/* 好友列表 */}
          <FriendList/>
        </div>
      </div>
    );
  }
}
