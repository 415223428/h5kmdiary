import React from 'react';
import { Relax } from 'plume2';
import { _ } from 'wmkit';
import { isDistributor } from '../../../../web_modules/wmkit/kit';
const style = require('../css/style.css');

// 默认头像
const defaultImg = require('../img/default-img.png');

@Relax
export default class Friend extends React.Component<any, any> {
  props: {
    relaxProps?: {
      tab: string;
      isDistributor: boolean;
    };
    friend: any;
    index: number;
  };

  static relaxProps = {
    tab: 'tab'
  };

  render() {
    let { friend } = this.props;
    let { tab } = this.props.relaxProps;
    let customerName;
    let headImg;
    if ('3' == tab) {
      customerName = friend.customerName ? friend.customerName : '';
      headImg =   friend.headImg?friend.headImg:defaultImg;
    } else {
      customerName = friend.invitedNewCustomerName
        ? friend.invitedNewCustomerName
        : friend.invitedNewCustomerAccount;
      headImg =   friend.invitedNewCustomerHeadImg?friend.invitedNewCustomerHeadImg:defaultImg;
    }

    let orderNum = friend.orderNum ? friend.orderNum : 0;
    let amount = friend.amount ? friend.amount : '0';

    return (
      <div className="friend-box">
        <div className="friend-list" style={{padding:0}}>
          {/* 好友列表 */}
          <ul className="list-ul">
            <li>
              <img className="portrait" src={headImg} style={{width:'0.9rem',height:'0.9rem',borderRadius:'50%'}}/>
              <div className="div-text">
                <span className="left-text">
                  <span className="name">{customerName}</span>
                  <span className="money">
                    累计消费金额：<i className="iconfont icon-qian" style={{color:'#ff4d4d'}}/><span className="number">{amount}</span>
                  </span>
                </span>
                <span className="right-text">
                  <span className="date">
                    {tab == '3' ? '首单时间：' : '注册时间：'}
                    {tab == '3'
                      ? _.formatDay(friend.firstOrderTime)
                      : _.formatDay(friend.registerTime)}
                  </span>
                  <span className='order'>已购买<span style={isDistributor?{color:'#333'}:{color:'FF4D4D'}}>{orderNum}</span>单</span>
                </span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
