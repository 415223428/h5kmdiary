import { Action, Actor, IMap } from 'plume2';
import { List } from 'immutable';

export default class MyCustomerActor extends Actor {
  defaultState() {
    return {
      // 选择的标题1：邀新人数 2：有效邀新 3：我的顾客
      tab: '1',
      t:'1',
      // 是否分销员
      isDistributor: false,
      // 邀请人数
      totalNum: 0,
      // 邀请好友列表
      friends: [],
      mySubordinate:{},
    };
  }

  /**
   * 切换tab
   * @param state
   * @param id
   */
  @Action('tab:change')
  changeTab(state, id) {
    return state.set('tab', id);
  }
/**
   * 切换tab
   * @param state
   * @param i
   */
  @Action('t:change')
  changeT(state, i) {
    return state.set('t', i);
  }
  /**
   * 当前用户是否是分销员
   * @param state
   * @param isDistributor
   */
  @Action('tab:isDistributor')
  isDistributor(state, isDistributor) {
    return state.set('isDistributor', isDistributor);
  }

  /**
   * 更新邀请好友列表
   * @param {IMap} state
   * @param {Array<any>} params
   * @returns {any}
   */
  @Action('tab:setFriends')
  setFriends(state: IMap, params: Array<any>) {
    return state.update('friends', (value: List<any>) => {
      return value.push(...params);
    });
  }

  /**
   * 更新邀请总人数
   * @param state
   * @param totalNum
   */
  @Action('tab:setTotalNum')
  setTotalNum(state, totalNum) {
    return state.set('totalNum', totalNum);
  }

@Action('mySubordinate:init')
mySubordinate(state,context){
  return state.set('mySubordinate',context)
}

}
