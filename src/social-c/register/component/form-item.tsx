import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { FormInput, noop, Button, Alert, ValidConst } from 'wmkit';
import AppStore from '../store';
const TimerButton = Button.Timer;

@Relax
export default class FormItem extends React.Component<any, any> {
  _store: AppStore;

  props: {
    relaxProps?: {
      mobile: string;
      password: string;
      code: string;
      buttonValue: string;
      isShowpwd: boolean;
      patcha: string;
      showPwd: Function;
      getMobile: Function;
      getPassword: Function;
      getCode: Function;
      sendCode: Function;
      image: string;
      patchca: Function;
      getPatcha: Function;
      toggleShowAgreement: Function;
    };
  };

  static relaxProps = {
    mobile: 'mobile',
    password: 'password',
    code: 'code',
    buttonValue: 'buttonValue',
    isShowpwd: 'isShowpwd',
    showPwd: noop,
    getMobile: noop,
    getPassword: noop,
    getCode: noop,
    sendCode: noop,
    image: 'image',
    patchca: noop,
    getPatcha: noop,
    toggleShowAgreement: noop,
    patcha: 'patcha'
  };

  constructor(props: any) {
    super(props);
  }

  render() {
    const {
      getMobile,
      getPassword,
      getCode,
      buttonValue,
      showPwd,
      isShowpwd,
      sendCode,
      toggleShowAgreement
    } = this.props.relaxProps;
    let {
      mobile,
      password,
      code,
      image,
      patchca,
      getPatcha,
      patcha
    } = this.props.relaxProps;
    return (
      <div className="register-box">
        <FormInput
          label="手机号"
          type="tel"
          placeHolder="请输入您的手机号"
          pattern="[0-9]*"
          defaultValue={mobile}
          maxLength={11}
          onChange={(e) => this._setMobile(e)}
        />
        <FormInput
          label="验证码"
          type="number"
          placeHolder="请输入验证码"
          pattern="[0-9]*"
          defaultValue={code}
          maxLength={6}
          onChange={(e) => getCode(e.target.value)}
          other={
            <TimerButton
              shouldStartCountDown={() => this._sendCode()}
              resetWhenError
              text={buttonValue}
              onClick={() => sendCode(mobile)}
              defaultStyle={{
                color: '#000',
                borderColor: '#000'
              }}
            />
          }
        />
        <div className="row form-item">
          <span className="form-text">密码</span>
          <div className="eyes-box">
            <input
              placeholder="请输入6-16位字母或数字密码"
              type={isShowpwd ? 'text' : 'password'}
              value={password}
              pattern="/^[A-Za-z0-9]{6,16}$/"
              className="form-input"
              maxLength={16}
              onChange={(e) => getPassword(e.target.value)}
            />
            <i
              onClick={() => showPwd()}
              className={`iconfont icon-${
                isShowpwd ? 'yanjing' : 'iconguanbiyanjing'
              }`}
            />
          </div>
        </div>
        <div className="register-tips">
          点击注册代表您已阅读并接受<a href="javascript:void(0)" onClick={()=>toggleShowAgreement()}>
            <span>《会员注册协议》</span>
          </a>
        </div>
      </div>
    );
  }

  _sendCode = () => {
    const { mobile, patcha } = this.props.relaxProps;
    const regex = ValidConst.phone;
    if (mobile == '') {
      Alert({
        text: '手机号不能为空！'
      });
      return false;
    } else if (!regex.test(mobile)) {
      Alert({
        text: '手机号格式有误!'
      });
      return false;
    } else {
      return true;
    }
  };

  _setMobile = (e) => {
    let value = e.target.value;
    const { getMobile, mobile } = this.props.relaxProps;
    //圆点.特殊处理
    let lastchart = value.substr(value.length - 1, 1);
    if (
      value.toString() % 1 != 0 ||
      lastchart == '.' ||
      lastchart == '+' ||
      lastchart == '-'
    ) {
      getMobile(mobile);
    } else {
      if (value.length > 11) {
        getMobile(mobile);
      } else {
        getMobile(value);
      }
    }
  };
}
