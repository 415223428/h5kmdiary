import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import AppStore from '../store';

@Relax
export default class Info extends React.Component<any, any> {
  _store: AppStore;

  props: {
    relaxProps?: {
      customerName: string;
    };
  };

  static relaxProps = {
    customerName: 'customerName'
  };

  constructor(props: any) {
    super(props);
  }

  render() {
    let { customerName } = this.props.relaxProps;

    return (
      <div className="register-info">
        <p className="invite-tips">请使用有效手机号注册</p>
        {customerName && <p className="invite-text">邀请人：{customerName}</p>}
      </div>
    );
  }
}
