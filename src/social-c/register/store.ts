/**
 * Created by feitingting on 2017/7/13.
 */
import { Store } from 'plume2';
import { _, Alert, history, WMkit } from 'wmkit';
import { cache, config } from 'config';
import RegisterActor from './actor/register-actor';
import * as webapi from './webapi';
import { isNullOrUndefined } from 'util';

export default class AppStore extends Store {
  bindActor() {
    return [new RegisterActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  /**
   * 初始化UUID，获取图形验证码
   * @param id
   */
  init = async (id, employeeId, inviteeId) => {
    const res = await webapi.getSiteInfo();
    if (res.code == config.SUCCESS_CODE) {
      let registerContent = (res.context as any).registerContent;
      this.dispatch('agreeMent:init', registerContent);
    }

    this.dispatch('register:uuid', id);
    if (!isNullOrUndefined(employeeId)) {
      this.dispatch('register:employeeId', employeeId);
    }
    if (!isNullOrUndefined(WMkit.inviteeId())) {
      this.dispatch('register:inviteeId', inviteeId);

      // 查询邀请人姓名
      const { code, context } = await webapi.getCustomerInfo(WMkit.inviteeId());
      if (code == config.SUCCESS_CODE) {
        if (context) {
          this.dispatch('register:customerName', (context as any).customerName);
        }
      }
    }
    this.state().get('employeeId');
    // this.patchca();
  };

  patchca = async () => {
    const uuid = this.state().get('UUID');
    const res = await webapi.patchca(uuid);
    if (res.code == 'K-000000') {
      this.dispatch('register:image', (res as any).context);
    }
  };

  showPwd = () => {
    const showpass = this.state().get('isShowpwd');
    this.dispatch('register:showPass', showpass);
  };

  getMobile = (tel: string) => {
    this.dispatch('register:mobile', tel);
  };

  getPassword = (pass: string) => {
    this.dispatch('register:pass', pass);
  };

  getCode = (code: string) => {
    this.dispatch('register:code', code);
  };

  sendCode = (tel: string) => {
    //加入图片验证码和UUID两个参数
    const uuid = this.state().get('UUID');
    const patcha = this.state().get('patcha');
    return webapi.sendCode(tel, uuid, patcha).then((res) => {
      const { code, message } = res;
      if (code == 'K-000000') {
        Alert({
          text: '发送验证码成功！'
        });
      } else {
        Alert({
          text: message
        });
        return Promise.reject(message);
      }
    });
  };

  doRegister = async () => {
    /**
     * 参数准备*/
    const mobile = this.state().get('mobile');
    const sendCode = this.state().get('code');
    const password = this.state().get('password');
    const patcha = this.state().get('patcha');
    const uuid = this.state().get('UUID');
    const employeeId = this.state().get('employeeId');
    const inviteeId = this.state().get('inviteeId');
    if (
      WMkit.testTel(mobile) &&
      WMkit.testPass(password) &&
      WMkit.testVerificationCode(sendCode)
    ) {
      const { code, message, context } = await webapi.register(
        mobile,
        sendCode,
        password,
        patcha,
        uuid,
        employeeId,
        inviteeId
      );
      if (code == config.SUCCESS_CODE) {
        //清除本地缓存的审核未通过的或者正在审核中的账户信息
        WMkit.clearLoginCache();
        localStorage.removeItem(cache.PENDING_AND_REFUSED);

        //是否直接可以登录 0 否 1 是
        if (!(context as any).isLoginFlag) {
          Alert({
            text: '注册成功，请完善账户信息'
          });
          //提示完善账户信息
          history.push(`/improve-information/${(context as any).customerId}`);
        } else {
          //直接跳转到主页
          localStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
          sessionStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
          Alert({
            text: '登录成功'
          });
          setTimeout(() => {
            history.push('/');
            _.showRegisterModel((context as any).couponResponse, true);
          }, 1000);
        }
      } else {
        Alert({
          text: message
        });
      }
    } else {
      return false;
    }
  };

  getPatcha = (value) => {
    this.dispatch('register:patcha', value);
  };

  //切换注册协议登录显示
  toggleShowAgreement = () => {
    this.dispatch('loginModal:toggleShowAgreement');
  };
}
