/**
 * Created by feitingting on 2017/7/13.
 */
import { Fetch } from 'wmkit';

/**
 * 获取平台站点信息
 * @type {Promise<AsyncResult<T>>}
 */
export const getSiteInfo = () => {
  return Fetch('/system/baseConfig')
}

/**
 * 发送验证码
 * @param tel
 * @returns {Promise<Result<T>>}
 */
export const sendCode = (tel: string, uuid: string, patchca: string) => {
  return Fetch('/checkSmsByRegister', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: tel,
      patchca: patchca,
      uuid: uuid
    })
  });
};

/**
 * 登录系统
 * @param mobile
 * @param code
 * @param password
 * @returns {Promise<Result<T>>}
 */
export const register = (
  mobile: string,
  code: string,
  password: string,
  patchca: string,
  uuid: string,
  employeeId: string,
  inviteeId: string
) => {
  return Fetch('/register', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: mobile,
      customerPassword: password,
      verifyCode: code,
      patchca: patchca,
      uuid: uuid,
      employeeId: employeeId,
      inviteeId: inviteeId
    })
  });
};

/**
 * 根据UUID去获取图形验证码
 * @param id
 * @returns {Promise<Result<T>>}
 */
export const patchca = (id: string) => {
  return Fetch(`/patchca/${id}`, {
    method: 'GET'
  });
};

/**
 * 根据客户id查询客户信息
 * @param {string} customerId
 * @returns {Promise<Result<any>>}
 */
export const getCustomerInfo = (customerId: string) => {
  return Fetch(`/customer/customerInfoById/${customerId}`, {
    method: 'GET'
  });
};
