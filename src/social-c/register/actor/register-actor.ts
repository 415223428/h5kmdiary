/**
 * Created by feitingting on 2017/7/13.
 */
import { Action, Actor, IMap } from 'plume2';

export default class RegisterActor extends Actor {
  defaultState() {
    return {
      mobile: '', //手机号
      code: '', //验证码
      password: '', //密码
      mobileChange: true, //监听手机号码的变化
      passChange: true, //监听密码变化
      codeChange: true, //监听验证码变化
      buttonValue: '发送验证码', //按钮值
      isShowpwd: false,
      image: '', //图形验证码url
      UUID: '', //UUID
      patcha: '', //图像验证码的值,
      inviteeId: '', // 邀请人的客户id
      customerName: '', // 邀请人的名称
      employeeId: '', //业务员id
      //显示注册协议
      showAgreement: false,
      registerContent: ''
    };
  }

  /**
   * 是否显示密码
   * @param state
   * @param show
   */
  @Action('register:showPass')
  showPass(state, show: boolean) {
    return state.set('isShowpwd', !show);
  }

  /**
   * 监听手机号码状态值
   * @param state
   * @param tel
   */
  @Action('register:mobile')
  getMobile(state, tel: string) {
    return state.set('mobile', tel);
  }

  /**
   * 开启倒计时
   * @param state
   * @param start
   */
  @Action('register:setTime')
  setTime(state, start: number) {
    return state.set('buttonValue', start);
  }

  /**
   * 递归
   * @param state
   * @param value
   */
  @Action('register:time')
  time(state, value: number) {
    return state.set('buttonValue', value - 1);
  }

  /**
   * 监听密码状态值
   * @param state
   * @param value
   */
  @Action('register:pass')
  getPass(state, value: string) {
    return state.set('password', value);
  }

  /**
   * 监听验证码的状态值
   * @param state
   * @param value
   */
  @Action('register:code')
  getCode(state, value: string) {
    return state.set('code', value);
  }

  /**
   * 图形验证码
   * @param state
   * @param image
   */
  @Action('register:image')
  image(state, image: string) {
    return state.set('image', image);
  }

  @Action('register:uuid')
  UUID(state, id) {
    return state.set('UUID', id);
  }

  @Action('register:patcha')
  patcha(state, value) {
    return state.set('patcha', value);
  }

  @Action('register:employeeId')
  employeeId(state, id) {
    return state.set('employeeId', id);
  }

  /**
   * 邀请人id
   * @param state
   * @param inviteeId
   */
  @Action('register:inviteeId')
  inviteeId(state, inviteeId) {
    return state.set('inviteeId', inviteeId);
  }

  /**
   * 邀请人姓名
   * @param state
   * @param customerName
   */
  @Action('register:customerName')
  setCustomerName(state, customerName) {
    return state.set('customerName', customerName);
  }

  @Action('loginModal:toggleShowAgreement')
  toggleShowAgreement(state) {
    return state.set('showAgreement', !state.get('showAgreement'))
  }

  @Action('agreeMent:init')
  init(state: IMap, registerContent: string) {
    return state.set('registerContent', registerContent);
  }
}
