import React from 'react';
import { StoreProvider } from 'plume2';
import { Button } from 'wmkit';
import AppStore from './store';
import FormItem from './component/form-item';
import Info from './component/info';
import Agreement from './component/agreement';
import { WMkit } from 'wmkit';

const LongBlueButton = Button.LongBlue;
const UUID = require('uuid-js');

/**
 * 社交电商小C—注册页面
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class Register extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '注册';
    let { inviteeId } = this.props.match.params;
    if (inviteeId) {
      WMkit.setShareInfoCache(inviteeId, null);
    }
    this.store.init(
      UUID.create().toString(),
      this.props.match.params.employeeId,
      this.props.match.params.inviteeId
    );
  }

  componentDidUpdate() {
    window.addEventListener('scroll', this._initScroll);
  }

  render() {
    const showAgreement = this.store.state().get('showAgreement');
    return (
      <div className="content register-content">
        <Info />
        <FormItem />
        <div className="register-btn">
          <LongBlueButton text="注册" onClick={() => this.store.doRegister()} />
        </div>
        {showAgreement && <Agreement />}
      </div>
    );
  }

  /**
   * IOS端点击input键盘收起的事件处理
   * @private
   */
  _initScroll = () => {
    document.body.addEventListener('focusout', () => {
      //软键盘收起的事件处理
      window.scrollTo(0, 0);
    });
  };
}
