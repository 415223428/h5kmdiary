import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };
/**
 * 初始化商品详情
 * @param id
 * @returns {Promise<Result<TResult>>}
 */
export const init = async (id) => {
  return Fetch<Result<any>>(`/goods/spu/${id}`);
};

/**
 * 检查开店礼包有效状态
 * @param id
 * @returns {Promise<Result<TResult>>}
 */
export const verify = async (id) => {
  return Fetch<Result<any>>(`/distribute/verify/storeBags/sku/${id}`);
};

/**
 * 立即购买
 */
export const immediateBuy = (id) => {
  return Fetch<TResult>('/trade/store-bags-buy', {
    method: 'POST',
    body: JSON.stringify({
      goodsInfoId: id
    })
  });
};
/**
 * 获取店铺基本信息
 * @param id 店铺Id
 */
export const fetchStoreInfo = (id) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  const params = {
    storeId: id
  };
  return isLoginOrNotOpen
    ? Fetch('/store/storeInfo', {
        method: 'POST',
        body: JSON.stringify(params)
      })
    : Fetch('/store/unLogin/storeInfo', {
        method: 'POST',
        body: JSON.stringify(params)
      });
};
