import { IOptions, msg, Store } from 'plume2';

import { fromJS } from 'immutable';
import { cache, config } from 'config';
import { Alert, history, WMkit, wxShare, Confirm } from 'wmkit';
import DescActor from './actor/desc-actor';
import GoodsActor from './actor/goods-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new DescActor(), new GoodsActor()];
  }

  /**
   * sku初始化
   * @param id
   */
  init = async (id, url) => {
    // 检查开店礼包有效状态
    if (!(await this.verify(id))) {
      return;
    }
    const { code, context } = (await webapi.init(id)) as any;

    if (code == config.SUCCESS_CODE) {
      /**商品详情pv/uv埋点*/
      (window as any).myPvUvStatis(id, context.goods.companyInfoId);
      //赋予skuId
      context.skuId = id;
      //无内容的storeGoodsTabs需要过滤出来
      let storeGoodsTabs = [];
      context.storeGoodsTabs.map((v) => {
        if (
          context.goodsTabRelas.length > 0 &&
          context.goodsTabRelas.find((item) => item.tabId == v.tabId) &&
          context.goodsTabRelas.find((item) => item.tabId == v.tabId).tabDetail
        ) {
          storeGoodsTabs.push(v);
        }
      });

      this.transaction(() => {
        this.dispatch('goods-detail: info', context);
        this.dispatch('desc:data', context.goods.goodsDetail);
        //存储url
        this.dispatch('goods-detail:url', url);
        //图文详情tab
        this.dispatch('goods-detail:storeGoodsTabs', storeGoodsTabs);
        //tab关联内容
        if (context.goodsTabRelas) {
          this.dispatch(
            'goods-detail:storeGoodsTabContent',
            context.goodsTabRelas
          );
        }
      });
    } else {
      history.replace('/goods-failure');
    }
  };

  /**
   * 检查开店礼包有效状态
   */
  verify = async (id) => {
    const { code, context } = (await webapi.verify(id)) as any;
    if (code != config.SUCCESS_CODE) {
      Confirm({
        text: '很抱歉，商品已失效,请重新选择',
        okBtn: '确定',
        confirmCb: () => history.push('/user-center')
      });
      return false;
    }
    return true;
  };

  /**
   * 立即购买
   */
  immediateBuy = async (id) => {
    // 检查开店礼包有效状态
    if (!(await this.verify(id))) {
      return;
    }
    const { code } = await webapi.immediateBuy(id);
    if (code === config.SUCCESS_CODE) {
      history.push('/order-confirm');
      (window as any).y = '';
    } else {
      Confirm({
        text: '很抱歉，商品已失效,请重新选择',
        okBtn: '确定',
        confirmCb: () => history.push('/user-center')
      });
    }
  };

  showModal = () => {
    this.dispatch('goods-detail:modal');
  };

  closeModal = () => {
    this.dispatch('goods-detail:modal:close');
  };

  //切换视频暂停播放
  toggleShowVideo = () => {
    this.dispatch('goodsDetail:toggleShowVideo');
  };

  //图文详情tab切换
  changeTabKey = (index) => {
    this.dispatch('goodsDetail:changeTabKey', index);
  };
}
