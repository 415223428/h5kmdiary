import { Actor, Action, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class GoodsActor extends Actor {
  defaultState() {
    return {
      //sku信息
      goodsInfo: [],
      //spu信息
      goods: {},
      //商品规格
      goodsSpecs: [],
      //商品规格值
      goodsSpecDetails: [],
      //spu图片集合(前10张,如果sku没有图片)
      images: [],
      // 店铺信息
      store: {},
      url: '',
      //显示播放的视频
      showVideo: false,
      //图文详情tab
      storeGoodsTabs: [],
      //图文详情tab标签,默认为-1
      tabKey: -1,
      //所有sku信息
      goodsInfos: fromJS([]),
      //spu所有返回值
      spuContext: {},
      renderVideo: true,
      renderSlider: true,
      //图文详情的内容
      storeGoodsTabContent: []
    };
  }

  /**
   * 商品详情信息
   * @param state
   * @param goodsInfo
   * @param goods
   * @param goodsSpecs
   * @param goodsSpecDetails
   * @param images
   * @returns {IMap}
   */
  @Action('goods-detail: info')
  goodsDetail(
    state: IMap,
    {
      skuId,
      goodsInfos,
      goods,
      goodsPropDetailRels,
      goodsSpecs,
      goodsSpecDetails,
      images
    }
  ) {
    //4.根据当前sku信息,标记每个规格项的默认选中值
    let goodsInfoList = fromJS(goodsInfos);
    if (goodsSpecDetails) {
      goodsInfos = goodsInfoList.map((goodsInfo) => {
        //遍历该规格项对应的所有规格值
        let specStr = goodsSpecDetails
          .filter((specDetail) =>
            goodsInfo.get('mockSpecDetailIds').contains(specDetail.specDetailId)
          )
          .map((spec) => {
            return spec.detailName;
          })
          .join(' ');
        return goodsInfo.set('specText', specStr);
      });
    }
    let goodsInfo = fromJS(goodsInfos).find((item) => {
      return item.get('goodsInfoId') == skuId;
    });

    let allImgsIm;

    //零售销售--当前sku+spu的图片,sku图片为空时，取spu的第一张图片作为sku的图片，spu也没有图片时，就是空数组
    allImgsIm = goodsInfo.get('goodsInfoImg')
      ? fromJS([]).push(goodsInfo.get('goodsInfoImg'))
      : images.length > 0
        ? fromJS([]).push(images[0].artworkUrl)
        : fromJS([]).push('');
    if (allImgsIm.size > 0) {
      fromJS(images).map((image) => {
        if (allImgsIm.size >= 10) {
          return;
        }
        allImgsIm = allImgsIm.push(image.get('artworkUrl'));
      });
    }

    return state
      .set('goodsInfo', fromJS(goodsInfo))
      .set(
        'spuContext',
        fromJS({
          skuId,
          goodsPropDetailRels,
          goodsSpecs,
          goodsSpecDetails,
          images
        })
      )
      .set('goods', fromJS(goods))
      .set('goodsPropDetailRels', fromJS(goodsPropDetailRels))
      .set('goodsSpecs', fromJS(goodsSpecs))
      .set('goodsSpecDetails', fromJS(goodsSpecDetails))
      .set('images', allImgsIm)
      .set('showVideo', fromJS(goods).get('goodsVideo') == '' ? false : true);
  }

  @Action('goods-detail:props')
  setGoodsProps(state: IMap, goodsProps) {
    return state.set('goodsProps', goodsProps);
  }

  @Action('goods-detail:url')
  saveUrl(state, url) {
    return state.set('url', url);
  }

  @Action('goodsDetail:toggleShowVideo')
  toggleShowVideo(state) {
    return state.set('showVideo', !state.get('showVideo'));
  }

  //图文详情
  @Action('goods-detail:storeGoodsTabs')
  storeGoodsTabs(state, result) {
    return state.set('storeGoodsTabs', fromJS(result));
  }

  //图文详情tab切换
  @Action('goodsDetail:changeTabKey')
  changeTabKey(state, index) {
    return state.set('tabKey', index);
  }

  //初始化店铺信息
  @Action('goodsDetail:initStore')
  initStore(state: IMap, { storeId, storeName, storeLogo, companyType }) {
    return state.set(
      'store',
      fromJS({
        storeId,
        storeName,
        storeLogo,
        companyType
      })
    );
  }

  @Action('goodsDetail:renderVideo')
  renderVideo(state) {
    return state
      .set('renderVideo', !state.get('renderVideo'))
      .set('renderSlider', !state.get('renderSlider'));
  }

  //图文详情内容
  @Action('goods-detail:storeGoodsTabContent')
  storeGoodsTabContent(state, result) {
    return state.set('storeGoodsTabContent', fromJS(result));
  }

  @Action('change:shareModal')
  changeShareModal(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }
}
