import { Actor, Action, IMap } from 'plume2';

export default class DescActor extends Actor {
  defaultState() {
    return {
      // 商品详情内容
      descData: '',
      // 是否已从后台获取商品详情
      gotDescData: false,
    };
  }

  @Action('desc:data')
  descData(state: IMap, data: string) {
    return state.set('descData', data).set('gotDescData', true);
  }
}
