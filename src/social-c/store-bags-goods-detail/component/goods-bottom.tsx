import React, { Component } from 'react';
import { Relax, IMap, msg } from 'plume2';
import '../css/style.css'

import { Button, noop, history, WMkit } from 'wmkit';

const LongBlueButton = Button.LongBlue;

@Relax
export default class GoodsDetailTitle extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      immediateBuy: Function;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    immediateBuy: noop
  };

  constructor(props) {
    super(props);
  }
  render() {
    const { goodsInfo, immediateBuy } = this.props.relaxProps;
    const invalid = goodsInfo.get('stock') <= 0;

    return (
      <div className="detail-bottom-height">
        <div className="detail-bottom">
          <LongBlueButton
            disabled={invalid}
            defaultStyle={{ height: 47, lineHeight: '47px' }}
            text="立即购买"
            onClick={() => immediateBuy(goodsInfo.get('goodsInfoId'))}
          />
        </div>
      </div>
    );
  }
}
