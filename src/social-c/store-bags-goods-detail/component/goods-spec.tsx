import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';

import { NumberInput, noop, Alert } from 'wmkit';
import { IList } from 'typings/globalType';

@Relax
export default class GoodsDetailSpec extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      goods: IMap;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    goods: 'goods'
  };

  render() {
    const { goodsInfo, goods } = this.props.relaxProps;

    const goodsUnit = goods.get('goodsUnit') || '';
    return (
      <div className="gec-box">
        <div className="detail-gec b-1px-t">
          规格
          <div className="gec">
            <span>
              {goodsInfo.get('specText') ? goodsInfo.get('specText') : '无'}
            </span>
          </div>
        </div>
      </div>
    );
  }
}
