import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { _, WMkit } from 'wmkit';

/**
 * 零售销售类型-价格展示
 */
@Relax
export default class GoodsDetailPriceRetail extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo'
  };

  render() {
    const { goodsInfo } = this.props.relaxProps;
    return (
      <div className="market-price">
        <div className="b-1px-t">
          <div className="price-container">
            <div className="price-box">
              <div className="price-item">
                <i className="iconfont icon-qian" />
                {_.addZero(goodsInfo.get('marketPrice'))}&nbsp;&nbsp;{localStorage.getItem('loginSaleType') == '1'?<span style={{marginLeft: '.26rem',fontSize: '.26rem',color: '#FF4D4D',}}><span style={{color:'#333'}}>预估点数</span>{goodsInfo.get('kmPointsValue')}</span>:null}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
