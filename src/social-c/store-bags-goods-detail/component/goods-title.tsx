import React, { Component } from 'react';
import { Relax, IMap, msg } from 'plume2';
import { WMImage, WMkit } from 'wmkit';

@Relax
export default class GoodsDetailTitle extends Component<any, any> {
  props: {
    relaxProps?: {
      goods: IMap;
    };
  };

  static relaxProps = {
    goods: 'goods'
  };

  render() {
    const { goods } = this.props.relaxProps;
    return (
      <div className="detail-box">
        <div className="title-left">
          <div className="sku-det">
            {goods.get('companyType') === 0 && (
              <div className="self-sales">自营</div>
            )}
            {goods.get('goodsName')}
          </div>
          {goods.get('goodsSubtitle') && (
            <div className="sku-det sec-title">
              {goods.get('goodsSubtitle')}
            </div>
          )}
        </div>
      </div>
    );
  }
}
