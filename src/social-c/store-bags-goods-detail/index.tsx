import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit, WMWholesaleChoose, WMRetailChoose } from 'wmkit';
import { ShareModal, GraphicShare } from 'biz';

import AppStore from './store';
import ImgRetail from './component/img-retail';
import Title from './component/goods-title';
import GoodsPriceRetail from './component/goods-price-retail';
import Spec from './component/goods-spec';
import Desc from './component/goods-desc';
import Bottom from './component/goods-bottom';
import './css/style.css';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsDetail extends React.Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    window.scroll(0, 0);
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    //已登录或未开放，调用原来的
    if (WMkit.isLoginOrNotOpen()) {
      this.store.init(id, this.props.match.url).then((res) => {
        document.title = this.store
          .state()
          .getIn(['goodsInfo', 'goodsInfoName']);
      });
    }
  }

  render() {
    return (
      <div className="goods-detail">
        {/*轮播动图*/}
        <ImgRetail />
        {/*商品名称,收藏*/}
        <Title />
        {/*价格信息*/}
        <GoodsPriceRetail />
        {/*规格*/}
        <Spec />
        {/*商品详情*/}
        <Desc />
        {/*底部加入购物车、立即购买等按钮*/}
        <Bottom />
      </div>
    );
  }
}
