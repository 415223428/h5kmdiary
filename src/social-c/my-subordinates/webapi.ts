import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };
/**
 * 获取开店礼包信息
 */
export const getCommissionList = (params) => {
  return Fetch<TResult>('/customer/getCommissionList', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};
