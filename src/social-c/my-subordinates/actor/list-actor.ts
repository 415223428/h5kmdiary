import { Actor, Action } from 'plume2';

import { fromJS } from 'immutable';

export default class ListActor extends Actor {
  defaultState() {
    return {
      //我的下级详细
      mySubordinates:[],
    };
  }

  /**
   * 赋值我的下级
   */
  @Action('list:mySubordinates')
  changeDetail(state, val) {
    return state.set('mySubordinates', val);
  }
}
