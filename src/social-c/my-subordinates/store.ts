import { Store, IOptions } from 'plume2';
import ListActor from './actor/list-actor';

import { config } from 'config';
import { history } from 'wmkit';
import * as webapi from './webapi';
export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new ListActor()];
  }

  /**
   * 初始化
   */
  init = async (customerId,inviteLevel) => {
    const params = {
      customerId,
      inviteLevel
    }
    const res = await webapi.getCommissionList(params);
    if(res.code=='K-000000'){
      this.dispatch('list:mySubordinates',res.context)
    }
  }

}
