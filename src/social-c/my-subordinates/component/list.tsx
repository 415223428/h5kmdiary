import React, { Component } from 'react';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';
import { WMImage, noop, history, _ } from 'wmkit';

@Relax
export default class SubordinateList extends Component<any, any> {
  props: {
    relaxProps?: {
      mySubordinates: IList;
    };
  };

  static relaxProps = {
    mySubordinates: 'mySubordinates'
  };

  render() {
    const { mySubordinates } = this.props.relaxProps;
    return (
      <div className='subordinate-list'>
        <div className="ul-tab">
          <span>订单编号</span>
          <span>资金类型</span>
          <span>佣金金额</span>
          <span>入账情况</span>
          <span>时间</span>
        </div>
        <div className='ul-list'>
          <ul className="ul-list-li">
            {mySubordinates ?
              mySubordinates.map((i, index) => {
                return (
                  <li key={index}>
                    <div>{i.businessId}</div>
                    <div>{this._getFundsType(i.fundsType)}</div>
                    <div>
                      <i className="iconfont icon-qian" />
                      {i.receiptPaymentAmount}
                    </div>
                    <div>
                      {this._getFundsStatus(i.fundsStatus)}
                    </div>
                    <div>
                      <span>{i.createTime.substring(0, 10)}</span>
                    </div>
                  </li>
                )
              }) : null
            }
          </ul>
        </div>
      </div>
    )
  }
  /**
   * 显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsInfo) => {
    let showPrice = goodsInfo.get('salePrice') || 0;
    return _.addZero(showPrice);
  };

  _getFundsType = (type) => {
    if (type === 1) {
      return '分销佣金'
    } else if (type === 2) {
      return '佣金提现'
    } else if (type === 3) {
      return '邀新奖励'
    } else if (type === 4) {
      return '佣金提成'
    } else if(type === 5){
      return '余额支付'
    }
  }

  _getFundsStatus = (type) =>{
    if(type === 0){
      return '等待入账'
    }else if(type === 1){
      return '已入账'
    }
  }
}
