import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import { history } from 'wmkit';
import SubordinateList from './component/list';
import './css/style.css';

/**
 * 我的下级详情
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class StoreBags extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    let state = history.location.state ? history.location.state : null;
    this.store.init(state.customerId,state.inviteLevel);
    // let customerId='8a8888826d4e0d33016d5d7c5f1201c4'
    // let inviteLevel = '0'
    this.store.init(state.customerId,state.inviteLevel);

  }
  render() {
    return (
      <div >
        <SubordinateList/>
      </div>
    );
  }
}
