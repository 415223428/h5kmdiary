import { Fetch } from 'wmkit';

/**
 * 获取 商品 搜索历史
 */
export const getGoodsHistory = () => {
  return Fetch('/goods/history', {
    method: 'GET'
  });
};

/**
 * 获取 店铺 搜索历史
 */
export const getStoreHistory = () => {
  return Fetch('/store/history', {
    method: 'GET'
  });
};

/**
 * 添加一条 商品 搜索历史
 */
export const addGoodsHistory = (queryString) => {
  return Fetch('/goods/history', {
    method: 'POST',
    body: JSON.stringify({ keyword: queryString })
  });
};

/**
 * 添加一条 店铺 搜索历史
 */
export const addStoreHistory = (queryString) => {
  return Fetch('/store/history', {
    method: 'POST',
    body: JSON.stringify({ keyword: queryString })
  });
};

/**
 * 清空 商品 搜索历史
 */
export const clearGoodsHistory = () => {
  return Fetch('/goods/history', {
    method: 'DELETE'
  });
};

/**
 * 清空 店铺 搜索历史
 */
export const clearStoreHistory = () => {
  return Fetch('/store/history', {
    method: 'DELETE'
  });
};
