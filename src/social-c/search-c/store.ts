import { Store } from 'plume2';
import { fromJS } from 'immutable';
import { config } from 'config';
import * as webapi from './webapi';
import SearchActor from './actor/search-actor';
import { WMkit } from 'wmkit';

export default class AppStore extends Store {
  bindActor() {
    return [new SearchActor()];
  }

  constructor(props) {
    super(props);
    if (__DEV__) {
      //debug
      (window as any)._store = this;
    }
  }

  /**
   * 变更搜索关键字
   */
  changeKeyword = (keyword: string) => {
    this.dispatch('search:keyword', keyword);
  };

  /**
   * 查询搜索历史
   * @returns {Promise<void>}
   */
  getHistory = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      if (this.state().get('key') == 'goods') {
        //如果是查询商品历史
        const res = await webapi.getGoodsHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS(res.context).reverse());
        }
      } else if (this.state().get('key') == 'supplier') {
        //如果是查询店铺历史
        const res = await webapi.getStoreHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS(res.context).reverse());
        }
      }
    }
  };

  /**
   * 添加搜索记录
   * @param {string} keyword
   */
  addHistory = async (keyword: string) => {
    if (WMkit.isLoginOrNotOpen()) {
      if (this.state().get('key') == 'goods') {
        //如果是添加商品历史
        await webapi.addGoodsHistory(keyword);
      } else if (this.state().get('key') == 'supplier') {
        //如果是添加店铺历史
        await webapi.addStoreHistory(keyword);
      }
    }
  };

  /**
   * 清除搜索记录
   */
  clearHistory = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      if (this.state().get('key') == 'goods') {
        //如果是清除商品历史
        const res = await webapi.clearGoodsHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS([]));
        }
      } else if (this.state().get('key') == 'supplier') {
        //如果是清除店铺历史
        const res = await webapi.clearStoreHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS([]));
        }
      }
    }
  };

  /**
   * tab切换
   */
  tabActive = async (tabKey: string) => {
    this.dispatch('tab:history', tabKey);
    await this.getHistory();
  };
}
