import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { history, noop, WMkit } from 'wmkit';
import Tabs from './tab';

@Relax
export default class SearchBar extends React.Component<any, any> {
  _input: any;
  // 点击取消后定时500ms。如果页面没有正常跳转，跳转到首页。如果页面正常跳转了，取消定时器。用于处理直接打开搜索页面后点击取消返回的问题
  _timer: any;

  props: {
    relaxProps?: {
      key: string;
      keyword: string;
      searchHistory: any;
      addHistory: Function;
      clearHistory: Function;
      changeKeyword: Function;
    };
  };

  static relaxProps = {
    key: 'key',
    keyword: 'keyword',
    searchHistory: 'searchHistory',
    addHistory: noop,
    clearHistory: noop,
    changeKeyword: noop
  };

  componentDidMount() {
    this._input.focus();
  }

  componentWillUnmount() {
    if (this._timer) {
      clearTimeout(this._timer);
    }
  }

  render() {
    const { keyword, searchHistory, key } = this.props.relaxProps;

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div className="input-box">
              <i className="iconfont icon-sousuo" onClick={this._goSearch} />
              <form action="javascript:;">
                <input
                  autoFocus={true}
                  type="search"
                  placeholder={'搜索商品'}
                  value={keyword}
                  maxLength={100}
                  ref={(input) => (this._input = input)}
                  onChange={this._inputOnChange}
                  onKeyDown={(e) => {
                    if (e.which == 13) {
                      this._goSearch();
                    }
                  }}
                />
              </form>
            </div>
            <div>
              <a
                href="javascript:;"
                className="search-text"
                onClick={() => {
                  history.goBack();
                  this._timer = setTimeout(() => {
                    // 如果500ms后页面还没有跳转，那么就去首页
                    history.replace('/main');
                  }, 500);
                }}
              >
                取消
              </a>
            </div>
          </div>
        </div>

        <Tabs />

        <div className="search-content">
          <div className="search-title">
            <span>搜索历史</span>
            <a href="javascript:;" onClick={this._clearSearchHistory}>
              清除
            </a>
          </div>
          <div className="history-list">
            {searchHistory.count() > 0 ? (
              searchHistory.toJS().map((s) => {
                return (
                  <div
                    className="item"
                    onClick={() => {
                      this._search(s);
                    }}
                  >
                    {s}
                  </div>
                );
              })
            ) : (
              <span className="no-history">暂无搜索记录</span>
            )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 搜索框内容变化
   * @param e
   * @private
   */
  _inputOnChange = (e) => {
    this.props.relaxProps.changeKeyword(e.target.value);
  };

  /**
   * 输入关键字后进行搜索
   * @private
   */
  _goSearch = () => {
    // 关键字
    const { keyword } = this.props.relaxProps;
    this._search(keyword);
  };

  /**
   * 直接点击已有记录进行搜索
   */
  _search = (keywordTemp) => {
    keywordTemp = keywordTemp.trim();

    const { key } = this.props.relaxProps;
    let reqPath = '/shop-index-c/' + WMkit.inviteeId(); //默认跳转小C-店铺精选页

    if (!keywordTemp) {
      // 不带搜索参数跳转商品列表页
      history.push(`${reqPath}`);
      return;
    }

    // 处理历史搜索记录
    this._handleSearchHistory(keywordTemp);

    // 带搜索参数跳转商品列表页
    history.push(`${reqPath}?q=${encodeURIComponent(keywordTemp)}`);
  };

  /**
   * 处理搜索历史
   * @param keyword
   * @private
   */
  _handleSearchHistory = (keyword) => {
    const { addHistory } = this.props.relaxProps;
    addHistory(keyword);
  };

  /**
   * 清除历史记录
   * @private
   */
  _clearSearchHistory = () => {
    const { clearHistory } = this.props.relaxProps;
    clearHistory();
  };
}
