import React from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
@Relax
export default class Layer extends React.Component<any, any> {
  props: {
    relaxProps?: {
      detailList: string;
      closeLayer: Function;
      picture: string;
    };
  };

  static relaxProps = {
    detailList: 'detailList',
    closeLayer: noop,
    picture: 'picture'
  };

  render() {
    const { detailList, closeLayer, picture } = this.props.relaxProps;
    return (
      <div className="detail-div">
        <div className="scroll-div">
          <div className="white-div">
            <div
              className="about-box"
              dangerouslySetInnerHTML={{
                __html: detailList
              }}
            />
          </div>
        </div>
        <div className="close" onClick={() => closeLayer(false)}>
          <i className="iconfont icon-Close f-style" />
        </div>
      </div>
    );
  }
}
