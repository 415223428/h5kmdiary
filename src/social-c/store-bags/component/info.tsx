import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';

@Relax
export default class Info extends Component<any, any> {
  props: {
    relaxProps?: {
      _openDetail: Function;
      picture: string;
    };
  };

  static relaxProps = {
    _openDetail: noop,
    picture: 'picture'
  };

  render() {
    const { _openDetail, picture } = this.props.relaxProps;

    return (
      <div className="bags-container">
        <div className="bags">
          <div
            className="bags-bg"
            style={{ backgroundImage: `url(${picture})` }}
          >
            <a
              className="infoBox"
              href="javascript:;"
              onClick={() => _openDetail(true)}
            >
              详细说明&nbsp;&nbsp;
              <i className="iconfont icon-help" />
            </a>
          </div>
        </div>
      </div>
    );
  }
}
