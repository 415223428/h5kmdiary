import React, { Component } from 'react';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';
import { WMImage, noop, history, _ } from 'wmkit';

@Relax
export default class List extends Component<any, any> {
  props: {
    relaxProps?: {
      //礼包商品
      goodsList: IList;
    };
  };

  static relaxProps = {
    goodsList: 'goodsList'
  };

  render() {
    const { goodsList } = this.props.relaxProps;
    return (
      <div className="bags-list">
        <p className="title">超值开店礼包</p>
        <ul className="bags-item">
          {goodsList.map((goodsInfo, index) => {
            // 库存等于0或者起订量大于剩余库存
            const invalid = goodsInfo.get('stock') <= 0;
            let containerClass = invalid
              ? 'bags-box invalid-goods'
              : 'bags-box';
            return (
              <li
                className={containerClass}
                key={goodsInfo.get('goodsInfoId')}
                onClick={() => {
                  if (!invalid) {
                    history.push(
                      '/store-bags-goods-detail/' + goodsInfo.get('goodsInfoId')
                    );
                  }
                }}
              >
                <div className="img-box">
                  <WMImage
                    src={goodsInfo.get('goodsInfoImg')}
                    width="100%"
                    height="100%"
                  />
                </div>

                <p className="bags-title">
                  {goodsInfo.get('companyType') == 0 && (
                    <div className="self-sales">自营</div>
                  )}
                  {goodsInfo.get('goodsInfoName')}
                </p>
                <p className="bags-spec">{goodsInfo.get('specText')}</p>
                <p className="price">
                  <i className="iconfont icon-qian" />
                  {this._calShowPrice(goodsInfo)}{localStorage.getItem('loginSaleType') == '1'?<span style={{marginLeft: '.26rem',fontSize: '.24rem',color: '#FF4D4D',}}><span style={{color:'#333'}}>预估点数</span>{goodsInfo.get('kmPointsValue')}</span>:null}
                  {invalid && <div className="out-stock">缺货</div>}
                </p>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
  /**
   * 显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsInfo) => {
    let showPrice = goodsInfo.get('salePrice') || 0;
    return _.addZero(showPrice);
  };
}
