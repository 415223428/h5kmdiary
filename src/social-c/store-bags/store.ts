import { Store, IOptions } from 'plume2';
import StoreActor from './actor/store-actor';

import { config } from 'config';
import { history } from 'wmkit';
import * as webapi from './webapi';
export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new StoreActor()];
  }

  /**
   * 初始化
   */
  init = async () => {
    //获取优惠券分类
    let storeBagsRes = (await webapi.storeBags()) as any;
    //分销开关打开且招募开关打开且为购买升级，才能进入该页面
    if (
      storeBagsRes.code === config.SUCCESS_CODE &&
      storeBagsRes.context.applyFlag == 1 &&
      storeBagsRes.context.applyType == 0
    ) {
      let recruitDesc = storeBagsRes.context.recruitDesc;
      let goodsInfos = storeBagsRes.context.goodsInfos;
      let recruitImg = storeBagsRes.context.recruitImg;
      this.dispatch('InvitActor:getGoodsList', goodsInfos);
      this.dispatch('InvitActor:getDetailData', recruitDesc);
      this.dispatch('InvitActor:getDetailImg', recruitImg);
    } else {
      history.replace('/error');
    }
  };

  /**
   * 打开详情弹层
   */
  _openDetail = async (val) => {
    this.dispatch('InvitActor:changeDetail', val);
  };

  /**
   * 关闭详情弹层
   */
  closeLayer = async (val) => {
    this.dispatch('InvitActor:changeDetail', val);
  };
}
