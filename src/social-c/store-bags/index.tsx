import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';

import Info from './component/info';
import List from './component/list';
import Layer from './component/layer';
import './css/style.css';

/**
 * 开店礼包
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class StoreBags extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }
  componentWillUnmount(){
    const picture = document.getElementsByClassName('view-large')[0];
    {
      picture?
      picture.remove():null
    }
  }
  render() {
    let detailState = this.store.state().get('detailState');
    return (
      <div className="container">
        {/*礼包说明*/}
        <Info />

        {/*开店礼包列表*/}
        <List />

        {detailState && <Layer />}
      </div>
    );
  }
}
