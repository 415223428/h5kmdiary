import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };
/**
 * 获取开店礼包信息
 */
export const storeBags = () => {
  return Fetch<TResult>('/distribute/storeBags', {
    method: 'POST'
  });
};
