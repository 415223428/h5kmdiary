import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit, WMWholesaleChoose, WMRetailChoose, _ } from 'wmkit';
import { cache } from 'config';
import { ShareModal, GraphicShare } from 'biz';

import AppStore from './store';
import ImgSlider from './component/img-slider';
import ImgRetail from './component/img-retail';
import Title from './component/goods-title';
import GoodsPriceWholesale from './component/goods-price-wholesale';
import GoodsPriceRetail from './component/goods-price-retail';
import Spec from './component/goods-spec';
import Desc from './component/goods-desc';
import Bottom from './component/goods-bottom';
import StoreName from './component/store-name';
import Promotion from './component/goods-promotion';
import PromotionShade from './component/promotion-shade';
import './css/style.css';
import GoodsCoupon from './component/goods-coupon';
import CouponMask from './component/coupon-mask';
import HeaderSocial from './component/header-social';
import GoodsShare from './component/goods-share';
import GoodsEvaluation from './component/goods-evaluation';
import GoodsShareImg from './component/goods-share-img';
import wx from 'weixin-js-sdk';
import { truncateSync } from 'fs';
import { fromJS } from 'immutable';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsDetail extends React.Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
    this.state = props;
    //这边判断是否是微信小程序环境
    console.log('wx====>', wx);
    wx.miniProgram.getEnv(function (res) {
      (window as any).isMiniProgram = res.miniprogram;
    });
    //获取分销员customerId
    const { id, goodsId, skuId, shareType } = this.props.match.params;
    //并分销员会员ID和分销渠道存入缓存，分销渠道为店铺
    if (id != '0') {
      WMkit.setShareInfoCache(id, '2');
    }
  }

  _parseQueryString = (url) => {
    var obj = {};
    var keyvalue = [];
    var key = "",
      value = "";
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    for (var i in paraString) {
      keyvalue = paraString[i].split("=");
      key = keyvalue[0];
      value = keyvalue[1];
      obj[key] = value;
    }
    return obj;
  }

  componentWillMount() {
    window.scroll(0, 0);
  }

  componentDidMount() {
    const { id, goodsId, skuId } = this.props.match.params;
    //已登录或未开放，调用原来的
    if (WMkit.isLoginOrNotOpen()) {
      this.store.memberInfo();
      if (id != '0') {
        this.store
          .init(id, goodsId, skuId, this.props.match.url)
          .then((res) => {
            document.title = this.store
              .state()
              .getIn(['goodsInfo', 'goodsInfoName']);
          });
      } else {
        const loginData = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
        let customerId = loginData.customerId;
        this.store
          .init(customerId, goodsId, skuId, this.props.match.url)
          .then((res) => {
            document.title = this.store
              .state()
              .getIn(['goodsInfo', 'goodsInfoName']);
          });
      }
    } else {
      //未登录并且已经开放的，调用新的
      this.store
        .initWithOutLogin(id, goodsId, skuId, this.props.match.url)
        .then((res) => {
          document.title = this.store
            .state()
            .getIn(['goodsInfo', 'goodsInfoName']);
        });
    }
  }

  render() {
    //商品名称传递到小程序里
    if (this.store.state().get('goods').size > 0) {
      wx.miniProgram.getEnv((res) => {
        if (res.miniprogram) {
          wx.miniProgram.postMessage({
            data: {
              type: 'goodsShare',
              goodsName: this.store
                .state()
                .get('goods')
                .get('goodsName'),
              skuId: this.store
                .state()
                .get('spuContext')
                .get('skuId'),
              shareUserId: WMkit.getUserId(),
              token: window.token
            }
          });
        }
      });
    }
    const id = this.props.match.params.id;
    const saleType = this.store.state().getIn(['goods', 'saleType']);
    let goodsInfo = this.store.state().get('goodsInfo');
    //当商品允许分销且分销审核通过，视为分销商品，不展示促销和优惠券
    const distributionFlag = goodsInfo.get('distributionGoodsAudit') == 2;
    const goodsId = this.store.state().get('goodsId');
    //当前选中商品是否为正在进行的抢购活动
    let flashsaleGoodsFlag = false;
    let flashsalePrice;
    const { isDistributor } = this.state;
    const search = this.props.location.search;
    const { inviteCode }: any = this._parseQueryString(search);
    // sessionStorage.getItem('inviteCode');
    { inviteCode ? sessionStorage.setItem('otherinviteCode', inviteCode) : null }
    return (
      <div>
        {/* 分享图片 */}
        {/* <GoodsShareImg
          flashsalePrice={flashsalePrice}
          flashsaleGoodsFlag={flashsaleGoodsFlag} /> */}
        <div className="goods-detail" style={{ position: 'relative' }}>
          {WMkit.isDistributorLogin() &&
            id == '0' && <HeaderSocial skuId={this.props.match.params.skuId} />}

          {/*轮播动图*/}
          {saleType === 0 ? <ImgSlider /> : <ImgRetail />}

          {/*价格信息*/}
          {saleType === 0 ?
            <GoodsPriceWholesale /> :
            <GoodsPriceRetail
              flashsalePrice={flashsalePrice}
              flashsaleGoodsFlag={flashsaleGoodsFlag} />}

          {/*商品名称,收藏*/}
          <Title />

          {//该商品有促销活动时，展示促销活动详情
            !distributionFlag && this.store.state().get('goodsInfo').size > 0 ? (
              this.store
                .state()
                .get('goodsInfo')
                .get('marketingLabels').size == 0 ? null : (
                  <Promotion />
                )
            ) : null}

          {/*优惠券信息*/}
          {!distributionFlag && this.store.state().get('goodsInfo').size > 0 ? (
            this.store
              .state()
              .get('goodsInfo')
              .get('couponLabels').size == 0 ? null : (
                <GoodsCoupon />
              )
          ) : null}


          {/*规格*/}
          <Spec />

          {/*店铺*/}
          <StoreName />

          {/* 评价 */}
          <GoodsEvaluation />
          {/* {this.store.state().get('isShow') &&
            !this.store
              .state()
              .get('top3Evaluate')
              &&<GoodsEvaluation />
          } */}

          {/*商品详情*/}
          <Desc />

          {/*批发销售类型-规格选择弹框*/}
          <WMWholesaleChoose
            data={this.store
              .state()
              .get('spuContext')
              .toJS()}
            visible={this.store.state().get('wholesaleVisible')}
            isImmediate={this.store.state().get('isImmediate')}
            changeSpecVisible={this.store.changeWholesaleVisible}
          />
          {/*零售销售类型-规格选择弹框*/}
          <WMRetailChoose
            data={this.store
              .state()
              .get('spuContext')
              .toJS()}
            visible={this.store.state().get('retailSaleVisible')}
            isImmediate={this.store.state().get('isImmediate')}
            changeSpecVisible={this.store.changeRetailSaleVisible}
            dataCallBack={this.store.changeSpecVisibleAndRender}
          />

          {/*如果是微信小程序环境,就显示分享按钮*/}
          {/* {(window as any).isMiniProgram && <GoodsShare />} */}

          {/*底部加入购物车、立即购买等按钮*/}
          <Bottom />
        </div>
      </div>
    );
  }
}
