import React, { Component } from 'react';

import { IMap, Relax } from 'plume2';
import { List, fromJS } from 'immutable';
import { IList } from 'typings/globalType';
import { storage, WMImage, Slider } from 'wmkit';
import { cache } from 'config';
import 'video.js/dist/video-js.css';

const defaultImg = require('../images/none.png');
const windowWidth = window.innerWidth.toString() + 'px';

@Relax
export default class ImgRetail extends Component<any, any> {
  static relaxProps = {
    goodsInfo: 'goodsInfo',
    goods: 'goods',
    renderVideo: 'renderVideo',
    renderSlider: 'renderSlider',
    images: 'images'
  };

  props: {
    relaxProps?: {
      goodsInfo: IMap;
      goods: IMap;
      renderVideo: boolean;
      renderSlider: boolean;
      images: IList;
    };
  };

  render() {
    const { goodsInfo, goods, images, renderSlider } = this.props.relaxProps;

    let sliderImages = images;
    //图片为空的SKU统一都变为默认图片
    sliderImages.map((v, index) => {
      if (v == '') {
        sliderImages = sliderImages.set(index, defaultImg);
      }
    });

    //之后如果有交互,从商品列表页面进入,首先加载传递过来的图片,此处代码保留,暂定
    let loading = false;
    if (loading) {
      const goodsFirstImage = storage('session').get(cache.GOODS_FIRST_IMAGE);
      //有首图的情况下
      return goodsFirstImage ? (
        <div className="slider" style={{ height: windowWidth }}>
          <WMImage
            src={goodsFirstImage}
            width={windowWidth}
            height={windowWidth}
          />
        </div>
      ) : (
        <div
          className="slider loading-inset"
          style={{ height: window.innerWidth }}
        />
      );
    }
    return !goods.isEmpty() ? (
      <div className="slider">
        <Slider
          videoPlay={goods.get('goodsVideo')}
          renderVideo={this.props.relaxProps.renderVideo}
          renderSlider={this.props.relaxProps.renderSlider}
          data={sliderImages}
          height={windowWidth}
          width={windowWidth}
          zoom={true}
          videoJsOptions={{
            autoplay: false,
            controls: true,
            playsinline: true, //ios端行内播放视频
            allowFullScreen: false,
            language: 'chz', //设置语言
            sources: [
              {
                src: goods.get('goodsVideo'),
                type: 'video/mp4'
              }
            ],
            controlBar: {
              fullscreenToggle: false //是否显示全屏按钮
            }
          }}
        />
      </div>
    ) : (
      <div className="slider" style={{ height: windowWidth }}>
        <WMImage src={defaultImg} width={windowWidth} height={windowWidth} />
      </div>
    );
  }
}
