import React, { Component } from 'react';
import { Relax, IMap, msg } from 'plume2';
import { noop, WMImage, WMkit, Fetch, Alert,history } from 'wmkit';
import { IList } from 'typings/globalType';
import { cache, config } from 'config';

@Relax
export default class GoodsDetailTitle extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      follow: boolean;
      changeFollow: Function;
      init: Function;
      url: String;
      goods: IMap;
      goodsInfos: IList;
      isfenxiang: boolean;
      changefenxiang: Function;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    follow: 'follow',
    changeFollow: noop,
    init: noop,
    url: 'url',
    goods: 'goods',
    goodsInfos: 'goodsInfos',
    isfenxiang: 'isfenxiang',
    changefenxiang: noop,
  };

  parseQueryString(url) {
    var obj = {};
    var keyvalue = [];
    var key = "",
      value = "";
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    for (var i in paraString) {
      keyvalue = paraString[i].split("=");
      key = keyvalue[0];
      value = keyvalue[1];
      obj[key] = value;
    }
    return obj;
  }

  render() {
    const { goodsInfo, follow, goods, changefenxiang } = this.props.relaxProps;
    const isdistributor = WMkit.isShowDistributionButton();
    const { channel }: any = this.parseQueryString(location.href);
    return (
      <div className="detail-box">
        <div className="title-left">
          {/*商品编号功能产品要删除的*/}
          {/*<div className="sku-title">{goodsInfo.get('goodsInfoNo')}</div>*/}
          <div className="sku-det" style={{ fontWeight: 'bold' }}>
            {goods.get('goodsName')}</div>
          {goods.get('goodsSubtitle') && (
            <div className="sku-det sec-title">
              {goods.get('goodsSubtitle')}
            </div>
          )}
        </div>
        <div
          className="right-bar-item b-1px-l"
          style={{ minWidth: 70, color: '#333333' }}
          onClick={() => this.collect()}
        >
          {follow ? (
            <img src={require('../images/heart.png')} alt="" style={{
              width: '0.34rem',
              height: '0.34rem',
            }} />
          ) : (
              <img src={require('../images/heartblank.png')} alt="" style={{
                width: '0.34rem',
                height: '0.34rem',
              }} />
            )}
          <span>{follow ? '已收藏' : '收藏'}</span>
        </div>
        {
          isdistributor ?
            <div
              className="right-bar-item b-1px-l"
              style={{ minWidth: '1rem', color: '#333333' }}
              onClick={() => {
                this.shareMain()
              }}
            >
              <img src={require('../images/share.png')} alt=""
                style={{
                  width: '0.34rem',
                  height: '0.34rem',
                }} />
              <span>分享</span>
            </div> : null
        }

      </div>
    );
  }
  collect() {
    const {
      follow,
      changeFollow,
      goodsInfo,
      init,
      url,
      goods,
      goodsInfos
    } = this.props.relaxProps;
    let saleType = goods.get('saleType');
    if (WMkit.isLoginOrNotOpen()) {
      changeFollow(
        !follow,
        saleType == 0
          ? goodsInfos.get(0).get('goodsInfoId')
          : goodsInfo.get('goodsInfoId')
      );
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          init(
            saleType == 0
              ? goodsInfos.get(0).get('goodsInfoId')
              : goodsInfo.get('goodsInfoId'),
            url
          );
        }
        //callBack:null
      });
    }
  }
  save() {
    console.log('%csave', 'color:red');
  }
  shareMain() {
    const {
      goodsInfo,
      init,
      url,
      goods,
      goodsInfos,
      changefenxiang
    } = this.props.relaxProps;
    const customerId = localStorage.getItem(cache.LOGIN_DATA)
    const inviteeId = customerId?JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId : '';
    const inviteCode = sessionStorage.getItem('inviteCode');
    const lineShowPrice = this._wholeSalePriceInfo(
      goods.get('linePrice'),
      goodsInfos
    );
    const newUrl = `shop-index/goods-detail/${inviteeId}/${goodsInfo.get('goodsId')}/${goodsInfo.get('goodsInfoId')}/?channel=mall&inviteCode=${inviteCode}`;
    let saleType = goods.get('saleType');
    if (WMkit.isLoginOrNotOpen()) {
      Fetch('/distributor-goods/add', {
        method: 'POST',
        body: JSON.stringify({
          goodsInfoId: goodsInfo.get('goodsInfoId'),
          goodsId: goodsInfo.get('goodsId'),
          storeId: goodsInfo.get('storeId')
        })
      }).then((res) => {
        if (res.code != config.SUCCESS_CODE) {
          Alert({
            text: res.message
          });
        }
      });
      console.log(111111111111111111);
      
      history.push({
        pathname: '/share',
        state: {
          goodsInfo ,
          url,
          lineDrawingPrice:lineShowPrice,
        }
      });

    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          init(
            saleType == 0
              ? goodsInfos.get(0).get('goodsInfoId')
              : goodsInfo.get('goodsInfoId'),
            url
          )
        }
        //callBack:null
      });
    }
  }
  _wholeSalePriceInfo = (linePrice, goodsInfos) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        // 已登录时,找出最高的市场价
        let maxMarketPrice = null;
        goodsInfos.forEach((info, index) => {
          if (index === 0) {
            maxMarketPrice = info.get('marketPrice');
          } else {
            maxMarketPrice =
              info.get('marketPrice') > maxMarketPrice
                ? info.get('marketPrice')
                : maxMarketPrice;
          }
        });
        return maxMarketPrice;
      } else {
        return null;
      }
    }
  };
}
