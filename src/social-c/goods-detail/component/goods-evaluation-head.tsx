import React, { PureComponent } from 'react';
import { Relax } from 'plume2';

@Relax
export default class GoodsEvaluationHead extends PureComponent<any, any> {
  static relaxProps = {};

  constructor(props) {
    super(props);
  }

  render() {
    const {} = this.props.relaxProps;
    return (
      <div className="evaluation-head">
        <div className="evaluation-head-list">
          <div className="evaluation-head-list-item active" onClick={() => {}}>
            商品
          </div>
          <div className="evaluation-head-list-item" onClick={() => {}}>
            详情
          </div>
          <div className="evaluation-head-list-item">评价</div>
        </div>
      </div>
    );
  }
}
