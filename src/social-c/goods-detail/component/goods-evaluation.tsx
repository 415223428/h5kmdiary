import React, { PureComponent } from 'react';
import { Relax } from 'plume2';
import { _, history } from 'wmkit';
import GoodsEvaluationItem from './goods-evaluation-item';
import { div } from 'wmkit/common/util';
import { IList } from 'typings/globalType';

const windowHeight = window.innerHeight.toString() + 'px';

@Relax
export default class GoodsEvaluation extends PureComponent<any, any> {
  static relaxProps = {
    top3Evaluate: 'top3Evaluate',
    goods: 'goods'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { top3Evaluate, goods } = this.props.relaxProps;

    //评价总数
    const evaluateConut = top3Evaluate.getIn([
      'goodsEvaluateCountResponse',
      'evaluateConut'
    ]);
    let count;
    if (evaluateConut > 10000) {
      count = _.div(evaluateConut, 10000).toFixed(1) + '万+';
    } else {
      count = evaluateConut||0;
    }
    const evaluateResp: IList = top3Evaluate.getIn([
      'listResponse',
      'goodsEvaluateVOList'
    ]);

    const praise = top3Evaluate.getIn(['goodsEvaluateCountResponse', 'praise'])
      ? top3Evaluate.getIn(['goodsEvaluateCountResponse', 'praise'])
      : 100;

    return (
      <div className="content evaluation b-1px-b" id="evalaute">
        <div
          className="evaluation-top b-1px-b"
          onClick={() => {
            history.push(`/evaluation-list/${goods.get('goodsId')}`);
          }}
        >
          <span className="evaluation-text">评价 (<span style={{
            color: '#FF4D4D'
          }}>{count}</span>)</span>
          {/* <span className="evaluation-text text-grey">{praise}%好评</span> */}
          <span>
            <span className="evaluation-text text-grey" style={{
              color: '#333333',
            }}>查看全部
          </span>
            <img src={require('../images/chakan.png')} alt="" style={{
              width: '.17rem', marginLeft: '.2rem',
              position: 'relative',
              top: '.03rem'
            }} />
          </span>
        </div>
        {count == 0 ? (
          <div className="no-evaluation">
            <img src={require('../images/no-evaluation.png')} alt="" />
            <p>暂无评价</p>
          </div>
        ) : (
            evaluateResp &&
            !evaluateResp.isEmpty() && (
              <div>
                <div className="evaluation-list">
                  <GoodsEvaluationItem />
                </div>
                {/* <div className="evaluation-bottom">
                  <a
                    className="evaluation-bottom-btn"
                    onClick={() => {
                      history.push(`/evaluation-list/${goods.get('goodsId')}`);
                    }}
                  >
                    查看全部评价
                </a>
                </div> */}
              </div>
            )
          )}
      </div>
    );
  }
}
