import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import wx from 'weixin-js-sdk';
import { IList } from 'typings/globalType';
import { cache } from 'config';
import { noop, _, WMkit } from 'wmkit';

@Relax
export default class GoodsShare extends Component<any, any> {
  props: {
    relaxProps?: {
      shareVisible: boolean;
      changeShare: Function;
      goodsInfo: IMap;
      goods: IMap;
      images: IList;
      goodsInfos: IList;
      spuContext: IMap;
    };
  };

  static relaxProps = {
    shareVisible: 'shareVisible',
    changeShare: noop,
    goodsInfo: 'goodsInfo',
    goods: 'goods',
    images: 'images',
    goodsInfos: 'goodsInfos',
    spuContext: 'spuContext'
  };

  render() {
    const { shareVisible, changeShare, goodsInfo } = this.props.relaxProps;
    return (
      <div className="goods-share">
        <div className="share-btn" onClick={() => changeShare()}>
          <i className="iconfont icon-fenxiang" />
          {WMkit.isDistributorLogin() &&
            goodsInfo.get('distributionGoodsAudit') == '2' && (
              <span>分享赚</span>
            )}
        </div>
        {shareVisible && (
          <div className="poper">
            <div
              className="mask"
              onClick={() => {
                changeShare(false);
              }}
            />
            <div className="poper-content sharingLayer">
              {WMkit.isDistributorLogin() &&
              goodsInfo.get('distributionGoodsAudit') == '2' ? (
                <div className="title b-1px-b">
                  <p className="price">
                    赚{goodsInfo.get('distributionCommission')}元
                  </p>
                  <p className="infoText">
                    好友通过你分享的链接购买商此商品，你就能赚{goodsInfo.get(
                      'distributionCommission'
                    )}元的利润~
                  </p>
                </div>
              ) : (
                <div className="share-title">分享给好友</div>
              )}
              <div
                className="icon-content"
                style={{ justifyContent: 'center' }}
              >
                <div
                  className="icon"
                  onClick={() => {
                    this.jumpSmallProgram();
                  }}
                >
                  <img src={require('../images/photo-album.png')} alt="" />
                  <p>图文分享</p>
                </div>
              </div>
              <div
                className="share-btn-pro b-1px-t"
                onClick={() => {
                  changeShare(false);
                }}
              >
                取消分享
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }

  /**
   * 跳转到微信小程序图文分享
   */
  jumpSmallProgram = () => {
    const { goodsInfo, goods } = this.props.relaxProps;
    let lineShowPrice = goods.get('linePrice');
    let loginData = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
    let customerId = loginData.customerId;
    //拼装小程序图文分享需要的参数
    let params = {
      goodsInfoName: goodsInfo.get('goodsInfoName'),
      goodsInfoImg: goodsInfo.get('goodsInfoImg'),
      specText: goodsInfo.get('specText'),
      marketPrice: goodsInfo.get('marketPrice'),
      lineShowPrice: lineShowPrice ? '￥' + lineShowPrice : null,
      skuId: goodsInfo.get('goodsInfoId'),
      //分享类型，0：普通分享，1：分享赚(店铺内) ,2：分享赚（店铺外）3:邀新，4：分享店铺
      shareType: 1,
      //inviteId==0,即为小B主动触发，传customerId过去，否则就视为小C打开的二次分享来处理。
      inviteeId: WMkit.inviteeId() ? WMkit.inviteeId() : customerId,
      goodsId: goods.get('goodsId')
    };
    //这边获取下商品的数据等
    wx.miniProgram.navigateTo({
      url: `../canvas/canvas?data=${JSON.stringify(params)}`
    });
    // wx.miniProgram.navigateTo({url: `/pages/canvas/canvas?data=111`})
  };
}
