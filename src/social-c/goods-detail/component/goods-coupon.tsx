import React from 'react';
import { IMap, Relax, msg } from 'plume2';

import { CouponLabel, noop, WMkit } from 'wmkit';

@Relax
export default class GoodsCoupon extends React.Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      init: Function;
      url: String;
      changeCouponMask: Function;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    init: noop,
    url: 'url',
    changeCouponMask: noop
  };
  render() {
    const { changeCouponMask, goodsInfo } = this.props.relaxProps;
    let couponLabels =
      goodsInfo.size > 0 && goodsInfo.get('couponLabels').size > 0
        ? goodsInfo.get('couponLabels')
        : null;
    return couponLabels ? (
      <div
        className="gec-box"
        onClick={() => {
          changeCouponMask();
        }}
      >
        <div className="detail-gec">
          <span className="gec-title">领券</span>
          <div className="gec">
            {couponLabels.slice(0, 3).map((label, i) => {
              return <CouponLabel key={i} text={label.get('couponDesc')} />;
            })}
          </div>
          <i className="iconfont icon-jiantou1" />
        </div>
      </div>
    ) : null;
  }
}
