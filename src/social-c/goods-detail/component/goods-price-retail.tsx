import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { _, WMkit } from 'wmkit';

/**
 * 零售销售类型-价格展示
 */
@Relax
export default class GoodsDetailPriceRetail extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      goods: IMap;
    };
    //秒杀单价
    flashsalePrice: Number;
    //是否正在秒杀活动中
    flashsaleGoodsFlag: boolean;
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    goods: 'goods'
  };

  render() {
    const { goodsInfo, goods } = this.props.relaxProps;
    const { flashsalePrice, flashsaleGoodsFlag } = this.props;
    //划线价
    const lineShowPrice = this._originPriceInfo(
      goods.get('linePrice'),
      goodsInfo
    );
    //当商品允许分销且分销审核通过，视为分销商品
    const distributionFlag = goodsInfo.get('distributionGoodsAudit') == '2';
    return (
      <div className="market-price" style={{padding:'0 0.08rem'}}>
        <div className="b-1px-t">
          <div className="price-container" style={{margin:0,padding: '0.37rem 0 0.15rem'}}>
            <div className="price-box" style={{width:'100%'}}>
              <div className="price-item" style={{paddingRight:'.1rem',height:"100%",fontSize:'0.36rem',width:'100%',display:'flex',justifyContent:'space-between'}}>
                <div>
                  <i className="iconfont icon-qian" style={{fontSize:'0.36rem'}}/>
                  {distributionFlag
                    ? _.addZero(goodsInfo.get('marketPrice'))
                    : _.addZero(goodsInfo.get('salePrice'))}&nbsp;&nbsp;
                  {!!lineShowPrice && (
                    <span className="line-price">
                      ￥{_.addZero(lineShowPrice)}
                    </span>
                  )}

                </div>
                <div>
                  {WMkit.isDistributor() &&
                    !flashsaleGoodsFlag &&
                    // !sessionStorage.getItem(cache.INVITEE_ID) &&
                    goodsInfo.get('distributionGoodsAudit') == '2' && (
                      <span style={styles.commission}>
                        <span style={{color:'#333'}}>赚</span>{_.addZero(goodsInfo.get('distributionCommission'))}元
                      </span>
                  )}
                  {localStorage.getItem('loginSaleType') == '1'?<span style={styles.commission}><span style={{color:'#333'}}>预估点数</span>{goodsInfo.get('kmPointsValue')}</span>:null}

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 获取是否展示划线价,以及划线价
   *   a.若划线价存在,则展示
   *   b.若划线价不存在
   *     b.1.登录前,不展示
   *     b.2.登陆后,展示sku市场价
   * @private
   */
  _originPriceInfo = (linePrice, goodsInfoIm) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        return goodsInfoIm.get('marketPrice');
      } else {
        return null;
      }
    }
  };
}
const styles = {
  commission: {
    marginLeft: '.2rem',
    fontSize: '.26rem',
    color: '#FF4D4D',

  },
  colorStyle: {
    color: '#fff'
  }
};
