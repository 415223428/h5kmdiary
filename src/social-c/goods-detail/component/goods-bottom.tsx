import React, { Component } from 'react';
import { Relax, IMap, msg } from 'plume2';
import { cache } from 'config';
import { Button, noop, history, WMkit } from 'wmkit';

const SubmitButton = Button.Submit;
const service=require('../images/service.png')
const car=require('../images/car.png')
const home=require('../images/home.png')
const purCar=require('../images/join-car.png')
const buying=require("../images/buying.png")
@Relax
export default class GoodsDetailTitle extends Component<any, any> {
  props: {
    relaxProps?: {
      purchaseCount: number;
      store: IMap;
      onlineServiceFlag: boolean;
      goods: IMap;
      goodsInfo: IMap;
      updatePurchaseCount: () => {};
      changeWholesaleVisible: Function;
      changeRetailSaleVisible: Function;
      isImmediateDo: Function,
      isNotImmediateDo: Function,
    };
  };

  static relaxProps = {
    purchaseCount: 'purchaseCount',
    store: 'store',
    onlineServiceFlag: 'onlineServiceFlag',
    goods: 'goods',
    goodsInfo: 'goodsInfo',
    updatePurchaseCount: noop,
    changeWholesaleVisible: noop,
    changeRetailSaleVisible: noop,
    isImmediateDo: noop,
    isNotImmediateDo: noop,
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    // 商品详情中购物车数量角标更新方法
    msg.on('purchaseNum', this.props.relaxProps.updatePurchaseCount);
  }

  componentWillUnmount() {
    msg.off('purchaseNum', this.props.relaxProps.updatePurchaseCount);
  }

  render() {
    const {
      purchaseCount,
      store,
      onlineServiceFlag,
      changeWholesaleVisible,
      changeRetailSaleVisible,
      goods,
      isImmediateDo,
      isNotImmediateDo,
      goodsInfo
    } = this.props.relaxProps;

    return (
      <div className="detail-bottom-height">
        <div className="detail-bottom" style={{ height: 'auto' }}>
          <div className="detail-bottom-bar" style={{height:'100%'}}>
            <div className="bar-item-box">
              {/*{onlineServiceFlag &&*/}
              {!(window as any).isMiniProgram && (
                  <div
                    className="bar-item-shopindex"
                    // style={{ minWidth: 70 }}
                    onClick={() => {
                      if (WMkit.isLoginOrNotOpen()) {
                        WMkit.getQiyuCustomer();
                        // history.push(`/chose-service/${store.get('storeId')}`);
                        (window as any).ysf('product', {
                          show: 1, // 1为打开， 其他参数为隐藏（包括非零元素）
                          title: goods.get('goodsName'),
                          desc: goods.get('goodsSubtitle') || '',
                          picture: goods.get('goodsImg'),
                          note: `￥${goodsInfo.get('grouponPrice')}` || '',
                          url: `https://m.kmdiary.com${location.pathname}`
                        });
                        window.location.href = (window as any).ysf('url');
                        (window as any).openqiyu();
                      } else {
                        msg.emit('loginModal:toggleVisible', {
                          callBack: () => {
                            // history.push(
                            //   `/chose-service/${store.get('storeId')}`
                            // );
                            WMkit.getQiyuCustomer();
                            (window as any).ysf('product', {
                              show: 1, // 1为打开， 其他参数为隐藏（包括非零元素）
                              title: goods.get('goodsName'),
                              desc: goods.get('goodsSubtitle'),
                              picture: goods.get('goodsImg'),
                              note: `￥${goodsInfo.get('grouponPrice')}`,
                              url: `https://m.kmdiary.com${location.pathname}`
                            });
                            window.location.href = (window as any).ysf('url');
                            (window as any).openqiyu();
                          }
                        });
                      }
                    }}
                  >
                    {/* <i className="iconfont icon-kefu2" /> */}
                    <img src={service} alt="" />
                    <span>客服</span>
                  </div>
                 )}
              <div
                className="bar-item-shopindex"
                onClick={
                  () => this._goStore()
                  // history.push(`/store-main/${store.get('storeId')}`)
                }
              >
                <img src={home} alt=""/>
                <span>店铺</span>
              </div>
              <div
                className="bar-item-shopindex"
                onClick={() => history.push('/purchase-order')}
              >
                <div className="cart-content">
                  {purchaseCount > 0 && (
                    <div className="num-tips" style={{top:'-1px',right:'-10px',width:'18px',height:'18px'}}>
                      {purchaseCount}
                    </div>
                  )}
                  <img src={car} alt=""/>
                </div>
                <span className="red">购物车</span>
              </div>
            </div>

            <div style={{display:'flex',alignItems:'center'}}>
              {/* <SubmitButton
                defaultStyle={{
                  height: 47,
                  lineHeight: '47px',
                  backgroundColor: '#000',
                  borderColor: '#000'
                }}
                text="加入购物车"
                onClick={() =>
                  goods.get('saleType') == 0
                    ? changeWholesaleVisible()
                    : changeRetailSaleVisible(true)
                }
              />
              <SubmitButton
                defaultStyle={{ height: 47, lineHeight: '47px' }}
                text="立即购买"
                onClick={() =>
                  goods.get('saleType') == 0
                    ? changeWholesaleVisible()
                    : changeRetailSaleVisible(true)
                }
              /> */}
              <img src={purCar} alt="" style={{width:'2.22rem',height:'100%'}} onClick={() =>{
                  isNotImmediateDo()
                  goods.get('saleType') == 0
                    ? changeWholesaleVisible()
                    : changeRetailSaleVisible(true)
                }
              }/>
              <img src={buying} alt="" style={{width:'2.22rem',height:'100%'}} onClick={() =>{
                  isImmediateDo()
                  goods.get('saleType') == 0
                    ? changeWholesaleVisible()
                    : changeRetailSaleVisible(true)
                }
              }/>
            </div>
          </div>
        </div>
      </div>
    );
  }

  _goStore = () => {
    //店铺范畴，去小B的店铺精选页
    let isDistributor = WMkit.isDistributor();
    let id;
    //如果是分销员，取customerId
    if (isDistributor) {
      id = JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId;
    } else {
      //取inviteId
      id = WMkit.inviteeId();
    }
    history.push(`/shop-index-c/${id}`);
  };
}
