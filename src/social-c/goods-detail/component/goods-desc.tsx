import React, { Component } from 'react';
import { Relax } from 'plume2';

import { Blank } from 'wmkit';
import { IMap, IList } from 'typings/globalType';

const windowHeight = window.innerHeight.toString() + 'px';
const noneImg = require('../images/detailnone.png');

@Relax
export default class GoodsDetailDesc extends Component<any, any> {
  // props: {
  //   relaxProps?: {
  //     descData: string;
  //     gotDescData: boolean;
  //     goodsBrand: IMap;
  //     goodsProps: IList;
  //     goodsPropDetailRels: IList;
  //     storeGoodsTabContent:IList;
  //   };
  // };

  static relaxProps = {
    descData: 'descData',
    gotDescData: 'gotDescData',
    goodsBrand: 'goodsBrand',
    goodsProps: 'goodsProps',
    goodsPropDetailRels: 'goodsPropDetailRels',
    storeGoodsTabs: 'storeGoodsTabs',
    tabKey: 'tabKey',
    changeTabKey: () => {},
    storeGoodsTabContent: 'storeGoodsTabContent'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      descData,
      goodsBrand,
      goodsProps,
      goodsPropDetailRels,
      storeGoodsTabs,
      tabKey,
      storeGoodsTabContent
    } = this.props.relaxProps;
    return (
      <div id="goods-detail-container">
        <div>
          <div style={{ overflow: 'hidden' }}>
            <div className="fill-detail">
              <div className="pull-text">
                <div className="line-border" />
                <div className="over-text">
                  <span>图文详情</span>
                </div>
              </div>
            </div>
            <div className="img-detail-tab b-1px-t" style={{ display: 'none' }}>
              <div
                className={`layout-item ${tabKey == -1 ? 'cur' : ''}`}
                onClick={() => this._changeTabKey(-1)}
              >
                商品详情
              </div>
              {storeGoodsTabs &&
                storeGoodsTabs.size > 0 &&
                storeGoodsTabs.map((tab, index) => {
                  return (
                    <div
                      className={`layout-item ${
                        tab.get('tabId') == tabKey ? 'cur' : ''
                      }`}
                      onClick={() => this._changeTabKey(tab.get('tabId'))}
                    >
                      {tab.get('tabName')}
                    </div>
                  );
                })}
            </div>
            <div
              className="parameter"
              style={{
                display:
                  tabKey == -1 &&
                  ((goodsBrand && goodsBrand.get('brandName')) ||
                    (goodsProps && goodsProps.size > 0))
                    ? 'block'
                    : 'none'
              }}
            >
              <div className="b-1px-b" style={{ paddingBottom: 8 }}>
                {/* <h4>产品参数</h4> */}
                <ul>
                  {goodsBrand &&
                    goodsBrand.get('brandName') && (
                      <li>
                        <div className="left">品牌</div>
                        <div className="right">
                          {goodsBrand.get('brandName')}
                          {goodsBrand.get('nickName') &&
                            `（${goodsBrand.get('nickName')}）`}
                        </div>
                      </li>
                    )}

                  {goodsProps &&
                    goodsProps.size > 0 &&
                    goodsProps
                      .toJS()
                      .map((prop) =>
                        this._renderPropDetail(prop, goodsPropDetailRels)
                      )
                      .filter((li) => li != null)}
                </ul>
              </div>
            </div>
            {descData !== '' ? (
              <div
                className="web-intro"
                style={{ display: tabKey == -1 ? 'block' : 'none' }}
                dangerouslySetInnerHTML={{ __html: descData }}
              />
            ) : (
              <Blank img={noneImg} content="商家暂未添加商品详情哦" />
            )}
            {storeGoodsTabContent.size > 0 &&
              tabKey != -1 && (
                <div className="web-intro">
                  <div
                    dangerouslySetInnerHTML={{
                      __html: storeGoodsTabContent
                        .filter((v) => v.get('tabId') == tabKey)
                        .get(0)
                        .get('tabDetail')
                    }}
                  />
                </div>
              )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 渲染具体属性值
   * @param prop
   * @param goodsPropDetailRels
   * @returns {string}
   */
  _renderPropDetail = (prop, goodsPropDetailRels: IList) => {
    let detailNm = null;
    if (goodsPropDetailRels && goodsPropDetailRels.size > 0) {
      const goodsDetailRel = goodsPropDetailRels.find(
        (rel) => rel.get('propId') == prop.propId
      );
      if (goodsDetailRel) {
        const goodsDetail = (prop.goodsPropDetails as Array<any>).find(
          (detail) => goodsDetailRel.get('detailId') == detail.detailId
        );
        if (goodsDetail) {
          detailNm = goodsDetail.detailName;
        }
      }
    }
    return detailNm ? (
      <li>
        <div className="left">{prop.propName}</div>
        <div className="right">{detailNm}</div>
      </li>
    ) : null;
  };

  //图文详情tab切换
  _changeTabKey = (index) => {
    const { changeTabKey } = this.props.relaxProps;
    changeTabKey(index);
  };
}
