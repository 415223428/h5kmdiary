import React from 'react';
import { StoreProvider } from 'plume2';
import { WMWholesaleChoose, WMRetailChoose } from 'wmkit';
import { ShareModal, GraphicShare } from 'biz';

import AppStore from './store';
import GoodsList from './component/goods-list';
import GoodsTab from './component/goods-tab';
import GoodsTabShade from './component/goods-tab-shade';
import GoodsSearchBar from './component/goods-search-bar';
import Search from '../../search';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsListIndex extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '商品列表';
    // 目录编号
    let cateId = '';
    // 目录名称
    let cateName = '';
    // 搜索关键字
    let queryString = '';

    // 商品列表带搜索条件有两个来源：1.全部分类页面 a 2.从搜索页面进行搜索 b
    // a场景选中的分类信息作为参数拼接在url后面，通过截取url获取
    // b场景关键字临时存储在SessionStorage中，从SessionStorage读取
    // // 解析url中的参数
    let search = this.props.location.search;
    search = search.substring(1, search.length);

    let searchArray = search.split('&');
    searchArray.forEach((kv) => {
      const split = kv.split('=');
      if (split[0] === 'cid') {
        cateId = split[1];
      } else if (split[0] === 'cname') {
        cateName = decodeURIComponent(split[1]) || '';
      } else if (split[0] === 'q') {
        queryString = decodeURIComponent(split[1]) || '';
      }
    });
    this.store.init(cateId, cateName, queryString);
    console.log('%cqweeeeeee','color:red');
    
  }

  render() {
    return (
      <div>
        {/*顶部搜索框*/}
        <GoodsSearchBar />

        {/*顶部的tab*/}
        <GoodsTab />

        {/*tab遮罩*/}
        <GoodsTabShade />

        {/*商品列表*/}
        <GoodsList />

        {/*批发销售类型-规格选择弹框*/}
        <WMWholesaleChoose
          data={this.store.state().get('chosenSpu')}
          visible={this.store.state().get('wholesaleVisible')}
          changeSpecVisible={this.store.changeWholesaleVisible}
        />

        {/*零售销售类型-规格选择弹框*/}
        <WMRetailChoose
          data={this.store.state().get('chosenSpu')}
          visible={this.store.state().get('retailVisible')}
          changeSpecVisible={this.store.changeRetailVisible}
        />

        {/*分享赚弹框*/}
        <ShareModal
          shareType={2}
          visible={this.store.state().get('shareVisible')}
          changeVisible={this.store.changeShareVisible}
        />

        {false && <Search />}
      </div>
    );
  }
}
