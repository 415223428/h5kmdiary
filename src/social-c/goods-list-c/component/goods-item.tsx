import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMImage, _ } from 'wmkit';
import { GoodsNum, MarketingLabel } from 'biz';

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const {
      goodsItem,
      listView,
      changeShareVisible,
      changeRetailVisible
    } = this.state;
    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 起订量
    const count = goodsInfo.get('count') || 0;
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;
    // 营销标签
    const marketingLabels = goodsInfo.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsInfo.get('couponLabels');

    let containerClass = listView
      ? invalid
        ? 'goods-list invalid-goods'
        : 'goods-list'
      : invalid
        ? 'goods-box invalid-goods'
        : 'goods-box';

    // 社交电商相关内容显示与否
    const social = true;
    // 社交电商-c相关内容显示与否
    const socialC = true;
    //禁用分享赚
    const socialDisabled = false;

    return (
      <div
        key={id}
        className={containerClass}
        onClick={() => history.push('/goods-detail/' + goodsItem.get('id'))}
      >
        <div className="img-box">
          <WMImage
            src={goodsInfo.get('goodsInfoImg')}
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail b-1px-b">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>

          {social ? (
            listView && <div className="marketing" />
          ) : (
            <div className="marketing">
              {goodsInfo.get('companyType') == 0 && (
                <div className="self-sales">自营</div>
              )}
              {(marketingLabels || couponLabels) && (
                <MarketingLabel
                  marketingLabels={marketingLabels}
                  couponLabels={couponLabels}
                />
              )}
            </div>
          )}

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {this._calShowPrice(goodsItem, buyCount)}
              </span>
              {social &&
                !socialC && (
                  <span
                    className={
                      invalid ? 'commission commission-disabled' : 'commission'
                    }
                  >
                    /&nbsp;赚12
                  </span>
                )}
              {socialC && <span className="disable-price">￥12.00</span>}
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            {social ? (
              socialC ? (
                !invalid && (
                  <div className="social-btn-box">
                    <div
                      className={
                        socialDisabled
                          ? 'social-btn social-btn-disabled'
                          : 'social-btn'
                      }
                      onClick={(e) => {
                        changeRetailVisible();
                        e.stopPropagation();
                      }}
                    >
                      抢购
                    </div>
                  </div>
                )
              ) : (
                !invalid && (
                  <div className="social-btn-box">
                    <div
                      className="social-btn social-btn-ghost"
                      onClick={(e) => {
                        history.push('/graphic-material');
                        e.stopPropagation();
                      }}
                    >
                      发圈素材
                    </div>
                    <div
                      className={
                        socialDisabled
                          ? 'social-btn social-btn-disabled'
                          : 'social-btn'
                      }
                      onClick={(e) => {
                        !socialDisabled && changeShareVisible();
                        e.stopPropagation();
                      }}
                    >
                      分享赚
                    </div>
                  </div>
                )
              )
            ) : (
              <GoodsNum
                value={buyCount}
                max={stock}
                disableNumberInput={invalid}
                goodsInfoId={id}
                onAfterClick={this._afterChangeNum}
              />
            )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
  };
}
