import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';

import GoodsTabFilter from './goods-tab-filter';

/**
 * 商品列表遮罩
 */
@Relax
export default class GoodsTabShade extends Component<any, any> {
  props: {
    relaxProps?: {
      showShade: boolean;
    };
  };

  static relaxProps = {
    showShade: 'showShade'
  };

  render() {
    const { showShade } = this.props.relaxProps;

    return (
      <div style={showShade ? {} : { display: 'none' }}>
        <div
          style={{
            position: 'fixed',
            left: 0,
            top: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,.5)',
            zIndex: 9999
          }}
        >
          {/*品牌规格筛选项*/}
          <GoodsTabFilter />
        </div>
      </div>
    );
  }
}
