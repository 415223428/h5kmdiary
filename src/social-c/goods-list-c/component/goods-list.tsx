import React, { Component } from 'react';
import { fromJS } from 'immutable';
import { IMap, Relax } from 'plume2';

import { Blank, ListView, noop, WMkit, Loading } from 'wmkit';
import { cache } from 'config';
import GoodsItem from './goods-item';
import SpuItem from './spu-item';
import { IList } from '../../../../typings/globalType';

const noneImg = require('../img/list-none.png');

@Relax
export default class GoodsList extends Component<any, any> {
  _listView: any;

  props: {
    relaxProps?: {
      searchState: IMap;
      listView: boolean; // 小图,大图
      goodsShowType: number; // sku列表,spu列表
      loading: boolean;
      changeShareVisible: Function;
      // 选中的类目
      selectedCate: IMap;
      sortType: IMap;
      selectedBrandIds: any;
      queryString: string;
      initialEnd: boolean;
      handleDataReached: (data: Object) => void;
      querySpuAndOpenSpec: (skuId) => void;
      isNotEmpty: boolean;
      selectSelfCompany: boolean;
      goodsProps: IList;
      changeRetailVisible: Function;
    };
  };

  static relaxProps = {
    searchState: 'searchState',
    listView: 'listView',
    goodsShowType: 'goodsShowType',
    loading: 'loading',
    selectedCate: 'selectedCate',
    sortType: 'sortType',
    selectedBrandIds: 'selectedBrandIds',
    queryString: 'queryString',
    initialEnd: 'initialEnd',
    handleDataReached: noop,
    querySpuAndOpenSpec: noop,
    changeShareVisible: noop,
    isNotEmpty: 'isNotEmpty',
    selectSelfCompany: 'selectSelfCompany',
    goodsProps: 'goodsProps',
    changeRetailVisible: noop
  };
  constructor(props) {
    super(props);
    this.state = {
      // 防止修改商品数量,引起参数变化,重新查询商品列表(刷新页面,体验不好)
      dtoListParam: JSON.parse(localStorage.getItem(WMkit.purchaseCache())) || []
    };
  }
  render() {
    let {
      listView,
      goodsShowType,
      selectedCate,
      sortType,
      queryString,
      initialEnd,
      selectedBrandIds,
      handleDataReached,
      querySpuAndOpenSpec,
      isNotEmpty,
      selectSelfCompany,
      goodsProps,
      changeShareVisible,
      changeRetailVisible
    } = this.props.relaxProps;
    // 组件刚执行完mount，搜索条件没有注入进来，先不加载ListView，避免先进行一次无条件搜索，再立刻进行一次有条件搜索
    if (!initialEnd) {
      return <Loading />;
    }

    let layoutClassName = listView && isNotEmpty ? null : 'view-box';

    // 搜索条件
    // 关键字
    const keyword = queryString;

    // 目录编号
    const cateId =
      selectedCate.get('cateId') == 0 ? null : selectedCate.get('cateId');

    // 只展示自营商品
    const companyType = selectSelfCompany ? '0' : '';

    // 排序标识，默认为按时间倒序
    let sortFlag = 0;
    if (sortType.get('type') == 'dateTime' && sortType.get('sort') == 'desc') {
      sortFlag = 0;
    } else if (
      sortType.get('type') == 'dateTime' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 1;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'desc'
    ) {
      sortFlag = 2;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 3;
    }

    // 如果使用关键字搜索，默认排序要传null，为了让更匹配关键字的排在前面(ES默认排序)
    if (
      keyword &&
      (sortType.get('type') === '' || sortType.get('type') === 'default')
    ) {
      sortFlag = null;
    }

    // 品牌
    const brandIds = (selectedBrandIds && selectedBrandIds.toJS()) || null;

    // 属性值
    let propDetails = [];
    if (goodsProps && goodsProps.count() > 0) {
      propDetails = goodsProps
        .map((v) => {
          let prop = fromJS({});
          const detailIds = v
            .get('goodsPropDetails')
            .filter((de) => de.get('checked') === true)
            .map((de) => de.get('detailId'));
          return prop
            .set('propId', v.get('propId'))
            .set('detailIds', detailIds);
        })
        .filter((v) => v.get('detailIds') && v.get('detailIds').size > 0)
        .toJS();
    }

    let dataUrl;
    if (WMkit.isLoginOrNotOpen()) {
      if (goodsShowType == 0) {
        dataUrl = '/goods/skus'; //登陆后的sku列表
      } else {
        dataUrl = '/goods/spus'; //登陆后的spu列表
      }
    } else {
      if (goodsShowType == 0) {
        dataUrl = '/goods/skuListFront'; //开放访问的sku列表
      } else {
        dataUrl = '/goods/spuListFront'; //开放访问的spu列表
      }
    }
    return (
      <ListView
        className={layoutClassName}
        url={dataUrl}
        style={{ height: 'calc(100vh - 88px)' }}
        params={{
          keywords: keyword,
          cateId: cateId,
          companyType: companyType,
          brandIds: brandIds,
          propDetails: propDetails,
          sortFlag: sortFlag,
          esGoodsInfoDTOList: this.state.dtoListParam
        }}
        dataPropsName={
          goodsShowType == 0
            ? 'context.esGoodsInfoPage.content'
            : 'context.esGoods.content'
        }
        otherProps={['goodsIntervalPrices']}
        renderRow={(item: any) => {
          if (goodsShowType == 0) {
            return (
              <GoodsItem
                changeShareVisible={() => changeShareVisible()}
                changeRetailVisible={() => changeRetailVisible()}
                goodsItem={fromJS(item)}
                listView={listView}
                key={item.id}
              />
            );
          } else {
            return (
              <SpuItem
                changeShareVisible={() => changeShareVisible()}
                changeRetailVisible={() => changeRetailVisible()}
                goods={fromJS(item)}
                listView={listView}
                key={item.id}
                spuAddCartFunc={querySpuAndOpenSpec}
              />
            );
          }
        }}
        renderEmpty={() => <Blank img={noneImg} content="没有搜到任何商品～" />}
        ref={(_listView) => (this._listView = _listView)}
        onDataReached={handleDataReached}
      />
    );
  }
}
