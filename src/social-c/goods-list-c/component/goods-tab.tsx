import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class GoodsTab extends Component<any, any> {
  props: {
    relaxProps?: { openShade: Function };
  };

  static relaxProps = { openShade: noop };

  render() {
    const { openShade } = this.props.relaxProps;
    return (
      <div className="shop-title-box b-1px-t b-1px-b">
        <span />
        <span
          onClick={() => {
            openShade('goodsFilter');
          }}
        >
          筛选<i className="iconfont icon-shaixuan" />
        </span>
      </div>
    );
  }
}
