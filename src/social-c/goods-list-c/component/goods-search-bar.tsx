import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { noop, history } from 'wmkit';

@Relax
export default class GoodsSearchBar extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeLayout: Function;
      listView: boolean;
      queryString: string;
    };
  };

  static relaxProps = {
    changeLayout: noop,
    listView: 'listView',
    queryString: 'queryString'
  };

  render() {
    const { changeLayout, listView, queryString } = this.props.relaxProps;

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div>
              <a
                href="javascript:void(0)"
                className="list-tab"
                style={{ borderRight: '1px solid #ff1f4e' }}
                onClick={() => {}}
              >
                <i
                  className="iconfont icon-jiantou"
                  style={{
                    fontSize: '.38rem',
                    marginRight: '.3rem'
                  }}
                />
              </a>
            </div>
            <div
              className="input-box"
              style={{ backgroundColor: 'transparent' }}
              onClick={() =>
                history.push({ pathname: '/search', state: { queryString } })
              }
            >
              <input
                type="text"
                value={queryString}
                placeholder="输入搜索商品名称"
              />
              <i
                className="iconfont icon-cuo"
                style={{ fontSize: '.32rem', color: '#000', opacity: 0.3 }}
              />
            </div>

            <div>
              <a
                href="javascript:void(0)"
                className="list-tab"
                onClick={() => changeLayout()}
              >
                <i
                  className={`iconfont ${
                    listView ? 'icon-datu' : 'icon-fenlei'
                  }`}
                />
                <span>{listView ? '大图' : '列表'}</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
