import {fromJS} from "immutable";
import {Store} from "plume2";
import CateActor from "./actor/cate-actor";
import * as Api from "./webapi";


export default class AppStore extends Store {
  bindActor() {
    return [new CateActor]
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  /**
   * 初始化分类数据
   * @returns {Promise<void>}
   */
  init = async (cateId) => {
    Api.getAllCates().then((res) => {
      const context = fromJS(res).get('context')
      let cateList = fromJS(JSON.parse(context) || [])

      // 过滤掉默认分类
      cateList = cateList.filter(cate => cate.get('isDefault') == "NO")

      this.dispatch('cateActor:init', cateList)

      //如果传入catId则计算index
      if(cateId==undefined) return;
      let cateFilter = cate => {
        if(cate.get('cateId') == cateId) return true
        if(
            cate.get('goodsCateList').size!=0
            &&
            cate.get('goodsCateList').filter(cateFilter).size!=0)
          return true
        return false
      }
      for(let i = 0; i < cateList.size; i++) {
        if(cateFilter(cateList.get(i))) {
          this.dispatch('cateActor:index', i)
          break
        }
      }
    })
  }


  /**
   * 更新选中的一级分类
   * @param index
   */
  changeIndex = (index) => {
    this.dispatch('cateActor:index', index)
  }

}