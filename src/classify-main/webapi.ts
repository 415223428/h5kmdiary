import {Fetch} from 'wmkit'

/**
 * 获取全部分类信息
 * @type {Promise<AsyncResult<T>>}
 */
export const getAllCates = () => {
  return Fetch('/goods/allGoodsCates')
}
