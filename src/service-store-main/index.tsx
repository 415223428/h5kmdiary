import React from 'react';
import { StoreProvider, msg } from 'plume2';

import { Loading, history, WMkit } from 'wmkit';
import { ShareModal, BStoreClose, RegisterCoupon } from 'biz';

import AppStore from './store';
import SearchBar from './component/search-bar';
import Head from './component/head';
import GoodsList from './component/goods-list';
import Footer from './component/footer';
import * as webapi from './webapi';

import Search from '../shop-search';
import MagicBox from '@wanmi/magic-box';
import * as magicBoxProps from '../magicBoxProps';
const styles = require('./css/style.css');
@StoreProvider(AppStore, { debug: __DEV__ })
export default class StoreMain extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { sid } = this.props.match.params;
    this.store.init(sid);
    msg.on('purchaseNum', this._fetchPurChaseNum);
    this._fetchPurChaseNum();
    localStorage.setItem('X-STIE-STOREID', sid);
  }

  componentWillUnmount() {
    msg.off('purchaseNum', this._fetchPurChaseNum);
    msg.emit('registerCouponVisible', { visible: false });
    localStorage.removeItem('X-STIE-STOREID');
  }

  state: {
    //sku种类数量
    purchaseNum: 0;
  };

  render() {
    const { path = '', params } = this.props.match;
    const { sid } = params;
    let props = { ...magicBoxProps.props };
    props.storeId = sid;
    const xSiteVisiable = this.store.state().get('xSiteVisiable');
    if (xSiteVisiable == null) {
      return null;
    }
    return (
      <div>
        <div style={{ height: 144 }}>
          <Loading loading={this.store.state().get('loading')} />
          <SearchBar storeId={sid} />
          <Head />
        </div>
        {/* <Head />
        <GoodsList /> */}
        {xSiteVisiable ? <MagicBox style={{ height: 'calc(100vh - 144px - 47px)' }} {...props} /> : <GoodsList />}

        <Footer storeId={sid} path={path} />

        {/* 购物车 */}
        <div
          className="bottom-cart show"
          onClick={() => history.push('/purchase-order')}
        >
          <i className="iconfont icon-gouwuche" />
          {this.state.purchaseNum > 0 && (
            <div className="tag">{this.state.purchaseNum}</div>
          )}
        </div>

        {/*分享赚弹框*/}
        <ShareModal
          shareType={2}
          goodsInfo={this.store.state().get('checkedSku')}
          visible={this.store.state().get('shareVisible')}
          changeVisible={this.store.changeShareVisible}
        />

        {false && <Search />}

        {/*分销员禁用状态提示*/}
        <BStoreClose />
        <RegisterCoupon />
      </div>
    );
  }

  /**
   * 查询购物车数量
   */
  _fetchPurChaseNum = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      const { context, message, code } = await webapi.fetchPurchaseCount();
      this.setState({ purchaseNum: context });
    } else {
      const purCache = JSON.parse(localStorage.getItem(WMkit.purchaseCache()));
      const count = purCache ? purCache.length : 0;
      //未登录或者是开放，获取缓存中的购物车数量
      this.setState({ purchaseNum: count });
    }
  };
}
