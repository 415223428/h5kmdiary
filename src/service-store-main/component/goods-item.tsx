import React from 'react';
import { IMap, msg } from 'plume2';

import { GoodsNum, MarketingLabel } from 'biz';
import { _, Alert, history, WMImage, WMkit } from 'wmkit';

export default class GoodsItem extends React.Component<any, any> {
  props: {
    goodsItem: IMap;
    changeShareVisible: Function;
    isDistributor: boolean;
    saveCheckedSku: Function;
    isShow: boolean;
  };

  render() {
    const { goodsItem, changeShareVisible, isDistributor, isShow } = this.props;
    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 起订量
    const count = goodsInfo.get('count') || 0;
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;
    // 会员价
    const salePrice = goodsInfo.get('salePrice') || 0;
    // 最低的区间价
    const intervalMinPrice = goodsInfo.get('intervalMinPrice') || 0;
    let containerClass = invalid ? 'goods-list invalid-goods' : 'goods-list';
    // 营销标签
    const marketingLabels = goodsInfo.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsInfo.get('couponLabels');

    // 社交电商相关内容显示与否
    const social = goodsInfo.get('distributionGoodsAudit') == 2 ? true : false;
    const distributionCommission = goodsInfo.get('distributionCommission');
    const marketPrice = goodsInfo.get('marketPrice');
    //禁用分享赚
    const socialDisabled = false;

    //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
    //好评率
    let favorableRate = '100';
    if (
      goodsInfo.get('goodsEvaluateNum') &&
      goodsInfo.get('goodsEvaluateNum') != 0
    ) {
      favorableRate = _.mul(
        _.div(
          goodsInfo.get('goodsFavorableCommentNum'),
          goodsInfo.get('goodsEvaluateNum')
        ),
        100
      ).toFixed(0);
    }

    //评论数
    let evaluateNum = '暂无';
    const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
    if (goodsEvaluateNum) {
      if (goodsEvaluateNum < 10000) {
        evaluateNum = goodsEvaluateNum;
      } else {
        const i = _.div(goodsEvaluateNum, 10000).toFixed(1);
        evaluateNum = i + '万+';
      }
    }

    //销量
    let salesNum = '暂无';
    const goodsSalesNum = goodsInfo.get('goodsSalesNum');
    if (goodsSalesNum) {
      if (goodsSalesNum < 10000) {
        salesNum = goodsSalesNum;
      } else {
        const i = _.div(goodsSalesNum, 10000).toFixed(1);
        salesNum = i + '万+';
      }
    }
    // 预估点数
    let pointNum = goodsInfo.get('kmPointsValue');
    //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆
    var data = {
      // 要发送到七鱼的商品或者订单的数据对象
      picture: goodsInfo.get('goodsInfoImg'),
      title: goodsInfo.get('goodsInfoName'),
      price: social
        ? _.addZero(marketPrice)
        : goodsInfo.get('priceType') == 1
          ? _.addZero(intervalMinPrice)
          : _.addZero(salePrice),
      url: `https://m.kmdiary.com/goods-detail/ + ${id}`,
      showCustomMsg: 1,
    }
    return (
      <div
        key={id}
        className={containerClass}
        // onClick={() => history.push('/goods-detail/' + id)}
        onClick={() => window.parent.postMessage(data, '*')}
        style={{ borderRadius: "10px 10px 0 0" }}
      >
        <div className="img-box">
          <WMImage
            mode="pad"
            src={goodsInfo.get('goodsInfoImg')}
            alt=""
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>

          {/* 评价 */}
          {isShow ? (
            <div className="goods-evaluate">
              <span className="goods-evaluate-spn">{salesNum}销量</span>
              <span className="goods-evaluate-spn mar-lr-28">
                {evaluateNum}评价
              </span>
              <span className="goods-evaluate-spn">{favorableRate}%好评</span>
              {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
            </div>
          ) : (
              <div className="goods-evaluate">
                <span className="goods-evaluate-spn">{salesNum}销量</span>
              </div>
            )}

          {social ? (
            <div className="marketing" />
          ) : (
              <div className="marketing">
                {!social &&
                  (marketingLabels || couponLabels) && (
                    <MarketingLabel
                      marketingLabels={marketingLabels}
                      couponLabels={couponLabels}
                    />
                  )}
              </div>
            )}
          {/* {social && !WMkit.inviteeId() && isDistributor } */}
          <div className="bottom">
            <div className="bottom-price">
              <span className="price" >
                <i className="iconfont icon-qian" />
                {social
                  ? _.addZero(marketPrice)
                  : goodsInfo.get('priceType') == 1
                    ? _.addZero(intervalMinPrice)
                    : _.addZero(salePrice)}
              </span>
              {social &&
                !WMkit.inviteeId() &&
                isDistributor && (
                  <span
                    className={
                      invalid ? 'commission commission-disabled' : 'commission'
                    }
                  >
                    /&nbsp;赚{_.addZero(distributionCommission)}
                  </span>
                )}
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            {social && !WMkit.inviteeId() && isDistributor ? (
              !invalid && (
                <div className="social-btn-box">
                  <div
                    className="social-btn social-btn-ghost"
                    onClick={async (e) => {
                      e.stopPropagation();
                      const result = await WMkit.getDistributorStatus();
                      if (result) {
                        history.push(
                          '/graphic-material/' + goodsInfo.get('id')
                        );
                      } else {
                        msg.emit('bStoreCloseVisible', true);
                      }
                    }}
                  >
                    发圈素材
                  </div>
                  <div
                    className={
                      socialDisabled
                        ? 'social-btn social-btn-disabled'
                        : 'social-btn'
                    }
                    onClick={async (e) => {
                      e.stopPropagation();
                      const result = await WMkit.getDistributorStatus();
                      if (result) {
                        if ((window as any).isMiniProgram) {
                          this.props.saveCheckedSku(goodsInfo);
                          !socialDisabled && changeShareVisible();
                        } else {
                          Alert({
                            text: '请到小程序端进行该操作!'
                          });
                        }
                      } else {
                        msg.emit('bStoreCloseVisible', true);
                      }
                    }}
                  >
                    分享赚
                  </div>
                </div>
              )
            ) : (
                <GoodsNum
                  value={buyCount}
                  max={stock}
                  disableNumberInput={invalid}
                  goodsInfoId={id}
                />
              )}
          </div>
        </div>
      </div>
    );
  }
}
