import React from 'react';
import { IMap, msg, Relax } from 'plume2';
import { noop, WMkit } from 'wmkit';

const defaultImg = require('../img/defalutShop.png');
@Relax
export default class Head extends React.Component<any, any> {
  props: {
    relaxProps?: {
      store: IMap;
      follow: Function;
      unfollow: Function;
      init: Function;
      isShow: boolean;
    };
  };

  static relaxProps = {
    // 店铺信息
    store: 'store',

    // 关注店铺
    follow: noop,
    // 取消关注店铺
    unfollow: noop,
    init: noop,
    isShow: 'isShow'
  };

  render() {
    const { store, isShow } = this.props.relaxProps;
    let compositeScore = 5;
    if (store.get('storeEvaluateSumVO')) {
      const sumCompositeScore = store.getIn([
        'storeEvaluateSumVO',
        'sumCompositeScore'
      ]);
      compositeScore = sumCompositeScore == 0 ? 5 : sumCompositeScore;
    }
    return (
      <div>
        <div className="store-logo b-1px">
          {store.get('storeLogo') ? (
            <img src={store.get('storeLogo')} />
          ) : (
            <img src={defaultImg} />
          )}
        </div>

        <div className="store-head-box b-1px-b">
          <div>
            <span className="left-span">{store.get('storeName')}</span>
            {store.get('companyType') == 0 && (
              // <div
              //   className="self-sales"
              //   style={{ marginLeft: 10, marginRight: 0 }}
              // >
              //   自营
              // </div>
              <img src={require('../img/ziying.png')} alt="" className="ziyingimg"></img>
            )}
            {isShow ? (
              <span className="right-span">
                综合评分 {compositeScore.toFixed(2)}
              </span>
            ) : null}
          </div>
          {/*<a*/}
          {/*  className="store-like "*/}
          {/*  onClick={this._clickAttention}*/}
          {/*  href="javascript:;"*/}
          {/*>*/}
          {/*  <i*/}
          {/*    className={`iconfont ${*/}
          {/*      store.get('isFollowed') ? 'icon-guanzhu1' : 'icon-guanzhu'*/}
          {/*    }`}*/}
          {/*  />*/}
          {/*  <span>{store.get('isFollowed') ? '已关注' : '关注'}</span>*/}
          {/*</a>*/}
        </div>
      </div>
    );
  }

  /**
   * 关注/取消
   */
  _clickAttention = () => {
    const { follow, unfollow, store, init } = this.props.relaxProps;
    if (WMkit.isLoginOrNotOpen()) {
      if (store.get('isFollowed')) {
        unfollow(store.get('storeId'));
      } else {
        follow(store.get('storeId'));
      }
    } else {
      //显示登录弹框
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          init(store.get('storeId'));
        }
      });
    }
  };
}
