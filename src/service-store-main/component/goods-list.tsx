import React from 'react';
import { Relax } from 'plume2';

import { IList } from 'typings/globalType';
import { Blank, noop } from 'wmkit';
import { fromJS } from 'immutable';

import GoodsItem from './goods-item';

const noneImg = require('../img/none.png');
@Relax
export default class GoodsList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      skus: IList;
      changeShareVisible: Function;
      isDistributor: boolean;
      saveCheckedSku: Function;
      isShow: boolean;
    };
  };

  static relaxProps = {
    skus: 'skus',
    changeShareVisible: noop,
    isDistributor: 'isDistributor',
    saveCheckedSku: noop,
    isShow: 'isShow'
  };

  render() {
    const {
      skus,
      changeShareVisible,
      isDistributor,
      saveCheckedSku,
      isShow
    } = this.props.relaxProps;
    return skus.count() > 0 ? (
      <div className="store-content">
        <div className="store-new-title">店铺上新</div>
        <div className="address-box" style={{margin:"0 0.2rem"}}>
        {skus
          .toJS()
          .map((sku) => (
            <GoodsItem
              saveCheckedSku={(sku) => saveCheckedSku(sku)}
              goodsItem={fromJS(sku)}
              changeShareVisible={() => changeShareVisible()}
              isDistributor={isDistributor}
              isShow={isShow}
            />
          ))}
        <div className="no-more-tips">没有更多了</div>
        </div>
      </div>
    ) : (
      <Blank img={noneImg} content="没有搜到任何商品～" />
    );
  }
}
