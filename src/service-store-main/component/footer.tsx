import React from 'react';
import { msg, Relax } from 'plume2';
import { history, WMkit } from 'wmkit';
import { url } from 'config';

@Relax
export default class Footer extends React.Component<any, any> {
  props: {
    storeId: number;
    path: string;
    relaxProps?: {
      onlineServiceFlag: boolean;
    };
  };

  static relaxProps = {
    onlineServiceFlag: 'onlineServiceFlag'
  };

  render() {
    const { storeId, path } = this.props;
    const { onlineServiceFlag } = this.props.relaxProps;
    return (
      <div className="store-foot">
        <div className="store-foot-content b-1px-t">
          <a
            href="javascript:;"
            className={`b-1px-r ${
              path.startsWith(url.STORE_MAIN) ? 'cur' : ''
            }`}
            onClick={() => history.push(`/store-main/${storeId}`)}
          >
            <i className="iconfont icon-dianpushouye" />
            <span>店铺首页</span>
          </a>
          <a
            href="javascript:;"
            className={`b-1px-r ${
              path.startsWith(url.STORE_GOODS_CATES) ? 'cur' : ''
            }`}
            onClick={() => history.push(`/store-goods-cates/${storeId}`)}
          >
            <i className="iconfont icon-dianpufenlei" />
            <span>商品分类</span>
          </a>
          <a
            href="javascript:;"
            className={`b-1px-r ${
              path.startsWith(url.STORE_PROFILE) ? 'cur' : ''
            }`}
            onClick={() => history.push(`/store-profile/${storeId}`)}
          >
            <i className="iconfont icon-dianpudangan" />
            <span>店铺档案</span>
          </a>
          {onlineServiceFlag &&
            !(window as any).isMiniProgram && (
              <a
                // href="javascript:;"
                className={`${path.startsWith(url.MEMBER_SHOP) ? 'cur' : ''}`}
                onClick={() => {
                  // if (WMkit.isLoginOrNotOpen()) {
                  //   // history.push(`/chose-service/${storeId}`);
                  //   window.location.href = (window as any).ysf('url');
                  //   (window as any).openqiyu();
                  // } else {
                  //   msg.emit('loginModal:toggleVisible', {
                  //     callBack: () => {
                        // history.push(`/chose-service/${storeId}`);
                        WMkit.getQiyuCustomer();
                        window.location.href = (window as any).ysf('url');
                        (window as any).openqiyu();
                  //     }
                  //   });
                  // }
                }}
              >
                {/* <i className="iconfont icon-kefu2" /> */}
                <img src={require('../img/service.png')} alt=""/>
                <span>店铺客服</span>
              </a>
            )}
        </div>
      </div>
    );
  }
}
