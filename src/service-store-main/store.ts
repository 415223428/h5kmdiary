import { msg, Store } from 'plume2';

import { fromJS } from 'immutable';

import StoreMainActor from './actor/store-main-actor';
import GoodsActor from './actor/goods-actor';
import * as webApi from './webapi';
import { Alert, WMkit, _ } from 'wmkit';
import { evaluateWebapi } from 'biz';

export default class AppStore extends Store {
  bindActor() {
    return [new StoreMainActor(), new GoodsActor()];
  }

  payAttention = () => {
    this.dispatch('store:liked');
  };

  /**
   * 初始化
   */
  init = async (id) => {
    const res = await webApi.fetchIsVisiable(id);
    const { configOrder } = (res as any).data;
    let viewData = Array.isArray(configOrder)
      ? configOrder
      : configOrder.children;
    viewData = viewData ? viewData : [];
    this.dispatch('goods:xSiteVisiable', viewData.length == 0 ? false : true);
    const [skuRes, storeRes, onlineRes, evluateRes] = (await Promise.all([
      webApi.fetchSkusForMain(id),
      webApi.fetchStoreInfo(id),
      webApi.fetchOnlineServiceList(id),
      evaluateWebapi.isShow()
    ])) as any;
    if (WMkit.isLogin()) {
      const couponRes = await webApi.fetchStoreCoupon(id, WMkit.getUserId());
      if (couponRes.code == 'K-000000') {
        _.showRegisterModel(couponRes.context);
      }
    }
    this.transaction(() => {
      if (onlineRes.code == 'K-000000') {
        this.dispatch('store:setOnlineServiceFlag', onlineRes.context != null);
      }
      if (skuRes.code == 'K-000000') {
        this.dispatch(
          'goods: init',
          fromJS(skuRes.context.esGoodsInfoPage.content)
        );
      }
      if (storeRes.code == 'K-000000') {
        /**店铺pv/uv埋点*/
        (window as any).myPvUvStatis(null, storeRes.context.companyInfoId);
        this.dispatch('store: info: init', fromJS(storeRes.context));
      } else {
        msg.emit('storeCloseVisible', true);
      }
      this.dispatch('goodsActor:isShow', evluateRes);
      this.dispatch('store: loading', false);
      this.dispatch('goods:isDistributor', WMkit.isShowDistributionButton());
    });
  };

  /**
   * 切换分享赚弹框显示与否
   */
  changeShareVisible = () => {
    this.dispatch('goodsActor:changeShareVisible', true);
  };

  /**
   * 关注
   */
  follow = async (id) => {
    const res = (await webApi.follow(id)) as any;
    if (res.code == 'K-000000') {
      this.init(id);
    } else {
      Alert({ text: res.message });
    }
  };

  /**
   * 取消关注
   */
  unfollow = async (id) => {
    const res = (await webApi.unfollow(id)) as any;
    this.init(id);
  };

  saveCheckedSku = (sku) => {
    this.dispatch('goodsActor:saveCheckedSku', sku);
  };
}
