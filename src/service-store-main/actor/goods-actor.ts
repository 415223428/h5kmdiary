import { Action, Actor, IMap } from 'plume2';
import { IList } from 'typings/globalType';
import { fromJS } from 'immutable';

export default class GoodsActor extends Actor {
  defaultState() {
    return {
      // 首页sku集合
      skus: [],
      //是否显示分享赚、发圈素材、只看分享赚按钮
      isDistributor: false,
      //分享赚选中的sku
      checkedSku: fromJS({}),
      //是否展示评价相关信息
      isShow: false,
      xSiteVisiable: null
    };
  }

  /**
   * 首页商品初始化
   */
  @Action('goods: init')
  skuInit(state: IMap, skus: IList) {
    return state.set('skus', skus);
  }

  /**
   *  是否显示分享赚、发圈素材、只看分享赚按钮
   */
  @Action('goods:isDistributor')
  setIsDistributor(state, isDistributor: boolean) {
    return state.set('isDistributor', isDistributor);
  }

  @Action('goodsActor:saveCheckedSku')
  saveCheckedSku(state, sku) {
    return state.set('checkedSku', sku);
  }

  @Action('goodsActor:isShow')
  setIsShow(state, flag: boolean) {
    return state.set('isShow', flag);
  }

  @Action('goods:xSiteVisiable')
  setXSiteVisiable(state, xSiteVisiable: boolean) {
    return state.set('xSiteVisiable', xSiteVisiable)
  }
}
