import { Fetch, WMkit } from 'wmkit';
import { config } from 'config';
/**
 * 查询首页商品列表
 * @param id 店铺Id
 */
export const fetchSkusForMain = (id) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  // 首页展示20条数据
  const params = {
    storeId: id,
    pageSize: 20,
    // 上架时间倒序
    sortFlag: 1
  };
  return isLoginOrNotOpen
    ? Fetch('/goods/skus', {
      method: 'POST',
      body: JSON.stringify(params)
    })
    : Fetch('/goods/skuListFront', {
      method: 'POST',
      body: JSON.stringify(params)
    });
};

/**
 * 获取店铺基本信息
 * @param id 店铺Id
 */
export const fetchStoreInfo = (id) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  const params = {
    storeId: id
  };
  return isLoginOrNotOpen
    ? Fetch(`/store/storeInfo`, {
      method: 'POST',
      body: JSON.stringify(params)
    })
    : Fetch(`/store/unLogin/storeInfo`, {
      method: 'POST',
      body: JSON.stringify(params)
    });
};

/**
 * 关注
 * @param id 店铺Id
 */
export const follow = (id) => {
  const params = {
    storeId: id
  };
  return Fetch(`/store/storeFollow`, {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 取消关注
 * @param id 店铺Id
 */
export const unfollow = (id) => {
  const params = {
    storeIds: [id]
  };
  return Fetch(`/store/storeFollow`, {
    method: 'DELETE',
    body: JSON.stringify(params)
  });
};

/**
 * 查询购物车数量
 */
export const fetchPurchaseCount = () => {
  return Fetch(`/site/countGoods`);
};

/**
 * 获取客服信息
 */
export const fetchOnlineServiceList = (storeId) => {
  return Fetch(`/customerService/qq/detail/${storeId}/1`);
};

/**
 * 获取一组进店赠券优惠券
 * @param storeId
 * @param customerId
 */
export const fetchStoreCoupon = (storeId, customerId) => {
  return Fetch(`/store/storeCoupons/${storeId}/${customerId}`);
};

/**
 * 请求魔方是否发布过页面
 * @param storeId
 */
export const fetchIsVisiable = (storeId) => {
  return fetch(`${config.RENDER_HOST}/magic/d2cStore/000000/weixin/index?storeId=${storeId}`).then((res: any) => {
    return res.json();
  });
}