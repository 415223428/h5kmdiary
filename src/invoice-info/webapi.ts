/**
 * Created by feitingting on 2017/7/31.
 */
import {Fetch} from 'wmkit'
export  const  fetchInvoiceInfo =(id:string, type:string)=>{
    return Fetch(`/trade/invoice/${id}/${type}`,{
        method: 'GET',
    })
}