import React, { Component } from 'react';
import { FormItem, FindArea } from 'wmkit';
import { StoreProvider } from 'plume2';
import AppStore from './store';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class InvoiceInfo extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '发票信息';
    const { tid, type } = this.props.match.params;
    this.store.init(tid, type);
  }

  constructor(props) {
    super(props);
  }

  render() {
    const invoiceInfo = this.store.state().get('invoiceInfo');
    const type = invoiceInfo.get('type');

    const address = `${invoiceInfo.get('address')}`;
    const contacts = invoiceInfo.get('contacts');
    const phone = invoiceInfo.get('phone');
    const projectName = invoiceInfo.get('projectName');
    const title = invoiceInfo.get('title');
    const flag = invoiceInfo.get('flag');
    const identification = invoiceInfo.get('identification');
    const phoneNo = invoiceInfo.get('phoneNo');
    const companyAddress = invoiceInfo.get('companyAddress');
    const companyName = invoiceInfo.get('companyName');
    const account = invoiceInfo.get('account');
    const bank = invoiceInfo.get('bank');
    return (
      <div>
        <FormItem
          labelName="发票信息"
          placeholder={type == 1 ? '增值税专用发票' : '普通发票'}
        />
        <div className={type == 1 ? 'hide' : 'showblock'}>
          <FormItem
            labelName="发票抬头"
            placeholder={title == '' ? '个人' : title}
          />
          {flag == 1 ? (
            <FormItem
              labelName="纳税人识别号"
              placeholder={identification || '无'}
            />
          ) : (
            ''
          )}
        </div>
        <div className={type == 1 ? 'showblock' : 'hide'}>
          <FormItem labelName="单位全称" placeholder={companyName} />
          <FormItem labelName="纳税人识别号" placeholder={identification} />
          <FormItem labelName="注册电话" placeholder={phoneNo} />
          <FormItem labelName="注册地址" placeholder={companyAddress} />
          <FormItem labelName="银行基本户号" placeholder={account} />
          <FormItem labelName="开户行" placeholder={bank} />
        </div>
        <FormItem labelName="开票项目" placeholder={projectName} />
        <FormItem
          labelName="发票收货地址"
          placeholder={contacts + ' ' + phone + ' ' + address}
        />
      </div>
    );
  }
}
