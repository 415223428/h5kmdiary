/**
 * Created by feitingting on 2017/7/31.
 */
import { Actor, Action, IMap } from 'plume2';
export default class InvoiceActor extends Actor {
  defaultState() {
    return {
      invoiceInfo: {
        // 订单开票id
        orderInvoiceId: '',
        //发票类型，0：普通发票 1：增值税专用发票 -1：无
        type: '',
        // 0:个人 1:单位，必传
        flag: 0,
        //发票的收货地址
        address: '',
        //联系人
        contacts: '',
        //联系电话
        phone: '',
        //开票项目
        projectName: '',
        //发票抬头
        title: '',
        //纳税人识别号
        identification: '',
        //单位名称
        companyName: '',
        //注册电话
        phoneNo: '',
        //开户行
        bank: '',
        //银行账户
        account: '',
        //注册地址
        companyAddress: '',
        // 省
        provinceId: '',
        // 市
        cityId: '',
        // 区
        areaId: ''
      }
    };
  }

  /**
   * 通用状态值，地址联系人电话类型
   * @param state
   * @param res
   * @returns {Map<K, V>}
   */
  @Action('invoice:common')
  common(state: IMap, res: IMap) {
    return state
      .setIn(['invoiceInfo', 'address'], (res as any).address)
      .setIn(['invoiceInfo', 'contacts'], (res as any).contacts)
      .setIn(['invoiceInfo', 'phone'], (res as any).phone)
      .setIn(['invoiceInfo', 'type'], (res as any).type)
      .setIn(['invoiceInfo', 'projectName'], (res as any).projectName)
      .setIn(['invoiceInfo', 'provinceId'], (res as any).provinceId)
      .setIn(['invoiceInfo', 'cityId'], (res as any).cityId)
      .setIn(['invoiceInfo', 'areaId'], (res as any).areaId);
  }

  /**
   * 普票名称
   * @param state
   * @param res
   * @returns {Map<K, V>}
   */
  @Action('invoice:general')
  general(state: IMap, res: IMap) {
    return state
      .setIn(['invoiceInfo', 'title'], (res as any).generalInvoice.title)
      .setIn(['invoiceInfo', 'flag'], (res as any).generalInvoice.flag)
      .setIn(
        ['invoiceInfo', 'identification'],
        (res as any).generalInvoice.identification
      );
  }

  /**
   * 增票专用
   * @param state
   * @param res
   * @returns {Map<K, V>}
   */
  @Action('invoice:special')
  special(state: IMap, res: IMap) {
    return state
      .setIn(
        ['invoiceInfo', 'companyName'],
        (res as any).specialInvoice.companyName
      )
      .setIn(['invoiceInfo', 'phoneNo'], (res as any).specialInvoice.phoneNo)
      .setIn(['invoiceInfo', 'bank'], (res as any).specialInvoice.bank)
      .setIn(['invoiceInfo', 'account'], (res as any).specialInvoice.account)
      .setIn(
        ['invoiceInfo', 'companyAddress'],
        (res as any).specialInvoice.address
      )
      .setIn(
        ['invoiceInfo', 'identification'],
        (res as any).specialInvoice.identification
      );
  }
}
