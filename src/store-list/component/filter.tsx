import React from 'react';
import { Relax } from 'plume2';
import { noop, FindArea } from 'wmkit';
import { fromJS } from 'immutable';
const confirm=require('../img/confirm.png')
const reset=require('../img/reset.png')
@Relax
export default class Filter extends React.Component<any, any> {
  props: {
    relaxProps?: {
      visible: boolean;
      changeCompanyType: Function;
      changeProvOption: Function;
      changeCityOption: Function;
      clickFilter: Function;
    };
  };

  static relaxProps = {
    visible: 'visible',
    changeCompanyType: noop,
    changeProvOption: noop,
    changeCityOption: noop,
    clickFilter: noop
  };

  constructor(props) {
    super(props);
    const initProvinces = FindArea.findProvinceCity();
    this.state = {
      selProvinceId: '',
      companyType: null,
      searchProvinces: initProvinces,
      initProvinces: initProvinces //保留一份原始的未选中的地区信息
    };
  }

  render() {
    const visible = this.props.relaxProps.visible;
    if (!visible) {
      return null;
    }

    let { selProvinceId, companyType, searchProvinces } = this.state;
    searchProvinces = searchProvinces.map((prov) => {
      //当前省份是否选中全部
      if (prov.get('checked')) {
        return prov.set('highLight', true);
      }
      //当前省份下有地市被选中
      if (prov.get('children').some((city) => city.get('checked'))) {
        return prov.set('highLight', true);
      }
      return prov;
    });

    return (
      <div>
        {
          <div>
            <div className="mask" onClick={this._closeFilter} />
            <div className="filter-box" style={{ zIndex: 9999 }}>
              <div className="filter-content">
                <div className="filter-item">
                  <div className="filter-head">
                    <h4>店铺类型</h4>
                  </div>
                  <div className="filter-sort">
                    <span
                      className='store-sorts-item'
                      // {
                        // companyType == 0
                        //   ? 'store-sorts-item select-item-checked'
                        //   : 'store-sorts-item'
                      // }
                      style={companyType == 0?{background:'linear-gradient(to right,#FF6A4D,#FF1A1A)',color: '#fff',borderRadius:'2px'}:{}}
                      onClick={() => this._changeCompanyType(companyType)}
                    >
                      自营店铺
                      {/* {companyType == 0 && <i className="iconfont icon-gou" />} */}
                    </span>
                  </div>
                </div>

                {/* 地区选择 */}
                <div className="filter-item">
                  <div className="filter-head">
                    <h4>所在地区</h4>
                    <i className="iconfont icon-jiantou2" />
                  </div>
                  {/* <div className="locate-box">
                    <div className="locate-item">
                      <i className="iconfont icon-shouhuodizhi"></i>
                      <span>定位失败</span>
                      <span>南京市</span>
                    </div>
                    <a href="javascript:;" className="refresh-locate">
                      <i className="iconfont icon-shuaxin"></i>
                      刷新定位
                     </a>
                  </div> */}
                </div>

                <div className="locate-content">
                  <div className="locate-province">
                    <a
                      href="javascript:;"
                      className={selProvinceId == '' ? 'cur' : ''}
                      onClick={() => this._changeCurrProvince('')}
                    >
                      所有地区
                    </a>
                    {searchProvinces &&
                      searchProvinces.toJS().map((v) => {
                        return (
                          <a
                            key={v.code}
                            className={
                              v.code == selProvinceId
                                ? 'cur'
                                : v.highLight && 'cur'
                            }
                            href="javascript:;"
                            onClick={() => this._changeCurrProvince(v.code)}
                          >
                            {v.name}
                          </a>
                        );
                      })}
                  </div>
                  <div className="locate-area">
                    {this._renderCityList(selProvinceId)}
                  </div>
                </div>
              </div>

              {/*底部按钮*/}
              <div className="filter-btn1">
                {/* <button
                  className="filter-btn-reset"
                  onClick={this._resetFilter}
                >
                  重置
                </button> */}
                  <img src={reset} alt="" onClick={this._resetFilter}/>

                {/* <button className="filter-btn-yes" onClick={this._submitFilter}>
                  确定
                </button> */}
                  <img src={confirm} alt="" onClick={this._submitFilter}/>

              </div>
            </div>
          </div>
        }
      </div>
    );
  }

  /**
   * 根据选中省份渲染城市列表
   */
  _renderCityList = (selProvId) => {
    if (selProvId == '') {
      return null;
    }
    const { searchProvinces } = this.state;
    const currProv = searchProvinces.find((v) => v.get('code') == selProvId);
    const provChecked = currProv.get('checked'); //当前省份是否选中全部
    const cityList = currProv.get('children');

    return (
      <div>
        <a
          className={`b-1px-b ${provChecked == true ? 'cur' : null}`}
          href="javascript:;"
          onClick={() => this._toggleProvince(selProvId, provChecked)}
        >
          全部
          {provChecked == true && <i className="iconfont icon-danxuan" />}
        </a>

        {cityList &&
          cityList.toJS().map((city) => {
            return (
              <a
                key={city.code}
                className={`b-1px-b ${city.checked == true ? 'cur' : null}`}
                onClick={() =>
                  this._toggleCity(selProvId, city.code, city.checked)
                }
                href="javascript:;"
              >
                {city.name}
                {city.checked == true && (
                  <i className="iconfont icon-danxuan" />
                )}
              </a>
            );
          })}
      </div>
    );
  };

  /**
   * 选中/取消选中省份
   */
  _toggleProvince = (provId, currChecked) => {
    const { searchProvinces } = this.state;

    this.setState({
      searchProvinces: searchProvinces.map((prov) => {
        if (prov.get('code') == provId) {
          prov = prov.set('checked', !currChecked);
        }
        return prov;
      })
    });
  };

  /**
   * 选中/取消选中城市
   */
  _toggleCity = (selProvId, cityId, currChecked) => {
    const { searchProvinces } = this.state;

    this.setState({
      searchProvinces: searchProvinces.map((prov) => {
        if (prov.get('code') == selProvId) {
          prov = prov.set(
            'children',
            prov.get('children').map((city) => {
              if (city.get('code') == cityId) {
                city = city.set('checked', !currChecked);
              }
              return city;
            })
          );
        }
        return prov;
      })
    });
  };

  /**
   * 修改选中省份
   * @private
   */
  _changeCurrProvince = (code) => {
    this.setState({ selProvinceId: code });
  };

  /**
   * 修改是否自营
   */
  _changeCompanyType = (currCompanyType) => {
    if (currCompanyType == 0) {
      this.setState({ companyType: null }); //设为查询全部
    } else {
      this.setState({ companyType: 0 }); //设为查询平台自营
    }
  };

  /**
   * 重置筛选信息
   */
  _resetFilter = () => {
    this.setState({
      selProvinceId: '',
      companyType: null,
      searchProvinces: this.state.initProvinces //重置成之前保留的原始的未选中的地区信息
    });
  };

  /**
   * 根据选中筛选项进行筛选,并关闭筛选框
   */
  _submitFilter = () => {
    const {
      changeCompanyType,
      changeProvOption,
      changeCityOption,
      clickFilter
    } = this.props.relaxProps;
    let provList = fromJS([]);
    let cityList = fromJS([]);

    if (this.state.selProvinceId == '') {
      //点击所有地区之后确定搜索
      this.setState({
        searchProvinces: this.state.initProvinces //重置成之前保留的原始的未选中的地区信息
      });
    } else {
      const searchProvinces = this.state.searchProvinces;
      //将选中的省份,最终都转换成地市List进行搜索(但必须要保证用户填写省份的时候,同时填写了地市)
      provList = searchProvinces
        .filter((prov) => prov.get('checked'))
        .map((prov) => prov.get('code'));
      cityList = searchProvinces
        .flatMap((prov) => prov.get('children'))
        .filter((city) => city.get('checked'))
        .map((city) => city.get('code'));
    }
    changeCompanyType(this.state.companyType); //是否直营
    changeProvOption(provList); //选中的省份idList
    changeCityOption(cityList); //选中的地市idList
    clickFilter(false);
  };

  /**
   * 关闭筛选框
   */
  _closeFilter = () => {
    const { clickFilter } = this.props.relaxProps;
    clickFilter(false);
  };
}
