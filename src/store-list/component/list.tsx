import React from 'react'
import { Link } from 'react-router-dom'
import { Relax } from 'plume2'
import {FindArea,Blank,ListView,history} from 'wmkit'
import  ListViewOwen  from '../list-view/index'

const noneImg = require('../img/none.png')
const defaultImg = require('../img/defalutShop.png')
const inStore=require('../img/in-store.png')
@Relax
export default class List extends React.Component<any, any> {
  props: {
    relaxProps?: {
      initialEnd: boolean,
      keywords: string, //搜索关键字
      companyType: any,
      provinceIds: any,
      cityIds: any
    }
  }

  static relaxProps = {
    initialEnd: 'initialEnd',
    keywords: 'keywords',
    companyType: 'companyType',
    provinceIds: 'provinceIds',
    cityIds: 'cityIds'
  }

  render() {
    const {initialEnd} = this.props.relaxProps;
    // 组件刚执行完mount，搜索条件没有注入进来，先不加载ListView，避免先进行一次无条件搜索，再立刻进行一次有条件搜索
    if(!initialEnd){
      return null;
    }

    const {keywords,companyType,provinceIds,cityIds} = this.props.relaxProps;
    const allAreaIds = provinceIds.concat(cityIds).toJS();
    return (
      <ListViewOwen
        url="/stores"
        params={{
          keywords,
          companyType,
          allAreaIds
        }}
        renderRow={this._storeInfoRow}
        renderEmpty={() => <Blank img={noneImg} content="没有搜到任何店铺～"/>}
      />
    )
  }


  /**
   * 店铺信息
   */
  _storeInfoRow = (storeInfo) => {
    return (
      <div key={storeInfo.storeId} className="supplier-item b-1px-b address-box" onClick={() => history.push(`/store-main/${storeInfo.storeId}`)} >
        <div className="supplier-left supplier-store">
          <div className="store-img-head">
            <img src={storeInfo.storeLogo || defaultImg} alt="" />
          </div>
          <div className="supplier-content">
            <h4 style={{}}>{storeInfo.storeName}</h4>
            <p>{storeInfo.supplierName}</p>
            <div className="self-sales" style={storeInfo.companyType == 0 ? null : {visibility:'hidden'}}>自营</div>
          </div>
        </div>
        <div className="supplier-right" style={{justifyContent:'center'}}>
          <Link to={`/store-main/${storeInfo.storeId}`}><img src={inStore} alt=""/></Link>
          <p style={{position:'relative',top:'0.5rem'}}>
            <img src={require("../img/didian.png")} alt=""/>
            {FindArea.findCity(storeInfo.cityId)}
          </p>
        </div>
      </div>
    )
  }
}