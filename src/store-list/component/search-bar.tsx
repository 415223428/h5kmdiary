import React from 'react'
import { Relax } from 'plume2'
import {noop, history} from 'wmkit'
@Relax
export default class SearchBar extends React.Component<any, any>{

  props: {
    relaxProps?: {
      clickFilter: Function,// 打开筛选框
      keywords: string, //搜索关键字
    }
  }

  static relaxProps = {
    clickFilter: noop,
    keywords: 'keywords',
  }

  render() {
    const {keywords} = this.props.relaxProps;
    return (

      <div className="search-container">
        <div className="search-box">
          <div className="input-box" onClick={() => history.push({pathname: '/search', state: {queryString:keywords, key:'supplier'}})}>
            <i className="iconfont icon-sousuo"></i>
            <input type="text" placeholder="搜索商家" value={keywords}/>
          </div>
          <div>
            <a href="javascript:;" onClick={() => this._open()} className="list-tab">
              {/* <i className="iconfont icon-shaixuan"></i>
              <span>筛选</span> */}
              <img src={require('../img/shaixuan.png')} alt="" style={{width:".3rem",height:".3rem"}}></img>
            </a>
          </div>
        </div>
      </div>

    )
  }

  /**
   * 打开筛选
   */
  _open = () => {
    const { clickFilter } = this.props.relaxProps;
    clickFilter(true);
  }
}