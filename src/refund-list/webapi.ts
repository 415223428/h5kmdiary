import {Fetch} from 'wmkit'

/**
 * 取消
 */
export const cancel = (rid: string, reason: string) => {
  return Fetch(`/return/cancel/${rid}?reason=${reason}`, {method: 'POST'})
};