import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

/**
 * Created by chenpeng on 2017/7/10.
 */
export default class TabActor extends Actor {
  defaultState() {
    return {
      key: 'all', //tab-key // 列表数据
      form: {
        // 邀请人ID
        returnFlowState: '',
        inviteeId: '',
        // 分销渠道
        channelType: ''
      }
    };
  }

  @Action('tab:change')
  init(state: IMap, key: string) {
    return state.set('key', key);
  }

  /**
   * 设置列表数据
   */
  @Action('return-list-form:setForm')
  setForm(state: IMap, params: IMap) {
    return state.set('form', fromJS(params));
  }
}
