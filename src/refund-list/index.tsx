import React, { Component } from 'react';

import { StoreProvider } from 'plume2';
import { ListView, Blank } from 'wmkit';
import { fromJS } from 'immutable';

import AppStore from './store';
import RefundTab from './component/refund-tab';
import RefundListItem from './component/refund-list-item';

@StoreProvider(AppStore)
export default class RefundList extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    this.store.onTabChange('');
  }

  render() {
    let form = (this.store.state().get('form') || fromJS([])).toJS();
    return (
      <div>
        <RefundTab />
        <ListView
          url="/return/page"
          params={form}
          renderRow={(order: object, index: number) => (
            <RefundListItem refundOrder={order} index={index} />
          )}
          renderEmpty={() => (
            <Blank
              img={require('./img/list-none.png')}
              content="您还没有退货退款哦"
              isToGoodsList={true}
            />
          )}
        />
      </div>
    );
  }
}
