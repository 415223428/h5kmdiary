import React, { Component } from 'react';
import { Relax } from 'plume2';
import { Const } from 'config';
import { Button, history, Confirm, noop, WMkit } from 'wmkit';
import { Link } from 'react-router-dom';

const SmallBlueButton = Button.SmallBlue;
const noneImg = require('../img/emptygoods.png');

@Relax
export default class RefundListItem extends Component<any, any> {
  props: {
    relaxProps?: {
      cancel: Function;
    };
    refundOrder: any;
    index: number;
  };

  static relaxProps = {
    cancel: noop
  };

  render() {
    let { refundOrder } = this.props;

    let returnFlowState = refundOrder.returnFlowState;
    let applyPrice = refundOrder.returnPrice.applyStatus
      ? refundOrder.returnPrice.applyPrice
      : refundOrder.returnPrice.totalPrice;

    //退货赠品
    const returnGifts = refundOrder.returnGifts || [];
    //所有商品
    let items = refundOrder.returnItems.concat(returnGifts);
    //分销渠道
    const channelType = WMkit.channelType();
    return (
      <div className="ships">
        <div className="order-item address-box" style={{
          border: '1px solid rgba(155, 149, 148, 0)',
          background: '#fff'
        }}>
          <div
            className="order-head"
            onClick={() =>
              history.push(`/store-main/${refundOrder.company.storeId}`)
            }
          >
            {WMkit.inviteeId() && WMkit.isShop() ? (
              <div className="order-head-num">
                {/* <span>{refundOrder.platform == 'CUSTOMER' ? '退' : '代'}</span> */}
                <img src={require('../img/tuikuan.png')} alt="" style={{
                  width: '.4rem', height: '.4rem'
                }} />
                <div className="order-storeName">
                  <Link to="/shop-index-c">
                    {refundOrder.distributorName}的{refundOrder.shopName}
                  </Link>
                </div>
              </div>
            ) : (
                <div className="order-head-num">
                  {/* <span>{refundOrder.platform == 'CUSTOMER' ? '退' : '代'}</span> */}
                  <img src={require('../img/tuikuan.png')} alt="" style={{
                    width: '.42rem', height: '.39rem'
                  }} />
                  <div className="order-storeName">
                    {refundOrder.company.companyType == 0 && (
                      <div className="self-sales">自营</div>
                    )}
                    {refundOrder.company.storeName}
                  </div>
                </div>
              )}

            <div className="status" style={{
              fontWeight: 500,
              color: 'rgba(153,153,153,1)',
            }}>
              {refundOrder.returnType == 'RETURN'
                ? Const.returnGoodsState[returnFlowState]
                : Const.returnMoneyState[returnFlowState] || ''}
            </div>
          </div>
          <div
            className="limit-img ship-img"
            onClick={() => history.push(`/return-detail/${refundOrder.id}`)}
          >
            <div className="img-content">
              {items.map((item, index) => {
                return index < 4 ? (
                  <img
                    className="img-item"
                    src={item.pic ? item.pic : noneImg}
                  />
                ) : null;
              })}
            </div>
            <div className="right-context">
              <div className="total-num" style={{
                fontWeight: 500,
                color: 'rgba(153,153,153,1)',
              }}>共{items.length}种</div>
              <i className="iconfont icon-jiantou1" />
            </div>
          </div>
          <div className="bottom">
            <div className="price">
              <i className="iconfont icon-qian" />
              {applyPrice.toFixed(2)}
            </div>
            <div className="botton-box">
              {returnFlowState === 'INIT' && (
                <SmallBlueButton
                  defaultStyle={{ borderColor: '#ebebeb', color: '#333' }}
                  text="取消退单"
                  onClick={() => {
                    Confirm({
                      title: '取消退货退款',
                      text: '您确定要取消该退货退款？',
                      confirmCb: () => {
                        this.props.relaxProps.cancel(refundOrder.id);
                      },
                      okBtn: '确定',
                      cancelBtn: '取消'
                    });
                  }}
                />
              )}
              {/* {returnFlowState === 'INIT' && (
                <img src={require('../img/quxiaodingdan.png')} alt=""
                  style={{
                    width: '1.7rem',
                    height: '.5rem'
                  }}
                  onClick={() => {
                    Confirm({
                      title: '取消退货退款',
                      text: '您确定要取消该退货退款？',
                      confirmCb: () => {
                        this.props.relaxProps.cancel(refundOrder.id);
                      },
                      okBtn: '确定',
                      cancelBtn: '取消'
                    });
                  }} />
              )} */}
              {/*退货单的已审核状态*/}
              {returnFlowState === 'AUDIT' &&
                refundOrder.returnType == 'RETURN' && (
                  <SmallBlueButton
                    text="填写物流"
                    onClick={() =>
                      history.push({
                        pathname: `/logistics-input/${refundOrder.id}`,
                        state: { storeId: refundOrder.company.storeId }
                      })
                    }
                  />
                  // <img src={require('../img/quxiaodingdan.png')} alt=""
                  //   style={{
                  //     width: '1.7rem',
                  //     height: '.5rem'
                  //   }}
                  //   onClick={() =>
                  //     history.push({
                  //       pathname: `/logistics-input/${refundOrder.id}`,
                  //       state: { storeId: refundOrder.company.storeId }
                  //     })
                  //   } />
                )}
            </div>
          </div>
        </div>
        {/* <div className="bot-line" /> */}
      </div>
    );
  }
}
