import React, {Component} from "react";
import {fromJS} from 'immutable'
import {Relax} from "plume2";
import {noop, Tabs} from "wmkit";


const tabList = [
  {'label': '全部', 'tabKey': 'all'},
  {'label': '待审核', 'tabKey': 'INIT'},
  {'label': '待填写物流', 'tabKey': 'AUDIT'},
  {'label': '待商家收货', 'tabKey': 'DELIVERED'},
  {'label': '待退款', 'tabKey': 'RECEIVED'},
  {'label': '已完成', 'tabKey': 'COMPLETED'},
  {'label': '拒绝退款', 'tabKey': 'REJECT_REFUND'},
  {'label': '拒绝收货', 'tabKey': 'REJECT_RECEIVE'},
  {'label': '已作废', 'tabKey': 'VOID'}
]


@Relax
export default class RefundTab extends Component<any, any> {

  props: {
    relaxProps?: {
      key: string
      onTabChange: Function
    }
  }


  static relaxProps = {
    key: 'key',
    onTabChange: noop
  }


  render() {
    let {key, onTabChange} = this.props.relaxProps
    let tabs = fromJS(tabList)

    return (
        <div style={{height: 48,background:'#fafafa'}}><Tabs style={{width: '100%',display:'-webkit-box',background:'#fff'}} curClass="redcur" data={tabs} activeKey={key} onTabClick={onTabChange}/>
        </div>
    )
  }
}
