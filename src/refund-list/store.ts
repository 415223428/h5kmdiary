/**
 * Created by chenpeng on 2017/7/10.
 */
import { Store } from 'plume2';
import { Alert, WMkit } from 'wmkit';
import TabActor from './actor/tab-actor';
import * as webapi from './webapi';
import { fromJS } from 'immutable';

export default class AppStore extends Store {
  bindActor() {
    return [new TabActor()];
  }

  /**
   * 切换tab
   * @param tabKey
   */
  onTabChange = (tabKey: string) => {
    if (this.state().get('key') === tabKey) {
      return false;
    }
    let form = {};
    if (tabKey !== 'all') {
      form['returnFlowState'] = tabKey;
    }
    // 有邀请人ID并且是店铺内退单，则显示店铺内订单，否则是全部订单
    if (WMkit.inviteeId() && WMkit.isShop()) {
      form['inviteeId'] = WMkit.inviteeId();
      form['channelType'] = WMkit.channelType();
    }
    this.transaction(() => {
      this.dispatch('tab:change', tabKey);
      this.dispatch('return-list-form:setForm', fromJS(form));
    });
  };

  /**
   * 取消退单
   * @param rid
   * @returns {Promise<TResult|TResult2|TResult1>}
   */
  cancel = async (rid: string) => {
    const { code, message } = await webapi.cancel(rid, '用户取消');
    if (code == 'K-000000') {
      Alert({ text: '成功取消退单' });
      location.reload();
    } else {
      Alert({ text: message });
    }
  };
}
