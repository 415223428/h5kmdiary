import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const style = require('./css/style.css');
import DetailList from './component/detail-list';
import AppalyLayer from './component/appaly-layer';
@StoreProvider(AppStore, { debug: __DEV__ })
export default class CashDetail extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { dcId } = this.props.match.params;
    this.store.init(dcId);
  }

  render() {
    let visible = this.store.state().get('visible');
    return (
      <div className="cash-detail">
        {visible ? <AppalyLayer /> : <DetailList />}
      </div>
    );
  }
}
