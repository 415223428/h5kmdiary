import {Action, Actor, IMap} from 'plume2';
import { fromJS } from 'immutable'
export default class DetailActor extends Actor {
  defaultState() {
    return {
      visible: false,
      vo: fromJS({}),
      drawCashId:'',
      headImgUrl: ''
    }
  }

  /**
   * 提示框是否显示
   */
  @Action('CashActor:LayerShow')
  tipLayerShow(state, val) {
    return state.set('visible', val);
  }


  /**
   * 初始化提现单详情
   */
  @Action('draw:cash:detail:fetch')
  init(state: IMap, res: object) {
    return state.set('vo', res)
  }

  /**
   *
   * @param state
   * @param val
   */
  @Action('set:drawCashId')
  setDrawCashId(state, val) {
    return state.set('drawCashId', val);
  }

  /**
   * 微信昵称
   */
  @Action('CashActor:setheadImgUrl')
  setheadImgUrl(state, headImgUrl) {
    return state.set('headImgUrl', headImgUrl);
  }
}
