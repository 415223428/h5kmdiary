import {Store, IOptions, msg} from 'plume2';

import DetailActor from './actor/detail-actor';
import {Confirm, history, WMkit} from "wmkit";
import {init, sendCancel} from "./webapi";
import { fromJS } from 'immutable'
import {getLinkedAccountInfo} from "../applicat-form/webapi";
export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new DetailActor()];
  }

  /**
   * 取消单个提现单申请
   * @param dcId
   */
  sendCancel = async (dcId) =>{
    const data = await sendCancel(dcId) as any;
    if(data.code == 'K-000000'){
      if (!WMkit.isLoginOrNotOpen()) {
        msg.emit('loginModal:toggleVisible', {
          callBack: () => {
            history.push('/balance/cash-record');
          }
        });
      } else {
        history.push('/balance/cash-record');
      }
    }else{
      Confirm({
        text: data.message,
        okBtn: '确定',
        confirmCb: () => history.push('/balance/cash-record')
      })
    }

  }

  /**
   * 初始化
   */
  init = async (dcId) => {
    const data = await init(dcId) as any;
    if (data.code == 'K-000000') {
      const vo = (data.context.customerDrawCashVO);
      this.transaction(() => {
        this.dispatch('draw:cash:detail:fetch',fromJS(vo))
      })
    } else {
      Confirm({
        text: data.message,
        okBtn: '确定',
        confirmCb: () => history.push('/balance/cash-record')
      })
    }

    //账户信息
    const accountInfo = (await getLinkedAccountInfo()) as any;
    if (accountInfo.code == 'K-000000') {
      const headImgUrl = accountInfo.context.headimgurl;
      this.transaction(() => {
        this.dispatch('CashActor:setheadImgUrl', headImgUrl);
      });
    }
  };

  /**
   * 提示框取确认击事件
   */
  sureLayer = async (dcId) => {
      this.dispatch('CashActor:LayerShow', true);
      this.dispatch('set:drawCashId', dcId);
  };

  /**
   * 提示框取消点击事件
   */
  cancelLayer = async (val) => {
    this.dispatch('CashActor:LayerShow', val);
    this.dispatch('set:drawCashId', '');
  };
}
