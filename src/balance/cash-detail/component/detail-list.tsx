import React from 'react';
import { Link } from 'react-router-dom';
import { _, FormItem, noop } from 'wmkit';
import { IMap, Relax } from 'plume2';
import { IList } from '../../../../typings/globalType';
const cancel=require('../img/cancel.png')
@Relax
export default class DetailList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      sureLayer: Function;
      vo: IMap;
      headImgUrl:string
    };
  };

  static relaxProps = {
    sureLayer: noop,
    vo: 'vo',
    headImgUrl: 'headImgUrl'
  };

  render() {
    const { sureLayer, vo ,headImgUrl} = this.props.relaxProps;
    let statusTxt = '';
    if (vo.get('customerOperateStatus') == 1) {
      statusTxt = '已取消';
    } else if (vo.get('customerOperateStatus') == 0) {
      if (vo.get('auditStatus') == 0) {
        statusTxt = '待审核';
      } else if (vo.get('auditStatus') == 1) {
        statusTxt = '已打回';
      } else if (vo.get('auditStatus') == 2 && vo.get('drawCashStatus') == 2) {
        statusTxt = '已完成';
      } else if (vo.get('auditStatus') == 2 && vo.get('drawCashStatus') == 1) {
        statusTxt = '待审核';
      }
    }
    const drawCashChannel = vo.get('drawCashChannel') == 0 ? '微信钱包' : '支付宝';
    const drawCashSum = _.fmoney(vo.get('drawCashSum'), 2);
    return (
      <div className="detail-list">
        <FormItem labelName="提现状态" placeholder={statusTxt} />
        <FormItem labelName="提现方式" placeholder={drawCashChannel} />
        <FormItem labelName="提现单号" placeholder={vo.get('drawCashNo')} />
        <FormItem
          labelName="申请时间"
          placeholder={_.formatDate(vo.get('applyTime'))}
        />
        <div className="account-div">
          <span className="left-title">提现账户</span>
          <span className="right-account">
            <span className="name">{vo.get('drawCashAccountName')}</span>
            <img
              className="pointer"
              src={headImgUrl}
            />
          </span>
        </div>

        <FormItem
          labelName="提现金额"
          textStyle={{ color: '#FF1F4E' }}
          placeholder={'¥' + drawCashSum}
        />
        <FormItem labelName="申请备注" placeholder={vo.get('drawCashRemark')?vo.get('drawCashRemark'):'无'} />
        {vo.get('auditStatus') == 1 && (
          <FormItem labelName="驳回原因" placeholder={vo.get('rejectReason')} />
        )}
        <div className="bottom-div">
          {vo.get('customerOperateStatus') == 0 &&
            vo.get('auditStatus') == 0 && (
              <div
                className="cancel-btn"
                style={{padding:'0',border:'none'}}
                onClick={() => sureLayer(vo.get('drawCashId'))}
              >
                <img src={cancel} alt="" style={{width:'100%'}}
                     onClick={() => sureLayer(vo.get('drawCashId'))}/>
              </div>
            )}
        </div>
      </div>
    );
  }
}
