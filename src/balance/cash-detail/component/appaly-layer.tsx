import React from 'react';
import { Link } from 'react-router-dom';
import { WMkit, noop, history } from 'wmkit';
import { msg, Relax } from 'plume2';
@Relax
export default class AppalyLayer extends React.Component<any, any> {
  props: {
    relaxProps?: {
      cancelLayer: Function;
      drawCashId: string;
      sendCancel: Function;
    };
  };

  static relaxProps = {
    cancelLayer: noop,
    drawCashId: 'drawCashId',
    sendCancel: noop
  };
  render() {
    const { cancelLayer, drawCashId, sendCancel } = this.props.relaxProps;
    return (
      <div className="black-layer">
        <div className="white-layer">
          <span className="payment-title">是否确认取消当前提现申请</span>
          <div className="down-btn">
            <div className="cancel-btn" onClick={() => cancelLayer(false)}>
              再想想
            </div>
            <div
              className="submit-btn"
              onClick={() => {
                sendCancel(drawCashId);
              }}
            >
              确定
            </div>
          </div>
        </div>
      </div>
    );
  }
}
