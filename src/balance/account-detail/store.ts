import { Store, IOptions } from 'plume2';

import { Const } from 'config';
import { fromJS } from 'immutable';
import { getFundsDetailList } from './webapi';

import FundsDetailActor from './actor/account-detail-actor';
import { Alert } from 'wmkit';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new FundsDetailActor()];
  }
  /**
   * 初始化余额明细列表
   */
  init = async ({ pageNum, pageSize } = { pageNum: 0, pageSize: 10 }) => {
    const params = this.state()
      .get('form')
      .toJS();
    const pageList: any = await getFundsDetailList({
      ...params,
      pageSize,
      pageNum
    });
    if (pageList.res.code === 'K-000000') {
      this.transaction(() => {
        this.fetchFunds(pageList.res);
      });
    } else {
      Alert({
        text: (pageList.res as any).message
      });
    }
  };

  /**
   * 设置账户明细列表
   * @param res
   */
  fetchFunds = (res: any) => {
    if (res.context) {
      this.dispatch('funds:detail:setFunds', fromJS(res.context.content));
    }
  };

  /**
   * 切换tab
   */
  changeTab = (tabType) => {
    this.dispatch('funds:detail:changeTab', tabType);
    this.init();
  };
}
