import React, { Component } from 'react';
import { IMap, StoreProvider } from 'plume2';
import AppStore from './store';
const style = require('./css/style.css');
import Header from './component/header';
import AccountDetailList from './component/account-detail';
import Blank from 'wmkit/blank';
import ListView from 'wmkit/list-view';
import { fromJS } from 'immutable';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class AccountDetail extends Component<any, any> {
  store: AppStore;
  componentDidMount() {
    this.store.init();
  }
  render() {
    let form = (this.store.state().get('form') || fromJS([])).toJS();
    let fetchFunds = this.store.fetchFunds;
    return (
      <div className="account-detail">
        <Header />
        <ListView
          className="account-ul"
          url="/customer/funds/page"
          params={form}
          isPagination={true}
          // style={{ height: window.innerHeight - 88,background:"rgba(255,255,255,1)" }}
          style={{ height: "100%",background:"rgba(255,255,255,1)" }}
          renderRow={(fund: IMap) => {
            return <AccountDetailList funds={fund} />;
          }}
          renderEmpty={() => (
            <Blank
              img={require('./img/list-none.png')}
              content="您暂时还没有收入支出哦"
              isToGoodsList={true}
            />
          )}
          onDataReached={(res) => fetchFunds(res)}
        />
      </div>
    );
  }
}
