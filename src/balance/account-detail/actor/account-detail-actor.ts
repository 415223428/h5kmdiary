import { Action, Actor } from 'plume2';
import { IMap } from 'typings/globalType';
import { List } from 'immutable';

export default class AccountDetailActor extends Actor {
  defaultState() {
    return {
      // Tab类型 0: 全部, 1: 收入, 2: 支出
      form: {
        tabType: '0', //排序字段
        sortColumn: 'createTime', //排序规则 desc asc
        sortRole: 'desc'
      },
      funds: [] //明细列表=
    };
  }

  /**
   * 切换tab展示余额明细
   * @param state
   * @param tabType
   */
  @Action('funds:detail:changeTab')
  changeTab(state: IMap, tabType) {
    return state.setIn(['form', 'tabType'], tabType);
  }

  /**
   * 更新订单数据
   */
  @Action('funds:detail:setFunds')
  updateOrders(state: IMap, params: Array<any>) {
    return state.update('funds', (value: List<any>) => {
      return value.push(...params);
    });
  }

  /**
   * 切换tab时清除订单数据
   */
  @Action('funds:detail:clearFunds')
  clearOrders(state: IMap) {
    return state.set('funds', List());
  }
}
