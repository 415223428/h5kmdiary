import { Fetch } from 'wmkit';
type TResult = { code: string; message: string; context: any };

/**
 * 获取余额明细分页列表
 */
export const getFundsDetailList = (filterParams = {}) => {
  return Fetch('/customer/funds/page', {
    method: 'POST',
    body: JSON.stringify({
      ...filterParams
    })
  });
};
