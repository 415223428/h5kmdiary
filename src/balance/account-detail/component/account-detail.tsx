import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';
import moment from 'moment';
import { Const } from 'config';
const FUNDS_TYPE = {
  1: '推广返利',
  2: '佣金提现',
  3: '邀新奖励',
  4: '自购返利',
  5: '推广提成',
  6: '余额支付',
  7: '余额支付退款'
};
@Relax
export default class AccountDetailList extends Component<any, any> {
  props: {
    funds: any;
  };

  render() {
    let { funds } = this.props;
    return (
      <li>
        <span className="left-spn">
          <span className="black-text">{FUNDS_TYPE[funds.subType]}</span>
          {funds.subType == 6 || funds.subType == 7 ? (
            <span className="black-text">{funds.businessId}</span>
          ) : null}
          {/*<span className="gray-text">关联用户：小羊苏西</span>*/}
          <span className="date">
          {funds
            ? moment(funds.createTime).format(Const.SECONDS_FORMAT)
            : '-'}
        </span>
        </span>
        <span className="right-spn">
          <span className={funds.subType == 6 ? "money" : "money red-color"}>
            {this._handReceiptPaymentAmount(
              funds.subType,
              funds.receiptPaymentAmount
            )}
          </span>
        </span>

      </li>
    );
  }
  _handReceiptPaymentAmount = (fundsType, receiptPaymentAmount) => {
    let sign = '+';
    if (fundsType == 2 || fundsType == 6) {
      sign = '-';
    }
    return sign + '￥' + receiptPaymentAmount.toFixed(2);
  };
}
