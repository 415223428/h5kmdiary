import React from 'react';
import { Link } from 'react-router-dom';
import { Relax, IMap } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class Header extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { list: '全部', id: 0 },
        { list: '收入', id: 1 },
        { list: '支出', id: 2 }
      ]
    };
  }
  props: {
    relaxProps?: {
      changeTab: Function;
      form: IMap;
    };
  };

  static relaxProps = {
    changeTab: noop,
    form: 'form'
  };

  _selectLi(key) {
    this.props.relaxProps.changeTab(key);
  }

  render() {
    const { form } = this.props.relaxProps;
    const key = form.get('tabType');
    return (
      <ul className="header-ul">
        {this.state.data.map((o, i) => {
          return (
            <li
              key={i}
              className={key == o.id && 'select-li'}
              onClick={() => this._selectLi(o.id)}
            >
              <span>{o.list}</span>
            </li>
          );
        })}
      </ul>
    );
  }
}
