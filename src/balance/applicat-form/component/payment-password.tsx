import React from 'react';
import { Link } from 'react-router-dom';
import { WMkit, noop, history } from 'wmkit';
import { msg, Relax } from 'plume2';
@Relax
export default class PaymentPassword extends React.Component<any, any> {
  props: {
    relaxProps?: {
      cancelLayer: Function;
      addCustomerDrawCash: Function;
      changePayPwd: Function;
      checkPayPwdRes: boolean;
      payPwdTime: number;
    };
  };

  static relaxProps = {
    cancelLayer: noop,
    addCustomerDrawCash: noop,
    changePayPwd: noop,
    checkPayPwdRes: 'checkPayPwdRes',
    payPwdTime: 'payPwdTime'
  };
  render() {
    const {
      cancelLayer,
      addCustomerDrawCash,
      changePayPwd,
      checkPayPwdRes,
      payPwdTime
    } = this.props.relaxProps;
    let payPwdErrorTime = 3 - payPwdTime;
    return (
      <div className="black-layer ">
        <div className="white-layer address-box">
          <span className="payment-title">请输入支付密码</span>
          <div className="input-div">
            <input
              type="password"
              className="int-pass"
              onChange={(e) => {
                changePayPwd(e.target.value);
              }}
            />
          </div>
          <div className="forget-pass">
            {checkPayPwdRes ? null : (
              <span className="left-text">
                {payPwdTime == 3
                  ? '账户已冻结，请30分钟后重试'
                  : '密码错误，还有' + payPwdErrorTime + '次机会'}
              </span>
            )}
            <span className="right-text" onClick={() => this.forgetPayPwd()}>
              忘记密码？
            </span>
          </div>
          <div className="down-btn">
            <div className="left-btn" onClick={() => cancelLayer(false)}>
              取消
            </div>
            {checkPayPwdRes ? (
              <div className="right-btn" onClick={() => addCustomerDrawCash()}>
                提交
              </div>
            ) : payPwdTime == 3 ? (
              <div className="no-submit-btn">提交</div>
            ) : (
              <div className="no-right-btn" onClick={() => addCustomerDrawCash()}>
                提交
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 忘记密码
   */
  forgetPayPwd() {
    history.push({
      pathname: '/balance-pay-password',
      state: { forget: true }
    });
  }
}
