import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { FormInput, _, ValidConst, Alert, noop } from 'wmkit';
import { Const } from 'config';
const sub=require('../img/submit.png')
@Relax
export default class CashForm extends React.Component<any, any> {
  props: {
    relaxProps?: {
      money: number;
      alreadyDrawCash: number;
      paymentPass: Function;
      changeCustomerDrawCashInfo: Function;
      headImgUrl: string;
      nickName: string;
      checkCountDrawCashSum: Function;
      checkDrawCashSum: Function;
      minDrawCash:number;
    };
  };

  static relaxProps = {
    money: 'money',
    alreadyDrawCash: 'alreadyDrawCash',
    paymentPass: noop,
    changeCustomerDrawCashInfo: noop,
    headImgUrl: 'headImgUrl',
    nickName: 'nickName',
    checkCountDrawCashSum: noop,
    checkDrawCashSum: noop,
    minDrawCash:'minDrawCash'
  };

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      OverCash: false,
      noticeTitle: ''
    };
  }

  render() {
    const {
      money,
      alreadyDrawCash,
      nickName,
      headImgUrl
    } = this.props.relaxProps;
    // 提现上限
    const maxLimit = money > Const.MAX_DRAW_CASH ? Const.MAX_DRAW_CASH : money;
    // 可提现金额，余额小于限定金额10000，则是余额，否则取限定金额
    let canDrawCash = maxLimit;
    if (alreadyDrawCash) {
      // 已提现金额 < 最大限额
      if (alreadyDrawCash < Const.MAX_DRAW_CASH) {
        // 剩余可提现金额 =（最大限额 - 已提现金额）
        let restDrawCash = _.sub(Const.MAX_DRAW_CASH, alreadyDrawCash);
        canDrawCash = maxLimit > restDrawCash ? restDrawCash : maxLimit;
      } else {
        canDrawCash = 0;
      }
    }
    return (
      <div>
        <div className="up-div">
          <img className="portrait" src={headImgUrl} />
          <span className="user-name">{nickName}</span>
        </div>
        <div className="down-div">
          <span className="cash-money">提现金额</span>
          <div className="rmb">
            <span className="unit">￥</span>
            <input
              type="number"
              className="write-int"
              onChange={(e) => this.setInput(e.target.value, canDrawCash)}
              value={this.state.value}
            />
          </div>
          <div className="write-div">
            {this.state.OverCash ? (
              <span className="tips">{this.state.noticeTitle}</span>
            ) : (
              <span>
                <span className="tips-span">{`本次最多可提现￥${canDrawCash}`}</span>
                <span
                  className="all-cash"
                  onClick={() => this._allCash(canDrawCash)}
                >
                  全部提现
                </span>
              </span>
            )}
          </div>
          <FormInput
            label={'备注'}
            placeHolder={'（非必填）最多输入50字'}
            onChange={(e) => this.setDrawCashRemark(e)}
            maxLength={50}
            defaultValue={null}
          />
        </div>
        {this.state.OverCash || this.state.value == '' ? (
          <div className="no-cash-btn">提交</div>
        ) : (
          <div
            className="cash-btn"
            onClick={() => this._paymentPass(this.state.value)}
          >
           <img src={sub} alt="" style={{width:'100%'}}/>
          </div>
        )}
      </div>
    );
  }

  /**
   * 提现金额input输入
   */
  setInput(inputMoney, canDrawCash) {
    const {minDrawCash} = this.props.relaxProps;
    if (!inputMoney) {
      this.setState({ OverCash: false, value: '', noticeTitle: '' });
    } else {
      const regex = ValidConst.price;
      this.setState({ OverCash: true, value: inputMoney, noticeTitle: '' });
      // 输入不合法
      if (!regex.test(inputMoney)) {
        this.setState({ noticeTitle: '输入有效金额（保留两位小数）' });
      } else if (inputMoney > canDrawCash) {
        // 提现金额超出我的余额
        this.setState({ noticeTitle: '输入金额超过可提现金额' });
      } else if (inputMoney < minDrawCash) {
        // 最少提现金额不可小于1元
        this.setState({
          noticeTitle: `最少提现金额不可小于${minDrawCash}元`
        });
      } else {
        this.setState({ OverCash: false });
        const { changeCustomerDrawCashInfo } = this.props.relaxProps;
        // 输入合法，设置提现金额
        changeCustomerDrawCashInfo('drawCashSum', inputMoney);
      }
    }
  }

  /**
   * 修改备注内容
   * @param e
   */
  setDrawCashRemark(e) {
    const { changeCustomerDrawCashInfo } = this.props.relaxProps;
    if (e.target.value != '') {
      changeCustomerDrawCashInfo('drawCashRemark', e.target.value);
    }
  }

  /**
   * 全部提现点击事件
   */
  _allCash(num) {
    const { changeCustomerDrawCashInfo } = this.props.relaxProps;
    this.setState({ value: num });
    changeCustomerDrawCashInfo('drawCashSum', num);
  }

  /**
   * 提交提现申请
   * @private
   */
  _paymentPass = async (drawCash) => {
    const {
      paymentPass,
      checkCountDrawCashSum,
      checkDrawCashSum
    } = this.props.relaxProps;
    if (!drawCash) {
      Alert({
        text: '提现金额不能为空！'
      });
      return;
    }
    //检验账户可提现金额
    let checkFlag = await checkDrawCashSum(drawCash);
    if (!checkFlag) {
      Alert({
        text: '余额不足，请修改提现金额！'
      });
      return;
    }

    //检验当天提现金额是否超出限制
    if (!(await checkCountDrawCashSum(drawCash))) {
      return;
    }
    paymentPass(true);
  };
}
