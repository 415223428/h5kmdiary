import { Action, Actor } from 'plume2';

export default class CashActor extends Actor {
  defaultState() {
    return {
      // 最多提现金额
      money: 0,
      // 我的今日已提现金额
      alreadyDrawCash: 0,
      visible: false,
      payPwd: '',
      customerDrawCashInfo: {
        drawCashAccountName: '',
        drawCashSum: 0,
        drawCashRemark: ''
      },
      checkPayPwdRes: true,
      payPwdTime: 0,
      nickName: '',
      headImgUrl: '',
      minDrawCash:1,
    };
  }

  /**
   * 获取提现金额数
   */
  @Action('CashActor:getMoney')
  getMoney(state, val) {
    return state.set('money', val);
  }

  /**
   * 我的今日已提现金额
   */
  @Action('cash:form:draw:money')
  getDrawMoney(state, val) {
    return state.set('alreadyDrawCash', val);
  }

  /**
   * 支付密码输入框是否显示
   */
  @Action('CashActor:passWordShow')
  passWordShow(state, val) {
    return state.set('visible', val);
  }

  /**
   * 支付密码输入框是否显示
   */
  @Action('CashActor:changePayPwd')
  changePayPwd(state, val) {
    return state.set('payPwd', val);
  }

  /**
   * 修改提现内容
   */
  @Action('CashActor:changeCustomerDrawCashInfo')
  changeCustomerDrawCashInfo(state, changeValue) {
    return state.setIn(
      ['customerDrawCashInfo', changeValue['key']],
      changeValue['value']
    );
  }

  /**
   * 校验密码结果
   */
  @Action('CashActor:checkPayPwdRes')
  checkPayPwdRes(state, checkPayPwdRes) {
    return state.set('checkPayPwdRes', checkPayPwdRes);
  }

  /**
   * 密码错误次数
   */
  @Action('CashActor:countPayPwdTime')
  countPayPwdTime(state, payPwdTime) {
    return state.set('payPwdTime', payPwdTime);
  }

  /**
   * 微信昵称
   */
  @Action('CashActor:setNickName')
  setnickname(state, nickName) {
    return state.set('nickName', nickName);
  }

  /**
   * 微信昵称
   */
  @Action('CashActor:setheadImgUrl')
  setheadImgUrl(state, headImgUrl) {
    return state.set('headImgUrl', headImgUrl);
  }


  @Action('cash:minDrawCash')
  setMinDrawCash(state, minDrawCash) {
    return state.set('minDrawCash', minDrawCash);
  }
}
