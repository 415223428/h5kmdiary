import { Store, IOptions, msg } from 'plume2';

import { WMkit, Alert, history, _ } from 'wmkit';
import { Const, config } from 'config';
import CashActor from './actor/cash-actor';
import { isNullOrUndefined } from 'util';
import {
  init,
  addCustomerDrawCash,
  checkCustomerPayPwd,
  getLoginCustomerInfo,
  getLinkedAccountInfo,
  countDrawCashSumByCustId,
  getcashout
} from './webapi';
import moment from 'moment';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }
  submitStatus;

  bindActor() {
    return [new CashActor()];
  }

  /**
   * 初始化
   */
  init = async () => {
    const res = (await init()) as any;
    if (res.code == 'K-000000') {
      const withdrawAmountTotal =
        res.context.withdrawAmountTotal == null
          ? 0
          : res.context.withdrawAmountTotal;
      //获取可提现金额数
      this.dispatch('CashActor:getMoney', withdrawAmountTotal);
    }

    // 查询会员当日已提现余额
    const drawRes = await countDrawCashSumByCustId();
    if (drawRes.code == config.SUCCESS_CODE) {
      //获取可提现金额数
      this.dispatch(
        'cash:form:draw:money',
        drawRes.context ? drawRes.context : 0
      );
    }

    const customerInfo = (await getLoginCustomerInfo()) as any;
    if (customerInfo.code == 'K-000000') {
      this.dispatch(
        'CashActor:countPayPwdTime',
        customerInfo.context.payErrorTime
      );
      if (
        !customerInfo.context.payLockTime ||
        (customerInfo.context.payLockTime &&
          moment(
            moment(customerInfo.context.payLockTime).format(
              Const.SECONDS_FORMAT
            )
          )
            .add(30, 'm')
            .isBefore(moment(moment(Date.now()).format(Const.SECONDS_FORMAT))))
      ) {
        this.dispatch('CashActor:checkPayPwdRes', true);
      } else {
        this.dispatch('CashActor:checkPayPwdRes', false);
      }
    }

    const data = (await getLinkedAccountInfo()) as any;
    if (data.code == 'K-000000') {
      const nickName = data.context.nickname;
      const headImgUrl = data.context.headimgurl;
      this.transaction(() => {
        this.dispatch('CashActor:setNickName', nickName);
        this.dispatch('CashActor:setheadImgUrl', headImgUrl);
        this.dispatch('CashActor:changeCustomerDrawCashInfo', {
          key: 'drawCashAccountName',
          value: nickName
        });
      });
    }
  };

  /**
   * 提现申请单提交点击事件
   */
  paymentPass = async (val) => {
    this.dispatch('CashActor:passWordShow', val);
  };
  /**
   * 支付密码取消点击事件
   */
  cancelLayer = async (val) => {
    this.dispatch('CashActor:passWordShow', val);
    this.changePayPwd('');
  };

  /**
   * 修改提现申请内容
   */
  changeCustomerDrawCashInfo = async (key, value) => {
    this.dispatch('CashActor:changeCustomerDrawCashInfo', {
      key: key,
      value: value
    });
  };

  /**
   * 新增申请记录
   */
  addCustomerDrawCash = WMkit.onceFunc(async () => {
    let customerDrawCashInfo = this.state()
      .get('customerDrawCashInfo')
      .toJS();
    //检验当天提现金额是否超出限制
    if (!(await this.checkCountDrawCashSum(customerDrawCashInfo.drawCashSum))) {
      return;
    }
    //校验输入密码
    let checkPayPwd = await this.checkCustomerPayPwd();
    if (!checkPayPwd) {
      this.dispatch('CashActor:checkPayPwdRes', false);
      await this.getLoginCustomerInfo();
      return;
    } else {
      this.dispatch('CashActor:checkPayPwdRes', true);
    }

    //检验账户可提现金额
    let checkFlag = await this.checkDrawCashSum(
      customerDrawCashInfo.drawCashSum
    );
    if (!checkFlag) {
      Alert({
        text: '余额不足，请修改提现金额！'
      });
      return;
    }
    customerDrawCashInfo.openId = WMkit.wechatOpenId().openid;
    // 微信openId来源 0:PC 1:MOBILE 2:App
    var userAgent = navigator.userAgent.toLowerCase(), //获取userAgent
     isInapp = userAgent.indexOf("kmsdapi")>=0;//查询是否有相关app的相关字段
     console.log(userAgent,'userAgent__++++');
     console.log(JSON.stringify(customerDrawCashInfo));
     let appOpenId = localStorage.getItem('appOpenId');
     console.log(appOpenId);
     if(isInapp){
       customerDrawCashInfo.drawCashSource = 2;
       customerDrawCashInfo.appOpenId = appOpenId;
    }else{
      customerDrawCashInfo.drawCashSource = 1;
     }
     var noOpenId = isNullOrUndefined(customerDrawCashInfo.appOpenId)||customerDrawCashInfo.appOpenId=='undefined'
     if(noOpenId&&isInapp){
      Alert({
        text: '为了您的账号安全，请在app中使用微信登录并且使用最新版本app'
      });
      return
     }
    const res = (await addCustomerDrawCash(customerDrawCashInfo)) as any;
    if (res.code == 'K-000000') {
      if (!WMkit.isLoginOrNotOpen()) {
        msg.emit('loginModal:toggleVisible', {
          callBack: () => {
            history.push(
              '/balance/successful/' + res.context.customerDrawCashVO.drawCashId
            );
          }
        });
      } else {
        history.push(
          '/balance/successful/' + res.context.customerDrawCashVO.drawCashId
        );
      }
    } else {
      Alert({
        text: res.message,
        time: 1000
      });
    }
  }, null);

  /**
   * 提交时校验可提现余额
   * @param drawCashSum
   */
  checkDrawCashSum = async (drawCashSum) => {
    const res = (await init()) as any;
    if (res.code == 'K-000000') {
      const withdrawAmountTotal = res.context.withdrawAmountTotal;
      if (drawCashSum > withdrawAmountTotal) {
        return false;
      } else {
        return true;
      }
    }
  };

  /**
   * 输入密码修改
   * @param payPwd
   */
  changePayPwd = async (payPwd) => {
    this.dispatch('CashActor:changePayPwd', payPwd);
  };

  /**
   * 校验输入支付密码
   * @param payPassword
   */
  checkCustomerPayPwd = async () => {
    let payPassword = this.state().get('payPwd');
    if (payPassword == '') {
      Alert({
        text: '输入密码不能为空！'
      });
      return false;
    }
    const res = await checkCustomerPayPwd(payPassword);
    if (res.code == 'K-000000') {
      return true;
    } else {
      Alert({
        text: res.message,
        time: 1000
      });
      return false;
    }
  };

  /**
   * 获取当前登陆人信息
   */
  getLoginCustomerInfo = async () => {
    const res = (await getLoginCustomerInfo()) as any;
    if (res.code == 'K-000000') {
      this.dispatch('CashActor:countPayPwdTime', res.context.payErrorTime);
    }
  };

  /**
   * 检验当天提现金额是否超出限制
   */
  checkCountDrawCashSum = async (drawCashSum) => {
    //检验当天提现金额是否超出限制
    let res = (await countDrawCashSumByCustId()) as any;
    if (res.code == config.SUCCESS_CODE) {
      if (res.context) {
        // 即将提现金额+已提现金额
        drawCashSum = _.add(drawCashSum, res.context);
      }
      if (drawCashSum > Const.MAX_DRAW_CASH) {
        Alert({
          text: '提现金额超出当天最大提现金额',
          time: 1000
        });
        return false;
      } else {
        return true;
      }
    } else {
      Alert({
        text: res.message,
        time: 1000
      });
      return false;
    }
  };

  setCashOut = async ()=>{
    const res = (await getcashout()) as any;
    if(res.code === 'K-000000'){
      this.dispatch('cash:minDrawCash',res.context)
    }
  }
}
