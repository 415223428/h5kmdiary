import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const style = require('./css/style.css');
import CashForm from './component/cash-form';
import PaymentPassword from './component/payment-password';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class ApplicatForm extends Component<any, any> {
  store: AppStore;
  componentWillMount() {
    this.store.init();
    this.store.setCashOut();
  }
  componentDidMount(){
  }
  render() {
    let visible = this.store.state().get('visible');
    return (
      <div className="container-div">
        {/*这条是正式代码*/}
        {visible ? <PaymentPassword /> : <CashForm />}
      </div>
    );
  }
}
