import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import { fromJS } from 'immutable';

import { Blank, ListView } from 'wmkit';
const style = require('./css/style.css');

import AppStore from './store';
import Header from './component/header';
import List from './component/list';
import CancelModal from './component/appaly-layer';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class Balance extends Component<any, any> {
  store: AppStore;

  render() {
    let form = (this.store.state().get('form') || fromJS([])).toJS();
    let fetchDrawCashs = this.store.fetchDrawCashs;
    // 取消提现申请弹窗的显隐
    let cancelModalVisible = this.store.state().get('cancelModalVisible');

    return (
      <div className="cash-record">
        <Header />
        <ul className="cash-list">
          <ListView
            url="/draw/cash/page"
            params={form}
            style={{ height: window.innerHeight - 88 }}
            renderRow={(customerDrawCash: object, index: number) => {
              return <List drawCash={customerDrawCash} index={index} />;
            }}
            renderEmpty={() => (
              <Blank
                img={require('./img/list-none.png')}
                content="您暂时还没有提现申请哦"
                isToGoodsList={false}
              />
            )}
            onDataReached={(res) => fetchDrawCashs(res)}
          />
          {cancelModalVisible && <CancelModal />}
        </ul>
      </div>
    );
  }
}
