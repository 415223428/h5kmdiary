import { Fetch } from 'wmkit';
type TResult = { code: string; message: string; context: any };

/**
 * 获取全部提现单列表
 * @returns {Promise<Result<TResult>>}
 */
export const init = (params) => {
  return Fetch('/draw/cash/page', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 取消单个提现单
 * @returns {Promise<Result<TResult>>}
 */
export const cancelDrawCash = (drawCashId) => {
  return Fetch(`/draw/cash/cancel/${drawCashId}`);
};
