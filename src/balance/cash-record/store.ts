import { Store, IOptions, msg } from 'plume2';

import { fromJS } from 'immutable';
import { Alert, Confirm, history, WMkit } from 'wmkit';
import { config } from 'config';
import { init, cancelDrawCash } from './webapi';
import DrawCashActor from './actor/draw-cash-actor';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new DrawCashActor()];
  }

  onTabChange = (id) => {
    this.dispatch('top:active', id);
    this.init();
  };

  /**
   * 设置列表
   * @param res
   */
  fetchDrawCashs = (res: any) => {
    if (res.context) {
      this.dispatch(
        'draw-cash-list-form:setDataList',
        fromJS(res.context.content)
      );
    }
  };

  /**
   * 初始化
   */
  init = async () => {
    const key = this.state().get('key');
    this.dispatch('form:audit:status', null);
    this.dispatch('form:finish:status', null);
    this.dispatch('form:customer:operate:status', null);
    this.dispatch('form:draw:cash:status', null);
    if (key === 1) {
      // 待审核：待审核，包括后台审核后提现失败的单子
      this.dispatch('form:audit:status', 0);
    } else if (key === 2) {
      this.dispatch('form:audit:status', 1);
    } else if (key === 3) {
      this.dispatch('form:finish:status', 1);
    }
    if (key != 0) {
      this.dispatch('form:customer:operate:status', 0);
    }
    const params = this.state().get('form');
    const data = (await init(params)) as any;
    if (data.code == 'K-000000') {
      const dataList = data.context.customerDrawCashVOPage.content;
      this.transaction(() => {
        this.dispatch('draw:cash:list:fetch', dataList);
      });
    } else {
      Confirm({
        text: data.message,
        okBtn: '确定',
        confirmCb: () => history.push('/balance/home')
      });
    }
  };

  /**
   * 取消提现记录时，取消弹窗的显隐
   * @param flag
   */
  onCancelModalVisible = (flag) => {
    this.dispatch('draw:cash:modal:visible', flag);
  };

  /**
   * 点击取消提现按钮时，控制弹窗显示并存下需要取消的提现申请ID
   * @param drawCashId
   */
  onShowCancelModal = (drawCashId) => {
    this.dispatch('draw:cash:id', drawCashId);
    this.onCancelModalVisible(true);
  };

  /**
   * 弹窗隐藏事件
   */
  onHideCancelModal = () => {
    this.dispatch('draw:cash:id', '');
    this.onCancelModalVisible(false);
  };

  /**
   * 取消提现申请
   * @param drawCashId
   * @returns {Promise<void>}
   */
  cancelDrawCash = async (drawCashId) => {
    const data = (await cancelDrawCash(drawCashId)) as any;
    if (data.code == config.SUCCESS_CODE) {
      Alert({ text: '操作成功', cb: () => location.reload() });
      this.onHideCancelModal();
    } else {
      Confirm({
        text: data.message,
        okBtn: '确定',
        confirmCb: () => history.push('/balance/cash-record')
      });
    }
  };
}
