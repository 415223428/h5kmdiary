import React from 'react';
import { Relax } from 'plume2';
import { Link } from 'react-router-dom';
import { noop } from 'wmkit';

@Relax
export default class AppalyLayer extends React.Component<any, any> {
  props: {
    relaxProps?: {
      onHideCancelModal: Function;
      drawCashId: string;
      cancelDrawCash: Function;
    };
  };

  static relaxProps = {
    onHideCancelModal: noop,
    drawCashId: 'drawCashId',
    cancelDrawCash: noop
  };
  render() {
    const {
      onHideCancelModal,
      drawCashId,
      cancelDrawCash
    } = this.props.relaxProps;

    return (
      <div className="black-layer">
        <div className="white-layer address-box">
          <span className="payment-title">是否确认取消当前提现申请</span>
          <div className="down-btn">
            <div
              className="left-btn"
              onClick={() => onHideCancelModal(false)}
            >
              再想想
            </div>
            <div
              className="right-btn"
              onClick={() => {
                cancelDrawCash(drawCashId);
              }}
            >
              确定
            </div>
          </div>
        </div>
      </div>
    );
  }
}
