import React from 'react';
import { Link } from 'react-router-dom';
import { _, history, WMkit, noop } from 'wmkit';
import { msg, Relax } from 'plume2';
import { IList } from '../../../../typings/globalType';

@Relax
export default class List extends React.Component<any, any> {
  props: {
    relaxProps?: {
      dataList: IList;
      pageSize: number;
      pageNum: number;
      total: number;
      onShowCancelModal: Function;
    };
    drawCash: any;
    index: number;
  };

  static relaxProps = {
    dataList: 'dataList',
    pageSize: 'pageSize',
    pageNum: 'pageNum',
    total: 'total',
    onShowCancelModal: noop
  };

  render() {
    const { drawCash } = this.props;
    const { onShowCancelModal } = this.props.relaxProps;
    let statusTxt = '';
    if (drawCash.customerOperateStatus == 1) {
      statusTxt = '已取消';
    } else if (drawCash.customerOperateStatus == 0) {
      if (drawCash.auditStatus == 0) {
        statusTxt = '待审核';
      } else if (drawCash.auditStatus == 1) {
        statusTxt = '已打回';
      } else if (drawCash.auditStatus == 2 && drawCash.drawCashStatus == 2) {
        statusTxt = '已完成';
      } else if (drawCash.auditStatus == 2 && drawCash.drawCashStatus == 1) {
        statusTxt = '待审核';
      }
    }

    return (
      <li >
        <div
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/balance/cash-detail/' + drawCash.drawCashId);
                }
              });
            } else {
              history.push('/balance/cash-detail/' + drawCash.drawCashId);
            }
          }}
        >
          <span className="up-span">
            <span className="order-num">{drawCash.drawCashNo}</span>
            <span className="examine">{statusTxt}</span>
          </span>
          <span className="center-span">
            <span className="date">{_.formatDate(drawCash.applyTime)}</span>
            {drawCash.drawCashSum && (
              <span className="money">¥ {drawCash.drawCashSum.toFixed(2)}</span>
            )}
          </span>
        </div>
        <div className="down-div">
          {drawCash.customerOperateStatus == 0 &&
            drawCash.auditStatus == 0 && (
              <div
                className="btn-div"
                onClick={() => onShowCancelModal(drawCash.drawCashId)}
              >
                取消申请
              </div>
            )}
        </div>
        {/* <div className="bot-line" /> */}
      </li>
    );
  }
}
