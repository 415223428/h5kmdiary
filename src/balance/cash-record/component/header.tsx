import React from 'react';
import { Link } from 'react-router-dom';
import {Relax} from "plume2";
import noop from "wmkit/noop";

@Relax
export default class Header extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { list: '全部', id: 0 },
        { list: '待审核', id: 1 },
        { list: '已打回', id: 2 },
        { list: '已完成', id: 3 }
      ],
      selectId: 0
    };
  }

  static relaxProps = {
    onTabChange: noop
  };

  render() {
    return (
      <ul className="header-ul">
        {this.state.data.map((o, i) => {
          return (
            <li
              key={i}
              className={this.state.selectId == o.id && 'select-li'}
              onClick={() => this._selectLi(o.id)}
            >
              <span>{o.list}</span>
            </li>
          );
        })}
      </ul>
    );
  }


  _selectLi(id) {
    const { onTabChange } = this.props.relaxProps;
    onTabChange(id);
    this.setState({
      selectId: id
    });
  }
}
