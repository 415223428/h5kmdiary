import { Actor, Action, IMap } from 'plume2';
import { List } from 'immutable';

export default class DrawCashActor extends Actor {
  defaultState() {
    return {
      key: 0, // 选中的tab key
      form: {
        // 列表数据
        auditStatus: null, //审核状态 2个
        finishStatus: null, //完成状态 1个
        customerOperateStatus: null //用户操作状态 1个
      },
      dataList: [], //提现单列表
      // 记录提现记录ID，取消提现记录用
      drawCashId: '',
      // 取消提现记录时，取消弹窗的显隐
      cancelModalVisible: false
    };
  }

  /**
   * 设置tab
   */
  @Action('top:active')
  topActive(state: IMap, key: string) {
    return state.set('key', key);
  }

  @Action('form:field')
  changeField(state: IMap, { field, value }) {
    return state.setIn(['form', field], value);
  }

  @Action('form:audit:status')
  changeAuditStatus(state: IMap, value) {
    return state.setIn(['form', 'auditStatus'], value);
  }

  @Action('form:finish:status')
  changeFinishStatus(state: IMap, value) {
    return state.setIn(['form', 'finishStatus'], value);
  }

  @Action('form:customer:operate:status')
  changeCustomerOperateStatus(state: IMap, value) {
    return state.setIn(['form', 'customerOperateStatus'], value);
  }

  @Action('draw:cash:list:fetch')
  drawcashlistfetch(state: IMap, value) {
    return state.set('dataList', value);
  }

  /**
   * 更新数据
   */
  @Action('draw-cash-list-form:setDataList')
  updateDateList(state: IMap, params: Array<any>) {
    return state.update('dataList', (value: List<any>) => {
      return value.push(...params);
    });
  }

  /**
   * 取消提现记录时，取消弹窗的显隐
   * @param {IMap} state
   * @param flag
   * @returns {Map<string, any>}
   */
  @Action('draw:cash:modal:visible')
  setDrawCashCancelModal(state: IMap, flag) {
    return state.set('cancelModalVisible', flag);
  }

  /**
   * 记录提现记录ID，取消提现记录用
   * @param {IMap} state
   * @param drawCashId
   * @returns {Map<string, any>}
   */
  @Action('draw:cash:id')
  setDrawCashCancelId(state: IMap, drawCashId) {
    return state.set('drawCashId', drawCashId);
  }
}
