import { Store, IOptions } from 'plume2';

import { Alert, Confirm, history, _ } from 'wmkit';
import { config } from 'config';
export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [];
  }

  /**
   * 初始化
   */
  init = async () => {};
}
