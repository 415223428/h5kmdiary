import React from 'react';
import history from 'wmkit/history';
import { Relax } from 'plume2';

let defaultScrollTop = 0;
@Relax
export default class SuccesPage extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      drawCashId: this.props.drawCashId
    };
  }

  componentDidMount() {
    // 组件挂载时将defaultScrollTop赋值滚动条的scrollTop
    let latoutNode = document.getElementById('latoutRef');
    if (latoutNode) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    return (
      <div className="succes-div" id="latoutRef">
        <img className="succes-img" src={require('../img/success.png')} />
        <span className="title">提现申请提交成功</span>
        <span className="text">请等待管理员审核</span>
        <div className="btn-bottom">
          <div
            className="left-btn"
            onClick={() => {
              this.gotoCashDetail();
            }}
          >
            查看提现申请
          </div>
          <div className="right-btn" onClick={this._backToMain}>
            返回首页
          </div>
        </div>
      </div>
    );
  }

  /**
   * 返回首页
   * @private
   */
  _backToMain() {
    history.push('/');
  }

  /**
   * 提现详情
   */
  gotoCashDetail() {
    history.push('/balance/cash-detail/' + this.props.drawCashId);
  }
}
