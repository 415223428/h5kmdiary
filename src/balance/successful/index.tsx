import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const style = require('./css/style.css');
import SuccesPage from './component/succes-page';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class Successful extends Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
  }

  render() {
    const { drawCashId } = this.props.match.params;
    return (
      <div className="successful">
        <SuccesPage drawCashId={drawCashId} />
      </div>
    );
  }
}
