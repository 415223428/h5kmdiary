import { Fetch } from 'wmkit'
type TResult = { code: string; message: string ; context: any}

/**
 * 获取余额页面详情
 * @returns {Promise<Result<TResult>>}
 *
 */
export const init = () => {
  return Fetch('/customer/funds/statistics');
}