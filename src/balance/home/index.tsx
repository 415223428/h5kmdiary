import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const style = require('./css/style.css');
import Header from './component/header';
import List from './component/list';
import Withdrawtip from './component/withdraw-tips';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class Balance extends Component<any, any> {
  store: AppStore;

  componentWillMount(): void {
    this.store.init();
  }

  render() {
    return (
      <div>
        {
          this.store.state().get('isWithdrawTip') ?
            <Withdrawtip />
            : null
        }
        <div className="background-fill">
          <Header />
          <List />
        </div>
      </div>
    );
  }
}
