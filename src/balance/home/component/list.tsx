import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMkit } from 'wmkit';
import { msg } from 'plume2';
export default class List extends React.Component<any, any> {
  render() {
    return (
      <ul className="balance-list">
        <li
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/balance/deposit');
                }
              });
            } else {
              history.push('/balance/deposit');
            }
          }}
        >
          <div className="left-div">
            {/* <i className="iconfont icon-tixian" /> */}
            <img src={require('../img/tixian.png')}></img>
            <span className="text">提现</span>
          </div>
          <i className="iconfont icon-jiantou1" />
        </li>

        <li
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/balance/account-detail');
                }
              });
            } else {
              history.push('/balance/account-detail');
            }
          }}
        >
          <div className="left-div">
            {/* <i className="iconfont icon-zhanghumingxi" /> */}
            <img src={require('../img/mingxi.png')}></img>
            <span className="text">账户明细</span>
          </div>
          <i className="iconfont icon-jiantou1" />
        </li>

        <li
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/balance/cash-record');
                }
              });
            } else {
              history.push('/balance/cash-record');
            }
          }}
        >
          <div className="left-div">
            {/* <i className="iconfont icon-tixianjilu" /> */}
            <img src={require('../img/jilu.png')}></img>
            <span className="text">提现记录</span>
          </div>
          <i className="iconfont icon-jiantou1" />
        </li>
      </ul>
    );
  }
}
