import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from "plume2";
import { _, noop } from "wmkit";
@Relax
export default class Header extends React.Component<any, any> {
  props: {
    relaxProps?: {
      amount: IMap;
      changeIsWithdrawTip:Function;
    };
  };

  static relaxProps = {
    amount: 'amount',
    changeIsWithdrawTip:noop,
  };
  render() {
    const { amount ,changeIsWithdrawTip} = this.props.relaxProps;
    const accountBalanceTotal = amount.get('accountBalanceTotal') ? amount.get('accountBalanceTotal') : 0;
    const blockedBalanceTotal = amount.get('blockedBalanceTotal') ? amount.get('blockedBalanceTotal') : 0;
    const alreadyDrawAmount = amount.get('alreadyDrawAmount') ? amount.get('alreadyDrawAmount') : 0;
    const question = require('../img/question.png')
    return (
      <div className="header1">
        <div className="red-bj" >
          {/* <div className="question"
          onClick={()=>{
            changeIsWithdrawTip();
          }}
          ><img src={question} alt=""></img></div> */}
          <span className="money">
            {
              accountBalanceTotal ?
                <div>
                  <span className="unit">￥</span>
                  <span className="number">{_.fmoney(accountBalanceTotal, 2)}</span>
                </div>
                :
                <div>
                  <span className="unit">￥</span>
                  <span className="number">0.00</span>
                </div>
            }
          </span>
          <span className="cash-balance">当前余额</span>
          <ul className="settle-ul">
            <li className="li-box">
              <span className="rmb">￥{_.fmoney(blockedBalanceTotal, 2)}</span>
              <span className="name">待入账余额</span>
            </li>
            <div className="line-center">
              <li className="line" style={{ background: '#999' }} />
            </div>
            <li className="li-box">
              <span className="rmb">￥{_.fmoney(alreadyDrawAmount, 2)}</span>
              <span className="name">已提现余额</span>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
