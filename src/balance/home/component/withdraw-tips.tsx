import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from "plume2";
import { _, history, noop } from "wmkit";
@Relax
export default class WithdrawTip extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeIsWithdrawTip:Function;
    };
  };

  static relaxProps = {
    changeIsWithdrawTip:noop,
  };
  render() {
    const { changeIsWithdrawTip } = this.props.relaxProps;
    const button = require('../img/button.png')
    const cancel = require('../img/cancel.png')
    const tip = require('../img/tip.png')
    return (
      <div className='drawwith-tip'>
        <div className='modal-box' 
        onClick={()=>{
          changeIsWithdrawTip();
        }}
        ></div>
        <div className='tip'>
          <div className='tip-button'
            onClick={()=>{
              history.push('/balance/deposit')
            }}
          >
            <img src={button}></img>
          </div>
          <div className='tip-cancel'
            onClick={()=>{
              changeIsWithdrawTip();
            }}
          >
            <img src={cancel}></img>
          </div>
        </div>
      </div>
    )
  }
}
