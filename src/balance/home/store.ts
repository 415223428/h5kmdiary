import { Store, IOptions } from 'plume2';

import { Alert, Confirm, history, _ } from 'wmkit';
import { config } from 'config';
import DetailActor from "./actor/actor";
import {init} from "./webapi";
import { fromJS } from 'immutable'
export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new DetailActor()];
  }

  /**
   * 初始化
   */
  init = async () => {
    const data = await init() as any;
    if (data.code == 'K-000000') {
      const amount = data.context;
      this.transaction(() => {
        this.dispatch('set:amount',fromJS(amount))
      })
    } else {
      Confirm({
        text: data.message,
        okBtn: '确定',
        confirmCb: () => history.push('/user-center')
      })
    }

  };

  changeIsWithdrawTip=()=>{
    this.dispatch('actor:withdrawtip')
  }
}
