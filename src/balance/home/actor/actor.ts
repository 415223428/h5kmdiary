import { Action, Actor } from 'plume2';
import { fromJS } from 'immutable';
export default class DetailActor extends Actor {
  defaultState() {
    return {
      amount: fromJS({}),
      isWithdrawTip:false
    };
  }

  /**
   * 根据返回的数据填充字段
   */
  @Action('set:amount')
  tipLayerShow(state, val) {
    return state.set('amount', val);
  }
  
  @Action('actor:withdrawtip')
  changewhitdrawtip(state){
    return state.set('isWithdrawTip',!state.get('isWithdrawTip'))
  }
}
