import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const style = require('./css/style.css');
import BindWecat from './component/bind-wecat';
import Cash from './component/cash';
import AppalyLayer from '../cash-detail';
@StoreProvider(AppStore, { debug: __DEV__ })
export default class Balance extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }
  render() {
    let visible = this.store.state().get('visible');
    let loading = this.store.state().get('loading');
    return (
      <div className="container-div">
        {loading == null ? null : visible ? <Cash /> : <BindWecat />}
      </div>
    );
  }
}
