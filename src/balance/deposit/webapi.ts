import { Fetch } from 'wmkit'
type TResult = { code: string; message: string ; context: any}

/**
 * 获取全部提现单列表
 * @returns {Promise<Result<TResult>>}
 *
 */
export const init = () => {
  return Fetch('/third/login/linked-account-flags');
}