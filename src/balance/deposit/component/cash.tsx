import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMkit, _ ,Alert} from 'wmkit';
import { msg } from 'plume2';
export default class Cash extends React.Component<any, any> {
  render() {
    return (
      <div>
        <div className="cash">提现至</div>
        <ul className="wecat-wallet">
          <li
            onClick={() => {
              var userAgent = navigator.userAgent.toLowerCase(), //获取userAgent
              noInapp = userAgent.indexOf("kmsdapi")==-1;//查询是否有相关app的相关字段
              if (!WMkit.isLoginOrNotOpen()) {
                if(!(_.isWeixin())&&noInapp){
                  Alert({
                    text: '请到APP或用微信浏览器打开商城提现'
                  });
                  return
                }
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push('/balance/applicat-form');
                  }
                });
              } else {
                if(!_.isWeixin()&&noInapp){
                  Alert({
                    text: '请到APP或用微信浏览器打开商城提现'
                  });
                  return
                }
                history.push('/balance/applicat-form');
              }
            }}
          >
            <div className="left-div">
              <img className="wecat-icon" src={require('../img/wechat.png')} />
              <span className="text">微信钱包</span>
            </div>
            <i className="iconfont icon-jiantou1" />
          </li>
        </ul>
        <div className="tips">
          微信提现：审核过后，将会提现至您账号绑定的微信钱包中，请确保您的微信开通钱包功能及完成实名认证。
        </div>
      </div>
    );
  }
}
