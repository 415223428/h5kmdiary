import React from 'react';
import { Link } from 'react-router-dom';
import {WMkit,history} from "wmkit";
import {msg, Relax} from "plume2";
@Relax
export default class BindWecat extends React.Component<any, any> {
  render() {
    return (
      <div className="black-div ">
        <div className="white-div address-box">
          <span className="text">
            您的账号未绑定微信，请至账户安全-关联账号
          </span>
          <div className="sure-btn"
               onClick={() => {
                 if (!WMkit.isLoginOrNotOpen()) {
                   msg.emit('loginModal:toggleVisible', {
                     callBack: () => {
                       history.push('/linked-account');
                     }
                   });
                 } else {
                   history.push('/linked-account');
                 }
               }}>确定</div>
        </div>
      </div>
    );
  }
}
