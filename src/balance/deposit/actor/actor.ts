import {Action, Actor} from 'plume2';
export default class DetailActor extends Actor {
  defaultState() {
    return {
      visible: false,
      loading: null
    }
  }

  /**
   * 提示框是否显示
   */
  @Action('show:no:bind:tip')
  tipLayerShow(state, val) {
    return state.set('visible', val);
  }

  /**
   * 提示框是否显示
   */
  @Action('show:loading')
  loading(state, val) {
    return state.set('loading', val);
  }
}
