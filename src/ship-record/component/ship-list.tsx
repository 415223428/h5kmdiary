import * as React from 'react'
import { Link } from 'react-router-dom'

import { Relax } from 'plume2'
import { fromJS } from 'immutable'

import { Button, history } from 'wmkit'

import AppStore from '../store'
const SmallBlueButton = Button.SmallBlue
const noneImg = require('../img/none_img.png')

@Relax
export default class ShipList extends React.Component<any, any> {

  store: AppStore


  props: {
    relaxProps?: {
      orderId: string,
      tradeDilivery: any,
      type: string

    }
  }


  static relaxProps = {
    orderId: 'orderId',
    tradeDilivery: 'tradeDilivery',
    type: 'type'
  }


  render() {
    return (
      <div className="order-list" style={{ padding: '10px 5px' }}>
        {
          this.props.relaxProps.tradeDilivery.map((v) => {
            let total = 0;
            const skuIdSet = fromJS(v.shippingItems).concat(fromJS(v.giftItemList)).map(item => {
              total += item.get('itemNum');
              return item.get('skuId');
            }).toSet();

            return (
              <div className="ships">
                <div className="order-item address-box" style={{ borderTop: "0px", padding: '0px' }}>

                  <div className="limit-img ship-img" style={{ padding: 0, border: 'none', height: '2rem'  ,borderBottom:"1px solid rgba(229,229,229,1)"}}
                    onClick={() => this._toShipList(v.deliverId)}>
                    <div className="img-content">
                      {
                        v.shippingItems.concat(v.giftItemList).map((item, index) => {
                          return index < 4 ?
                            <div style={{ flexDirection: 'row', display: 'flex' }}>
                              <img className="img-item" src={item.pic ? item.pic : noneImg} style={{ width: '1.6rem', height: '1.6rem' }} />
                              {v.shippingItems.concat(v.giftItemList).length == 1 &&
                                <div style={{
                                  flexDirection: 'column',
                                  display: 'flex',
                                  justifyContent: 'center',
                                  fontSize: '0.26rem',
                                  color: '#666',
                                  marginLeft: '0.21rem'
                                }}>
                                  <span>{item.itemName}</span>
                                  <span>{item.specDetails}</span>
                                </div>
                              }
                            </div>
                            : null
                        })
                      }
                    </div>
                    <div className="right-context">
                      <div className="total-num" style={{ color: '#333' }}>共{skuIdSet.size}种<br />共{total}件</div>
                      <i className='iconfont icon-jiantou1' />
                    </div>
                  </div>
                  <div className="ship-status">
                    <p style={{ height: '.84rem',marginBottom:'0rem',padding:'0 .2rem',borderBottom:'1px solid rgba(229,229,229,1)' }}>
                      <span className="name">{v.logistics.logisticCompanyName}</span>
                      <span
                        className="grey">{v.logistics.logisticNo}</span>
                    </p>
                    <p style={{height: '.84rem',marginBottom:'0rem',padding:'0 .2rem',borderBottom:'1px solid rgba(229,229,229,1)'}}>
                      <span className="name">发货时间</span>
                      <span className="grey">
                        {v.deliverTime ? v.deliverTime.split(" ")[0] : "物流更新中"}
                      </span>
                    </p>
                  </div>
                  <div className="ship-bottom " style={{height:"1.2rem" ,padding:'0 .2rem'}}>
                    {/* <SmallBlueButton text={'物流信息'} onClick={() => this._toLogisticInfo(v.deliverId)} /> */}
                    <button
                      onClick={() => this._toLogisticInfo(v.deliverId)}
                      className="btn-color-gray-small"
                    >查看物流</button>
                  </div>
                </div>
                {/* <div className="bot-line"></div> */}
              </div>
            )
          })
        }
      </div>
    )
  }

  _toShipList = (deliverId) => {
    const { orderId } = this.props.relaxProps
    history.push(`/ship-list/${orderId}/${deliverId}`)
  }


  _toLogisticInfo = (deliverId) => {
    const { orderId, type } = this.props.relaxProps
    history.push(`/logistics-info/${orderId}/${deliverId}/${type}`)
  }
}
