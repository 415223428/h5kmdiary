import {Store} from 'plume2'
import {Confirm, Alert} from  'wmkit'
import RecordActor from "./actor/record-actor";
import  * as webapi from  './webapi'
/**
 * Created by feitingting on 2017/7/24.
 */
export  default class AppStore extends Store {


  bindActor() {
    return [
      new RecordActor
    ]
  }


  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  init = async (id: string, type:string) => {
    const res = await webapi.fetchAllDeliverys(id,type)
    if (res.code == 'K-000000') {
      this.dispatch('record:init', (res.context as any).tradeDeliver);
      this.dispatch('record:type', type);

    }
    if ((res.context as any).status == 'DELIVERED') {
      this.transaction(() => {
        this.dispatch('record:deliveryStatus', true)
      })
    }
  }


  orderId = (id: string) => {
    this.dispatch('record:orderId', id)
  }


  onConfirm = async (id: string) => {
    Confirm({
      title: '确认收货',
      text: '确认已收到全部货品？',
      cancelBtn: '取消',
      okBtn: '确定'
      , confirmCb: () => this.confirmReceive(id)
    })
  }


  confirmReceive = async (id: string) => {
    const {code, context, message} = await  webapi.confirmReceiveAll(id)
    if (code == "K-000000") {
      Alert({
        text: "收货成功！"
      })
      this.dispatch('record:deliveryStatus', false)
    } else {
      Alert({
        text: '操作失败' + ',' + '记录不存在'
      })
    }
  }
}
