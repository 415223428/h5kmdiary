import React from 'react';
import { StoreProvider } from 'plume2';
import { Button } from 'wmkit';
import AppStore from './store';
import FormItem from './component/form-item';
import TipBox from './component/tip-box';
import Result from './component/result';

const SubmitButton = Button.LongBlue;
@StoreProvider(AppStore, { debug: __DEV__ })
export default class ImproveInformation extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '账户信息';
    const { cid } = (this.props as any).match.params;
    this.store.init(cid);
  }

  /*  componentDidMount() {
    /!**
     * complete控制显示完善信息表单页还是显示提交以后的提示页
     * result控制显示是否审核通过和是否提交成功
     * 两个标识相结合控制四种不同的显示情况*!/
    const complete = this.props.location.complete
    const result = this.props.location.result
    const customerId = this.props.location.customerId
    const customerDetail = this.props.location.customerDetail
    if (__DEV__) {
    }
    if (complete == false && result == false) {
      this.store.start(complete, result, customerId, customerDetail)
    } else {
      this.store.init(complete, result, customerId)
    }
  }*/

  constructor(props) {
    super(props);
  }

  render() {
    const checked = this.store.state().get('checked');
    const initialName = this.store.state().get('initialName');
    return (
      <div>
        <div
          className={
            checked == 0 && initialName != null && initialName != ''
              ? 'hide'
              : 'showblock'
          }
        >
          <div className="content register-content">
            <div style={{ height: 'calc(100vh - 64px)', overflowY: 'auto' }}>
              <TipBox />
              <FormItem />
            </div>
            <div className="improve-btn">
              <SubmitButton text="提交" onClick={() => this.store.doPerfect()}>
                提交
              </SubmitButton>
            </div>
          </div>
        </div>
        <div
          className={
            checked == 0 && initialName != null && initialName != ''
              ? 'showblock'
              : 'hide'
          }
        >
          <Result />
        </div>
      </div>
    );
  }
}
