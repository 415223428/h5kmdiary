import React from 'react'
import { IMap, Relax, Store } from "plume2";
import PropTypes from 'prop-types'
import { noop, Picker, FormSelect, FindArea } from 'wmkit'

@Relax
export default class FormItem extends React.Component<any, any> {

  store: Store


  props: {
    relaxProps?: {
      customerDetail: IMap
      getCompanyName: Function,
      getAddressDetail: Function,
      getContact: Function,
      getContactTel: Function,
      getArea: Function
    }
  };


  static relaxProps = {
    customerDetail: 'customerDetail',
    getCompanyName: noop,
    getAddressDetail: noop,
    getContact: noop,
    getContactTel: noop,
    getArea: noop
  }


  static contextTypes = {
    _plume$Store: PropTypes.object
  }


  constructor(props, ctx) {
    super(props)
    this.store = ctx['_plume$Store']

  }


  render() {
    const customerDetail = this.store.state().get('customerDetail')
    const customerName = customerDetail.get('customerName')
    const customerAddress = customerDetail.get('customerAddress')
    const contactName = customerDetail.get('contactName')
    const contactPhone = customerDetail.get('contactPhone')
    const { getCompanyName, getAddressDetail, getContact, getContactTel, getArea } = this.props.relaxProps
    let provinceId = customerDetail.get("provinceId")
    let cityId = customerDetail.get("cityId")
    let areaId = customerDetail.get("areaId")
    let area = provinceId ? cityId ? [provinceId.toString(), cityId.toString(), areaId.toString()] : [provinceId.toString()] : []
    //拼接省市区名字
    let areaName = FindArea.addressInfo(provinceId, cityId, areaId)
    //对应显示的样式
    let textStyle = provinceId ? { color: '#333' } : {}
    return (
      <div>
        <div className="row form-item">
          <span className="form-text"><i>*</i>称呼/公司名称</span>
          <input placeholder="请填写您的称呼或您公司的名称" type="text" className="form-input" value={customerName}
            maxLength={15} onChange={(e) => getCompanyName(e.target.value)} />
        </div>
        <Picker
          extra="所在地区"
          title="选择地址"
          format={(values) => {
            return values.join('/');
          }}
          value={area}
          className="addr-picker"
          onOk={(val) => getArea(val)}
        >
          <FormSelect labelName="选择地址" placeholder="请选择您所在的地区" textStyle={textStyle}
            selected={{ 'key': area, 'value': areaName }} />
        </Picker>
        <div className="row form-item">
          <span className="form-text">详细地址</span>
          <input placeholder="请填写您的详细地址" type="text" className="form-input" value={customerAddress}
            maxLength={60} onChange={(e) => getAddressDetail(e.target.value)} />
        </div>
        <div className="row form-item">
          <span className="form-text"><i>*</i>常用联系人</span>
          <input placeholder="请填写一位常用联系人" type="text" className="form-input" value={contactName}
            maxLength={15} onChange={(e) => getContact(e.target.value)} />
        </div>
        <div className="row form-item">
          <span className="form-text"><i>*</i>常用联系人手机号</span>
          <input placeholder="请填写联系人常用手机号" type="text" className="form-input" value={contactPhone}
            maxLength={11} onChange={(e) => getContactTel(e.target.value)} />
        </div>
      </div>
    )
  }
}
