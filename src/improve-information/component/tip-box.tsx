import React from 'react'
import { Relax } from "plume2";
import AppStore from "../store";
const icon = require('./img/icon.png')
const failed = require('./img/failed.png')
const styles = require('../css/style.css')

@Relax
export default class TipBox extends React.Component<any, any> {


  store: AppStore


  props: {
    relaxProps?: {
      checked: number,
      initialName: string,
      rejectReason: string,
    }
  }


  static relaxProps = {
    checked: 'checked',
    initialName: 'initialName',
    rejectReason: 'rejectReason',
  }


  constructor(props: any) {
    super(props)
  }


  render() {
    const { checked, initialName, rejectReason } = this.props.relaxProps
    return (
      <div className="tip-box">
        <img src={checked == 2 ? failed : icon} className={styles.img} />
        {
          checked == 2 ?
            <div className={styles.rightCon}>
              <p className={styles.darktext}>您提交的账户信息审核未通过！</p>
              <p className={styles.grey}>原因是：<span className={styles.blue}>{rejectReason}</span></p>
              <p className={styles.grey}>请您修改账户基本信息重新提交</p>
            </div> :
            <span className={styles.text}>您还需完善账户信息才可正常访问商品信息</span>
        }
      </div>
    )
  }
}
