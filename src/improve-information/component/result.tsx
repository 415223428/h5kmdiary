import React from 'react'
import {Relax} from "plume2";
import  AppStore from '../store'
const failed = require('./img/result-failed.png')
const sucess = require('./img/result-suc.png')

@Relax
export default class Result extends React.Component<any, any> {


  store: AppStore


  props: {
    relaxProps?: {
      checked: number,
      minutes: number,
    }
  }


  static relaxProps = {
    checked: 'checked',
    minutes: 'minutes',
  }


  constructor(props) {
    super(props)
  }


  render() {
    const {checked, minutes} = this.props.relaxProps
    return (
      <div className="list-none">
        <img src={ checked == 0 ? sucess : failed} width="200"/>

        <div>
          <p >您的账户信息已经提交审核</p>
          <p>请耐心等待。{minutes}s后自动跳转到登录页面……</p>
        </div>
      </div>
    )
  }
}
