/**
 * Created by feitingting on 2017/7/17.
 */
import { Fetch } from 'wmkit';

export const doPerfect = (params = {}) => {
  return Fetch('/perfect', {
    method: 'POST',
    body: JSON.stringify({
      ...params
    })
  });
};

/**
 * 验证手机号检验
 * @param {string} phone
 * @returns {Promise<Result<any>>}
 */
export const checkPhone = (phone: string) => {
  return Fetch(`/ishave/kmaccount/${phone}`, {
    method: 'POST',
  });
};
