/**
 * Created by feitingting on 2017/7/17.
 */
import { IMap, Store, msg } from 'plume2';
import { _, Alert, history, storage, ValidConst, WMkit,Confirm } from 'wmkit';
import { cache, config } from 'config';
import DetailActor from './actor/detail-actor';
import * as webapi from './webapi';

/**校验名称和联系人*/
function _testName(name: string) {
  if (name == '') {
    Alert({
      text: '请填写名称！'
    });
    return false;
  } else if (name.length < 2 || name.length > 15) {
    Alert({
      text: '名称为2-15个字符！'
    });
    return false;
  }
  return true;
}

function _testContact(contact: string) {
  if (contact == '') {
    Alert({
      text: '请填写常用联系人！'
    });
    return false;
  } else if (contact.length < 2 || contact.length > 15) {
    Alert({
      text: '联系人名称为2-15个字符！'
    });
    return false;
  }
  return true;
}

/**校验地址详情*/
function _testAddress(address: string) {
  if (address == '') {
    Alert({
      text: '请填写详细地址！'
    });
    return false;
  } else if (address.length < 5 || address.length > 60) {
    Alert({
      text: '地址长度为5-60个字符！'
    });
    return false;
  }
  return true;
}

/**校验手机号码*/
function _testTel(tel: string) {
  const regex = ValidConst.phone;
  if (tel == '') {
    Alert({
      text: '请填写联系人手机号！'
    });
    return false;
  } else if (!regex.test(tel)) {
    Alert({
      text: '无效的手机号！'
    });
    return false;
  } else {
    return true;
  }
}

export default class AppStore extends Store {
  bindActor() {
    return [new DetailActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  start = (
    complete: boolean,
    result: boolean,
    customerId: string,
    customerDetail: IMap
  ) => {
    this.transaction(() => {
      this.dispatch('detail:complete', complete);
      this.dispatch('detail:result', result);
      this.dispatch('detail:customerId', customerId);
      this.dispatch('detail:customerDetail', customerDetail);
    });
  };

  /**
   * 初始化，查询会员基本信息
   * @returns {Promise<void>}
   */
  init = async (id: string) => {
    const res = storage('local').get(cache.PENDING_AND_REFUSED);
    //如果存在，更改用户ID的状态值
    this.dispatch('detail:customerId', id);
    if (res) {
      const customer = JSON.parse(res).customerDetail;
      this.transaction(() => {
        this.dispatch('detail:customer', customer);
        this.dispatch('detail:checked', JSON.parse(res).checkState);
      });
    }
    this.startTime();
  };

  startTime = () => {
    //正在审核中，开启倒计时，自动跳转到登录页
    if (
      this.state().get('checked') == 0 &&
      this.state().get('initialName') != null &&
      this.state().get('initialName') != ''
    ) {
      this.setTime();
    }
  };

  setTime = () => {
    const start = this.state().get('minutes');
    const time = start - 1;
    if (time > 0) {
      this.dispatch('detail:time', time);
      setTimeout(() => {
        this.setTime();
      }, 1000);
    } else {
      WMkit.clearLoginCache();
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          history.push('/');
        }
      });
      history.push('/');
    }
  };

  getCompanyName = (name: string) => {
    this.dispatch('detail:companyName', name);
  };

  getAddressDetail = (address: string) => {
    this.dispatch('detail:address', address);
  };

  getContact = (contact: string) => {
    this.dispatch('detail:contact', contact);
  };

  getContactTel = (tel: string) => {
    this.dispatch('detail:contactTel', tel);
  };

  getArea = (value: number[]) => {
    this.dispatch('detail:area', value);
  };

  /**完善账户信息*/
  doPerfect = async () => {
    const customer = this.state()
      .get('customerDetail')
      .toJS();
    /**先校验必填项*/
    if (
      _testName(customer.customerName) &&
      _testContact(customer.contactName) &&
      _testTel(customer.contactPhone)
    ) {
      //不为空，且不是不限
      if (customer.provinceId) {
        if (!_testAddress(customer.customerAddress)) {
          return false;
        }
      }
      if (customer.customerAddress != '') {
        //未选择省份或者选择不限时，都视为未选择
        if (customer.provinceId == '' || customer.provinceId == null) {
          Alert({
            text: '请选择所在地区！'
          });
          return false;
        }
      }
      const { code, message, context } = await webapi.doPerfect(customer);
      if (code == config.SUCCESS_CODE) {
        //清除缓存
        localStorage.removeItem(cache.PENDING_AND_REFUSED);
        //是否直接可以登录 0 否 1 是
        if (!(context as any).isLoginFlag) {
          //注册券弹框
          _.showRegisterModel((context as any).couponResponse, false);
          Alert({
            text: '提交成功，将会尽快给您审核！'
          });
          this.transaction(() => {
            this.dispatch('detail:checked', 0);
            this.dispatch('detail:initialName', customer.customerName);
          });

          this.startTime();
        } else {
          //直接跳转到主页
          localStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
          sessionStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
          const lo = WMkit.messAlter((context as any));
          if (await lo === 1) {
            Confirm({
              text: '您的手机号码未绑定会员编号，请通过会员编号登录进行手机号码绑定，后续方可通过手机号码登录',
              okBtn: '确定',
              confirmCb: ()=>{
                console.log('已经记录了');
                WMkit.recordLogin((context as any).customerId,(context as any).customerAccount)
                WMkit.clearLoginCache();
              },
              maskClose:false
            });

            WMkit.clearLoginCache();
            history.push('/login');
            return;
          }
          Alert({
            text: '登录成功！'
          });
          // if((context as any).sourceType === null){
          //   const checkPhone = await webapi.checkPhone((context as any).customerAccount);
          //   if (checkPhone.code === 'K-000001') {
          //       Alert({
          //         text: '您的手机号码未绑定会员编号，请通过会员编号登录进行手机号码绑定，后续方可通过手机号码登录'
          //       })
          //   }
          // }
          setTimeout(() => {
            history.push('/');
            //注册券弹框
            _.showRegisterModel((context as any).couponResponse, true);
          }, 1000);
        }
      } else {
        Alert({
          text: message
        });
        return false;
      }
    }
  };
}
