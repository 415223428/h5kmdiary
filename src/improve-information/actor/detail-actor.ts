import {Actor, Action, IMap} from "plume2";
/**
 * Created by feitingting on 2017/7/17.
 */
export  default class DetailActor extends Actor {


  defaultState() {
    return {
      //审核状态,0表示正在审核中，2表示审核未通过
      checked:0,
      rejectReason:"",
      minutes:6,
      //添加一个用户名，用来决定提示信息（注册时候未填写，state=2，name为空）
      initialName:"",
      customerDetail: {
        customerId: "",
        customerName: "",
        provinceId: "",
        cityId: "",
        areaId: "",
        customerAddress: "",
        contactName: "",
        contactPhone: "",
      }
    }
  }


  /**
   * 客户详情
   * @param state
   * @param res
   * @returns {any}
   */
  @Action('detail:customer')
  customerDetail(state, res: any) {
    return state
      .setIn(['customerDetail', 'customerName'], (res as any).customerName)
      .setIn(['customerDetail', 'customerAddress'], (res as any).customerAddress)
      .setIn(['customerDetail', 'contactName'], (res as any).contactName)
      .setIn(['customerDetail', 'contactPhone'], (res as any).contactPhone)
      .setIn(['customerDetail', 'provinceId'], (res as any).provinceId)
      .setIn(['customerDetail', 'cityId'], (res as any).cityId)
      .setIn(['customerDetail', 'areaId'], (res as any).areaId)
      .set('initialName', (res as any).customerName)
      .set('rejectReason', (res as any).rejectReason)

  }


  @Action('detail:customerId')
  customerId(state: IMap, id: string) {
    return state.setIn(['customerDetail', 'customerId'], id)
  }




  /**
   * 是否已完善账户信息
   * @param state
   * @param complete
   */
  @Action('detail:complete')
  complete(state, complete: boolean) {
    return state.set('complete', complete)
  }


  /**
   * 是否正确完善账户信息(是否通过审核或者是否提交成功)
   * @param state
   * @param result
   */
  @Action('detail:result')
  result(state, result: boolean) {
    return state.set('result', result)
  }


  /**
   * 初始倒计时，5
   * @param state
   * @param time
   */
  @Action('detail:time')
  time(state, time: number) {
    return state.set('minutes', time)
  }


  /**
   * 获取省市区
   * @param state
   * @param area
   * @returns {any}
   */
  @Action('detail:area')
  getArea(state, area: number[]) {
    const [provinceId, cityId, areaId] = area
    return state.setIn(['customerDetail', 'provinceId'], provinceId)
                .setIn(['customerDetail', 'cityId'], cityId)
                .setIn(['customerDetail', 'areaId'], areaId)
  }


  /**
   * 监听公司名称状态值
   * @param state
   * @param name
   * @returns {any}
   */
  @Action('detail:companyName')
  getCompanyName(state, name: string) {
    return state.setIn(['customerDetail', 'customerName'], name)
  }


  /**
   * 提交成功后，给name一个值，否则页面没法进行倒计时跳转
   * @param state
   * @param name
   */
  @Action('detail:initialName')
  initialName(state,name:string){
    return state.set('initialName',name)
  }

  /**
   * 监听地址状态值
   * @param state
   * @param address
   * @returns {any}
   */
  @Action('detail:address')
  getAddress(state, address: string) {
    return state.setIn(['customerDetail', 'customerAddress'], address)
  }


  /**
   * 联系人状态值
   * @param state
   * @param contact
   * @returns {any}
   */
  @Action('detail:contact')
  getContact(state, contact: string) {
    return state.setIn(['customerDetail', 'contactName'], contact)
  }


  /**
   * 联系人电话状态值
   * @param state
   * @param tel
   * @returns {any}
   */
  @Action('detail:contactTel')
  getContactTel(state, tel: string) {
    return state.setIn(['customerDetail', 'contactPhone'], tel)
  }


  @Action('detail:checked')
  checked(state,checked:number){
    return state.set('checked',checked)
  }
}

