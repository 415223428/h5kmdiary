import { Actor, Action } from 'plume2'
import { fromJS, Map } from 'immutable'
import { IMap ,IList} from 'typings/globalType'
export default class GoodsActor extends Actor {
  defaultState() {
    return {
      goodsList: [],
      advList: [],
      title: '',
      fullBannerImages:[],
      fullBannerUrl:'',
      fullBannertime: 5,
      fullBannerSwitchFlag:false,
      bannerSwitchFlag:false,
      bannerImages: [],
      bannerUrl:'',
      openFlag:1,
      flashOpenFlag:1,
      goodsCount:2,
      flashGoodsCount:2,
    }
  }

  /**
   * 商品列表
   * @param state
   * @param res
   * @returns {Map<string, V>}
   */
  @Action('goodsActor:goodsList')
  goodsList(state: IMap, res) {
    return state.set('goodsList', fromJS(res));
  }
  @Action('imgShow:imgShow')
  ifShowImg(state: IMap, isShow) {
    return state.set('ifShow', isShow);
  }
  @Action('goods:bannerSwitchFlag')
  bannerSwitchFlag(state, bannerSwitchFlag) {
    return state.set('bannerSwitchFlag', bannerSwitchFlag);
  }


  @Action('goodsActor:title')
  title(state: IMap, title) {
    return state.set('title', title);
  }
  /**
   * 广告列表
   * @param state
   * @param res
   * @returns {Map<string, V>}
   */
  @Action('goodsActor:advList')
  advList(state: IMap, res) {
    return state.set('advList', fromJS(res));
  }


  //闪屏设置
  @Action('shanPing:init')
  init(state) {
    return state
      .set('fullBannerImages',state.get('fullBannerImages'))
      .set('fullBannertime', state.get('fullBannertime'))
      .set('fullBannerUrl', state.get('fullBannerUrl'))
      .set('fullBannerSwitchFlag',state.get('fullBannerSwitchFlag'))
      .set('bannerSwitchFlag',state.get('bannerSwitchFlag'))
      .set('bannerUrl',state.get('bannerUrl'))
      .set('bannerImages',state.get('bannerImages'))
  }

  // 设置倒计时
  @Action('init:time')
  setTime(state, fullBannertime) {
    return state.set('fullBannertime', fullBannertime);
  }

   // 设置背景logo
   @Action('init:logo')
   setLogo(state, res) {
     console.log(res.fullBannerImages);
     
     return state
     
    //  .set('fullBannerImages', JSON.parse( res.fullBannerImages))
    //  .set('fullBannertime', res.fullBannertime)
     .set('fullBannerUrl', res.fullBannerUrl)
     .set('fullBannerSwitchFlag', res.fullBannerSwitchFlag)
     .set('bannerSwitchFlag', res.bannerSwitchFlag)
     .set('bannerUrl', res.bannerUrl)
    //  .set('bannerImages',JSON.parse( res.bannerImages) )

   }

   //关闭闪屏
   @Action('shanPing:fullBannerSwitchFlag')
   setisShowImg(state,fullBannerSwitchFlag){
    return state.set('fullBannerSwitchFlag',!fullBannerSwitchFlag)
   }

/**
   * logo
   * @param state
   * @param config
   */
  @Action('init:Imge')
  Imge(state, config: IList) {
    return state
      .set('fullBannerImages', config)
  }
  /**
   * logo
   * @param state
   * @param config
   */
  @Action('init:bannerImages')
  bannerImagesImge(state, config: IList) {
    return state
      .set('bannerImages', config)
  }

  @Action('goodsActor:initSetting')
  FlashAndGrouponSetting(state, res) {
    return state
    .set('openFlag', res.openFlag)
    .set('flashOpenFlag', res.flashOpenFlag)
    .set('goodsCount', res.goodsCount)
    .set('flashGoodsCount', res.flashGoodsCount)
  }
}