import { Actor, Action } from 'plume2';
import { fromJS, Map } from 'immutable';
import { IMap } from 'typings/globalType';

export default class DistributionActor extends Actor {
  defaultState() {
    return { dSetting: {}, distributor: {}, showBackRoot: false };
  }

  /**
   *
   * @param state 分销设置
   * @param res
   */
  @Action('distributeActor:setting')
  DistributionSetting(state: IMap, res) {
    return state.set('dSetting', fromJS(res));
  }

  /**
   *
   * @param state 设置分销员信息
   * @param res
   */
  @Action('distributeActor:distributor')
  DistributorInfo(state: IMap, res) {
    return state.set('distributor', res);
  }

  /**
   * 返回自己店铺按钮展示与否
   */
  @Action('distributeActor:changeVisual')
  changeWholesaleVisible(state, value) {
    return state.set('showBackRoot', value);
  }
}
