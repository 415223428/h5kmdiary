import { Fetch, WMkit } from 'wmkit';
type TResult = { code: string; message: string; context: any };

/**
 * 查询商品列表
 * @param params
 * @returns {Promise<IAsyncResult<T>>}
 */
export const fetchGoodsList = () => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  return isLoginOrNotOpen
    ? Fetch<TResult>('/goods/skus', {
        method: 'POST',
        body: JSON.stringify({
          pageSize: 20,
          sortFlag: 0
        })
      })
    : Fetch<TResult>('/goods/skuListFront', {
        method: 'POST',
        body: JSON.stringify({
          pageSize: 20,
          sortFlag: 0
        })
      });
};

/**
 * 查询分销设置及分销员信息
 *
 * @param param
 */
export const fetchDistributionSetting = (param) => {
  return Fetch<TResult>('/distribute/setting', {
    method: 'POST',
    body: JSON.stringify({
      inviteeId: param
    })
  });
};

/**
 * 获取广告
 * @param params
 * @returns {Promise<IAsyncResult<T>>}
 */
export const queryBaseConfig = () => {
  return Fetch<TResult>('/system/baseConfig', {
    method: 'GET'
  });
};


/**
 * 获取限时抢购，拼团
 * @param params
 * @returns {Promise<IAsyncResult<T>>}
 */
export const getFlashAndGrouponSetting = () => {
  return Fetch<TResult>('/system/getFlashAndGrouponSetting', {
    method: 'GET'
  });
};

/**
 * 获取从小程序端进行登录的登录信息
 */
export const getLoginInfo = (customerId) => {
  return Fetch<TResult>(`/third/login/wechat/logininfo/${customerId}`, {
    method: 'GET',
  });
};
// export const getIfShow = () => {
//   return Fetch<TResult>('/system/advertisingConfig', {
//     method: 'GET',
//   });
// };
export const fetchLogo = () => {
  return Fetch<TResult>('/system/advertisingConfig', {
    method: 'GET',
    // body: JSON.stringify({})
  });
};


export const getAdver = () => {
  return Fetch<TResult>('/system/advertisingConfig', {
    method: 'GET',
  });
};

/**
 * 查询分销员信息
 * @returns {Promise<Result<T>>}
 */
export const fetchDistributorInfo = () => {
  return Fetch('/distribute/distributor-info');
};