import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';
import { fromJS } from 'immutable';
import { noop ,WMkit} from 'wmkit';

@Relax
export default class SearchBar extends React.Component<any, any> {
  props: {
    relaxProps?: {
      distributor: IMap;
      dSetting: IMap;
      toMainPageAndClearInviteCache: Function;
      showBackRoot: boolean;
    };
  };

  static relaxProps = {
    distributor: 'distributor',
    dSetting: 'dSetting',
    toMainPageAndClearInviteCache: noop,
    showBackRoot: 'showBackRoot'
  };

  render() {
    const {
      dSetting,
      distributor,
      toMainPageAndClearInviteCache,
      showBackRoot
    } = this.props.relaxProps;
    const isDistributor =
      distributor && fromJS(distributor).get('distributorFlag');
    const forbiddenFlag =
      distributor && fromJS(distributor).get('forbiddenFlag');
    const openFlag = fromJS(dSetting).get('openFlag'); //分销设置是否打开
    const showFlag = isDistributor && openFlag && !forbiddenFlag;
    let customerName = fromJS(distributor).get('customerName');
    if (
      customerName &&
      customerName.length == 11 &&
      customerName.indexOf('****') != -1
    ) {
      customerName = customerName.substring(6);
    }
    const storeName = customerName
      ? customerName + '的' + fromJS(dSetting).get('shopName')
      : '';
    return (
      <div className="main-search" id="searchBar">
        {/* {showFlag ? <div className="main-shop-name">{storeName}</div> : ''} */}
        <Link className="main-search-link" to="/search">
          <i className="iconfont icon-sousuo" />
          <input placeholder="搜索商品" />
        </Link>
        <div className="searchbutton">
          <Link to="/purchase-order">
            <img src={require('../css/img/shopping.png')} alt="" />
          </Link>
          {
            !(window as any).isMiniProgram && (
              <img src={require('../css/img/service.png')} alt=""
                   onClick={() => {
                     WMkit.getQiyuCustomer();
                     window.location.href = (window as any).ysf('url');
                     (window as any).openqiyu();
                   }}
              />
            )}
        </div>
        {/* {showBackRoot && (
          <div
            className="head-back-btn"
            style={{ marginLeft: '0.15rem', paddingTop: '0.02rem' }}
          >
            <span onClick={() => toMainPageAndClearInviteCache()}>
              返回自己店铺
            </span>
          </div>
        )} */}
      </div>
    );
  }
}
