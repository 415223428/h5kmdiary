import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';
import { fromJS } from 'immutable';
import { noop, history } from 'wmkit';

@Relax
export default class SearchBar extends React.Component<any, any> {
  props: {
    relaxProps?: {
      fullBannerImages: Array<any>;
      fullBannerUrl:String;
      fullBannertime:String;
      fullBannerSwitchFlag:boolean;
      changgefullBannerSwitchFlag:Function;
    };
  };

  static relaxProps = {
    fullBannerImages: 'fullBannerImages',
    fullBannerUrl: 'fullBannerUrl',
    fullBannertime: 'fullBannertime',
    fullBannerSwitchFlag: 'fullBannerSwitchFlag',
    changgefullBannerSwitchFlag:noop,
  };

  render() {
    const {
      fullBannerImages,
      fullBannerUrl,
      fullBannertime,
      fullBannerSwitchFlag,
      changgefullBannerSwitchFlag,
    } = this.props.relaxProps;
    // const imgUrl = fullBannerImages[0].url||require('../css/img/logo.png');
    console.log(fullBannerImages[0]);
    return (
          <div className='jumpMain' onClick={()=>{
            history.push(`../${fullBannerUrl}`)
            changgefullBannerSwitchFlag(fullBannerSwitchFlag)
          }}>
          <img src={fullBannerImages && fullBannerImages.length ?fullBannerImages[0].url:require('../css/img/logo.png')} alt="" />
          <div className='jumpDiv'>
            <button className='jump' 
            onClick={(e) => {
              e.stopPropagation()
              changgefullBannerSwitchFlag(fullBannerSwitchFlag)}}
            >跳过 <span> {fullBannertime}</span></button>
          </div>
        </div> 
    );
  }
}
