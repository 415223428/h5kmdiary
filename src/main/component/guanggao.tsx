import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';
import { fromJS } from 'immutable';
import { noop, history } from 'wmkit';

@Relax
export default class SearchBar extends React.Component<any, any> {
  props: {
    relaxProps?: {
      bannerSwitchFlag: boolean;
      bannerImages:Array<any>;
      bannerUrl:String;
      isShowAD:Function;
    };
  };

  static relaxProps = {
    bannerImages:"bannerImages",
    bannerUrl:'bannerUrl',
    isShowAD:noop,
  };

  render() {
    const {
      bannerImages,
      bannerUrl,
      isShowAD
    } = this.props.relaxProps;
    // const bannerImages = this.store.state().toJS().bannerImages;
    // const time = this.store.state().get('time');
    // const isShowAD = this.store.state().get('bannerSwitchFlag')
    // const fullBannerUrl = this.store.state().get('fullBannerUrl')
    // const imgUrl = bannerImages[0].url||require('../css/img/logo.png');
    
    return (
      <div >
          <div className="guanggao"
          onClick={()=>{
            history.push(`../${bannerUrl}`)
            isShowAD(isShowAD);
          }}
          >
            <img src={bannerImages && bannerImages.length ? bannerImages[0].url:require('../css/img/logo.png')} />
            <div className="xx"
            onClick={(e)=>{
              e.stopPropagation();
              isShowAD(isShowAD)
            }}
            >
              <div>x</div>
            </div>
          </div>
      </div>
    );
  }
}
