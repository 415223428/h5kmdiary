import { Store, IOptions, msg } from 'plume2';
import { WMkit, Alert } from 'wmkit';
import * as webapi from './webapi';
import { cache } from 'config';
import { mergePurchase } from 'biz';
import GoodsActor from './actor/goods-actor';
import DistributorActor from './actor/distributor-actor';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
  }

  bindActor() {
    return [new GoodsActor(), new DistributorActor()];
  }

  /**
   * 初始化数据
   */
  init = async () => {
    //获取用户
    // const { code, context } = await webapi.fetchGoodsList();
    // if (code == 'K-000000') {
    //   this.dispatch(
    //     'goodsActor:goodsList',
    //     (context as any).esGoodsInfoPage.content
    //   );
    // }
    const inviteeId = WMkit.inviteeId();
    if (inviteeId) {
      const { code, context } = await webapi.fetchDistributionSetting(
        inviteeId
      );
      if (code == 'K-000000') {
        this.dispatch(
          'distributeActor:setting',
          (context as any).distributionSettingSimVO

        );
        this.dispatch(
          'distributeActor:distributor',
          (context as any).distributionCustomerSimVO
        );
      }
    }
    if (WMkit.isDistributorLoginForShare()) {
      this.dispatch('distributeActor:changeVisual', true);
    }
      let {code,context} =  await webapi.fetchDistributorInfo();
      if(code == "K-000000"){
        if((context as any).distributionCustomerVO){
          sessionStorage.setItem('inviteCode',(context as any).distributionCustomerVO.inviteCode) 
        }
      }
  };

  initImg = async () => {
      const res = await webapi.fetchLogo();
      if (res['status'] == '404') {
        this.dispatch('shanPing:init');
      } else {
        this.dispatch('init:logo', res);
    };
    this.fetchLogo();
  };

  initSetting = async () => {
    const{ code, context } = await webapi.getFlashAndGrouponSetting();
    
    if (code =='K-000000') {
      console.log((context as any).grouponSettingVO);
      this.dispatch('goodsActor:initSetting', (context as any).grouponSettingVO);
    } 
    
};

  getAdv = async () => {
    //获取广告
    const { code, context } = await webapi.queryBaseConfig();
    if (code == 'K-000000') {
      var obj = [];
      var advres = [];
      if ((context as any).mobileBanner != null) {
        obj = eval('(' + (context as any).mobileBanner + ')');
      }

      obj.forEach((item) => {
        advres.push(item.url);
      });
      this.dispatch('goodsActor:advList', advres);
      this.dispatch('goodsActor:title', (context as any).pcTitle);
    }
  };

  //退出分享链接到商城首页,并更新邀请人和分销渠道缓存(分销渠道变为商城)
  toMainPageAndClearInviteCache = () => {
    WMkit.toMainPageAndClearInviteCache();
    this.dispatch('distributeActor:changeVisual', false);
    this.dispatch('distributeActor:distributor', {});
  };

  getLoginInfo = async (customerId) => {
    const { code, context } = await webapi.getLoginInfo(customerId);
    if (code == 'K-000000') {
      window.token = (context as any).token;
      localStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context as any));
      sessionStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context as any));
      WMkit.setIsDistributor();
      // 登录成功，发送消息，查询分销员信息 footer/index.tsx
      msg.emit('userLoginRefresh');
      // b.登陆成功,需要合并登录前和登陆后的购物车
      mergePurchase(null);
      // c.登陆成功,跳转拦截前的路由s
      Alert({ text: '登录成功' });
    }
  };

//弹窗广告
  // 请求logo
  fetchLogo = async () => {
    const{ code, context } = await webapi.fetchLogo();
    if (code == 'K-000000') {
      this.dispatch('init:logo', context);
      this.dispatch('init:Imge',JSON.parse((context as any).fullBannerImages))
      this.dispatch('init:bannerImages',JSON.parse((context as any).bannerImages))
      {this.state().get('fullBannerSwitchFlag')?
      this.count(this.state().get('fullBannertime')):null};
    } else {
      this.dispatch('shanPing:init');
      this.count(this.state().get('fullBannertime'))
    }
  };

  isShowAD = async (bannerSwitchFlag) => {
    this.dispatch('goods:bannerSwitchFlag', !bannerSwitchFlag)
    sessionStorage.setItem('bannerSwitchFlag',"1");
  }


  //闪屏

  // 倒计时
  count = async(s) =>  {
    const count1 = this.count
    let time;
    const fullBannerSwitchFlag = this.state().get('fullBannerSwitchFlag')||0;
    if (s !== -1) {
      this.dispatch('init:time', s);
      s--
    } else {
      s = 5;
      // this.jump()
      this.changgefullBannerSwitchFlag(fullBannerSwitchFlag);
      return;
    }

    let timtout = setTimeout(() => {
      count1(s)
    }, 1000);

    if(fullBannerSwitchFlag === 0){
      clearTimeout(timtout);
    }
  }

  changgefullBannerSwitchFlag = (fullBannerSwitchFlag)=>{
    this.dispatch('shanPing:fullBannerSwitchFlag',fullBannerSwitchFlag);
    sessionStorage.setItem('fullBannerSwitch',"1");
  }
}
