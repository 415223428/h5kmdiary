import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit } from 'wmkit';
import { cache } from 'config';
import AppStore from './store';
import SearchBar from './component/search-bar';
import ShanPing from './component/shangping';
import Guanggao from './component/guanggao';
import MagicBox from '@wanmi/magic-box';
import * as magicBoxProps from '../magicBoxProps';
import wx from 'weixin-js-sdk'
import Flashsale from '../flash-sale';
import Grouponcenter from '../groupon-center';

const styles = require('./css/style.css');
const innerHeight = window.innerHeight;
@StoreProvider(AppStore, { debug: __DEV__ })
export default class Main extends React.Component<any, any> {
  store: AppStore;
  parseQueryString(url) {
    var obj = {};
    var keyvalue = [];
    var key = "",
      value = "";
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    for (var i in paraString) {
      keyvalue = paraString[i].split("=");
      key = keyvalue[0];
      value = keyvalue[1];
      obj[key] = value;
    }
    return obj;
  }
  componentWillMount(){
    this.store.initSetting();
  }
  componentDidMount() {
    //小程序向外链的H5传值，适用于店铺外分享，显示带有小B店铺的首页。
    if (this.props.location.search) {
      let searchObj = WMkit.searchToObj(this.props.location.search);
      //将inviteeId重新存储
      if ((searchObj as any).inviteeId) {
        sessionStorage.setItem(cache.INVITEE_ID, (searchObj as any).inviteeId);
      }
      //登录返回的登录人ID，传到H5这边
      if ((searchObj as any).customerId) {
        this.store.getLoginInfo((searchObj as any).customerId)
      }
    }

    // this.store.getAdv();
    this.store.init();
    this.store.fetchLogo();
    window.addEventListener('scroll', this.handleScroll);

    const { cstoken }: any = this.parseQueryString(location.href)
    // if (cstoken) {
    //   alert(cstoken)
    // }
    let ua = navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i)) {
      //ios的ua中无miniProgram，但都有MicroMessenger（表示是微信浏览器）
      wx.miniProgram.getEnv((res) => {
        if (res.miniprogram) {
          if (cstoken) {
            alert('在小程序里');
          }
        } else {
          if (cstoken) {
            alert('微信浏览器');
          }
          let oScript = document.createElement('script');
          oScript.src = 'https://qiyukf.com/script/244c655f2a1623d0be6dde0e6d1947be.js';
          document.body.appendChild(oScript);
        }
      })
    } else {
      if (cstoken) {
        alert('普通浏览器');
      }
      let oScript = document.createElement('script');
      oScript.src = 'https://qiyukf.com/script/244c655f2a1623d0be6dde0e6d1947be.js';
      document.body.appendChild(oScript);
    }

    //   const customer = localStorage["b2b-wechat@login"];
    // {
    //   customer && (window as any).ysf() ?

    //     (window as any).ysf('config', {
    //       unconfirm: 1,
    //       uid: JSON.parse(customer).customerDetail.customerDetailId,
    //       name: JSON.parse(customer).customerDetail.customerName,
    //       mobile: JSON.parse(customer).customerDetail.contactPhone,
    //     }) : ''
    // }

  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll = () => {
    let sceneList = document.getElementById('sceneList');
    let cateList = document.getElementById('cateList');
    let flashTitle = document.getElementById('flashTitle');
    let swiperSceneList = document.querySelector('#swiper-sceneList') as HTMLElement;
    if (swiperSceneList && swiperSceneList.getBoundingClientRect().top <= 0.3) {
      swiperSceneList.style.position = 'fixed';
      swiperSceneList.style.top = '0';
      swiperSceneList.style.margin = '0';
      swiperSceneList.style.left = '0';
      swiperSceneList.style.zIndex = '999';
    }
    if (flashTitle
      &&(flashTitle.getBoundingClientRect().bottom >= flashTitle.offsetHeight
      || flashTitle.getBoundingClientRect().bottom <= 0.3)) {
      swiperSceneList.style.position = '';
      swiperSceneList.style.top = '';
      swiperSceneList.style.zIndex = '';
    }
  }

  render() {

    const props = { ...magicBoxProps.props };
    const fullBannerSwitchFlag = this.store.state().get('fullBannerSwitchFlag');
    const bannerSwitchFlag = this.store.state().get("bannerSwitchFlag");
    const flashOpenFlag = this.store.state().get("flashOpenFlag");
    const flashGoodsCount = this.store.state().get("flashGoodsCount");
    const openFlag = this.store.state().get("openFlag");
    const goodsCount = this.store.state().get("goodsCount");
    console.log(flashGoodsCount);
    
    return (
      <div >
        {fullBannerSwitchFlag && sessionStorage.getItem('fullBannerSwitch') == null ?
          <div>
            <ShanPing />
          </div>
          :
          <div>
            {
              bannerSwitchFlag && sessionStorage.getItem('bannerSwitchFlag') == null ?
                <div>
                  <Guanggao />
                  <SearchBar />
                  {/* <div id="MagicBox" style={{ overflow: "hidden auto" }}> */}
                  <MagicBox style={{ height: '100vh', paddingBottom: 44 }} {...props} />
                  {/* </div> */}
                </div>
                :
                <div>
                  <SearchBar />
                  <MagicBox style={{ height: 'auto' }} {...props} />
                  {
                    flashOpenFlag ?
                      <Flashsale state="main" flashGoodsCount={flashGoodsCount} />
                      : null
                  }
                  {
                    openFlag ?
                      <Grouponcenter page="main" goodsCount={goodsCount} />
                      : null
                  }
                  <p style={{ color: 'rgb(21, 20, 20)', fontSize: '12px', textAlign: 'center', marginTop: '20px', marginBottom: '4px' }}>粤ICP备19120716号-1</p>
                  <div style={{ color: 'rgb(21, 20, 20)', fontSize: '12px', marginBottom: '80px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                    <img style={{ width: '20px', height: '20px', marginRight: '6px', display: 'flex' }} src={require("./css/img/police-icon.png")} alt="" />粤公网安备 44010602007138号
                  </div>
                </div>
            }

            {/* <Grouponcenter page="main" />
            <p style={{ color: 'rgb(21, 20, 20)', fontSize: '12px', textAlign: 'center', marginTop: '20px', marginBottom: '4px' }}>粤ICP备19120716号-1</p>
            <div style={{ color: 'rgb(21, 20, 20)', fontSize: '12px', marginBottom: '80px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              <img style={{ width: '20px', height: '20px', marginRight: '6px', display: 'flex' }} src={require("./css/img/police-icon.png")} alt="" />粤公网安备 44010602007138号
            </div> */}
          </div>
        }
      </div>
    );
  }
}
