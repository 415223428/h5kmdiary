import { Fetch } from 'wmkit'
type TResult = { code: string; message: string ; context: any}

/**
 * 获取订单商品列表
 * @returns {Promise<Result<TResult>>}
 */
export const fetchWillBuyGoodsList = () => {
  return Fetch<TResult>('/trade/purchase')
}