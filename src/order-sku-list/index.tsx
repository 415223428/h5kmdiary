import React, { Component } from 'react';
import { SkuList } from 'wmkit';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import Gift from './component/gift';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class OrderSkuList extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { sid } = this.props.match.params;
    this.store.fetchWillBuyGoodsList(sid);
  }

  render() {
    const skuList = this.store
      .state()
      .get('orderSkus')
      .map((sku) => {
        let price = sku.levelPrice;
        if (sku.isFlashSaleGoods) {
          price = sku.price;
        }
        return {
          imgUrl: sku.pic,
          skuName: sku.skuName,
          specDetails: sku.specDetails,
          price: price,
          num: sku.num,
          unit: sku.unit,
          distributionCommission: sku.distributionCommission,
          distributionGoodsAudit: sku.distributionGoodsAudit,
          isFlashSaleGoods: sku.isFlashSaleGoods
        };
      });
    return (
      <div style={{ background: '#fafafa' }}>
        <SkuList
          data={skuList}
          isSelf={this.store.state().get('isSelf')}
          commissionStrType={1}
        />
        <Gift />
      </div>
    );
  }
}
