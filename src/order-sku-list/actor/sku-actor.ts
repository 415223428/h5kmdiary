import { Action, Actor, IMap } from "plume2";

export default class SkuActor extends Actor {
  defaultState() {
    return {
      orderSkus: [], //待购买商品
      isSelf: false, // 是否是自营
      gifts: [], //赠品
    }
  }

  /**
   * 存储待购买商品集合
   * @param state
   * @param orderSkus
   * @returns {Map<string, V>}
   */
  @Action('sku: list: fetch')
  fetchOrderSkus(state: IMap, { skus, isSelf, gifts }) {
    return state.set('orderSkus', skus)
      .set('isSelf', isSelf)
      .set('gifts', gifts)
  }
}
