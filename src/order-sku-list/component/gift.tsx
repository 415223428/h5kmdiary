import React, { Component } from 'react'
import { Relax } from 'plume2'

const noneImg = require('../img/none.png')
const imgUrl = false
@Relax
export default class Gift extends Component<any, any> {

  props: {
    relaxProps?: {
      gifts: any
    }
  }

  static relaxProps = {
    gifts: 'gifts'
  }

  render() {
    const { gifts } = this.props.relaxProps
    return gifts && gifts.length > 0 &&
      <div className="gift-box">
        <div className="gift">
          {
            gifts.map(g => {
              return <div className="goods-list">
                <img className="img-box" src={g.pic ? g.pic : noneImg} />
                <div className="detail">
                  <a className="title" href="#"><div className="tag-gift">赠品</div>{g.skuName}</a>
                  <p className="gec">{g.specDetails}</p>
                  <div className="bottom">
                    <span className="price"><i className="iconfont icon-qian" />0.00</span>
                    <span className="num">×{g.num}{g.unit}</span>
                  </div>
                </div>
              </div>
            })
          }
        </div>
      </div>

  }

}
