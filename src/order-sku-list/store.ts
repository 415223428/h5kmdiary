import { Store } from 'plume2';
import SkuActor from './actor/sku-actor';
import * as webApi from './webapi';
import { Confirm, history } from 'wmkit';

export default class AppStore extends Store {
  bindActor() {
    return [new SkuActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  /**
   * 确认订单初始化
   * @returns {Promise<void>}
   */
  fetchWillBuyGoodsList = async (sid) => {
    const skuRes = (await webApi.fetchWillBuyGoodsList()) as any;
    if (skuRes.code == 'K-000000') {
      const store = skuRes.context.tradeConfirmItems.filter(
        (f) => f.supplier.storeId == sid
      )[0];
      store.tradeItems.forEach((item) => {
        if (item.distributionCommission) {
          item.distributionCommission = item.distributionCommission * item.num;
        }
      });
      this.transaction(() => {
        this.dispatch('sku: list: fetch', {
          skus: store.tradeItems,
          isSelf: store.supplier.isSelf,
          gifts: store.gifts
        });
      });
    } else {
      Confirm({
        text: skuRes.message,
        okBtn: '确定',
        confirmCb: () => history.push('/purchase-order')
      });
    }
  };
}
