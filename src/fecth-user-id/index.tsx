import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import Container from './component/container';
import { cache} from 'config';
const style = require('./css/style.css');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class fecthUserId extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
    const customerDetail:any =  JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
    this.store.initFecthAccount(customerDetail.customerAccount);
  }

  render() {
    return (
      <div className="use-setting">
        <Container />
      </div>
    );
  }
}
