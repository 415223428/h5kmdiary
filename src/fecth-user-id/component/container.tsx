import React, { Component } from 'react';
import { Relax } from 'plume2';
import { Button } from 'wmkit';
import ReactClipboard from 'react-clipboardjs-copy';
const Long = Button.Long;
const logout = require('../img/logout.png')
@Relax
export default class Container extends Component<any, any> {
  props: {
    relaxProps?: {
      customer: any;
      otherAccount: any;
    };
  };

  static relaxProps = {
    customer: 'customer',
    otherAccount: 'otherAccount',
  };

  render() {
    const { customer, otherAccount } = this.props.relaxProps;
    console.log(otherAccount);
    let mobile = customer.get('customerAccount');
    let headImg = customer.get('headImg') ? customer.get('headImg') : require('../../../web_modules/images/default-headImg.png');
    if (mobile) {
      mobile = mobile.substr(0, 3) + '****' + mobile.substr(7);
    }
    return (
      <div>
        <div className="avatarBox b-1px-b" style={{ height: '1.5rem' }}>
          <div className="border-round">
            <img src={headImg} alt="" style={{ borderRadius: '50%' }} />
          </div>
          <div>
            <p className="use-name">{customer.get('customerName')}</p>
            <p className="use-num">{mobile}</p>
          </div>
        </div>

        <div
          className="setting-item b-1px-b"
        >
          <label className="myDetail">我的其他商城账号详情</label>
        </div>

        {
          otherAccount.map((v) => {
            return <ReactClipboard
              text={v.customerId}
              onSuccess={() => alert('已复制会员编号')}
              onError={() => alert('已复制会员编号')}
            >
              <div
                className="setting-item b-1px-b"
                >
                <div className='text-label'>
                  {
                    v.sourceType ?
                      <label className = 'text-title'>时代会员编号:</label> :
                      <label className = 'text-title'>商城会员编号:</label>
                  }
                  {
                    v.sourceType?
                    <div className='text-number'>{v.customerId}</div>
                    :
                    <div>
                    <div className='text-number'>{v.customerId.substring(0,16)}</div>
                    <div className='text-number'>{v.customerId.substring(16)}</div>
                  </div>
                  }
                </div>
                <div>复制</div>
              </div>
            </ReactClipboard>

          })
        }

        <div className='tip-text'>
          <div >
            温馨提示：
          </div>
          <div>
            1.我的其他商城账号查询，仅可查询到您当前手机号码相关联的其他商城账号。
          </div>
          <div>
            2.您可通过复制会员编号，使用商城的账号登录访问到其他账号。
          </div>
        </div>
      </div>
    );
  }
}
