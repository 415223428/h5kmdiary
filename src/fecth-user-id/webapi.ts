import { Fetch } from 'wmkit';

/**
 * 查询会员信息
 */
export const fetchCustomerCenterInfo = () => {
  return Fetch('/customer/customerCenter');
};


export const fetchOtherAccount = (phone) =>{
  return Fetch(`/getPhoneUserId/${phone}`,{
    method:'POST'
  })
}