import { Actor, Action } from 'plume2';

export default class SettingsActor extends Actor {
  defaultState() {
    return {
      // 用户信息
      customer: {},
      otherAccount:{},
    };
  }

  /**
   * 初始化
   */
  @Action('init')
  init(state, { customer }) {
    return state.set('customer', customer);
  }

  @Action('otherAccount:init')
  fecthOtherAccount(state, value) {
    return state.set('otherAccount', value.customerVOList)
  }
}
