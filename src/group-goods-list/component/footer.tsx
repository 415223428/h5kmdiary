import React from 'react';
import { Link } from 'react-router-dom';

export default class Footer extends React.Component<any, any> {
  render() {
    return (
      <div className="footer">
        <ul>
          <li>
            <i className="iconfont icon-pin" />
            <span style={styles.text}>拼购</span>
          </li>
          <li>
            <i className="iconfont icon-huo" />
            <span style={styles.text}>热销排行</span>
          </li>
          <li>
            <i className="iconfont icon-yingyongjieshao" />
            <span style={styles.text}>玩法介绍</span>
          </li>
          <li className="cur">
            <i className="iconfont icon-wode2" />
            <span style={styles.text}>我的拼购</span>
          </li>
        </ul>
      </div>
    );
  }
}

const styles = {
  text: {
    fontSize: 10,
    color: '#333333'
  }
};
