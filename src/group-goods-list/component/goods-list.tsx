import React from 'react';
import { ListView, WMImage } from 'wmkit';

const data = [
  {
    storeName: '店铺名称',
    groupStatus: '拼团中',
    payStatus: '已支付',
    goodsImg:
      'https://wanmi-b2b.oss-cn-shanghai.aliyuncs.com/201809180930533730?x-oss-process=image/resize,m_fill,w_563,h_563/sharpen,100/format,webp',
    goodsName:
      '沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤...',
    goodsNum: 2,
    goodsPrice: '￥122.00',
    specText: '规格1 规格2 规格3'
  },
  {
    storeName: '店铺名称',
    groupStatus: '拼团成功',
    payStatus: '已支付',
    goodsImg:
      'https://wanmi-b2b.oss-cn-shanghai.aliyuncs.com/201809180930533730?x-oss-process=image/resize,m_fill,w_563,h_563/sharpen,100/format,webp',
    goodsName:
      '沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤...',
    goodsNum: 2,
    goodsPrice: '￥122.00',
    specText: '规格1 规格2 规格3'
  },
  {
    storeName: '店铺名称',
    groupStatus: '拼团失败',
    payStatus: '金额退回',
    goodsImg:
      'https://wanmi-b2b.oss-cn-shanghai.aliyuncs.com/201809180930533730?x-oss-process=image/resize,m_fill,w_563,h_563/sharpen,100/format,webp',
    goodsName:
      '沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤沙滩条纹短裤...',
    goodsNum: 2,
    goodsPrice: '￥122.00',
    specText: '规格1 规格2 规格3'
  }
];

const layoutClassName = 'goods-list-box';
let containerClass = 'goods-item-box';

export default class GoodsList extends React.Component<any, any> {
  render() {
    return (
      <ListView
        className={layoutClassName}
        dataSource={data}
        renderRow={(row) => this._renderRow(row)}
      />
    );
  }

  _renderRow = (row) => {
    return (
      <div className={containerClass}>
        <div className="list-img-box">
          <WMImage src={row.goodsImg} width="174px" height="174px" />
          <div className="list-num">2人团</div>
        </div>
        <div className="list-detail">
          <div className="goods-name">{row.goodsName}</div>
          <div>
            <span className="price">
              <i className="iconfont icon-qian" />
              122.00
            </span>
            <span style={{ marginLeft: 8 }} className="line-price">
              ￥100.00
            </span>
          </div>
          <div className="list-bottom">
            <span className="bottom-group-complete">3000人已拼团</span>
            <div className="group-pin">拼</div>
          </div>
        </div>
      </div>
    );
  };
}
