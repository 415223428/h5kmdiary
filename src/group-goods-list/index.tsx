import React from 'react';
import './css/style.css';
import GoodsList from './component/goods-list';
import Footer from './component/footer';

export default class GroupGoodsList extends React.Component<any, any> {
  render() {
    return (
      <div className="container">
        <GoodsList />
        <Footer />
      </div>
    );
  }
}
