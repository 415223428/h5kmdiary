import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };

export const grouponDetail = (grouponNo) => {
  return Fetch<TResult>(
    WMkit.isLogin()
      ? `/goods/groupon/groupon-detail/${grouponNo}`
      : `/goods/unLogin/groupon/groupon-detail/${grouponNo}`,
    {
      method: 'GET'
    }
  );
};

/**
 * 获取服务时间
 */
export const queryServerTime = () => {
  return Fetch('/system/queryServerTime');
};

/**
 * 获取热门推荐、精选拼团列表数据
 * @type {Promise<AsyncResult<T>>}
 */
export const fetchGrouponCenterList = () => {
  return Fetch<TResult>('/groupon/center/list', {
    method: 'POST',
    body: JSON.stringify({ sticky: true })
  });
};

/**
 * 获取团实例信息
 */
export const initGrouponNotice = (grouponNo) => {
  return Fetch<Result<any>>('/groupon/grouponInstanceInfo/' + grouponNo);
};

export const immediateBuyValidate = (params = {}) => {
  return Fetch<TResult>('/groupon/vaildateGrouponStatusForOpenOrJoin', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

export const shareInfo = () => {
  return Fetch('/customer/customerCenter')
}