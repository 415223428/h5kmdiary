import React from 'react';
import { Link } from 'react-router-dom';

export default class Footer extends React.Component<any, any> {
  render() {
    return (
      <div className="footer">
        <ul>
          <li>
            <Link to="/">
              <i className="iconfont icon-shouye01" />
              <span style={styles.text}>拼购</span>
            </Link>
          </li>
          <li>
            <Link to="/">
              <i className="iconfont icon-fenlei" />
              <span style={styles.text}>热销排行</span>
            </Link>
          </li>
          <li>
            <Link to="/">
              <i className="iconfont icon-gouwuche" />
              <span style={styles.text}>玩法介绍</span>
            </Link>
          </li>
          <li className="cur">
            <Link to="/">
              <i className="iconfont icon-zhanghao" />
              <span style={styles.text}>我的拼购</span>
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

const styles = {
  text: {
    fontSize: 10,
    color: '#333333'
  }
};
