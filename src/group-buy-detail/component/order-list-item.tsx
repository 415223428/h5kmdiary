import React from 'react';
export default class OrderItem extends React.Component<any, any> {
  render() {
    const item = this.props.item;
    return (
      <div>
        <div style={styles.item}>
          <div style={styles.itemTop}>
            <div style={styles.shop}>{item.storeName}</div>
            <div>
              <span style={styles.groupStatus}>{item.groupStatus}</span>
              <span style={styles.payStatus}>{item.payStatus}</span>
            </div>
          </div>
          <div
            style={
              item.groupStatus == '拼团中'
                ? { ...styles.middle, ...styles.middleBottom }
                : { ...styles.middle }
            }
          >
            <img src={item.goodsImg} style={styles.img} />
            <div style={styles.goods}>
              <div style={styles.goodsName}>{item.goodsName}</div>
              <div style={styles.goodsNum}>X{item.goodsNum}</div>
              <div>
                <span style={styles.goodsNum}>实付款</span>
                <span style={styles.goodsPrice}>{item.goodsPrice}</span>
              </div>
            </div>
          </div>
          {item.groupStatus == '拼团中' && (
            <div style={styles.bottom}>
              <div>
                <span style={styles.countDown}>拼团倒计时:</span>
                <span style={styles.time}>24:24:23</span>
              </div>
              <div style={styles.button}>还差1人&nbsp;&nbsp;邀请参团</div>
            </div>
          )}
        </div>
        <div className="bot-line" style={{ marginTop: 0 }} />
      </div>
    );
  }
}

const styles = {
  item: {
    marginTop: 13,
    //height: 177,
    paddingLeft: 9,
    paddingRight: 9,
    borderTop: '1.5px solid #000000',
    flexDirection: 'column',
    borderLeft: '1px solid #EBEBEB',
    borderRight: '1px solid #EBEBEB'
  },
  itemTop: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 37,
    borderBottom: '1px solid #EBEBEB'
  },
  shop: {
    fontSize: 15
  },
  groupStatus: {
    fontSize: 14,
    color: '#FF1F4E'
  },
  payStatus: {
    fontSize: 14,
    marginLeft: 5,
    color: '#FF1F4E'
  },
  middle: {
    display: 'flex',
    height: 95,
    flexDirection: 'row',
    alignItems: 'center'
  },
  middleBottom: {
    borderBottom: '1px solid #DCDEE1'
  },
  img: {
    width: 70,
    height: 70,
    minWidth: 70
  },
  goods: {
    marginLeft: 11,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 70
  },
  goodsName: {
    height: 28,
    fontSize: 13,
    overflow: 'hidden'
  },
  goodsNum: {
    fontSize: 10
  },
  goodsPrice: {
    fontSize: 15,
    color: '#CB4255'
  },
  bottom: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 44
  },
  countDown: {
    fontSize: 13
  },
  time: {
    fontSize: 13,
    color: '#FF1F4E',
    marginLeft: 10
  },
  button: {
    width: 126,
    height: 28,
    borderRadius: 13,
    display: 'flex',
    backgroundColor: '#F91A53',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 12
  }
} as any;
