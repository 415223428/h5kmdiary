import React from 'react';
import { Relax } from 'plume2';
import { ListView, WMImage, noop, history } from 'wmkit';
const layoutClassName = 'goods-list-box';
let containerClass = 'goods-item-box';
@Relax
export default class GoodsList extends React.Component<any, any> {
  _listView: any;
  static relaxProps = {
    handleDataReached: noop,
    toRefresh: 'toRefresh'
  };

  render() {
    let { handleDataReached, toRefresh } = this.props.relaxProps;
    return (
      <ListView
        className={layoutClassName}
        url="/groupon/center/list"
        params={{
          sticky: true
        }}
        ref={(_listView) => (this._listView = _listView)}
        onDataReached={handleDataReached}
        dataPropsName="context.grouponCenterVOList.content"
        renderRow={(row) => this._renderRow(row)}
      />
    );
  }

  _renderRow = (row) => {
    return (
      <div
        className={containerClass}
        key={row.goodsId}
        onClick={() => this._goGrouponDetail(row)}
      >
        <div className="list-img-box">
          <WMImage src={row.goodsImg} 
            // width="100%" 
            height="90%"
            style={{objectFit: 'cover'}}
          />
          {/* <div className="list-num">{row.grouponNum}人团</div> */}
        </div>
        <div className="list-detail">
          <div style={{ width: '3.1rem',fontSize:'0.28rem',marginLeft:'0' }} className="goods-name">
            {row.goodsName}
          </div>
          <div>
            <span className="price" style={{fontSize:'0.28rem',fontWeight:'bold'}}>
              <i className="iconfont icon-qian" />
              {row.grouponPrice.toFixed(2)}
            </span>
            {row.marketPrice && (
              <span style={{ marginLeft: 8, fontSize:'0.22rem' }} className="line-price">
                {row.marketPrice.toFixed(2)}
              </span>
            )}
          </div>
          <div className="list-bottom">
            <span className="bottom-group-complete">
              {row.alreadyGrouponNum}人已拼团
            </span>
            <div className="group-pin">拼</div>
          </div>
        </div>
      </div>
    );
  };

  _goGrouponDetail = (item) => {
    history.push('/spellgroup-detail/' + item.goodsInfoId);
  };
}
