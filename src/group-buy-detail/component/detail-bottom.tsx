import React from 'react';
import { Relax } from 'plume2';
import moment from 'moment';
import { CountDown, noop, history } from 'wmkit';
import { init } from 'src/balance/applicat-form/webapi';

const seeOhterT=require('../img/see-other-tuan.png')
const cantuan=require("../img/cantuan.png")
const kaituan=require("../img/kaituan.png")
const seeOhterTG=require('../img/see-other-tuangou.png')
const inviteF=require('../img/invite-friends.png')

const buttonText = {
  //活动已结束
  2: '快去看看其他团吧~',
  4: '我要参团',
  5: '我也开个团',
  //已经参团，拼团成功，等待收货
  7: '看看其他团购',
  8: '邀请好友'
};
@Relax
export default class DetailBottom extends React.Component<any, any> {
  static relaxProps = {
    customerVOList: 'customerVOList',
    activityInfo: 'activityInfo',
    goodsInfoId: 'goodsInfoId',
    serverTime: 'serverTime',
    grouponDetailOptStatus: 'grouponDetailOptStatus',
    doGroup: noop,
    grouponNo: 'grouponNo',
    toggleJoinPeopleModal: noop,
    endHandle: noop,
    changefenxiang:noop,
  };

  render() {
    const {
      customerVOList,
      activityInfo,
      goodsInfoId,
      serverTime,
      grouponDetailOptStatus,
      doGroup,
      grouponNo,
      toggleJoinPeopleModal,
      endHandle,
      changefenxiang
    } = this.props.relaxProps;
    const endTime = moment(activityInfo.get('endTime'));
    return (
      <div className="bottom-container">
        <div className="group-avatar-container">
          <div className="avatar-list">
            {customerVOList.count() > 0 &&
              customerVOList.slice(0, 5).map((vo, index) => {
                return (
                  <img
                    key={index}
                    className="group-avatar"
                    src={
                      vo.get('headimgurl')
                        ? vo.get('headimgurl')
                        : require('../img/default-img.png')
                    }
                  />
                );
              })}
            {/* <img className="group-avatar" src={require('../img/avatar.png')} /> */}
            {customerVOList.count() > 5 && (
              <i
                onClick={() => toggleJoinPeopleModal()}
                style={{ display: 'flex', alignItems: 'center' }}
                className="iconfont icon-more"
              />
            )}
          </div>
          <div className="avatar-list">
            {grouponDetailOptStatus == 2 ? (
              <span className="red-text">活动已结束，你来晚了!</span>
            ) : grouponDetailOptStatus == 5 ? (
              <span className="red-text">来晚一步，已成功组团!</span>
            ) : grouponDetailOptStatus == 7 ? (
              <span className="red-text">拼团成功，等待收货吧！</span>
            ) : (
              <span className="pain-text">
                还差<span className="red-text">
                  {activityInfo.get('waitGrouponNum')}
                </span>人组团成功，
              </span>
            )}
            {grouponDetailOptStatus == 4 || grouponDetailOptStatus == 8 ? (
              <div style={{ display: 'flex', flexDirection: 'row' }}>
                <CountDown
                  endHandle={() => endHandle(grouponNo)}
                  visible={endTime && serverTime}
                  colorStyle={{ color: '#FF4D4D' }}
                  timeOffset={moment
                    .duration(endTime.diff(serverTime))
                    .asSeconds()
                    .toFixed(0)}
                  //timeOffset={6}
                  timeStyle={{fontSize:'0.26rem'}}
                />
                <span className="pain-text">后结束</span>
              </div>
            ) : null}
          </div>
        </div>
        <div className="buy-btn" 
        onClick={() => {
          // history.push('/share')
          if(grouponDetailOptStatus==8){
            changefenxiang();
          }else(
            history.push('/groupon-center')
          )
          // doGroup();
        }
          }>
         {/* /!* {buttonText[grouponDetailOptStatus]} *!/ */}
         <img src={grouponDetailOptStatus==2?seeOhterT
                   :grouponDetailOptStatus==4?cantuan
                   :grouponDetailOptStatus==5?kaituan
                   :grouponDetailOptStatus==7?seeOhterTG:inviteF} alt=""
                   style={{width:' 4.6rem',height: '0.8rem'}}/>
        </div>
      </div>
    );
  }
}
