import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { NumberInput, noop } from 'wmkit';
import SockJsClient from 'react-stomp';
import { IList } from 'typings/globalType';

@Relax
export default class Notice extends Component<any, any> {
  constructor(props) {
    super(props);
  }
  props: {
    relaxProps?: {
      initGrouponNoWait: Function;
      topics: IList;
      url: String;
    };
  };
  static relaxProps = {
    initGrouponNoWait: noop,
    topics: 'topics',
    url: 'url'
  };

  render() {
    const { initGrouponNoWait, url, topics } = this.props.relaxProps;
    return (
      <div>
        <SockJsClient
          url={url}
          topics={topics.toJS()}
          onMessage={(msg) => {
            initGrouponNoWait(msg);
          }}
          ref={(client) => {
            // this.clientRef = client;
          }}
        />
      </div>
    );
  }
}
