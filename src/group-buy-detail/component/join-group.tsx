import React from 'react';
import { Relax } from 'plume2';
import moment from 'moment';
import { CountDown, noop } from 'wmkit';
import { immediateBuy } from 'wmkit/goods-choose/retail-choose/webapi';
@Relax
export default class JoinGroup extends React.Component<any, any> {
  static relaxProps = {
    grouponInstanceList: 'grouponInstanceList',
    serverTime: 'serverTime',
    toggleSpecModal: noop,
    toggleWaitGroupModal: noop,
    immediateJoin: noop,
    goodsInfoId: 'goodsInfoId',
    openSpecModal: noop
  };
  render() {
    const {
      grouponInstanceList,
      serverTime,
      toggleSpecModal,
      toggleWaitGroupModal,
      immediateJoin,
      goodsInfoId,
      openSpecModal
    } = this.props.relaxProps;
    return (
      <div className="join-group">
        <div className="group-box">
          <span className="group-title">直接参与下面的团</span>
          <span className="see-more" onClick={() => toggleWaitGroupModal()}>
            <span className="more">查看更多</span>
            <i className="iconfont icon-jiantou1" />
          </span>
        </div>
        <ul className="group-list">
          {grouponInstanceList.count() > 0 &&
            grouponInstanceList.map((group, index) => {
              const endTime = moment(group.get('endTime'));
              return (
                <li key={index}>
                  <div className="left-pointer">
                    <img
                      src={
                        group.get('headimgurl')
                          ? group.get('headimgurl')
                          : require('../img/default-img.png')
                      }
                    />
                  </div>
                  <div className="center-phone">
                    {group.get('customerName')}
                  </div>
                  <div className="right-group">
                    <div className="left-info">
                      <span className="text1">
                        还差<span>
                          {group.get('grouponNum') - group.get('joinNum')}
                        </span>人成团
                      </span>
                      <span className="text2">距结束 </span>
                      <CountDown
                        colorStyle={{ color: '#ff1f4e' }}
                        visible={endTime && serverTime}
                        timeOffset={moment
                          .duration(endTime.diff(serverTime))
                          .asSeconds()
                          .toFixed(0)}
                      />
                    </div>
                    <div
                      className="right-btn"
                      onClick={
                        () => openSpecModal(group.get('grouponNo'))
                        //immediateJoin(group.get('grouponNo'), goodsInfoId)
                      }
                    >
                      立刻参团
                    </div>
                  </div>
                </li>
              );
            })}
        </ul>
      </div>
    );
  }
}
