import React from 'react';
import { Relax } from 'plume2';
import { toFixed2 } from 'wmkit/common/util';
import { fromJS } from 'immutable';
@Relax
export default class GoodsDetail extends React.Component<any, any> {
  static relaxProps = {
    goods: 'goods',
    activityInfo: 'activityInfo',
    goodsInfos: 'goodsInfos',
    goodsInfoId: 'goodsInfoId'
  };
  render() {
    const {
      goods,
      activityInfo,
      goodsInfos,
      goodsInfoId
    } = this.props.relaxProps;
    //删除团长的SKU
    const goodsInfo = goodsInfos
      .filter((info) => info.get('goodsInfoId') == goodsInfoId)
      .get(0);
    const leaderSku = goodsInfo ? goodsInfo : fromJS({});
    
    return (
      <div className="goods-container">
        <img
          className="goods-img"
          src={
            goods.get('goodsImg')
              ? goods.get('goodsImg')
              : require('../img/none.png')
          }
        />
        <div className="goods-right">
          <div className="goods-name">{goods.get('goodsName')}</div>
          {/* <div className="goods-spec">规格1 规格2 规格3</div> */}
          <div style={{marginLeft:'0.3rem'}}>
            <span className="goods-sale-price">
              ￥{leaderSku.get('grouponPrice')
                ? leaderSku.get('grouponPrice').toFixed(2)
                : '0.00'}
            </span>
            {/* <span className="sale-num">2件起售</span> */}
          </div>
          <div className="goods-price">
            <div className="goods-group">
              <span className="group-num">{activityInfo.get('grouponNum')}</span>
              <span className="group-buy">人团</span>
            </div>
            <span className="group-complete">
              {activityInfo.get('alreadyGrouponNum')}人已拼团
            </span>
          </div>
        </div>
      </div>
    );
  }
}
