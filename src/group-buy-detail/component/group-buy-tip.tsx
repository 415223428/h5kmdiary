import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { history, NumberInput, noop } from 'wmkit';

@Relax
export default class GroupBuyTip extends Component<any, any> {
  constructor(props) {
    super(props);
  }
  props: {
    relaxProps?: {
      grouponInst: IMap;
    };
  };
  static relaxProps = {
    grouponInst: 'grouponInst'
  };

  render() {
    const { grouponInst } = this.props.relaxProps;
    return (
      <div
        className="group-buy-detail-tip"
        onClick={() =>
          history.push('/group-buy-detail/' + grouponInst.get('grouponNo'))
        }
      >
        {/* <div className="tip-btn">
          <span className="tip-text">
            <i className="iconfont icon-laba" />
            1111邀请你来参团
          </span>
        </div> */}
        {grouponInst && grouponInst.get('customerName') ? (
          <div className="tip-btn">
            <span className="group-buy-tip-text">
              <i className="iconfont icon-laba" />
              {grouponInst.get('customerName')}邀请你来参团
            </span>
          </div>
        ) : null}
      </div>
    );
  }
}
