import React from 'react';
import { history } from 'wmkit';
export default class PlayWay extends React.Component<any, any> {
  render() {
    return (
      <div className="play-way-container">
        <div className="step-header">
          <span className="step-header-text bold">拼团玩法</span>
          <div onClick={() => history.push('/groupon-rule')}>
            <span className="step-header-text">玩法详情</span>
            <i className="iconfont icon-jiantou1" />
          </div>
        </div>
        <div className="step">
          <div className="step-one">
            <div className="step-num">1</div>
            <div className="step-text-container">
              <span className="step-text">选择商品</span>
              <span className="step-text">付款开团/参团</span>
            </div>
            <i className="iconfont icon-jiantou1" />
          </div>
          <div className="step-one">
            <div className="step-num">2</div>
            <div className="step-text-container">
              <span className="step-text">邀请并等待</span>
              <span className="step-text">好友支持参团</span>
            </div>
            <i className="iconfont icon-jiantou1" />
          </div>
          <div className="step-one">
            <div className="step-num">3</div>
            <div className="step-text-container">
              <span className="step-text">达到人数</span>
              <span className="step-text">顺利成团</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
