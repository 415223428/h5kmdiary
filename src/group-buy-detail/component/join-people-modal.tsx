import React, { Component } from 'react';
import { List, fromJS } from 'immutable';
import { Relax, IMap } from 'plume2';
import { NumberInput, noop } from 'wmkit';

const windowWidth = window.innerWidth.toString() + 'px';

@Relax
export default class JoinPeopleModal extends Component<any, any> {
  props: {
    relaxProps?: {
      customerVOList: any;
      toggleJoinPeopleModal: Function;
    };
  };

  static relaxProps = {
    customerVOList: 'customerVOList',
    toggleJoinPeopleModal: noop
  };

  render() {
    const { customerVOList, toggleJoinPeopleModal } = this.props.relaxProps;
    return (
      <div className="join-group-modal">
        <div className="head">参团人员</div>
        <ul className="join-group-list">
          {customerVOList.map((vo) => {
            return (
              <li>
                <div className="left-pointer">
                  <img
                    src={
                      vo.get('headimgurl')
                        ? vo.get('headimgurl')
                        : require('../img/default-img.png')
                    }
                  />
                </div>
              </li>
            );
          })}
        </ul>
        <div className="close">
          <img
            className="close-img"
            onClick={() => toggleJoinPeopleModal()}
            src={require('../img/close.png')}
          />
        </div>
      </div>
    );
  }
}
