import React from 'react'
import { Relax } from 'plume2'
// import Fetch from '../../../web_modules/wmkit/fetch';
import { noop, history, WMkit, _, Fetch } from 'wmkit'
import html2canvas from 'html2canvas'
import Canvas2Image from './canvas2image'
import { cache, config } from 'config';
import wx from 'weixin-js-sdk';
import { shareGoods } from 'wmkit/weixin/webapi';
import { fromJS } from 'immutable';

const windowWidth = window.innerWidth.toString() + 'px';
const wWidth = document.body.clientWidth;
const QRCode = require('qrcode.react')
const logo1 = require('../img/logo1.png')
const toimg = require('../img/toimg.png')
const download = require('../img/download.png')

@Relax
export default class GoodsShareImg extends React.Component<any, any> {
  static relaxProps = {
    customer: 'customer',
    goods: 'goods',
    activityInfo: 'activityInfo',
    goodsInfos: 'goodsInfos',
    goodsInfoId: 'goodsInfoId',
    isShow: 'isShow',
    changefenxiang: noop,
    sendId: noop,
    isfenxiang: 'isfenxiang',
    closefenxiang: noop,
    iscloseImg: noop,
    closeImg: 'closeImg',
    isopenImg: noop,
    fenxiangId: 'fenxiangId',
    closeChoose: 'closeChoose',
    openWechatShare: noop,
    isWechatShare: 'isWechatShare',
    grouponNo:'grouponNo',
  };

  render() {

    const {
      customer,
      goods,
      activityInfo,
      goodsInfos,
      goodsInfoId,
      isfenxiang,
      closefenxiang,
      iscloseImg,
      closeImg,
      isopenImg,
      fenxiangId,
      closeChoose,
      openWechatShare,
      isWechatShare,
      grouponNo
    } = this.props.relaxProps;

    let imgUrl = goods.get('goodsImg')
      ? goods.get('goodsImg')
      : require('../img/none.png');

    let headImg = customer.get('headImg')
      ? customer.get('headImg')
      : require('../../../web_modules/images/default-headImg.png');
    let newImgUrl;
    {
      imgUrl ?
        newImgUrl = imgUrl.replace(/kmdiary-com.oss-cn-shenzhen.aliyuncs.com/, "m.kmdiary.com/share-oss") : null
    }
    const goodsInfo = goodsInfos
      .filter((info) => info.get('goodsInfoId') == goodsInfoId)
      .get(0);
    const leaderSku = goodsInfo ? goodsInfo : fromJS({});
    const isdistributor = WMkit.isShowDistributionButton();
    const inviteeId = isdistributor ? JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId : '';
    const inviteCode = isdistributor ? sessionStorage.getItem('inviteCode') : '';
    const url = `group-buy-detail/${grouponNo}channel=mall&inviteeId=${inviteeId}&inviteCode=${inviteCode}`;
    console.log(grouponNo);
    let type = navigator.userAgent.toLowerCase().match(/MicroMessenger/i)
    return (
      <div>
        {isfenxiang  ?
          (
            <div className={closeImg ? `fenxiang` : `fenxiang2`} ref="imageWrapper" style={{ top: "0" }}
            >
              <div className="motaikuan"
                onClick={() => {
                  closefenxiang();
                  isopenImg();
                }}
              ></div>
              <div className="" id='saveimg' style={{ zIndex: 9999 }}>
                {
                  !closeImg && !isWechatShare ?
                    <img src={require('../img/tip.png')} alt="" style={{ width: "5.1rem" }}></img>
                    : null
                }
                {
                  !closeImg && isWechatShare ?
                    <img src={require('../img/wechatShare.png')} alt=""
                      style={{
                        width: "5.1rem",
                        top: '.2rem',
                        right: '1rem',
                        position: 'absolute'
                      }}></img>
                    : null
                }
                <div className="content-fenxiang address-box" id='fenxiang'>
                  {
                    closeImg ?
                      (
                        <div>
                          <div style={{ display: "flex", padding: '0.2rem 0.14rem', fontSize: '.28rem' }}>
                            <img src={headImg} style={{ width: '.9rem', height: '.9rem', display: 'block', borderRadius: '50%', marginRight: '.14rem' }} alt="" />
                            <div style={{ display: "flex", flexDirection: 'column', justifyContent: 'space-between', textAlign: 'left' }}>
                              <div style={{ color: '#222', fontWeight: 'bold' }}>{customer.get('customerName')}</div>
                              <div style={{ color: '#444' }}>给你分享一个产品，快来看看吧~</div>
                            </div>
                          </div>
                          <div style={{ display: "flex", justifyContent: "center" }}>
                            <img
                              className="sssimg"
                              // src={images.get(0)}
                              src={newImgUrl}
                              alt=""
                            ></img>
                          </div>
                          <div className="goods-name">{goods.get('goodsName')}</div>
                          <div style={{ marginLeft: '0.3rem' }}>
                            <span className="goods-sale-price" style={{display:'flex'}}>
                              ￥{leaderSku.get('grouponPrice')
                                ? leaderSku.get('grouponPrice').toFixed(2)
                                : '0.00'}(拼团价)
                            </span>
                            {/* <span className="sale-num">2件起售</span> */}
                          </div>
                          <div className="anniu">
                            <div>
                              <QRCode value={`https://m.kmdiary.com/${url}`} style={{ margin: ".2rem", width: "1.4rem", height: "1.4rem" }} />
                            </div>
                            <img src={logo1} alt="" style={{ borderRadius: 0 }} />
                          </div>
                        </div>
                      ) : null
                  }
                </div>

              </div>
              {
                closeChoose ?

                  <div className="quxiao">
                    <div className="tupian">
                      {type !== null ?
                        <div className="tupiandiv"
                          onClick={() => {
                            // this._downloadImg();
                            openWechatShare();
                            iscloseImg();
                            this._wechatShare(url, goodsInfo);
                          }}
                        >
                          <img src={require('../img/wechat.png')}></img>
                          微信好友
                        </div>
                        : null
                      }
                      <div className="tupiandiv"
                        onClick={() => {
                          let cntElem = document.getElementById('fenxiang');
                          var width = cntElem.offsetWidth; //获取dom 宽度
                          var height = cntElem.offsetHeight; //获取dom 高度
                          this._downloadImg(width, height);
                          iscloseImg();
                        }}
                      >
                        <img src={require('../img/imgShare.png')}></img>
                        生产海报分享
                  </div>
                    </div>
                    <div className="wenzi"
                      onClick={() => {
                        closefenxiang();
                        isopenImg();
                      }}
                    >
                      取消
                        </div>
                  </div> : null
              }
            </div>
          ) : null}
      </div>
    )
  }
  _downloadImg = (width, height) => {
    var canvas = document.createElement("canvas"); //创建一个canvas节点
    var scale = 2; //定义任意放大倍数 支持小数
    canvas.width = width * scale; //定义canvas 宽度 * 缩放
    canvas.height = height * scale; //定义canvas高度 *缩放
    canvas.getContext("2d").scale(scale, scale)
    const opts = {
      allowTaint: true, // 允许加载跨域的图片 （使用这个会报错）
      useCORS: true,  // 允许加载跨域图片
      tainttest: true, // 检测每张图片都已经加载完成
      backgroundColor: null,
      scale: scale
    }
    html2canvas(document.querySelector("#fenxiang"), opts).then(function (canvas) {
      var context = canvas.getContext('2d');
      // context.mozImageSmoothingEnabled = false;
      // context.webkitImageSmoothingEnabled = false;
      // context.msImageSmoothingEnabled = false;
      context.imageSmoothingEnabled = false;
      var img = Canvas2Image.convertToPNG(canvas, canvas.width, canvas.height)
      var detail = document.getElementById("fenxiang");
      detail.appendChild(img);
      // document.body.appendChild(img);

      img.style.width = `6.11rem`;
      img.style.height = `9.35rem`;
      // console.log(canvas.toDataURL());
      // canvas.toBlob(function(blob){
      //   (window as any).shareImg(blob);
      // }, "image/jpeg", 0.95);
      (window as any).shareImg(canvas.toDataURL());
    })
  };

  _wechatShare = (url, goodsInfo) => {
    let that = this
    Fetch('/third/share/weChat/getSign', {
      method: 'POST',
      body: JSON.stringify({
        url: location.href
      })
    }).then((res) => {
      if (res.code == config.SUCCESS_CODE) {
        that._shareLinkToFriend(res.context, `http://m.kmdiary.com/${url}`, goodsInfo)
      }
    })
  };


  _close = () => {
    this.state.cloasefenxiang();
    this.state.isopenImg();
  }


  /**
 * 根据设价方式,计算显示的价格
 * @returns 显示的价格
 * @private
 */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };


  /**
 * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
 * @private
 */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
  };


  /**
   * 分享链接给微信好友
   * @private
   * @param info 获取后台生成的微信分享参数（签名，时间戳）
   * @param url 分享的链接
   * */
  _shareLinkToFriend = (info, url, goodsInfo) => {
    let title, imgUrl, desc;
    title = goodsInfo.get('goodsInfoName');
    imgUrl = goodsInfo.get('goodsInfoImg');
    desc = `￥ ${_.addZero(goodsInfo.get('marketPrice'))}元`
    const { appId, timestamp, nonceStr, signature } = info;
    wx.config({
      debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
      appId: appId, // 必填，公众号的唯一标识
      timestamp: timestamp, // 必填，生成签名的时间戳
      nonceStr: nonceStr, // 必填，生成签名的随机串
      signature: signature, // 必填，签名，见附录1
      jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    });

    wx.ready(() => {
      //分享给朋友
      wx.onMenuShareAppMessage({
        title: title, // 分享标题
        desc: desc, // 分享描述
        link: url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl: imgUrl, // 分享图标
        // type: '', // 分享类型,music、video或link，不填默认为link
        // dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
        success: function (ret) {
          // 用户确认分享后执行的回调函数
          // afterSuccess();
          // alert(ret.errMsg)
          if (WMkit.isLogin()) {
            shareGoods();
          }
        },
        cancel: function () {
          // 用户取消分享后执行的回调函数
          // onCancel();
        },
        fail: (err) => {
          // alert(err.errMsg)
        }
      });
    })
  };
}
