import React, { Component } from 'react';
import { List, fromJS } from 'immutable';
import { Relax, IMap } from 'plume2';
import moment from 'moment';
import { NumberInput, ListView, noop, CountDown } from 'wmkit';

const windowWidth = window.innerWidth.toString() + 'px';

@Relax
export default class WaitGroupModal extends Component<any, any> {
  static relaxProps = {
    activityInfo: 'activityInfo',
    toggleWaitGroupModal: noop,
    handleModalDataReached: noop,
    modalRefresh: 'modalRefresh',
    modalJoinGroup: noop,
    serverTime: 'serverTime'
  };

  render() {
    const {
      activityInfo,
      toggleWaitGroupModal,
      handleModalDataReached,
      modalRefresh,
      modalJoinGroup,
      serverTime
    } = this.props.relaxProps;
    return (
      <div className="wait-group-modal">
        <img className="head" src={require('../img/head1.png')} />
        <ListView
          style={{ height: '6.5rem' }}
          className="wait-group-list"
          url="/groupon/instance/page"
          params={{
            grouponActivityId: activityInfo.get('grouponActivityId')
          }}
          dataPropsName="context.grouponInstanceVOS.content"
          renderRow={(item: any) => {
            const endTime = moment(item.endTime);
            return (
              <li>
                <div className="left-pointer">
                  <img
                    src={
                      item.headimgurl
                        ? item.headimgurl
                        : require('../img/default-img.png')
                    }
                  />
                </div>
                <div className="center-phone">{item.customerName}</div>
                <div className="right-group">
                  <div className="left-info">
                    <span className="text1">
                      还差<span>{item.grouponNum - item.joinNum}</span>人成团
                    </span>
                    <span className="text2">距结束</span>
                    <CountDown
                      colorStyle={{ color: '#999' }}
                      visible={endTime && serverTime}
                      timeOffset={moment
                        .duration(endTime.diff(serverTime))
                        .asSeconds()
                        .toFixed(0)}
                    />
                  </div>
                  <div
                    className="right-btn"
                    onClick={() => modalJoinGroup(item.grouponNo)}
                  >
                    立刻参团
                  </div>
                </div>
              </li>
            );
          }}
          onDataReached={handleModalDataReached}
        />
        <div className="close">
          <img
            className="close-img"
            onClick={() => toggleWaitGroupModal()}
            src={require('../img/close.png')}
          />
        </div>
      </div>
    );
  }
}
