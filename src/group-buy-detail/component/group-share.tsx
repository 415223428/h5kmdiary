import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import wx from 'weixin-js-sdk';
import { fromJS } from 'immutable';
import { IList } from 'typings/globalType';
import { noop, _, WMkit, Check, Fetch, Alert } from 'wmkit';
import { config } from 'config';

@Relax
export default class GroupShare extends Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
  }

  static relaxProps = {
    groupShareModal: 'groupShareModal',
    toggleGroupShareModal: noop,
    spuContext: 'spuContext',
    goods: 'goods',
    goodsInfos: 'goodsInfos',
    goodsInfoId: 'goodsInfoId',
    grouponNo: 'grouponNo'
  };

  render() {
    const { groupShareModal, toggleGroupShareModal } = this.props.relaxProps;
    return (
      <div className="goods-share">
        {groupShareModal && (
          <div className="poper">
            <div
              className="mask"
              onClick={() => {
                toggleGroupShareModal();
              }}
            />
            <div className="poper-content sharingLayer">
              <div className="share-title">分享给好友</div>
              <div
                className="icon-content"
                style={{ justifyContent: 'center' }}
              >
                <div
                  className="icon"
                  onClick={() => {
                    this.jumpSmallProgram();
                  }}
                >
                  <img src={require('../img/photo-album.png')} alt="" />
                  <p>图文分享</p>
                </div>
              </div>
              <div
                className="share-btn-pro b-1px-t"
                onClick={() => {
                  toggleGroupShareModal(false);
                }}
              >
                取消分享
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }

  /**
   * 跳转到微信小程序图文分享
   */
  jumpSmallProgram = () => {
    const {
      goods,
      goodsInfos,
      goodsInfoId,
      spuContext,
      grouponNo
    } = this.props.relaxProps;

    const skuId = spuContext.get('skuId');
    //筛出团长的SKU
    const goodsInfo = goodsInfos
      .filter((info) => info.get('goodsInfoId') == goodsInfoId)
      .get(0);
    const leaderSku = goodsInfo ? goodsInfo : fromJS({});
    //拼装小程序图文分享需要的参数
    let params = {
      goodsInfoName: goodsInfo.get('goodsInfoName'),
      //拼团价
      grouponPrice: goodsInfo.get('grouponPrice'),
      delPrice: goods.get('marketPrice'),
      skuId: skuId,
      spuId: goods.get('goodsId'),
      //邀请参团
      shareType: 3,
      specText: leaderSku.get('specText'),
      grouponNo: grouponNo,
      goodsInfoImg: goods.get('goodsImg')
    };
    wx.miniProgram.navigateTo({
      url: `../canvas/canvas?data=${JSON.stringify(params)}`
    });
  };
}
