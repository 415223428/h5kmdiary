import { Action, Actor } from 'plume2';
import { fromJS } from 'immutable';

export default class ActivityActor extends Actor {
  defaultState() {
    return {
      activityInfo: fromJS({}),
      grouponDetails: fromJS({}),
      //业务类型，决定触发的事件,默认展示我要参团
      grouponDetailOptStatus: 4,
      grouponNo: ''
    };
  }

  @Action('init :activityInfo')
  activityInfo(state, res) {
    return state.update('activityInfo', (activityInfo) =>
      activityInfo.merge(res)
    );
  }

  @Action('init :grouponDetails')
  grouponDetails(state, res) {
    return state
      .update('grouponDetails', (grouponDetails) => grouponDetails.merge(res))
      .set('grouponDetailOptStatus', res.grouponDetailOptStatus);
  }

  @Action('save :grouponNo')
  grouponNo(state, grouponNo) {
    return state.set('grouponNo', grouponNo);
  }
}
