import { Action, Actor } from 'plume2';
import { fromJS } from 'immutable';

export default class OtherGroupActor extends Actor {
  defaultState() {
    return {
      grouponInstanceList: fromJS([]),
      customerVOList: fromJS([]),
      modalRefresh: false
    };
  }

  @Action('init :grouponInstanceList')
  activityInfo(state, res) {
    return state.update('grouponInstanceList', (grouponInstanceList) =>
      grouponInstanceList.merge(res)
    );
  }

  @Action('init :customerVOList')
  customerVOList(state, res) {
    return state.update('customerVOList', (customerVOList) =>
      customerVOList.merge(res)
    );
  }

  @Action('modal: list: refresh')
  modalRefresh(state, modalRefresh) {
    return state.set('modalRefresh', modalRefresh);
  }
}
