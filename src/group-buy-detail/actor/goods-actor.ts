import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';
import { noop } from 'wmkit';
export default class GoodsActor extends Actor {
  defaultState() {
    return {
      goods: fromJS({}),
      goodsInfos: fromJS([]),
      //团长的skuid
      goodsInfoId: '',
      serverTime: 0,
      spuContext: fromJS({}),
      loading: true,
      specModal: false,
      waitGroupModal: false,
      toRefresh: false,
      //邀请参团弹出
      groupShareModal: false,
      //参团人员弹窗
      joinPeopleModal: false,
      //点击我也开个团或者立即参团存储的团号，开团情况团号为空，参团情况，直接存储当前团号
      targetGroupNo: '',
      isShow: false,
      isfenxiang:false,
      closeImg:true,
      fenxiangId:'',
      closeChoose:true,
      customer: {},
    };
  }

  @Action('init :goods')
  activityInfo(state, res) {
    return state.update('goods', (goods) => goods.merge(res));
  }

  @Action('init :goodsInfos')
  initGoodsInfos(state, res) {
    return state.update('goodsInfos', (goodsInfos) => goodsInfos.merge(res));
  }

  @Action('init :goodsInfoId')
  goodsInfoId(state, goodsInfoId) {
    return state.set('goodsInfoId', goodsInfoId);
  }

  @Action('init :serverTime')
  serverTime(state, time) {
    return state.set('serverTime', time);
  }

  @Action('init :spuContext')
  spuContext(
    state,
    {
      skuId,
      goodsInfos,
      goods,
      goodsPropDetailRels,
      goodsSpecs,
      goodsSpecDetails,
      goodsLevelPrices,
      goodsCustomerPrices,
      goodsIntervalPrices,
      images
    }
  ) {
    return state.set(
      'spuContext',
      fromJS({
        skuId,
        goodsInfos,
        goods,
        goodsPropDetailRels,
        goodsSpecs,
        goodsSpecDetails,
        goodsLevelPrices,
        goodsCustomerPrices,
        goodsIntervalPrices,
        images
      })
    );
  }

  @Action('loading :end')
  loadingEnd(state) {
    return state.set('loading', false);
  }

  @Action('toggle :specModal')
  specModal(state) {
    return state.set('specModal', !state.get('specModal'));
  }

  @Action('toggle :toggleWaitGroupModal')
  toggleWaitGroupModal(state) {
    return state.set('waitGroupModal', !state.get('waitGroupModal'));
  }

  @Action('goods: list: refresh')
  torefresh(state, toRefresh) {
    return state.set('toRefresh', toRefresh);
  }

  @Action('toggle :groupShareModal')
  groupShareModal(state) {
    return state.set('groupShareModal', !state.get('groupShareModal'));
  }

  @Action('toggle :toggleJoinPeopleModal')
  toggleJoinPeopleModal(state) {
    return state.set('joinPeopleModal', !state.get('joinPeopleModal'));
  }

  @Action('loading :start')
  loadingStart(state) {
    return state.set('loading', true);
  }

  @Action('group:targetGroupNo')
  targetGroupNo(state, groupNo) {
    return state.set('targetGroupNo', groupNo);
  }

  @Action('goods:isShow')
  setIsShow(state, flag) {
    return state.set('isShow', flag);
  }
//打开分享页面
  @Action('goods-actor:isfenxiang')
  isfenxiang(state){
    return state.set('isfenxiang',true)
  }
//关闭分享页面
  @Action('goods-actor:closefenxiang')
  closefenxiang(state){
    return state.set('isfenxiang',false)
  }
  //关闭img
  @Action('goods-actor:closeImg')
  closeImg(state){
    return state
    .set('closeImg',false)
    .set('closeChoose',false)
  }
  //打开Img
  @Action('goods-actor:openImg')
  openImg(state){
    return state
    .set('closeImg',true)
    .set('closeChoose',true)

  }
  @Action('goods-actor:openWechatShare')
  openWechatShare(state){
    return state.set('isWechatShare',true)
  }

  @Action('goods-actor:closeWechatShare')
  closeWechatShare(state){
    return state.set('isWechatShare',false)
  }

  @Action('member:init')
  init(state: IMap, { customer }) {
    return state
      .set('customer', fromJS(customer))
  }
}
