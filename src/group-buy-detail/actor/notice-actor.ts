import { Actor, Action, IMap } from 'plume2';
import { fromJS } from 'immutable';
import { config } from 'config';

export default class NoticeActor extends Actor {
  defaultState() {
    return {
      //直接参团信息
      grouponInst: {},
      //团活动信息
      grouponActivityId: '',
      //最新显示的团编号
      noticeGrouponNo: '',
      //后台通知的团编号
      grouponNoWait: '',
      // 团tip显示标记
      tips: false,
      // websocket注册的后台地址
      url: config.BFF_HOST + '/endpoint',
      // websocket监听主题
      topics: ['/topic/getGrouponInstance']
    };
  }

  /**
   * 团信息
   * @param grouponActivity
   * @param grouponInsts
   * @param openButton
   * @returns {IMap}
   */
  @Action('notice: grouponInst')
  initGrouponInst(state, grouponInst) {
    //团活动信息
    return state.set('grouponInst', fromJS(grouponInst));
  }
  /**
   * 最新显示的团编号
   */
  @Action('notice: change: grouponNo')
  changeGrouponNo(state, grouponNo) {
    return state.set('noticeGrouponNo', grouponNo);
  }
  /**
   * 最新显示的团编号
   */
  @Action('notice: change: grouponNoWait')
  changeGrouponNoWait(state, grouponNo) {
    return state.set('grouponNoWait', grouponNo);
  }

  /**
   * url
   */
  @Action('notice: init: url')
  initUrl(state, url) {
    return state.set('url', url);
  }

  /**
   * topicss
   */
  @Action('notice: init: topics')
  initTopic(state, topics) {
    return state.set('topics', topics);
  }
  /**
   * 是否显示参团tip通知
   */
  @Action('notice :toggleTips')
  toggleWaitGroupModal(state) {
    return state.set('tips', !state.get('tips'));
  }
}
