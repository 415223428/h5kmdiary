import { fromJS } from 'immutable';

import { IOptions, Store, msg } from 'plume2';
import { history, Alert, Confirm, _, WMkit } from 'wmkit';
import { config, cache } from 'config';
import GoodsActor from './actor/goods-actor';
import ActivityActor from './actor/activity-actor';
import OtherGroupActor from './actor/other-group-actore';
import NoticeActor from './actor/notice-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [
      new GoodsActor(),
      new ActivityActor(),
      new OtherGroupActor(),
      new NoticeActor()
    ];
  }

  init = async (grouponNo) => {
    const { code, message, context } = await webapi.grouponDetail(grouponNo);
    if (code == config.SUCCESS_CODE) {
      //团长的sku能否在返回的spu列表中找到
      if (
        fromJS((context as any).goodsInfos).find(
          (sku) =>
            sku.get('goodsInfoId') == (context as any).grouponDetails.goodInfoId
        )
      ) {
        //存储团编号
        this.dispatch('save :grouponNo', grouponNo);
        //拼团信息
        const grouponDetails = (context as any).grouponDetails;
        this.dispatch('init :grouponDetails', grouponDetails);
        //团长的skuId
        this.dispatch('init :goodsInfoId', grouponDetails.goodInfoId);
        //团活动
        this.dispatch(
          'init :activityInfo',
          fromJS(grouponDetails.grouponActivity)
        );
        //spu信息
        this.dispatch('init :goods', fromJS((context as any).goods));
        //sku信息
        this.dispatch('init :goodsInfos', fromJS((context as any).goodsInfos));
        //参团人员
        this.dispatch(
          'init :customerVOList',
          fromJS(grouponDetails.customerVOList)
        );
        //其他团信息
        this.dispatch(
          'init :grouponInstanceList',
          (context as any).grouponInstanceList
        );
        this.dispatch('loading :end');
        //赋予skuId
        (context as any).skuId = grouponDetails.goodInfoId;
        this.dispatch('init :spuContext', context);
        //获取服务时间
        const serverTime = await webapi.queryServerTime();
        //存储服务时间
        this.dispatch('init :serverTime', serverTime);
      } else {
        history.replace('/goods-failure');
      }
    } else {
      history.replace('/goods-failure');
    }
  };

  /**
   * 获取精选拼团
   */
  // fetchGrouponCenterList = async () => {
  //   const { code, message, context } = await webapi.fetchGrouponCenterList();
  // };

  doGroup = async () => {
    let grouponDetailOptStatus = this.state().get('grouponDetailOptStatus');
    //我要参团或者我也开个团
    if (grouponDetailOptStatus == 4 || grouponDetailOptStatus == 5) {
      if (WMkit.isLogin()) {
        //先校验
        this.immediateJoin(
          this.state().get('grouponNo'),
          this.state().get('goodsInfoId'),
          grouponDetailOptStatus
        );
      } else {
        msg.emit('loginModal:toggleVisible', {
          callBack: () => {
            history.push('/group-buy-detail/' + this.state().get('grouponNo'));
          }
        });
      }
    }
    //邀请参团，弹出分享弹框
    if (grouponDetailOptStatus == 8) {
      if ((window as any).isMiniProgram == true) {
        this.toggleGroupShareModal();
      } else {
        Alert({
          text: '请到小程序端进行该操作！'
        });
      }
    }
    //查看其它团购，跳转至拼团首页
    if (grouponDetailOptStatus == 7 || grouponDetailOptStatus == 2) {
      history.push('/groupon-center');
    }
  };

  toggleSpecModal = () => {
    this.dispatch('toggle :specModal');
  };

  toggleWaitGroupModal = () => {
    this.dispatch('toggle :toggleWaitGroupModal');
  };

  handleDataReached = (data: any) => {
    if (data.code !== config.SUCCESS_CODE) {
      return false;
    } else {
      this.initToRefresh(false);
    }
  };
  /**
   * 等待成团实例弹框的分页处理
   */
  handleModalDataReached = (data) => {
    if (data.code !== config.SUCCESS_CODE) {
      return false;
    } else {
      this.initModalToRefresh(false);
    }
  };

  initToRefresh = (flag) => {
    this.dispatch('goods: list: refresh', flag);
  };

  initModalToRefresh = (flag) => {
    this.dispatch('modal: list: refresh', flag);
  };

  modalJoinGroup = (grouponNo) => {
    if (WMkit.isLogin()) {
      //存储团标号
      this.dispatch('group:targetGroupNo', grouponNo);
      //隐藏团实例弹框
      this.toggleWaitGroupModal();
      //显示规格弹框
      this.toggleSpecModal();
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          history.push('/group-buy-detail/' + this.state().get('grouponNo'));
        }
      });
    }
  };

  toggleGroupShareModal = () => {
    this.dispatch('toggle :groupShareModal');
  };

  /**
   * 后台推送待提示的团消息no
   */
  initGrouponNoWait = (grouponInstanceVO) => {
    const grouponNo = grouponInstanceVO.grouponNo;
    const grouponActivityId = grouponInstanceVO.grouponActivityId;
    let grouponActivityIdLive = '';
    if (this.state().get('activityInfo').size > 0) {
      grouponActivityIdLive = this.state()
        .get('activityInfo')
        .get('grouponActivityId');
    }
    // 只记录统一活动的商品
    if (grouponActivityId == grouponActivityIdLive) {
      this.dispatch('notice: change: grouponNoWait', grouponNo);
    }
  };

  /**
   * 俩秒钟执行一次（避免后台团数据过多高频率弹出）
   * 判断grouponNoWait 和grouponNo（是否有新开团）
   * 如果有根据团no获取团实例信息
   */
  notice = () => {
    setInterval(() => this.initGrouponNotice(), 1000 * 2);
  };

  initGrouponNotice = async () => {
    const noticeGrouponNo = this.state().get('noticeGrouponNo');
    const grouponNoWait = this.state().get('grouponNoWait');
    if (
      // 第一次
      ('' == noticeGrouponNo && '' != grouponNoWait) ||
      // 第N次
      grouponNoWait != noticeGrouponNo
    ) {
      // 查询团实例信息
      const res = (await webapi.initGrouponNotice(grouponNoWait)) as any;
      if (res.code == config.SUCCESS_CODE) {
        this.dispatch('notice: grouponInst', res.context);
        this.dispatch('notice: change: grouponNo', res.context.grouponNo);
        this.toggleNotice();
        this.timeOut();
      }
    }
  };

  timeOut = () => {
    setTimeout(() => {
      this.toggleNotice();
    }, 1000 * 2);
  };

  toggleNotice = () => {
    this.dispatch('notice :toggleTips');
  };

  immediateJoin = async (grouponNo, goodsInfoId, status) => {
    //已登录,status==4,表示参团，status==5，表示开团
    const { code, message } = await webapi.immediateBuyValidate({
      customerId: JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId,
      grouponNo: status == 5 ? null : grouponNo,
      goodsInfoId: goodsInfoId,
      openGroupon: status == 5
    });
    if (code != config.SUCCESS_CODE) {
      if (code == 'K-050402' || code == 'K-050403') {
        Alert({
          text: '已参与该团!'
        });
      }
      if (code == 'K-050401' || code == 'K-050404') {
        Alert({
          text: message
        });
        //团状态改变了，要刷页面
        this.endHandle(grouponNo);
      } else {
        Alert({
          text: message
        });
      }
    } else {
      //弹出规格框
      if (status == 5) {
        //我要开团，存储的团号为空
        this.openSpecModal('');
      } else {
        this.openSpecModal(grouponNo);
      }
    }
  };

  /**
   * 参团人员弹窗
   */
  toggleJoinPeopleModal = () => {
    this.dispatch('toggle :toggleJoinPeopleModal');
  };

  endHandle = (grouponNo) => {
    this.dispatch('loading :start');
    this.init(grouponNo);
    this.notice();
  };

  openSpecModal = (groupNo) => {
    //打开弹窗
    this.toggleSpecModal();
    //存储团号
    this.dispatch('group:targetGroupNo', groupNo);
  };

    //改变分享页面的开关
    changefenxiang = () => {
      this.dispatch('goods-actor:isfenxiang');
    };
    closefenxiang = () => {
  
      this.dispatch('goods-actor:closefenxiang');
      this.dispatch('goods-actor:closeWechatShare');
    };
  
    iscloseImg = () => {
      this.dispatch('goods-actor:closeImg');
    };
  
    isopenImg = () => {
      this.dispatch('goods-actor:openImg');
    };
  
    //传入对应的分享页面Id
    sendId = (fenxiangId) => {
      this.dispatch('goods-actor:fenxiangId', fenxiangId);
    }
  
    openWechatShare=()=>{
      this.dispatch('goods-actor:openWechatShare');
    }

    memberInfo = async () => {
      const res = (await Promise.all([
        webapi.shareInfo()
      ])) as any;
        this.dispatch('member:init', {
          customer: res[0].context,
        });
    };
}
