import React from 'react';
import { fromJS } from 'immutable';
import { IMap, StoreProvider } from 'plume2';
import './css/style.css';
import AppStore from './store';
import Share from '../share'
import GoodsDetail from './component/goods-detail';
import DetailBottom from './component/detail-bottom';
import PlayWay from './component/play-way';
import GoodsList from './component/goods-list';
import JoinGroup from './component/join-group';
import Notice from './component/notice';
import GroupBuyTip from './component/group-buy-tip';
import JoinPeopleModal from './component/join-people-modal';
import { WMGrouponChoose,WMkit } from 'wmkit';
import WaitGroupModal from './component/wait-group-modal';
import GroupShare from './component/group-share';
import GoodsShareImg from './component/goods-share-img'

@StoreProvider(AppStore, { debug: __DEV__ })
export default class OrderList extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { gid } = this.props.match.params;
    this.store.init(gid);
    this.store.notice();
    if (WMkit.isLoginOrNotOpen()) {
      this.store.memberInfo();}
  }

  render() {
    const tips = this.store.state().get('tips');
    const openShare = this.store.state().get('openShare');
    return (
      !this.store.state().get('loading') && (
        <div className="container" style={{ padding: '0',height:'100vh',overflowX:'hidden' }}>
          <GoodsShareImg />
          <Notice />
          {tips && <GroupBuyTip />}
          <GoodsDetail />
          <DetailBottom />
          <PlayWay />
          {this.store.state().get('grouponDetailOptStatus') == 5 &&
            this.store
              .state()
              .get('grouponInstanceList')
              .count() > 0 && <JoinGroup />}
          <GoodsList />
          {this.store.state().get('joinPeopleModal') && <JoinPeopleModal />}
          {this.store.state().get('waitGroupModal') && <WaitGroupModal />}

          <WMGrouponChoose
            openGroupon={
              this.store.state().get('grouponDetailOptStatus') == 5 &&
                this.store.state().get('targetGroupNo') == ''
                ? true
                : false
            }
            grouponData={this.store
              .state()
              .get('grouponDetails')
              .toJS()}
            data={this.store
              .state()
              .get('spuContext')
              .toJS()}
            visible={this.store.state().get('specModal')}
            changeSpecVisible={this.store.toggleSpecModal}
            dataCallBack={() => { }}
            grouponNo={
              this.store.state().get('grouponDetailOptStatus') == 5 &&
                this.store.state().get('targetGroupNo') == ''
                ? null
                : this.store.state().get('targetGroupNo')
            }
          />
          <GroupShare />
        </div>
      )
    );
  }
}
