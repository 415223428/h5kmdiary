import React, { Component } from 'react';
import { Relax } from 'plume2';
import { List } from 'immutable';
import { history } from 'wmkit';

@Relax
export default class ImgSlider extends Component<any, any> {
  props: {
    relaxProps?: {
      grouponAdvert: List<any>;
    };
  };

  static relaxProps = {
    grouponAdvert: 'grouponAdvert'
  };

  render() {
    const { grouponAdvert } = this.props.relaxProps;

    return (
      <div className="swiper-container banner-swiper" id="swiper-container1">
        <div className="swiper-wrapper">
          {grouponAdvert.map((advert, index) => {
            return (
              <div className="swiper-slide" key={index}>
                <img
                  className="groupon-img"
                  src={advert.get('artworkUrl')}
                  alt=""
                  onClick={() =>
                    advert.get('linkGoodsInfoId') &&
                    history.push(
                      '/spellgroup-detail/' + advert.get('linkGoodsInfoId')
                    )
                  }
                />
              </div>
            );
          })}
        </div>
        <div className="swiper-pagination" />
      </div>
    );
  }
}
