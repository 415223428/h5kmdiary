import * as React from 'react';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';
import SelectListTopItem from './select-list-top-item';
import { noop } from 'wmkit';

@Relax
export default class SelectListTop extends React.Component<any, any> {
  props: {
    relaxProps?: {
      chooseCateId: string;
      grouponCates: IList;
      saveOffsetTop: Function;
    };
  };

  static relaxProps = {
    chooseCateId: 'chooseCateId',
    grouponCates: 'grouponCates',
    saveOffsetTop: noop
  };
  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const { chooseCateId, grouponCates } = this.props.relaxProps;

    return (
      <div style={{marginTop:'0.4rem'}}>
        <div className={'select-list-title'} id="selectListTitle" style={{justifyContent:'flex-start',paddingTop:'0.4rem'}}>
          <img src={require('../img/selected-purchase.png')} alt="" style={{width:'0.4rem',height:'0.4rem',marginLeft:'0.2rem'}}/>
          <span style={{color:'#000'}}>精选拼团</span>
          {/* <img src={require('../img/title.png')} alt="" /> */}
        </div>
        <div style={{ height: '40px'}}>
          <div className={'layout-top'} id="layout-top">
            <div className="layout-content" style={{borderBottom:'1px solid #e6e6e6', width:'100%'}}>
              {grouponCates.map((o) => (
                <SelectListTopItem
                  key={o.get('grouponCateId')}
                  label={o.get('grouponCateName')}
                  active={
                    chooseCateId
                      ? o.get('grouponCateId') === chooseCateId
                      : o.get('defaultCate') == 1
                  }
                  defaultCate={o.get('defaultCate')}
                  tabKey={o.get('grouponCateId')}
                />
              ))}  
            </div>
          </div>
        </div>
      </div>
    );
  }
}
