import * as React from 'react';
import { Relax } from 'plume2';
import { div } from 'wmkit/common/util';
import { IList } from 'typings/globalType';
import { history } from 'wmkit';

const noneImg = require('../img/none.png');

@Relax
export default class HotRecommend extends React.Component<any, any> {
  props: {
    relaxProps?: {
      grouponHotList: IList;
    };
  };

  static relaxProps = {
    grouponHotList: 'grouponHotList'
  };

  render() {
    const { grouponHotList } = this.props.relaxProps;

    return (
      <div className="hot-recommen" style={{marginTop:'0.6rem'}}>
        <div className="hot-title">
          <div className="hot-title-left">
            {/* <span>热门推荐</span> */}
            <img src={require('../img/worth-purchase.png')} alt="超值拼团"/>
            <span>超值拼团</span>
          </div>
          <div className="hot-title-right">
            <span />
            <span />
          </div>
        </div>
        <div className="swiper-container" id="swiper-container2">
          <div className="swiper-wrapper">
            {grouponHotList.map((item, index) => {
              return (
                <div className="swiper-slide" key={index}>
                  <div className="hot-list">
                    {item.map((info) => {
                      return (
                        <a
                          key={info.get('goodsId')}
                          className="hot-list-item"
                          onClick={() =>
                            history.push(
                              '/spellgroup-detail/' + info.get('goodsInfoId')
                            )
                          }
                        >
                          <div className="img-box">
                            <img
                              src={
                                info.get('goodsImg')
                                  ? info.get('goodsImg')
                                  : noneImg
                              }
                              alt=""
                            />
                          </div>
                          <span className="hot-name">
                            {info.get('goodsName')}
                          </span>
                          <div className="hot-price">
                            <span>
                              ￥{info.get('grouponPrice').toFixed(2) || '0.00'}
                            </span>
                          </div>
                          <div className="hot-num">
                            <span className="hot-join-num span-overflow">
                              {info.get('alreadyGrouponNum')}人已拼团
                            </span>
                            <span className="group">{info.get('grouponNum')}人团</span>
                          </div>
                        </a>
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
