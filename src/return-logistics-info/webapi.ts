/**
 * Created by feitingting on 2017/8/3.
 */
import {Fetch} from 'wmkit'
export const fetchDeliveryDetail = (code: string, id: string) => {
  return Fetch('/trade/deliveryInfos', {
    method: 'POST',
    body: JSON.stringify({
      companyCode:code,
      deliveryNo:id
    })
  })
}

export const fetchCompanyInfo = (id: string) => {
  return Fetch(`/return/returnLogistics/${id}`, {
    method: 'GET',
  })
}