import React from 'react'
import {StoreProvider} from 'plume2'
import AppStore from './store'
import Status from  './component/status'
import Logistics from  './component/logistics'
const icon = require('./component/img/none.png')

@StoreProvider(AppStore, {debug: __DEV__})
export default class ReturnLogisticsInfo extends React.Component<any, any> {

  store: AppStore


  componentWillMount() {
    const {rid} = this.props.match.params
    this.store.fetchBasicInfo(rid)
  }


  render() {

    return (
      <Logistics/>
    )
  }
}


