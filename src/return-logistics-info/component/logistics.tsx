import React from 'react';
import { Relax } from 'plume2';
import AppStore from '../store';
import Status from './status';
const styles = require('../css/style.css');
// const icon = require('../component/img/none.png');
const icon = require('../component/img/meiyou.png');

@Relax
export default class Logistics extends React.Component<any, any> {
  store: AppStore;

  props: {
    relaxProps?: {
      detail: any;
      result: boolean;
    };
  };

  static relaxProps = {
    detail: 'detail',
    result: 'result'
  };

  render() {
    const { result, detail } = this.props.relaxProps;

    return (
      <div style={{
        background:'#fafafa'
      }}>
        <Status />
        <div className={result ? 'list-none hide' : 'list-none showblock'}>
          <img src={icon} />
          <span style={style.darktext}>暂无相关物流信息</span>
        </div>
        {result && (
          <div>
            <h2 className="order-title">物流信息</h2>
            <ul className={styles.box}>
              {detail.map((v) => {
                return (
                  <li className={styles.cur}>
                    <div>
                      <span />
                      {v.context}
                    </div>
                    <p className="time">{v.time}</p>
                  </li>
                );
              })}
            </ul>
          </div>
        )}
      </div>
    );
  }
}

const style = {
  darktext: {
    color: ' #333',
    fontSize: '0.28rem',
    margin: '10px 0 0 0',
    lineHeight: '25px',
  }
};
