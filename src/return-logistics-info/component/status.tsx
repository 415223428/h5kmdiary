import React from 'react'
import {IMap, Relax} from 'plume2'
import AppStore from '../store'
@Relax
export default class Status extends React.Component<any, any> {

  store: AppStore


  props: {
    relaxProps?: {
      companyInfo: IMap
    }
  }


  static relaxProps = {
    companyInfo: 'companyInfo'
  }


  render() {
    const {companyInfo} = this.props.relaxProps
    //物流公司名称
    const company = companyInfo.get('company')
    //物流公司代码
    const code = companyInfo.get('code')
    //创建日期
    const createTime = companyInfo.get('createTime')
    //运单号
    const no = companyInfo.get('no')
    return (
      <div className="order-status address-box">
        <div className="ship-status">
          <p>
            <span className="name">发货日期</span>
            <span className="grey">{createTime==""?"无":createTime.split(' ')[0]}</span>
          </p>
          <p>
            <span className="name">物流公司</span>
            <span className="grey">{company==""?"无":company}</span>
          </p>
          <p>
            <span className="name">物流单号</span>
            <span className="grey">{no==""?"无":no}</span>
          </p>
        </div>
      </div>
    )
  }
}
