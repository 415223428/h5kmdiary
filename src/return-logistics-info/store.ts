/**
 * Created by feitingting on 2017/8/3.
 */
import {Store} from 'plume2'
import {Alert} from 'wmkit'
import ReturnLogisticsInfoActor from './actor/logistic-info-actor'
import * as webapi from './webapi'
export default class AppStore extends Store {

  bindActor() {
    return [
      new ReturnLogisticsInfoActor
    ]
  }


  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  /**
   * 获取跟踪到的物流信息
   * @param id
   * @returns {Promise<void>}
   */
  init = async (id: string) => {
    const code = this.state().getIn(['companyInfo', 'code'])
    const res = await  webapi.fetchDeliveryDetail(code, id)
    if ((res as any).length > 0) {
      this.dispatch('logistics:init', res)
    } else {
/*      Alert({
        text:"暂无相关物流信息！",
        time:2000
      })*/
      this.dispatch('logistics:result')
    }
  }


  /**
   * 获取快递公司基本信息
   * @param id
   * @returns {Promise<void>}
   */
  fetchBasicInfo = async (id: string) => {
    const res = await webapi.fetchCompanyInfo(id)
    if (res.code == "K-000000" && res.context) {
        this.dispatch('logistics:info', res.context)
        this.init((res as any).context.no)
    }else{
      this.dispatch('logistics:result')
    }
  }
}