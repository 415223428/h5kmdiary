import { Fetch } from 'wmkit';

/**
 * 获取绑定状态
 */
export const getThirdAccounts = () => {
  return Fetch('/third/login/linked-account-flags');
};

/**
 * 删除关联
 * @param type
 */
export const deleteAccount = (type) => {
  return Fetch(`/third/login/remove/bind/${type}`, {
    method: 'DELETE'
  });
};

/**
 * 绑定微信
 * @param code
 */
export const wechatBind = (code) => {
  return Fetch('/third/login/wechat/bind', {
    method: 'POST',
    body: JSON.stringify({ code: code, type: 'MOBILE' })
  });
};

/**
 * 获取微信授权登录开关
 */
export const fetchWxLoginStatus = () => {
  return Fetch('/third/login/wechat/status/MOBILE');
};
