import { Action, Actor } from 'plume2';

export default class LinkedAccountActor extends Actor {
  defaultState() {
    return {
      wxFlag: null,
      serverStatus: {
        wechat: null
      }
    };
  }

  @Action('INIT')
  init(state, data) {
    return state.set('wxFlag', data.wxFlag);
  }

  @Action('linked-account:server-status')
  getServerStatus(state, data) {
    return state.setIn(['serverStatus', data.key], !!data.status);
  }
}
