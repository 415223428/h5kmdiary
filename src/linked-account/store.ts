import { Store } from 'plume2';

import { config } from 'config';
import parse from 'url-parse';
import { _, Alert, history, wxAuth } from 'wmkit';
import LinkedAccountActor from './actor/linked-account-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new LinkedAccountActor()];
  }

  init = async (refreshFlag = false) => {
    //微信回调，获取code
    const statusRes = await webapi.fetchWxLoginStatus();
    if (statusRes.code == config.SUCCESS_CODE) {
      this.dispatch('linked-account:server-status', {
        key: 'wechat',
        status: statusRes.context
      });
      if (!!statusRes.context) {
        const res = await webapi.getThirdAccounts();
        if (res.code == config.SUCCESS_CODE) {
          this.dispatch('INIT', res.context);
        } else {
          Alert({ text: res.message });
        }
      }
    } else {
      Alert({ text: statusRes.message });
    }

    if (!refreshFlag) {
      //微信授权回调获取code
      if (_.isWeixin()) {
        await this.wechatAuth();
      }
    }
  };

  deleteWechat = async () => {
    const res = await webapi.deleteAccount('WECHAT');
    if (res.code == config.SUCCESS_CODE) {
      Alert({
        text: '解除成功'
      });
    } else {
      Alert({
        text: res.message
      });
    }
    this.init();
  };

  bindWechat = () => {
    // if (_.isWeixin()) {
    wxAuth.getAuth(location.href,'bind');
    // } else {
    //   Alert({
    //     text: '请在微信客户端绑定'
    //   });
    // }
  };

  /**
   * 微信auth2.0
   */
  wechatAuth = async () => {
    const {
      query: { state, code }
    } = parse(window.location.search, true);
    if (code) {
      const res = await webapi.wechatBind(code);
      if (res.code == config.SUCCESS_CODE) {
        Alert({
          text: '绑定成功'
        });
        // setTimeout(() => {
        //   window.location.href = '/linked-account';
        // }, 1000);
      } else {
        Alert({
          text: res.message
        });
        // window.location.href = '/linked-account';
      }
      this.init(true);
      history.replace('/linked-account');
      // history.replace('/user-safe');
    }
  };
}
