import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import AccountList from './component/account-list';
const style = require('./css/style.css');

@StoreProvider(AppStore)
export default class LinkedAccount extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    return (
      <div className="service-main">
        <AccountList />
        <div className="bottom-tip">
          <p>1.账号关联之后，用户可使用快捷登录进入商城</p>
          <p>2.为了您的账号安全，15天内不可解除账号关联</p>
        </div>
      </div>
    );
  }
}
