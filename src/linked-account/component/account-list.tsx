import React from 'react';
import { Confirm, noop } from 'wmkit';
import { Relax } from 'plume2';
import { IMap } from 'typings/globalType';

@Relax
export default class AccountList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      wxFlag: string;
      deleteWechat: Function;
      bindWechat: Function;
      serverStatus: IMap;
    };
  };

  static relaxProps = {
    wxFlag: 'wxFlag',
    deleteWechat: noop,
    bindWechat: noop,
    serverStatus: 'serverStatus'
  };

  render() {
    const { wxFlag, serverStatus } = this.props.relaxProps;
    //未取到http返回的信息之前不展示任何信息
    const showFlag = serverStatus.get('wechat') != null && wxFlag != null;
    const enableFlag = serverStatus.get('wechat');
    return (
      <div>
        <div className="service-box">
          <div
            className="item"
            onClick={() => {
              this.wechatClick(wxFlag);
            }}
          >
            {/* <i className="iconfont icon-weixin icon-item" /> */}
            <img src={require('../img/wechat.png')}/>
            <span className="account-name">微信</span>
            <span>
              {showFlag
                ? enableFlag
                  ? wxFlag
                    ? '已绑定'
                    : '未绑定'
                  : '未启用'
                : ''}
            </span>
            {serverStatus.get('wechat') && (
              <i className="iconfont icon-jiantou1 arrow" />
            )}
          </div>
        </div>
      </div>
    );
  }

  wechatClick(wxFlag) {
    const { deleteWechat, bindWechat, serverStatus } = this.props.relaxProps;

    if (!serverStatus.get('wechat')) return;

    if (wxFlag) {
      //解绑流程
      Confirm({
        text: '您是否要解除关联？\n' + '解除后将无法使用快捷登录',
        okBtn: '解除',
        confirmCb: () => deleteWechat()
      });
    } else {
      //绑定流程
      bindWechat();
    }
  }
}
