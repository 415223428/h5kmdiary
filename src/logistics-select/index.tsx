import React from 'react'
import {StoreProvider} from 'plume2'
import AppStore from './store'

import Head from './components/head'
import LogisticsList from './components/logistics-list'
const style = require('./css/style.css')


@StoreProvider(AppStore)
export default class LogisticsSelect extends React.Component<any, any> {
  store: AppStore


  constructor(props) {
    super(props)
  }


  componentDidMount() {
    this.store.init(this.props.location.state.storeId)
  }


  render() {
    return (
      <div>
        <Head/>
        <LogisticsList/>
      </div>
    )
  }
};
