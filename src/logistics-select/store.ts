import { Store } from 'plume2';
import { fromJS } from 'immutable';
import LogisticsActor from './actor/logistics-actor';
import { storage, history, Alert } from 'wmkit';
import { cache } from 'config';
import { fetchLogisticCompany } from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new LogisticsActor()];
  }

  constructor(props) {
    super(props);
    if (__DEV__) {
      //debug
      (window as any)._store = this;
    }
  }

  init = async (storeId) => {
    const { context } = await fetchLogisticCompany();

    this.dispatch('logisticsActor:init', context);
  };

  /**
   * 保存添加的物流公司
   */
  save = (expressName) => {
    if (!expressName || !expressName.trim()) {
      Alert({ text: '物流公司名称不能为空' });
      return;
    }

    this.chooseCompany(fromJS({ expressName: expressName, expressCode: '' }));
  };

  /**
   * 选择物流公司
   */
  chooseCompany = (logistic) => {
    let logisticInfo = storage('session').get(cache.LOGISTICS_INFO);
    logisticInfo = JSON.parse(logisticInfo);

    (logisticInfo as any).expressName = logistic.get('expressName');
    (logisticInfo as any).expressCode = logistic.get('expressCode');

    storage('session').set(cache.LOGISTICS_INFO, JSON.stringify(logisticInfo));

    history.go(-1);
  };
}
