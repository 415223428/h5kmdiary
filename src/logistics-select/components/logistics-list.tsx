import React from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class LogisticsList extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      logisticsList: any;
      chooseCompany: Function;
    };
  };

  static relaxProps = {
    logisticsList: 'logisticsList',
    chooseCompany: noop
  };

  constructor(props) {
    super(props);
  }

  render() {
    return <div>{this._renderLoisticsItem()}</div>;
  }

  _renderLoisticsItem() {
    const { logisticsList, chooseCompany } = this.props.relaxProps;

    return logisticsList.toArray().map((logistic) => {
      return (
        <div
          className="logistic-select"
          onClick={() => chooseCompany(logistic)}
        >
          <span>{logistic.get('expressName')}</span>
        </div>
      );
    });
  }
}
