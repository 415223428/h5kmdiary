import React from 'react'
import {Relax} from 'plume2'
import {noop} from 'wmkit'

@Relax
export default class Head extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      save: Function
    }
  }


  static relaxProps = {
    save: noop
  }


  constructor(props) {
    super(props);
    this.state = {
      logisticsName: ''
    }
  }


  render() {
    return (
      <div className="logistics-head">
        <input placeholder="没有选择物流公司请在此输入" maxLength={10} value={this.state.logisticsName} onChange={(e) => this.setState({logisticsName: e.target.value})}/>
        {/* <span onClick={() => this.props.relaxProps.save(this.state.logisticsName)}>保存</span> */}
        <img src={require('../img/sure.png')} alt="" style={{
          width:'1.1rem',
          height:'.48rem'
        }}/>
      </div>
    )
  }
}


const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    padding: 20
  } as any,

  button: {
    display: 'flex',
    border: '2px solid #3d85cc',
    width:  75,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center'
  } as any,

  saveFont: {
    display: 'flex',
    color: '#3d85cc'
  } as any,

  input: {
    display: 'flex',
    width: '70%'
  }
}
