import { Fetch } from 'wmkit';

/**
 * 查询物流公司列表
 */
export const fetchLogisticCompany = () => {
  return Fetch<Result<any>>('/boss/expressCompany');
};
