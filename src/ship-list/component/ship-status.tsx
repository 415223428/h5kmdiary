import React from 'react'
import {Relax} from 'plume2'
import AppStore from '../store'

@Relax
export default class ShipStatus extends React.Component<any, any> {

  store: AppStore


  props: {
    relaxProps?: {
      goodList: any
    }
  }


  static relaxProps = {
    goodList: 'goodList'
  }


  constructor(props) {
    super(props)
  }


  render() {
    const {goodList} = this.props.relaxProps
    return (
      <div className="order-status">
        <div className="ship-status">
          <p>
            <span className="name">发货日期</span>
            {/* <span className="grey">{goodList.get('deliveryTime').split(' ')[0]}</span> */}
            <span className="grey">{goodList.get('deliveryTime')?goodList.get('deliveryTime').split(' ')[0]:"物流更新中"}</span>
          </p>
          <p>
            <span className="name">物流公司</span>
            <span className="grey">{goodList.get('logistics').get('logisticCompanyName')}</span>
          </p>
          <p>
            <span className="name">物流单号</span>
            <span className="grey">{goodList.get('logistics').get('logisticNo')}</span>
          </p>
        </div>
      </div>
    )
  }
}
