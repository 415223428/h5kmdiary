import React from 'react'
import {Relax, IMap} from 'plume2'

import {GiftList} from 'wmkit'

import AppStore from '../store'

const noneImg = require('../img/none_img.png')

@Relax
export default class GoodsList extends React.Component<any, any> {

  store: AppStore


  props: {
    relaxProps?: {
      goodList: IMap
    }
  }


  static relaxProps = {
    goodList: 'goodList'
  }


  constructor(props) {
    super(props)
  }


  render() {
    const {goodList} = this.props.relaxProps;

    return (
      <div className="ship-list">
        <h2 className="order-title">商品清单</h2>
        <div>
          {
            goodList.get('shippingItems').map((v, i) => {
              return this._renderSkuInfo(v, i);
            })
          }
          {goodList.get('giftItemList') && <GiftList data={goodList.get('giftItemList')} shipMode={true} />}
        </div>
      </div>
    )
  }

  _renderSkuInfo = (item, index) => {
    return <div className="goods-list ship-list" key={index}>
      <img className="img-box" src={item.pic ? item.pic : noneImg}/>
      <div className="detail">
        <a className="title" href="#">{item.itemName}</a>
        <p className="gec">{item.specDetails}</p>
        <div className="bottom">
          <span className="num">×{item.itemNum}{item.unit ? item.unit : ''}</span>
        </div>
      </div>
    </div>
  }
}
