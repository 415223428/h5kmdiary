/**
 * Created by feitingting on 2017/7/26.
 */
import { Fetch } from 'wmkit';

/**
 * 根据运单号检索商品详情
 * @param id
 * @returns {Promise<Result<T>>}
 */
export const fetchAllGoods = (orderId: string, deliverId: string) => {
  return Fetch(`/trade/shipments/${orderId}/${deliverId}/null`, {
    method: 'GET'
  });
};
