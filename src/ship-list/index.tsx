import React from 'react'
import {StoreProvider} from 'plume2'
import  AppStore from './store'
import ShipStatus from './component/ship-status'
import GoodsList from './component/goods-list'

@StoreProvider(AppStore, {debug: __DEV__})
export default class ShipList extends React.Component<any, any> {

  store: AppStore


  componentWillMount() {
    document.title = "商品清单"
    const {oid} = this.props.match.params
    const {tid} = this.props.match.params
    this.store.init(oid,tid)
  }


  constructor(props) {
    super(props)
  }


  render() {
    return (
      <div style={{background: '#fafafa', height: '100%'}}>
        <ShipStatus/>
        <GoodsList/>
      </div>
    )
  }
}
