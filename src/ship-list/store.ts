/**
 * Created by feitingting on 2017/7/26.
 */
import  {Store} from 'plume2'
import  ListActor from './actor/list-actor'
import  * as webapi from  './webapi'
export  default class AppStore extends Store {

  bindActor() {
    return [
      new ListActor
    ]
  }


  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  init = async (orderId:string,deliveryId: string) => {
    const res = await  webapi.fetchAllGoods(orderId,deliveryId)
    this.dispatch('list:goodItems', res.context)
  }
}