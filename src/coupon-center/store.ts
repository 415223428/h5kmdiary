import { msg, Store } from 'plume2';

import { Alert } from 'wmkit';
import { config } from 'config';

import CouponActor from './actor/coupon-actor';
import FormActor from './actor/coupon-list-form-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new CouponActor(), new FormActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  /**
   * 初始化数据-优惠券分类
   * @returns {Promise<void>}
   */
  init = async () => {
    //获取优惠券分类
    let couponCatesRes = (await webapi.listCouponCate()) as any;
    if (couponCatesRes.code === config.SUCCESS_CODE) {
      let couponCateList = couponCatesRes.context;
      couponCateList.push({ couponCateId: -1, couponCateName: '全部优惠券' });
      this.dispatch('couponcates: list', couponCateList);
      //分类默认选择第一个
      this.setCouponCate(
        couponCateList[0] ? couponCateList[0].couponCateId : -1
      );

      this.dispatch('coupon: list: initial: end');
    } else {
      Alert({ text: couponCatesRes.message });
    }
  };

  /**
   * 下拉菜单的显示隐藏
   */
  changeDrapMenu = () => {
    this.dispatch('change: drapmenu');
  };

  /**
   * 分类弹层的显示隐藏
   */
  changeCateMask = () => {
    this.dispatch('change: catemask');
  };

  /**
   * 优惠券类型选择
   * @param index
   */
  setCouponType = (index) => {
    this.transaction(() => {
      this.changeDrapMenu();
      this.dispatch('coupon: type: change', index);
      this.dispatch('coupon: list: form: set', {
        filed: 'couponType',
        value: index === -1 ? null : index
      });
      this.clearCouponState();
    });
  };

  /**
   * 优惠券分类选择
   * @param index
   */
  setCouponCate = (index) => {
    this.transaction(() => {
      this.dispatch('coupon: tab: change', index);

      this.dispatch('coupon: list: form: set', {
        filed: 'couponCateId',
        value: index === -1 ? null : index
      });
      this.clearCouponState();
    });
  };

  /**
   *设置选中分类索引
   */
  changeActivedKey = (index) => {
    this.dispatch('change: actived-key', index);
  };

  /**
   * 领取优惠券
   */
  fetchCoupon = async (coupon) => {
    if (window.token) {
      const { code, message } = await webapi.fetchCoupon(
        coupon.get('couponId'),
        coupon.get('activityId')
      );
      if (code === config.SUCCESS_CODE) {
        this.setCouponState(coupon, 'fetchCoupon');
      } else {
        Alert({ text: message });
        //会员等级导致的领券失败不刷新页面
        if (code === 'K-080203') {
          return;
        }
        this.state().get('toRefresh')();
      }
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => this.init()
      });
    }
  };

  /**
   *状态变化的优惠券
   */
  setCouponState = (coupon, opt) => {
    if (opt === 'fetchCoupon') {
      //领取成功
      this.dispatch('coupon: list: coupon-state: changed', {
        filed: coupon.get('activityConfigId'),
        value: { hasFetched: true }
      });
    }
    if (opt === 'countOver') {
      //倒计时结束
      this.dispatch('coupon: list: coupon-state: changed', {
        filed: coupon.get('activityConfigId'),
        value: { countOver: true }
      });
    }
  };

  /**
   *清空变化的优惠券
   */
  clearCouponState = () => {
    this.dispatch('coupon: list: coupon-state: clear');
  };

  /**
   * 优惠券倒计时结束
   */
  countOver = (coupon) => {
    this.setCouponState(coupon, 'countOver');
  };

  /**
   * 初始话刷新method
   * @param toRefresh
   */
  initToRefresh = (toRefresh) => {
    this.dispatch('coupon: list: torefresh', toRefresh);
  };
}
