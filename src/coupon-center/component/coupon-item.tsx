import React from 'react';
import { Relax } from 'plume2';
import { history, noop } from 'wmkit';

import CountDown from './count-down';
const common = require('../img/common.png');
const store = require('../img/store.png');
const storeCoupon=require('../img/store-coupon.png')
const commonCoupon=require('../img/common-coupon.png')

const COUPON_TYPE = {
  0: '通用券',
  1: '店铺券',
  2: '运费券'
};

export default class CouponItem extends React.Component<any, any> {
  render() {
    const {
      coupon,
      otherProps,
      countOver,
      couponStateChanged,
      fetchCoupon
    } = this.props;
    return (
      <div>
        {this._getCouponStatus(coupon, couponStateChanged) !== 0 && (
          <div className={`item  ${this._buildItemType(coupon)}`} style={coupon.get('couponType')==0? {background:`url(${common}) top center`,backgroundSize:'cover'}:{background:`url(${store}) top center`,backgroundSize:'cover'}}>
            <div className="left">
              <p className="money">
                ￥
                <span className="money-text">{coupon.get('denomination')}</span>
              </p>
              <p className="left-tips">{this._buildFullBuyPrice(coupon)}</p>
              {coupon.get('couponWillEnd') && (
                <img src={require('../img/expiring.png')} alt="" />
              )}
            </div>
            <div className="right">
              <div className="right-info">
                <div className="right-top">
                  {/* <i style={coupon.get('couponType')==0?{background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))'}:{background:'linear-gradient(135deg,rgba(255,139,8,1),rgba(247,187,88,1))'}}>{COUPON_TYPE[coupon.get('couponType')]}</i> */}
                 { coupon.get('couponType')==0?(<img style={{width:'.8rem',height:'.3rem'}} src={commonCoupon}/>):
                   coupon.get('couponType')==1?(<img style={{width:'.8rem',height:'.3rem'}} src={storeCoupon}/>):
                   (<img style={{width:'.8rem',height:'.3rem'}} src={storeCoupon}/>)}
                  <p>
                    <span> {this._buildStorename(coupon, otherProps)}</span>
                  </p>
                </div>
                {coupon.get('couponType') == 2 && <div className="range-box" />}
                {coupon.get('couponType') != 2 && (
                  <div className="range-box">
                    限<span> {this._buildScope(coupon, otherProps)}</span>可用
                  </div>
                )}
                <div className="bottom-box">{this._buildRangDay(coupon)}</div>
              </div>

              {/* * 1.立即领取 百分比 */}
              {this._getCouponStatus(coupon, couponStateChanged) === 1 && (
                <div className="operat-box colum-center">
                  <p className="coupon-time">
                  <span style={{color:'#000',fontSize:'.24rem'}}>已抢</span>{(coupon.get('fetchPercent') * 100).toFixed(0)}%
                  </p>
                  <a href="javascript:;" onClick={() => fetchCoupon(coupon)}>
                    立即领取
                  </a>
                  {/*img立即领取  */}
                </div>
              )}

              {/* * 2.立即领取 倒计时 */}
              {this._getCouponStatus(coupon, couponStateChanged) === 2 && (
                <div className="operat-box colum-center">
                  <p>即将结束</p>
                  <p className="coupon-time">
                    <CountDown
                      allowFontScaling={false}
                      numberOfLines={1}
                      endHandle={countOver}
                      parame={coupon}
                      timeOffset={this._getTimeoffset(coupon)}
                    />
                  </p>
                  <a href="javascript:;" onClick={() => fetchCoupon(coupon)}>
                    立即领取
                  </a>
                  {/* img立即领取 */}
                </div>
              )}

              {/* * 3.已经领取 立即使用|查看使用范围 */}
              {this._getCouponStatus(coupon, couponStateChanged) === 3 && (
                <div className="operat-box colum-center" style={{justifyContent:'flex-end'}}>
                  <img src={require('../img/get-coupon.png')} alt="" style={{position:'absolute',right:'-0.08rem',top:'0.1rem'}}/>
                  <div style={{padding:'0 .1rem',height: '0.5rem',borderRadius:' 0.24rem',fontSize: '0.26rem',
                    color: '#FF4D4D',display: 'flex',alignItems: 'center',border: '1px solid #FF4D4D'}}
                    onClick={() =>
                      this._handleClick(coupon, couponStateChanged)
                    }
                  >
                    {this._txtBox(coupon, couponStateChanged)}
                  </div>
                  {/* img 立即使用 */}
                </div>
              )}

              {/* * 5.已抢光 */}
              {this._getCouponStatus(coupon, couponStateChanged) === 5 && (
                <div className="operat-box colum-center">
                  {/* <div className="gray-state">已抢光</div> */}
                  <img src={require('../img/empty.png')} alt="" style={{position:'absolute',right:'-0.08rem',top:'0.1rem'}}/>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    );
  }

  /**
   * 优惠券样式
   * 0 通用券
   * 1 店铺券
   * 2 运费券
   */
  _buildItemType = (coupon) => {
    let text = '';
    if (coupon.get('couponType') === 0) {
      text = '';
    } else if (coupon.get('couponType') === 1) {
      text = 'store-item';
    } else if (coupon.get('couponType') === 2) {
      text = 'freight-item';
    }
    return `${text}`;
  };

  /***
   * 满减金额
   */
  _buildFullBuyPrice = (coupon) => {
    return coupon.get('fullBuyType') === 0
      ? '无门槛'
      : `满${coupon.get('fullBuyPrice')}可用`;
  };
  /**
   * 优惠券使用店铺名称
   */
  _buildStorename = (coupon, otherProps) => {
    let text = '';
    if (coupon.get('platformFlag') === 1) {
      text = '全平台可用';
    } else {
      text = otherProps.storeMap[coupon.get('storeId')];
      text = `仅${text}可用`;
    }
    return `${text}`;
  };

  /**
   * 优惠券使用范围
   */
  _buildScope = (coupon, otherProps?: any) => {
    let text = '';
    let scopeType = '';
    if (coupon.get('scopeType') == 0) {
      scopeType = '商品：';
      text = '全部商品';
    } else if (coupon.get('scopeType') == 1) {
      scopeType = '品牌：';
      text = '仅限';
      if (otherProps)
        coupon.get('scopeIds').forEach((value) => {
          let name = otherProps.brandMap[value]
            ? '[' + otherProps.brandMap[value] + ']'
            : '';
          text = `${text}${name}`;
        });
    } else if (coupon.get('scopeType') == 2) {
      scopeType = '品类：';
      text = '仅限';
      if (otherProps)
        coupon.get('scopeIds').forEach((value) => {
          let name = otherProps.cateMap[value]
            ? '[' + otherProps.cateMap[value] + ']'
            : '';
          text = `${text}${name}`;
        });
    } else if (coupon.get('scopeType') == 3) {
      scopeType = '分类：';
      text = '仅限';
      if (otherProps)
        coupon.get('scopeIds').forEach((value) => {
          let name = otherProps.storeCateMap[value]
            ? '[' + otherProps.storeCateMap[value] + ']'
            : '';
          text = `${text}${name}`;
        });
    } else {
      scopeType = '商品：';
      text = '部分商品';
    }

    return `${scopeType}${text}`;
  };

  /***
   * 生效时间
   */
  _buildRangDay = (coupon) => {
    return coupon.get('rangeDayType') === 1
      ? `领取后${coupon.get('effectiveDays')}天内有效`
      : `${coupon.get('couponStartTime')}——${coupon.get('couponEndTime')}`;
  };

  /**
   * 按钮文本展示
   */
  _txtBox = (coupon, couponStateChanged) => {
    if (
      couponStateChanged.getIn([
        `${coupon.get('activityConfigId')}`,
        'hasFetched'
      ])
    ) {
      return !this._convertCouponStarted(coupon) ? '查看可用商品' : '立即使用';
    }
    if (coupon.get('hasFetched')) {
      return !coupon.get('couponStarted') ? '查看可用商品' : '立即使用';
    }
  };

  /**
   * 点击事件
   */
  _handleClick = (coupon, couponStateChanged) => {
    if (
      couponStateChanged.getIn([
        `${coupon.get('activityConfigId')}`,
        'hasFetched'
      ])
    ) {
      this._goCoupronPromotion(coupon);
    }
    if (coupon.get('hasFetched')) {
      this._goCoupronPromotion(coupon);
    }
  };

  /**
   * 1：查看使用范围
   * 2：立即使用
   */
  _goCoupronPromotion = (coupon) => {
    history.push({
      pathname: '/coupon-promotion',
      state: {
        couponId: coupon.get('couponId'),
        activityId: coupon.get('activityId')
      }
    });
  };

  /**
   * 计算couponStarted 优惠券开始标记
   * @private
   */
  _convertCouponStarted = (coupon) => {
    let couponStarted = false;
    if (coupon.get('rangeDayType') === 0) {
      if (
        new Date(coupon.get('couponStartTime').replace(/-/g, '/')).getTime() -
          new Date().getTime() <
        0
      ) {
        couponStarted = true;
      }
    }
    //领取生效
    if (coupon.get('rangeDayType') === 1) {
      couponStarted = true;
    }

    return couponStarted;
  };

  /**
   * 获取优惠券状态
   * 1.立即领取 百分比
   * 2.立即领取 倒计时
   * 3.已经领取  立即使用|查看使用范围
   * 4.
   * 5.已抢光
   */
  _getCouponStatus = (coupon, couponStateChanged) => {
    let status = 0;
    //倒计时结束
    if (
      !couponStateChanged.getIn([
        `${coupon.get('activityConfigId')}`,
        'countOver'
      ])
    ) {
      // 1.立即领取 百分比
      // 立即领取
      if (
        !couponStateChanged.getIn(
          [`${coupon.get('activityConfigId')}`, 'hasFetched']
        ) &&
        !coupon.get('hasFetched') &&
        coupon.get('leftFlag')
      ) {
        // 1.立即领取 百分比
        status = 1;
        if (coupon.get('activityWillEnd')) {
          //2.立即领取 倒计时
          status = 2;
        }
      }
      // 3.已经领取  立即使用|查看使用范围
      if (
        couponStateChanged.getIn(
          [`${coupon.get('activityConfigId')}`, 'hasFetched']
        ) ||
        coupon.get('hasFetched')
      ) {
        status = 3;
      }

      // 5.已抢光
      if (!coupon.get('hasFetched') && !coupon.get('leftFlag')) {
        status = 5;
      }
    }
    return status;
  };

  /**
   * 倒计时
   * 一小时内：倒计时+59秒  避免出现00：00 的时候活动仍在进行中
   *
   */
  _getTimeoffset = (coupon) => {
    return (coupon.get('activityCountDown') / 1000 - 3600 > 0
      ? coupon.get('activityCountDown') / 1000
      : coupon.get('activityCountDown') / 1000 + 59
    ).toFixed(0);
  };
}
