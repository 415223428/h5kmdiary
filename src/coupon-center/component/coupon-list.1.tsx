import React from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class CouponList extends React.Component<any, any> {
  render() {
    return (
      <div className="center-list">
        <div className="item">
          <div className="left">
            <p className="money">
              ￥
              <span className="money-text">200000</span>
            </p>
            <p className="left-tips">满999999999999可用</p>
          </div>
          <div className="right">
            <div className="right-info">
              <div className="right-top">
                <i>通用券</i>
                <p>
                  <span>全平台可用全全平台可用全全平台可用全全平台可用全</span>
                </p>
              </div>
              <div className="range-box">
                限<span>品牌：仅限[P&G][联合利华]仅限[P&G][联合利华]等等</span>可用
              </div>
              <div className="bottom-box">2017-09-27至2017-10-08</div>
            </div>
            <div className="operat-box">
              <div className="gray-state">已抢光</div>
            </div>
          </div>
        </div>
        <div className="item">
          <div className="left">
            <p className="money">
              ￥
              <span className="money-text">200000</span>
            </p>
            <p className="left-tips">满999999999999可用</p>
          </div>
          <div className="right">
            <div className="right-info">
              <div className="right-top">
                <i>通用券</i>
                <p>
                  <span>全平台可用全全平台可用全全平台可用全全平台可用全</span>
                </p>
              </div>
              <div className="range-box">
                限<span>品牌：仅限[P&G][联合利华]仅限[P&G][联合利华]等等</span>可用
              </div>
              <div className="bottom-box">2017-09-27至2017-10-087-10-08</div>
            </div>
            <div className="operat-box colum-center">
              <p>即将结束</p>
              <p className="coupon-time">13时44分</p>
              <a href="javascript:;">立即领取</a>
            </div>
          </div>
        </div>
        <div className="item">
          <div className="left">
            <p className="money">
              ￥
              <span className="money-text">200000</span>
            </p>
            <p className="left-tips">满999999999999可用</p>
          </div>
          <div className="right">
            <div className="right-info">
              <div className="right-top">
                <i>通用券</i>
                <p>
                  <span>全平台可用全全平台可用全全平台可用全全平台可用全</span>
                </p>
              </div>
              <div className="range-box">
                限<span>品牌：仅限[P&G][联合利华]仅限[P&G][联合利华]等等</span>可用
              </div>
              <div className="bottom-box">2017-09-27至2017-10-087-10-08</div>
            </div>
            <div className="operat-box colum-center">
              <p>已抢</p>
              <p className="coupon-time">50%</p>
              <a href="javascript:;">立即领取</a>
            </div>
          </div>
        </div>
        <div className="item">
          <div className="left">
            <p className="money">
              ￥
              <span className="money-text">200000</span>
            </p>
            <p className="left-tips">满999999999999可用</p>
          </div>
          <div className="right">
            <div className="right-info">
              <div className="right-top">
                <i>通用券</i>
                <p>
                  <span>全平台可用全全平台可用全全平台可用全全平台可用全</span>
                </p>
              </div>
              <div className="range-box">
                限<span>品牌：仅限[P&G][联合利华]仅限[P&G][联合利华]等等</span>可用
              </div>
              <div className="bottom-box">2017-09-27至2017-10-087-10-08</div>
            </div>
            <div className="operat-box colum-center">
              <img src={require('../img/get-coupon.png')} alt="" />
              <a href="javascript:;" className="get-now">
                立即领取
              </a>
            </div>
          </div>
        </div>
        <div className="item store-item">
          <div className="left">
            <p className="money">
              ￥
              <span className="money-text">200000</span>
            </p>
            <p className="left-tips">满999999999999可用</p>
          </div>
          <div className="right">
            <div className="right-info">
              <div className="right-top">
                <i>店铺券</i>
                <p>
                  <span>全平台可用全全平台可用全全平台可用全全平台可用全</span>
                </p>
              </div>
              <div className="range-box">
                限<span>品牌：仅限[P&G][联合利华]仅限[P&G][联合利华]等等</span>可用
              </div>
              <div className="bottom-box">2017-09-27至2017-10-087-10-08</div>
            </div>
            <div className="operat-box colum-center">
              <p>即将结束</p>
              <p className="coupon-time">13时44分</p>
              <a href="javascript:;">立即领取</a>
            </div>
          </div>
        </div>
        <div className="item freight-item">
          <div className="left">
            <p className="money">
              ￥
              <span className="money-text">200000</span>
            </p>
            <p className="left-tips">满999999999999可用</p>
          </div>
          <div className="right">
            <div className="right-info">
              <div className="right-top">
                <i>运费券</i>
                <p>
                  <span>全平台可用全全平台可用全全平台可用全全平台可用全</span>
                </p>
              </div>
              <div className="range-box">
                限<span>品牌：仅限[P&G][联合利华]仅限[P&G][联合利华]等等</span>可用
              </div>
              <div className="bottom-box">2017-09-27至2017-10-087-10-08</div>
            </div>
            <div className="operat-box colum-center">
              <p>即将结束</p>
              <p className="coupon-time">13时44分</p>
              <a href="javascript:;">立即领取</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
