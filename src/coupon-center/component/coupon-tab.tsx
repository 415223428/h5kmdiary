import React from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
import { IList } from 'typings/globalType';

@Relax
export default class Tab extends React.Component<any, any> {
  _cateRefs = [];
  _scroll;
  props: {
    relaxProps?: {
      // 优惠券分类列表
      couponCateList: IList;
      // 选中的优惠券分类下标
      activecouponCateIddKey: Number;
      //tab选中的下标
      activedKey: Number;
      //tab选中的优惠券分类id
      couponCateId: String;
      //修改tab选中的优惠券分类id
      setCouponCate: Function;
      //修改tab选中的下标
      changeActivedKey: Function;
      //分类弹层的显示隐藏
      changeCateMask: Function;
    };
  };

  static relaxProps = {
    couponCateList: 'couponCateList',
    couponCateId: 'couponCateId',
    activedKey: 'activedKey',
    setCouponCate: noop,
    changeActivedKey: noop,
    //分类弹层的显示隐藏
    changeCateMask: noop
  };
  componentDidUpdate() {
    this.scrollViewTo();
  }

  componentDidMount() {
    setTimeout(() => {
      this.scrollViewTo();
    }, 10);
  }
  render() {
    const {
      changeCateMask,
      couponCateList,
      couponCateId,
      setCouponCate,
      changeActivedKey
    } = this.props.relaxProps;

    return (
      <div className="tab-box">
        <div
          className="tab-left"
          ref={(ref) => {
            this._scroll = ref;
          }}
        >
          {couponCateList.map((cateItme, index) => {
            return (
              <a
                href="javascript:;"
                key={cateItme.get('couponCateId')}
                className={
                  couponCateId == cateItme.get('couponCateId')
                    ? 'actived'
                    : null
                }
                onClick={() => {
                  setCouponCate(cateItme.get('couponCateId'));
                  changeActivedKey(index);
                }}
                ref={(dom) => {
                  this._cateRefs[cateItme.get('couponCateId')] = dom;
                }}
              >
                {cateItme.get('couponCateName')}
                {/* {cateItme.get('couponCateId')}~~~~~~~
                {couponCateId}------{couponCateId ==
                  cateItme.get('couponCateId')} */}
              </a>
            );
          })}
        </div>
        <div
          className="tab-right"
          onClick={() => {
            changeCateMask();
          }}
        >
          全部
          <i className="iconfont icon-xiala-copy" />
        </div>
      </div>
    );
  }
  scrollViewTo = () => {
    const { couponCateList, activedKey } = this.props.relaxProps;
    let allLeft = 0;
    couponCateList.map((cateItem, index) => {
      const currDom = this._cateRefs[cateItem.get('couponCateId')];
      const width = currDom.offsetWidth;
      if (index < activedKey) {
        allLeft = allLeft + width + 30;
      } else {
        this._scroll.scrollTo(allLeft, 0);
        return;
      }
    });
  };
}
