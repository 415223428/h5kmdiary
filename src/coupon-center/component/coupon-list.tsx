import React from 'react';
import { fromJS } from 'immutable';
import { Relax } from 'plume2';
import { IMap } from 'typings/globalType';

import { ListView, noop, Blank, WMkit } from 'wmkit';

import CouponItem from './coupon-item';
@Relax
export default class CouponList extends React.Component<any, any> {
  _listView: any;
  props: {
    relaxProps?: {
      form: IMap;
      toRefresh: any;
      initialEnd: any;
      couponStateChanged: IMap;
      fetchCoupon: Function;
      countOver: Function;
      initToRefresh: Function;
    };
  };

  static relaxProps = {
    form: 'form',
    toRefresh: 'toRefresh',
    initialEnd: 'initialEnd',
    couponStateChanged: 'couponStateChanged',
    fetchCoupon: noop,
    countOver: noop,
    initToRefresh: noop
  };
  render() {
    const {
      couponStateChanged,
      form,
      initialEnd,
      fetchCoupon,
      countOver,
      initToRefresh
    } = this.props.relaxProps;
    // 组件刚执行完mount，搜索条件没有注入进来，先不加载WmListView，避免先进行一次无条件搜索，再立刻进行一次有条件搜索
    if (!initialEnd) {
      return null;
    }
    const url = this.couponCenterUrl();
    const listViewProps = {
      url: url,
      style: { height: 'calc(100vh - 1.7rem)' },
      params: form.toJS(),
      columnWrapperStyle: {},
      isPagination: true,
      renderRow: (item, index, otherProps) => {
        return (
          <CouponItem
            couponStateChanged={fromJS(couponStateChanged)}
            countOver={countOver}
            fetchCoupon={fetchCoupon}
            coupon={fromJS(item)}
            key={index}
            otherProps={otherProps}
          />
        );
      },
      renderEmpty: () => (
        <Blank
          img={require('../img/no-coupon.png')}
          content="啊哦，什么券都没有~"
        />
      ),
      keyProps: 'activityConfigId',
      otherProps: ['brandMap', 'storeMap', 'cateMap', 'storeCateMap']
    };

    return (
      <div className="list center-list">
        <ListView
          {...listViewProps}
          ref={(_listView) => (this._listView = _listView)}
          toRefresh={(_init) => initToRefresh(_init)}
        />
      </div>
    );
  }

  couponCenterUrl() {
    const url = WMkit.isLoginOrNotOpen()
      ? '/coupon-info/center'
      : '/coupon-info/front/center';
    return url;
  }
}
