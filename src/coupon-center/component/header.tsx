import React from 'react';
import { Relax, msg } from 'plume2';
import { history, noop, WMkit } from 'wmkit';

@Relax
export default class Header extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeDrapMenu: Function;
      showDrapMenu: boolean;
    };
  };

  static relaxProps = {
    changeDrapMenu: noop,
    showDrapMenu: 'showDrapMenu'
  };

  render() {
    const { changeDrapMenu, showDrapMenu } = this.props.relaxProps;

    return (
      <div>
        <div className="headerBox">
          <a
            className="header-title"
            href="javascript:;"
            onClick={() => {
              changeDrapMenu();
            }}
            style={{fontSize:'0.32rem',color:'#333'}}
          >
            领券中心
            <i
              className="iconfont icon-jiantou2"
              style={showDrapMenu ? { transform: 'rotate(180deg)',color:'#999',fontSize:'0.28rem' } : {color:'#999',fontSize:'0.28rem'}}
            />
          </a>
          <a
            href="javascript:;"
            className="header-right"
            onClick={() => {
              this.myCoupon_();
            }}
            style={{fontSize:'0.26rem',color:'#333'}}
          >
            我的优惠券
          </a>
        </div>
      </div>
    );
  }

  myCoupon_() {
    if (WMkit.isLoginOrNotOpen()) {
      history.push('/my-coupon');
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          history.push('/my-coupon');
        }
      });
    }
  }
}
