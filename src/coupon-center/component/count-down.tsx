'use strict';

import React, { Component } from 'react';
export default class CountDown extends Component<any, any> {
  timer;
  _isMounted;
  static defaultProps = {
    timeOffset: 0,
    overHandler: () => {},
    timeStyle: {},
    colonStyle: {},
    timeClock: {},
    time: {},
    timeDaysStyle: {},
    hideSeconds: true, //隐藏秒
    parame: {},
    //倒计时结束的处理
    endHandle: () => {}
  };

  state = {
    //默认倒计时时间，正整数，单位：秒
    timeOffset: this.props.timeOffset
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this._isMounted = true;
    if (this._isMounted) {
      this._doCount();
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
  render() {
    return <span>{this._timeFormat(this.state.timeOffset)}</span>;
  }

  _timeFormat = (timeOffset) => {
    const hour = Math.floor((timeOffset / 60 / 60) % 24);
    const min = Math.floor((timeOffset / 60) % 60);
    const second = timeOffset % 60;
    let trueHour = hour < 10 ? '0' + hour : hour;
    let truemin = min < 10 ? '0' + min : min;
    let trueSec = second < 10 ? '0' + second : second;
    if (trueHour == '00' && truemin == '00' && trueSec == '59') {
      //只显示时分  基础数据会加上59秒
      //清除定时器
      this.props.endHandle(this.props.parame);
    }
    return (
      <span>
        {trueHour}时{truemin}分
      </span>
    );
  };

  /**
   * 计时器倒计时
   */
  _doCount = () => {
    this.timer = setInterval(() => {
      if (this.state.timeOffset <= 1) {
        clearTimeout(this.timer);
        this.props.overHandler();
      }
      this.setState({
        timeOffset: this.state.timeOffset - 1
      });
    }, 1000);
  };
}
