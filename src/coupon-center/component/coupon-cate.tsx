import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
import { IList } from 'typings/globalType';

/**
 * 点击全部分类弹窗
 */
@Relax
export default class CouponCate extends Component<any, any> {
  props: {
    relaxProps?: {
      //显示优惠券分类
      showCateMask: boolean;
      //选中分类的key
      couponCateId: String;
      //分类数组
      couponCateList: IList;
      //分类弹层的显示隐藏
      changeCateMask: Function;
      //设置选中的分类key
      changeActivedKey: Function;
      // 设置选中分类id
      setCouponCate: Function;
    };
  };

  static relaxProps = {
    showCateMask: 'showCateMask',
    couponCateList: 'couponCateList',
    couponCateId: 'couponCateId',
    activedKey: 'activedKey',
    changeActivedKey: noop,
    changeCateMask: noop,
    setCouponCate: noop
  };

  render() {
    const {
      showCateMask,
      changeCateMask,
      couponCateId,
      changeActivedKey,
      couponCateList,
      setCouponCate
    } = this.props.relaxProps;
    return (
      showCateMask && (
        <div className="coupon-container">
          <div className="coupon-cate-box">
            <div className="coupon-content">
              <p>全部券分类</p>
              <div className="coupon-list">
                {couponCateList.map((cateItme, index) => {
                  return (
                    <a
                      href="javascript:;"
                      key={cateItme.get('couponCateId')}
                      className={
                        couponCateId == cateItme.get('couponCateId')
                          ? 'actived'
                          : null
                      }
                      onClick={() => {
                        changeActivedKey(index);
                        setCouponCate(cateItme.get('couponCateId'));
                        changeCateMask();
                      }}
                    >
                      {cateItme.get('couponCateName')}
                    </a>
                  );
                })}
              </div>
              <a href="javascript:;" onClick={() => changeCateMask()}>
                取消
              </a>
            </div>
            <div className="coupon-bottom" onClick={() => changeCateMask()} />
          </div>
        </div>
      )
    );
  }
}
