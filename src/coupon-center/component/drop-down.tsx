import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
/**
 * 点击我的优惠券筛选券分类
 */
@Relax
export default class DropDown extends Component<any, any> {
  props: {
    relaxProps?: {
      //优惠券类型 0通用券 1店铺券 2运费券
      couponType: number;
      //下拉是否显示
      showDrapMenu: boolean;
      //优惠券优惠券类型选择
      setCouponType: Function;
      //下拉菜单的显示隐藏
      changeDrapMenu: Function;
    };
  };

  static relaxProps = {
    //优惠券类型 0通用券 1店铺券 2运费券
    couponType: 'couponType',
    //下拉是否显示
    showDrapMenu: 'showDrapMenu',
    //优惠券优惠券类型选择
    setCouponType: noop,
    //下拉菜单的显示隐藏
    changeDrapMenu: noop
  };

  render() {
    const {
      couponType,
      showDrapMenu,
      changeDrapMenu,
      setCouponType
    } = this.props.relaxProps;
    return (
      showDrapMenu && (
        <div className="coupon-container">
          <div className="coupon-cate-box">
            <div className="coupon-top">
              <a
                href="javascript:;"
                className={couponType == null ? 'actived' : ''}
                onClick={() => setCouponType(null)}
              >
                全部券类型
              </a>
              <a
                href="javascript:;"
                className={couponType == 0 ? 'actived' : ''}
                onClick={() => {
                  setCouponType(0);
                }}
              >
                通用券
              </a>
              <a
                href="javascript:;"
                className={couponType == 1 ? 'actived' : ''}
                onClick={() => {
                  setCouponType(1);
                }}
              >
                店铺券
              </a>
              {/* <a
                href="javascript:;"
                className={couponType == 2 ? 'actived' : ''}
                onClick={() => {
                  setCouponType(2);
                }}
              >
                运费券
              </a> */}
            </div>
            <div className="coupon-bottom" onClick={() => changeDrapMenu()} />
          </div>
        </div>
      )
    );
  }
}
