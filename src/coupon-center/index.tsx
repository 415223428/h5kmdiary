import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import Header from './component/header';
import Tab from './component/coupon-tab';
import CouponList from './component/coupon-list';
import CouponCate from './component/coupon-cate';
import DropDown from './component/drop-down';
/**
 * 领券中心 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class CouponCenter extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    return (
      <div className="container">
        <div style={{ height: '1.7rem' }}>
          <div className="top-box">
            <Header />
            <Tab />
          </div>
        </div>

        <CouponList />
        <DropDown />
        <CouponCate />
      </div>
    );
  }
}
