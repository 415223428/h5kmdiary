import React, { Component } from 'react';
import { WMImage } from 'wmkit';

export default class ErrorPage extends Component<any, any> {
  render() {
    return (
      <div className="list-none">
        <WMImage
          src={require('./img/warning.png')}
          width="200px"
          height="200px"
        />
        <p>啊哦，进入人数较多，请稍后重试</p>
        <div className="half">
          <button className="btn btn-ghost reload">
            <i className="iconfont icon-shuaxin" />&nbsp;&nbsp;刷新重试
          </button>
        </div>
      </div>
    );
  }
}
