import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from '../register-agreement/store';
const style = require('./css/style.css');

@StoreProvider(AppStore)
export default class RegisterAgreement extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '注册协议';
    this.store.init();
  }

  render() {
    return (
      <div>
        <div
          className="agree-box"
          dangerouslySetInnerHTML={{
            __html: this.store.state().get('registerContent')
          }}
        />
      </div>
    );
  }
}
