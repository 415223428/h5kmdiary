import {Fetch} from 'wmkit'
/**
 * 获取平台站点信息
 * @type {Promise<AsyncResult<T>>}
 */
export const getSiteInfo = () => {
  return Fetch('/system/baseConfig')
}