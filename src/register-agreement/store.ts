import {Store} from "plume2";
import * as webapi from './webapi';
import {config} from "config";
import AgreeMentActor from "./actor/agreeMent-actor";
export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if(__DEV__){
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [
      new AgreeMentActor
    ]
  }

  init = async () => {
    const res = await webapi.getSiteInfo()
    if(res.code == config.SUCCESS_CODE){
      let registerContent = (res.context as any).registerContent
      this.dispatch('agreeMent:init',registerContent)
    }
  }
}