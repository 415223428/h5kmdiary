import {Action,Actor, IMap} from 'plume2'

export default class AgreeMentActor extends Actor {
  defaultState() {
    return {
      registerContent: ''
    }
  }


  @Action('agreeMent:init')
  init(state: IMap, registerContent: string) {
    return state.set('registerContent', registerContent);
  }
}