import React from 'react'
import { StoreProvider } from 'plume2'
import AppStore from './store'
const styles = require('../user-safe-password/css/style.css');
const submit=require('./img/submit.png')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class UserSafePassword extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    if (__DEV__) {
    }
    this.store.init()
  }


  render() {
    const password = this.store.state().get('password')
    const customerId = this.store.state().get('customerId')
    const isShowpwd = this.store.state().get('isShowpwd')
    return (
      <div className="content register-content">
        <div className="register-box usersafe-pw-next-box" style={{paddingBottom: '0',width: '100%',margin: '0 auto',}}>
          <div className="row form-item" style={{
            height: '1.1rem',
            padding: '.16rem .45rem'
          }}>
            <input
              className="formInput"
              type={isShowpwd ? "text" : "password"}
              pattern="/^[A-Za-z0-9]{6,16}$/"
              placeholder="请输入新的登录密码"
              value={password}
              maxLength={16}
              style={{
                fontSize: '0.28rem',
                fontFamily: 'PingFang SC',
                fontWeight: 500,
                color: '#333333',
                width: '85%'
              }}
              onChange={(e) => this.store.getNewPass(e.target.value)}
            />
            <i onClick={this.store.showPwd}
              className={`iconfont icon-${isShowpwd ? 'yanjing' : 'iconguanbiyanjing'}`}
              style={{
                fontSize: '.36rem'
              }}></i>
          </div>

          {/* <div className="row form-item">
            <span className="form-text">新密码1</span>
            <input placeholder="请输入新的登录密码" type={isShowpwd ? "text" : "password"} className="form-input" pattern="/^[A-Za-z0-9]{6,16}$/"
                   maxLength={16}  value={password} onChange={(e) => this.store.getNewPass(e.target.value)}/>
            <i onClick={this.store.showPwd}
               className={`iconfont icon-${isShowpwd ? 'yanjing' : 'iconguanbiyanjing'}`}></i>
          </div> */}

          <div className="register-tips" style={{
            padding:'.45rem'
          }}>
            <p style={{
              fontSize: '.26rem',
              fontFamily: 'PingFang SC',
              fontWeight: 500,
              color: 'rgba(153, 153, 153, 1)',
              lineHeight: '.39rem',
            }}>提示：</p>

            <p style={{
              fontSize: '.26rem',
              fontFamily: 'PingFang SC',
              fontWeight: 500,
              color: 'rgba(153, 153, 153, 1)',
              lineHeight: '.39rem',
            }}>1.为了保障您的账户信息安全，在您变更账户重要信息时，需要对您的身份进行验证。感谢您的理解和支持！</p>

            <p style={{
              fontSize: '.26rem',
              fontFamily: 'PingFang SC',
              fontWeight: 500,
              color: 'rgba(153, 153, 153, 1)',
              lineHeight: '.39rem',
            }}>2.如出现收不到短信的情况，可能是由于通信网络异常造成，请您稍后重新尝试操作！</p>
          </div>
          <div
            style={{
            position: 'relative',
            width: '4.6rem',
            margin: 'auto'
          }}
          >
            {/* <button className="btn btn-primary myBtn" onClick={() => this.store.updatePass()}>提交
          </button> */}
          <img src={submit} alt="" style={{width:'100%'}} onClick={() => this.store.updatePass()}/>
          </div>
        </div>
        <div className="footer-copyright">
              <p>粤ICP备19120716号-1</p>
            </div>
      </div>
    )
  }
}
