/**
 * Created by feitingting on 2017/7/20.
 */
import {Fetch} from 'wmkit'

export const updatePass = (customerId: string, vertiCode: string, password: string, isForgetPassword: boolean) => {
  return Fetch('/passwordByForgot', {
    method: 'POST',
    body: JSON.stringify({
      customerId: customerId,
      verifyCode: vertiCode,
      customerPassword: password,
      isForgetPassword:isForgetPassword
    })
  })
}