/**
 * Created by feitingting on 2017/7/20.
 */
import { Store, IOptions } from 'plume2';
import * as webapi from './webapi';
import { Alert } from 'wmkit';
import SafePassNextActor from './actor/safepass-next-actor';
import { cache } from 'config';
import { history, WMkit } from 'wmkit';

export default class AppStore extends Store {
  bindActor() {
    return [new SafePassNextActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  init = () => {
    this.dispatch('safepassnext:init');
  };

  getNewPass = (pass: string) => {
    this.dispatch('safepassnext:newpass', pass);
  };

  showPwd = () => {
    this.dispatch('safepassnext:showpass', this.state().get('isShowpwd'));
  };

  updatePass = async () => {
    const newpass = this.state().get('password');
    const vertiCode = localStorage.getItem(cache.FORGET_CODE);
    const customerId = localStorage.getItem(cache.CUSTOMER_ID);
    if (WMkit.testPass(newpass)) {
      const { code, message, context } = await webapi.updatePass(
        customerId,
        vertiCode,
        newpass,
        !WMkit.isLogin()
      );
      if (code == 'K-000000') {
        //清除token
        WMkit.clearLoginCache();
        Alert({
          text: '密码修改成功！'
        });
        setTimeout(() => {
          history.push({
            pathname: '/login'
          });
        }, 1000);
      } else {
        Alert({
          text: message
        });
      }
    }
  };
}
