/**
 * Created by feitingting on 2017/7/20.
 */
import { Action, Actor } from 'plume2';

export default class SafePassNextActor extends Actor {


  defaultState() {
    return {
      password: "",
      isShowpwd: false,
    }
  }


  /**
   * 监听密码状态值
   * @param state
   */
  @Action('safepassnext:init')
  init(state) {
    return state.set('password', "")
      .set('isShowpwd', false)
  }


  /**
   * 设置新密码
   * @param state
   * @param value
   */
  @Action('safepassnext:newpass')
  getPass(state, value: string) {
    return state.set('password', value)
  }


  /**
   * 是否显示密码
   * @param state
   * @param showpass
   */
  @Action('safepassnext:showpass')
  showpass(state, showpass: boolean) {
    return state.set('isShowpwd', !showpass)
  }
}
