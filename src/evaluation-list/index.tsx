import React from 'react';
import { StoreProvider } from 'plume2';

import AppStore from './store';
import GoodsEvaluation from './component/goods-evaluation';
import FindBigPicture from './component/find-big-picture';
import './css/style.css';
import Header from './component/header';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class EvaluationList extends React.PureComponent<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="goods-detail" style={{height:"100%",width:"100%",position:"absolute",background:"#fff"
    }}>
        {/* <Header /> */}
        {!this.store.state().get('findBigImage') && (
          <GoodsEvaluation godosId={this.props.match.params.goodsId} />
        )}

        {/* 查看大图 */}
        {this.store.state().get('findBigImage') && <FindBigPicture />}
      </div>
    );
  }
}
