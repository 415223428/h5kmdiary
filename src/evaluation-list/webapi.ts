import { Fetch, WMkit } from 'wmkit';

/**
 * 商品评价点赞
 * @param customergoodsevaluatepraise
 */
export const addCustomerGoodsEvaluatePraise = (customergoodsevaluatepraise) => {
  return Fetch('/customergoodsevaluatepraise/add', {
    method: 'POST',
    body: JSON.stringify(customergoodsevaluatepraise)
  });
};

/**
 * 获取商品评论详情
 * @param goodsEvaluateByIdRequest
 */
export const getCustomerGoodsEvaluate = (goodsEvaluateByIdRequest) =>{
  return Fetch('/goodsEvaluate/getCustomerGoodsEvaluate', {
    method: 'POST',
    body: JSON.stringify(goodsEvaluateByIdRequest)
  });
}
