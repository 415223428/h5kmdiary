import React, { PureComponent } from 'react';
import { IMap, Relax } from 'plume2';
import { Star, noop } from 'wmkit';
import { IList } from '../../../typings/globalType';

@Relax
export default class EvaluateDetail extends PureComponent<any, any> {
  props: {
    relaxProps?: {
      bigImgList: IList;
      bigImgIndex: number;
      addCustomerGoodsEvaluatePraiseForImage: Function;
      goodsEvaluate: IMap;
    };
  };

  static relaxProps = {
    bigImgList: 'bigImgList',
    bigImgIndex: 'bigImgIndex',
    addCustomerGoodsEvaluatePraiseForImage: noop,
    goodsEvaluate: 'goodsEvaluate'
  };

  constructor(props) {
    super(props);
    this.state = {
      zanFlag: false
    };
  }

  render() {
    const {
      bigImgList,
      bigImgIndex,
      addCustomerGoodsEvaluatePraiseForImage,
      goodsEvaluate
    } = this.props.relaxProps;
    const evaluate = bigImgList.get(bigImgIndex).get('goodsEvaluate');
    return (
      <div className="down-content">
        <Star star={evaluate.get('evaluateScore')} />
        <span className="specific">{evaluate.get('specDetails')}</span>
        <span className="introduce">{evaluate.get('evaluateContent')}</span>
        <div className="bottom-innocuo">
          <div className="left-innocuo">
            <i
              className={
                goodsEvaluate.get('isPraise')
                  ? 'iconfont icon-zan red-color'
                  : 'iconfont icon-zan'
              }
              onClick={() => {
                addCustomerGoodsEvaluatePraiseForImage(evaluate.get('evaluateId'));
              }}
            />
            <span>{goodsEvaluate.get('goodNum')}</span>
          </div>
        </div>
      </div>
    );
  }

}
