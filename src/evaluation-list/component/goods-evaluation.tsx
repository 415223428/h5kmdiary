import React, { PureComponent } from 'react';
import { Relax } from 'plume2';
import GoodsEvaluationItem from './goods-evaluation-item';
import { Blank, ListView, noop } from 'wmkit';
import * as WMkit from 'wmkit/kit';

const noneImg = require('../images/list-none.png');
@Relax
export default class GoodsEvaluation extends PureComponent<any, any> {
  static relaxProps = {
    handleDataReached: noop
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { handleDataReached } = this.props.relaxProps;
    let url = '/goodsDetailEvaluate/evaluatePage';
    if (WMkit.isLogin()) {
      url = '/goodsDetailEvaluate/evaluatePageLogin';
    }
    return (
      <div className="evaluation-list b-1px-b">
        <ListView
          url={url}
          style={{background:'#fff'}}
          params={{ goodsId: this.props.godosId }}
          dataPropsName={'context.goodsEvaluateVOPage.content'}
          renderRow={(item: any) => {
            return (
              <GoodsEvaluationItem
                evaluate={item}
                key={item.evaluateId + 'list'}
              />
            );
          }}
          renderEmpty={() => (
            <Blank
              img={noneImg}
              content="快去购买吧~成为第一个分享者~~迈出购物达人的第一步"
            />
          )}
          onDataReached={handleDataReached}
        />
      </div>
    );
  }
}
