import React, { PureComponent } from 'react';
import { Relax } from 'plume2';
import moment from 'moment';
import { noop, Star } from 'wmkit';
import { IList } from '../../../typings/globalType';

const defaultImg = require('../images/default-img.png');
@Relax
export default class GoodsEvaluationItem extends PureComponent<any, any> {
  contentRef;

  props: {
    relaxProps?: {
      addCustomerGoodsEvaluatePraise: Function;
      showBigImg: Function;
      zanGoodsEvaluateList: IList;
    };
    evaluate: any;
  };

  static relaxProps = {
    showBigImg: noop,
    addCustomerGoodsEvaluatePraise: noop,
    zanGoodsEvaluateList: 'zanGoodsEvaluateList'
  };

  constructor(props) {
    super(props);
    this.state = {
      isLong: false,
      isExpand: false,
      zanFlag: false
    };
  }

  componentDidMount() {
    const { evaluate } = this.props;
    if (evaluate.isShow == 1) {
      let _height = this.contentRef.offsetHeight;
      if (_height > 18 * 5 - 5) {
        this.setState({
          isLong: true
        });
      }
    }
  }

  render() {
    const {
      showBigImg,
      addCustomerGoodsEvaluatePraise
    } = this.props.relaxProps;
    let { isLong, isExpand } = this.state;
    const { evaluate } = this.props;
    this._zanEvent(evaluate);

    return (
      evaluate.isShow == 1 && (
        <div className="evaluation-item">
          <div className="evaluation-item-head">
            <div className="evaluation-item-head-user">
              <img src={defaultImg} alt="" style={{
                width: '.7rem',
                height: '.7rem'
              }} />
              <div className="evaluation-item-head-info">
                <span className="evaluation-text" style={{
                  fontSize: '.28rem',
                  fontFamily: 'PingFang SC',
                  fontWeight: 'bold',
                  color: 'rgba(51,51,51,1)',
                }}>
                  {evaluate.isAnonymous == 0 ? evaluate.customerName : '匿名'}
                </span>
                <Star star={evaluate.evaluateScore} />
              </div>
            </div>
            <div>
              <div className="evaluation-item-head-time">
                {moment(evaluate.evaluateTime).format('YYYY-MM-DD')}
              </div>
              <div className="evaluation-item-bottom-praise" style={{
                float: 'right',
                paddingTop: '.2rem'
              }}>
                {/* <i
                  className={
                    evaluate.isPraise
                      ? 'iconfont icon-zan red-color'
                      : 'iconfont icon-zan'
                  }
                  onClick={() => {
                    addCustomerGoodsEvaluatePraise(evaluate.evaluateId);
                  }}
                /> */}
                <span onClick={() => {
                  addCustomerGoodsEvaluatePraise(evaluate.evaluateId);
                }}>
                  {
                    evaluate.isPraise
                      ? <img src={require('../images/2.png')} alt="" style={{
                        width: '.3rem',
                        height: '.27rem'
                      }} />
                      : <img src={require('../images/1.png')} alt="" style={{
                        width: '.3rem',
                        height: '.27rem'
                      }} />
                  }
                </span>
                &nbsp;
                &nbsp;
                <span>{evaluate.goodNum}</span>
              </div>
            </div>
          </div>
          <div
            className="evaluation-item-body"
            style={isExpand ? {} : { maxHeight: 18 * 5 + 'px' }}
          >
            <p
              className={
                'evaluation-item-text ' + (isExpand ? '' : 'expand-text')
              }
              ref={(dom) => {
                this.contentRef = dom;
              }}
              style={{
                fontSize: '.26rem',
                fontFamily: 'PingFang SC',
                fontWeight: 500,
                color: 'rgba(51,51,51,1)',
                lineHeight: '.39rem'
              }}
            >
              {evaluate.evaluateContent}
            </p>
          </div>
          {isLong && (
            <div className="evaluation-item-expand">
              <span onClick={() => this.setState({ isExpand: !isExpand })}>
                <span>{isExpand ? '收起' : '展开'}</span>
                <i
                  className="iconfont icon-xiala-copy"
                  style={{
                    transform: 'rotate(' + (isExpand ? 180 : 0) + 'deg)'
                  }}
                />
              </span>
            </div>
          )}
          {evaluate.evaluateImageList && (
            <div className="evaluation-item-img">
              {/* 4张图加载className="list-four" 一张图加载className list-one */}
              <ul className={this._imgCss(evaluate.evaluateImageList.length)}>
                {evaluate.evaluateImageList.map((v) => {
                  return (
                    <li
                      className="evaluation-item-img-item "
                      key={v.imageId}
                      onClick={() => {
                        showBigImg(evaluate, v.imageId);
                      }}
                    >
                      <img src={v.artworkUrl} alt="" />
                    </li>
                  );
                })}
              </ul>
              {
                <div className="evaluation-item-img-tag">
                  <span>共{evaluate.evaluateImageList.length}张</span>
                </div>
              }
            </div>
          )}

          <div className="evaluation-item-bottom">
            <div className="evaluation-item-bottom-info">
              <div className="evaluation-item-bottom-info-text">
                <span>{evaluate.specDetails}</span>
              </div>
              <p className="evaluation-item-bottom-info-text">
                购买时间：{moment(evaluate.buyTime).format('YYYY-MM-DD')}
              </p>
            </div>
            {/* <div className="evaluation-item-bottom-praise">
              <i
                className={
                  evaluate.isPraise
                    ? 'iconfont icon-zan red-color'
                    : 'iconfont icon-zan'
                }
                onClick={() => {
                  addCustomerGoodsEvaluatePraise(evaluate.evaluateId);
                }}
              />
              <span>{evaluate.goodNum}</span>
            </div> */}
          </div>
          {evaluate.evaluateAnswer && (
            <div
              className="evaluation-item-bottom-info-text"
              style={{ marginTop: 10 }}
            >
              <span>掌柜回复：{evaluate.evaluateAnswer}</span>
            </div>
          )}
        </div>
      )
    );
  }

  _zanEvent = (evaluate) => {
    const { zanGoodsEvaluateList } = this.props.relaxProps;
    if (evaluate.isPraise) {
      return evaluate;
    } else {
      let goodNum = 0;
      //已点赞过
      const index = zanGoodsEvaluateList.findIndex(
        (zanGoodsEvaluate) =>
          zanGoodsEvaluate.get('evaluateId') == evaluate.evaluateId
      );
      if (index > -1) {
        evaluate.isPraise = 1;
        evaluate.goodNum = zanGoodsEvaluateList.get(index).get('goodNum');
        return evaluate;
      } else {
        return evaluate;
      }
    }
  };

  _imgCss = (imgCount) => {
    let imgCss;
    if (imgCount == 1) {
      imgCss = 'evaluation-item-img-list list-one clearfix';
    } else if (imgCount == 4) {
      imgCss = 'evaluation-item-img-list list-four clearfix';
    } else {
      imgCss = 'evaluation-item-img-list list-six clearfix';
    }
    return imgCss;
  };
}
