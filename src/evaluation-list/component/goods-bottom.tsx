import React, { PureComponent } from 'react';
import { IMap, Relax } from 'plume2';
import { Button, noop } from 'wmkit';

const SubmitButton = Button.Submit;

@Relax
export default class GoodsBottom extends PureComponent<any, any> {
  props: {
    relaxProps?: {
      purchaseCount: number;
      goods: IMap;
      changeWholesaleVisible: Function;
      changeRetailSaleVisible: Function;
    };
  };

  static relaxProps = {
    purchaseCount: 'purchaseCount',
    goods: 'goods',
    changeWholesaleVisible: noop,
    changeRetailSaleVisible: noop
  };

  render() {
    const {
      purchaseCount,
      changeWholesaleVisible,
      changeRetailSaleVisible,
      goods
    } = this.props.relaxProps;
    return (
      <div style={{ height: 48 }}>
        <div className="detail-bottom">
          <div className="detail-bottom-bar">
            <div className="bar-item">
              <i className="iconfont icon-kefu2" />
              <span>客服</span>
            </div>
            <div className="bar-item">
              <i className="iconfont icon-shouye01" />
              <span>店铺</span>
            </div>
            <div className="bar-item">
              <div className="cart-content">
                {purchaseCount > 0 && (
                  <div className="num-tips">{purchaseCount}</div>
                )}
                <i className="iconfont icon-gouwuche" />
              </div>
              <span className="red">购物车</span>
            </div>
          </div>
          <SubmitButton
            defaultStyle={{ height: '0.94rem', lineHeight: '0.94rem' }}
            text={'加入购物车'}
            onClick={() =>
              goods.get('saleType') == 0
                ? changeWholesaleVisible()
                : changeRetailSaleVisible(true)
            }
          />
        </div>
      </div>
    );
  }
}
