import { Action, Actor } from 'plume2';
import { fromJS, List } from 'immutable';

export default class EvaluationListActor extends Actor {
  defaultState() {
    return {
      //大图模式图片数组
      bigImgList: [],
      //查看大图
      findBigImage: false,
      //获取图片地址
      bigImgIndex: 0,
      //商品评价详情
      goodsEvaluate: {},
      //已点赞数据
      zanGoodsEvaluateList: []
    };
  }

  //查看大图
  @Action('evaluationList:findBigPicture')
  findBigPicture(state, value) {
    return state.set('findBigImage', value);
  }

  //获取图片地址
  @Action('evaluationList:bigImgIndex')
  setEvaluate(state, value) {
    const bigImgList = state.get('bigImgList');
    let index = 0;
    bigImgList.map((v, i) => {
      if (v.get('imageId') == value) {
        index = i;
      }
    });
    return state.set('bigImgIndex', index);
  }

  @Action('evaluationList:bigImgList')
  setBigImgList(state, list) {
    let bigImglist = [];
    let textlist = [];
    list.map((v) => {
      const evaluateImageList = v.evaluateImageList;
      if (evaluateImageList != null) {
        bigImglist.push(...evaluateImageList);
      }
    });
    return state.update('bigImgList', (imgs) =>
      imgs.concat(fromJS(bigImglist))
    );
  }

  @Action('evaluateList:changeIndex')
  changeIndex(state, val) {
    return state.set('bigImgIndex', val);
  }

  @Action('evaluateList:setGoodsEvaluate')
  setGoodsEvaluate(state, val) {
    return state.set('goodsEvaluate', val);
  }

  @Action('evaluateList:setZanGoodsEvaluateList')
  setZanGoodsEvaluateList(state, goodsEvaluate) {
    return state.update('zanGoodsEvaluateList', (list) => list.push(goodsEvaluate));
  }
}
