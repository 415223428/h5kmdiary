import { IOptions, msg, Store } from 'plume2';

import EvaluationListActor from './actor/evaluation-list-actor';
import * as WMkit from 'wmkit/kit';
import { config } from 'config';
import * as webapi from './webapi';
import { Alert } from 'wmkit';
import { fromJS } from 'immutable';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new EvaluationListActor()];
  }

  //获取图片地址
  showBigImg = (goodsEvaluate, imageId) => {
    this.dispatch('evaluateList:setGoodsEvaluate', fromJS(goodsEvaluate));
    this.dispatch('evaluationList:findBigPicture', true);
    //图片地址
    this.dispatch('evaluationList:bigImgIndex', imageId);
  };

  closeBigImg = () => this.dispatch('evaluationList:findBigPicture', false);

  handleDataReached = (data: any) => {
    if (data.code !== config.SUCCESS_CODE) {
      return;
    }
    const { goodsEvaluateVOPage } = data.context;
    this.dispatch('evaluationList:bigImgList', goodsEvaluateVOPage.content);
  };

  curIndex = (index: number) =>
    this.dispatch('evaluateList:changeIndex', index);

  init = () => {
    location.reload();
  };

  /**
   * 商品评价点赞
   * @param goodsEvaluateId
   */
  addCustomerGoodsEvaluatePraise = async (goodsEvaluateId) => {
    if (WMkit.isLogin()) {
      const { code, message } = await webapi.addCustomerGoodsEvaluatePraise({
        goodsEvaluateId: goodsEvaluateId
      });
      if (code == config.SUCCESS_CODE) {
        Alert({ text: '点赞成功!' });
        //获取评价详情
        const res:any = await webapi.getCustomerGoodsEvaluate({
          evaluateId: goodsEvaluateId
        });
        if(res.code == config.SUCCESS_CODE){
          this.dispatch('evaluateList:setGoodsEvaluate', fromJS(res.context));
          const {evaluateId,isPraise,goodNum} = res.context;
          this.dispatch('evaluateList:setZanGoodsEvaluateList',fromJS({evaluateId,isPraise,goodNum}));
        }
      } else {
        Alert({ text: message });
      }
    } else {
      //显示登录弹框
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          location.reload();
        }
      });
    }
  };

  /**
   * 商品评价点赞
   * @param goodsEvaluateId
   */
  addCustomerGoodsEvaluatePraiseForImage = async (goodsEvaluateId) => {
    if (WMkit.isLogin()) {
      const { code, message } = await webapi.addCustomerGoodsEvaluatePraise({
        goodsEvaluateId: goodsEvaluateId
      });
      if (code == config.SUCCESS_CODE) {
        Alert({ text: '点赞成功!' });
        //获取评价详情
        const res:any = await webapi.getCustomerGoodsEvaluate({
          evaluateId: goodsEvaluateId
        });
        if(res.code == config.SUCCESS_CODE){
          this.dispatch('evaluateList:setGoodsEvaluate', fromJS(res.context));
        }
      } else {
        Alert({ text: message });
      }
    } else {
      //显示登录弹框
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          location.reload();
        }
      });
    }
  };
}
