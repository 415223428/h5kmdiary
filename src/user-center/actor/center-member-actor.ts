import { Actor, Action, IMap } from 'plume2';
import { fromJS } from 'immutable';
import { IList } from 'typings/globalType';

export default class CenterMemberActor extends Actor {
  defaultState() {
    return {
      pcLogo: [],
      //显示在会员中心的数据
      customer: {},
      goodsFollow: 0,
      storeFollow: 0,
      // 平台客服标题
      serviceTitle: '',
      // 平台客服列表
      onlineServices: [],
      growthValueIsOpen: false, //是否打开成长值
      pointsIsOpen: false, //积分是否打开
      evaluateIsOpen: false, //评价是否打开
      amount: fromJS({}),
      // 分销员信息
      distributor: {},
      // 热销商品
      hotGoodsList: [],
      // 分享赚选中的sku
      checkedSku: {},
      // 分享弹出显示与否
      shareVisible: false,
      // 分销设置
      distributeSetting: {},
      isfenxiang:false,
      closeImg:true,
      fenxiangId:'',
      closeChoose:true,
      isWechatShare:false,
      isWithdrawTip:false
    };
  }
/**
   * logo
   * @param state
   * @param config
   */
  @Action('logo:init')
  getLogo(state, config: IList) {
    return state
      .set('pcLogo', config)
  }
  /**
   * 初始化数据
   * @param state
   * @param customer
   * @returns {Map<string, V>}
   */
  @Action('member:init')
  init(state: IMap, { customer, serviceTitle, onlineServices }) {
    return state
      .set('customer', fromJS(customer))
      .set('serviceTitle', serviceTitle)
      .set('onlineServices', onlineServices);
  }

  /**
   * 初始化qq客服
   * @param state
   * @param customer
   * @returns {Map<string, V>}
   */
  @Action('member:initOnlineServices')
  initOnlineServices(state: IMap, { serviceTitle, onlineServices }) {
    return state
      .set('serviceTitle', serviceTitle)
      .set('onlineServices', onlineServices);
  }

  /**
   * 初始化商品收藏
   * @param state
   * @param goodsFollow
   * @returns {Map<string, V>}
   */
  @Action('goodsFollow:init')
  goodsFollow(state: IMap, goodsFollow) {
    return state.set('goodsFollow', goodsFollow);
  }

  /**
   * 初始化商品收藏
   * @param state
   * @param storeFollow
   * @returns {Map<string, V>}
   */
  @Action('storeFollow:init')
  storeFollow(state: IMap, storeFollow) {
    return state.set('storeFollow', storeFollow);
  }

  /**
   * 是否成长值关闭了
   */
  @Action('userInfo:growthValueIsOpen')
  growthValueIsOpen(state: IMap) {
    return state.set('growthValueIsOpen', true);
  }

  /**
   * 是否成长值关闭了
   */
  @Action('userInfo:pointsIsOpen')
  pointsIsOpen(state: IMap) {
    return state.set('pointsIsOpen', true);
  }

  /**
   * 评价是否打开
   */
  @Action('userInfo:evaluateIsOpen')
  evaluateIsOpen(state: IMap) {
    return state.set('evaluateIsOpen', true);
  }

    /**
   * 根据返回的数据填充字段
   */
  @Action('set:amount')
  tipLayerShow(state, val) {
    return state.set('amount', val);
  }

  // 分销员信息
  @Action('set:distributor')
  distributor(state,val) {
    return state.set('distributor', val);
  }

  // 热销商品
  @Action('set:hotGoodsList')
  hotGoodsList(state,val) {
    return state.set('hotGoodsList', val);
  }

  /**
   * 切换分享赚弹框显示与否
   */
  @Action('center: share: visible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }

  /**
   * 数据变化
   * @param state
   * @param context
   * @returns {*}
   */
  @Action('center: field: change')
  initList(state: IMap, { field, value }) {
    return state.set(field, value);
  }

  // 店铺状态
  @Action('set:distributeSetting')
  distributeSetting(state,val) {
    return state.set('distributeSetting', val);
  }


   //打开分享页面
   @Action('goods-actor:isfenxiang')
   isfenxiang(state){
     return state.set('isfenxiang',true)
   }
   
 //关闭分享页面
   @Action('goods-actor:closefenxiang')
   closefenxiang(state){
     return state.set('isfenxiang',false)
   }
 
   //关闭img
   @Action('goods-actor:closeImg')
   closeImg(state){
     return state.set('closeImg',false)
     .set('closeChoose',false)
   }
   
   //打开Img
   @Action('goods-actor:openImg')
   openImg(state){
     return state.set('closeImg',true)
     .set('closeChoose',true)
   }
 
   @Action('goods-actor:fenxiangId')
   sendId(state,fenxiangId){
     return state.set('fenxiangId',fenxiangId)
   }

   @Action('goods-actor:openWechatShare')
   openWechatShare(state){
     return state.set('isWechatShare',true)
   }
 
   @Action('goods-actor:closeWechatShare')
   closeWechatShare(state){
     return state.set('isWechatShare',false)
   }

   @Action('actor:withdrawtip')
   changewhitdrawtip(state){
     return state.set('isWithdrawTip',!state.get('isWithdrawTip'))
   }
}



