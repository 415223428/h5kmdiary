import { Fetch } from 'wmkit';

/**
 * 我的会员中心
 * @returns
 */
export const fetchCustomerCenterInfo = () => {
    return Fetch(
        '/customer/customerCenter',
        {},
        {
            showTip: false
        }
    );
};

/**
 * 查询商品收藏数量
 */
export const fetchGoodsFollowNum = () => {
  return Fetch(
    '/goods/goodsFollowNum',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 查询店铺关注数量
 */
export const fetchStoreFollowNum = () => {
  return Fetch(
    '/store/storeFollowNum',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 获取平台客服信息
 */
export const fetchOnlineServiceList = () => {
  return Fetch(
    '/customerService/qq/detail/0/1',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 查询成长值是否开启
 */
export const growthValueIsOpen = () => {
  return Fetch(
    '/growthValue/isOpen',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 获取积分设置
 */
export const basicRules = () => {
  return Fetch(
    '/pointsConfig',
    {},
    {
      showTip: false
    }
  );
};

// 获取用户余额
export const init = () => {
  return Fetch('/customer/funds/statistics');
}

// 获取分销员信息
export const fetchDistributorInfo = () => {
  return Fetch(
    '/distribute/distributor-info',
    {},
    {
      showTip: false
    }
  )
}

// 获取热销商品
export const fetchHotDistributeList = (params) => {
  return Fetch(
    '/goods/shop/add-distributor-goods',
    {
      method: 'POST',
      body: JSON.stringify(params)
    },
    {
      showTip: false
    }
  );
}

// 获取店铺状态
export const fetchInvitorInfo = () => {
  return Fetch(
    '/distribute/setting-invitor',
    {},
    {
      showTip: false
    }
  );
}