import React, { Component } from 'react';
import { IMap, msg, Relax } from 'plume2';
import { IList } from 'typings/globalType';
import { _, history, Blank, WMkit, Alert, noop } from 'wmkit';
import { GoodsNum } from 'biz';
import GoodsShareImg from './goods-share-img'
import {cache} from 'config'

const noneImg = require('./img/list-none.png');
const faquan = require('../img/faquan.png')
const share = require('../img/share.png')
const self = require('../img/self-support.png')
@Relax
export default class SellwellGoods extends Component<any, any> {
  props: {
    relaxProps?: {
      customer: any;
      hotGoodsList: IList;
      saveCheckedSku: Function;
      changeShareVisible: Function;
      distributor: IMap;
      changefenxiang: Function;
      sendId: Function;
      isfenxiang: boolean;
      cloasefenxiang: Function;
      closeImg: boolean;
      iscloseImg: Function;
      isopenImg: Function;
      fenxiangId: String;
      getLogo: Function;
      closeChoose: boolean;
      openWechatShare: Function;
      isWechatShare: boolean;
    };
  };

  static relaxProps = {
    customer: 'customer',
    hotGoodsList: 'hotGoodsList',
    saveCheckedSku: noop,
    changeShareVisible: noop,
    distributor: 'distributor',
    changefenxiang: noop,
    sendId: noop,
    isfenxiang: 'isfenxiang',
    cloasefenxiang: noop,
    iscloseImg: noop,
    closeImg: 'closeImg',
    isopenImg: noop,
    fenxiangId: 'fenxiangId',
    getLogo: noop,
    closeChoose: 'closeChoose',
    openWechatShare: noop,
    isWechatShare: 'isWechatShare',
  };
  constructor(props) {
    super(props);
    this.state = props;
  }
  render() {
    const {
      changefenxiang,
      sendId,
      customer,
      isfenxiang,
      cloasefenxiang,
      closeImg,
      iscloseImg,
      isopenImg,
      fenxiangId,
      hotGoodsList,
      saveCheckedSku,
      changeShareVisible,
      distributor,
      getLogo,
      closeChoose,
      openWechatShare, isWechatShare
    } = this.props.relaxProps;
    // const {goodsItem}=this.state;

    // 分销员是否禁用 0: 启用中  1：禁用中
    let forbidFlag = distributor.get('forbiddenFlag') || 0;
    // console.log(forbidFlag);

    // FIXME 销量、评价、好评展示与否，后期读取后台配置开关
    // 此版本默认都展示
    const isShow = true;
    return (
      <div className="sellwell-goods" >
        <div className="sellwell-goods-title" style={{ color: '#000', fontSize: '0.28rem', marginLeft: '0.4rem', fontWeight: 'bold' }}>猜你喜欢</div>
        <div className="sellwell-goods-list">
          {hotGoodsList && hotGoodsList.size > 0 ? (
            hotGoodsList.map((goods) => {
              // sku信息
              const goodsInfo = goods.get('goodsInfo');
              /////////////
              let params = goodsInfo.get('goodsInfoId');
              // 库存
              const stock = goodsInfo.get('stock');
              // 商品是否要设置成无效状态
              // 起订量
              const count = goodsInfo.get('count') || 0;
              // 库存等于0或者起订量大于剩余库存
              const invalid = stock <= 0 || (count > 0 && count > stock);
              const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;

              //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
              //好评率
              let favorableRate = '100';
              if (
                goodsInfo.get('goodsEvaluateNum') &&
                goodsInfo.get('goodsEvaluateNum') != 0
              ) {
                favorableRate = _.mul(
                  _.div(
                    goodsInfo.get('goodsFavorableCommentNum'),
                    goodsInfo.get('goodsEvaluateNum')
                  ),
                  100
                ).toFixed(0);
              }

              //评论数
              let evaluateNum = '暂无';
              const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
              if (goodsEvaluateNum) {
                if (goodsEvaluateNum < 10000) {
                  evaluateNum = goodsEvaluateNum;
                } else {
                  const i = _.div(goodsEvaluateNum, 10000).toFixed(1);
                  evaluateNum = i + '万+';
                }
              }

              //销量
              let salesNum = '暂无';
              const goodsSalesNum = goodsInfo.get('goodsSalesNum');
              if (goodsSalesNum) {
                if (goodsSalesNum < 10000) {
                  salesNum = goodsSalesNum;
                } else {
                  const i = _.div(goodsSalesNum, 10000).toFixed(1);
                  salesNum = i + '万+';
                }
              }
              // 预估点数
              let pointNum = goodsInfo.get('kmPointsValue');
              //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆
              const inviteeId = JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '';
              const inviteCode = sessionStorage.getItem('inviteCode');
              const url = `goods-detail/${goodsInfo.get('goodsInfoId')}/?channel=mall&inviteeId=${inviteeId}&inviteCode=${inviteCode}`;
              const id = goodsInfo.get('goodsInfoId');
              return (
                <div>

                  {
                  // isfenxiang ?
                    // <GoodsShareImg
                    //   goodsItem={goods}
                    //   isfenxiang={isfenxiang}
                    //   customer={customer}
                    //   cloasefenxiang={cloasefenxiang}
                    //   closeImg={closeImg}
                    //   iscloseImg={iscloseImg}
                    //   isopenImg={isopenImg}
                    //   fenxiangId={fenxiangId}
                    //   params={params}
                    //   getLogo={getLogo}
                    //   closeChoose={closeChoose}
                    //   openWechatShare={openWechatShare}
                    //   isWechatShare={isWechatShare}
                    // />
                    // :
                   !invalid?
                    <div
                      className="sellwell-goods-list-item"
                      key={goods.get('id')}
                      onClick={() =>
                        history.push(
                          '/goods-detail/' + goodsInfo.get('goodsInfoId')
                        )
                      }
                      style={{
                        width: '7.1rem', height: '3.3rem', background: '#fff', boxShadow: '0 0.06rem 0.20rem 0 rgba(0, 0, 0, 0.04)',
                        borderRadius: '0.2rem', display: 'flex', alignItems: 'center', margin: '0.2rem auto', padding: '0.3rem'
                      }}
                    >
                      <div className="sellwell-img-box" style={{ width: '2.2rem', height: '2.2rem', objectFit: 'scale-down', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <img
                          src={goodsInfo.get('goodsInfoImg') || noneImg}
                          // width="100%"
                          height="100%"
                          alt=""
                          style={{ objectFit: 'cover' }}
                        />
                      </div>
                      <div className="sellwell-goods-item-right" style={{ marginLeft: '0.4rem' }}>
                        <p className="sellwell-name" style={{ fontSize: '0.26rem', color: '#333', fontWeight: 'bold' }}>
                          {goodsInfo.get('goodsInfoName')}
                        </p>
                        <span className="sellwell-spe" style={{ fontSize: '0.26rem', color: '#333', fontWeight: 'bold' }}>
                          {goodsInfo.get('specText')}
                        </span>

                        {/* 评价 */}
                        {isShow ? (
                          <div className="goods-evaluate" style={{ marginTop: '0.1rem', marginBottom: '0.1rem' }}>
                            <span className="goods-evaluate-spn" style={{ fontSize: '0.24rem' }}>
                              {salesNum}销量
                          </span>
                            <span className="goods-evaluate-spn mar-lr-28" style={{ fontSize: '0.24rem' }}>
                              {evaluateNum}
                              评价
                          </span>
                            <span className="goods-evaluate-spn" style={{ fontSize: '0.24rem' }}>
                              {favorableRate}
                              %好评
                          </span>
                            {localStorage.getItem('loginSaleType') == '1' ? <span className="goods-evaluate-spn" style={{ fontSize: '0.24rem' }}>预估点数{pointNum}</span> : null}
                          </div>
                        ) : (
                            <div className="goods-evaluate" style={{ marginTop: '0.1rem', marginBottom: '0.1rem' }}>
                              <span className="goods-evaluate-spn" style={{ fontSize: '0.2rem' }}>
                                {salesNum}销量
                          </span>
                            </div>
                          )}

                        {goodsInfo.get('companyType') == 0 && (
                          <div className="marketing">
                            <div className="self-sales">
                              自营
                            {/* <img src={self} alt=""/> */}
                            </div>
                          </div>
                        )}

                        <div className="sellwell-bottom" style={{ margin: '0.1rem 0' }}>
                          <div className="sellwell-bottom-left" style={{ display: 'flex', alignItems: 'center', marginLeft: '-0.05rem' }}>
                            <span style={{ fontSize: '0.27rem', fontWeight: 'bold', color: '#FF4D4D' }}>￥</span>
                            <span style={{ fontSize: '0.38rem', fontWeight: 'bold', color: '#FF4D4D' }}>{_.addZero(goodsInfo.get('marketPrice'))}</span>
                            {WMkit.isDistributor() && !forbidFlag && (
                              <div style={{ color: '#FF4D4D', fontSize: '0.23rem' }}>
                                <span style={{ color: '#333', fontSize: '0.23rem' }}>&nbsp;&nbsp;赚</span>
                                ￥{_.addZero(
                                  goodsInfo.get('distributionCommission')
                                )}
                              </div>
                            )}
                          </div>
                        </div>
                        {invalid && <div className="out-stock">缺货</div>}
                        {WMkit.isDistributor() && !invalid ? (
                          <div className="sellwell-bottom-right">
                            {WMkit.isDistributor() && !forbidFlag ? (
                              <div style={{ display: 'flex', alignItems: 'center' }}>
                                <img src={faquan} alt="" style={{ border: 'none', width: '1.3rem' }}
                                  onClick={async (e) => {
                                    e.stopPropagation();
                                    const result = await WMkit.getDistributorStatus();
                                    if (
                                      (result.context as any).distributionEnable
                                    ) {
                                      history.push(
                                        '/graphic-material/' + goods.get('id')
                                      );
                                    } else {
                                      let reason = (result.context as any)
                                        .forbiddenReason;
                                      msg.emit('bStoreCloseVisible', {
                                        visible: true,
                                        reason: reason
                                      });
                                    }
                                  }} />
                                <img src={share} alt="" style={{ border: 'none', width: '1.3rem', marginLeft: '0.1rem' }}
                                  onClick={async (e) => {
                                    e.stopPropagation();
                                    // sendId(goods.get('id'));
                                    // changefenxiang();
                                    history.push({
                                      pathname: '/share',
                                      state: {
                                        goodsInfo ,
                                        url,
                                        id
                                      }
                                    });
                                  }} />
                              </div>
                            ) : (
                                <GoodsNum
                                  value={buyCount}
                                  max={stock}
                                  disableNumberInput={invalid}
                                  goodsInfoId={goodsInfo.get('goodsInfoId')}
                                />
                              )}
                          </div>
                        ) : null}
                      </div>
                    </div>
                    :null
                  }
                </div>
              );
            })
          ) : (
              <Blank img={noneImg} content="没有搜到任何商品～" />
            )}
        </div>
      </div>
    );
  }
}
