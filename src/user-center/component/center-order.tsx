import React from 'react';
import { Link } from 'react-router-dom';
import { msg, Relax } from 'plume2';
import { history, WMkit } from 'wmkit';

@Relax
export default class centerOrder extends React.Component<any, any> {
  props: {
    relaxProps?: {
      pointsIsOpen: boolean;
      evaluateIsOpen: boolean;
    };
  };

  static relaxProps = {
    pointsIsOpen: 'pointsIsOpen',
    evaluateIsOpen: 'evaluateIsOpen'
  };

  render() {
    const isShop = WMkit.isShop();
    const { pointsIsOpen, evaluateIsOpen } = this.props.relaxProps;
    return (
      <div className="user-order">
        <div
          className="my-order-title"
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/order-list');
                }
              });
            } else {
              history.push('/order-list');
            }
          }}
        >
          <label style={{color:'#000',fontWeight:'bold',fontSize:'0.28rem'}}>订单中心</label>
          <div style={{fontSize:'0.23rem',color:'#999'}}>
            <span>查看全部订单</span>
            <i style={{fontSize:'0.23rem',color:'#999',fontWeight:'bold'}} className="iconfont icon-jiantou1" />
          </div>         
        </div>
        <div className="order-status-center clearfix">
            <div
              className="item"
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      history.push('/group-order-list');
                    }
                  });
                } else {
                  history.push('/group-order-list');
                }
              }}
            >
              <div className="icon-box">
                <img src={require('./img/icon/group.png')} className="iconImg" style={{width:'0.4rem',height:'0.48rem'}} />
              </div>
              <p>拼团订单</p>
            </div>
          <div
            className="item"
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push({
                      pathname: '/order-list',
                      state: {
                        status: 'payState-NOT_PAID'
                      }
                    });
                  }
                });
              } else {
                history.push({
                  pathname: '/order-list',
                  state: {
                    status: 'payState-NOT_PAID'
                  }
                });
              }
            }}
          >
            <div className="icon-box">
              {0 > 0 ? <span className="pot-num">0</span> : null}
              <img src={require('./img/icon/pay.png')} className="iconImg" style={{width:'0.47rem',height:'0.47rem'}} />
            </div>
            <p>待支付</p>
          </div>

          {/* <div
            className="item"
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push({
                      pathname: '/order-list',
                      state: {
                        status: 'flowState-AUDIT'
                      }
                    });
                  }
                });
              } else {
                history.push({
                  pathname: '/order-list',
                  state: {
                    status: 'flowState-AUDIT'
                  }
                });
              }
            }}
          >
            <div className="icon-box">
              {0 > 0 ? <span className="pot-num">0</span> : null}
              <img src={require('./img/icon/2.png')} className="iconImg" />
            </div>
            <p>待发货</p>
          </div> */}

          <div
            className="item"
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push({
                      pathname: '/order-list',
                      state: {
                        status: 'flowState-DELIVERED'
                      }
                    });
                  }
                });
              } else {
                history.push({
                  pathname: '/order-list',
                  state: {
                    status: 'flowState-DELIVERED'
                  }
                });
              }
            }}
          >
            <div className="icon-box">
              {0 > 0 ? <span className="pot-num">0</span> : null}
              <img src={require('./img/icon/receipt.png')} className="iconImg" style={{width:'0.5rem',height:'0.47rem'}} />
            </div>
            <p>待收货</p>
          </div>

          {evaluateIsOpen && (
            <div
              className="item"
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      history.push('/evaluate/evaluate-center');
                    }
                  });
                } else {
                  history.push('/evaluate/evaluate-center');
                }
              }}
            >
              <div className="icon-box">
                <img src={require('./img/icon/comment.png')} className="iconImg" style={{width:'0.53rem',height:'0.47rem'}} />
              </div>
              <p>待评价</p>
            </div>
          )}

          <div
            className="item"
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push('/refund-list');
                  }
                });
              } else {
                history.push('/refund-list');
              }
            }}
          >
            <div className="icon-box">
              {0 > 0 ? <span className="pot-num">0</span> : null}
              <img src={require('./img/icon/withdraw.png')} className="iconImg" style={{width:'0.6rem',height:'0.47rem'}} />
            </div>
            <p>我的售后</p>
          </div>
        </div>

        {/*店铺精选端，以下部分隐藏*/}
        {/* {!isShop && (
          <div
            className="order-status-center clearfix"
            style={{ paddingTop: 0 }}
          >
            <div
              className="item"
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      history.push('/group-order-list');
                    }
                  });
                } else {
                  history.push('/group-order-list');
                }
              }}
            >
              <div className="icon-box">
                <img src={require('./img/icon/6.png')} className="iconImg" />
              </div>
              <p>拼团订单</p>
            </div>
            {pointsIsOpen && (
              <div
                className="item"
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        history.push('/points-order-list');
                      }
                    });
                  } else {
                    history.push('/points-order-list');
                  }
                }}
              >
                <div className="icon-box">
                  <img src={require('./img/icon/7.png')} className="iconImg" />
                </div>
                <p>积分订单</p>
              </div>
            )}
            <div className="item" onClick={() => history.push('/not-develop')}>
              <div className="icon-box">
                <img src={require('./img/icon/8.png')} className="iconImg" />
              </div>
              <p>服务订单</p>
            </div>
            <div className="item" onClick={() => history.push('/not-develop')}>
              <div className="icon-box">
                <img src={require('./img/icon/9.png')} className="iconImg" />
              </div>
              <p>卡券订单</p>
            </div>
            <div className="item" onClick={() => history.push('/not-develop')}>
              <div className="icon-box">
                <img src={require('./img/icon/10.png')} className="iconImg" />
              </div>
              <p>活动报名</p>
            </div>
          </div>
        )} */}
      </div>
    );
  }
}
