import React from 'react';
import { Relax } from 'plume2';
import { history, WMkit } from 'wmkit';
import { msg } from 'plume2';

@Relax
export default class CommonTools extends React.Component<any, any> {
  props: {
    relaxProps?: {
      onlineServices: any;
      serviceTitle: string;
      pointsIsOpen: boolean;
    };
  };

  static relaxProps = {
    onlineServices: 'onlineServices',
    serviceTitle: 'serviceTitle',
    pointsIsOpen: 'pointsIsOpen'
  };
  render() {
    const {
      onlineServices,
      serviceTitle,
      pointsIsOpen
    } = this.props.relaxProps;
    const isShop = WMkit.isShop();
    return (
      <div className="user-order">
        {/* <div className="my-order-title">
          <label>常用功能</label>
        </div> */}
        <div className="order-status-center clearfix">
          {
            !isShop &&
            // pointsIsOpen &&
             (
              <div
                className="item"
                style={{width:'25%'}}
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        history.push('/my-coupon');
                      }
                    });
                  } else {
                    history.push('/my-coupon');
                  }
                }}
              >
                <div className="icon-box">
                  <img src={require('./img/icon/coupon.png')} className="iconImg"/>
                </div>
                <p>优惠券</p>
              </div>
          )}

          {!isShop && (
            <div
              className="item"
              style={{width:'25%'}}
              onClick={() => history.push('/groupon-center')}
            >
              <div className="icon-box">
                <img src={require('./img/icon/go-group.png')} className="iconImg" />
              </div>
              <p>拼团购</p>
            </div>
          )}

          {!isShop && (
            <div
              className="item"
              style={{width:'25%'}}
              onClick={() => history.push('/flash-sale')}
            >
              <div className="icon-box">
                <img src={require('./img/icon/onsale.png')} className="iconImg" />
              </div>
              <p>限时特价</p>
            </div>
          )}

          {!isShop && !WMkit.isDistributor() && (
            <div
              className="item"
              style={{width:'25%'}}
              onClick={() => history.push('/goodsList')}
            >
              <div className="icon-box">
                <img src={require('./img/icon/go-shop.png')} className="iconImg" style={{width:'0.6rem'}}/>
              </div>
              <p>去逛逛</p>
            </div>
          )}

          {!isShop && WMkit.isDistributor() && (
            <div
              className="item"
              style={{width:'25%'}}
              onClick={() => history.push('/distribution-goods-list')}
            >
              <div className="icon-box">
                <img src={require('./img/icon/promotion.png')} className="iconImg" />
              </div>
              <p>推广商品</p>
            </div>
          )}

          {!isShop ? (
            <div
              className="item"
              style={{width:'25%'}}
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      history.push('/user-collection');
                    }
                  });
                } else {
                  history.push('/user-collection');
                }
              }}
            >
              <div className="icon-box">
                <img src={require('./img/icon/collection.png')} className="iconImg" />
              </div>
              <p>我的收藏</p>
            </div>
          ) : (
            ''
        )}

          <div
            className="item"
            style={{width:'25%'}}
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push('/receive-address');
                  }
                });
              } else {
                history.push('/receive-address');
              }
            }}
          >
            <div className="icon-box">
              <img src={require('./img/icon/address.png')} className="iconImg" />
            </div>
            <p>我的地址</p>
          </div>

          {/* 新手课堂 */}
          <div
            className="item"
            style={{width:'25%'}}
            onClick={()=>{
              history.push('./about-us')
            }}
            // onClick={() => {
            //   if (!WMkit.isLoginOrNotOpen()) {
            //     msg.emit('loginModal:toggleVisible', {
            //       callBack: () => {
            //         history.push('/receive-address');
            //       }
            //     });
            //   } else {
            //     history.push('/receive-address');
            //   }
            // }}
          >
            <div className="icon-box">
              <img src={require('./img/icon/class.png')} className="iconImg" />
            </div>
            <p>新手课堂</p>
          </div>



          {/* {!isShop &&
            pointsIsOpen && (
              <div
                className="item"
                onClick={() => {
                  history.push('/points-mall');
                }}
              >
                <div className="icon-box">
                  <img src={require('./img/icon/14.png')} className="iconImg" />
                </div>
                <p>积分商城</p>
              </div>
          )} */}


          {/* {!isShop ? (
            <div
              className="item"
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      history.push('/store-attention');
                    }
                  });
                } else {
                  history.push('/store-attention');
                }
              }}
            >
              <div className="icon-box">
                <img src={require('./img/icon/16.png')} className="iconImg" />
              </div>
              <p>关注店铺</p>
            </div>
          ) : (
            ''
          )} */}

          {
            // onlineServices.size > 0 &&
            !(window as any).isMiniProgram && (
              <div
                className="item"
                style={{width:'25%'}}
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        // history.push('/chose-service/0');
                        WMkit.getQiyuCustomer();
                        window.location.href = (window as any).ysf('url');
                        (window as any).openqiyu();
                      }
                    });
                  } else {
                    // history.push('/chose-service/0');
                    WMkit.getQiyuCustomer();
                    window.location.href = (window as any).ysf('url');
                    (window as any).openqiyu();
                  }
                }}
              >
                <div className="icon-box">
                  <img src={require('./img/icon/customer-service.png')} className="iconImg" />
                </div>
                {/* <p>{serviceTitle}</p> */}
                <p>联系客服</p>
              </div>
            )}
        </div>
      </div>
    );
  }
}
