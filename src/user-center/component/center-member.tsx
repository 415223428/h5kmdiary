import React from 'react';
import { Relax, msg, IMap } from 'plume2';
import wx from 'weixin-js-sdk';
import { history, WMkit, noop, _ } from 'wmkit';
import ReactClipboard from 'react-clipboardjs-copy';

const defaultImg = require('../../../web_modules/images/default-headImg.png');
const withDraw = require('../component/img/withdraw.png')
const mystore = require('../img/mystore.png')
const news = require('../img/news.png')
const setting = require('../img/setting1.png')
@Relax
export default class CenterMember extends React.Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {
      switchIsOpen: false,
      switchDesc: ''
    };
  }

  props: {
    relaxProps?: {
      customer: any;
      flush: Function;
      growthValueIsOpen: boolean;
      pointsIsOpen: boolean;
      amount: IMap;
      distributor: IMap;
      distributeSetting: IMap;
      shopClosedTip: Function;
      changeIsWithdrawTip: Function;
    };
  };

  static relaxProps = {
    customer: 'customer',
    flush: noop,
    growthValueIsOpen: 'growthValueIsOpen',
    pointsIsOpen: 'pointsIsOpen',
    amount: 'amount',
    distributor: 'distributor',
    distributeSetting: 'distributeSetting',
    shopClosedTip: noop,
    changeIsWithdrawTip: noop,
  };

  // status 状态为true的时候 皇冠图标文字颜色 至为灰色
  render() {
    const { customer, flush, amount, distributor, distributeSetting, changeIsWithdrawTip } = this.props.relaxProps;
    const accountBalanceTotal = amount.get('accountBalanceTotal') ? amount.get('accountBalanceTotal') : 0;
    const blockedBalanceTotal = amount.get('blockedBalanceTotal') ? amount.get('blockedBalanceTotal') : 0;
    let forbidFlag = distributor ? distributor.get('forbiddenFlag') : '';
    let shopOpenFlag = distributeSetting ? distributeSetting.get('shopOpenFlag') : '';
    // console.error(distributor)
    let mobile = customer.get('customerAccount');
    let headImg = customer.get('headImg')
      ? customer.get('headImg')
      : defaultImg;

    if (mobile) {
      mobile = mobile.substr(0, 3) + '****' + mobile.substr(7);
    }

    // 登录或者开发访问
    let visible = WMkit.isLoginOrNotOpen();
    // 小B访问其他小B店铺
    let isback = WMkit.isDistributorLoginForShare();
    const customerId = `${customer.get('customerId')}`;
    return (
      <div className="user-center-top">
        <div className="box">
          {/*<p>我的</p>*/}
          <div className="user-box">
            <img src={headImg} style={{ width: '100%', height: '100%' }} />
          </div>
          <div className="user-context">
            <div
              className="name"
              onClick={() => {
                if (visible) {
                  return;
                }
                if ((window as any).isMiniProgram) {
                  wx.miniProgram.navigateTo({
                    url: '../quicklogin/quicklogin'
                  });
                } else {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      flush();
                    }
                  });
                }
              }}
            >
              {visible ?
                customerId.length < 32
                  ? customerId
                  : customer.get('customerName')
                : '登录/注册 >'}
            </div>
            {/* {visible && <p className="number">{mobile}</p>} */}
            {WMkit.isLoginOrNotOpen() && WMkit.isDistributor() && (
              <div
                style={{
                  display: 'flex', flexDirection: 'column'
                }}
              >
                <div
                  style={{
                    display: 'flex', alignItems: 'center', justifyContent: 'center', borderRadius: '0.15rem', marginBottom: '0.1rem',
                    background: 'linear-gradient(165deg,rgba(245,225,144,1),rgba(255,248,204,1),rgba(240,203,117,1))', width: '1.4rem', height: '0.3rem',
                  }}
                >
                  <span style={{ color: '#B37400', fontSize: '0.18rem' }}>{distributor.get('distributorLevelName')}</span>
                </div>
                <div
                  style={{ display: 'flex', alignItems: 'center' }}
                >
                  <ReactClipboard
                    text={distributor.get('inviteCode')}
                    onSuccess={() => alert('已复制邀请码')}
                    onError={() => alert('已复制邀请码')}
                  >
                    <div style={{ display: 'flex', alignItems: 'center', color: '#333', fontSize: '0.17rem' }}>
                      <span style={{ fontSize: '0.23rem' }}>邀请码：</span>
                      <div
                        style={{
                          padding: '0 .1rem', height: '0.3rem', border: '1px solid #333', borderRadius: '0.15rem', fontSize: '0.17rem', color: '#333',
                          display: 'flex', justifyContent: 'center', alignItems: 'center', marginRight: '0.2rem'
                        }}
                      >
                        {distributor.get('inviteCode') || '-'}
                      </div>
                      <span style={{ fontSize: '0.24rem' }}>复制</span>
                    </div>
                  </ReactClipboard>
                </div>
              </div>
            )}
            {
              WMkit.isLoginOrNotOpen() && !WMkit.isDistributor() && (
                <div
                  style={{
                    background: 'rgba(17,211,190,0.4)',
                    width: '2.4rem', height: '0.45rem',
                    display: 'flex', justifyContent: 'space-evenly',
                    alignItems: 'center', borderRadius: '0.23rem'
                  }}
                  onClick={() => {
                    history.push('./store-bags');
                  }}
                >
                  {/* <img src="" alt=""/> */}
                  <span style={{ color: '#005044', fontSize: '0.23rem' }}>升级店主去赚钱</span>
                  <i className="iconfont icon icon-jiantou" style={{ transform: 'rotate(180deg)', fontSize: '0.22rem', color: '#005044' }}></i>
                </div>
              )
            }
          </div>
        </div>

        {/* 右侧按钮 */}
        {this.isStore()}

        {/* {isback && (
          <div
            className="back-store"
            onClick={() => WMkit.toMainPageAndClearInviteCache()}
          >
            返回自己店铺
          </div>
        )} */}
        <div className="account"
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/balance/home');
                }
              });
            } else {
              history.push('/balance/home');
            }
          }}
        >
          <div className="left">
            <div className='left-money'>
              <span style={{ color: '#F5C05D', fontSize: '.26rem' }} className='my-center-money'>账户余额</span>
              {
                accountBalanceTotal ?
                  <div>
                    <span className='my-center-money' style={{ fontSize: '0.4rem' }} >￥</span>
                    <span className='my-money'>{_.fmoney(accountBalanceTotal, 2)}</span>
                  </div>
                  :
                  <div>
                    <span className='my-center-money' style={{ fontSize: '0.4rem' }} >￥</span>
                    <span className='my-money'>0.00</span>
                  </div>
              }
            </div>
            <div className='left-money'>
              <span style={{ color: '#F5C05D', fontSize: '.26rem' }} className='my-center-money'>待入账余额</span>
              {
                blockedBalanceTotal ?
                  <div>
                    <span className='my-center-money' style={{ fontSize: '0.4rem' }} >￥</span>
                    <span className='my-money'style={{ fontSize: '0.4rem' }}>{_.fmoney(blockedBalanceTotal, 2)}</span>
                  </div>
                  :
                  <div>
                    <span className='my-center-money' style={{ fontSize: '0.4rem' }} >￥</span>
                    <span className='my-money'style={{ fontSize: '0.4rem' }}>0.00</span>
                  </div>
              }
            </div>
          </div>

          <div>
            {/* <div className="user-center-question">
                      <img src={require('./img/question.png')} alt=""
                        onClick={(e) => {
                          if (e && e.stopPropagation)
                            //因此它支持W3C的stopPropagation()方法
                            e.stopPropagation();
                          else
                            //否则，我们需要使用IE的方式来取消事件冒泡
                            window.event.cancelBubble = true;
                          changeIsWithdrawTip();
                        }}
                      />
                    </div> */}
            <div
              className='my-center-money'
              style={{ color: '#F5C05D', fontSize: '0.26rem', marginBottom: '.1rem' }}
            >查看账户明细 ></div>
            <div className="withdraw"
              onClick={(e) => {

                //阻止冒泡/捕获事件
                if (e && e.stopPropagation)
                  //因此它支持W3C的stopPropagation()方法
                  e.stopPropagation();
                else
                  //否则，我们需要使用IE的方式来取消事件冒泡
                  window.event.cancelBubble = true;

                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      history.push('/balance/deposit');
                    }
                  });
                } else {
                  history.push('/balance/deposit');
                }
              }}
            >
              {/* <span>立即提现</span>
            <i className="iconfont icon-jiantou"></i> */}
              <img src={withDraw} alt="" style={{ width: '1.6rem' }} />
            </div>
          </div>
        </div>
        {visible && this.checkIsOpen()}
      </div>
    );
  }

  /**
   * 成长值与积分的渲染
* @returns {any}
    */
  checkIsOpen = () => {
    const { customer, pointsIsOpen, growthValueIsOpen } = this.props.relaxProps;
    if (growthValueIsOpen && pointsIsOpen) {
      return (
        <p className="growthValue noTriangle">
          <span onClick={() => history.push('/member-center')}>
            成长值:{customer.get('growthValue')}
            <i className="iconfont icon-jiantou"></i>
          </span>
          <span onClick={() => history.push('/user-integral')}>
            积分值:{customer.get('pointsAvailable')}
            <i className="iconfont icon-jiantou"></i>
          </span>
        </p>
      );
    } else if (!growthValueIsOpen && pointsIsOpen) {
      //成长值关闭，积分开启
      return (
        <p
          className="growthValue noTriangle"
          onClick={() => history.push('/user-integral')}
        >
          <span>
            积分值:{customer.get('pointsAvailable')}
            <i className="iconfont icon-jiantou"></i>
          </span>
        </p>
      );
    } else if (growthValueIsOpen && !pointsIsOpen) {
      //成长值开启，积分关闭
      return (
        <p
          className="growthValue noTriangle"
          onClick={() => history.push('/member-center')}
        >
          <span>
            成长值:{customer.get('growthValue')}
            <i className="iconfont icon-jiantou"></i>
          </span>
        </p>
      );
    } else {
      return null;
    }
  };
  stopBubble(e) {
    //如果提供了事件对象，则这是一个非IE浏览器
    if (e && e.stopPropagation)
      //因此它支持W3C的stopPropagation()方法
      e.stopPropagation();
    else
      //否则，我们需要使用IE的方式来取消事件冒泡
      window.event.cancelBubble = true;
  }
  // 店铺按钮是否开放
  isStore() {
    const { distributor, distributeSetting, shopClosedTip } = this.props.relaxProps;
    let forbidFlag = distributor ? distributor.get('forbiddenFlag') : '';
    let shopOpenFlag = distributeSetting ? distributeSetting.get('shopOpenFlag') : '';
    if (WMkit.isLoginOrNotOpen() && WMkit.isDistributor()) {
      return (
        <div className="settingBox">
          {shopOpenFlag == 1 && (
            <img
              src={mystore}
              style={{ width: '0.4rem', height: '0.4rem', marginRight: '0.2rem' }}
              onClick={() =>
                forbidFlag
                  ? shopClosedTip(distributor.get('forbiddenReason'))
                  : history.push('/shop-index')
              }
            />
          )}
          {
            !(window as any).isMiniProgram && (
              <img
                src={require('./img/service.png')}
                style={{ width: '0.4rem', height: '0.4rem', marginRight: '0.2rem' }}
                alt=""
                onClick={() => {
                  WMkit.getQiyuCustomer();
                  window.location.href = (window as any).ysf('url');
                  (window as any).openqiyu();
                }}
              />
            )}
          <img
            src={setting}
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push('/settings');
                  }
                });
              } else {
                history.push('/settings');
              }
            }}
            style={{ width: '0.4rem', height: '0.4rem' }}
          />
        </div>
      );
    } else {
      return (
        <div className="settingBox">
          {!(window as any).isMiniProgram && (
            <img
              src={news}
              style={{ width: '0.4rem', height: '0.4rem', marginRight: '0.2rem' }}
              alt=""
              onClick={() => {
                WMkit.getQiyuCustomer();
                window.location.href = (window as any).ysf('url');
                (window as any).openqiyu();
              }}
            />
          )}
          <img
            src={setting}
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push('/settings');
                  }
                });
              } else {
                history.push('/settings');
              }
            }}
            style={{ width: '0.4rem', height: '0.4rem' }}
          />
          {/* <i
            className="iconfont icon-shezhi"
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push('/settings');
                  }
                });
              } else {
                history.push('/settings');
              }
            }}
          /> */}
          {/*<i className="iconfont icon-xiaoxi font-4" />*/}
        </div>
      );
    }
  }
}
