import React from 'react';
import { history, WMkit } from 'wmkit';
import { msg, Relax } from 'plume2';

@Relax
export default class MyAssets extends React.Component<any, any> {
  props: {
    relaxProps?: {
      pointsIsOpen: boolean;
    };
  };

  static relaxProps = {
    pointsIsOpen: 'pointsIsOpen'
  };

  render() {
    const isShop = WMkit.isShop();
    const { pointsIsOpen } = this.props.relaxProps;
    return (
      <div className="user-order my-assets">
        <div className="my-order-title">
          <label>我的资产</label>
        </div>
        <div className="order-status-center clearfix">
          {!isShop ? (
            <div
              className="item"
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      history.push('/my-coupon');
                    }
                  });
                } else {
                  history.push('/my-coupon');
                }
              }}
            >
              <div className="icon-box">
                <img src={require('./img/icon/11.png')} className="iconImg"/>
              </div>
              <p>优惠券</p>
            </div>
          ) : (
            ''
          )}

          <div
            className="item"
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    history.push('/balance/home');
                  }
                });
              } else {
                history.push('/balance/home');
              }
            }}
          >
            <div className="icon-box">
              <img src={require('./img/icon/12.png')} className="iconImg"/>
            </div>
            <p>余额</p>
          </div>

          {!isShop &&
            pointsIsOpen && (
              <div
                className="item"
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        history.push('/user-integral');
                      }
                    });
                  } else {
                    history.push('/user-integral');
                  }
                }}
              >
                <div className="icon-box">
                  <img src={require('./img/icon/13.png')} className="iconImg"/>
                </div>
                <p>积分</p>
              </div>
            )}
        </div>
      </div>
    );
  }
}
