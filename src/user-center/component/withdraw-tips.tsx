import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from "plume2";
import { _, history, noop } from "wmkit";
@Relax
export default class WithdrawTip extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeIsWithdrawTip:Function;
    };
  };

  static relaxProps = {
    changeIsWithdrawTip:noop,
  };
  render() {
    const { changeIsWithdrawTip } = this.props.relaxProps;
    const button = require('../img/button.png')
    const cancel = require('../img/cancel.png')
    const tip = require('../img/tip.png')
    return (
      <div className='drawwith-tip'>
        <div className='modal-box'
        onClick={(e)=>{
          if (e && e.stopPropagation)
          //因此它支持W3C的stopPropagation()方法
          e.stopPropagation();
        else
          //否则，我们需要使用IE的方式来取消事件冒泡
          window.event.cancelBubble = true;
          changeIsWithdrawTip();
        }}
        ></div>
        <div className='tip'>
          <div className='tip-button'
            onClick={()=>{
              history.push('/balance/deposit')
            }}
          >
            <img src={button}></img>
          </div>
          <div className='tip-cancel'
            onClick={(e)=>{
              if (e && e.stopPropagation)
              //因此它支持W3C的stopPropagation()方法
              e.stopPropagation();
            else
              //否则，我们需要使用IE的方式来取消事件冒泡
              window.event.cancelBubble = true;
              changeIsWithdrawTip();
            }}
          >
            <img src={cancel}></img>
          </div>
        </div>
      </div>
    )
  }
}
