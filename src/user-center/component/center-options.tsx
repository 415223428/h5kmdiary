import React from 'react';
import { Link } from 'react-router-dom';
import { Relax, msg } from 'plume2';
import { history, WMkit } from 'wmkit';

@Relax
export default class CenterOptions extends React.Component<any, any> {
  props: {
    relaxProps?: {
      onlineServices: any;
      serviceTitle: string;
    };
  };

  static relaxProps = {
    onlineServices: 'onlineServices',
    serviceTitle: 'serviceTitle'
  };

  render() {
    const { onlineServices, serviceTitle } = this.props.relaxProps;
    return (
      <div className="order-option">
        <div
          className="item"
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/user-info');
                }
              });
            } else {
              history.push('/user-info');
            }
          }}
        >
          <i className="iconfont icon-zhanghao icon-item" />
          <span>基本信息</span>
          <i className="iconfont icon-jiantou1 arrow" />
        </div>

        <div
          className="item"
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/user-finance');
                }
              });
            } else {
              history.push('/user-finance');
            }
          }}
        >
          <i className="iconfont icon-ddje icon-item" />
          <span>财务信息</span>
          <i className="iconfont icon-jiantou1 arrow" />
        </div>

        <div
          className="item"
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/receive-address');
                }
              });
            } else {
              history.push('/receive-address');
            }
          }}
        >
          <i className="iconfont icon-dz icon-item" />
          <span>收货信息</span>
          <i className="iconfont icon-jiantou1 arrow" />
        </div>

        <div
          className="item"
          onClick={() => {
            if (!WMkit.isLoginOrNotOpen()) {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/user-safe');
                }
              });
            } else {
              history.push('/user-safe');
            }
          }}
        >
          <i className="iconfont icon-suoding icon-item" />
          <span>账号安全</span>
          <i className="iconfont icon-jiantou1 arrow" />
        </div>
        {onlineServices.size > 0 &&
          !(window as any).isMiniProgram && (
            <div
              className="item"
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      history.push('/chose-service/0');
                    }
                  });
                } else {
                  history.push('/chose-service/0');
                }
              }}
            >
              <i className="iconfont icon-kefu2 icon-item" />
              <span>{serviceTitle}</span>
              <i className="iconfont icon-jiantou1 arrow" />
            </div>
          )}
      </div>
    );
  }
}
