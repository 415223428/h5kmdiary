import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit,history } from 'wmkit';
import AppStore from './store';
import CenterMember from './component/center-member';
import CenterOrder from './component/center-order';
// import MyAssets from './component/my-assets';
import CommonTools from './component/common-tools';
import SellwellGoods from './component/sellwell-goods';
import WithdrawTip from './component/withdraw-tips';
const style = require('./css/style.css');

@StoreProvider(AppStore)
export default class extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {}

  componentDidMount() {
    if (WMkit.isLoginOrNotOpen()) {
      this.store.init();
      this.store.goodsFollow();
      this.store.storeFollow();
      this.store.basicRules();
    } else {
      this.store.basicRules();
      this.store.initOnLineSerivce();
    }
  }

  render() {
    return (
      <div className="my-center" style={{height:'100vh'}}>
        {
          this.store.state().get('isWithdrawTip')?
          <WithdrawTip/>:
          null
        }
        {/*我的*/}
        <CenterMember />
        {this.checkIfLogin()}
        {/*我的订单*/}
        <CenterOrder />
        {/*我的资产*/}
        {/* <MyAssets /> */}
        {/*常用功能*/}
        <CommonTools />
        {WMkit.isLoginOrNotOpen() && <SellwellGoods/>}
        <div className="footer-copyright">
          {/* <p>© 2017-2018 南京万米信息技术有限公司</p> */}
          <p>粤ICP备19120716号-1</p>
        </div>
      </div>
    );
  }
  /**
   * 工具栏的渲染
   * @returns {any}
  */
  checkIfLogin = () => {
    // 当前用户是否是分销员
    if(WMkit.isLoginOrNotOpen() && WMkit.isDistributor()){
      return (
        <div className="toolbar">
          <div className="toolbar-item" style={{borderRight:'1px solid #ddd'}}
               onClick={()=>{
                history.push('/social-c/my-customer')
              }}>
            <span>我的用户</span>
            <i className="iconfont icon-jiantou"></i>
          </div>
          <div 
            className="toolbar-item" 
            style={{borderRight:'1px solid #ddd'}}
            onClick={()=>{
              history.push('/sales-perform')
            }}
          >
            <span>销售业绩</span>
            <i className="iconfont icon-jiantou"></i>
          </div>
          <div 
            className="toolbar-item"
            onClick={() => {
              history.push('/customer-order-list')
            }}
          >
            <span>销售订单</span>
            <i className="iconfont icon-jiantou"></i>
          </div>
        </div>
      );
    }else {
      return (
        <div className="toolbar">
          <div 
            className="toolbar-item" 
            style={{borderRight:'none'}}
            onClick={() => {
              history.push('/social-c/my-customer')
            }}
          >
            <span>我的用户</span>
            <i className="iconfont icon-jiantou"></i>
          </div>
        </div>
      );
    }
  }
}
