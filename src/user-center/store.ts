import { IOptions, msg, Store } from 'plume2';
import { fromJS } from 'immutable';
import { Alert, Confirm } from 'wmkit';
import { cache, config } from 'config';
import { evaluateWebapi } from 'biz';
import { init } from "./webapi";
import * as webapi from './webapi';
import CenterMemberActor from './actor/center-member-actor';

export default class AppStore extends Store {
  bindActor() {
    return [new CenterMemberActor()];
  }

  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  /**
   * 会员中心初始化数据
   */
  init = async () => {
    const res = (await Promise.all([
      webapi.fetchCustomerCenterInfo(),
      webapi.fetchOnlineServiceList()
    ])) as any;
    if (
      res[0].code == config.SUCCESS_CODE &&
      res[1].code == config.SUCCESS_CODE
    ) {
      // 1.过滤出需要的客服列表信息
      let onlineServices = res[1].context
        ? res[1].context.qqOnlineServerItemRopList
        : [];
      onlineServices = fromJS(
        onlineServices.map((item) => {
          return {
            customerServiceName: item.customerServiceName,
            customerServiceAccount: item.customerServiceAccount
          };
        })
      );
      // 2.设置用户信息、客服标题、客服列表信息
      this.transaction(() => {
        this.dispatch('member:init', {
          customer: res[0].context,
          serviceTitle:
            res[1].context && res[1].context.qqOnlineServerRop.serviceTitle,
          onlineServices: onlineServices
        });
      });


      // 这是获取余额
      const data = await init() as any;
      if (data.code == 'K-000000') {
        const amount = data.context;
        this.transaction(() => {
          this.dispatch('set:amount', fromJS(amount))
        })
      }

      // 获取分销信息
      const result = (await webapi.fetchDistributorInfo()) as any;
      if (result.code == 'K-000000'&&result.context.distributionCustomerVO!=null) {
        const distributor = result.context;
        // 设置缓存
        let distributionCustomerVO = (distributor as any).distributionCustomerVO;
        // sessionStorage.setItem('saleType',(distributionCustomerVO as any).distributorLevelName)
        this.transaction(() => {
          this.dispatch('set:distributor', fromJS(distributor.distributionCustomerVO))
        })
      }
      // 获取热销商品
      const list = (await
        webapi.fetchHotDistributeList({
          pageNum: 0,
          pageSize: 10,
          sortFlag: 0
        })
      ) as any;
      if (list.code == 'K-000000') {
        const hotGoodsList = list.context
          ? list.context.esGoodsInfoPage.content
          : [];
        this.transaction(() => {
          this.dispatch('set:hotGoodsList', fromJS(hotGoodsList))
        })
      }
    }

    // 获取店铺状态
    const store = (await webapi.fetchInvitorInfo()) as any;
    if (store.code == 'K-000000') {
      const storeStatus = store.context;
      this.transaction(() => {
        this.dispatch('set:distributeSetting', fromJS(storeStatus.distributionSettingSimVO))
      })
    }

    //积分与成长值
    this.growthValueIsOpen();
  };

  /**
   * 会员中心初始化数据
   */
  goodsFollow = async () => {
    const { context, code, message } = await webapi.fetchGoodsFollowNum();
    if (code !== config.SUCCESS_CODE) {
      Alert({ text: message });
      return;
    }
    this.dispatch('goodsFollow:init', context);
  };

  /**
   * 会员中心初始化数据
   */
  storeFollow = async () => {
    const { context, code, message } = await webapi.fetchStoreFollowNum();
    if (code !== config.SUCCESS_CODE) {
      Alert({ text: message });
      return;
    }
    this.dispatch('storeFollow:init', context);
  };

  /**
   * 刷新页面状态
   */
  flush = async () => {
    this.init();
    this.goodsFollow();
    this.storeFollow();
    this.basicRules();
  };

  /**
   * 开放访问qq客服初始化
   */
  initOnLineSerivce = async () => {
    const { code, context, message } = await webapi.fetchOnlineServiceList();
    if (code === config.SUCCESS_CODE) {
      let onlineServices = (context as any)
        ? (context as any).qqOnlineServerItemRopList
        : [];
      onlineServices = fromJS(
        onlineServices.map((item) => {
          return {
            customerServiceName: item.customerServiceName,
            customerServiceAccount: item.customerServiceAccount
          };
        })
      );
      this.dispatch('member:initOnlineServices', {
        serviceTitle:
          (context as any) && (context as any).qqOnlineServerRop.serviceTitle,
        onlineServices: onlineServices
      });
    } else {
      Alert({ text: message });
      return;
    }
  };

  growthValueIsOpen = async () => {
    let res: any = await webapi.growthValueIsOpen();
    if (res && res.code == config.SUCCESS_CODE && res.context.open) {
      this.dispatch('userInfo:growthValueIsOpen');
    }
  };

  basicRules = async () => {
    // 积分开关是否打开
    const {
      code: pCode,
      context: pContext
    } = (await webapi.basicRules()) as any;
    if (pCode === config.SUCCESS_CODE && pContext.status === 1) {
      this.dispatch('userInfo:pointsIsOpen');
    }
    // 评价开关是否打开
    const eFlag = (await evaluateWebapi.isShow()) as any;
    if (eFlag) {
      this.dispatch('userInfo:evaluateIsOpen');
    }
  };

  /**
   * 数据变更
   * @param {any} field
   * @param {any} value
   */
  onFieldChange = ({ field, value }) => {
    this.dispatch('center: field: change', { field, value });
  };

  /**
   * 分享赚选中的sku
   * @param sku
   */
  saveCheckedSku = (sku) => {
    this.onFieldChange({ field: 'checkedSku', value: fromJS(sku) });
  };

  /**
   * 切换分享赚弹框显示与否
   */
  changeShareVisible = () => {
    this.dispatch('center: share: visible');
  };

  /**
   * 店铺关闭或者分销员禁用时弹窗
   * @param reason
   */
  shopClosedTip = (reason) => {
    msg.emit('bStoreCloseVisible', {
      visible: true,
      reason: reason
    });
  };
  //改变分享页面的开关
  changefenxiang = () => {
    this.dispatch('goods-actor:isfenxiang');
  };
  cloasefenxiang = () => {
    this.dispatch('goods-actor:closefenxiang');
    this.dispatch('goods-actor:closeWechatShare');
  };

  iscloseImg = () => {
    this.dispatch('goods-actor:closeImg');
  };

  isopenImg = () => {
    this.dispatch('goods-actor:openImg');
  };

  //传入对应的分享页面Id
  sendId = (fenxiangId) => {
    this.dispatch('goods-actor:fenxiangId', fenxiangId);
  }

  //分享赚logo
  getLogo = async () => {
    const res = (await Promise.all(
      [webapi.fetchCustomerCenterInfo()]
    )) as any;
    if (res[0].code == config.SUCCESS_CODE) {
      this.dispatch('logo:init', (res[0].context as any).pcLogo
        ? JSON.parse((res[0].context as any).pcLogo)[0]
        : { url: '' });
    }
  }


  openWechatShare = () => {
    this.dispatch('goods-actor:openWechatShare');
  }

  changeIsWithdrawTip=()=>{
    this.dispatch('actor:withdrawtip')
  }
}
