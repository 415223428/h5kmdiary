import React, { Component } from 'react';
import { Alert, history } from 'wmkit';

import SuccessContent from './component/confirm-success';

export default class ConfirmSuccess extends Component<any, any> {
  componentWillMount() {
    // 解析url中的参数
    if (!this.props.location || !this.props.location.state) {
      Alert({
        text: '积分订单不存在'
      });
      history.push('/points-order-list');
      return;
    }
  }

  render() {
    const { result } = this.props.location.state;
    return <SuccessContent result={result} />;
  }
}
