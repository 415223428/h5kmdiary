import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { WMImage, Alert, _, history } from 'wmkit';

export default class SuccessContent extends Component<any, any> {
  render() {
    const { result } = this.props;

    return (
      <div className="list-none list-height">
        <WMImage
          src={require('./img/result-suc.png')}
          width="200px"
          height="200px"
        />
        <div>
          <p>订单提交成功！</p>
        </div>
        <div>
          <div className="bot-box">
            <div style={{ paddingBottom: 5 }}>
              <div className="line" />
              <div className="item">
                订单编号
                <span>{result.tid}</span>
              </div>
              <div className="item">
                订单金额
                <span>
                  {result.points}积分
                </span>
              </div>
            </div>
          </div>
          <div className="bot-line" style={{ marginBottom: '1.6rem' }} />
        </div>
        <div className="bottom-btn">
          <div className="half">
            <button
              className="btn btn-ghost"
              onClick={() => history.push('/points-order-list')}
            >
              查看订单
            </button>
          </div>
          <div className="half">
            <button className="btn btn-ghost" onClick={this._backToMain}>
              返回首页
            </button>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 返回首页
   * @private
   */
  _backToMain() {
      history.push('/points-mall');
  }
}
