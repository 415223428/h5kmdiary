import React, { Component } from 'react';

import { Relax, IMap } from 'plume2';
import { history, WMkit, noop } from 'wmkit';

import { IList } from 'typings/globalType';

const styles = require('../css/style.css');

const noneImg = require('../img/imgnone.png');

@Relax
export default class SkuList extends Component<any, any> {
  props: {
    store: IMap;
    index: number;
    relaxProps?: {
      orderConfirm: IList;
      payOptions: IList;
      shopName: string;
      inviteeName: string;
      saveSessionStorage: Function;
    };
  };

  static relaxProps = {
    orderConfirm: 'orderConfirm',
    payOptions: 'payOptions',
    shopName: 'shopName',
    inviteeName: 'inviteeName',
    saveSessionStorage: noop
  };

  render() {
    const {
      orderConfirm,
      payOptions,
      shopName,
      inviteeName
    } = this.props.relaxProps;
    const { store, index } = this.props;
    const { supplier, tradeItems, gifts } = store as any;
    const payType = orderConfirm.getIn([index, 'payType']);
    const payIndex = payOptions.findIndex((f) => f.get('id') == payType);
    const allSkus = tradeItems.concat(gifts);
    const cartList = allSkus.filter((sku, index) => index < 4).map((sku) => {
      return {
        url: sku.pic
      };
    });

    return (
      <div className="sku-list-head">
        {WMkit.isShop() ? (
          <div className="sku-head sku-list-head">
            <span>订单： </span>
            <span>
              {inviteeName}的{shopName}
            </span>
          </div>
        ) : (
            <div className="sku-head sku-list-head">
              <span>订单： </span> 
              {supplier.isSelf == true && <div className="self-sales">自营</div>}
              <span> {supplier.storeName}</span>
            </div>
          )}
        <div className="limit-img" onClick={() => this._goToNext()}>
          <div className="img-content">
            {cartList.map((v, i) => (
              <div className='cardList-goods'>
                <img key={i} src={v.url ? v.url : noneImg} className="img-item" />
                {/* <div style={{ color: "rgba(102,102,102,1" }}>{detail.get("tradeItems").get(i).get("skuName")}</div> */}
              </div>
            ))}
          </div>
          <div className="right-context">
            <div className="total-num">
              <div>{allSkus.length}</div>
            </div>
            <i className="iconfont icon-jiantou1" />
          </div>
        </div>
      </div>
    );
  }

  /**
   * 查看商品清单
   * @private
   */
  _goToNext = () => {
    const { supplier } = this.props.store as any;
    this.props.relaxProps.saveSessionStorage('sku-list');
    history.push(`/order-sku-list/${supplier.storeId}`);
  };

  /**
   * 支付配送方式
   */
  _goToWays = () => {
    const { supplier } = this.props.store as any;
    this.props.relaxProps.saveSessionStorage('payment');
    history.push(`/payment-delivery/${supplier.storeId}`);
  };
}
