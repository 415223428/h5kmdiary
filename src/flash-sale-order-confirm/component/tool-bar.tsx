import React, { Component } from "react";

import { Relax, IMap } from "plume2";
import { FormRegexUtil, noop, _ } from 'wmkit'

@Relax
export default class ToolBar extends Component<any, any> {

  props: {
    relaxProps?: {
      buyerRemark: string
      sperator: boolean
      invoice: IMap
      defaultInvoiceAddr: IMap
      totalPrice: number

      submit: Function
    }
  }

  static relaxProps = {
    buyerRemark: 'buyerRemark',
    sperator: 'sperator',
    invoice: 'invoice',
    defaultInvoiceAddr: 'defaultInvoiceAddr',
    totalPrice: 'totalPrice',

    submit: noop
  }


  render() {
    const { totalPrice } = this.props.relaxProps
    return (
      <div className="bottom-fixed">
        <div className="order-bottom">
          <p>订单金额：<span><i className="iconfont icon-qian" />{_.addZero(totalPrice)}</span></p>
          <button onClick={() => this._submit()}>提交订单</button>
        </div>
      </div>
    )
  }


  /**
   * 提交订单
   * @private
   */
  _submit = () => {
    const { submit } = this.props.relaxProps

    submit(false)
  }
}
