import React, { Component } from 'react';

import { _, WMkit } from 'wmkit';
import { fromJS } from 'immutable';
import { Switch } from 'antd-mobile';
import noop from 'wmkit/noop';
import {
  OrderMaxPointDiscountQL,
  OrderMaxPointQL,
  OrderUsePointDiscountQL
} from '../ql';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';

const TYPES = {
  '0': '满减优惠',
  '1': '满折优惠',
  '2': '满赠优惠'
};
@Relax
export default class AmountStatistics extends Component<any, any> {
  props: {
    index: number;
    store: any;
    single: boolean;
    couponTotal: number;
    totalCommission: number;
    storeBagsFlag: boolean;
    useCoupons: Function;
    relaxProps?: {
      totalPoint: number;
      pointConfig: any;
      setUsePoint: Function;
      changeSwitch: Function;
      pointDiscount: number;
      maxPoint: number;
      maxPointDiscount: number;
      showPoint: boolean;
      usePoint: number;
      integralInput: string;
      payOptions: IList;
      payType: number;
    };
  };
  static relaxProps = {
    totalPoint: 'totalPoint',
    pointConfig: 'pointConfig',
    showPoint: 'showPoint',
    usePoint: 'usePoint',
    integralInput: 'integralInput',
    payOptions: 'payOptions',
    payType: 'payType',
    setUsePoint: noop,
    changeSwitch: noop,
    maxPoint: OrderMaxPointQL,
    // 积分可抵扣的最大金额
    maxPointDiscount: OrderMaxPointDiscountQL,
    //积分抵扣金额
    pointDiscount: OrderUsePointDiscountQL
  };
  render() {
    const {
      single,
      couponTotal,
      useCoupons,
      totalCommission,
      storeBagsFlag
    } = this.props;
    const {
      pointConfig,
      totalPoint,
      maxPoint,
      maxPointDiscount,
      setUsePoint,
      pointDiscount,
      changeSwitch,
      showPoint,
      integralInput,
      payType,
      payOptions,
    } = this.props.relaxProps;
    const { discountsPrice, tradePrice } = this.props.store;
    const prices = (fromJS(discountsPrice) || fromJS([])).groupBy((item) =>
      item.get('type')
    );
    const opening = totalPoint
      ? _.sub(pointConfig.get('overPointsAvailable'), totalPoint) > 0
      : false;
    const openStatus = pointConfig && pointConfig.get('status');
    const payIndex = payOptions.findIndex((f) => f.get('id') == payType);

    return (
      <div className="total-price address-box">
        <div className="pay-delivery">
          <span>支付配送</span>
          <div className="ways-item">
            <div>
              <p>{payType >= 0 && payOptions.getIn([payIndex, 'name'])}</p>
              <p>快递配送</p>
            </div>
          </div>
        </div>
        {single && (
          <div className="total-list">
            <span>订单金额</span>
            <span className="price-color">
              <i className="iconfont icon-qian" />
              {_.addZero(tradePrice.totalPrice - couponTotal - pointDiscount)}
            </span>
          </div>
        )}
        <div className="total-list">
          <span>商品金额</span>
          <span>
            <i className="iconfont icon-qian" />
            {_.addZero(tradePrice.goodsPrice)}
          </span>
        </div>
        {prices
          .map((val, key) => {
            const price = val
              .map((v) => v.get('amount'))
              .reduce((a, b) => (a += b));
            return (
              price > 0 && (
                <div className="total-list" key={key}>
                  <span>{TYPES[key]}</span>
                  <span>
                    -<i className="iconfont icon-qian" />
                    {_.addZero(price)}
                  </span>
                </div>
              )
            );
          })
          .toArray()}
        <div className="total-list">
          <span>配送费用</span>
          <span>
            <i className="iconfont icon-qian" />
            {_.addZero(tradePrice.deliveryPrice)}
          </span>
        </div>
        {single &&
          WMkit.isDistributorLogin() &&
          totalCommission > 0 && (
            <div className="total-list total-rebate b-1px-t">
              <span>预计返利</span>
              <span>
                <i className="iconfont icon-qian" />
                {totalCommission && totalCommission.toFixed(2)}
              </span>
            </div>
          )}
      </div>
    );
  }
}
