import React, { Component } from 'react';

import { Relax } from 'plume2';

import { IList } from 'typings/globalType';

import StoreItem from './store-item';
import { noop } from 'wmkit';

@Relax
export default class StoreList extends Component<any, any> {
  props: {
    relaxProps?: {
      stores: IList;
      couponTotal: number;
      totalCommission: number;
      storeBagsFlag: boolean;
      useCoupons: Function;
    };
  };

  static relaxProps = {
    stores: 'stores',
    couponTotal: 'couponTotal',
    totalCommission: 'totalCommission',
    storeBagsFlag: 'storeBagsFlag',
    useCoupons: noop
  };

  render() {
    const {
      stores,
      couponTotal,
      useCoupons,
      totalCommission,
      storeBagsFlag
    } = this.props.relaxProps;
    return (
      <div>
        {stores
          .toJS()
          .map((store, index) => (
            <StoreItem
              key={index}
              store={store}
              index={index}
              single={stores.size == 1}
              useCoupons={useCoupons}
              couponTotal={couponTotal}
              totalCommission={totalCommission}
              storeBagsFlag={storeBagsFlag}
            />
          ))}
      </div>
    );
  }
}
