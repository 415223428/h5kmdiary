import React, { Component } from 'react';
import { Relax } from 'plume2';
import { _, noop, WMkit, FormSelect } from 'wmkit';
import { Switch } from 'antd-mobile';
import {
  OrderMaxPointQL,
  OrderMaxPointDiscountQL,
  OrderUsePointDiscountQL
} from '../ql';

@Relax
export default class AllAmount extends Component<any, any> {
  props: {
    relaxProps?: {
      totalPrice: number;
      goodsTotalPrice: number;
      couponTotal: number;
      discountsTotalPrice: number;
      totalDeliveryPrice: number;
      totalCommission: number;
      storeBagsFlag: boolean;
      totalPoint: number;
      pointConfig: any;
      useCoupons: Function;
      setUsePoint: Function;
      changeSwitch: Function;
      pointDiscount: number;
      maxPoint: number;
      maxPointDiscount: number;
      showPoint: boolean;
      usePoint: number;
      integralInput: string;
    };
  };

  static relaxProps = {
    totalPrice: 'totalPrice',
    goodsTotalPrice: 'goodsTotalPrice',
    couponTotal: 'couponTotal',
    discountsTotalPrice: 'discountsTotalPrice',
    totalDeliveryPrice: 'totalDeliveryPrice',
    totalCommission: 'totalCommission',
    storeBagsFlag: 'storeBagsFlag',
    totalPoint: 'totalPoint',
    pointConfig: 'pointConfig',
    showPoint: 'showPoint',
    usePoint: 'usePoint',
    integralInput: 'integralInput',
    useCoupons: noop,
    setUsePoint: noop,
    changeSwitch: noop,
    maxPoint: OrderMaxPointQL,
    // 积分可抵扣的最大金额
    maxPointDiscount: OrderMaxPointDiscountQL,
    //积分抵扣金额
    pointDiscount: OrderUsePointDiscountQL
  };

  render() {
    const {
      totalPrice,
      goodsTotalPrice,
      couponTotal,
      discountsTotalPrice,
      totalDeliveryPrice,
      totalCommission,
      useCoupons,
      storeBagsFlag,
      totalPoint,
      pointConfig,
      maxPoint,
      maxPointDiscount,
      setUsePoint,
      pointDiscount,
      changeSwitch,
      showPoint,
      integralInput
    } = this.props.relaxProps;
    const openStatus = pointConfig && pointConfig.get('status');
    const opening =
      (totalPoint
        ? _.sub(pointConfig.get('overPointsAvailable'), totalPoint) > 0
        : false) || maxPoint == 0;
    return (
      <div className="total-price mb10">
        {!WMkit.isShop() &&
          !storeBagsFlag && (
            <div className="total-list" onClick={() => useCoupons()}>
              <span style={{ flex: 1 }}>使用优惠券</span>
              <span>
                <i className="iconfont icon-qian" />
                {_.addZero(couponTotal)}
              </span>
              <i className="iconfont icon-jiantou1" />
            </div>
          )}
        {openStatus == '1' && (
          <div className="integralBox">
            <div className="total-list  integralItem">
              <div className="form-text">积分</div>
              <Switch
                checked={opening ? false : showPoint}
                disabled={opening || maxPoint == 0}
                onChange={(e) => {
                  changeSwitch(e);
                }}
              />
            </div>
            {!opening && showPoint ? (
              <span>
                使用
                <input
                  type="text"
                  className="integral-input"
                  value={integralInput}
                  onChange={(e) => setUsePoint(e.target.value, maxPoint)}
                />
                积分&nbsp;抵扣<span className="new-theme-text">
                  <i className="iconfont icon-qian" />
                {pointDiscount}
                </span>
              </span>
            ) : (
              <p>
                共<span className="new-theme-text">{totalPoint}</span>积分&nbsp;
                {opening ? (
                  '达到' +
                  pointConfig.get('overPointsAvailable') +
                  '积分后可用于下单抵扣'
                ) : (
                  <span>
                    最多可用{maxPoint}积分抵扣<span className="new-theme-text">
                      <i className="iconfont icon-qian" />
                    {maxPointDiscount}
                    </span>
                  </span>
                )}
              </p>
            )}
            {showPoint && (
              <p>
                共<span className="new-theme-text">{totalPoint}</span>积分&nbsp;最多可用{
                maxPoint
              }积分抵扣<span className="new-theme-text">
                  <i className="iconfont icon-qian" />
                {maxPointDiscount}
                </span>
              </p>
            )}
            <div className="gray-bg" />
          </div>
        )}
        <div className="total-list">
          <span>订单总额</span>
          <span className="price-color">
            <i className="iconfont icon-qian" />
            {_.addZero(totalPrice)}
          </span>
        </div>
        <div className="total-list">
          <span>商品总额</span>
          <span>
            <i className="iconfont icon-qian" />
            {_.addZero(goodsTotalPrice)}
          </span>
        </div>
        {!WMkit.isShop() && (
          <div className="total-list">
            <span>优惠总额</span>
            <span>
              -<i className="iconfont icon-qian" />
              {_.addZero(discountsTotalPrice)}
            </span>
          </div>
        )}
        {pointConfig &&
        pointConfig.get('status') == '1' && (
          <div className="total-list">
            <span>积分抵扣</span>
            <span>
                -<i className="iconfont icon-qian" />
              {_.addZero(pointDiscount)}
              </span>
          </div>
        )}
        <div className="total-list">
          <span>配送费用</span>
          <span>
            <i className="iconfont icon-qian" />
            {_.addZero(totalDeliveryPrice)}
          </span>
        </div>
        {WMkit.isDistributorLogin() &&
          totalCommission > 0 && (
            <div className="total-list total-rebate b-1px-t">
              <span>预计返利</span>
              <span>
                <i className="iconfont icon-qian" />
                {totalCommission && totalCommission.toFixed(2)}
              </span>
            </div>
          )}
      </div>
    );
  }
}
