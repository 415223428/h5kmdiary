import React, { Component } from 'react';
import { Relax } from 'plume2';

import { FormText, noop } from 'wmkit';
import { IList } from 'typings/globalType';

@Relax
export default class Remark extends Component<any, any> {
  props: {
    storeId: number;
    relaxProps?: {
      orderConfirm: IList; // 订单提交项目

      saveBuyerRemark: Function; //保存买家备注
    };
  };

  static relaxProps = {
    orderConfirm: 'orderConfirm',

    saveBuyerRemark: noop
  };

  render() {
    const { orderConfirm, saveBuyerRemark } = this.props.relaxProps;
    const storeItem = orderConfirm.find(
      (o) => o.get('storeId') == this.props.storeId
    );
    const buyerRemark = storeItem ? storeItem.get('buyerRemark') : '';
    return (
      <div>
        <FormText
          label={'订单备注'}
          placeholder="点此输入订单备注（100字以内）"
          value={buyerRemark}
          onChange={(e) =>
            saveBuyerRemark({
              remark: e.target.value,
              storeId: this.props.storeId
            })
          }
          maxLength={100}
        />
      </div>
    );
  }
}
