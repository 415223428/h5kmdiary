import React, { Component } from 'react';

import { Relax, IMap } from 'plume2';

import SkuList from './sku-list';
import Invoice from './invoice';
import Enclosure from './enclosure';
import AmountStatistics from './amount-statistics';
import Remark from './remark';

export default class StoreItem extends Component<any, any> {
  props: {
    store: IMap;
    index: number;
    single: boolean;
    couponTotal: number;
    totalCommission: number;
    storeBagsFlag: boolean;
    useCoupons: Function;
  };

  render() {
    const {
      store,
      index,
      single,
      useCoupons,
      couponTotal,
      totalCommission,
      storeBagsFlag
    } = this.props as any;
    return (
      <div >
        <div className="address-box">
          <SkuList store={store} index={index} />
          <Invoice
            storeId={store.supplier.storeId}
            supplierId={store.supplier.supplierId}
          />
          <Remark storeId={store.supplier.storeId} />
          {/*<Enclosure storeId={store.supplier.storeId} />*/}
        </div>
        <AmountStatistics
          store={store}
          index={index}
          single={single}
          useCoupons={useCoupons}
          couponTotal={couponTotal}
          totalCommission={totalCommission}
          storeBagsFlag={storeBagsFlag}
        />
      </div>
    );
  }
}
