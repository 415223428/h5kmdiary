import React from 'react'

import {StoreProvider} from 'plume2';
import {Button} from 'wmkit';

import FormItem from './component/form-item';
import AppStore from './store';
import './css/style.css'


const SubmitButton = Button.LongBlue;
const submit=require('./images/submit.png')
const edit=require('./images/edit.png')

@StoreProvider(AppStore, {debug: __DEV__})
export default class UserInvoice extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }


  render() {
    const invoiceBean = this.store.state().get("invoice");
    const editStatus = this.store.state().get("editStatus");

    let disabledFlag = false;
    let toEditFlag = false;
    if (invoiceBean.size == 0){
      disabledFlag = true;
    }else{
      if (invoiceBean.get("checkState") == 0){
        disabledFlag = true;
      }else if ((invoiceBean.get("checkState") == 1 || invoiceBean.get("checkState") == 2) && !editStatus){
        toEditFlag = true;
      }
    }
    return (
      <div className="content register-content">
        <FormItem/>
        <div className="register-btn user-invloice-btn" style={{padding:'0'}}>
          {toEditFlag||disabledFlag ?
            <img src={edit} alt="" onClick={() => {this.store.toEditStatus()}} style={{width:'100%'}}/>
            // <SubmitButton text="编辑" disabled={disabledFlag} onClick={() => {this.store.toEditStatus()}}>编辑</SubmitButton>
            :
            // <SubmitButton text="提交" disabled={disabledFlag} onClick={() => {this.store.submitInvoice()}}>提交</SubmitButton>
            <img src={submit} alt="" onClick={() => {this.store.submitInvoice()}} style={{width:'100%'}}/>
          }
        </div>
      </div>
    )
  }
}
