import React from 'react';
import { Link } from 'react-router-dom';

import { IMap, Relax } from 'plume2';
import { UploadImage, FormInput, Tips, noop, WMImage } from 'wmkit';

const businessExample = require('../images/id5.jpg');
const taxerExample = require('../images/id8.jpg');
const sign=require('../images/sign.png')
const dui=require('../images/dui.png')
const cuo=require('../images/x.png')

require('../css/style.css')
/**
 * 审核的tip渲染帮助object
 * key值为checkState值，为3时是没有记录的情况
 *
 * @type {{0: {iconName: string; text: string}; 1: {iconName: string; text: string}; 2: {iconName: string; text: string}; 3: {iconName: string; text: string}}}
 */

const CheckStateObject = {
  0: {
    imgName: sign,
    text: '资质审核中，您无法编辑审核中增票资质'
  },
  1: { imgName: dui, text: '您的增票资质已通过审核，可正常使用' },
  2: { imgName: cuo, text: '您的增票资质未通过审核，请修改后重新提交' },
  3: {
    imgName: sign,
    text: '通过增票资质审核后您将可以开具增值税专用发票'
  }
};

let ZoomImage = null;

@Relax
export default class FormItem extends React.Component<any, any> {
  props: {
    relaxProps?: {
      invoiceBean: IMap;
      setValue: Function;
      addImg: Function;
      editStatus: boolean;
    };
  };

  static relaxProps = {
    invoiceBean: 'invoice',
    setValue: noop,
    addImg: noop,
    editStatus: 'editStatus'
  };

  constructor(props: any) {
    super(props);
    this.state = {};
    System.import('../../../web_modules/wmkit/image-util/zoom-image').then(
      (zoomImage) => {
        ZoomImage = zoomImage;
      }
    );
  }

  render() {
    let { invoiceBean, editStatus } = this.props.relaxProps;
    let businessImage: string = invoiceBean.get('businessLicenseImg');
    let indentiImage: string = invoiceBean.get('taxpayerIdentificationImg');
    let rejectReason: string = invoiceBean.get('rejectReason');
    let invalidFlag: number = invoiceBean.get('invalidFlag');
    rejectReason = invalidFlag == 1 ? '作废' : rejectReason;

    let checkState = invoiceBean.get('checkState');

    let cannotEdit = false;
    if (!checkState && checkState !== 0) {
      checkState = 3;
    } else {
      if (
        checkState == 0 ||
        ((checkState == 1 || checkState == 2) && !editStatus)
      ) {
        //待审核 || 审核之后的未编辑状态
        cannotEdit = true;
      }
    }

    let businessImageFlag = true,
      indentiImageFlag = true;
    if (!businessImage) {
      businessImage = businessExample;
      businessImageFlag = false;
    } else {
      businessImage = eval('(' + businessImage + ')')[0]['url'];
    }
    if (!indentiImage) {
      indentiImage = taxerExample;
      indentiImageFlag = false;
    } else {
      indentiImage = eval('(' + indentiImage + ')')[0]['url'];
    }

    return (
      <div className="register-box user-invloice-box">
        {/* <Tips
          iconName={CheckStateObject[checkState]['iconName']}
          text={
            CheckStateObject[checkState]['text'] +
            (checkState == 2 ? `，原因是：${rejectReason}` : '')
          }
        /> */}
        <div className='head-tip'>
          <img src={(CheckStateObject[checkState]['imgName'])} alt=""/>
          <span>{
            CheckStateObject[checkState]['text'] +
            (checkState == 2 ? `，原因是：${rejectReason}` : '')
          }</span>
        </div>
        <FormInput
          label="单位全称"
          placeHolder="请输入正确的单位全称"
          maxLength={50}
          defaultValue={invoiceBean.get('companyName')}
          onChange={(e) => this._setValue('companyName', e.target.value)}
          disabled={cannotEdit}
        />
        <FormInput
          label="纳税人识别号"
          pattern="[0-9a-zA-Z]*"
          placeHolder="请输入正确的纳税人识别号"
          maxLength={20}
          defaultValue={invoiceBean.get('taxpayerNumber')}
          onChange={(e) => this._setValue('taxpayerNumber', e.target.value)}
          disabled={cannotEdit}
        />
        <FormInput
          label="注册电话"
          pattern="[0-9-]*"
          placeHolder="请输入正确的注册电话"
          maxLength={20}
          defaultValue={invoiceBean.get('companyPhone')}
          onChange={(e) => this._setValue('companyPhone', e.target.value)}
          disabled={cannotEdit}
        />
        <FormInput
          label="注册地址"
          placeHolder="请输入正确的注册地址"
          maxLength={60}
          defaultValue={invoiceBean.get('companyAddress')}
          onChange={(e) => this._setValue('companyAddress', e.target.value)}
          disabled={cannotEdit}
        />
        {cannotEdit ? (
          <FormInput
            label="银行基本户号"
            type="text"
            defaultValue={this._bankNoForView(invoiceBean.get('bankNo'))}
            disabled={cannotEdit}
          />
        ) : (
          <FormInput
            label="银行基本户号"
            type="number"
            pattern="[0-9]*"
            placeHolder="请输入正确的银行基本户号"
            maxLength={30}
            defaultValue={invoiceBean.get('bankNo')}
            onChange={(e) => this._setValue('bankNo', e.target.value)}
            disabled={cannotEdit}
          />
        )}
        <FormInput
          label="开户行"
          placeHolder="请输入正确的开户行"
          maxLength={50}
          defaultValue={invoiceBean.get('bankName')}
          onChange={(e) => this._setValue('bankName', e.target.value)}
          disabled={cannotEdit}
        />
        <div className="invoice-user">
          <h4>营业执照复印件</h4>
          <div className="upload-box">
            <div className="example-box">
              <div className="delete-img" style={{ border: 'none' }}>
                <WMImage
                  src={businessImage}
                  width="53px"
                  height="53px"
                  ifZoom={true}
                />
                {businessImageFlag ? (
                  cannotEdit ? (
                    ''
                  ) : (
                    <a
                      href="javascript:void(0)"
                      onClick={() => this._removeImage('businessLicenseImg')}
                    >
                      ×
                    </a>
                  )
                ) : (
                  <div
                    className="shadow"
                    onClick={() => this._zoomImage(businessImage)}
                  >
                    示例
                  </div>
                )}
              </div>
            </div>
            {cannotEdit || businessImageFlag ? (
              ''
            ) : (
              <UploadImage
                onSuccess={this._uploadBusinessImage}
                onLoadChecked={() => {
                  return true;
                }}
                repeat
                className="upload"
                size={5}
              >
                <div className="upload-item">
                  <div className="upload-content">
                    <i className="iconfont icon-add11" />
                  </div>
                </div>
              </UploadImage>
            )}
          </div>
          <p className="grey-tips" style={{background:'#fff',color:'#999'}}>
            仅支持jpg、jpeg、png、gif文件，仅限上传1张，大小不超过5M
          </p>
        </div>
        <div className="invoice-user">
          <h4>一般纳税人认证资格复印件</h4>
          <div className="upload-box">
            <div className="example-box">
              <div className="delete-img" style={{ border: 'none' }}>
                <WMImage
                  src={indentiImage}
                  width="53px"
                  height="53px"
                  ifZoom={true}
                />
                {indentiImageFlag ? (
                  cannotEdit ? (
                    ''
                  ) : (
                    <a
                      href="javascript:void(0)"
                      onClick={() =>
                        this._removeImage('taxpayerIdentificationImg')
                      }
                    >
                      ×
                    </a>
                  )
                ) : (
                  <div
                    className="shadow"
                    onClick={() => this._zoomImage(indentiImage)}
                  >
                    示例
                  </div>
                )}
              </div>
            </div>
            {cannotEdit || indentiImageFlag ? (
              ''
            ) : (
              <UploadImage
                onSuccess={this._uploadIndentiImage}
                onLoadChecked={() => {
                  return true;
                }}
                repeat
                className="upload"
                size={5}
              >
                <div className="upload-item">
                  <div className="upload-content">
                    <i className="iconfont icon-add11" />
                  </div>
                </div>
              </UploadImage>
            )}
          </div>
          <p className="grey-tips" style={{background:'#fff',color:'#999'}}>
            仅支持jpg、jpeg、png、gif文件，仅限上传1张，大小不超过5M
          </p>
        </div>
      </div>
    );
  }

  /**
   * 银行卡展示时控制格式
   * @param bankNo
   * @returns {any}
   * @private
   */
  _bankNoForView = (bankNo) => {
    if (!bankNo) return bankNo;
    let res = '';
    while (true) {
      if (bankNo.length < 4) {
        res += bankNo;
        break;
      }
      res += bankNo.substring(0, 4) + ' ';
      bankNo = bankNo.substring(4, bankNo.length);
    }
    return res;
  };

  /**
   * 图片上传方法 -- 营业执照
   * @param loadResult
   * @param imageBase64
   * @private
   */
  _uploadBusinessImage = (loadResult) => {
    if (loadResult.code == 'K-000000') {
      let { addImg } = this.props.relaxProps;
      addImg(loadResult.context, 'businessLicenseImg');
    }
  };

  /**
   * 图片上传方法 -- 纳税人
   * @param loadResult
   * @param imageBase64
   * @private
   */
  _uploadIndentiImage = (loadResult) => {
    if (loadResult.code == 'K-000000') {
      let { addImg } = this.props.relaxProps;
      addImg(loadResult.context, 'taxpayerIdentificationImg');
    }
  };

  /**
   * 表单值统一存入方法
   * @param key
   * @param value
   * @private
   */
  _setValue = (key, value) => {
    this.props.relaxProps.setValue(key, value);
  };

  /**
   * 删除用户已经上传的图片
   * @param imageType
   * @private
   */
  _removeImage = (imageType) => {
    this.props.relaxProps.setValue(imageType, null);
  };

  _zoomImage = (src: string) => {
    ZoomImage.renderZoomImage({ src });
  };
}
