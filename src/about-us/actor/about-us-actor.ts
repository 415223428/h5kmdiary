import { Action, Actor, IMap } from 'plume2';

export default class AboutUsActor extends Actor {
  defaultState() {
    return {
      context: ''
    };
  }

  /**
   * 关于我们
   *
   * @param {IMap} state
   * @param {string} context
   * @returns
   * @memberof AboutUsActor
   */
  @Action('about: us: context')
  init(state: IMap, context: string) {
    return state.set('context', context);
  }
}
