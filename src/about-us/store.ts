import { Store } from 'plume2';
import * as webapi from './webapi';
import { config } from 'config';
import AboutUsActor from './actor/about-us-actor';
export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new AboutUsActor()];
  }

  init = async () => {
    const res = await webapi.fetchAboutUsContext();
    const { code, context } = res;
    if (code == config.SUCCESS_CODE) {
      this.dispatch('about: us: context', context);
    }
  };
}
