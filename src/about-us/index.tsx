import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const style = require('./css/style.css');

@StoreProvider(AppStore)
export default class AboutUs extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '关于我们';
    this.store.init();
  }

  render() {
    return (
      <div>
        <div
          className="about-box"
          dangerouslySetInnerHTML={{
            __html: this.store.state().get('context')
          }}
        />
      </div>
    );
  }
}
