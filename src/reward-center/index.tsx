import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit } from 'wmkit';

import AppStore from './store';
import RewardTop from './component/reward-top';
import SellwellGoods from './component/sellwell-goods';
import InviteFriend from './component/invit-friend';
import MyCustomer from './component/my-customer';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class DistributionCenter extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    if (WMkit.isLoginOrNotOpen()) {
      this.store.init();
    }
  }

  render() {
    return (
      <div className="distribution-container  reward-center">
        <RewardTop />
        <MyCustomer />
        <InviteFriend />
        <SellwellGoods />
      </div>
    );
  }
}
