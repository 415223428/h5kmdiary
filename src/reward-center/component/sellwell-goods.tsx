import React, { Component } from 'react';
import { msg, Relax } from 'plume2';
import { IList } from 'typings/globalType';
import { _, history, Blank, WMkit, Alert, noop } from 'wmkit';
import { GoodsNum } from 'biz';
const noneImg = require('../img/list-none.png');

@Relax
export default class SellwellGoods extends Component<any, any> {
  props: {
    relaxProps?: {
      hotGoodsList: IList;
      saveCheckedSku: Function;
      changeShareVisible: Function;
    };
  };

  static relaxProps = {
    hotGoodsList: 'hotGoodsList',
    saveCheckedSku: noop,
    changeShareVisible: noop
  };

  render() {
    const { hotGoodsList } = this.props.relaxProps;

    return (
      <div className="sellwell-goods">
        <div className="sellwell-goods-title">热销商品</div>
        <div className="sellwell-goods-list">
          {hotGoodsList && hotGoodsList.size > 0 ? (
            hotGoodsList.map((goods) => {
              // FIXME 销量、评价、好评展示与否，后期读取后台配置开关
              // 此版本默认都展示
              const isShow = true;
              // sku信息
              const goodsInfo = goods.get('goodsInfo');
              // 库存
              const stock = goodsInfo.get('stock');
              // 商品是否要设置成无效状态
              // 起订量
              const count = goodsInfo.get('count') || 0;
              // 库存等于0或者起订量大于剩余库存
              const invalid = stock <= 0 || (count > 0 && count > stock);
              const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;

              //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
              //好评率
              let favorableRate = '100';
              if (
                goodsInfo.get('goodsEvaluateNum') &&
                goodsInfo.get('goodsEvaluateNum') != 0
              ) {
                favorableRate = _.mul(
                  _.div(
                    goodsInfo.get('goodsFavorableCommentNum'),
                    goodsInfo.get('goodsEvaluateNum')
                  ),
                  100
                ).toFixed(0);
              }

              //评论数
              let evaluateNum = '暂无';
              const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
              if (goodsEvaluateNum) {
                if (goodsEvaluateNum < 10000) {
                  evaluateNum = goodsEvaluateNum;
                } else {
                  const i = _.div(goodsEvaluateNum, 10000).toFixed(1);
                  evaluateNum = i + '万+';
                }
              }

              //销量
              let salesNum = '暂无';
              const goodsSalesNum = goodsInfo.get('goodsSalesNum');
              if (goodsSalesNum) {
                if (goodsSalesNum < 10000) {
                  salesNum = goodsSalesNum;
                } else {
                  const i = _.div(goodsSalesNum, 10000).toFixed(1);
                  salesNum = i + '万+';
                }
              }
              // 预估点数
              let pointNum = goodsInfo.get('kmPointsValue');
              //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆

              let containerClass = invalid ? 'invalid-goods' : '';

              return (
                <div
                  className={`sellwell-goods-list-item ${containerClass}`}
                  key={goods.get('id')}
                  onClick={() =>
                    history.push(
                      '/goods-detail/' + goodsInfo.get('goodsInfoId')
                    )
                  }
                >
                  <div className="sellwell-img-box">
                    <img
                      src={goodsInfo.get('goodsInfoImg') || noneImg}
                      width="100%"
                      height="100%"
                      alt=""
                    />
                  </div>
                  <div className="sellwell-goods-item-right">
                    <p className="sellwell-name">
                      {goodsInfo.get('goodsInfoName')}
                    </p>
                    <span className="sellwell-spe">
                      {goodsInfo.get('specText')}
                    </span>

                    {/* 评价 */}
                    {isShow ? (
                      <div className="goods-evaluate">
                        <span className="goods-evaluate-spn">
                          {salesNum}销量
                        </span>
                        <span className="goods-evaluate-spn mar-lr-28">
                          {evaluateNum}
                          评价
                        </span>
                        <span className="goods-evaluate-spn">
                          {favorableRate}
                          %好评
                        </span>
                        {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
                      </div>
                    ) : (
                      <div className="goods-evaluate">
                        <span className="goods-evaluate-spn">
                          {salesNum}销量
                        </span>
                      </div>
                    )}

                    {goodsInfo.get('companyType') == 0 && (
                      <div className="marketing">
                        <div className="self-sales">自营</div>
                      </div>
                    )}
                    <div className="sellwell-bottom">
                      <div className="sellwell-bottom-left">
                        <span>￥{_.addZero(goodsInfo.get('marketPrice'))}</span>
                        {invalid && <div className="out-stock">缺货</div>}
                      </div>
                      {!invalid && (
                        <GoodsNum
                          value={buyCount}
                          max={stock}
                          disableNumberInput={invalid}
                          goodsInfoId={goodsInfo.get('goodsInfoId')}
                        />
                      )}
                    </div>
                  </div>
                </div>
              );
            })
          ) : (
            <Blank img={noneImg} content="没有搜到任何商品～" />
          )}
        </div>
      </div>
    );
  }
}
