import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { _, history } from 'wmkit';

const defaultImg = require('../../../web_modules/images/default-img.png');

@Relax
export default class DistributionTop extends Component<any, any> {
  props: {
    relaxProps?: {
      customerInfo: IMap;
      distributor: IMap;
      customerBalance: IMap;
      inviteInfo: IMap;
    };
  };

  static relaxProps = {
    customerInfo: 'customerInfo',
    customerBalance: 'customerBalance',
    inviteInfo: 'inviteInfo'
  };

  render() {
    const { customerInfo, customerBalance, inviteInfo } = this.props.relaxProps;

    let headImg = customerInfo.get('headImg')
      ? customerInfo.get('headImg')
      : defaultImg;

    return (
      <div className="distribution-top">
        <div className="distribution-top-info">
          <p className="distribu-title">奖励中心</p>
          <div className="distribu-user">
            <img className="distribu-user-img" src={headImg} alt="" />
          </div>
          <div className="distribu-leve">
            {customerInfo.get('customerName')}
          </div>
          {inviteInfo.get('customerName') && (
            <div className="distribu-inviter">
              邀请人：{inviteInfo.get('customerName') || '-'}
            </div>
          )}
        </div>
        <div className="distribu-top-ccount">
          <div className="distribu-ccount-left">
            <span>账户余额</span>
            <span>
              {customerBalance.get('accountBalanceTotal')
                ? _.fmoney(customerBalance.get('accountBalanceTotal'), 2)
                : '0.00'}
            </span>
          </div>
          <div
            className="distribu-ccount-right"
            onClick={() => history.push('/balance/deposit')}
          >
            立即提现 <span>></span>
          </div>
        </div>
      </div>
    );
  }
}
