import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMkit } from 'wmkit';
import { IMap, msg, Relax } from 'plume2';

import { cache } from 'config';
import { fromJS } from 'immutable';

@Relax
export default class MyCustomer extends React.Component<any, any> {
  props: {
    relaxProps?: {
      inviteCustomer: IMap;
      distributeSetting: IMap;
    };
  };

  static relaxProps = {
    inviteCustomer: 'inviteCustomer',
    distributeSetting: 'distributeSetting'
  };

  render() {
    const { inviteCustomer, distributeSetting } = this.props.relaxProps;
    const showFlag = WMkit.isDistributorLoginForShare();
    const openFlag = fromJS(distributeSetting).get('openFlag');
    let inviteNum = inviteCustomer ? inviteCustomer.get('inviteNum') : 0;
    let validInviteNum = inviteCustomer
      ? inviteCustomer.get('validInviteNum')
      : 0;
    let myCustomerNum = inviteCustomer
      ? inviteCustomer.get('myCustomerNum')
      : 0;
    return (
      <div>
        {openFlag ? (
            <div className="sales-friend">
            <div className="my-sales">
            <div
              className="up-nav"
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      //邀新注册
                      history.push('/social-c/my-customer');
                    }
                  });
                } else {
                  //邀请注册
                  history.push('/social-c/my-customer');
                }
              }}
            >
              <span className="left-span">我的用户</span>
              <i className="iconfont icon-jiantou1" />
            </div>
            <ul className="down-number">
              <li onClick={() => this._gotoMyCustomer('1')}>
                <span className="money">{inviteNum ? inviteNum : '0'}</span>
                <span className="name">邀新人数</span>
              </li>
              <li onClick={() => this._gotoMyCustomer('2')}>
                <span className="money">
                  {validInviteNum ? validInviteNum : '0'}
                </span>
                <span className="name">有效邀新</span>
              </li>
              <li>
              </li>
            </ul>
          </div>
          </div>
        ) : null}

      </div>
    );
  }

  /**
   * 跳转我的用户页面
   * @param tab
   * @private
   */
  _gotoMyCustomer = (tab) => {
    if (!WMkit.isLoginOrNotOpen()) {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          //邀新注册
          history.push('/social-c/my-customer/' + tab);
        }
      });
    } else {
      //邀请注册
      history.push('/social-c/my-customer/' + tab);
    }
  };
}
