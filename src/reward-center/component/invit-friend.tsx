import React from 'react';
import { history, WMkit } from 'wmkit';
import { msg, IMap, Relax } from 'plume2';
import { fromJS } from 'immutable';

@Relax
export default class InviteFriend extends React.Component<any, any> {
  props: {
    relaxProps?: {
      distributeSetting: IMap;
    };
  };

  static relaxProps = {
    distributeSetting: 'distributeSetting'
  };

  render() {
    const { distributeSetting } = this.props.relaxProps;
    const applyType = distributeSetting.get('applyType');
    const openFlag = distributeSetting.get('openFlag'); //是否打开分销设置
    const applyFlag = distributeSetting.get('applyFlag'); //是否打开申请入口
    const inviteFlag = distributeSetting.get('inviteFlag'); //是否开启邀请奖励

    const distributor = WMkit.isDistributor();
    //判断规则：
    // 1.社交分销开关(openFlag)控制显示
    // 2.申请入口(applyFlag)打开：
    //    小c根据申请条件展示
    // 3.申请入口(applyFlag)关闭:
    //   3.1 邀新奖励(inviteFlag)打开：小b小c都显示邀新入口
    //   3.2 邀新奖励(inviteFlag)关闭：都不显示
    return (
      <div>
        {distributeSetting.size > 0 && openFlag == 1 ? (
          applyFlag ? (
            <div className="invit-friend">
              <div
                className="invit-bj"
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        if (!distributor) {
                          //小C
                          if (applyType) {
                            //邀新注册
                            history.push('/invite-friends');
                          } else {
                            //开店礼包
                            history.push('/store-bags');
                          }
                        }
                      }
                    });
                  } else {
                    if (!distributor) {
                      //小C
                      if (applyType) {
                        //邀新注册
                        history.push('/invite-friends');
                      } else {
                        //开店礼包
                        history.push('/store-bags');
                      }
                    } else {
                      //小B
                      if (inviteFlag) {
                        history.push('/invite-friends');
                      }
                    }
                  }
                }}
              >
                {!distributor && (
                  <div className="three-text">
                    <img
                      src={
                        applyType
                          ? fromJS(distributeSetting).get(
                              'inviteRecruitEnterImg'
                            )
                          : fromJS(distributeSetting).get('buyRecruitEnterImg')
                      }
                      width="100%"
                      height="100%"
                    />
                  </div>
                )}
              </div>
            </div>
          ) : inviteFlag ? (
            <div className="invit-friend">
              <div
                className="invit-bj"
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        history.push('/invite-friends');
                      }
                    });
                  } else {
                    //邀请注册
                    history.push('/invite-friends');
                  }
                }}
              >
                <div className="three-text">
                  <img
                    src={distributeSetting.get('inviteEnterImg')}
                    width="100%"
                    height="100%"
                  />
                </div>
              </div>
            </div>
          ) : (
            ''
          )
        ) : (
          ''
        )}
      </div>
    );
  }
}
