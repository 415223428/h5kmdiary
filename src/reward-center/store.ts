import { IOptions, Store } from 'plume2';
import { fromJS } from 'immutable';
import moment from 'moment';
import { config } from 'config';
import { WMkit, history } from 'wmkit';
import * as webapi from './webapi';
import CenterActor from './actor/center-actor';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new CenterActor()];
  }

  /**
   * 分销中心初始化数据
   */
  init = async () => {
    //如果当前已经为分销员跳转分销中心
    if( WMkit.isDistributor()){
        history.replace('/distribution-center');
      return
    }

      const res = (await Promise.all([
        webapi.fetchCustomerCenterInfo(),
        webapi.customerBalanceInfo(),
        webapi.fetchInvitorInfo(),
        webapi.fetchInviteCutomer(),
        webapi.fetchHotDistributeList({
          pageNum: 0,
          pageSize: 10,
          sortFlag: 0
        })
      ])) as any;

      // 会员信息
      if (res[0].code == config.SUCCESS_CODE) {
        this.onFieldChange({
          field: 'customerInfo',
          value: fromJS(res[0].context)
        });
      }

      // 会员余额信息
      if (res[1].code == config.SUCCESS_CODE) {
        this.onFieldChange({
          field: 'customerBalance',
          value: fromJS(res[1].context)
        });
      }

      if (res[2].code == config.SUCCESS_CODE) {
        //分销设置信息
        this.onFieldChange({
          field: 'distributeSetting',
          value: fromJS((res[2].context as any).distributionSettingSimVO)
        });
        // 邀请人信息
        this.onFieldChange({
          field: 'inviteInfo',
          value: fromJS((res[2].context as any).distributionCustomerSimVO)
        });
      }
      //设置邀新人数
      if (res[3].code == config.SUCCESS_CODE) {
        this.onFieldChange({
          field: 'inviteCustomer',
          value: fromJS(res[3].context)
        });
      }
      //热销的分销商品
      if (res[4].code == config.SUCCESS_CODE) {
        const data = res[4].context
          ? res[4].context.esGoodsInfoPage.content
          : [];
        this.onFieldChange({
          field: 'hotGoodsList',
          value: fromJS(data)
        });
      }
  };

  /**
   * 数据变更
   * @param {any} field
   * @param {any} value
   */
  onFieldChange = ({ field, value }) => {
    this.dispatch('center: field: change', { field, value });
  };

  /**
   * 分享赚选中的sku
   * @param sku
   */
  saveCheckedSku = (sku) => {
    this.onFieldChange({ field: 'checkedSku', value: fromJS(sku) });
  };

  /**
   * 切换分享赚弹框显示与否
   */
  changeShareVisible = () => {
    this.onFieldChange({ field: 'shareVisible', value: true });
  };
}
