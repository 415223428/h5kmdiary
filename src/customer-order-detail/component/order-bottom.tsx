import React, { Component } from 'react'
import {IMap, Relax} from 'plume2'

import { history, OrderWrapper, noop, storage} from 'wmkit'
import {cache} from 'config'

import {operationsQL} from "../ql";


@Relax
export default class OrderBottom extends Component<any, any> {

  props: {
    relaxProps?: {
      operations: any
      detail: IMap
      _cancelOrder: Function
      applyRefund: Function
      defaultPay:Function
    },
  };


  static relaxProps = {
    operations: operationsQL,
    detail: 'detail',
    _cancelOrder: noop,
    applyRefund: noop,
    defaultPay:noop
  }


  render() {

    let {operations, detail,defaultPay}  = this.props.relaxProps
    let orderWrapper = OrderWrapper(detail)

    return (
      <div style={{height: 48}}>
        <div className="order-button-wrap ">
          {
            operations == undefined? null :
              operations.available.map(availableButton => {
                return <div className={availableButton == '去付款'? 'btn btn-ghost-red btn-small' : 'btn btn-ghost btn-small'}
                            onClick={() => {
                              this._operationButtons(orderWrapper.orderNo(), availableButton, orderWrapper.payId())
                            }}>
                  {availableButton}
                </div>
              })
          }
        </div>
      </div>
    )
  }


  /**
   * 订单操作按钮event handler
   */
  _operationButtons = async (tid, button, payTypeId) => {
    const {_cancelOrder,applyRefund,defaultPay} = this.props.relaxProps
    const { detail } = this.props.relaxProps
    let orderWrapper = OrderWrapper(detail)
    //应付金额
    let actualPrice = orderWrapper.totalPrice()    
    if(button == '取消订单') {
      _cancelOrder(tid)
    }else if(button == '确认收货') {
      history.push(`/ship-record/${tid}/0`)
    }else if(button == '去付款'  && actualPrice=='0.00'){      
      //0元素订单直接调默认支付接口
      defaultPay(tid)
    }else if(button == '去付款' && payTypeId == '0' && actualPrice!='0.00') {
      history.push(`/pay-online/${tid}`)
    }else if(button == '去付款' && payTypeId == '1'&& actualPrice!='0.00'){
      this._clearCache();
      history.push({
        pathname:`/fill-payment/${tid}`,
        search:'from=order-detail'
      })
    }else if(button == '退货退款') {
      applyRefund(tid)
    }
  }
  
    _clearCache = async () => {
      storage('session').del(cache.PAYMENT_ENCLOSES)
      storage('session').del(cache.PAYMENT_REMARK)
      storage('session').del(cache.PAYMENT_TIME)
    }
}
