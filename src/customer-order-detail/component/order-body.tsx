import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';
import { fromJS, Map } from 'immutable';
import { cache } from 'config';
import {
  FormItem,
  FormSelect,
  OrderWrapper,
  history,
  ImageListScroll
} from 'wmkit';

const noneImg = require('../img/none.png');

@Relax
export default class OrderBody extends Component<any, any> {
  props: {
    relaxProps?: {
      detail: IMap;
    };
  };

  static relaxProps = {
    detail: 'detail'
  };

  render() {
    const { detail } = this.props.relaxProps;
    const buySelf =
      detail.get('buyer') &&
      detail.get('buyer').get('id') == detail.get('inviteeId');
    let orderWrapper = OrderWrapper(detail);
    return (
      <div>
        <Link to={`/store-main/${orderWrapper.storeId()}`}>
          <div className="sku-head">
            {orderWrapper.isSelf() ? (
              <div className="self-sales" style={{ marginLeft: 5 }}>
                自营
              </div>
            ) : null}
            {orderWrapper.storeName()}
          </div>
        </Link>
        <Link to={`/distribute-order-detail-skus/${orderWrapper.orderNo()}`}>
          <div className="limit-img">
            <div className="img-content">
              {orderWrapper
                .tradeItems()
                .concat(orderWrapper.gifts())
                .toJS()
                .map((v, k) => {
                  if (k < 4) {
                    return (
                      <img className="img-item" src={v.pic ? v.pic : noneImg} />
                    );
                  }
                })}
            </div>
            <div className="right-context">
              <div className="total-num">
                <div>
                  共{
                    orderWrapper.tradeItems().concat(orderWrapper.gifts()).size
                  }种
                </div>
              </div>
              <i className="iconfont icon-jiantou1" />
            </div>
          </div>
        </Link>
        <div className="mb10">
          <FormSelect
            labelName="付款记录"
            placeholder=""
            selected={{ key: '1', value: orderWrapper.orderPayState() }}
            onPress={() =>
              this._toPayRecord(
                orderWrapper.orderNo(),
                orderWrapper.orderPayState()
              )
            }
          />
          <FormSelect
            labelName="发货记录"
            placeholder=""
            selected={{ key: '1', value: orderWrapper.orderDeliveryState() }}
            onPress={() =>
              buySelf
                ? this._toShipRecord(
                    orderWrapper.orderNo(),
                    orderWrapper.orderDeliveryState()
                  )
                : null
            }
            iconVisible={this._isIconVisible(
              buySelf,
              orderWrapper.orderDeliveryState()
            )}
          />
        </div>
        <div className="mb10 top-border">
          <FormSelect
            labelName="发票信息"
            placeholder=""
            selected={{ key: '1', value: orderWrapper.orderInvoice() }}
            onPress={() => this._toInvoice(orderWrapper.orderNo())}
            iconVisible={this._isInvoiceVisible(orderWrapper.orderInvoice())}
          />
        </div>
        <div className="mb10 top-border">
          <FormItem
            labelName="订单备注"
            placeholder={orderWrapper.buyerRemark()}
          />
          <FormItem
            labelName="卖家备注"
            placeholder={orderWrapper.sellerRemark()}
          />
          <ImageListScroll
            imageList={orderWrapper.encloses()}
            labelName="订单附件"
          />
        </div>
        <div className="total-price mb10">
          <div className="total-list">
            <span>应付金额</span>
            <span className="price-color">
              <i className="iconfont icon-qian" />
              {orderWrapper.totalPrice()}
            </span>
          </div>
          <div className="total-list">
            <span>商品总额</span>
            <span>
              <i className="iconfont icon-qian" />
              {orderWrapper.goodsPrice()}
            </span>
          </div>
          {orderWrapper.reductionPrice() != 0 && (
            <div className="total-list">
              <span>满减优惠</span>
              <span>
                -<i className="iconfont icon-qian" />
                {orderWrapper.reductionPrice()}
              </span>
            </div>
          )}
          {orderWrapper.discountPrice() != 0 && (
            <div className="total-list">
              <span>满折优惠</span>
              <span>
                -<i className="iconfont icon-qian" />
                {orderWrapper.discountPrice()}
              </span>
            </div>
          )}
          {orderWrapper.pointsPrice() != 0 && (
            <div className="total-list">
              <span>积分抵扣</span>
              <span>
                -<i className="iconfont icon-qian" />
                {orderWrapper.pointsPrice()}
              </span>
            </div>
          )}
          {orderWrapper.couponPrice() != 0 && (
            <div className="total-list">
              <span>优惠券</span>
              <span>
                -<i className="iconfont icon-qian" />
                {orderWrapper.couponPrice()}
              </span>
            </div>
          )}
          <div className="total-list">
            <span>配送费用</span>
            <span>
              <i className="iconfont icon-qian" />
              {orderWrapper.deliveryPrice()}
            </span>
          </div>

          <div className="total-list total-rebate b-1px-t">
            <span>预计可赚</span>
            <span>
              <i className="iconfont icon-qian" />
              {orderWrapper.commission()}
            </span>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 付款记录页
   */
  _toPayRecord = (tid: string, pay: string) => {
    history.push({
      pathname: `/pay-detail/${tid}`
    });
  };

  /**
   * 发票记录
   */
  _toInvoice = (id: string) => {
    const { detail } = this.props.relaxProps;
    let orderWrapper = OrderWrapper(detail);
    if (orderWrapper.orderInvoice() == '不需要发票') {
      return;
    } else {
      history.push({
        pathname: `/invoice-info/${id}/1`
      });
    }
  };

  _toShipRecord = (id: string, state: string) => {
    if (state == '未发货') {
      return;
    } else {
      history.push(`/ship-record/${id}/1`);
    }
  };

  /**
   * 发货记录icon是否显示
   * @param state
   * @returns {boolean}
   * @private
   */
  _isIconVisible = (buySelf, status) => {
    //已发货且为自购，才显示
    if (status === '未发货' || !buySelf) {
      return false;
    } else {
      return true;
    }
  };

  /**
   * 发票信息是否显示
   */
  _isInvoiceVisible = (invoice: string) => {
    if (invoice === '不需要发票') {
      return false;
    }
    return true;
  };
  /*  _showImage=()=>{
        System.import('..../wmkit/image-util/zoom-image').then((zoomImage) => {
          ZoomImage = zoomImage;
        });
      ZoomImage.renderZoomImage({src})
     }*/
}
