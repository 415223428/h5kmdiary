/**
 * Created by chenpeng on 2017/7/25.
 */
import {fromJS} from 'immutable'
import {Action, Actor, IMap} from "plume2";

export default class DetailActor extends Actor {
	defaultState() {
		return {
			detail: {}, //订单详情
			skus: [], // 商品清单
			gifts: [] //赠品清单
		}
	}


	/**
	 * 初始化订单
	 */
	@Action('detail-actor:init')
	init(state: IMap, res: object) {
		return state.update('detail', detail => detail.merge(res))
	}

	
	/**
	 * 设置商品清单
	 */
	@Action('detail-actor:setSkus')
	setSkus(state: IMap, param: any) {
		return state.set('skus', fromJS(param))
	}

  /**
	 * 设置赠品清单
   */
	@Action('detail-actor:setGifts')
  setGifts(state: IMap, param: any) {
		return state.set('gifts', fromJS(param))
	}
}