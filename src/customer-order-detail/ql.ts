/**
 * Created by chenpeng on 2017/7/25.
 */
import { IMap, QL } from 'plume2';

/**
 * 订单可用操作按钮DQL
 * @type {plume2.QueryLang}
 */
export const operationsQL = QL('operationsQL', [
  'detail',
  (order: IMap) => {
    let orderButtons = {
      available: [],
      id: ''
    };

    if (order) {
      const flow = order.getIn(['tradeState', 'flowState']);
      const pay = order.getIn(['tradeState', 'payState']);

      //取消订单
      const cancelButtons = [['INIT', 'NOT_PAID'], ['AUDIT', 'NOT_PAID']];
      //付款
      const payButtons = [
        ['AUDIT', 'NOT_PAID'],
        ['DELIVERED_PART', 'NOT_PAID'],
        ['DELIVERED', 'NOT_PAID'],
        ['CONFIRMED', 'NOT_PAID']
      ];
      //确认收货
      const confirmButtons = [
        ['DELIVERED', 'NOT_PAID'],
        ['DELIVERED', 'PAID'],
        ['DELIVERED', 'UNCONFIRMED']
      ];
      //退货退款
      const refundButtons = [['AUDIT', 'PAID'], ['COMPLETED', 'PAID']];
      const canReturnFlag = order.get('canReturnFlag');

      let availables = Array();
      payButtons.filter((button) => _calc(button)(flow, pay)).length > 0
        ? availables.push('去付款')
        : null;
      confirmButtons.filter((button) => _calc(button)(flow, pay)).length > 0
        ? availables.push('确认收货')
        : null;
      cancelButtons.filter((button) => _calc(button)(flow, pay)).length > 0
        ? availables.push('取消订单')
        : null;

      canReturnFlag ? availables.push('退货退款') : null;
      orderButtons['available'] = availables;
      orderButtons['id'] = order.get('id');
    }
    return orderButtons;
  }
]);

/**
 * 订单详情-商品清单QL
 */
export const skusQL = QL('skusQL', [
  'skus',
  (skus) => {
    return skus.toJS().map((val) => {
      return {
        imgUrl: val.pic,
        skuName: val.skuName,
        specDetails: val.specDetails,
        price: val.price,
        num: val.num,
        unit: val.unit,
        distributionCommission: val.distributionCommission,
        distributionGoodsAudit: val.distributionGoodsAudit,
        isAccountStatus: val.isAccountStatus
      };
    });
  }
]);

/**
 * 计算订单可用按钮
 */
const _calc = (button: Array<string>) => {
  return function(flow: string, pay: string) {
    return button[0] === flow && button[1] === pay;
  };
};
