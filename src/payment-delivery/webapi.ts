import { Fetch } from 'wmkit'

/**
 * 在线支付是否开启
 * @returns {Promise<Result<any>>}
 */
export const fetchOnlinePayStatus = () => {
  return Fetch('/pay/gateway/isopen/H5', {
    method: 'GET'
  })
}