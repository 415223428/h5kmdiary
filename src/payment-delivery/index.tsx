import React from 'react';
import { StoreProvider } from 'plume2';
import './css/payment-de.css'

import { RadioBox, noop, storage, history } from 'wmkit';
import { cache } from 'config';

import AppStore from './store';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class PaymentDelivery extends React.Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.store.init();
  }

  render() {
    const { sid } = this.props.match.params;
    return (
      <div>
        <div className="pay-ways" style={{borderBottom:"0.3rem solid #fafafa"}}>
          <h2 style={{margin:'0.1rem 0 0.2rem'}}>支付方式</h2>
          <div style={{height:"1px",background:"rgba(229,229,229,1)"
            // ,marginTop:"15px"
            }}></div>
          <RadioBox
            data={this.store
              .state()
              .get('payOptions')
              .toJS()}
            checked={this.store.state().get('payType')}
            onCheck={(v) =>
              this.store.onSelectPayInfo({ payId: v, storeId: sid })
            }
            style={{ width: '22.6%',borderRadius:"29px" }}
          />
        </div>

        <div className="pay-ways" style={{borderBottom:"0px solid #ebebeb"}}>
          <h2 style={{margin:'0.1rem 0 0.2rem'}}>配送方式</h2>
          <div style={{height:"1px",background:"rgba(229,229,229,1)"}}></div>
          <RadioBox
            data={[{ id: 1, name: '快递配送' }]}
            checked={1}
            onCheck={noop}
            style={{ width: '22.6%',borderRadius:"29px" }}
          />
        </div>
        <div className="payment-de-btn" style={{padding:"0px"}}>
          <img src={require('./img/confirm.png')} alt="" style={{height:'1rem',display:'block'}}
              onClick={() => this._save()}/>
        </div>
      </div>
    );
  }

  /**
   * 保存
   */
  _save = () => {
    const payType = this.store.state().get('payType');
    storage('session').set(cache.ORDER_CONFIRM_PAYTYPE, {
      payType
    });
    history.go(-1);
  };
}
