import { Fetch } from 'wmkit';

/**
 * 我的会员中心
 * @returns
 */
export const fetchCustomerCenterInfo = () => {
  return Fetch(
    '/customer/customerCenter',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 获取该会员作为分销员信息
 */
export const fetchDistributorInfo = () => {
  return Fetch(
    '/distribute/distributor-info',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 获取余额页面详情
 * @returns {Promise<Result<TResult>>}
 *
 */
export const customerBalanceInfo = () => {
  return Fetch(
    '/customer/funds/statistics',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 获取分销员的本月销售业绩
 * @param filterParams
 */
export function distributionPerformanceMonth() {
  return Fetch(
    '/distribution/performance/summary/month',
    {},
    {
      showTip: false
    }
  );
}

/**
 * 获取分销员昨天的销售业绩
 * @param filterParams
 */
export function distributionPerformanceYesterday(filterParams = {}) {
  return Fetch(
    '/distribution/performance/yesterday',
    {
      method: 'POST',
      body: JSON.stringify({
        ...filterParams
      })
    },
    {
      showTip: false
    }
  );
}

/**
 * 获取分销设置和邀请人的信息
 */
export const fetchInvitorInfo = () => {
  return Fetch(
    '/distribute/setting-invitor',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 获取分销员的销售业绩
 */
export const fetchDistributionPerformance = () => {
  return Fetch(
    '/distribute/sales/performance',
    {},
    {
      showTip: false
    }
  );
};

/**
 * 统计邀新人数
 */
export const fetchInviteCutomer = () => {
  return Fetch(
    '/customer/count-invite-customer',
    {
      method: 'POST'
    },

    {
      showTip: false
    }
  );
};

/**
 * 统计邀新人数
 */
export const fetchHotDistributeList = (params) => {
  return Fetch(
    '/goods/shop/add-distributor-goods',
    {
      method: 'POST',
      body: JSON.stringify(params)
    },
    {
      showTip: false
    }
  );
};

/**
 * 查询店铺是否是有效状态
 * @returns {Promise<Result<T>>}
 */
export const fetchShopStatus = () => {
  return Fetch(
    '/distribute/check/status',
    {},
    {
      showTip: false
    }
  );
};
