import { Actor, Action, IMap } from 'plume2';

export default class CenterActor extends Actor {
  defaultState() {
    return {
      // 显示在会员中心的数据
      customerInfo: {},
      // 分销员信息
      distributor: {},
      // 分销设置信息
      distributeSetting: {},
      // 会员余额
      customerBalance: {},
      // 分销员昨天销售业绩
      yesterdayPerformance: {},
      // 分销员本月销售业绩
      monthPerformance: {},
      // 邀请人信息
      inviteInfo: {},
      // 我的客户 - 邀新人数
      inviteCustomer: {},
      // 热销前十分销商品
      hotGoodsList: [],
      // 分享赚选中的sku
      checkedSku: {},
      // 分销等级详细说明弹层初始化状态
      detailState: false,
      // 分享弹出显示与否
      shareVisible: false
    };
  }

  /**
   * 数据变化
   * @param state
   * @param context
   * @returns {*}
   */
  @Action('center: field: change')
  initList(state: IMap, { field, value }) {
    return state.set(field, value);
  }

  /**
   * 详细说明弹层显示
   */
  @Action('InvitActor:changeDetail')
  changeDetail(state, val) {
    return state.set('detailState', val);
  }

  /**
   * 切换分享赚弹框显示与否
   */
  @Action('center: share: visible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }
}
