import React from 'react';
import { IMap, Relax } from 'plume2';
import { noop } from 'wmkit';

import { fromJS } from 'immutable';
@Relax
export default class Layer extends React.Component<any, any> {
  props: {
    relaxProps?: {
      distributeSetting: IMap;
      closeLayer: Function;
    };
  };
  static relaxProps = {
    distributeSetting: 'distributeSetting',
    closeLayer: noop
  };

  render() {
    const { distributeSetting, closeLayer } = this.props.relaxProps;
    return (
      <div className="detail-div">
        <div className="scroll-div">
          <div className="white-div">
            <div
              className="about-box"
              dangerouslySetInnerHTML={{
                __html: fromJS(distributeSetting).get('distributorLevelDesc')
              }}
            />
          </div>
        </div>
        <div className="close" onClick={() => closeLayer(false)}>
          <i className="iconfont icon-Close f-style" />
        </div>
      </div>
    );
  }
}
