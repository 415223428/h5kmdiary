import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { history, storage } from 'wmkit';
import { cache } from 'config';

@Relax
export default class SalesFriend extends Component<any, any> {
  constructor(props) {
    super(props);

    // 小眼睛显示还是隐藏
    let showFlag = storage().get(cache.MY_PERFORMANCE);
    showFlag == 'true'
      ? (this.state = { showFlag: true })
      : (this.state = { showFlag: false });
  }

  props: {
    relaxProps?: {
      // 分销员昨天销售业绩
      yesterdayPerformance: IMap;
      // 分销员本月销售业绩
      monthPerformance: IMap;
    };
  };

  static relaxProps = {
    yesterdayPerformance: 'yesterdayPerformance',
    monthPerformance: 'monthPerformance'
  };

  render() {
    const { yesterdayPerformance, monthPerformance } = this.props.relaxProps;
    const { showFlag } = this.state;
    return (
      <div className="sales-friend">
        <div className="my-sales">
          <div className="up-nav">
            <span className="span-box">
              <span className="left-span">我的销售业绩</span>
              <i
                className={`iconfont ${
                  showFlag ? 'icon-xianshi' : 'icon-buxianshi'
                } font-46`}
                onClick={() => this._onEyeClick()}
              />
            </span>
            <span
              className="span-box"
              onClick={() => history.push('/customer-order-list')}
            >
              <span className="right-span">推广订单</span>
              <i className="iconfont icon-jiantou1" />
            </span>
          </div>
          <ul
            className="down-number"
            onClick={() => history.push('/sales-perform')}
          >
            <li>
              <span className="money">
                {showFlag
                  ? `￥${yesterdayPerformance.get('saleAmount') || '0.00'}`
                  : '***'}
              </span>
              <span className="name">昨日销售额</span>
            </li>
            <li>
              <span className="money">
                {showFlag
                  ? `￥${yesterdayPerformance.get('commission') || '0.00'}`
                  : '***'}
              </span>
              <span className="name">昨日预估收益</span>
            </li>
            <li>
              <span className="money">
                {showFlag
                  ? `￥${(monthPerformance &&
                      monthPerformance.get('saleAmount')) ||
                      '0.00'}`
                  : '***'}
              </span>
              <span className="name">本月销售额</span>
            </li>
            <li>
              <span className="money">
                {showFlag
                  ? `￥${(monthPerformance &&
                      monthPerformance.get('commission')) ||
                      '0.00'}`
                  : '***'}
              </span>
              <span className="name">本月预估收益</span>
            </li>
          </ul>
        </div>
      </div>
    );
  }

  /**
   * 改变当前页销售数据显示
   * @private
   */
  _onEyeClick = () => {
    const { showFlag } = this.state;
    this.setState({ showFlag: !showFlag });
    storage().set(cache.MY_PERFORMANCE, !showFlag);
  };
}
