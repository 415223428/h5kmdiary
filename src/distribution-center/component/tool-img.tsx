import React, { Component } from 'react';
import { history, noop } from 'wmkit';
import { IMap, Relax } from 'plume2';

@Relax
export default class ToolImg extends Component<any, any> {
  constructor(props) {
    super(props);
  }

  props: {
    relaxProps?: {
      distributeSetting: IMap;
      distributor: IMap;
      shopClosedTip: Function;
    };
  };

  static relaxProps = {
    distributeSetting: 'distributeSetting',
    distributor: 'distributor',
    shopClosedTip: noop
  };

  render() {
    const {
      distributeSetting,
      distributor,
      shopClosedTip
    } = this.props.relaxProps;

    // 小店是否关闭 0：关闭，1：开启
    let shopOpenFlag = distributeSetting.get('shopOpenFlag');
    // 分销员是否禁用 0: 启用中  1：禁用中
    let forbidFlag = distributor.get('forbiddenFlag');

    // 小店关店后，不展示店铺入口
    let liStyle =
      shopOpenFlag == 0
        ? 'calc(100% / 2 - 0.27rem)'
        : 'calc(100% / 3 - 0.27rem)';

    return (
      <div className="tool-enter">
        <ul className="tool-imgs">
          {shopOpenFlag == 1 && (
            <li style={{ width: liStyle }}>
              <img
                src={require(forbidFlag
                  ? '../img/tool-1-disabled.png'
                  : '../img/tool-1.png')}
                alt=""
                onClick={() =>
                  forbidFlag
                    ? shopClosedTip(distributor.get('forbiddenReason'))
                    : history.push('/shop-index')
                }
              />
            </li>
          )}
          <li style={{ width: liStyle }}>
            <img
              src={require('../img/tool-2.png')}
              alt=""
              onClick={() => history.push('/distribution-goods-list')}
            />
          </li>
          <li style={{ width: liStyle }}>
            <img
              src={require('../img/tool-3.png')}
              alt=""
              onClick={() => history.push('/material-circle')}
            />
          </li>
        </ul>
      </div>
    );
  }
}
