import React from 'react';
import { history, WMkit } from 'wmkit';
import { msg, IMap, Relax } from 'plume2';

@Relax
export default class InviteFriend extends React.Component<any, any> {
  props: {
    relaxProps?: {
      distributeSetting: IMap;
    };
  };

  static relaxProps = {
    distributeSetting: 'distributeSetting'
  };

  render() {
    const { distributeSetting } = this.props.relaxProps;
    // 是否开启社交分销 0：关闭，1：开启
    const openFlag = distributeSetting.get('openFlag');
    // 是否开启申请入口  0：关闭  1：开启
    const applyFlag = distributeSetting.get('applyFlag');
    // 是否开启邀请奖励 0：关闭，1：开启
    const inviteFlag = distributeSetting.get('inviteFlag');

    //判断规则：分销总开关打开 && 开启申请入口 && 开启邀请奖励
    return (
      <div>
        {distributeSetting.size > 0 &&
          openFlag == 1 &&
          applyFlag == 1 &&
          inviteFlag == 1 && (
            <div className="invit-friend">
              <div
                className="invit-bj"
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () =>
                        //邀新注册
                        history.push('/invite-friends')
                    });
                  } else {
                    history.push('/invite-friends');
                  }
                }}
              >
                <div className="three-text">
                  <img
                    src={distributeSetting.get('inviteEnterImg')}
                    width="100%"
                    height="100%"
                  />
                </div>
              </div>
            </div>
          )}
      </div>
    );
  }
}
