import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import ReactClipboard from 'react-clipboardjs-copy';
import { _, history, noop } from 'wmkit';

const defaultImg = require('../../../web_modules/images/default-headImg.png');

@Relax
export default class DistributionTop extends Component<any, any> {
  props: {
    relaxProps?: {
      customerInfo: IMap;
      distributor: IMap;
      customerBalance: IMap;
      inviteInfo: IMap;
      distributeSetting: IMap;
      shopClosedTip: Function;
    };
  };

  static relaxProps = {
    _openDetail: noop,
    customerInfo: 'customerInfo',
    distributor: 'distributor',
    customerBalance: 'customerBalance',
    inviteInfo: 'inviteInfo',
    distributeSetting: 'distributeSetting',
    shopClosedTip: noop
  };

  render() {
    const {
      customerInfo,
      distributor,
      customerBalance,
      inviteInfo,
      distributeSetting,
      shopClosedTip
    } = this.props.relaxProps;

    let headImg = customerInfo.get('headImg')
      ? customerInfo.get('headImg')
      : defaultImg;

    // 分销员是否禁用 0: 启用中  1：禁用中
    let forbidFlag = distributor.get('forbiddenFlag');

    return (
      <div className="distribution-top">
        <div className="distribution-top-info">
          <p className="distribu-title">分销中心</p>
          <div className="distribu-user">
            <img className="distribu-user-img" src={headImg} alt="" />
          </div>
          <div className="distribu-leve">
            <div
              className={`distribu-leve  ${
                forbidFlag == 1 ? 'distribu-leve-disabled' : ''
              }`}
            >
              {customerInfo.get('customerName')}
              <span onClick={() => history.push('/distribution-rule')}>
                {distributor.get('distributorLevelName') ||
                  distributeSetting.get('distributorName')}
              </span>
              {forbidFlag == 1 && (
                <span
                  onClick={() =>
                    shopClosedTip(distributor.get('forbiddenReason'))
                  }
                  className="disabled-tag"
                >
                  已禁用
                </span>
              )}
            </div>
            {forbidFlag == 1 && (
              <div
                className="distribute-forbid"
                onClick={() =>
                  shopClosedTip(distributor.get('forbiddenReason'))
                }
              >
                禁用原因：{distributor.get('forbiddenReason') || ''}
              </div>
            )}
            {inviteInfo.get('customerName') && (
              <div className="distribu-inviter">
                邀请人：{inviteInfo.get('customerName')}
              </div>
            )}
            <div className="distribu-inviter-code">
              我的邀请码：
              <ReactClipboard
                text={distributor.get('inviteCode')}
                onSuccess={() => alert('已复制邀请码')}
                onError={() => alert('已复制邀请码')}
              >
                <span>{distributor.get('inviteCode') || '-'} 复制</span>
              </ReactClipboard>
            </div>
          </div>
          <div className="distribu-top-ccount">
            <div className="distribu-ccount-left">
              <span>账户余额</span>
              <span>
                {customerBalance.get('accountBalanceTotal')
                  ? _.fmoney(customerBalance.get('accountBalanceTotal'), 2)
                  : '0.00'}
              </span>
            </div>
            <div
              className="distribu-ccount-right"
              onClick={() => history.push('/balance/deposit')}
            >
              立即提现 <span>></span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
