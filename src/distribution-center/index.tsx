import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit } from 'wmkit';
import { ShareModal } from 'biz';

import AppStore from './store';
import DistributionTop from './component/distribution-top';
import SalesFriend from './component/sales-friend';
import SellwellGoods from './component/sellwell-goods';
import InviteFriend from './component/invit-friend';
import MyCustomer from './component/my-customer';
import ToolImg from './component/tool-img';
import Layer from './component/layer';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class DistributionCenter extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    if (WMkit.isLoginOrNotOpen()) {
      this.store.init();
    }
  }

  render() {
    let detailState = this.store.state().get('detailState');
    return (
      <div className="distribution-container">
        <DistributionTop />
        <SalesFriend />
        <InviteFriend />
        <MyCustomer />
        <ToolImg />
        <SellwellGoods />
        {detailState && <Layer />}

        {/*分享赚弹框*/}
        <ShareModal
          shareType={2}
          goodsInfo={this.store.state().get('checkedSku')}
          visible={this.store.state().get('shareVisible')}
          changeVisible={this.store.changeShareVisible}
        />
      </div>
    );
  }
}
