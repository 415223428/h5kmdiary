import { Fetch } from 'wmkit';
type TResult = { code: string; message: string; context: any };

// 请求图片,时间,是否打开闪屏
export const fetchLogo = () => {
  return Fetch<TResult>('/system/advertisingConfig', {
    method: 'GET',
    body: JSON.stringify({})
  });
};


// export const isShowImg = () => {
//   console.log('isShowImg');
//   return Fetch('/isShowImg', {
//     method: 'POST',
//     body: JSON.stringify({})
//   });
// };

// export const isShowAD = () => {
//   console.log('isShowAD');
//   return Fetch('/isShowAD', {
//     method: 'POST',
//     body: JSON.stringify({})
//   });
// };