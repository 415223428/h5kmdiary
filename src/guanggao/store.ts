import { Store } from 'plume2';
import parse from 'url-parse';

import { Alert, history, WMkit, storage, _, wxAuth } from 'wmkit';
import { cache, config } from 'config';
import ShanPingActor from './actor/shanPing-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new ShanPingActor()];
  }

  constructor(props) {
    super(props);
    (window as any)._store = this;
  }

  init = async () => {
    // this.dispatch('shanPing:init');
    this.fetchLogo();
  };

  // 请求logo
  fetchLogo = async () => {
    const res = await webapi.fetchLogo();
    console.log(res);
    if (res['status'] == '404') {
      this.dispatch('shanPing:init');
    } else {
      this.dispatch('init:logo', res);
    }
  };

  isShowAD = async (isShowAD) => {
    this.dispatch('shanPing:isShowAD', !isShowAD)
  }


}




