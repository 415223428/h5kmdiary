import { Actor, Action, IMap } from 'plume2';
export default class ShanPingActor extends Actor {
  defaultState() {
    return {
      bannerImages: require('../img/logo.png'),
      bannerUrl:'',
      time: 5,
      bannerSwitchFlag:true,
    };
  }

  /**
   * 页面初始化
   * @param state
   */
  @Action('shanPing:init')
  init(state) {
    return state
      .set('bannerImages', state.get('bannerImages'))
      .set('bannerUrl', state.get('bannerUrl'))
      .set('bannerSwitchFlag',state.get('bannerSwitchFlag'))
  }


   // 设置背景logo
   @Action('init:logo')
   setLogo(state, res) {
     return state
     .set('bannerImages', res.bannerImages)
     .set('bannerUrl', res.bannerUrl)
     .set('bannerSwitchFlag', res.bannerSwitchFlag)

   }

   //设置闪屏
   @Action('shanPing:isShowAD')
   setisShowAD(state,bannerSwitchFlag){
    return state.set('bannerSwitchFlag',bannerSwitchFlag)
   }
}
