import React from 'react';
import { Link } from 'react-router-dom';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const styles = require('./css/style.css');
const logo = require('./img/logo.png');
import { WMkit, history } from 'wmkit';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class guanggao extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '广告';
    this.store.init();
  }

  constructor(props: any) {
    super(props);
  }

  render() {
    const bannerImages = this.store.state().toJS().bannerImages;
    const time = this.store.state().get('time');
    const isShowAD = this.store.state().get('bannerSwitchFlag')
    const fullBannerUrl = this.store.state().get('fullBannerUrl')
    return (
      <div >
        {
          isShowAD?
          <div className="guanggao"
          onClick={()=>{
            history.push(fullBannerUrl)
          }}
          >
            <img src={bannerImages} alt="" />
            <div className="xx"
            onClick={(e)=>{
              e.stopPropagation();
              this.store.isShowAD(isShowAD)
            }}
            >
              <div>x</div>
            </div>
          </div>
           : null
        }
      </div>
    );
  }
}
