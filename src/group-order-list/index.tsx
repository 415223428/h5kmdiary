import React from 'react';
import { IMap, StoreProvider } from 'plume2';

import { ListView, Blank, GrouponBottom } from 'wmkit';
import './css/style.css';
import AppStore from './store';
import OrderItem from './component/order-list-item';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class OrderList extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '我的拼购';
  }
  render() {
    let fetchOrders = this.store.fetchOrders;
      let initToRefresh = this.store.initToRefresh;
      let countOver = this.store.countOver;
    return (
      <div className="container">
        <ListView
          toRefresh={(param)=>initToRefresh(param)}
          url="/trade/page/groupons"
          getServerTime={true}
          renderRow={(
            order: IMap,
            index: number,
            otherPropsObject,
            serverTime
          ) => {
            return (
              <OrderItem item={order} index={index} serverTime={serverTime} countOver={countOver}/>
            );
          }}
          renderEmpty={() => (
            <Blank
              img={require('./img/list-none.png')}
              content="您暂时还没有订单哦"
              isToGoodsList={false}
            />
          )}
          onDataReached={(res) => fetchOrders(res)}
        />
        <GrouponBottom currTab={'我的拼购'} />
      </div>
    );
  }
}
