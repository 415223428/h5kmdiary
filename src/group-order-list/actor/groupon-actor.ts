import { Actor, Action, IMap } from 'plume2';
import { fromJS } from 'immutable';
import { queryServerTime } from '../webapi';

export default class GrouponnActor extends Actor {
  defaultState() {
    return {
      //服务时间
      serverTime: 0
    };
  }
  @Action('groupon:detail:serverTime')
  queryServerTime(state, result) {
    return state.set('serverTime', result);
  }
}
