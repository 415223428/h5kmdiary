import { fromJS } from 'immutable';

import { IOptions, Store } from 'plume2';
import * as webapi from './webapi';

import GrouponActor from './actor/groupon-actor';

export default class AppStore extends Store {
  initToRefreshBack;
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
        (window as any)._store = this;
    }
  }

  bindActor() {
    return [new GrouponActor()];
  }
  /**
   * 设置订单列表
   * @param res
   */
  fetchOrders = (res: any) => {
    // if (res.context) {
    //   this.dispatch('order-list-form:setOrders', fromJS(res.context.content));
    // }
  };

  /**
   * 计算订单有效按钮
   */
  _calc = (button: Array<string>) => {
    return function(flow: string, pay: string) {
      return button[0] === flow && button[1] === pay;
    };
  };

  /**
   * 初始话刷新method
   * @param toRefresh
   */
  initToRefresh = (parm) => {
    this.initToRefreshBack = parm;
  };

  /**
   * 倒计时结束-刷新页面
   */
  countOver = () => {
    this.initToRefreshBack();
  };
}
