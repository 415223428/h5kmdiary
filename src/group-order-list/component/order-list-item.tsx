import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { noop, history, _, storage, WMkit, CountDown } from 'wmkit';
import { cache } from 'config';
import { IList } from 'typings/globalType';
import moment from 'moment';

const noneImg = require('../img/none.png');
const GROUPON_STATE = {
  0: '待支付',
  1: '拼团中，已支付',
  2: '拼团成功，已支付',
  3: '拼团失败，金额退回',
  4: '拼团失败，订单未支付'
};
export default class OrderItem extends React.Component<any, any> {
  render() {
    const item = this.props.item;
    const countOver = this.props.countOver;
    const tradeItem = item && item.tradeItems[0] ? item.tradeItems[0] : null;
    const grouponInstance = item && item.grouponInstance;
    //拼团结束时间
    const orderStatus = this._orderStatus(item);
    let endTime;
    let countDown_labelText;
    if (orderStatus == 0) {
      endTime = item.orderTimeOut && moment(item.orderTimeOut);
      countDown_labelText = '支付倒计时:';
    } else {
      endTime = grouponInstance && moment(grouponInstance.endTime);
      countDown_labelText = '拼团倒计时:';
    }
    const serverTime = this.props.serverTime;
    return (
      <div>
        {tradeItem && (
          <div>
            <div 
              className="item-trade"
              style={(this._orderStatus(item) == 0||
                (this._orderStatus(item) == 1 && grouponInstance))?
                { ...styles.countdownHeight } : null
              }
            >
              <div onClick={() => this._handLink(item)}>
                <div className="item-top">
                  <div
                    style={{
                      marginLeft: '0.2rem',
                      fontSize: '0.28rem',
                      flex: 1,
                      overflow: 'hidden',
                      whiteSpace: 'nowrap',
                      textOverflow: 'ellipsis'
                    }}
                  >
                    {item.supplier.storeName}
                  </div>
                  <div>
                    <span className="group-status">
                      {GROUPON_STATE[this._orderStatus(item)]}
                    </span>
                  </div>
                </div>
                <div
                  style={
                    this._orderStatus(item) - 3 < 0
                      ? { ...styles.middle, ...styles.middleBottom }
                      : { ...styles.middle, ...styles.middleBottom  }
                  }
                >
                  <img
                    src={tradeItem.pic ? tradeItem.pic : noneImg}
                    style={styles.img}
                  />
                  <div className="goods">
                    <div className="goods-name">{tradeItem.skuName}</div>
                    <div className="goods-num">x{tradeItem.num}</div>
                    <div>
                      <span className="goods-num">实付款</span>
                      <span className="price">￥</span>
                      <span className="goods-price">
                        {item.tradePrice.totalPrice}
                      </span>
                    </div>
                  </div>
                </div>
              </div>

              <div
                className="bottom"
                onClick={(e) => {
                  e.stopPropagation();
                  this._handLink(item);
                }}
                style={(this._orderStatus(item) == 0||
                  (this._orderStatus(item) == 1 && grouponInstance))?
                  { ...styles.countdownBottom } : null
                }
              >
                {(this._orderStatus(item) == 0 ||
                  (this._orderStatus(item) == 1 && grouponInstance)) && (
                  <div>
                    <span className="time">
                      <CountDown
                        endHandle={countOver}
                        visible={endTime && serverTime}
                        showTimeDays={true}
                        colorStyle={{ color: '#FF4D4D' }}
                        labelText={countDown_labelText}
                        labelTextStyle={{color:'#666',fontSize:'0.26rem',display:'inline-block',marginLeft: '0.2rem'}}
                        timeOffset={moment
                          .duration(endTime.diff(serverTime))
                          .asSeconds()
                          .toFixed(0)}
                        timeStyle={{fontSize:'0.28rem',fontWeight:'bold'}}
                      />
                    </span>
                  </div>
                )}
                {this._orderStatus(item) == 1 &&
                  grouponInstance && (
                    <div className="button">
                      还差{grouponInstance.grouponNum - grouponInstance.joinNum}
                      人&nbsp;邀请参团
                    </div>
                  )}
              </div>
            </div>
            {/* <div className="bot-line" style={{ marginTop: 0 }} /> */}
          </div>
        )}
      </div>
    );
  }

  _handLink = (order) => {
    const orderStatus = this._orderStatus(order);
    // let defaultUrl = history.push(`/order-detail/${order.id}`);
    if (orderStatus == 0) {
      history.push({
        pathname: '/order-list',
        state: {
          status: 'payState-NOT_PAID'
        }
      });
    } else if (orderStatus == 1) {
      history.push(`/group-buy-detail/${order.tradeGroupon.grouponNo}`);
    } else {
      history.push(`/order-detail/${order.id}`);
    }
  };

  /**
   * 拼团状态
   */
  _orderStatus = (order) => {
    let text = 0;

    if (order.tradeGroupon && order.tradeState) {
      let gs = order.tradeGroupon.grouponOrderStatus;
      let ps = order.tradeState.payState;
      // 待支付
      if (gs == 1 && ps == 'NOT_PAID') {
        text = 0;
      }
      // 拼团中，已支付拼团中，已支付
      if (gs == 1 && ps == 'PAID') {
        text = 1;
      }
      // 拼团成功，已支付
      if (gs == 2 && ps == 'PAID') {
        text = 2;
      }
      // 拼团失败，金额退回
      if (gs == 3 && ps == 'PAID') {
        text = 3;
      }
      // 拼团失败，订单未支付
      if (gs == 3 && ps == 'NOT_PAID') {
        text = 4;
      }
    }

    return text;
  };
}

const styles = {
  container: {
    flex: 1,
    paddingLeft: 11,
    paddingRight: 10
  },
  item: {
    marginTop: 13,
    //height: 177,
    paddingLeft: 9,
    paddingRight: 9,
    borderTop: '1.5px solid #000000',
    flexDirection: 'column',
    borderLeft: '1px solid #EBEBEB',
    borderRight: '1px solid #EBEBEB'
  },
  itemTop: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 37,
    borderBottom: '1px solid #EBEBEB'
  },
  shop: {
    fontSize: 15
  },
  groupStatus: {
    fontSize: 14,
    color: '#FF1F4E'
  },
  payStatus: {
    fontSize: 14,
    marginLeft: 5,
    color: '#FF1F4E'
  },
  middle: {
    display: 'flex',
    height: '1.74rem',
    flexDirection: 'row',
    alignItems: 'center'
  },
  middleBottom: {
    borderBottom: '1px solid #DCDEE1'
  },
  img: {
    width: '1.35rem',
    height: '1.35rem',
    minWidth: '1.35rem',
    margin: '0.2rem'
  },
  goods: {
    marginLeft: 11,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 70
  },
  goodsName: {
    height: 28,
    fontSize: 13,
    overflow: 'hidden'
  },
  goodsNum: {
    fontSize: 10
  },
  goodsPrice: {
    fontSize: 15,
    color: '#CB4255'
  },
  bottom: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 44
  },
  countDown: {
    fontSize: 13
  },
  time: {
    fontSize: 13,
    color: '#FF1F4E',
    marginLeft: 10
  },
  button: {
    width: 126,
    height: 28,
    borderRadius: 13,
    display: 'flex',
    backgroundColor: '#F91A53',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 12
  },
  countdownBottom: {
    height: '1.0rem'
  },
  countdownHeight: {
    height: '3.74rem'
  }
} as any;
