import { Fetch } from 'wmkit';

type TResult = { code: string; message: string; context: any };


/**
 * 获取服务时间
 */
export const queryServerTime = () => {
    return Fetch('/system/queryServerTime');
};