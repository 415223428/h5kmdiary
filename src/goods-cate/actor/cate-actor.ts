import { Actor, Action, IMap } from 'plume2'

export default class CateActor extends Actor {
  defaultState() {
    return {
      // 数据源
      cateList: [],
      // 选中的一级分类
      index: 0,
    }
  }

  /**
   * 数据初始化
   * @param state
   * @param cateList
   * @returns {Map<string, V>}
   */
  @Action('cateActor:init')
  cateInit(state : IMap, cateList) {
    return state.set('cateList', cateList)
  }

  /**
   * 更新选中的一级分类
   * @param state
   * @param index
   * @returns {Map<string, number>}
   */
  @Action('cateActor:index')
  index(state : IMap, index: number) {
    return state.set('index', index)
  }
}