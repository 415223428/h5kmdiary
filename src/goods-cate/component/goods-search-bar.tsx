import React from 'react'
import {Link} from 'react-router-dom'
import {history} from 'wmkit'

export default class GoodsSearchBar extends React.Component<any, any> {

  render() {

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div className="input-box">
              <i className="iconfont icon-sousuo"></i>
              <input type="text" placeholder="搜索商品" onClick={() => history.push('/search')}/>
            </div>
            <div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
