import React from "react"
import {Link} from "react-router-dom"
import {Relax} from "plume2"
import {noop} from 'wmkit'
import {IList} from "typings/globalType"

const img = require('./img/cate-img.jpg')
require('../css/style.css')
@Relax
export default class NavSide extends React.Component<any, any> {


  props: {
    relaxProps ?: {
      cateList: IList
      index: number
      changeIndex: Function
    }
    source: string
  }


  static relaxProps = {
    cateList: 'cateList',
    index: 'index',
    changeIndex: noop
  }


  render() {

    const {cateList, index, changeIndex} = this.props.relaxProps

    return (
      <div className="nav-side" style={{height:'calc(100vh - 2.1rem)',marginTop:'.2rem'}}>
        <ul>
          {
            cateList && cateList.toJS().map((cate, i) => {
              return (
                <li className={index === i ? 'cur b-1px-b' : 'b-1px-b'} onClick={() => {
                  changeIndex(i)
                }} key={cate.cateId}>
                  {/* {
                    this.props.source == 'goodsList' ? null : <img src={cate.cateImg ? cate.cateImg : img}/>
                  } */}
                  <div>
                    {
                      index === i ? <img src={require('./img/searchCur.png')} alt="" /> : <div style={{width:".14rem",height:".6rem"}}></div>
                    }
                  </div>
                  <div className="div-width" 
                  >
                    <span
                      style={{fontSize:'.28rem', fontFamily:'PingFang-SC-Medium'}} 
                      className={`${this.props.source == 'goodsList' ? 'min5 nav-side-text' : 'word'}`}>{cate.cateName}</span>
                  </div>
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
}
