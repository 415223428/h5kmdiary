import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';
const goods=require('./img/goods-logo.png')
@Relax
export default class MenuList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      cateList: IList;
      index: number;
    };
    handleClick: Function;
  };

  static relaxProps = {
    cateList: 'cateList',
    index: 'index'
  };

  render() {
    const { cateList, index } = this.props.relaxProps;

    let cate = cateList && cateList.get(index);

    if (!cate) {
      return null;
    }

    cate = cate.toJS();

    const handleClick = this.props.handleClick;

    return (
      <div className="menu-list address-box" style={{ background:'#fff',margin:".2rem" ,width:"5.5rem",height: 'calc(100vh - 2.1rem)'}}>
        <div className="menu-content b-1px-t line-before">
          {/** 一级分类名 **/}
          {/* <a
            href="javascript:;"
            className="new-btn"
            onClick={() => handleClick(cate.cateId, cate.cateName)}
          >
            {cate.cateName}
          </a> */}

          {// 二级分类
            cate.goodsCateList.map((level2Cate, level2Index) => {
              return (
                <dl className="menu-box" key={level2Cate.cateId}>
                  <dt
                  style={{background:'#fff'}}
                    onClick={() =>
                      handleClick(level2Cate.cateId, level2Cate.cateName)
                    }
                  >
                    {level2Cate.cateName}
                    <i style={{color:'#999'}} className="iconfont icon-jiantou1" />
                  </dt>
                  <div className="div-hr"></div>
                  <div className="div-dd" style={{flexWrap:'wrap'}}>
                    {// 三级分类
                      level2Cate.goodsCateList.map((level3Cate, level3Index) => {
                        return (
                          <div className="div-box"
                            style={{color:'#333',display:'flex',flexDirection:'column',opacity:1,height:'100%',
                                    alignItems:'center',justifyContent:'flex-start',background:'none'}}
                            key={level3Cate.cateId}
                            onClick={() =>
                              handleClick(level3Cate.cateId, level3Cate.cateName)
                            }
                          >
                            <img src={level3Cate.cateImg?level3Cate.cateImg:goods} alt="" 
                                  style={{width:'1.2rem',height:'1.2rem',marginBottom: '.1rem'}}/>
                            <span style={{fontSize:'0.24rem',color:' #333',textAlign:'center',marginBottom: '.15rem',width:'1.3rem'}}>
                              {level3Cate.cateName}</span>
                          </div>
                        );
                      })}
                  </div>
                </dl>
              );
            })}
        </div>
      </div>
    );
  }
}
