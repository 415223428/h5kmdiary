import { Action, Actor, IMap } from 'plume2';
import { IList } from '../../../../typings/globalType';

export default class CenterMemberActor extends Actor {
  defaultState() {
    return {
      userInfo: {}, //用户信息
      gradeList: [], //等级列表
      hotExchange: [], //积分兑换
      nextGradeInfo: {}, //下一个等级
      isLastOne: false, //是否是最后一个
      notGetGradeList: [], //当前后面所有的等级。不与上面数据合并
      nowPossessGradeInfo: {}, //当前的等级信息
      pointsAvailable: 0, //积分 值
      pointsIsOpen: false, //积分是否打开
      //商品评价相关信息是否展示
      isShow: false
    };
  }

  /**
   * 积分值
   */
  @Action('userInfo: pointsAvailable')
  pointsAvailable(state, pointsAvailable) {
    return state.set('pointsAvailable', pointsAvailable);
  }

  /**
   * 等级类表
   */
  @Action('member-center: gradeList')
  gradeList(state: IMap, gradeList: IList) {
    return state.set('gradeList', gradeList);
  }

  @Action('member-center: nowPossessGradeInfo')
  nowPossessGradeInfo(state: IMap, nowPossessGradeInfo: IList) {
    return state.set('nowPossessGradeInfo', nowPossessGradeInfo);
  }

  /**
   * 用户数据
   */
  @Action('member-center: userInfo')
  userInfo(state: IMap, userInfo: IMap) {
    return state.set('userInfo', userInfo);
  }

  /**
   * 积分兑换
   */
  @Action('member-center: hotExchange')
  hotExchange(state: IMap, hotExchange: IMap) {
    return state.set('hotExchange', hotExchange);
  }

  /**
   * 下一个等级数据
   */
  @Action('member-center: nextGradeInfo')
  nextGradeInfo(state: IMap, nextGradeInfo: IMap) {
    return state.set('nextGradeInfo', nextGradeInfo);
  }

  /**
   * 是不是最高等级
   */
  @Action('member-center: isLastOne')
  isLastOne(state: IMap, isLastOne: boolean) {
    return state.set('isLastOne', isLastOne);
  }

  /**
   * 当前后面所有的等级数据
   */
  @Action('member-center: notGetGradeList')
  nextGradeList(state: IMap, notGetGradeList: IList) {
    return state.set('notGetGradeList', notGetGradeList);
  }

  /**
   * 是否成长值关闭了
   */
  @Action('userInfo:pointsIsOpen')
  pointsIsOpen(state: IMap) {
    return state.set('pointsIsOpen', true);
  }

  @Action('member-center:isShow')
  setIsShow(state: IMap, flag: boolean) {
    return state.set('isShow', flag);
  }
}
