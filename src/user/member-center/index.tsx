import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import CenterMember from './component/center-member';
import './css/style.css';
import LevelPower from './component/level-power';
import GoodsList from './component/goods-list';
import PointsEnter from './component/points-enter';
import MemberFooter from '../../member-footer';
import HotExchange from './component/hot-exchange';

/**
 * 会员中心
 */
@StoreProvider(AppStore)
export default class MemberCenter extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
    this.store.getPointsInfo();
    this.store.basicRules();
  }

  render() {
    const pointsIsOpen = this.store.state().get('pointsIsOpen');

    return (
      <div className="my-center">
        {/*我的*/}
        <CenterMember />
        {/*等级会员权益*/}
        <LevelPower />
        {/*积分商城入口*/}
        {/* {pointsIsOpen && <PointsEnter />} */}
        {/* 热门兑换 */}
        {/* {pointsIsOpen && <HotExchange />} */}
        {/*会员最爱买*/}
        {/* <GoodsList /> */}
        {/*导航栏*/}
        {/* <MemberFooter path={'/member-center'} pointsIsOpen={pointsIsOpen}/> */}
      </div>
    );
  }
}
