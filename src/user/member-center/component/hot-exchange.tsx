import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { WMImage, history } from 'wmkit';
import { IList } from 'typings/globalType';
import 'swiper/dist/css/swiper.min.css';
@Relax
export default class HotExchange extends React.Component<any, any> {
  props: {
    relaxProps?: {
      hotExchange: IList;
    };
  };

  static relaxProps = {
    hotExchange: 'hotExchange'
  };

  render() {
    const { hotExchange } = this.props.relaxProps;
    return (
      <div className="member-integral-exchange">
        <div className="up-box">
          <img src={require('./img/hot-left.png')} alt="" />
          <span>热门兑换</span>
          <img src={require('./img/hot-right.png')} alt="" />
        </div>
        <div className="integral-list">
          <div className="swiper-container">
            <div className="swiper-wrapper">
              {hotExchange.map((val) => {
                return (
                  <div
                    className="swiper-slide"
                    key={val.get('pointsGoodsId')}
                    onClick={() =>
                      history.push({
                        pathname:
                          '/goods-detail/' +
                          val.get('goodsInfo').get('goodsInfoId'),
                        state: { pointsGoodsId: val.get('pointsGoodsId') }
                      })
                    }
                  >
                    <div
                      className="product-info"
                      onClick={() =>
                        history.push(
                          '/goods-detail/' +
                            val.get('goodsInfo').get('goodsInfoId')
                        )
                      }
                    >
                      <div className="picture">
                        <WMImage
                          src={
                            val.get('goodsInfo').get('goodsInfoImg') ||
                            val.get('goods').get('goodsImg')
                          }
                          width="100%"
                          height="100%"
                        />
                      </div>
                      <span className="text1">
                        {val.get('goodsInfo').get('goodsInfoName')}
                      </span>
                      <span className="text2">{val.get('points')}积分</span>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
