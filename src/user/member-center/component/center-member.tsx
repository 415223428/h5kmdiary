import React from 'react';
import { IMap, Relax } from 'plume2';
import { history, ProgressBar } from 'wmkit';

const defaultImg = require('../../../../web_modules/images/default-headImg.png');
const background = require('../component/img/background.png');
const imgHeight = (window.innerWidth as any).toString() / 2 + 'px';

@Relax
export default class CenterMember extends React.Component<any, any> {
  props: {
    relaxProps?: {
      userInfo: IMap;
      nextGradeInfo: IMap;
      isLastOne: boolean;
      pointsAvailable: any;
      pointsIsOpen: boolean;
    };
  };

  static relaxProps = {
    userInfo: 'userInfo',
    isLastOne: 'isLastOne',
    nextGradeInfo: 'nextGradeInfo',
    pointsAvailable: 'pointsAvailable',
    pointsIsOpen: 'pointsIsOpen'
  };

  render() {
    const {
      userInfo,
      nextGradeInfo,
      isLastOne,
      pointsAvailable,
      pointsIsOpen
    } = this.props.relaxProps;
    let headImg = userInfo.get('headImg');
    return (
      <div className="user-top" style={{background: `url(${background}) no-repeat top center`,backgroundSize: 'cover'}}>
        <div className="box member-box" style={{position:'static'}}>
          <div className="user-box">
            <img src={ headImg?headImg:defaultImg} style={{width:'1.2rem',height:'1.2rem'}}/>
          </div>
          <div className="user-context" style={{display:'flex',alignItems:'center'}}>
            <div className="name" style={{color:'#fff',fontSize:'0.36rem',fontWeight:'bold'}}>
              {userInfo.get('customerName')}
              {/* <label className="levelText">
                <img src={userInfo.get('rankBadgeImg') || defaultImg} alt="" />
                <span>{userInfo.get('customerLevelName')}</span>
              </label> */}
            </div>
            <p
              className="growthValue" style={{height:'1.2rem',top:'20%'}}
            >
              <span onClick={() => history.push({ pathname: '/growth-value' })} style={{marginBottom:'0.3rem',paddingRight:'0'}}>
                成长值:{userInfo.get('customerGrowthValue') || 0}
                <i className="icon iconfont icon-jiantou"></i>
              </span>
              {pointsIsOpen && (
                <span onClick={() => history.push({ pathname: '/user-integral' })} style={{paddingRight:'0'}}>
                  积分值:{pointsAvailable} 
                <i className="icon iconfont icon-jiantou"></i>
                </span>
              )}
            </p>
            
          </div>
        </div>
        <div className="progress-box">
          <ProgressBar
            percent={
              nextGradeInfo.get('growthValue') -
                userInfo.get('customerGrowthValue') <
              0
                ? 99
                : (((userInfo.get('customerGrowthValue') /
                    nextGradeInfo.get('growthValue')) *
                    100) as any)
            }
            barStyle={{border:'0.1rem solid #F7BB58',borderRadius:'0.1rem'}}
            style={{background:'rgba(255,240,214,0.3)',borderRadius:'0.1rem',height:'0.1rem'}}
          />
          <div className="progress-text" style={{fontSize:'0.24rem'}}>
            <span className="left" style={{color:'#FFF0D6'}}>
              当前成长值&nbsp;<span className="" style={{color:'#FFDB29'}}>
                {userInfo.get('customerGrowthValue')}
              </span>
            </span>
            {!isLastOne ? (
              <span className="right" style={{fontSize:'0.24rem',color:'#FFF0D6'}}>
                {/* 距离&nbsp;{nextGradeInfo.get('customerLevelName')}&nbsp;等级还差&nbsp;{nextGradeInfo.get(
                  'growthValue'
                ) -
                  userInfo.get('customerGrowthValue') <
                0
                  ? 1
                  : nextGradeInfo.get('growthValue') -
                    userInfo.get('customerGrowthValue')} */}
                    您已到达<span style={{color:'#FFDB29'}}>{userInfo.get('customerLevelName')}</span>
              </span>
            ) : (
              <span className="right">您已达到最高等级</span>
            )}
          </div>
        </div>
      </div>
    );
  }
}
