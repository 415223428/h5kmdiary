import React from 'react';
import { _, history, WMImage } from 'wmkit';
import { GoodsNum, MarketingLabel } from 'biz';
import { Relax } from 'plume2';

@Relax
export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);

    this.state = props;
  }

  props: {
    relaxProps?: {
      isShow: boolean;
    };
    goodsItem: any;
    listView: boolean;
  };

  static relaxProps = {
    isShow: 'isShow'
  };

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const {isShow} = this.props.relaxProps;
    const { goodsItem, listView } = this.state;
    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 起订量
    const count = goodsInfo.get('count') || 0;
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;
    // 营销标签
    const marketingLabels = goodsInfo.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsInfo.get('couponLabels');

    let containerClass = listView
      ? invalid
        ? 'goods-list invalid-goods'
        : 'goods-list'
      : invalid
        ? 'goods-box invalid-goods'
        : 'goods-box';

    // 社交电商相关内容显示与否
    const social = goodsInfo.get('distributionGoodsAudit') == 2 ? true : false;
    const distributionCommission = goodsInfo.get('distributionCommission');
    const marketPrice = goodsInfo.get('marketPrice');
    //禁用分享赚
    const socialDisabled = false;

    //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
    //好评率
    let favorableRate = '100';
    if (goodsInfo.get('goodsEvaluateNum') && goodsInfo.get('goodsEvaluateNum') != 0){
      favorableRate = _.mul(_.div(goodsInfo.get('goodsFavorableCommentNum'),goodsInfo.get('goodsEvaluateNum')),100).toFixed(0);
    }

    //评论数
    let evaluateNum = '暂无';
    const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
    if (goodsEvaluateNum) {
      if (goodsEvaluateNum<10000){
        evaluateNum = goodsEvaluateNum;
      } else {
        const i = _.div(goodsEvaluateNum,10000).toFixed(1);
        evaluateNum =  i+'万+';
      }
    }

    //销量
    let salesNum='暂无';
    const goodsSalesNum = goodsInfo.get('goodsSalesNum');
    if (goodsSalesNum){
      if (goodsSalesNum<10000){
        salesNum = goodsSalesNum;
      } else {
        const i = _.div(goodsSalesNum,10000).toFixed(1);
        salesNum =  i+'万+';
      }
    }
    // 预估点数
    let pointNum = goodsInfo.get('kmPointsValue');
    //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆

    return (
      <div
        key={id}
        className={containerClass}
        onClick={() => history.push('/goods-detail/' + goodsItem.get('id'))}
      >
        <div className="img-box">
          <WMImage
            src={goodsInfo.get('goodsInfoImg')}
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail b-1px-b">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>

          {/* 评价 */}
          {
            isShow?
              (
                <div className="goods-evaluate">
                  <span className="goods-evaluate-spn">{salesNum}销量</span>
                  <span className="goods-evaluate-spn mar-lr-28">{evaluateNum}评价</span>
                  <span className="goods-evaluate-spn">{favorableRate}%好评</span>
                  {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
                </div>
              )
              :
              (
                <div className="goods-evaluate">
                  <span className="goods-evaluate-spn">{salesNum}销量</span>
                </div>
              )
          }

          <div className="marketing">
            {goodsInfo.get('companyType') == 0 && (
              <div className="self-sales">自营</div>
            )}
            {!social &&
              (marketingLabels || couponLabels) && (
                <MarketingLabel
                  marketingLabels={marketingLabels}
                  couponLabels={couponLabels}
                />
              )}
          </div>

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {social
                  ? _.addZero(marketPrice)
                  : this._calShowPrice(goodsItem, buyCount)}
              </span>
              {invalid && <div className="out-stock">缺货</div>}
            </div>

            <GoodsNum
              value={buyCount}
              max={stock}
              disableNumberInput={invalid}
              goodsInfoId={id}
              onAfterClick={this._afterChangeNum}
            />
          </div>
        </div>
      </div>
    );
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (
      goodsInfo.get('priceType') === 1 &&
      goodsItem.getIn(['_otherProps', 'goodsIntervalPrices'])
    ) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
  };
}
