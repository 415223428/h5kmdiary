import React, { Component } from 'react';

import { ListView } from 'wmkit';
import GoodsItem from './goods-item';
import { fromJS } from 'immutable';

export default class GoodsList extends Component<any, any> {
  render() {
    return (
      <ListView
        className="view-box"
        isPagination={false}
        style={{ height: 'auto' }}
        pageSize={20}
        params={{
          keywords: '',
          brandIds: [],
          sortFlag: 0,
          esGoodsInfoDTOList: []
        }}
        url="/goods/skus"
        otherProps={['goodsIntervalPrices']}
        renderRow={(item: any) => {
          return (
            <GoodsItem
              goodsItem={fromJS(item)}
              listView={false}
              key={item.id}
            />
          );
        }}
      />
    );
  }
}
