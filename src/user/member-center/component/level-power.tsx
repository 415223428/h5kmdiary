import React from 'react';
import {Relax} from 'plume2';
import {Link} from 'react-router-dom';
import {history} from 'wmkit';
import {IList, IMap} from 'typings/globalType';

@Relax
export default class LevelPower extends React.Component<any, any> {
    props: {
        relaxProps?: {
            nowPossessGradeInfo: IMap;
            notGetGradeList: IList;
        };
    };

    static relaxProps = {
        nowPossessGradeInfo: 'nowPossessGradeInfo',
        notGetGradeList: 'notGetGradeList'
    };

    render() {
        const {nowPossessGradeInfo, notGetGradeList} = this.props.relaxProps;
    //   if( nowPossessGradeInfo.get('customerLevelRightsVOS') && nowPossessGradeInfo.get('customerLevelRightsVOS').size == 0 && notGetGradeList.size == 0) {
    //     return null;
    //   }
        return (
            <div className="levelPowerBox">
                <div className="levelPower" 
                    style={{width:'7.1rem',height:'2.7rem',background:'#fff',boxShadow:'0 0.06rem 0.2rem 0 rgba(0, 0, 0, 0.04)',margin:'0.3rem auto',padding:'0'}}
                >
                    <div style={{height:'0.88rem', display:'flex',justifyContent:'center',alignItems:'center',borderBottom:'1px solid #e6e6e6'}}>
                        <img src={require('./img/level.png')} alt="" style={{width:'0.3rem',height:'0.28rem',marginRight:'0.12rem'}} />
                        <p style={{color:'#000',fontSize:'0.28rem',fontWeight:'bold'}}>会员等级权益</p>
                    </div>
                    <ul className="levelContents" style={{display:'flex',alignItems:'center',justifyContent:'space-evenly',marginTop:'0.1rem',height:'1.6rem'}}>
                        <li
                            onClick={() =>
                                history.push(
                                    `/class-equity/${nowPossessGradeInfo.get(
                                        'customerLevelId'
                                    )}`
                                )
                            }
                            style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center',height:'100%'}}
                        >
                            <img src={require('./img/service.png')} alt="" style={{width:'0.7rem',height:'0.7rem',position:'static',transform:'none',marginBottom:'0.2rem'}} />
                            <span style={{fontSize:'0.26rem',color:'#333'}}>专属客服</span>
                        </li>
                        <li
                            onClick={() =>
                                history.push(
                                    `/class-equity/${nowPossessGradeInfo.get(
                                        'customerLevelId'
                                    )}`
                                )
                            }
                            style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center',height:'100%'}}
                        >
                            <img src={require('./img/medal.png')} alt="" style={{width:'0.7rem',height:'0.7rem',position:'static',transform:'none',marginBottom:'0.2rem'}} />
                            <span style={{fontSize:'0.26rem',color:'#333'}}>等级徽章</span>
                        </li>
                        <li
                            onClick={() =>
                                history.push(
                                    `/class-equity/${nowPossessGradeInfo.get(
                                        'customerLevelId'
                                    )}`
                                )
                            }
                            style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center',height:'100%'}}
                        >
                            <img src={require('./img/discount.png')} alt="" style={{width:'0.7rem',height:'0.7rem',position:'static',transform:'none',marginBottom:'0.2rem'}} />
                            <span style={{fontSize:'0.26rem',color:'#333'}}>会员专属折扣</span>
                        </li>
                        {/* {nowPossessGradeInfo.get('customerLevelRightsVOS') &&
                        nowPossessGradeInfo.get('customerLevelRightsVOS').map((value) => {
                            return (
                                <li
                                    key={value.get('rightsId')}
                                    onClick={() =>
                                        history.push(
                                            `/class-equity/${nowPossessGradeInfo.get(
                                                'customerLevelId'
                                            )}`
                                        )
                                    }
                                >
                                    <img src={value.get('rightsLogo')} alt=""/>
                                    <p> {value.get('rightsName')}</p>
                                </li>
                            );
                        })}

                        {notGetGradeList.map((value) => {
                            return (
                                <li
                                    key={value.get('rightsId')}
                                    className="dropShadow"
                                    onClick={() =>
                                        history.push(
                                            `/class-equity/${value.get('customerLevelId')}`
                                        )
                                    }
                                >
                                    <img src={value.get('rightsLogo')} alt=""/>
                                    <p>{value.get('rightsName')}</p>
                                </li>
                            );
                        })} */}
                        {/*等级图标为灰色时添加className="dropShadow"*/}
                    </ul>
                </div>
            </div>
        );
    }
}
