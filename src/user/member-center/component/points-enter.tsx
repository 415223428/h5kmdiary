import React from 'react';
import { history } from 'wmkit';

export default class PointsEnter extends React.Component<any, any> {
  render() {
    return (
      <div className="points-mall">
        <img
          src={require('./img/points-mall.jpg')}
          onClick={() => history.push('/points-mall')}
        />
      </div>
    );
  }
}

