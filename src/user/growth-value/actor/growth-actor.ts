import { Actor, Action, IMap } from 'plume2';
import { fromJS } from 'immutable';
import {IList} from "../../../../typings/globalType";

export default class GrowthActor extends Actor {
  defaultState() {
    return {
      layerVisible: false,
      isLastOne:false,
      levelInfo:{},
      basicRules:{}

    };
  }

  @Action('change: layerVisible')
  changeLayerVisible(state) {
    return state.set('layerVisible', !state.get('layerVisible'));
  }


  @Action('growth-value: basicRules')
  basicRules(state:IMap,basicRules:IMap) {
    return state.set('basicRules', basicRules);
  }





  /**
   * 是不是最高等级
   */
  @Action('growth-value: isLastOne')
  isLastOne(state:IMap) {
    return state.set('isLastOne', true);
  }

  /**
   * 等级列表
   */
  @Action('growth-value: levelInfo')
  levelInfo(state:IMap,levelInfo:IMap) {
    return state.set('levelInfo', levelInfo);
  }


}