import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';

import './css/style.css';
import CenterMember from './component/center-member';
import ValueList from './component/value-list';
import ValueLayer from './component/value-layer';

/**
 * 成长值明细
 */
@StoreProvider(AppStore)
export default class GrowthValue extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    return (
      <div className="my-center">
        {/*我的*/}
        <CenterMember />

        {/*成长值明细*/}
        <ValueList />

        {/*成长值明细规则*/}
        <ValueLayer />
      </div>
    );
  }
}
