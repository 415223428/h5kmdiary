import React, { Fragment } from 'react';
import { ListView, Blank } from 'wmkit';
import moment from 'moment';
import { Const } from 'config';

// type 0:普通方式 1:订单相关
let serviceTypeName = {
  0: { desc: '签到', type: 0 },
  1: { desc: '注册', type: 0 },
  2: { desc: '分享商品', type: 0 },
  3: { desc: '分享注册', type: 0 },
  4: { desc: '分享购买', type: 0 },
  5: { desc: '评论商品', type: 0 },
  6: { desc: '晒单', type: 0 },
  7: { desc: '完善基本信息', type: 0 },
  8: { desc: '绑定微信', type: 0 },
  9: { desc: '添加收货地址', type: 0 },
  10: { desc: '关注店铺', type: 0 },
  11: { desc: '订单完成', type: 1 }
};

export default class ValueList extends React.Component<any, any> {
  render() {
    return (
      <ul className="valueList b-1px-t">
        <ListView
          url="/customer/queryToGrowthValue"
          // params={{pageSize:10, pageNum: 0,}}
          style={{ height: window.innerHeight - 158 }}
          renderRow={this._InfoRow}
          renderEmpty={() => (
            <Blank
              img={require('../img/list-none.png')}
              content="您暂时还没有明细记录哟~"
              // isToGoodsList={true}
            />
          )}
        />
      </ul>
    );
  }

  /**
   * 列表数据
   */
  _InfoRow = (storeInfo, index) => {
    return (
      <Fragment>
        <li className="b-1px-b" key={index}>
          <div>
            <p className="title">
              {serviceTypeName[storeInfo.serviceType].desc}{' '}
            </p>
            {this._showGrowValDesc(storeInfo, storeInfo.serviceType)}
            <p className="time">
              {moment(storeInfo.opTime).format(Const.SECONDS_FORMAT)}
            </p>
          </div>
          <div className="add-text">+{storeInfo.growthValue || 0}</div>
        </li>
      </Fragment>
    );
  };

  _showGrowValDesc = (content, serviceType) => {
    const info = serviceTypeName[serviceType];
    if (info.type == 0) {
      return null;
    } else if (info.type == 1) {
      return <p className="order-rel">订单编号:{content.tradeNo}</p>;
    }
  };
}
