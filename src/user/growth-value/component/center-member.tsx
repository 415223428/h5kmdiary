import React from 'react';
import { Relax } from 'plume2';

import { noop, ProgressBar } from 'wmkit';
import { IMap } from 'typings/globalType';

const background = require('../img/background.png');
@Relax
export default class CenterMember extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeLayer: Function;
      isLastOne: boolean;
      levelInfo: IMap;
    };
  };

  static relaxProps = {
    changeLayer: noop,
    levelInfo: 'levelInfo',
    isLastOne: 'isLastOne'
  };

  render() {
    const { changeLayer, levelInfo, isLastOne } = this.props.relaxProps;
    return (
      <div className="user-top use-top-fixed" style={{background:`url(${background}) no-repeat top center`,backgroundSize:'cover'}}>
        <div className="box member-box">
          <div className="growth-context" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}> 
            <span style={{color:'#FFE3B2',fontSize:'0.6rem'}}>{levelInfo.get('nowHaveGrowthValue')}</span>
            <span style={{fontSize:'0.28rem',color:'#FFF0D6',marginTop:'0.3rem'}}>当前成长值</span>
          </div>
        </div>
        <div className="progress-box">
          <ProgressBar
            percent={
              levelInfo.get('needGrowthValue') < 0
                ? 99
                : (((levelInfo.get('nowHaveGrowthValue') /
                    levelInfo.get('nextGrowthValue')) *
                    100) as any)
            }
            barStyle={{border:'0.1rem solid #F7BB58',borderRadius:'0.1rem'}}
            style={{background:'rgba(255,240,214,0.3)',borderRadius:'0.1rem',height:'0.2rem'}}
          />
          <div className="progress-text" style={{fontSize:'0.24rem'}}>
            <span className="left" style={{color:'#FFF0D6',fontSize:'0.24rem',transform:'scale(1)'}}>
              当前等级：&nbsp;<span className="" style={{color:'#FFDB29'}}>
                {levelInfo.get('atPresentLevelName')}
              </span>
            </span>
            {!isLastOne ? (
              <span className="right" style={{color:'#FFF0D6',fontSize:'0.24rem'}}>
                距离&nbsp;{levelInfo.get('nextLevelName')}&nbsp;等级还差&nbsp;{levelInfo.get(
                  'needGrowthValue'
                ) < 0
                  ? 1
                  : levelInfo.get('needGrowthValue')}
              </span>
            ) : (
              <span  style={{color:'#FFF0D6',fontSize:'0.24rem'}}>您已达到最高等级</span>
            )}
          </div>
        </div>
        <div className="growth-rules" onClick={() => changeLayer()}>
          <span>规则</span>
          <i className="icon iconfont icon-jiantou"></i>
        </div>
      </div>
    );
  }
}
