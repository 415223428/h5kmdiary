import {Action, Actor, IMap} from 'plume2';
import {fromJS} from 'immutable';
import {IList} from "../../../../typings/globalType";

export default class EquityActor extends Actor {
  defaultState() {
    return {
      // 等级数据
      levelList: [],
      //当前的等级
      currentLevel: 1,
      //列表数据
      equityList: [],

      isLastOne:false,//是否是最高等级
      gradeList:[],
      levelInfo:{},//等级信息栏
      atPresentEquityNum :0,
      isDomComplete:false,
      // atPresentEquity:[]//权益列表
    };
  }

  /**
   * 获取等级数据
   */
  @Action('get: levelData')
  changeCateMask(state, val) {
    return state.set('levelList', fromJS(val));
  }

  /**
   * 当前等级
   */
  @Action('get: currentData')
  currentData(state, val) {
    return state.set('currentLevel', val);
  }

  /**
   * 列表数据
   */
  @Action('get: equityList')
  equityList(state, val) {
    return state.set('equityList', fromJS(val));
  }



  /**
   * 是不是最高等级
   */
  @Action('equity: isLastOne')
  isLastOne(state:IMap) {
    return state.set('isLastOne', true);
  }

  /**
   * 等级列表
   */
  @Action('equity: gradeList')
  setGradeList(state:IMap,gradeList:IList) {
    return state.set('gradeList', gradeList);
  }

  /**
   * 等级列表
   */
  @Action('equity: levelInfo')
  levelInfo(state:IMap,levelInfo:IMap) {
    return state.set('levelInfo', levelInfo);
  }

  /**
   * 当前轮播是第几个
   */
  @Action('equity: atPresentEquityNum')
  atPresentEquityNum(state:IMap,atPresentEquityNum:number) {
    return state.set('atPresentEquityNum', atPresentEquityNum);
  }

  /**
   * 当前轮播是第几个
   */
  @Action('equity: isDomComplete')
  isDomComplete(state:IMap) {
    return state.set('isDomComplete', true);
  }






}
