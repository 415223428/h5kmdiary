import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import CarouselHead from './component/carousel-head';
import EquityList from './component/equity-list';
const style = require('./css/style.css');
import { Loading, Blank } from 'wmkit';

@StoreProvider(AppStore)
export default class ChoseService extends React.Component<any, any> {
  store: AppStore;

  constructor(props){
    super(props);


  }



  componentWillMount(){


  }

  componentDidMount() {
    const {id} = this.props.match.params;
    this.store.query(id);


  }

  render() {
    return (
      <div className="equity-main">
        {
          !this.store.state().get("isDomComplete") &&  <Loading/>
        }
        <CarouselHead />
        <EquityList />
      </div>
    );
  }
}
