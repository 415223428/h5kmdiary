import { Store } from 'plume2';
import EquityActor from './actor/equity-actor';
// import * as webapi from './webapi';
import Swiper from 'swiper/dist/js/swiper.js';
import 'swiper/dist/css/swiper.min.css';
import { getRightsList, getUserInfo } from '../member-center/webapi';
import { fromJS } from 'immutable';

export default class AppStore extends Store {
  bindActor() {
    return [new EquityActor()];
  }

  query = async (id: string) => {
    const res: object = await Promise.all([getRightsList(), getUserInfo()]);
    let gradeList = res[0].context.customerLevelVOList;
    let userInfo = res[1].context;

    this.transaction(() => {
      this.dispatch('equity: gradeList', fromJS(gradeList));
    });

    //找到当前是第几个
    let index = gradeList.findIndex((item) => {
      return userInfo.customerLevelId == item.customerLevelId;
    });

    let levelInfo = {};
    if (index + 1 == gradeList.length) {
      //如果相等 那他就是最后一个了
      this.dispatch('equity: isLastOne');
       levelInfo = {
        atPresentLevelName: userInfo.customerLevelName,
      };
    } else {
      //组装一下数据
       levelInfo = {
        atPresentLevelName: userInfo.customerLevelName,
        nextLevelName: gradeList[index + 1].customerLevelName,
        needGrowthValue:
          gradeList[index + 1].growthValue - userInfo.customerGrowthValue
      };
    }

    this.dispatch('equity: levelInfo', fromJS(levelInfo));
    this.init(gradeList, id);
  };

  /**
   * 初始化，查询并设置平台客服列表
   */
  init = async (gradeList: any, id: string) => {
    //找到当前是第几个
    let index = gradeList.findIndex((item) => {
      return id == item.customerLevelId;
    });
    //获取等级数据
    const that = this;
    setTimeout(function() {
      new Swiper('.swiper-container', {
        initialSlide: index,
        slidesPerView: 4,
        spaceBetween: 20,
        centeredSlides: true,
        // slideToClickedSlide: true,
        on: {
          init: function() {
            that.dispatch('equity: isDomComplete');
          },
          slideChange: function() {
            that.dispatch('equity: atPresentEquityNum', this.activeIndex);
          }
        }
      });
    }, 1000);
  };
}
