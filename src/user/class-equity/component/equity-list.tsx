import React, { Fragment } from 'react';
import { history, _, Alert, Blank } from 'wmkit';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';
const noneImg = require('../img/none.png');

@Relax
export default class EquityList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      gradeList: IList;
      atPresentEquityNum: number;
    };
  };

  static relaxProps = {
    atPresentEquityNum: 'atPresentEquityNum',
    gradeList: 'gradeList'
  };

  render() {
    const { gradeList, atPresentEquityNum } = this.props.relaxProps;
    if (
      gradeList.get(atPresentEquityNum) &&
      gradeList.get(atPresentEquityNum).get('customerLevelRightsVOS').size == 0
    ) {
      return (
        <div style={{ marginTop: '3.3rem' }}>
          <Blank img={noneImg} content="该等级还有没权益哟～" />
        </div>
      );
    }
    return (
      <ul className="equity-list">
        {gradeList.get(atPresentEquityNum) &&
          gradeList
            .get(atPresentEquityNum)
            .get('customerLevelRightsVOS')
            .map((v, i) => {
              return (
                <li key={i}>
                  <div className="up-div">
                    <img className="left-icon" src={v.get('rightsLogo')} />
                    <span className="right-title">{v.get('rightsName')}</span>
                  </div>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: v.get('rightsDescription')
                    }}
                  />

                  {/*<span className="down-text">{v.text}</span>*/}
                </li>
              );
            })}
      </ul>
    );
  }
}
