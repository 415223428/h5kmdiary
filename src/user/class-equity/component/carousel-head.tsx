import React from 'react';
import { history, _, Alert } from 'wmkit';
import { Relax } from 'plume2';
import { IList, IMap } from 'typings/globalType';
import { debug } from 'util';
@Relax
export default class EquityList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      levelInfo: IMap;
      gradeList: IList;
      isLastOne: boolean;
      atPresentEquityNum: number;
    };
  };

  static relaxProps = {
    gradeList: 'gradeList',
    levelInfo: 'levelInfo',
    isLastOne: 'isLastOne',
    atPresentEquityNum: 'atPresentEquityNum'
  };

  render() {
    const {
      isLastOne,
      levelInfo,
      gradeList,
      atPresentEquityNum
    } = this.props.relaxProps;
    return (
      <div className="carousel-box">
        <div className="carousel-head">
          <div className="swiper-container equity-container">
            <div className="swiper-wrapper equity-wrapper">
              {gradeList.map((value, index) => {
                return (
                  <div
                    className="swiper-slide equity-slide"
                    key={value.get('customerLevelId')}
                  >
                    <div
                      className={
                        atPresentEquityNum == index
                          ? 'div-box select-div-box'
                          : 'div-box'
                      }
                    >
                      <img src={value.get('rankBadgeImg')} />
                      <span>{value.get('customerLevelName')}</span>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="carousel-bottom">
          <span className="current-grade">
            当前等级为<span className="red-font">
              {levelInfo.get('atPresentLevelName')}
            </span>
          </span>
          {!isLastOne && (
            <span className="next-grade">
              距离下个等级<span
                className="red-font"
                style={{ color: '#cea664' }}
              >
                {levelInfo.get('nextLevelName')}
              </span>还需要<span
                className="red-font"
                style={{ color: '#cea664' }}
              >
                {levelInfo.get('needGrowthValue') > 0
                  ? levelInfo.get('needGrowthValue')
                  : 1}
              </span>成长值
            </span>
          )}
        </div>
      </div>
    );
  }
}
