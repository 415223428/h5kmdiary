/**
 * Created by feitingting on 2017/7/20.
 */
import { Store, IOptions } from 'plume2';
import * as webapi from './webapi';
import { Alert } from 'wmkit';
import SafePassNextActor from './actor/safepass-next-actor';
import { cache } from 'config';
import { history, WMkit } from 'wmkit';

export default class AppStore extends Store {
  bindActor() {
    return [new SafePassNextActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  init = (forget) => {
    this.dispatch('paypassnext:init', forget);
  };

  getNewPass = (pass: string) => {
    this.dispatch('paypassnext:newpass', pass);
  };

  showPwd = () => {
    this.dispatch('paypassnext:showpass', this.state().get('isShowpwd'));
  };

  updatePass = async () => {
    const newpass = this.state().get('password');
    const vertiCode = localStorage.getItem(cache.PAY_CODE);
    const customerId = localStorage.getItem(cache.CUSTOMER_ID);
    if (WMkit.testPass(newpass)) {
      const { code, message, context } = await webapi.updatePass(
        customerId,
        vertiCode,
        newpass,
        this.state().get('forget')
      );
      if (code == 'K-000000') {
        Alert({
          text: '设置成功！',
          time: 3000
        });
        setTimeout(() => {
          history.push({
            pathname: '/user-safe'
          });
        }, 1000);
      } else {
        Alert({
          text: message,
          time: 3000
        });
      }
    }
  };
}
