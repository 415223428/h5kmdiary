import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const submit=require('./img/submit.png')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class UserSafePassword extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    if (__DEV__) {
    }
    //判断是忘记密码还是设置密码
    const forget =
      this.props.location.state && this.props.location.state.forget;
    this.store.init(forget);
  }

  render() {
    const password = this.store.state().get('password');
    const customerId = this.store.state().get('customerId');
    const isShowpwd = this.store.state().get('isShowpwd');
    return (
      <div className="content register-content">
        <div className="register-box">
          <div className="row form-item">
            <span className="form-text">支付密码</span>
            <input
              placeholder="请输入新的支付密码"
              type={isShowpwd ? 'text' : 'password'}
              className="form-input"
              pattern="/^[A-Za-z0-9]{6,16}$/"
              maxLength={16}
              value={password}
              onChange={(e) => this.store.getNewPass(e.target.value)}
            />
            <i
              onClick={this.store.showPwd}
              className={`iconfont icon-${
                isShowpwd ? 'yanjing' : 'iconguanbiyanjing'
              }`}
            />
          </div>
          <div className="register-tips">
            <p>提示：</p>

            <p>
              1.为了保障您的账户信息安全，在您变更账户重要信息时，需要对您的身份进行验证。感谢您的理解和支持！
            </p>

            <p>
              2.如出现收不到短信的情况，可能是由于通信网络异常造成，请您稍后重新尝试操作！
            </p>
          </div>
        </div>
        <div className="register-btn" style={{padding:'0'}}>
          {/* <button
            className="btn btn-primary"
            onClick={() => this.store.updatePass()}
          >
            提交
          </button> */}
          <img src={submit} alt="" style={{width:'100%'}} onClick={() => this.store.updatePass()}/>
        </div>
      </div>
    );
  }
}
