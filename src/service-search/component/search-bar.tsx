import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { history, noop } from 'wmkit';
import Tabs from './tab';

@Relax
export default class SearchBar extends React.Component<any, any> {
  _input: any;
  // 点击取消后定时500ms。如果页面没有正常跳转，跳转到首页。如果页面正常跳转了，取消定时器。用于处理直接打开搜索页面后点击取消返回的问题
  _timer: any;

  props: {
    relaxProps?: {
      key: string;
      keyword: string;
      searchHistory: any;
      addHistory: Function;
      clearHistory: Function;
      changeKeyword: Function;
    };
  };

  static relaxProps = {
    key: 'key',
    keyword: 'keyword',
    searchHistory: 'searchHistory',
    addHistory: noop,
    clearHistory: noop,
    changeKeyword: noop
  };

  componentDidMount() {
    this._input.focus();
  }

  componentWillUnmount() {
    if (this._timer) {
      clearTimeout(this._timer);
    }
  }

  render() {
    const { keyword, searchHistory, key } = this.props.relaxProps;

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div className="input-box">
              <i className="iconfont icon-sousuo" onClick={this._goSearch} />
              <form action="javascript:;">
                <input
                  autoFocus={true}
                  type="search"
                  placeholder={key == 'supplier' ? '搜索商家' : '搜索商品'}
                  value={keyword}
                  maxLength={100}
                  ref={(input) => (this._input = input)}
                  onChange={this._inputOnChange}
                  onKeyDown={(e) => {
                    if (e.which == 13) {
                      this._goSearch();
                    }
                  }}
                />
              </form>
            </div>
            <div>
              <a
                href="javascript:;"
                className="search-text"
                onClick={() => {
                  history.goBack();
                  this._timer = setTimeout(() => {
                    // 如果500ms后页面还没有跳转，那么就去首页
                    key == 'groupon'
                      ? history.replace('/service-groupon-center')
                      : history.replace('/serviceGoodsList');
                  }, 500);
                }}
              >
                取消
              </a>
            </div>
          </div>
        </div>

        {/*分销员搜索不展示此标签*/}
        {key == 'distribute' ||
        key == 'distributeGoods' ||
        key == 'groupon' ? null : (
          <Tabs />
        )}

        <div className="search-content">
          <div className="search-title">
            <span>搜索历史</span>
            <a href="javascript:;" onClick={this._clearSearchHistory}>
              <img src={require('../img/delete.png')} alt="" style={{width:".3rem",height:".32rem"}}></img>
            </a>
          </div>
          <div className="history-list">
            {searchHistory.count() > 0 ? (
              searchHistory.toJS().map((s, index) => {
                return (
                  <div
                    key={index}
                    className="item"
                    onClick={() => {
                      this._search(s);
                    }}
                  >
                    {s}
                  </div>
                );
              })
            ) : (
              <span className="no-history">暂无搜索记录</span>
            )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 搜索框内容变化
   * @param e
   * @private
   */
  _inputOnChange = (e) => {
    this.props.relaxProps.changeKeyword(e.target.value);
  };

  /**
   * 输入关键字后进行搜索
   * @private
   */
  _goSearch = () => {
    // 关键字
    const { keyword } = this.props.relaxProps;
    this._search(keyword);
  };

  /**
   * 直接点击已有记录进行搜索
   */
  _search = (keywordTemp) => {
    keywordTemp = keywordTemp ? keywordTemp.trim() : '';

    const { key } = this.props.relaxProps;
    //默认跳转商品列表
    let reqPath = '/serviceGoodsList';
    if (key == 'supplier') {
      //如果是查询店铺历史
      reqPath = '/service-store-list';
    } else if (key == 'distribute') {
      //跳转分销员选品页面
      reqPath = '/service-shop-goods';
    } else if (key == 'groupon') {
      // 跳转 拼团活动商品 搜索页面
      reqPath = '/service-groupon-search-list';
    } else if (key == 'distributeGoods') {
      // 跳转 分销推广商品 搜索页面
      reqPath = '/service-distribution-goods-list';
    }

    if (!keywordTemp) {
      // 不带搜索参数跳转商品列表页
      history.push(`${reqPath}`);
      return;
    }

    // 处理历史搜索记录
    this._handleSearchHistory(keywordTemp);

    // 带搜索参数跳转商品列表页
    history.push(`${reqPath}?q=${encodeURIComponent(keywordTemp)}`);
  };

  /**
   * 处理搜索历史
   * @param keyword
   * @private
   */
  _handleSearchHistory = (keyword) => {
    const { addHistory } = this.props.relaxProps;
    addHistory(keyword);
  };

  /**
   * 清除历史记录
   * @private
   */
  _clearSearchHistory = () => {
    const { clearHistory } = this.props.relaxProps;
    clearHistory();
  };
}
