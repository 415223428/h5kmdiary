import React from 'react'
import { StoreProvider } from 'plume2'
import AppStore from './store'

import StoreTop from './component/top'
import List from './component/list'
import StoreBottom from './component/store-bottom'


@StoreProvider(AppStore, { debug: __DEV__ })
export default class StoreAttention extends React.Component<any, any>{


  render() {

    return (
      <div>
        <StoreTop />
        <List />
        <StoreBottom />
      </div>
    )
  }
}