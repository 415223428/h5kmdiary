import React, { Component } from 'react';
import { Relax } from 'plume2';
import { List } from 'immutable';
import { noop, Check, Alert, Confirm, Button } from 'wmkit';
import { IList } from 'typings/globalType';

const SubmitButton = Button.Submit;

@Relax
export default class StoreBottom extends Component<any, any> {
  props: {
    relaxProps?: {
      //是否编辑
      edit: boolean;
      //所有的店铺id
      storeIds: IList;
      //勾选的店铺id
      checkedStoreIds: IList;
      //改变状态（编辑 | 完成）
      updateEdit: Function;
      //改变勾选项
      setCheckedStore: Function;
      //移除关注
      cancelAttention: Function;
    };
  };

  static relaxProps = {
    edit: 'edit',
    storeIds: 'storeIds',
    checkedStoreIds: 'checkedStoreIds',
    updateEdit: noop,
    setCheckedStore: noop,
    cancelAttention: noop
  };

  render() {
    const { edit, storeIds, checkedStoreIds } = this.props.relaxProps;
    const isCheckedAll = storeIds.size == checkedStoreIds.size;

    return (
      <div style={{ height: 48 }} className={edit ? 'show' : 'hide'}>
        <div className="bottom-bar-total" style={{justifyContent: 'space-between'}}>
          <div className="check-all">
            <div style={{ marginRight: '10px' }}>
              <Check
                checked={isCheckedAll}
                onCheck={() => this._checkedAll(isCheckedAll)}
              />
            </div>
            <span>全选</span>
          </div>
          <div>
            <SubmitButton
              text={'取消关注'}
              disabled={checkedStoreIds.size == 0}
              onClick={this._cancelAttention}
            />
          </div>
        </div>
      </div>
    );
  }

  /**
   * 全选
   */
  _checkedAll = (isCheckedAll) => {
    const { setCheckedStore } = this.props.relaxProps;
    if (isCheckedAll) {
      setCheckedStore(List());
    } else {
      setCheckedStore(this.props.relaxProps.storeIds);
    }
  };

  /**
   * 将选中店铺移除关注
   */
  _cancelAttention = () => {
    let { checkedStoreIds, cancelAttention } = this.props.relaxProps;
    if (checkedStoreIds.size == 0) {
      Alert({ text: '请选择需要取消关注的店铺' });
      return;
    }
    Confirm({
      title: '取消关注',
      text: '确定要取消关注所选店铺？',
      cancelBtn: '取消',
      okBtn: '确定',
      confirmCb: () => {
        cancelAttention(checkedStoreIds.toJS());
      }
    });
  };
}
