import React, { Component } from 'react'
import { Relax } from 'plume2'
import {List} from 'immutable'
import { noop, Check } from 'wmkit'
import {IList} from 'typings/globalType'
const styles = require('../css/style.css')

@Relax
export default class StoreTop extends Component<any, any> {
  props: {
    relaxProps?: {
      attentionNum: number,
      //是否编辑
      edit: boolean,
      //所有的店铺id
      storeIds: IList,
      //勾选的店铺id
      checkedStoreIds: IList,
      //改变状态（编辑 | 完成）
      updateEdit: Function,
      //改变勾选项
      setCheckedStore: Function,
    }
  };

  static relaxProps = {
    attentionNum: 'attentionNum',
    edit: 'edit',
    storeIds: 'storeIds',
    checkedStoreIds: 'checkedStoreIds',
    updateEdit: noop,
    setCheckedStore: noop,
  }

  render() {
    const { edit,attentionNum,storeIds,checkedStoreIds } = this.props.relaxProps;
    const isCheckedAll = storeIds.size == checkedStoreIds.size;

    return (
      <div style={{ height: 40 }}>
        <div className={styles.editHead}>
          <div className={styles.myStore}>
            <div className={edit ? 'show' : 'hide'} style={{ marginRight: 10 }}>
              <Check checked={isCheckedAll} onCheck={() => this._checkedAll(isCheckedAll)}/>
            </div>
            <p>关注店铺&nbsp;&nbsp;{attentionNum}</p>
          </div>
          {
            attentionNum > 0 &&
            <span onClick={this._operateEdit}>{edit ? '完成' : '编辑'}</span>
          }
        </div>
      </div>
    )
  }


  /**
   * 全选
   */
  _checkedAll = (isCheckedAll) => {
    const {setCheckedStore} = this.props.relaxProps;
    if(isCheckedAll){
      setCheckedStore(List());
    }else{
      setCheckedStore(this.props.relaxProps.storeIds);
    }
  }


  /**
   * 切换编辑
   */
  _operateEdit = () => {
    const { updateEdit } = this.props.relaxProps
    updateEdit()
  }
}