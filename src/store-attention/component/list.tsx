import React from 'react'
import { Link } from 'react-router-dom'
import { Relax } from 'plume2'
import {IList} from 'typings/globalType'
import { FindArea,Check,Blank,ListView,history,noop } from 'wmkit'

const noneImg = require('../img/none.png')
const defaultImg = require('../img/defalutShop.png')

@Relax
export default class List extends React.Component<any, any> {

  props: {
    relaxProps?: {
      //是否编辑
      edit: boolean,
      checkedStoreIds: IList,
      handleDataReached: (data: Object) => void,
      setCheckedStore: Function,
    }
  };

  static relaxProps = {
    edit: 'edit',
    checkedStoreIds: 'checkedStoreIds',
    handleDataReached: noop,
    setCheckedStore: noop,
  }

  render() {
    const {handleDataReached} = this.props.relaxProps;
    return (
      <ListView
        url="/store/storeFollows"
        renderRow={this._storeInfoRow}
        renderEmpty={() => <Blank img={noneImg} content="您还没有关注任何店铺～"/>}
        onDataReached={handleDataReached}
      />
    )
  }


  /**
   * 店铺信息
   */
  _storeInfoRow = (storeInfo) => {
    const { edit,checkedStoreIds } = this.props.relaxProps;
    const checked = checkedStoreIds.find(storeId => storeId == storeInfo.storeId);
    return (
      <div key={storeInfo.storeId} className="supplier-item b-1px-b" onClick={() => this._clickItem(edit,storeInfo,checked,checkedStoreIds)} >
        <div className="supplier-left supplier-store">
          {
            edit ? <div className="supplier-check">
              <Check checked={checked}/>
            </div> : null
          }
          <div className="store-img-head">
            <img src={storeInfo.storeLogo || defaultImg}  alt="" />
            {storeInfo.storeResponseState == 1 && <div className="shadow">店铺<br />已关闭</div>}
            {storeInfo.storeResponseState == 2 && <div className="shadow">店铺<br />已过期</div>}
            {storeInfo.storeResponseState == 3 && <div className="shadow">店铺<br />不存在</div>}
          </div>
          <div className="supplier-content">
            <h4>{storeInfo.storeName}</h4>
            <p>{storeInfo.supplierName}</p>
            <div className="self-sales" style={storeInfo.companyType == 0 ? null : {visibility:'hidden'}}>自营</div>
          </div>
        </div>
        <div className="supplier-right">
          <Link to={`/store-main/${storeInfo.storeId}`} style={edit || storeInfo.storeResponseState != 0 ? {visibility:'hidden'} : null}>进店</Link>
          <p>{FindArea.findCity(storeInfo.cityId)}</p>
        </div>
      </div>
    )
  }


  _clickItem = (edit,storeInfo,checked,checkedStoreIds) => {
    const {setCheckedStore} = this.props.relaxProps;
    // 如果正在编辑,点击则修改当前的勾选状态
    if(edit){
      if(checked){
        checkedStoreIds = checkedStoreIds.filter(storeId => storeId!=storeInfo.storeId);
      }else{
        checkedStoreIds = checkedStoreIds.push(storeInfo.storeId);
      }
      setCheckedStore(checkedStoreIds);
    }else if(storeInfo.storeResponseState == 0){
      // 非编辑页,点击则跳转店铺详情
      history.push(`/store-main/${storeInfo.storeId}`);
    }
  }

}