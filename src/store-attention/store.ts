import { Store } from 'plume2';
import { config } from 'config';
import { fromJS } from 'immutable';
import { Alert } from 'wmkit';
import { deleteStoreFollow } from './webapi';
import EditorActor from './actor/editor-actor';


export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if(__DEV__){
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [
      new EditorActor
    ]
  }

  /**
   * 店铺列表ListView查询得到的数据返回处理
   */
  handleDataReached = (data: any) => {
    if (data.code !== config.SUCCESS_CODE) {
      return false;
    }
    const storeContext = data.context;

    this.dispatch('store:mergeStoreIds', fromJS(storeContext.content.map(store => store.storeId)));
    this.dispatch('store:size', storeContext.totalElements);
  }


  /**
   *设置勾选的店铺
   */
  setCheckedStore = (checkedStoreIds) => {
    this.dispatch('store:setCheckedStore',checkedStoreIds)
  }


  /**
   * 改变编辑状态
   */
  updateEdit = () => {
    this.dispatch('store:edit')
  }


  /**
   * 改变编辑状态
   */
  cancelAttention = async (storeIds) => {
    const res = await deleteStoreFollow(storeIds);
    if (res.code == config.SUCCESS_CODE) {
      Alert({text: '取消关注成功！', cb: () => location.reload()})
    } else {
      Alert({text: res.message})
    }
  }

}