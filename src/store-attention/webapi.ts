import {Fetch} from 'wmkit'


/**
 * 移除我的收藏
 */
export const deleteStoreFollow = (storeIds) => {
  return Fetch('/store/storeFollow', {
    method: 'DELETE',
    body: JSON.stringify({
      storeIds:storeIds
    })
  })
}