import { Actor, Action, IMap } from 'plume2'


export default class EditorActor extends Actor {
  defaultState() {
    return {
      edit: false, // 编辑 | 完成
      attentionNum: 0, // 关注数量
      storeIds: [], //当前展示的所有店铺id列表
      checkedStoreIds: [], // 已勾选的店铺id列表
    }
  }

  /**
   * 设置显示的店铺id
   */
  @Action('store:mergeStoreIds')
  mergeStoreIds(state,storeIds) {
    return state.update('storeIds', s => s.concat(storeIds));
  }


  /**
   * 设置勾选的店铺id
   */
  @Action('store:setCheckedStore')
  setCheckedStore(state,checkedStoreIds) {
    return state.set('checkedStoreIds', checkedStoreIds)
  }


  /**
   * 设置关注数量
   */
  @Action('store:size')
  setAttentionNum(state,attentionNum) {
    return state.set('attentionNum', attentionNum)
  }


  /**
   * 是否编辑
   */
  @Action('store:edit')
  setEditStat(state) {
    return state.set('edit', !state.get('edit'))
  }

}