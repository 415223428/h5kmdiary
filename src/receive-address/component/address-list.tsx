import React from 'react';

import { Relax } from 'plume2';
import { Blank, FindArea, Check, noop, Confirm, history, storage } from 'wmkit';
import { cache } from 'config';
import { IList } from 'typings/globalType';
import { fromJS } from 'immutable';

const style = require('../css/style.css');
const edit=require('../img/edit.png')
const del=require('../img/del.png')
@Relax
export default class AddressList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      addressList: IList;
      setDefault: Function;
      deleteAddress: Function;
    };
  };

  static relaxProps = {
    addressList: 'addressList',
    setDefault: noop,
    deleteAddress: noop
  };

  render() {
    const { addressList, setDefault } = this.props.relaxProps;

    if (addressList.isEmpty()) {
      return (
        <Blank
          img={require('../img/none.png')}
          content="还没有收货地址，快去新增吧"
        />
      );
    }

    const renderList =
      addressList &&
      addressList
        .map((v, k) => (
          <div className="address-box" key={v.get('deliveryAddressId')}>

            <div
              className="address-item"
              onClick={() => {
                this._chooseAddress(v);
              }}
            >
              <img src={require('../img/position.png')} className="iconfont icon-dz posit" alt="" style={{width:"0.3rem",height:".37rem",marginRight:'0.2rem'}} />              
              <div style={{width:'100%'}}>
                <div className="address-name">
                  <div className='name'>
                    收货人：<span>{v.get('consigneeName')}</span>
                  </div>
                  <div className='phone'>{v.get('consigneeNumber')}</div>
                </div>
                <div className="address-detail">
                  收货地址：<span>{this._renderText(v)}</span>
                </div>
              </div>
            </div>
            <div className="address-operate">
              <div className="address-check">
                <Check
                  style={{marginRight:'0.1rem'}}
                  checked={v.get('isDefaltAddress') == 1}
                  onCheck={() => setDefault(v.get('deliveryAddressId'))}
                />
                <span>设为默认</span>
              </div>
              <div className="address-btn">
                <div
                  className="edit-row"
                  onClick={() => this._editAddress(v.get('deliveryAddressId'))}
                >
                  {/* <i className="iconfont icon-bianji edit" /> */}
                  <img src={edit} alt="" style={{width:'0.3rem'}}/>
                  <span>编辑</span>
                </div>
                <div
                  className="edit-row"
                  onClick={() =>
                    this._deleteAddress(v.get('deliveryAddressId'))
                  }
                >
                  {/* <i className="iconfont icon-shanchu delete" /> */}
                  <img src={del} alt="" style={{width:'0.3rem'}}/>
                  <span>删除</span>
                </div>
              </div>
            </div>
            {/* <div className="seperate_line" /> */}
          </div>
        ))
        .toArray();

    return (
      <div 
        // style={{ backgroundColor: '#fafafa', paddingBottom: '1.7rem' }}
        >
        {renderList}
        <div
          style={{
            padding: '0 .32rem .24rem .32rem',
            fontSize: '0.28rem',
            color: '#bbb'
          }}
        >
          提示：最多可以添加20条收货地址
        </div>
      </div>
    );
  }

  /**
   * 拼接地址
   * @param address
   * @returns {any}
   * @private
   */
  _renderText(address: any) {
    return (
      <span>
        {FindArea.addressInfo(
          address.get('provinceId'),
          address.get('cityId'),
          address.get('areaId')
        ) + address.get('deliveryAddress')}
      </span>
    );
  }

  /**
   * 编辑收货地址
   * @param accountId
   * @private
   */
  _editAddress(addressId) {
    history.push('/receive-address-edit/' + addressId);
  }

  /**
   * 删除收货地址
   * @param deliveryAddressId
   * @private
   */
  _deleteAddress(deliveryAddressId: string) {
    const { deleteAddress } = this.props.relaxProps;
    Confirm({
      title: '删除收货地址',
      text: '您确定要删除所选收货地址？',
      okBtn: '确定',
      cancelBtn: '取消',
      confirmCb: function() {
        deleteAddress(deliveryAddressId);
      }
    });
  }

  /**
   * 选择地址
   * @param address
   * @private
   */
  _chooseAddress(address) {
    let data = null;
    let dataType = '';
    if (storage('session').get(cache.ORDER_CONFIRM)) {
      dataType = cache.ORDER_CONFIRM;
      data = storage('session').get(cache.ORDER_CONFIRM);
      let { comeFrom } = JSON.parse(data);
      if (comeFrom == 'invoice') {
        data = storage('session').get(cache.ORDER_INVOICE);
        dataType = cache.ORDER_INVOICE;
      }
    } else if (storage('session').get(cache.ORDER_INVOICE)) {
      data = storage('session').get(cache.ORDER_INVOICE);
      dataType = cache.ORDER_INVOICE;
    } else {
      return;
    }
    const { addressList } = this.props.relaxProps;
    let page = '/order-confirm';
    let {
      defaultAddr,
      orderConfirm,
      pointsOrderConfirm,
      comeFrom
    } = JSON.parse(data);
    //始终保持地址数据为最新
    if (comeFrom == 'address') {
      defaultAddr = address;
    } else if (comeFrom == 'pointsAddress') {
      page = '/points-order-confirm';
      defaultAddr = address;
    } else if (comeFrom == 'flashSaleAddress') {
      page = '/flash-sale-order-confirm';
      defaultAddr = address;
    } else if (comeFrom.startsWith('invoiceAddress_')) {
      page = 'back';
      const storeId = comeFrom.substr(comeFrom.indexOf('_') + 1);
      comeFrom = 'invoice';
      orderConfirm = fromJS(orderConfirm)
        .map((o) => {
          if (o.get('storeId') == storeId) {
            o = o.set('defaultInvoiceAddr', address);
          }
          return o;
        })
        .toJS();
    }
    storage('session').set(
      dataType,
      JSON.stringify({
        defaultAddr,
        orderConfirm,
        pointsOrderConfirm,
        comeFrom
      })
    );
    if (page == 'back') {
      history.go(-1);
      return;
    }
    history.push({
      pathname: page,
      state: comeFrom == 'pointsAddress' && {
        params: {
          pointsGoodsId: pointsOrderConfirm.pointsGoodsId,
          num: pointsOrderConfirm.num
        }
      }
    });
  }
}
