import React from 'react';
import { Link } from 'react-router-dom';

import { StoreProvider } from 'plume2';
import { Button, history } from 'wmkit';
import AppStore from './store';
import AddressList from './component/address-list';
import './css/style.css'

const LongBlueButton = Button.LongBlue;
const add=require('./img/add.png')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class ReceiveAddress extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    this.store.init();
  }

  render() {
    return (
      <div
      // style={{ background: '#fafafa', height: '100%' }}
      >
        <AddressList />
        <div className="register-btn receive-btn" style={{padding:'0',lineHeight:'0.01rem'}}>
          {/* <LongBlueButton
            text={'新增'}
            disabled={this.store.state().get('showAdd')}
            onClick={() => {
              history.push('/receive-address-edit/-1');
            }}
          /> */}
          <img src={add} alt="" style={{height:'1rem'}} onClick={() => {
              history.push('/receive-address-edit/-1');
            }}/>
        </div>
      </div>
    );
  }
}
