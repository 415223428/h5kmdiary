import { Store } from "plume2";

import { Alert, storage, FormRegexUtil, history } from 'wmkit'
import { config, cache } from 'config'
import { fromJS } from 'immutable'

import * as webApi from './webapi'
import AddressActor from './actor/address-actor'

export default class AppStore extends Store {
  bindActor() {
    return [
      new AddressActor
    ]
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  /**
   * 初始化地址列表
   * @returns {Promise<void>}
   */
  init = async () => {
    const addresses = await webApi.fetchAddresses()
    this.transaction(() => {
      this.dispatch('address: list: fetch', addresses.context)
    })
  }


  /**
   * 设为默认收货地址
   */
  setDefault = async (addressId: string) => {
    const res = await webApi.setDefault(addressId);
    if (res.code !== config.SUCCESS_CODE) {
      Alert({ text: res.message, time: 1000 })
      return
    } else {
      Alert({ text: res.message, time: 1000 })
      this.init()
    }
  }


  /**
   * 删除收货地址
   */
  deleteAddress = async (addressId: string) => {
    const res = await webApi.deleteAddress(addressId);
    if (res.code !== config.SUCCESS_CODE) {
      Alert({ text: res.message, time: 1000 })
      return
    } else {
      let { defaultAddr, orderConfirm } =
        JSON.parse(storage('session').get(cache.ORDER_INVOICE)) || {} as any
      if ((defaultAddr && defaultAddr.deliveryAddressId == addressId)
        || (orderConfirm && fromJS(orderConfirm).some(o => o.defaultInvoiceAddr
          && o.defaultInvoiceAddr.deliveryAddressId == addressId))) {
        const { context } = await webApi.fetchCustomerDefaultAddr()
        orderConfirm = fromJS(orderConfirm).map(o => {
          if (o.getIn(['defaultInvoiceAddr', 'deliveryAddressId']) == addressId) {
            o = o.set('defaultInvoiceAddr', context)
          }
          return o
        }).toJS()
        let firstAddress = defaultAddr
        if (defaultAddr.deliveryAddressId == addressId) {
          firstAddress = context
        }
        storage('session').set(cache.ORDER_INVOICE, {
          defaultAddr: firstAddress, orderConfirm
        })
      }
      Alert({ text: res.message, time: 1000 })
      this.init()
    }
  }
}