import {Action, Actor, IMap} from "plume2";
import { fromJS } from 'immutable'

export default class AddressActor extends Actor {
  defaultState() {
    return {
      //收货地址列表
      addressList: [],
      //收货地址总数
      showAdd:false
    }
  }


  /**
   * 存储会员收货地址列表
   * @param state
   * @param addressList
   * @returns {Map<string, V>}
   */
  @Action('address: list: fetch')
  fetchAddressList(state: IMap, addressList) {

    if(fromJS(addressList).count() >= 20){
      state = state.set('showAdd', true)
    }else{
      state = state.set('showAdd', false)
    }
    return state.set('addressList', fromJS(addressList))
  }
}
