import React, { Component } from 'react';
import MagicBox from '@wanmi/magic-box';
import * as magicBoxProps from '../magicBoxProps';
// import { history } from 'wmkit';
// const imgSrc = require('./img/arrow.png');
export default class PageLink extends Component<any, any> {
  render() {
    const { pageCode, pageType, storeId } = this.props.match.params;
    let props = { ...magicBoxProps.props };
    if (storeId) {
      props.storeId = storeId;
    }
    return (
      // <div>
      <MagicBox
        {...props}
        pageType={pageType}
        pageCode={pageCode}
        part="MAIN"
      />
      // 回退暂时不要，哪天又需要打开注释就可以了
      /* <div
          style={{
            position: 'absolute',
            top: 5,
            left: 5,
            padding: 10,
            zIndex: 9999
          }}
        >
          <img
            style={{ width: 24, height: 24 }}
            src={imgSrc}
            onClick={() => history.goBack()}
          />
        </div>
      </div> */
    );
  }
}
