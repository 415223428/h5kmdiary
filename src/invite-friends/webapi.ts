/**
 * Created by qiaokang on 2019/3/11.
 */
import { Fetch } from 'wmkit';

/**
 * 查询分销配置信息
 * @returns {Promise<Result<any>>}
 */
export const getDistributionSetting = () => {
  return Fetch(`/customer/get-distribution-setting`, {
    method: 'GET'
  });
};

/**
 * 获取分销设置和邀请人的信息
 */
export const fetchInvitorInfo = () => {
  return Fetch('/distribute/setting-invitor');
};

/**
 * 生成小程序码
 * @param {string} inviteeId
 * @returns {Promise<Result<any>>}
 */
export const getMiniProgramCode = (inviteeId: string) => {
  return Fetch(`/distribution/miniProgram-code/distributionMiniProgramQrCode`, {
    method: 'POST',
    body: JSON.stringify({
      inviteeId: inviteeId,
      tag: 'register'
    })
  });
};
