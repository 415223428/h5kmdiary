import React, { Component } from 'react';
import { IMap, StoreProvider } from 'plume2';
import { fromJS } from 'immutable';
import AppStore from './store';
import { ListView, Blank } from 'wmkit';

import InvitHead from './component/invit-head';
import FriendList from './component/friend-list';
import InvitFriendPop from './component/invit-friend-pop';
import Detail from './component/detail';
const style = require('./css/style.css');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class InviteFriends extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    this.store.init();
  }

  render() {
    let invitState = this.store.state().get('invitState');
    let detailState = this.store.state().get('detailState');
    let fetchFriends = this.store.fetchFriends;
    let form = fromJS([]).toJS();

    return (
      <div style={{ backgroundColor: '#fff' }}>
        <InvitHead />
        <ListView
          url="/customer/page-invite-customer"
          params={form}
          style={{ height: window.innerHeight - 88 }}
          renderRow={(friend: IMap, index: number) => {
            return <FriendList key={index} friend={friend} index={index} />;
          }}
          renderEmpty={() => (
            <Blank
              img={require('./img/list-none.png')}
              content="您还没有邀请好友哦"
            />
          )}
          onDataReached={(res) => fetchFriends(res)}
        />

        { invitState && <InvitFriendPop />}
        { detailState && <Detail />}
      </div>
    );
  }
}
