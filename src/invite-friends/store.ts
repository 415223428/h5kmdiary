import { Store, IOptions } from 'plume2';
import { fromJS } from 'immutable';
import { _, WMkit, history } from 'wmkit';
import { cache, config } from 'config';
import InvitActor from './actor/invit-actor';
import * as webapi from './webapi';
export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new InvitActor()];
  }

  /**
   * 初始化
   */
  init = async () => {
    if (WMkit.isDistributor()) {
      // 当前用户是分销员
      this.dispatch('InvitActor:isDistributor', true);
    }

    // 查询分销配置信息
    const { code, context } = await webapi.getDistributionSetting();
    if (code == config.SUCCESS_CODE) {
      if (context) {
        let result = context as any;
        //1、分销大开关打开且邀新奖励开关打开2、小C身份且招募开关打开且为邀新升级类型，才能进入该页面
        if (
          result.openFlag &&
          (result.inviteFlag ||
            (!WMkit.isDistributor() && result.applyFlag && result.applyType))
        ) {
          this.dispatch('InvitActor:initDistributionSetting', context as any);
        } else {
          history.replace('/error');
        }
      }
    }

    //查询分销设置信息
    const res = await webapi.fetchInvitorInfo();
    if (res.code == config.SUCCESS_CODE) {
      this.dispatch(
        'distributeActor:setting',
        (res.context as any).distributionSettingSimVO
      );
    }
  };

  /**
   * 打开详情弹层
   */
  _openDetail = async (val, ruleDesc) => {
    this.dispatch('InvitActor:changeDetail', val);
    this.dispatch('InvitActor:getDetailData', ruleDesc.ruleDesc);
  };

  /**
   * 设置邀请好友列表
   * @param res
   */
  fetchFriends = (res: any) => {
    if (res.context) {
      if (res.context.totalElements) {
        this.dispatch('InvitActor:setTotalNum', res.context.totalElements);
      }

      this.dispatch('InvitActor:setFriends', fromJS(res.context.content));
    }
  };

  /**
   * 关闭详情弹层
   */
  closeLayer = async (val) => {
    this.dispatch('InvitActor:changeDetail', val);
  };

  /**
   * 打开邀请好友弹层
   */
  _openInitFriend = async (val) => {
    // 获取当前登陆用户信息
    const loginData = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
    if (_.isWeixin()) {
      // 微信浏览器中，调用小程序页面，否则不处理
      if (loginData && loginData.customerId) {
        let customerId = loginData.customerId;

        this.dispatch('InvitActor:changeFriends', val);
      }
    }
  };

  /**
   * 关闭邀请好友弹层
   */
  _closeInitFriend = async (val) => {
    this.dispatch('InvitActor:changeFriends', val);
  };
}
