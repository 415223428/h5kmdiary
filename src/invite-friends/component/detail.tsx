import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
import { NewLayer } from 'biz';
@Relax
export default class InvitHead extends React.Component<any, any> {
  props: {
    relaxProps?: {
      _closeDetail: Function;
      detailList: string;
      closeLayer: Function;
      picture: string;
    };
  };

  static relaxProps = {
    _closeDetail: noop,
    detailList: 'detailList',
    closeLayer: noop,
    picture: 'picture'
  };

  render() {
    const {
      _closeDetail,
      detailList,
      closeLayer,
      picture
    } = this.props.relaxProps;
    return (
      <NewLayer
        image={picture}
        detailData={detailList}
        _closeDetail={() => closeLayer(false)}
      />
    );
  }
}
