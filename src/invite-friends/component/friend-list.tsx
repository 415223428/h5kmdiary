import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { _ } from 'wmkit';

// 默认头像
const defaultImg = require('../img/default-img.png');

@Relax
export default class InvitHead extends React.Component<any, any> {
  props: {
    relaxProps?: {
    };
    friend: any;
    index: number;
  };

  static relaxProps = {
  };

  render() {
    let { friend } = this.props;
    let customerName = friend.invitedNewCustomerName ? friend.invitedNewCustomerName : friend.invitedNewCustomerAccount;
    let headImg = friend.invitedNewCustomerHeadImg?friend.invitedNewCustomerHeadImg:defaultImg;
    let orderNum = friend.orderNum ? friend.orderNum : 0;
    let amount = friend.amount ? friend.amount : '0';

    return (
      <div className="friend-list">
        {/* 好友列表 */}
        <ul className="list-ul">
          <li>
            <img
              className="portrait"
              src={headImg}
            />
            <div className="div-text">
              <span className="left-text">
                <span className="name">{customerName}</span>
                <span className="money">
                  累计消费金额：<span className="number">{amount}</span>
                </span>
              </span>
              <span className="right-text">
                <span className="date">注册时间：{_.formatDay(friend.registerTime)}</span>
                <span className="order">已购买{orderNum}单</span>
              </span>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}
