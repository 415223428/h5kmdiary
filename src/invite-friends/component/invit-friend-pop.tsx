import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
@Relax
export default class InvitHead extends React.Component<any, any> {
  props: {
    relaxProps?: {
      _closeInitFriend: Function;
    };
  };

  static relaxProps = {
    _closeInitFriend: noop
  };

  render() {
    const { _closeInitFriend } = this.props.relaxProps;
    return (
      <div className="pop-div">
        <div className="close" onClick={() => _closeInitFriend(false)}>
          <i className="iconfont icon-Close f-size" />
        </div>
        <img className="bj-img" src={require('../img/bj.png')} />
        <div className="div-box">
          <img className="invit-icon" src={require('../img/s2b.jpg')} />
          <span className="invit-text2">长按立即购买</span>
        </div>
        {/*<ul className="bottom-ul">*/}
          {/*<li>*/}
            {/*<div className="wecat-icon" />*/}
            {/*<span>微信好友</span>*/}
          {/*</li>*/}
          {/*<li>*/}
            {/*<div className="circle-icon" />*/}
            {/*<span>朋友圈</span>*/}
          {/*</li>*/}
          {/*<li>*/}
            {/*<div className="img-icon" />*/}
            {/*<span>保存到相册</span>*/}
          {/*</li>*/}
        {/*</ul>*/}
      </div>
    );
  }
}
