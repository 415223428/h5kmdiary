import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';
import wx from 'weixin-js-sdk';
import { noop, WMkit, Alert } from 'wmkit';
import { cache } from 'config';

@Relax
export default class InvitHead extends React.Component<any, any> {
  props: {
    relaxProps?: {
      _openDetail: Function;
      isDistributor: boolean;
      setting: IMap;
      _openInitFriend: Function;
      totalNum: number;
      dSetting: IMap;
    };
  };

  static relaxProps = {
    _openDetail: noop,
    isDistributor: 'isDistributor',
    setting: 'setting',
    _openInitFriend: noop,
    totalNum: 'totalNum',
    dSetting: 'dSetting'
  };

  render() {
    const {
      _openDetail,
      isDistributor,
      setting,
      _openInitFriend,
      totalNum,
      dSetting
    } = this.props.relaxProps;
    // 宣传海报
    let inviteImg = '';
    // 规则描述
    let ruleDesc;
    const applyFlag = dSetting.get('applyFlag');
    const inviteFlag = dSetting.get('inviteFlag');
    const openFlag = dSetting.get('openFlag');
    //展示邀新海报的情况
    //1.是分销员2.不是分销员且招募开关关闭且邀新奖励开关打开,均显示显示邀新奖励落地页海报
    if (dSetting.size > 0) {
      if (isDistributor || (!isDistributor && !applyFlag && inviteFlag)) {
        // 展示邀新海报
        inviteImg = setting.get('inviteImg');
        // 邀新奖励说明
        ruleDesc = setting.get('inviteDesc');
      } else {
        //展示邀请注册时招募落地页海报
        inviteImg = setting.get('inviteRecruitImg');
        // 招募规则说明
        ruleDesc = setting.get('recruitDesc');
      }
    }

    return (
      <div>
        <div className="head-img">
          <img className="position-img" src={inviteImg} />
          <span
            className="top-span"
            onClick={() => _openDetail(true, { ruleDesc })}
          >
            <span className="explain">详细说明</span>
            <i className="iconfont icon-help icon-width" />
          </span>
        </div>
        <div className="invit-btn">
          <div className="btn" onClick={() => this._openInitFriend()}>
            立即邀请
          </div>
          <span className="btn-text">
            您已成功邀请<span className="number">{totalNum}</span>位好友
          </span>
        </div>
      </div>
    );
  }

  _openInitFriend = () => {
    let { setting, isDistributor } = this.props.relaxProps;
    //小程序打开，去邀请
    if ((window as any).isMiniProgram) {
      let shareImg;
      //分销员
      if (isDistributor) {
        //邀新开关打开
        if (setting.get('inviteFlag')) {
          shareImg = setting.get('inviteShareImg');
        }
      } else {
        //不是分销员，招募开关打开且为邀请注册
        if (setting.get('applyFlag') && setting.get('applyType')) {
          //展示邀请的转发海报
          shareImg = setting.get('recruitShareImg');
        }
        //邀新打开
        if (setting.get('inviteFlag')) {
          shareImg = setting.get('inviteShareImg');
        }
      }
      let params = {
        paper: shareImg,
        inviteeId:
          JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '',
        token: (window as any).token
      };
      //这边获取下商品的数据等
      wx.miniProgram.navigateTo({
        url: `../invitenew/invitenew?data=${JSON.stringify(params)}`
      });
    } else {
      Alert({
        text: '请到小程序端进行该操作!'
      });
    }
  };
}
