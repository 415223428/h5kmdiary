import { Action, Actor, IMap } from 'plume2';
import { List, fromJS } from 'immutable';
export default class InvitActor extends Actor {
  defaultState() {
    return {
      // 邀请朋友-详情弹层初始化状态
      detailState: false,
      // 邀请好友弹层初始化状态
      invitState: false,
      // 是否分销员
      isDistributor: false,
      //详细说明富文本内容
      detailList: [],
      picture: '',
      totalNum: 0,
      miniProgramCode: '', // 小程序码
      friends: [], // 已邀请好友
      setting: fromJS({}),
      dSetting: fromJS({})
    };
  }

  /**
   * 邀请朋友-详情弹层显示
   */
  @Action('InvitActor:changeDetail')
  changeDetail(state, val) {
    return state.set('detailState', val);
  }

  /**
   * 邀请好友弹层显示
   */
  @Action('InvitActor:changeFriends')
  changeFriends(state, val) {
    return state.set('invitState', val);
  }

  /**
   * 获取详细说明内容
   */
  @Action('InvitActor:getDetailData')
  getDetailData(state, val) {
    return state.set('detailList', val);
  }

  /**
   * 初始化分销配置信息
   * @param state
   * @param setting
   */
  @Action('InvitActor:initDistributionSetting')
  initDistributionSetting(state, res) {
    return state.update('setting', (setting) => setting.merge(res));
  }

  /**
   * 更新已邀请好友信息
   * @param {IMap} state
   * @param {Array<any>} params
   * @returns {any}
   */
  @Action('InvitActor:setFriends')
  updateFriends(state: IMap, params: Array<any>) {
    return state.update('friends', (value: List<any>) => {
      return value.push(...params);
    });
  }

  /**
   * 更新已邀请总人数
   * @param state
   * @param totalNum
   */
  @Action('InvitActor:setTotalNum')
  setTotalNum(state, totalNum) {
    return state.set('totalNum', totalNum);
  }

  /**
   * 当前用户是否分销员
   * @param state
   * @param isDistributor
   */
  @Action('InvitActor:isDistributor')
  isDistributor(state, isDistributor) {
    return state.set('isDistributor', isDistributor);
  }

  /**
   * 分销设置信息
   * @param state
   * @param res
   */
  @Action('distributeActor:setting')
  DistributionSetting(state: IMap, res) {
    return state.set('dSetting', fromJS(res));
  }
}
