import { Fetch, WMkit } from 'wmkit';
type TResult = { code: string; message: string; context: any };

/**
 * 查询店铺会员详情信息
 */
export const fetchStoreVip = (storeId) => {
  return WMkit.isLoginOrNotOpen()
    ? Fetch<TResult>('/store/storeVip', {
        method: 'POST',
        body: JSON.stringify({ storeId })
      })
    : Fetch<TResult>('/store/storeVipFront', {
        method: 'POST',
        body: JSON.stringify({ storeId })
      });
};
