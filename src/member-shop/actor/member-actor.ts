import { Actor, Action } from 'plume2'

export default class MemberActor extends Actor {

  defaultState() {
    return {
      level: {}, // 会员的当前等级
      levelList: [], // 不同等级的折扣信息
      store: {}, // 店铺基本信息
    }
  }


  /**
   * 筛选展开收起
   */
  @Action('member:initParams')
  initParams(state,params) {
    return state.mergeDeep(params)
  }

}