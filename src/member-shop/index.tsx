import React from 'react'
import { StoreProvider } from 'plume2'
import Head from './component/head'
import MemberRights from './component/member-rights'
import AppStore from './store'
import './css/style.css'

@StoreProvider(AppStore, { debug: __DEV__ })
export default class MemberShop extends React.Component<any, any>{
  store: AppStore

  componentDidMount() {
    const { sid } = this.props.match.params
    this.store.init(sid);
  }

  render() {
    return (
      <div style={{ height: '100%', background: '#fafafa' }}>
        <Head />
        <MemberRights />
      </div>
    )
  }
}