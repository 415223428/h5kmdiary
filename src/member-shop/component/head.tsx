import React from 'react'
import {Relax,IMap} from 'plume2'
import {cache} from 'config'

@Relax
export default class Head extends React.Component<any, any>{
  props: {
    relaxProps?: {
      level: IMap
    }
  }

  static relaxProps = {
    level: 'level',// 会员信息
  }

  render() {
    const customerName = JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerDetail.customerName;
    const {level} = this.props.relaxProps;
    return (
      <div className="member-header">
        <h2>店铺会员</h2>
        {
          level && level.get('customerLevelId')?
            <div className="store-vip">
              <div className="vip-box">
                <div className="welfare"><span>{(level.get('customerLevelDiscount')*10).toFixed(1)}</span>折</div>
                <p>{customerName}</p>
                <p>{level.get('customerLevelName')}</p>
              </div>
            </div>
            :
            <div className="not-store-vip">
              <div className="vip-box">
                <p>您还不是该店铺会员噢~</p>
                <p>您可联系商家成为会员</p>
              </div>
            </div>
        }
      </div>
    )
  }
}