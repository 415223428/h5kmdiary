import React from 'react'
import { Relax } from 'plume2'
import { IMap, IList } from 'typings/globalType'

@Relax
export default class MemberRights extends React.Component<any, any>{
  props: {
    relaxProps?: {
      level: IMap,
      levelList: IList,
      store: IMap
    }
  }

  static relaxProps = {
    level: 'level', // 会员等级
    levelList: 'levelList',// 店铺的会员体系
    store: 'store'// 店铺信息
  }

  render() {
    const { level, levelList, store } = this.props.relaxProps;
    return (
      <div>
        <div className="rights-box b-1px-t">
          <h2>会员权益</h2>
          <p>成为该店铺的会员，可以享受该店铺会员等级对应的商品折扣率，当商家未按照会员等级设置价格或者单独指定了您的采购价时，商品折扣率不生效。</p>
          <table>
            <tr>
              <th style={{width:"50%"}}>会员等级</th>
              <th style={{width:"50%"}}>折扣</th>
            </tr>
            {
              levelList && levelList.toJS().map((v, k) => {
                return (
                  <tr key={k}>
                    <td>{v.customerLevelName}</td>
                    <td>{(v.customerLevelDiscount * 10).toFixed(1)}</td>
                  </tr>
                )
              })
            }

          </table>
        </div>
        {
          // (!level || !level.get('customerLevelId')) &&
          <div className="contact-supplier">
            <a href={`tel:${store.get('contactMobile')}`} >联系商家</a>
          </div>
        }
      </div>
    )
  }
}