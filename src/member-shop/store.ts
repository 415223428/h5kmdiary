import {msg, Store} from 'plume2'
import {fromJS} from 'immutable'
import {Alert} from "wmkit";
import {config} from 'config'
import MemberActor from './actor/member-actor'
import {fetchStoreVip} from './webapi'

export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if(__DEV__){
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [
      new MemberActor
    ]
  }


  /**
   * 初始化信息
   */
  init = async (storeId) => {
    const res = await fetchStoreVip(storeId) as any;
    if(res.code == config.SUCCESS_CODE) {
      /**店铺pv/uv埋点*/
      (window as any).myPvUvStatis(null, res.context.store.companyInfoId);

      this.dispatch('member:initParams',fromJS(res.context));
    }else{
      msg.emit('storeCloseVisible',true)
    }
  };


  /**
   * 修改是否自营
   */
  changeCompanyType = (companyType) => {
    this.dispatch('filter:companyType',companyType);
  }



}