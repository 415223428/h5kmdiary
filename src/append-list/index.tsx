import React from 'react'
import {fromJS} from 'immutable'
import {StoreProvider} from "plume2";
import {Slider} from 'wmkit'
import AppStore from "./store";
let imageList = new Array

@StoreProvider(AppStore, {debug: __DEV__})
export default class AppendList extends React.Component<any, any> {

  store: AppStore


  componentWillMount() {
    const {appendList} = this.props.location
    this.store.init(appendList)
  }


  render() {
    const images = this.store.state().get('images')
    images.map((v, i) => {
      imageList.push(v.image)
    })
    return (
      <div>
        <Slider data={fromJS(imageList)} height="500" width="400" zoom={true}/>
      </div>
    )
  }
}

