/**
 * Created by feitingting on 2017/8/3.
 */
import {Action, Actor} from "plume2";
import {IList} from "../../../typings/globalType";

export default class AppendActor extends Actor {

  defaultState() {
    return {
      images: []
    }
  }


  @Action('append:init')
  init(state, src: IList) {
    return state.set('images', src)
  }
}