/**
 * Created by feitingting on 2017/8/3.
 */
import {Store} from 'plume2'
import AppendActor from './actor/append-list-actor'
import {IList} from "../../typings/globalType";

export default class AppStore extends Store {

  bindActor() {
    return [new AppendActor]
  }


  init = (res: IList) => {
    this.dispatch('append:init', res)
  }
}