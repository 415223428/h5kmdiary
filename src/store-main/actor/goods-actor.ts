import { Action, Actor, IMap } from 'plume2';
import { IList } from 'typings/globalType';
import { fromJS } from 'immutable';

export default class GoodsActor extends Actor {
  defaultState() {
    return {
      customer: {},
      // 首页sku集合
      skus: [],
      //是否显示分享赚、发圈素材、只看分享赚按钮
      isDistributor: false,
      //分享赚选中的sku
      checkedSku: fromJS({}),
      //是否展示评价相关信息
      isShow: false,
      xSiteVisiable: null,
       // 分享弹出显示与否
       shareVisible: false,
      isfenxiang:false,
      closeImg:true,
      fenxiangId:'',
      closeChoose:true,
      isWechatShare:false,
    };
  }
  /**
   * 首页商品初始化
   */
  @Action('goods: init')
  skuInit(state: IMap, skus: IList) {
    return state.set('skus', skus);
  }

  /**
   *  是否显示分享赚、发圈素材、只看分享赚按钮
   */
  @Action('goods:isDistributor')
  setIsDistributor(state, isDistributor: boolean) {
    return state.set('isDistributor', isDistributor);
  }

  @Action('goodsActor:saveCheckedSku')
  saveCheckedSku(state, sku) {
    return state.set('checkedSku', sku);
  }

  @Action('goodsActor:isShow')
  setIsShow(state, flag: boolean) {
    return state.set('isShow', flag);
  }

  @Action('goods:xSiteVisiable')
  setXSiteVisiable(state, xSiteVisiable: boolean) {
    return state.set('xSiteVisiable', xSiteVisiable)
  }

   /**
   * 切换分享赚弹框显示与否
   */
  @Action('goodsActor:changeShareVisible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }
  //   /**
  //  * 存放点击分享赚时候的sku信息
  //  * @param state
  //  * @param sku
  //  */
  // @Action('goods-list:saveSku')
  // saveSku(state, sku) {
  //   return state.set('checkedSku', sku);
  // }
  //打开分享页面
  @Action('goods-actor:isfenxiang')
  isfenxiang(state){
    return state.set('isfenxiang',true)
  }

  //关闭分享页面
  @Action('goods-actor:closefenxiang')
  closefenxiang(state){
    return state.set('isfenxiang',false)
  }

  //关闭img
  @Action('goods-actor:closeImg')
  closeImg(state){
    return state.set('closeImg',false)
    .set('closeChoose',false)
  }

  //打开Img
  @Action('goods-actor:openImg')
  openImg(state){
    return state.set('closeImg',true)
    .set('closeImg',true)
  }

  @Action('goods-actor:fenxiangId')
  sendId(state,fenxiangId){
    return state.set('fenxiangId',fenxiangId)
  }
  
  @Action('goods-actor:openWechatShare')
  openWechatShare(state){
    return state.set('isWechatShare',true)
  }

  @Action('goods-actor:closeWechatShare')
  closeWechatShare(state){
    return state.set('isWechatShare',false)
  }
  @Action('member:init')
  init(state: IMap, { customer }) {
    return state
      .set('customer', fromJS(customer))
  }
}
