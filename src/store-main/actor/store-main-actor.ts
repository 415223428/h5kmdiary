import { Actor, Action, IMap } from 'plume2'

export default class StoreMainActor extends Actor {
  defaultState() {
    return {
      //关注状态
      liked: false,

      // 店铺信息
      store: {},
      // 加载中...
      loading: true,
      // 在线客服可见状态
      onlineServiceFlag: false,
      // 分享弹出显示与否
      shareVisible: false,
    }
  }

  /**
   * 点击关注
   */
  @Action('store:liked')
  change(state) {
    return state.set('liked', !state.get('liked'))
  }


  /**
   * 店铺信息初始化
   * @param state 
   * @param store 
   */
  @Action('store: info: init')
  infoInit(state: IMap, store: IMap) {
    return state.set('store', store)
  }

  /**
   * 切换分享赚弹框显示与否
   */
  @Action('goodsActor:changeShareVisible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }


  /**
   * 店铺加载中
   * @param state 
   * @param visible 
   */
  @Action('store: loading')
  load(state: IMap, visible: boolean) {
    return state.set('loading', visible)
  }

  /**
   * 设置在线客服可见状态
   */
  @Action('store:setOnlineServiceFlag')
  setOnlineServiceFlag(state: IMap, onlineServiceFlag: boolean) {
    return state.set('onlineServiceFlag', onlineServiceFlag)
  }
}
