import React from 'react';
import { Relax } from 'plume2';

import { IList } from 'typings/globalType';
import { Blank, noop } from 'wmkit';
import { fromJS } from 'immutable';

import GoodsItem from './goods-item';

const noneImg = require('../img/none.png');
@Relax
export default class GoodsList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      customer: any;
      skus: IList;
      changeShareVisible: Function;
      isDistributor: boolean;
      saveCheckedSku: Function;
      isShow: boolean;
      changefenxiang: Function;
       sendId: Function;
       isfenxiang: boolean;
       cloasefenxiang: Function;
       closeImg: boolean;
       iscloseImg: Function;
       isopenImg: Function;
       fenxiangId: String;
       closeChoose:boolean
    };
  };

  static relaxProps = {
    skus: 'skus',
    changeShareVisible: noop,
    isDistributor: 'isDistributor',
    saveCheckedSku: noop,
    isShow: 'isShow',
    changefenxiang: noop,
    sendId: noop,
    customer: 'customer',
    isfenxiang: 'isfenxiang',
    cloasefenxiang: noop,
    iscloseImg: noop,
    closeImg: 'closeImg',
    isopenImg: noop,
    fenxiangId: 'fenxiangId',
    closeChoose:'closeChoose',
  };

  render() {
    const {
      skus,
      changeShareVisible,
      isDistributor,
      saveCheckedSku,
      isShow,
      changefenxiang,
      sendId,
      customer,
      isfenxiang,
      cloasefenxiang,
      closeImg,
      iscloseImg,
      isopenImg,
      fenxiangId,
      closeChoose
    } = this.props.relaxProps;
    return skus.count() > 0 ? (
      <div className="store-content">
        <div className="store-new-title">店铺上新</div>
        <div className="address-box" style={{margin:"0 0.2rem"}}>
        {skus
          .toJS()
          .map((sku) => (
            <GoodsItem
              saveCheckedSku={(sku) => saveCheckedSku(sku)}
              goodsItem={fromJS(sku)}
              changeShareVisible={() => changeShareVisible()}
              isDistributor={isDistributor}
              isShow={isShow}
              sendId={sendId}
                changefenxiang={() => changefenxiang()}
                isfenxiang={isfenxiang}
                customer={customer}
                cloasefenxiang={cloasefenxiang}
                closeImg={closeImg}
                iscloseImg={iscloseImg}
                isopenImg={isopenImg}
                fenxiangId={fenxiangId}
                closeChoose={closeChoose}
            />
          ))}
        <div className="no-more-tips">没有更多了</div>
        </div>
      </div>
    ) : (
      <Blank img={noneImg} content="没有搜到任何商品～" />
    );
  }
}
