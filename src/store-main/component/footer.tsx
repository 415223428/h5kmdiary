import React from 'react';
import { Link, NavLink, Route,Switch } from 'react-router-dom';
import { msg, Relax } from 'plume2';
import { history, WMkit ,noop} from 'wmkit';
import { url } from 'config';

import StoreGoodsCates from '../../shop-goods-cate/index'
import StoreProfile from '../../store-profile/index';
import StoreMain from '../index';

const storeMainDark =require('../img/store-main.png')
const storeMainLight =require('../img/store-main-light.png')
const goodsCateDark =require('../img/goods-cate.png')
const goodsCateLight =require('../img/goods-cate-light.png')
const storeProDark =require('../img/store-profile.png')
const storeProLight =require('../img/store-profile-light.png')
@Relax
export default class Footer extends React.Component<any, any> {
  props: {
    storeId: number;
    path: string;
    relaxProps?: {
      onlineServiceFlag: boolean;
    };
  };

  static relaxProps = {
    tab: 'tab',
    changeTitle: noop,
    onlineServiceFlag: 'onlineServiceFlag'
  };
  render() {
    const { storeId, path } = this.props;
    const {onlineServiceFlag } = this.props.relaxProps;
    const storeMain=`/store-main/${storeId}`,
          goodsCate=`/store-goods-cates/${storeId}`,
          storeProfile=`/store-profile/${storeId}`;
    return (
      <div className="store-foot" style={{paddingBottom:0}}>
        <div className="store-foot-content b-1px-t footer" style={{bottom:0}}>
          {/* <a
            href="javascript:;"
            className={`b-1px-r ${
              path.startsWith(url.STORE_MAIN) ? 'cur' : ''
            }`}
            onClick={() => history.push(`/store-main/${storeId}`)}
          >
            <img src={require('../img/store-main.png')} alt=""/>
            <span>店铺首页</span>
          </a>
          <a
            href="javascript:;"
            className={`b-1px-r ${
              path.startsWith(url.STORE_GOODS_CATES) ? 'cur' : ''
            }`}
            onClick={() => history.push(`/store-goods-cates/${storeId}`)}
          >
            <img src={require('../img/goods-cate.png')} alt=""/>
            <span>商品分类</span>
          </a>
          <a
            href="javascript:;"
            className={`b-1px-r ${
              path.startsWith(url.STORE_PROFILE) ? 'cur' : ''
            }`}
            onClick={() => history.push(`/store-profile/${storeId}`)}
          >
            <img src={require('../img/store-profile.png')} alt=""/>
            <span>店铺档案</span>
          </a>
          <a
                href="javascript:;"
                className={`${path.startsWith(url.MEMBER_SHOP) ? 'cur' : ''}`}
                onClick={() => {
                  if (WMkit.isLoginOrNotOpen()) {
                    // history.push(`/chose-service/${storeId}`);
                    window.location.href = (window as any).ysf('url');
                  } else {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        // history.push(`/chose-service/${storeId}`);
                        window.location.href = (window as any).ysf('url');
                      }
                    });
                  }
                }}
              >
                <img src={require('../img/server.png')} alt=""/>
                <span>店铺客服</span>
              </a> */}


          {/* {onlineServiceFlag &&
            !(window as any).isMiniProgram && (
              <a
                href="javascript:;"
                className={`${path.startsWith(url.MEMBER_SHOP) ? 'cur' : ''}`}
                onClick={() => {
                  if (WMkit.isLoginOrNotOpen()) {
                    // history.push(`/chose-service/${storeId}`);
                    window.location.href = (window as any).ysf('url');
                  } else {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        // history.push(`/chose-service/${storeId}`);
                        window.location.href = (window as any).ysf('url');
                      }
                    });
                  }
                }}
              >

                <img src={require('../img/server.png')} alt=""/>
                <span>店铺客服</span>
              </a>
            )} */}

            <ul>
              <li className={`b-1px-r ${
                path.startsWith(url.STORE_MAIN) ? 'cur' : ''
              }`} style={{width: '100%'}}>
                <Link to={storeMain}>
                  <img src={path.startsWith(url.STORE_MAIN) ?storeMainLight:storeMainDark} alt=""/>
                  <span>店铺首页</span>
                </Link>
              </li>

              <li className={`b-1px-r ${
                path.startsWith(url.STORE_GOODS_CATES) ? 'cur' : ''
              }`} style={{width: '100%'}}>
                <Link to={goodsCate}>
                  <img src={path.startsWith(url.STORE_GOODS_CATES)?goodsCateLight:goodsCateDark} alt=""/>
                  <span>商品分类</span>
                </Link>
              </li>

              <li  className={`b-1px-r ${
                path.startsWith(url.STORE_PROFILE) ? 'cur' : ''
              }`} style={{width: '100%'}}>
                <Link to={storeProfile}>
                  <img src={path.startsWith(url.STORE_PROFILE) ?storeProLight:storeProDark} alt=""/>
                  <span>店铺档案</span>
                </Link>
              </li>
              { !(window as any).isMiniProgram && (
                <li className={`${path.startsWith(url.MEMBER_SHOP) ? 'cur' : ''}`}
                    onClick={() => {
                      // if (WMkit.isLoginOrNotOpen()) {
                      //   window.location.href = (window as any).ysf('url');
                      //   (window as any).openqiyu();
                      // } else {
                      //   msg.emit('loginModal:toggleVisible', {
                      //     callBack: () => {
                      //       window.location.href = (window as any).ysf('url');
                      //       (window as any).openqiyu();
                      //     }
                      //   });
                      // }
                      WMkit.getQiyuCustomer();
                      window.location.href = (window as any).ysf('url');
                      (window as any).openqiyu();
                    }}
                    style={{width: '100%'}}
                >
                  <a >
                    <img src={require('../img/server.png')} alt=""/>
                    <span>店铺客服</span>
                  </a>
                  {/* <Link to={`/store-main/${storeId}`}>
                  <img src={require('../img/server.png')} alt=""/>
                  <span>店铺客服</span>
                </Link> */}
                </li>
                )}
            </ul>
        </div>
      </div>

    );
  }
}
