import React from 'react'
import { Link } from 'react-router-dom'
import { history, noop } from 'wmkit'

export default class SearchBar extends React.Component<any, any> {

  props: {
    storeId: number
  }

  render() {
    const { storeId } = this.props
    return (
      <div className="store-search">
        <div className="search-box" >
          <div className="input-box" style={{opacity:0.6}}>
            <Link to={`/store-goods-search/${storeId}`}>
              <i className="iconfont icon-sousuo" style={{alignItems:'center'}}></i>
              <input type="search" placeholder="搜索" maxLength={100} style={{textAlign:'center'}}/>
            </Link>
          </div>
          <div>
            <a href="javascript:void(0)" className="list-tab"
              onClick={() => history.push('/')}>
              {/* <i className="iconfont icon-dianpushouye"></i> */}
              <img src={require('../img/shouye.png')} alt=""></img>
              <span>首页</span>
            </a>
          </div>
        </div>
      </div>
    )
  }


}
