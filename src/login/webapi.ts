/**
 * Created by feitingting on 2017/7/11.
 */
import { Fetch } from 'wmkit';
/**
 * 登录系统
 * @param   account,passwordFetch
 * @returns
 */
export const login = (account: string, pass: string) => {
  console.log("%c" + account, "color:red;font-size:50px");
  console.log("%c" + pass, "color:red;font-size:50px");
  return Fetch('/login', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: account,
      customerPassword: pass
    })
  });
  // return Fetch('/logincommon', {
  //   method: 'POST',
  //   body: JSON.stringify({
  //     customerAccount: account,
  //     customerPassword: pass
  //   })
  // });
};

/**
 * 获取登录页logo
 * @returns {Promise<Result<T>>}
 */
export const fetchBaseConfig = () => {
  return Fetch('/system/baseConfig', {
    method: 'GET'
  });
};

/**
 * 验证token
 * @returns {Promise<Result<T>>}
 */
export const isLogin = () => {
  return Fetch('/login', {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization:
        'Bearer' + ((window as any).token ? ' ' + (window as any).token : '')
    }
  });
};

/**
 * 获取微信授权登录开关
 */
export const fetchWxLoginStatus = () => {
  return Fetch('/third/login/wechat/status/MOBILE');
};

/**
 * 验证码登录  发送验证码
 * @type {Promise<AsyncResult<T>>}
 */
export const sendCode = (mobile) => {
  return Fetch(`/login/verification/${mobile}`, {
    method: 'POST'
  });
};

/**
 * 验证码登录系统
 * @param account
 * @param verificationCode
 * @returns {Promise<Result<T>>}
 */
export const loginWithVerificationCode = (
  account: string,
  verificationCode: string
) => {
  return Fetch('/login/verification', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: account,
      verificationCode: verificationCode
    })
  });
};

/**
 * 验证手机号检验
 * @param {string} phone
 * @returns {Promise<Result<any>>}
 */
export const checkPhone = (phone: string) => {
  return Fetch(`/ishave/kmaccount/${phone}`, {
    method: 'POST',
  });
};
