import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class AccontTab extends Component<any, any> {
  props: {
    relaxProps?: {
      isALogin: boolean;
      loginChange: Function;
    };
  };

  static relaxProps = {
    isALogin: 'isALogin',
    loginChange: noop
  };

  render() {
    let { isALogin, loginChange } = this.props.relaxProps;
    return (
      <div className="login-switch">
        <span
          className={isALogin ? 'curr' : ''}
          onClick={() => {
            loginChange();
          }}
        >
          账号登录
        </span>
        <span
          className={!isALogin ? 'curr' : ''}
          onClick={() => {
            loginChange();
          }}
        >
          验证码登录
        </span>
      </div>
    );
  }
}
