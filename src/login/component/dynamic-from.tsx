import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop, Button, WMkit } from 'wmkit';
const TimerButton = Button.Timer;
const login=require('../img/login.png')
@Relax
export default class DynamicFrom extends Component<any, any> {
  props: {
    relaxProps?: {
      account: string;
      verificationCode: string;
      showPass: Function;
      doLogin: Function;
      setAccount: Function;
      setVerificationCode: Function;
      sendCode: Function;
    };
  };

  static relaxProps = {
    account: 'account',
    verificationCode: 'verificationCode',
    doLogin: noop,
    setAccount: noop,
    setVerificationCode: noop,
    sendCode: noop
  };

  render() {
    let {
      setAccount,
      account,
      verificationCode,
      doLogin,
      setVerificationCode,
      sendCode
    } = this.props.relaxProps;
    if (account.length > 11) {
      account = account.substring(0, 11);
    }
    return (
      <div style={{ marginTop: 29, width: '80%' }}>
        <div className="login-item">
          <div className="inputBox eyes-box">
            {/* <i className={`iconfont icon-iconguanbiyanjing`} /> */}
            <img src={require('../img/phone.png')} alt=""/>
            <input
              className="formInput"
              type="tel"
              pattern="[0-9]*"
              placeholder="请输入手机号"
              value={account}
              maxLength={11}
              onChange={(e) => setAccount(e.target.value)}
            />
          </div>
        </div>
        <div className="login-item">
          <div className="inputBox eyes-box" style={{ paddingRight: 0 }}>
            {/* <i className={`iconfont icon-iconguanbiyanjing`} /> */}
            <img src={require('../img/code.png')} alt=""/>
            <input
              className="formInput sendCodeInput"
              type="text"
              placeholder="请输入手机验证码"
              pattern="/^[0-9]{6}$/"
              value={verificationCode}
              onChange={(e) => setVerificationCode(e.target.value)}
              maxLength={6}
            />
            <TimerButton
              text="获取验证码"
              resetWhenError={true}
              shouldStartCountDown={() => this._beforeSendCode(account)}
              onClick={() => this._sendCode(account)}
              defaultStyle={{
                // color: '#000',
                fontSize: '0.26rem',
                // borderColor: 'transparent',
                // borderLeftColor: '#e5e5e5',
                padding: '0.11rem',
                // lineHeight: '0.3rem',
                // height: '0.3rem',
                // paddingLeft: '9px'
                fontFamily: 'PingFang SC',
                fontWeight: '500',
                color: 'rgba(255,77,77,1)',
                // width: '1.5rem',
                // height: '.48rem',
                border: '1px solid rgba(255, 77, 77, 1)',
                borderRadius: '4px',
                margin:'auto 0 auto auto',
              }}
            />
          </div>
        </div>
        {/* <div className="findCode" /> */}
        <div className="login-btnBox">
          {/* <button className="login-btn" onClick={() => doLogin()}>
            登录
          </button> */}
          <img src={login} alt="" onClick={() => doLogin()}/>
        </div>
      </div>
    );
  }

  /**
   * 发送验证码前校验手机号是否填写或正确
   * @returns {boolean}
   * @private
   */
  _beforeSendCode = (account) => {
    return WMkit.testTel(account);
  };

  /**
   * 发送验证码
   * @private
   */
  _sendCode = (account) => {
    const { sendCode } = this.props.relaxProps;
    return sendCode(account);
  };
}
