import React, { Component } from 'react';
import { Relax } from 'plume2';
import { _, noop } from 'wmkit';

@Relax
export default class QuickLogin extends Component<any, any> {
  props: {
    relaxProps?: {
      weChatQuickLogin: Function;
    };
  };

  static relaxProps = {
    weChatQuickLogin: noop
  };

  render() {
    const { weChatQuickLogin } = this.props.relaxProps;

    return (
      <div className="login-list">
        {/* {_.isWeixin() && (
          <a onClick={() => weChatQuickLogin()}>
            <img src={require('../img/weChat.png')} alt="" />
            <span>微信登录</span>
          </a>
        )} */}
         <a onClick={() => weChatQuickLogin()}>
            <img src={require('../img/weChat.png')} alt="" />
            <span>微信登录</span>
          </a>
        {/*<a href="">
          <img src={require('../img/qq.png')} alt="" />
          <span>QQ登录</span>
        </a>
        <a href="">
          <img src={require('../img/weiBo.png')} alt="" />
          <span>微博登录</span>
        </a> */}
      </div>
    );
  }
}
