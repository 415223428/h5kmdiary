import { Store } from 'plume2';
import parse from 'url-parse';

import { Alert, history, WMkit, storage, _, wxAuth } from 'wmkit';
import { cache, config } from 'config';
import LoginActor from './actor/login-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new LoginActor()];
  }

  constructor(props) {
    super(props);
    (window as any)._store = this;
  }

  init = async () => {
    // 初始化基本信息和微信授权登录开关
    const res = await Promise.all([
      webapi.fetchBaseConfig(),
      webapi.fetchWxLoginStatus()
    ]);
    if (
      res[0].code == config.SUCCESS_CODE &&
      res[1].code == config.SUCCESS_CODE
    ) {
      this.dispatch('login:init', JSON.parse((res[0].context as any).pcLogo));
      this.dispatch('login:wxFlag', res[1].context);
    }
  };

  showPass = () => {
    const showpass = this.state().get('isShowpwd');
    this.dispatch('login:showPass', showpass);
  };

  doLogin = async () => {
    // true：账号密码登录  false：验证码登录
    const isALogin = this.state().get('isALogin');
    let result = null;
    if (isALogin) {
      /**
       * 获取用户名和密码，并去除所有空格*/
      const account = this.state()
        .get('account')
        .trim()
        .replace(/\s/g, '');
      const password = this.state()
        .get('password')
        .trim()
        .replace(/\s/g, '');
      // if (WMkit.testTel(account) && WMkit.testPass(password)) {
      if ( WMkit.testPass(password)) {
        const base64 = new WMkit.Base64();
        const res = await webapi.login(
          base64.urlEncode(account),
          base64.urlEncode(password)
        );
        result = res;
      }
    } else {
      /**
       * 获取用户名和验证码，并去除所有空格*/
      const account = this.state()
        .get('account')
        .trim()
        .replace(/\s/g, '');
      const verificationCode = this.state()
        .get('verificationCode')
        .trim()
        .replace(/\s/g, '');
      if (
        // WMkit.testTel(account) &&
        WMkit.testVerificationCode(verificationCode)
      ) {
        const res = await webapi.loginWithVerificationCode(
          account,
          verificationCode
        );
        result = res;
      }
    }
    const { context } = result;
    /**
     * 登录成功时*/
    if ((result as any).code == config.SUCCESS_CODE) {
      //获取审核状态
      _.switchLogin(context);
    }
    //密码输错5次，按钮设灰色
    else if ((result as any).code == 'K-010004') {
      this.transaction(() => {
        this.dispatch('login:buttonstate');
        this.dispatch('login:buttonvalue', result.message);
      });
      Alert({
        text: result.message
      });
      return;
    } else {
      /**
       * 登录失败，提示各类出错信息*/
      Alert({
        text: result.message
      });
      return false;
    }
  };

  setAccount = (account) => {
    this.transaction(() => {
      //登录按钮恢复可点击状态
      this.dispatch('login:enableButton');
      this.dispatch('login:account', account);
      this.dispatch('login:accountChange', this.state().get('accountChange'));
    });
  };

  setPressError = (errorPress: boolean) => {
    this.dispatch('login:errorPress', errorPress);
  };

  setPassword = (pass) => {
    this.dispatch('login:password', pass);
  };

  /**
   * 输入验证码
   * @param code
   */
  setVerificationCode = (code) => {
    this.dispatch('login:verificationCode', code);
  };

  /**忘记密码*/
  forgetPass = async () => {
    history.push('/user-safe-password');
  };

  /*切换登录方式*/
  loginChange = () => {
    this.dispatch('login:loginChange');
  };

  /**
   * 微信登录授权
   */
  weChatQuickLogin = () => {
    const url = location.href;

    wxAuth.getAuth(url.split('?')[0],'login');
  };

  /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
  /** ** ** ** ** ** ** ** ** ** ** ** ** * 验证码登录 * ** ** ** ** ** ** ** ** ** ** ** ** **/
  /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

  /**
   * 发送验证码给手机号码
   * @returns {Promise<Result<ReturnResult>>}
   */
  sendCode = (mobile) => {
    return webapi.sendCode(mobile).then((res) => {
      if (res.code === config.SUCCESS_CODE) {
        Alert({
          text: '验证码已发送，请注意查收！',
          time: 1000
        });
      } else {
        Alert({
          text: res.message,
          time: 1000
        });
        return Promise.reject(res.message);
      }
    });
  };
}
