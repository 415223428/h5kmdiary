import React from 'react';
import { Link } from 'react-router-dom';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import { noop, _ } from 'wmkit';
import AccontFrom from './component/account-form';
import DynamicFrom from './component/dynamic-from';
import AccountTab from './component/login-tab';
import QuickLogin from './component/quick-login';
const styles = require('./css/style.css');
const logo = require('./img/logo.png');
import { WMkit } from 'wmkit';
import wx from 'weixin-js-sdk';

const logo1 = require('./img/logo1.png');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class Login extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '登录-康美日记';
    this.store.init();
  }

  constructor(props: any) {
    super(props);
    wx.miniProgram.getEnv(function (res) {
      (window as any).isMiniProgram = res.miniprogram;
    });
    let searchObj = WMkit.searchToObj(this.props.location.search);
    if (searchObj) {
      let channel = (searchObj as any).channel;
      //分销渠道是店铺还是商城
      let channelType = channel == 'shop' ? '2' : '1';
      if ((searchObj as any).inviteeId)
        //并分销员会员ID和分销渠道存入缓存，分销渠道为商城
        WMkit.setShareInfoCache((searchObj as any).inviteeId, channelType);
    }
  }

  render() {
    const pcLogo = this.store.state().toJS().pcLogo;
    const isALogin = this.store.state().get('isALogin');
    const wxFlag = this.store.state().get('wxFlag');
    return (
      <div className="login-box">
        <div className="login-container">
          <div className="logo">
            <img
              // src={pcLogo && pcLogo.length >= 1 ? pcLogo[0].url : logo}
              src={logo1}
              style={{width: '1.65rem',height: '1.65rem'}}
              className="logoImg"
            />
          </div>
          <AccountTab />
          {isALogin ? <AccontFrom /> : <DynamicFrom />}
          <div className="register-account">
            <Link to="/register">
              <span className="dark-text">注册账号</span>
              {/* <i
                style={{ marginLeft: 5, fontSize: 12 }}
                className="iconfont icon-xiayibu1 dark-text"
              /> */}
            </Link>

            <Link to="/user-safe-password">
              <span className="dark-text">忘记密码</span>
            </Link>
          </div>
        </div>
        <div style={{ width: '100%' }}>
        {_.isWeixin() && (
          <div>
          <div className="footTitle">
            <p>其他登录方式</p>
          </div>
          {wxFlag && <QuickLogin />}
          </div>
          )}
          <div className="footer-copyright">
            <p>粤ICP备19120716号-1</p>
          </div>
        </div>
      </div>
    );
  }
}
