import { Store } from 'plume2';
import moment from 'moment';
import { storage, history, Alert, FormRegexUtil } from 'wmkit';
import { cache, Const } from 'config';
import { deliver } from './webapi';
import FormActor from './actor/form-actor';

export default class AppStore extends Store {
  bindActor() {
    return [new FormActor()];
  }

  constructor(props) {
    super(props);
    //debug
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  /**
   * 初始化
   */
  init = (rid, storeId) => {
    let logistic = storage('session').get(cache.LOGISTICS_INFO);
    let logisticsNo; // 物流单号
    let time = new Date(); // 发货时间
    let formatTime = moment(time).format(Const.DATE_FORMAT);
    let expressName; // 物流公司名称
    let expressCode; // 物流公司标准编码

    if (logistic) {
      logistic = JSON.parse(logistic);
      logisticsNo = (logistic as any).logisticsNo;
      time = (logistic as any).time;
      formatTime = (logistic as any).formatTime;
      expressName = (logistic as any).expressName;
      expressCode = (logistic as any).expressCode;
    }

    this.transaction(() => {
      this.dispatch('formActor: changeStoreId', storeId);
      this.dispatch('formActor: changeForm', {
        rid: rid,
        logisticsNo: logisticsNo,
        time: time,
        expressName: expressName,
        formatTime: formatTime,
        expressCode: expressCode
      });
    });
  };

  /**
   * 保存物流信息
   */
  save = async () => {
    const form = this.state().get('form');

    if (
      !FormRegexUtil(form.get('expressName'), '物流公司', { required: true }) ||
      !FormRegexUtil(form.get('logisticsNo'), '物流单号', { required: true }) ||
      !FormRegexUtil(form.get('formatTime'), '发货时间', { required: true })
    ) {
      return;
    }

    const params = {
      logisticCompanyCode: form.get('expressCode'),
      logisticCompany: form.get('expressName'),
      logisticNo: form.get('logisticsNo'),
      date: moment(new Date(form.get('time'))).format(Const.SECONDS_FORMAT)
    };

    const { code, message } = await deliver(form.get('rid'), params);

    if (code == 'K-000000') {
      Alert({ text: '添加成功' });
      storage('session').del(cache.LOGISTICS_INFO);
      history.push('/refund-list');
    } else {
      Alert({ text: message });
    }
  };

  /**
   * 修改form
   */
  changeForm = (form) => {
    this.dispatch('formActor: changeForm', form);
  };

  /**
   * 选择物流公司
   */
  chooseCompany = (storeId) => {
    const form = this.state().get('form');

    let logistic = {
      rid: form.get('rid'),
      expressCode: form.get('expressCode'),
      time: form.get('time'),
      expressName: form.get('expressName'),
      formatTime: form.get('formatTime'),
      logisticsNo: form.get('logisticsNo')
    };

    storage('session').set(cache.LOGISTICS_INFO, JSON.stringify(logistic));

    history.push({
      pathname: '/logistics-select',
      state: { storeId: this.state().get('storeId') }
    });
  };
}
