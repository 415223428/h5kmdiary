import {Fetch} from 'wmkit'

type DeliverParam = {
  logisticCompanyCode: string;
  logisticCompany: string;
  logisticNo: string;
  date: string;
}


/**
 * 填写物流信息
 */
export const deliver = (rid: string, values: DeliverParam) => {
  return Fetch<Result<any>>(`/return/deliver/${rid}`, {
    method: 'POST',
    body: JSON.stringify({
      code: values.logisticCompanyCode,
      company: values.logisticCompany,
      no: values.logisticNo,
      createTime: values.date
    })
  })
};