import React from 'react'
import moment from 'moment'
import DatePicker from 'react-mobile-datepicker'
import {Relax} from "plume2";
import {FormSelect, FormInput, noop} from 'wmkit'
import {Const} from 'config'

let currentDate = new Date()
const maxDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1)

@Relax
export default class FormItem extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeForm: Function,
      chooseCompany: Function,
      form: any
    }
  }


  static relaxProps = {
    changeForm: noop,
    chooseCompany: noop,
    form: 'form'
  }


  constructor(props: any) {
    super(props)
    this.state = {
      isOpen: false
    }
  }


  render() {
    const {form} = this.props.relaxProps
    let rid = form.get('rid')
    let time = form.get('time')

    return (
        <div className="register-box">
          <FormSelect labelName={'物流公司'} placeholder={'请选择物流公司'} selected={{key: 0, value: form.get('expressName')}}
                      onPress={() => this.chooseCompany()}/>
          <FormInput label="物流单号" placeHolder={'点此输入物流单号'} maxLength={50} defaultValue={form.get('logisticsNo')}
                     onChange={(e) => this.handleInput(e.target.value)}/>
          <FormSelect labelName={'发货时间'} placeholder={'请选择发货时间'} selected={{key: 1, value: form.get('formatTime')}}
                      onPress={this.handleClick}/>
          <DatePicker
              value={new Date(form.get('time'))}
              isOpen={this.state.isOpen}
              onSelect={this.handleSelect}
              onCancel={this.handleCancel}
              theme="ios"
              max={maxDate}
          />
        </div>
    )
  }


  handleClick = () => {
    this.setState({isOpen: true});
  }


  handleCancel = () => {
    this.setState({isOpen: false});
  }


  handleSelect = (time) => {
    this.setState({isOpen: false});
    this.props.relaxProps.changeForm({time: time, formatTime: moment(time).format(Const.DATE_FORMAT)});
  }


  handleInput = (v) => {
    this.setState({logisticsNo: v})
    this.props.relaxProps.changeForm({logisticsNo: v});
  }


  chooseCompany = () => {
    this.props.relaxProps.chooseCompany()
  }
}
