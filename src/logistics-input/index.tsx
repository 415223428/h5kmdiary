import React from 'react'
import {StoreProvider} from 'plume2'
import {Button} from 'wmkit'
import AppStore from './store'
import FormItem from './components/form-item'

const LongBlueButton = Button.LongBlue1
const save= require('./img/save.png')
@StoreProvider(AppStore)
export default class Logistics extends React.Component<any, any> {
  store: AppStore


  constructor(props) {
    super(props)
  }


  componentDidMount() {
    const {rid} = this.props.match.params;
    this.store.init(rid,this.props.location.state.storeId)
  }


  render() {
    return (
        <div className="content register-content" style={{
          height:'100vh',
        }}>
          <FormItem/>
          <div className="register-btn" style={{
            padding:0
          }}>
            {/* <LongBlueButton text="保存" onClick={() => this.store.save()}/> */}
            <img src={save} alt="" style={{width:'100%'}} onClick={() => this.store.save()}/>
          </div>
        </div>
    )
  }
}
