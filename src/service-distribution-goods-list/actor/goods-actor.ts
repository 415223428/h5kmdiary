import { Action, Actor } from 'plume2';
import { fromJS } from 'immutable';

export default class GoodsActor extends Actor {
  defaultState() {
    return {
      // 分享弹出显示与否
      shareVisible: false,
      // 分享赚选中的sku
      chosenSku: {}
    };
  }

  /**
   * 切换分享赚弹框显示与否
   */
  @Action('goodsActor:changeShareVisible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }

  /**
   * 分享赚选中的sku
   * @param state
   * @param context
   * @returns {any}
   */
  @Action('goodsActor:setSkuContext')
  setSpuContext(state, context) {
    return state.set('chosenSku', fromJS(context));
  }
}
