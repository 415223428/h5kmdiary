import React from 'react';
import { Link } from 'react-router-dom';
import { msg } from 'plume2';
import { WMImage, _, Alert, history, WMkit } from 'wmkit';
import { GoodsNum } from 'biz';

const noneImg = require('../img/list-none.png');

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = { ...props };
  }

  render() {
    const {
      goodsItem,
      saveCheckedSku,
      changeShareVisible,
      // FIXME 销量、评价、好评展示与否，后期读取后台配置开关
      isShow,
      isDistributor
    } = this.state;

    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 起订量
    const count = goodsInfo.get('count') || 0;
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;

    let containerClass = invalid ? 'goods-list invalid-goods' : 'goods-list';

    //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
    //好评率
    let favorableRate = '100';
    if (
      goodsInfo.get('goodsEvaluateNum') &&
      goodsInfo.get('goodsEvaluateNum') != 0
    ) {
      favorableRate = _.mul(
        _.div(
          goodsInfo.get('goodsFavorableCommentNum'),
          goodsInfo.get('goodsEvaluateNum')
        ),
        100
      ).toFixed(0);
    }

    //评论数
    let evaluateNum = '暂无';
    const goodsEvaluateNum = goodsInfo.get('goodsEvaluateNum');
    if (goodsEvaluateNum) {
      if (goodsEvaluateNum < 10000) {
        evaluateNum = goodsEvaluateNum;
      } else {
        const i = _.div(goodsEvaluateNum, 10000).toFixed(1);
        evaluateNum = i + '万+';
      }
    }

    //销量
    let salesNum = '暂无';
    const goodsSalesNum = goodsInfo.get('goodsSalesNum');
    if (goodsSalesNum) {
      if (goodsSalesNum < 10000) {
        salesNum = goodsSalesNum;
      } else {
        const i = _.div(goodsSalesNum, 10000).toFixed(1);
        salesNum = i + '万+';
      }
    }
    // 预估点数
    let pointNum = goodsInfo.get('kmPointsValue');
    //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆
    var data = {
      // 要发送到七鱼的商品或者订单的数据对象
      picture: goodsInfo.get('goodsInfoImg'),
      title: goodsInfo.get('goodsInfoName'),
      price: _.addZero(goodsInfo.get('marketPrice')),
      url: `https://m.kmdiary.com/goods-detail/ + ${ goodsInfo.get('goodsInfoId')}`,
      showCustomMsg: 1,
    }
    return (
      <div
        key={id}
        className={containerClass}
        // onClick={() =>
        //   history.push('/goods-detail/' + goodsInfo.get('goodsInfoId'))
        // }
         onClick={() => window.parent.postMessage(data, '*')}
      >
        <div className="img-box">
          <WMImage
            mode="pad"
            src={goodsInfo.get('goodsInfoImg') || noneImg}
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail b-1px-b">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>

          {/* 评价 */}
          {isShow ? (
            <div className="goods-evaluate">
              <span className="goods-evaluate-spn">{salesNum}销量</span>
              <span className="goods-evaluate-spn mar-lr-28">
                {evaluateNum}
                评价
              </span>
              <span className="goods-evaluate-spn">
                {favorableRate}
                %好评
              </span>
              {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
            </div>
          ) : (
              <div className="goods-evaluate">
                <span className="goods-evaluate-spn">{salesNum}销量</span>
              </div>
            )}

          {goodsInfo.get('companyType') == 0 && (
            <div className="marketing">
              <div className="self-sales">自营</div>
            </div>
          )}

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {_.addZero(goodsInfo.get('marketPrice'))}
              </span>
              {isDistributor && (
                <span className="commission">
                  &nbsp;/&nbsp;赚{_.addZero(
                    goodsInfo.get('distributionCommission')
                  )}
                </span>
              )}
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            {!invalid ? (
              <div className="social-btn-box">
                {isDistributor ? (
                  <div style={{ display: 'flex' }}>
                    <div
                      className="social-btn social-btn-ghost"
                      onClick={async (e) => {
                        e.stopPropagation();
                        const result = await WMkit.getDistributorStatus();
                        if ((result.context as any).distributionEnable) {
                          history.push(
                            '/graphic-material/' + goodsInfo.get('goodsInfoId')
                          );
                        } else {
                          let reason = (result.context as any).forbiddenReason;
                          msg.emit('bStoreCloseVisible', {
                            visible: true,
                            reason: reason
                          });
                        }
                      }}
                    >
                      发圈素材
                    </div>
                    <div
                      className="social-btn"
                      onClick={async (e) => {
                        e.stopPropagation();
                        const result = await WMkit.getDistributorStatus();
                        if ((result.context as any).distributionEnable) {
                          if ((window as any).isMiniProgram) {
                            //存储此时的sku
                            saveCheckedSku(goodsInfo);
                            changeShareVisible();
                          } else {
                            Alert({
                              text: '请到小程序端进行该操作!'
                            });
                          }
                        } else {
                          let reason = (result.context as any).forbiddenReason;
                          msg.emit('bStoreCloseVisible', {
                            visible: true,
                            reason: reason
                          });
                        }
                      }}
                    >
                      分享赚
                    </div>
                  </div>
                ) : (
                    <GoodsNum
                      value={buyCount}
                      max={stock}
                      disableNumberInput={invalid}
                      goodsInfoId={goodsInfo.get('goodsInfoId')}
                    />
                  )}
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
