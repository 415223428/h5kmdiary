import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

const styles = require('../css/style.css');

let tab = [
  {
    name: 'goodsCate',
    label: '分类'
  },
  {
    name: 'goodsSort',
    label: '排序'
  },
  {
    name: 'goodsFilter',
    label: '筛选'
  }
];
const sorts = {
  default: '综合',
  dateTime: '最新',
  salesNum: '销量',
  price: '价格',
  evaluateNum: '评论数',
  praise: '好评',
  collection: '收藏'
};

@Relax
export default class GoodsTab extends Component<any, any> {
  props: {
    relaxProps?: {
      openShade: Function;
      closeShade: Function;
      tabName: string;
      selectedCate: any;
      sortType: any;
    };
  };

  static relaxProps = {
    openShade: noop,
    closeShade: noop,
    tabName: 'tabName',
    selectedCate: 'selectedCate',
    sortType: 'sortType'
  };

  render() {
    const {
      openShade,
      closeShade,
      tabName,
      selectedCate,
      sortType
    } = this.props.relaxProps;
    // 排序字段
    const sort = sortType.get('type');

    return (
      <div className={styles.menuBar}>
        <div className={styles.menuContent + ' b-1px-t'}>
          {tab.map((v) => {
            // 是否是当前已展开的tab
            const match = v.name === tabName;

            return (
              <div
                className="item"
                key={v.name}
                onClick={() => {
                  // 在相同的tab上点击时，认为是要关闭tab
                  match ? closeShade() : openShade(v.name);
                }}
              >
                <span className={match ? 'new-theme-text' : ''}>
                  {v.name === 'goodsCate'
                    ? selectedCate.get('cateName')
                    : v.name == 'goodsSort'
                      ? sorts[sort] || '排序'
                      : v.label}
                </span>
                {v.name === 'goodsFilter' ? (
                  <i className="iconfont icon-shaixuan" />
                ) : match ? (
                  <i
                    className="iconfont icon-jiantou2 new-theme-text"
                    style={{ transform: 'rotate(180deg)' }}
                  />
                ) : (
                  <i className="iconfont icon-jiantou2" />
                )}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
