import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { history } from 'wmkit';

@Relax
export default class GoodsSearchBar extends React.Component<any, any> {
  props: {
    relaxProps?: {
      queryString: string;
    };
  };

  static relaxProps = {
    queryString: 'queryString'
  };

  render() {
    const { queryString } = this.props.relaxProps;

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div
              className="input-box"
              onClick={() =>
                //key: distributeGoods 表示是分销推广商品的搜索
                history.push({
                  pathname: '/service-search',
                  state: { queryString: queryString, key: 'distributeGoods' }
                })
              }
            >
              <i className="iconfont icon-sousuo" />
              <input
                type="text"
                value={queryString}
                placeholder="输入搜索商品名称"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
