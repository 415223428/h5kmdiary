import React, { Component } from 'react';
import { fromJS } from 'immutable';
import { IMap, Relax } from 'plume2';

import { IList } from 'typings/globalType';

import { Blank, ListView, noop, WMkit, Loading } from 'wmkit';
import GoodsItem from './goods-item';

const noneImg = require('../img/list-none.png');

@Relax
export default class GoodsList extends Component<any, any> {
  _listView: any;

  props: {
    relaxProps?: {
      changeShareVisible: Function;
      // 选中的类目
      selectedCate: IMap;
      sortType: IMap;
      selectedBrandIds: any;
      queryString: string;
      initialEnd: boolean;
      handleDataReached: (data: Object) => void;
      isNotEmpty: boolean;
      selectSelfCompany: boolean;
      selectedItem: boolean;
      goodsProps: IList;
      saveCheckedSku: Function;
    };
  };

  static relaxProps = {
    selectedCate: 'selectedCate',
    sortType: 'sortType',
    selectedBrandIds: 'selectedBrandIds',
    queryString: 'queryString',
    initialEnd: 'initialEnd',
    handleDataReached: noop,
    changeShareVisible: noop,
    isNotEmpty: 'isNotEmpty',
    selectSelfCompany: 'selectSelfCompany',
    selectedItem: 'selectedItem',
    goodsProps: 'goodsProps',
    saveCheckedSku: noop
  };
  constructor(props) {
    super(props);
    this.state = {
      // 防止修改商品数量,引起参数变化,重新查询商品列表(刷新页面,体验不好)
      dtoListParam:
        JSON.parse(localStorage.getItem(WMkit.purchaseCache())) || []
    };
  }
  render() {
    let {
      selectedCate,
      sortType,
      queryString,
      initialEnd,
      selectedBrandIds,
      handleDataReached,
      selectSelfCompany,
      goodsProps,
      changeShareVisible,
      saveCheckedSku
    } = this.props.relaxProps;

    // 组件刚执行完mount，搜索条件没有注入进来，先不加载ListView，避免先进行一次无条件搜索，再立刻进行一次有条件搜索
    if (!initialEnd) {
      return <Loading />;
    }

    // 搜索条件
    // 关键字
    const keyword = queryString;

    // 目录编号
    const cateId =
      selectedCate.get('cateId') == 0 ? null : selectedCate.get('cateId');

    // 只展示自营商品
    const companyType = selectSelfCompany ? '0' : '';

    // 排序标识，默认为按时间倒序
    let sortFlag = 1;
    if (sortType.get('type') == 'dateTime') {
      sortFlag = 1;
    } else if (sortType.get('type') == 'salesNum') {
      sortFlag = 4;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'desc'
    ) {
      sortFlag = 2;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 3;
    } else if (
      sortType.get('type') == 'commission' &&
      sortType.get('sort') == 'desc'
    ) {
      sortFlag = 8;
    } else if (
      sortType.get('type') == 'commission' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 9;
    }

    // 品牌
    const brandIds = (selectedBrandIds && selectedBrandIds.toJS()) || null;

    // 属性值
    let propDetails = [];
    if (goodsProps && goodsProps.count() > 0) {
      propDetails = goodsProps
        .map((v) => {
          let prop = fromJS({});
          const detailIds = v
            .get('goodsPropDetails')
            .filter((de) => de.get('checked') === true)
            .map((de) => de.get('detailId'));
          return prop
            .set('propId', v.get('propId'))
            .set('detailIds', detailIds);
        })
        .filter((v) => v.get('detailIds') && v.get('detailIds').size > 0)
        .toJS();
    }

    let dataUrl;
    if (WMkit.isLoginOrNotOpen()) {
      dataUrl = '/goods/shop/add-distributor-goods';
    }

    // 是否是分销员
    let isDistributor = WMkit.isDistributor();

    return (
      <ListView
        className="view-env"
        url={dataUrl}
        style={{ height: 'calc(100vh - 88px)' }}
        params={{
          likeGoodsName: keyword,
          cateId: cateId,
          companyType: companyType,
          brandIds: brandIds,
          propDetails: propDetails,
          sortFlag: sortFlag,
          esGoodsInfoDTOList: this.state.dtoListParam
        }}
        dataPropsName={'context.esGoodsInfoPage.content'}
        otherProps={['goodsIntervalPrices']}
        renderRow={(item: any) => {
          return (
            <GoodsItem
              changeShareVisible={() => changeShareVisible()}
              goodsItem={fromJS(item)}
              key={item.id}
              saveCheckedSku={saveCheckedSku}
              isShow={true}
              isDistributor={isDistributor}
            />
          );
        }}
        renderEmpty={() => <Blank img={noneImg} content="没有搜到任何商品～" />}
        ref={(_listView) => (this._listView = _listView)}
        onDataReached={handleDataReached}
      />
    );
  }
}
