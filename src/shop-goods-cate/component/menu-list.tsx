import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';
import { history } from 'wmkit';

@Relax
export default class MenuList extends React.Component<any, any> {
  props: {
    // 表示从非列表跳转进入还是从列表进入 true: 非列表
    flag: boolean;
    storeId: number;
    relaxProps?: {
      cateList: IList;
    };
    handleClick: Function;
  };

  static relaxProps = {
    cateList: 'cateList'
  };

  render() {
    const { cateList } = this.props.relaxProps;
    const { storeId, flag } = this.props;
    if (!cateList) {
      return null;
    }
    const cate = this._loop(cateList, cateList, 0);
    const handleClick = this.props.handleClick;
    return (
      <div className="menu-list" style={{ left: 0 }}>
        <div className="menu-content b-1px-t">
          <a
            href="javascript:;"
            className="new-btn"
            onClick={() => {
              flag
                ? history.push(`/store-goods-list/${storeId}`)
                : handleClick('', '分类');
            }}
          >
            全部商品
          </a>
          {// 二级分类
          cate.toJS().map((level2Cate, level2Index) => {
            return (
              <dl className="menu-box" key={level2Cate.storeCateId}>
                <dt
                  onClick={() =>
                    handleClick(level2Cate.storeCateId, level2Cate.cateName)
                  }
                >
                  {level2Cate.cateName}
                  <i className="iconfont icon-jiantou1" />
                </dt>
                {// 三级分类
                level2Cate.children &&
                  level2Cate.children.map((level3Cate, level3Index) => {
                    return (
                      <dd
                        key={level3Cate.storeCateId}
                        onClick={() =>
                          handleClick(
                            level3Cate.storeCateId,
                            level3Cate.cateName
                          )
                        }
                      >
                        {level3Cate.cateName}
                      </dd>
                    );
                  })}
              </dl>
            );
          })}
        </div>
      </div>
    );
  }

  //装载店铺分类数据
  _loop = (oldCateList, cateList, parentCateId) =>
    cateList
      .filter((cate) => cate.get('cateParentId') === parentCateId)
      .map((item) => {
        const childCates = oldCateList.filter(
          (cate) => cate.get('cateParentId') == item.get('storeCateId')
        );
        if (childCates && childCates.count()) {
          item = item.set('children', childCates);
          this._loop(oldCateList, childCates, item.get('storeCateId'));
        }
        return item;
      });
}
