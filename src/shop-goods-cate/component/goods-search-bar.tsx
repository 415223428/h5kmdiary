import React from 'react'
import { Link } from 'react-router-dom'
import { history } from 'wmkit'

export default class GoodsSearchBar extends React.Component<any, any> {

  props: {
    storeId: number
  }

  render() {
    const { storeId } = this.props

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div className="input-box">
              <i className="iconfont icon-sousuo"></i>
              <input  type="text" placeholder="搜索" style={{textAlign:'center'}}
                      onClick={() => history.push(`/store-goods-search/${storeId}`)} />
            </div>
            <div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
