import { Fetch, WMkit } from 'wmkit';

/**
 * 获取全部分类信息
 * @type {Promise<AsyncResult<T>>}
 */
export const getAllCates = (id: number) => {
  const params = {
    storeId: id
  };
  return Fetch('/storeCate', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 获取店铺基本信息
 * @param id 店铺Id
 */
export const fetchStoreInfo = (id) => {
  const params = {
    storeId: id
  };
  return WMkit.isLoginOrNotOpen()
    ? Fetch(`/store/storeInfo`, {
        method: 'POST',
        body: JSON.stringify(params)
      })
    : Fetch(`/store/unLogin/storeInfo`, {
        method: 'POST',
        body: JSON.stringify(params)
      });
};
