import { fromJS } from "immutable";
import {msg, Store} from "plume2";

import CateActor from "./actor/cate-actor";
import * as webApi from "./webapi";


export default class AppStore extends Store {
  bindActor() {
    return [new CateActor]
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  /**
   * 初始化分类数据
   * @returns {Promise<void>}
   */
  init = async (id: number) => {
    const res = await webApi.getAllCates(id)
    if (res.code == 'K-000000') {
      this.dispatch('cate: init', { cateList: fromJS(res.context), storeId: id })
    }

    const storeRes = await webApi.fetchStoreInfo(id) as any;
    if (storeRes.code == 'K-000000') {
      /**店铺pv/uv埋点*/
      (window as any).myPvUvStatis(null, storeRes.context.companyInfoId);
    } else {
      msg.emit('storeCloseVisible',true)
    }
  }
}