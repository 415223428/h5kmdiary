import React from "react";
import { StoreProvider } from "plume2";

import { history } from "wmkit";

import AppStore from "./store";
import MenuList from "./component/menu-list";
import GoodsSearchBar from "./component/goods-search-bar";
import Search from '../shop-search'

/**
 * 商品分类
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class GoodsCate extends React.Component<any, any> {

  store: AppStore

  componentDidMount() {
    let sid;
    if (this.props.match && this.props.match.params) {
      sid = this.props.match.params.sid
    }
    this.store.init(sid || this.props.storeId)
  }


  render() {
    let sid;
    if (this.props.match && this.props.match.params) {
      sid = this.props.match.params.sid
    }
    const source = this.props.source == 'goodsList' ? 'goodsList' : ''
    const handleClick = this.props.handleClick ? this.props.handleClick
      : (cateId, cateName) => history.push({
        pathname: `/store-goods-list/${sid || this.props.storeId}`,
        search: '?cid=' + cateId + '&cname=' + encodeURIComponent(cateName)
      })
    // 商品列表的分类可以整个隐藏，在分类组件里而不是在列表里面直接返回null，保证分类只mount一次，避免在商品列表每次展开都获取一次分类数据
    const hide = this.props.hide
    if (hide) {
      return null
    }

    return (
      <div>
        <div className="cate-box" style={source == 'goodsList' ? { top: 88, bottom: 0 } : { bottom: 8 }}>
          {
            this.props.source == 'goodsList' ? null :
              <GoodsSearchBar storeId={sid || this.props.storeId} />
          }
          <div>
            <MenuList flag={!!sid} storeId={sid || this.props.storeId} handleClick={handleClick} />
          </div>
        </div>
        {false && <Search />}
      </div>
    )
  }
}
