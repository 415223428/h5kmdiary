import React from 'react';
import { StoreProvider } from 'plume2';

import { history } from 'wmkit';
import AppStore from './store';
import GoodsList from './component/goods-list';
import GoodsTab from './component/goods-tab';
import GoodsTabShade from './component/goods-tab-shade';
import PromotionBottom from './component/promotion-bottom';
import PromotionCoupon from './component/promotion-coupon';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class CouponPromotion extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    let state = history.location.state ? history.location.state : null;
    if (state) {
      // 解析url中的参数
      // const { couponId, activityId } = this.props.match.params;
      this.store.init(state.couponId, state.activityId);
    }
  }

  render() {
    return (
      <div>
        {/*优惠券促销*/}
        <PromotionCoupon />

        {/*顶部的tab*/}
        <GoodsTab />

        {/*tab遮罩*/}
        <GoodsTabShade />

        {/*商品列表*/}
        <GoodsList />
        <PromotionBottom />
      </div>
    );
  }
}
