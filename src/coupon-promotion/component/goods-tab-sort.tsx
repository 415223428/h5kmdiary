import React, {Component} from 'react';
import {IMap, Relax} from 'plume2';
import {noop} from 'wmkit';

@Relax
export default class GoodsTabSort extends Component<any, any> {
  props: {
    relaxProps?: {
      closeShade: Function;
      setSort: Function;
      sortType: IMap;
      isShow: boolean;
    };
  };

  static relaxProps = {
    closeShade: noop,
    setSort: noop,
    sortType: 'sortType',
    isShow: 'isShow'
  };

  render() {
    const { closeShade, setSort, sortType, isShow } = this.props.relaxProps;

    // 排序字段
    const type = sortType.get('type');
    // 排序方式：升序 降序
    const sort = sortType.get('sort');

    return (
      <div style={{ width: '100%', display: 'flex', flexDirection: 'column' }}>
        <div style={{ backgroundColor: '#ffffff', width: '100%' }}>
          <div
            // className={
            //   type == 'default' || type == ''
            //     ? 'sort-item'
            //     : 'sort-item'
            // }
            className="sort-item"
            onClick={() => setSort('default')}
            style={type == 'default' || type == '' ? {color:'#FF4D4D',fontWeight:'bold'}:{}}
          >
            综合
          </div>

          <div className="sort-item" onClick={() => setSort('dateTime')}>
            <div 
              // className={type == 'dateTime' ? 'new-theme-text' : ''}
              style={type == 'dateTime' ? {color:'#FF4D4D',fontWeight:'bold'}:{}}
            >最新</div>
            <div
              className={
                type == 'dateTime'
                  ? 'sort-icon-box new-theme-text'
                  : 'sort-icon-box'
              }
            >
            </div>
          </div>

          <div
            // className={
            //   type == 'salesNum'
            //     ? 'sort-item new-theme-text'
            //     : 'sort-item'
            // }
            className="sort-item"
            onClick={() => setSort('salesNum')}
            style={type == 'salesNum' ? {color:'#FF4D4D',fontWeight:'bold'}:{}}
          >
            销量
          </div>

          <div className="sort-item" onClick={() => setSort('price')}>
            <div 
              // className={type == 'price' ? 'new-theme-text' : ''}
              style={type == 'price' ? {color:'#FF4D4D',fontWeight:'bold'}:{}}
            >价格</div>
            <div
              className={
                type == 'price' && sort == 'asc'
                  ? 'sort-icon-box new-theme-text'
                  : 'sort-icon-box'
              }
              style={{marginRight:'0.05rem',marginLeft:'0.05rem',display:'flex',alignItems:'center'}}
            >
              {/* <i className="iconfont icon-xiangshangpai" /> */}
              <img src={type == 'price' && sort == 'asc'? require('../img/up-active.png'):require('../img/up.png')} style={{height:'0.2rem'}}/>
            </div>
            <div
              // className={
              //   type == 'price' && sort == 'desc' ? 'new-theme-text' : ''
              // }
              style={{display:'flex',alignItems:'center'}}
            >
              {/* <i className="iconfont icon-xiangxiapai" /> */}
              <img src={type == 'price' && sort == 'desc'? require('../img/down-active.png'):require('../img/down.png')} style={{height:'0.2rem'}}/>
            </div>
          </div>

          {
            isShow ?
              (
                <div
                  // className={
                  //   type == 'evaluateNum'
                  //     ? 'sort-item new-theme-text'
                  //     : 'sort-item'
                  // }
                  className="sort-item"
                  onClick={() => setSort('evaluateNum')}
                  style={type == 'evaluateNum' ? {color:'#FF4D4D',fontWeight:'bold'}:{}}
                >
                  评论数
                </div>
              )
              :
              null
          }

          {
            isShow?
              (
                <div
                  // className={
                  //   type == 'praise'
                  //     ? 'sort-item new-theme-text'
                  //     : 'sort-item'
                  // }
                  className="sort-item"
                  onClick={() => setSort('praise')}
                  style={type == 'praise' ? {color:'#FF4D4D',fontWeight:'bold'}:{}}
                >
                  好评
                </div>
              )
              :
              null
          }

          <div
            className={
              type == 'collection'
                ? 'sort-item new-theme-text'
                : 'sort-item'
            }
            onClick={() => setSort('collection')}
            style={type == 'collection' ? {color:'#FF4D4D',fontWeight:'bold'}:{}}
          >
            收藏
          </div>
        </div>

        <div
          style={{ width: '100%', height: '100%' }}
          onClick={() => closeShade()}
        />
      </div>
    );
  }
}
