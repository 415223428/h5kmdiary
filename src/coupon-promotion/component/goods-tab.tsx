import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

const styles = require('../css/style.css');

let tab = [
  {
    name: 'goodsCate',
    label: '分类'
  },
  {
    name: 'goodsBrand',
    label: '品牌'
  },
  {
    name: 'goodsSort',
    label: '排序'
  }
];
const sorts = {
  default: '综合',
  dateTime: '最新',
  salesNum: '销量',
  price: '价格',
  evaluateNum: '评论数',
  praise: '好评',
  collection: '收藏'
};

@Relax
export default class GoodsTab extends Component<any, any> {
  props: {
    relaxProps?: {
      openShade: Function;
      closeShade: Function;
      tabName: string;
      sortType: any;
      brands: any;
      cates: any;
    };
  };

  static relaxProps = {
    openShade: noop,
    closeShade: noop,
    tabName: 'tabName',
    sortType: 'sortType',
    brands: 'brands',
    cates: 'cates'
  };

  render() {
    const {
      openShade,
      closeShade,
      tabName,
      sortType,
      brands,
      cates
    } = this.props.relaxProps;
    // 排序字段
    const sort = sortType.get('type');

    return (
      <div className={styles.menuBar}>
        <div className={styles.menuContent + ' b-1px-tb'}>
          {tab.map((v) => {
            // 是否是当前已展开的tab
            const match = v.name === tabName;
            if (v.name == 'goodsCate' && (!cates || cates.count() <= 0)) {
              return;
            } else if (
              v.name == 'goodsBrand' &&
              (!brands || brands.count() <= 0)
            ) {
              return;
            }
            return (
              <div
                className="item"
                onClick={() => {
                  // 在相同的tab上点击时，认为是要关闭tab
                  match ? closeShade() : openShade(v.name);
                }}
              >
                <span 
                  // className={match ? 'new-theme-text' : ''}
                  style={match?{color:'#FF4D4D',fontWeight:'bold',fontSize:'0.28rem'}:{fontSize:'0.28rem'}}
                >
                  {v.name == 'goodsSort' ? sorts[sort] || '排序' : v.label}
                </span>
                {match ? (
                  <i
                    className="iconfont icon-jiantou2 new-theme-text "
                    style={{ transform: 'rotate(180deg)',color:'#FF4D4D',fontWeight:'bold' }}
                  />
                ) : (
                  <i className="iconfont icon-jiantou2" style={{fontWeight:'bold' }}/>
                )}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
