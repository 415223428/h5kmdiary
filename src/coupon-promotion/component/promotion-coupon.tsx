import React, { Component } from 'react';
import { Relax } from 'plume2';
import moment from 'moment';
import { Const } from 'config';
import { CouponLabel } from 'wmkit';

const styles = require('../css/style.css');

@Relax
export default class PromotionCoupon extends Component<any, any> {
  props: {
    relaxProps?: {
      couponInfo: any;
    };
  };

  static relaxProps = {
    couponInfo: 'couponInfo'
  };

  render() {
    const { couponInfo } = this.props.relaxProps;
    if (!couponInfo) {
      return null;
    }
    const fullBuyPrice = couponInfo.get('fullBuyPrice')
      ? couponInfo.get('fullBuyPrice')
      : 0;
    const denomination = couponInfo.get('denomination')
      ? couponInfo.get('denomination')
      : 0;
    //拼接优惠券面值
    let couponLabel = `${
      couponInfo.get('fullBuyType') == 0 ? '满0' : `满${fullBuyPrice}`
    }减${denomination}`;

    return (
      <div style={{ height: '1.76rem',position:'relative' }}>
        <div className={styles.headerBg}>
          <div className="info">
            <p>
              {`${moment(couponInfo.get('startTime')).format(
                Const.DATE_FORMAT
              )}至${moment(couponInfo.get('endTime')).format(
                Const.DATE_FORMAT
              )} 以下商品可使用优惠券`}
            </p>
            <p>{this._buildScope(couponInfo.toJS())}</p>
            <p>
              {couponInfo.get('platformFlag') == 1
                ? '限平台：全平台可用'
                : `限平台：${
                    couponInfo.get('storeName')
                      ? couponInfo.get('storeName')
                      : ''
                  }`}
            </p>
            <CouponLabel 
              text={couponLabel} 
              style={{position:'absolute', bottom:'0.2rem', right:'0.2rem'}} 
              // pStyle={{background:'linear-gradient(90deg,rgba(254,125,80,1),rgba(255,77,77,1))'}}
            />
          </div>
        </div>
      </div>
    );
  }

  /**
   * 优惠券使用范围
   */
  _buildScope = (coupon) => {
    //营销类型(0,1,2,3,4) 0全部商品，1品牌，2平台(boss)类目,3店铺分类，4自定义货品（店铺可用）
    let scopeType = '限商品：';
    //范围名称
    let goodsName = '';

    if (coupon.scopeType == 0) {
      goodsName = '全部商品';
    } else if (coupon.scopeType == 1) {
      scopeType = '限品牌：';
      goodsName = '仅限';
      if (coupon.brandMap) {
        coupon.brandIds.forEach((value) => {
          let name = coupon.brandMap[value];
          if (name != null || name != undefined) {
            goodsName = `${goodsName}[${name}]`;
          }
        });
      }
    } else if (coupon.scopeType == 2) {
      scopeType = '限品类：';
      goodsName = '仅限';
      if (coupon.cateMap) {
        coupon.cateIds.forEach((value) => {
          let name = coupon.cateMap[value];
          if (name != null || name != undefined) {
            goodsName = `${goodsName}[${name}]`;
          }
        });
      }
    } else if (coupon.scopeType == 3) {
      scopeType = '限分类：';
      goodsName = '仅限';
      if (coupon.storeCateMap) {
        coupon.storeCateIds.forEach((value) => {
          let name = coupon.storeCateMap[value];
          if (name != null || name != undefined) {
            goodsName = `${goodsName}[${name}]`;
          }
        });
      }
    } else if (coupon.scopeType == 4) {
      goodsName = '部分商品';
    }

    return `${scopeType}${goodsName}`;
  };
}
