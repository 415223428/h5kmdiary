import React, { Component } from 'react';
import { history, _ } from 'wmkit';

const styles = require('../css/style.css');
const purCar=require('../img/pur-car.png')
export default class PromotionBottom extends Component<any, any> {
  props: {
    relaxProps?: {};
  };

  render() {
    return (
      <div style={{ height: '0.98rem' }}>
        <div className={styles.proTotal}>
          <div />
          {/* <button 
            onClick={() => history.push('/purchase-order')}
            style={{width:'100%',background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))',color:'#fff',fontSize:'0.32rem',fontWeight:'bold'}}
          >
            去购物车
          </button> */}
          <img src={purCar} alt=""  style={{width:'100%'}} onClick={() => history.push('/purchase-order')}/>
        </div>
      </div>
    );
  }
}
