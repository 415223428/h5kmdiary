import React, { Component } from 'react';
import { fromJS } from 'immutable';
import { IMap, Relax } from 'plume2';

import { Blank, ListView, noop } from 'wmkit';
import { IList } from 'typings/globalType';
import '../css/style.css'

import GoodsItem from './goods-item';

const noneImg = require('../img/none.png');

@Relax
export default class GoodsList extends Component<any, any> {
  _listView: any;

  props: {
    relaxProps?: {
      //请求参数
      couponId: string;
      activity: string;
      sortType: IMap;
      brandIds: IList;
      cateIds: IList;
      toRefresh: boolean;

      //对请求数据的处理
      dealData: Function;
      initToRefresh: Function;
      isShow: boolean;
    };
  };

  static relaxProps = {
    //请求参数
    couponId: 'couponId',
    activity: 'activity',
    sortType: 'sortType',
    brandIds: 'brandIds',
    cateIds: 'cateIds',
    toRefresh: 'toRefresh',

    //对请求数据的处理
    dealData: noop,
    initToRefresh: noop,
    isShow: 'isShow'
  };

  render() {
    let {
      couponId,
      activity,
      sortType,
      brandIds,
      cateIds,
      dealData,
      isShow
    } = this.props.relaxProps;

    // 组件刚执行完mount，搜索条件没有注入进来，先不加载WmListView，避免先进行一次无条件搜索，再立刻进行一次有条件搜索
    if (couponId == '' || activity == '') {
      return null;
    }

    /**
     * 0:最新倒序->按上下架时间倒序
     * 1:最新倒序->按上下架时间升序
     */
    let sortFlag = 0;
    if (sortType.get('type') == 'dateTime' && sortType.get('sort') == 'desc') {
      sortFlag = 1;
    } else if (
      sortType.get('type') == 'dateTime' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 1;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'desc'
    ) {
      sortFlag = 2;
    } else if (
      sortType.get('type') == 'price' &&
      sortType.get('sort') == 'asc'
    ) {
      sortFlag = 3;
    } else if (sortType.get('type') == 'salesNum') {
      sortFlag = 4;
    } else if (sortType.get('type') == 'evaluateNum') {
      sortFlag = 5;
    } else if (sortType.get('type') == 'praise') {
      sortFlag = 6;
    } else if (sortType.get('type') == 'collection') {
      sortFlag = 7;
    }

    return (
      <ListView
        className={'coupon-list'}
        url="/coupon-info/coupon-goods"
        style={{ height: window.innerHeight - 160 }}
        params={{
          couponId: couponId,
          activity: activity,
          sortFlag: sortFlag,
          brandIds: brandIds,
          cateIds: cateIds
        }}
        otherProps={['esGoodsInfoResponse.goodsIntervalPrices']}
        renderRow={(goodsItem: IMap) => {
          return (
            <GoodsItem
              goodsItem={fromJS(goodsItem)}
              key={fromJS(goodsItem).get('id')}
              isShow={isShow}
            />
          );
        }}
        renderEmpty={() => <Blank img={noneImg} content="没有搜到任何商品" />}
        ref={(_listView) => (this._listView = _listView)}
        onDataReached={(result) => dealData(result)}
      />
    );
  }
}
