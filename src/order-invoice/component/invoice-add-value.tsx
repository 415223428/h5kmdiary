import React, { Component } from 'react';
import { Relax } from 'plume2';
import { FormItem } from 'wmkit';
import InvoiceAddress from '../component/invoice-address';

@Relax
export default class InvoiceAddedValue extends Component<any, any> {
  props: {
    companyInfoId: number;
    relaxProps?: {
      key: string;
      VATInvoice: any;
    };
  };

  static relaxProps = {
    key: 'key',
    VATInvoice: 'VATInvoice'
  };

  render() {
    const { key, VATInvoice } = this.props.relaxProps;
    return (
      <div>
        {key === 'added-value' && VATInvoice ? (
          <div className="top-border">
            <FormItem
              labelName={'单位全称'}
              placeholder={VATInvoice.companyName}
            />
            <FormItem
              labelName={'纳税人识别号'}
              placeholder={VATInvoice.taxpayerNumber}
            />
            <FormItem
              labelName={'注册电话'}
              placeholder={VATInvoice.companyPhone}
            />
            <FormItem
              labelName={'注册地址'}
              placeholder={VATInvoice.companyAddress}
            />
            <FormItem
              labelName={'银行基本户号'}
              placeholder={VATInvoice.bankNo}
            />
            <FormItem labelName={'开户行'} placeholder={VATInvoice.bankName} />
            <div className="top-border" style={{ marginTop: 10 }}>
              <div className="row form-item">
                <div>
                  <span className="form-text">
                    开票项目<label>（增值税专用发票只支持明细）</label>
                  </span>
                </div>
              </div>
              <FormItem labelName={'明细'} />
            </div>
            <InvoiceAddress companyInfoId={this.props.companyInfoId} />
          </div>
        ) : null}
      </div>
    );
  }
}
