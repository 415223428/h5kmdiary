import React, { Component } from "react"
import { Relax } from "plume2"

import { RadioBox, noop } from 'wmkit'
import { IList } from 'typings/globalType'

@Relax
export default class InvoiceTab extends Component<any, any> {

  props: {
    companyInfoId: number
    relaxProps?: {
      key: string
      tabs: IList

      initTabActive: Function
    }
  }


  static relaxProps = {
    key: 'key',
    tabs: 'tabs',

    initTabActive: noop,
  }


  render() {
    const { key, initTabActive, tabs } = this.props.relaxProps;
    return (
      <div className="invoice-tab">
        <RadioBox
          data={tabs}
          checked={key}
          onCheck={(val) => initTabActive({
            tabKey: val,
            companyInfoId: this.props.companyInfoId
          })}
          style={{ width: '29.3%' }}
        />
      </div>
    )
  }
}
