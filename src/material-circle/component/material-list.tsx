import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { fromJS } from 'immutable';

import { noop, WMImage, NormalVideo, history, ListView, Blank } from 'wmkit';
import MaterialItem from '../component/material-item';
import MarketingMaterialItem from '../component/marketing-material-item';
const noneImg = require('../img/list-none.png');

@Relax
export default class MaterialList extends Component<any, any> {
  _listView: any;
  props: {
    //goodsInfoId: string;
    relaxProps?: {
      customer: any;
      visible: boolean;
      changeText: Function;
      changePop: Function;
      visibleMap: IMap;
      tabKey: number;
      selectSku: Function;
      showImgSwiper: Function;
      forbiddenFlag: number;
      changefenxiang: Function;
      sendId: Function;
      isfenxiang: boolean;
      cloasefenxiang: Function;
      closeImg: boolean;
      iscloseImg: Function;
      isopenImg: Function;
      fenxiangId: String;
      closeChoose:String;
      openWechatShare:Function;
      isWechatShare:boolean;
    };
  };

  static relaxProps = {
    visible: 'visible',
    changeText: noop,
    changePop: noop,
    visibleMap: 'visibleMap',
    tabKey: 'tabKey',
    selectSku: noop,
    showImgSwiper: noop,
    forbiddenFlag: 'forbiddenFlag',
    changefenxiang: noop,
    sendId: noop,
    isfenxiang: 'isfenxiang',
    customer: 'customer',
    cloasefenxiang: noop,
    iscloseImg: noop,
    closeImg: 'closeImg',
    isopenImg: noop,
    fenxiangId: 'fenxiangId',
    closeChoose:'closeChoose',
    openWechatShare:noop,
    isWechatShare:"isWechatShare",
    
  };

  render() {
    const {
      visibleMap,
      tabKey,
      selectSku,
      showImgSwiper,
      forbiddenFlag,
      changefenxiang,
      sendId,
      isfenxiang,
      customer,
      cloasefenxiang,
      closeImg,
      iscloseImg,
      isopenImg,
      fenxiangId,
      closeChoose
    } = this.props.relaxProps;

    return (
      <div className="material-list">
        <ListView
          url="/distribution/goods-matter/page/new"
          style={{ height: window.innerHeight }}
          params={{
            // goodsInfoId: this.props.goodsInfoId,
            sortColumn: 'updateTime',
            sortRole: 'desc',
            matterType: tabKey
          }}
          dataPropsName="context.distributionGoodsMatterPage.content"
          renderRow={(matterItem: IMap) => {
            return tabKey == 0 ? (
              <MaterialItem
                forbiddenFlag={forbiddenFlag}
                selectSku={(sku) => selectSku(sku)}
                key={fromJS(matterItem).get('id')}
                visible={visibleMap.get(fromJS(matterItem).get('id'))}
                onSpread={() => this._onSpread(fromJS(matterItem))}
                matterItem={fromJS(matterItem)}
                showImgSwiper={(matterItem, index) =>
                  showImgSwiper(matterItem, index)
                }
                sendId={sendId}
                changefenxiang={() => changefenxiang()}
                isfenxiang={isfenxiang}
                customer={customer}
                cloasefenxiang={cloasefenxiang}
                closeImg={closeImg}
                iscloseImg={iscloseImg}
                isopenImg={isopenImg}
                fenxiangId={fenxiangId}
                closeChoose={closeChoose}
              />
            ) : (
              <MarketingMaterialItem
                key={fromJS(matterItem).get('id')}
                visible={visibleMap.get(fromJS(matterItem).get('id'))}
                onSpread={() => this._onSpread(fromJS(matterItem))}
                matterItem={fromJS(matterItem)}
                showImgSwiper={(matterItem, index) =>
                  showImgSwiper(matterItem, index)
                }
              />
            );
          }}
          renderEmpty={() => (
            <Blank img={noneImg} content="该商品没有发圈素材" />
          )}
          ref={(_listView) => (this._listView = _listView)}
        />
      </div>
    );
  }
  _onSpread = (matterItem) => {
    const { changeText } = this.props.relaxProps;
    changeText(matterItem.get('id'));
  };
}
