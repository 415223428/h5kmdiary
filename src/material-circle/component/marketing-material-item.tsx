import React, { Component } from 'react';
import ReactClipboard from 'react-clipboardjs-copy';
import wx from 'weixin-js-sdk';
import moment from 'moment';
import { Relax, IMap } from 'plume2';
import { Const } from 'config';
import { noop, WMImage, NormalVideo, history, ListView, Blank } from 'wmkit';
import { fromJS } from 'immutable';
import MatterItem from '../../graphic-material/component/matter-item';

export default class MarketingMaterialItem extends Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeigth: 0,
      key: this.props.matterItem.get('id')
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  componentDidMount() {
    const { key } = this.state;
    let p = document.getElementById(key);
    this.setState({
      scrollHeigth: p.scrollHeight
    });
  }

  render() {
    const { matterItem } = this.props;
    return (
      <div className="material-item">
        <div className="material-item-top">
          <div className="top-img-box">
            <img
              src={require('../img/km.png')}
              alt=""
            />
          </div>
          <div className="material-name">
            <div className="material-name-top rowFlexBetween">
              <span>官方精选</span>
              {/* <span className="share-time">
                {matterItem.get('recommendNum')}
                <span>
                次分享
                </span>
              </span> */}
            </div>
            <span className="material-name-bottom">
              {' '}
              {matterItem.get('updateTime')
                ? moment(matterItem.get('updateTime')).format(Const.DATE_FORMAT)
                : '-'}
            </span>
          </div>
        </div>
        <div style={{
          paddingLeft: '1.15rem'
        }}>
        <p
          id={this.state.key}
          className={
            this.props.visible && this.state.scrollHeigth > 105
              ? 'material-detail show-all-detail'
              : 'material-detail'
          }
        >
          {matterItem.get('recommend')}
        </p>
        {this.state.scrollHeigth > 105 ? (
          this.props.visible ? (
            <div className="open-all" onClick={() => this.props.onSpread()}>
              收起<i className="iconfont icon-jiantou2 change-arrow" />
            </div>
          ) : (
            <div className="open-all" onClick={() => this.props.onSpread()}>
              展开全文<i className="iconfont icon-jiantou2" />
            </div>
          )
        ) : null}
        <ul className="clearfix">
          {matterItem.get('matter') &&
            JSON.parse(matterItem.get('matter')).map((item, index) => {
              return (
                <li
                  key={index}
                  onClick={() => this.props.showImgSwiper(matterItem, index)}
                >
                  <img src={item.imgSrc} alt="" />
                  {item.linkSrc && (
                    <img
                      src={item.linkSrc}
                      style={{
                        width: 20,
                        height: 20,
                        position: 'absolute',
                        bottom: 0,
                        right: 0
                      }}
                      alt=""
                    />
                  )}
                </li>
              );
            })}
        </ul>
        <div className="material-share-bottom">
          {(window as any).isMiniProgram && (
            <ReactClipboard
              text={matterItem.get('recommend')}
              onSuccess={() => this._shareMoments(matterItem)}
              onError={(e) => console.log(e)}
            >
              <a href="javascript:;" className="save" style={{display:'none'}}>
                <i className="iconfont icon-xiazai" />
                保存图文
              </a>
            </ReactClipboard>
          )}
        </div>
        </div>
      </div>
    );
  }

  //携带图片到小程序原生页面分享
  _shareMoments = (matterItem) => {
    if ((window as any).isMiniProgram) {
      let params = {
        materialList: matterItem.get('matter'),
        token: (window as any).token,
        id: matterItem.get('id'),
        matterType: matterItem.get('matterType')
      };
      wx.miniProgram.navigateTo({
        url: `../material/material?data=${JSON.stringify(params)}`
      });
    }
  };

  // _drawCanvas = (matterItem) => {
  //   let matterObj = JSON.parse(matterItem);
  //   for (let i = 0; i < matterObj.length; i++) {
  //     // canvas.id = i;
  //     let src;
  //     if (matterObj[i].imgSrc) {
  //       let image = new Image();
  //       image.src = matterObj[i].imgSrc;
  //       //image.src = require('../img/invitation.png');
  //       image.setAttribute('crossOrigin', 'Anonymous');
  //       image.onload = () => {
  //         let canvas = document.createElement('canvas') as HTMLCanvasElement;
  //         let context = canvas.getContext('2d');
  //         canvas.width = 100;
  //         canvas.height = 100;
  //         context.drawImage(image, 0, 0, 100, 100);
  //         src = canvas.toDataURL('image/png');
  //       };
  //       console.log('33333333333');
  //       return <img src={src} />;
  //     }
  //   }
  // };
}
