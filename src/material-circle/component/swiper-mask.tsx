import React, { Component } from 'react';
import { Relax } from 'plume2';
import Swiper from 'swiper/dist/js/swiper.js';
@Relax
export default class SwiperMask extends Component<any, any> {
  props: {
    relaxProps?: {
      swiperVisible: boolean;
      swiperList: any;
      toggleSwiperVisible: Function;
    };
  };

  static relaxProps = {
    swiperVisible: 'swiperVisible',
    swiperList: 'swiperList',
    toggleSwiperVisible: () => {}
  };

  render() {
    const {
      swiperVisible,
      swiperList,
      toggleSwiperVisible
    } = this.props.relaxProps;
    return (
      swiperVisible && (
        <div className="material-mask" onClick={() => toggleSwiperVisible()}>
          <div
            className="swiper-container banner-swiper"
            id="swiper-container-material"
            style={{ background: 'none' }}
          >
            <div className="swiper-wrapper" style={{ alignItems: 'center' }}>
              {swiperList.toJS().map((v, index) => {
                return (
                  <div className="material-mask-item swiper-slide" key={index}>
                    <img
                      style={{ maxWidth: '100%', maxHeight: '100%' }}
                      src={v}
                      alt=""
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )
    );
  }
}
