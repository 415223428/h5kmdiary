import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class MaterialTop extends Component<any, any> {
  props: {
    relaxProps?: {
      tabKey: number;
      setTabKey: Function;
    };
  };

  static relaxProps = {
    tabKey: 'tabKey',
    setTabKey: noop
  };

  render() {
    const { tabKey, setTabKey } = this.props.relaxProps;
    return (
      <div style={{ height: '0.98rem' }}>
        <div className="material-top">
          <div
            className={
              tabKey == 0 ? 'material-top-item curr' : 'material-top-item'
            }
            onClick={() => setTabKey(0)}
          >
            商品推荐
            <i style={{
              width:'2rem',
              marginTop:'.3rem'
            }}/>
          </div>
          <div
            className={
              tabKey == 1 ? 'material-top-item curr' : 'material-top-item'
            }
            onClick={() => setTabKey(1)}
          >
            素材推广 
            <i style={{
              width:'2rem',
              marginTop:'.3rem'
            }}/>
          </div>
        </div>
        <i style={{
          width: '100vw',
          height: '1px',
          display: 'flex',
          top: '.88rem',
          position: 'absolute',
          background: 'rgba(230,230,230,1)',
        }}></i>
      </div>
    );
  }
}
