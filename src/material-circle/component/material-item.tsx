import React, { Component } from 'react';
import ReactClipboard from 'react-clipboardjs-copy';
import wx from 'weixin-js-sdk';
import moment from 'moment';
import { Relax, IMap } from 'plume2';
import { Const } from 'config';
import { noop, WMImage, NormalVideo, history, ListView, Blank,WMkit } from 'wmkit';
import { fromJS } from 'immutable';
import { cache } from 'config';
import GoodsShareImg from './goods-share-img'

export default class MaterialItem extends Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeigth: 0,
      key: this.props.matterItem.get('id')
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  componentDidMount() {
    const { key } = this.state;
    let p = document.getElementById(key);
    this.setState({
      scrollHeigth: p.scrollHeight
    });
  }
  render() {
    const { 
      matterItem, 
      forbiddenFlag,
      customer,
      isfenxiang,
      cloasefenxiang,
      closeImg,
      changefenxiang,
      sendId,
      iscloseImg,
      isopenImg,
      fenxiangId,
      closeChoose,
      openWechatShare,
      isWechatShare
    } = this.props;
    const goodsInfo = matterItem.get('goodsInfo') || fromJS({});
    const id = goodsInfo.get('goodsInfoId')
    const isDistributor = JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerDetail.isDistributor;
    const isdistributor = WMkit.isShowDistributionButton();
    const inviteeId =isdistributor? JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId :'';
    const inviteCode=isdistributor?sessionStorage.getItem('inviteCode'):'';
    const url = `goods-detail/${goodsInfo.get('goodsInfoId')}/?channel=mall&inviteeId=${inviteeId}&inviteCode=${inviteCode}`;
    return (
      <div>
        <div className="material-item">
          {/* <i className='line'></i> */}
          <div className="material-item-top">
            <div className="top-img-box">
              <img
                src={require('../img/km.png')}
                alt=""
              />
            </div>
            <div className="material-name">
              <div className="material-name-top rowFlexBetween">
                <span>官方精选</span>
                {/* <span className="share-time">
                  {matterItem.get('recommendNum')}
                  <span>
                    次分享
                  </span>
                </span> */}
              </div>
              <span className="material-name-bottom">
                {' '}
                {matterItem.get('updateTime')
                  ? moment(matterItem.get('updateTime')).format(Const.DATE_FORMAT)
                  : '-'}
              </span>
            </div>
          </div>
          <div style={{
            paddingLeft: '1.15rem'
          }}>
            <p
              id={this.state.key}
              className={
                this.props.visible && this.state.scrollHeigth > 105
                  ? 'material-detail show-all-detail'
                  : 'material-detail'
              }
            >
              {matterItem.get('recommend')}
            </p>
            {this.state.scrollHeigth > 105 ? (
              this.props.visible ? (
                <div className="open-all" onClick={() => this.props.onSpread()}>
                  收起<i className="iconfont icon-jiantou2 change-arrow" />
                </div>
              ) : (
                  <div className="open-all" onClick={() => this.props.onSpread()}>
                    展开全文<i className="iconfont icon-jiantou2" />
                  </div>
                )
            ) : null}
            <ul className="clearfix">
              {matterItem
                .get('matter')
                .split(',')
                .map((v, index) => {
                  return (
                    <li
                      key={index}
                      onClick={() => this.props.showImgSwiper(matterItem, index)}
                    >
                      <img src={v} alt="" />
                    </li>
                  );
                })}
            </ul>
            {
              (isDistributor) ? (
                <div className="material-share" style={{border:'none'}}>
                  <img
                    className="material-share-img"
                    src={
                      goodsInfo.get('goodsInfoImg')
                        ? goodsInfo.get('goodsInfoImg')
                        : require('../img/none.png')
                    }
                  />
                  <div
                    className="material-share-info"
                    onClick={() =>
                      history.push('/goods-detail/' + goodsInfo.get('goodsInfoId'))
                    }
                  >
                    <p className="share-info-name">{goodsInfo.get('goodsInfoName')}</p>
                    <div className="share-info-price">
                      <span style={{marginRight:'0.1rem'}}>￥{goodsInfo.get('marketPrice')}</span>
                      <span style={{
                        marginRight:'0.1rem',textDecoration:'line-through',fontSize:'0.26rem',color:'#999'
                        }}>￥{goodsInfo.get('marketPrice')}</span>
                      {forbiddenFlag == 0 &&
                        ` 赚￥${goodsInfo.get('distributionCommission')}`}
                      {forbiddenFlag == 0 && (
                        <div
                          className="share-info-icon"
                          onClick={(e) => 
                            // this.props.selectSku(goodsInfo)
                            {
                              e.stopPropagation();
                              // sendId(matterItem.get('id'));
                              // changefenxiang();
                              history.push({
                                pathname: '/share',
                                state: {
                                  goodsInfo ,
                                  url,
                                  id,
                                }
                              });
                            }
                          }
                        >
                          <i className="iconfont icon-fenxiang2" style={{
                            fontSize: '.28rem',
                            paddingRight: '.1rem',
                            color:'#999'
                          }} />
                          分享赚
                        </div>
                      )}
                    </div>
                  </div>
                  {/* {forbiddenFlag == 0 && (
              <div
                className="share-info-icon"
                onClick={() => this.props.selectSku(goodsInfo)}
              >
                <i className="iconfont icon-fenxiang2" />
                分享赚
              </div>
            )} */}
                </div>
              ) : (null)
            }

            <div className="material-share-bottom">
              {(window as any).isMiniProgram && (
                <ReactClipboard
                  text={matterItem.get('recommend')}
                  onSuccess={() => this._shareMoments(matterItem)}
                  onError={(e) => console.log(e)}
                >
                  <a href="javascript:;" className="save"  style={{display:'none'}}>
                    <i className="iconfont icon-xiazai" />
                    保存图文
                </a>
                </ReactClipboard>
              )}
            </div>

          </div>
        </div>
    </div>
    )
  }

  //携带图片到小程序原生页面分享
  _shareMoments = (matterItem) => {
    if ((window as any).isMiniProgram) {
      let params = {
        materialList: matterItem.get('matter'),
        token: (window as any).token,
        id: matterItem.get('id'),
        matterType: matterItem.get('matterType')
      };
      wx.miniProgram.navigateTo({
        url: `../material/material?data=${JSON.stringify(params)}`
      });
    }
  };
}
