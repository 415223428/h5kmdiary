import { Fetch } from 'wmkit';

/**
 * 获取该会员作为分销员信息
 */
export const fetchDistributorInfo = () => {
  return Fetch('/distribute/distributor-info');
};
export const shareInfo = () => {
  return Fetch('/customer/customerCenter')
}
