import { Actor, Action } from 'plume2';
import { fromJS } from 'immutable';

export default class IndexActor extends Actor {
  defaultState() {
    return {
      customer: {},
      visible: true,
      visiblePop: false, //分享图文信息Pop
      momentsSuccess: false, //朋友圈分享成功
      //每一个item推荐文字的展开隐藏
      visibleMap: fromJS({}),
      //0:商品素材 1:营销素材
      tabKey: 0,
       // 分享弹出显示与否
      shareVisible: false,
       // 分享选中的sku
      checkedSku: fromJS({}),
      //素材图片预览
      swiperVisible: false,
      //素材大图
      swiperList: fromJS([]),
      //分销员禁用标识，0:开启，1：禁用
      forbiddenFlag: 0,
      isfenxiang:false,
      closeImg:true,
      fenxiangId:'',
      closeChoose:true,
      isWechatShare:false,
    };
  }

  @Action('change:textVisible')
  changeCateVisible(state, id) {
    //获取原来的状态值
    const oldState = state.get('visibleMap').get(id);
    return state.setIn(['visibleMap', id], !oldState);
  }

  @Action('change: popVisible')
  changePopVisible(state) {
    return state.set('visiblePop', !state.get('visiblePop'));
  }

  @Action('change: momentsVisible')
  changeMomentsVisible(state) {
    return state
      .set('visiblePop', false)
      .set('momentsSuccess', !state.get('momentsSuccess'));
  }

  @Action('material: setTabKey')
  setTabKey(state, tab: number) {
    return state.set('tabKey', tab);
  }

  @Action('material: changeShareVisible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }

  @Action('material: selectSku')
  selectSku(state, sku) {
    return state.set('checkedSku', sku);
  }

  @Action('material :swiper')
  swiper(state, result) {
    return state.set('swiperList', result);
  }

  @Action('material :toggleSwiperVisible')
  toggleSwiperVisible(state) {
    return state.set('swiperVisible', !state.get('swiperVisible'));
  }

  @Action('material :forbiddenFlag')
  forbiddenFlag(state, flag) {
    return state.set('forbiddenFlag', flag);
  }
  //打开分享页面
  @Action('material:isfenxiang')
  isfenxiang(state){
    return state.set('isfenxiang',true)
  }
//关闭分享页面
  @Action('material:closefenxiang')
  closefenxiang(state){
    return state.set('isfenxiang',false)
  }
  //关闭img
  @Action('material:closeImg')
  closeImg(state){
    return state.set('closeImg',false)
    .set('closeChoose',false)
  }
  //打开Img
  @Action('material:openImg')
  openImg(state){
    return state.set('closeImg',true)
    .set('closeChoose',true)
  }

  @Action('material:fenxiangId')
  senId(state,fenxiangId){
    return state.set('fenxiangId',fenxiangId)
  }

  
  @Action('goods-actor:openWechatShare')
  openWechatShare(state){
    return state.set('isWechatShare',true)
  }

  @Action('goods-actor:closeWechatShare')
  closeWechatShare(state){
    return state.set('isWechatShare',false)
  }
  @Action('member:init')
  init(state, { customer }) {
    return state
      .set('customer', fromJS(customer))
  }
}
