import React from 'react';
import { StoreProvider } from 'plume2';
import { ShareModal } from 'biz';
import AppStore from './store';
import Footer from '../footer';
import MaterialTop from './component/material-top';
import MaterialList from './component/material-list';
import SwiperMask from './component/swiper-mask';
const styles = require('./css/style.css');
import { cache } from 'config';


@StoreProvider(AppStore, { debug: __DEV__ })
export default class MaterialCircle extends React.Component<any, any> {
  store: AppStore;
  componentWillMount() {}
  componentDidMount() {
    this.store.memberInfo();
    this.store.fetchDistributorInfo();
  }

  render() {
    const { mid } = this.props.match.params;
    const isDistributor = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
    // console.log('isDistributorisDistributorisDistributorisDistributor');
    // console.log(isDistributor);
    
    return (
      <div className={styles.materialContainer}>
        {isDistributor.customerDetail.isDistributor ? (<MaterialTop />):(null)}

        
        <MaterialList />
        {
          <ShareModal
            shareType={2}
            goodsInfo={this.store.state().get('checkedSku')}
            visible={this.store.state().get('shareVisible')}
            changeVisible={this.store.changeShareVisible}
          />
        }
        <Footer path={'/material-circle'} />
        <SwiperMask />
      </div>
    );
  }
}
