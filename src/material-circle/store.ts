import { Store } from 'plume2';
import IndexActor from './actor/center-actor';
import Swiper from 'swiper/dist/js/swiper.js';
import 'swiper/dist/css/swiper.min.css';
import { fromJS } from 'immutable';
import { Alert} from 'wmkit';
import * as webapi from './webapi';

export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new IndexActor()];
  }

  /**
   * 初始化加载数据
   * @returns {Promise<void>}
   */
  init = async () => {
    setTimeout(function() {
      const hotSwiper = new Swiper('#swiper-container-material', {
        spaceBetween: 20,
        pagination: {
          type: 'fraction',
          el: '.hot-title-right'
        }
      });
    }, 1000);
  };
  memberInfo = async () => {
    const res = (await Promise.all([
      webapi.shareInfo()
    ])) as any;
    console.log('--------------------11/20');
    console.log(res);
    
      this.dispatch('member:init', {
        customer: res[0].context,
      });
  };
  /**
   * 全文or收起
   */
  changeText = (id) => {
    this.dispatch('change:textVisible', id);
  };

  /**
   * 分享图文信息Pop
   */
  changePop = () => {
    this.dispatch('change: popVisible');
  };

  /**
   * 朋友圈分享成功
   */
  changeMoments = () => {
    this.dispatch('change: momentsVisible');
  };

  setTabKey = (tab: number) => {
    this.dispatch('material: setTabKey', tab);
  };

  changeShareVisible = () => {
    this.dispatch('material: changeShareVisible');
  };
/**
   * 分享赚选中的sku
   * @param sku
   */
  saveCheckedSku = (sku) => {
    this.dispatch('material:setSkuContext', sku);
  };
//改变分享页面的开关
changefenxiang = () => {
  this.dispatch('material:isfenxiang');
};
cloasefenxiang = () => {
  this.dispatch('goods-actor:closeWechatShare');
  this.dispatch('material:closefenxiang');
};

iscloseImg = () => {
  this.dispatch('material:closeImg');
};

isopenImg = () => {
  this.dispatch('material:openImg');
};

//传入对应的分享页面Id
sendId = (fenxiangId) => {
  this.dispatch('material:fenxiangId', fenxiangId);
}

selectSku = (sku) => {
    if ((window as any).isMiniProgram) {
      //存储当前选中的sku
      this.dispatch('material: selectSku', sku);
      this.changeShareVisible();
    } else {
      Alert({
        text: '请到小程序端进行该操作!'
      });
    }
  };

  showImgSwiper = async (matterItem, index) => {
    let result = [];
    //商品素材，从下标为index的地方分割再重新排序
    if (matterItem.toJS().matterType == 0) {
      let imageList = matterItem.toJS().matter.split(',');
      let end = imageList.slice(index, imageList.length);
      let front = imageList.slice(0, index);
      //两个数组拼接，end在前，front在后
      let result = end.concat(front);
      this.dispatch('material :swiper', fromJS(result));
    }
    //营销素材
    if (matterItem.toJS().matterType == 1) {
      let marketingImgs = [];
      let imageList = JSON.parse(matterItem.toJS().matter);
      imageList.map((v) => {
        marketingImgs.push(v.imgSrc);
      });
      let end = marketingImgs.slice(index, marketingImgs.length);
      let front = marketingImgs.slice(0, index);
      //两个数组拼接，end在前，front在后
      let result = end.concat(front);
      this.dispatch('material :swiper', fromJS(result));
    }
    //营销素材，只展示素材图片
    await this.init();
    //展示swiper
    this.toggleSwiperVisible();
  };

  toggleSwiperVisible = () => {
    this.dispatch('material :toggleSwiperVisible');
  };

  /**
   * 获取分销员信息
   */
  fetchDistributorInfo = async () => {
    const res = await webapi.fetchDistributorInfo();
    if (res.code == 'K-000000') {
      const distributionCustomerVO = (res.context as any)
        .distributionCustomerVO;
      //分销员是否禁用
      this.dispatch(
        'material :forbiddenFlag',
        distributionCustomerVO.forbiddenFlag
      );
    }
  };

  openWechatShare=()=>{
    this.dispatch('goods-actor:openWechatShare');
  }
}
