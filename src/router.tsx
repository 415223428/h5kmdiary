/**
 * 路由配置文件
 *   hasBottom     是否有底部导航栏
 *   withoutLogin  true:完全不需要登陆的页面(如登录,注册,修改密码等页面)
 *   openAccess    true:需要根据"是否需要登陆"的开关,来判定该页面是否需要登陆
 */
import { WMkit } from 'wmkit';

const errorRouter = {
  // 不配置path，什么都匹配不到的情况匹配这个，要放最后
  path: '',
  title:  '找不到页面',
  exact: true,
  asyncComponent: () => System.import('./error'),
  withoutLogin: true
};
const routers = [
  {
    path: '/shanPing',
    title: document.title,
    exact: true,
    asyncComponent: () => System.import('./shanPing'),
    openAccess: true
  },
  {
    path: '/bangDingVipNo',
    title: document.title,
    exact: true,
    asyncComponent: () => System.import('./bangDingVipNo'),
    // openAccess: true,
    withoutLogin: true
  },
  {
    path: '/bangDingPhone',
    title: document.title,
    exact: true,
    asyncComponent: () => System.import('./bangDingPhone'),
    // openAccess: true,
    withoutLogin: true
  },
  {
    path: '/guanggao',
    title: document.title,
    exact: true,
    asyncComponent: () => System.import('./guanggao'),
    openAccess: true
  },
  {
    path: '/',
    title:  document.title||'首页-康美日记',
    exact: true,
    asyncComponent: () =>
         System.import('./main'),
    hasBottom: true,
    openAccess: true
  },
  {
    path: '/page/:pageType/:pageCode/:storeId?',
    title: document.title,
    exact: true,
    asyncComponent: () => System.import('./x-site'),
    openAccess: true
  },
  {
    path: '/register/:employeeId?',
    title:  '注册-康美日记',
    exact: true,
    asyncComponent: () => System.import('./register'),
    withoutLogin: true
  },
  {
    path: '/improve-information/:cid',
    title:  '完善账户信息-康美日记',
    exact: true,
    asyncComponent: () => System.import('./improve-information'),
    withoutLogin: true
  },
  {
    path: '/purchase-order',
    title:  '购物车-康美日记',
    exact: true,
    asyncComponent: () => System.import('./purchase-order'),
    hasBottom: true,
    openAccess: true
  },
  {
    path: '/register-agreement',
    title:  '注册协议-康美日记',
    exact: true,
    asyncComponent: () => System.import('./register-agreement'),
    withoutLogin: true
  },
  {
    path: '/goods-detail/:id',
    title:  '商品详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./goods-detail'),
    openAccess: true
  },
  {
    path: '/login',
    title:  '登录-康美日记',
    exact: true,
    asyncComponent: () => System.import('./login'),
    withoutLogin: true
  },
  {
    path: '/returnsFirst/:tid',
    title:  '申请退货退款-康美日记',
    exact: true,
    asyncComponent: () => System.import('./return-refund/return-first-step')
  },
  {
    path: '/goodsList',
    title:  '商品列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./goods-list'),
    hasBottom: true,
    openAccess: true
  },
  {
    path: '/serviceGoodsList',
    title:  '商品列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-goods-list'),
    hasBottom: true,
    openAccess: true
  },
  {
    path: '/goods-list-promotion/:mid',
    title:  '促销列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./goods-list-promotion'),
    hasBottom: false,
    openAccess: true
  },
  {
    path: '/order-confirm',
    title:  '确认订单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./order-confirm')
  },
  {
    path: '/points-order-confirm',
    title:  '确认积分订单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./points-order-confirm')
  },
  {
    path: '/order-list',
    title:  '订单列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./order-list'),
    hasBottom: true
  },
  {
    path: '/customer-order-detail/:tid',
    title:  '客户订单详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./customer-order-detail'),
    hasBottom: true
  },
  {
    path: '/customer-order-list',
    title:  '推广订单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./customer-order-list'),
    hasBottom: true
  },
  {
    path: '/returnsSecond',
    title:  '申请退货退款-康美日记',
    exact: true,
    asyncComponent: () => System.import('./return-refund/return-second-step')
  },
  {
    path: '/applySuccess/:rid',
    title:  '退货退款申请提交成功-康美日记',
    exact: true,
    asyncComponent: () => System.import('./return-refund/return-refund-success')
  },
  {
    path: '/user-center',
    title:  '我的-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-center'),
    hasBottom: true,
    openAccess: true
  },
  {
    path: '/member-center',
    title:  '会员中心-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user/member-center'),
    openAccess: true
  },
  {
    path: '/growth-value',
    title:  '会员中心-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user/growth-value'),
    openAccess: true
  },
  {
    path: '/settings',
    title:  '设置-康美日记',
    exact: true,
    asyncComponent: () => System.import('./settings'),
    hasBottom: false
  },
  {
    path: '/fecth-user-id',
    title:  '账号查看-康美日记',
    exact: true,
    asyncComponent: () => System.import('./fecth-user-id'),
    hasBottom: false
  },
  {
    path: '/user-info',
    title:  '基本信息-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-info')
  },
  {
    path: '/user-finance',
    title:  '财务信息-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-finance')
  },
  {
    path: '/user-account',
    title:  '银行账户-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-account')
  },
  {
    path: '/user-account-edit/:accountId',
    title:  '银行账户编辑-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-account-edit')
  },
  {
    path: '/user-invoice',
    title:  '增票资质-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-invoice')
  },
  {
    path: '/user-email',
    title:  '财务邮箱-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-email')
  },
  {
    path: '/user-safe',
    title:  '账号安全-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-safe')
  },
  {
    path: '/user-safe-password',
    title:  '修改密码-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-safe-password'),
    withoutLogin: true
  },
  {
    path: '/user-safe-mobile',
    title:  '修改绑定手机号-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-safe-mobile')
  },
  {
    path: '/user-safe-mobile-next',
    title:  '修改绑定手机号-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-safe-mobile-next')
  },
  {
    path: '/balance-pay-password',
    title:  '设置支付密码-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance-pay-password')
  },
  {
    path: '/balance-pay-password-next',
    title:  '设置支付密码-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance-pay-password-next')
  },
  {
    path: '/applyRefund/:tid',
    title:  '申请退货退款-康美日记',
    exact: true,
    asyncComponent: () => System.import('./return-refund/refund-first-step')
  },
  {
    path: '/main',
    title:  '首页-康美日记',
    exact: true,
    asyncComponent: () => System.import('./main'),
    openAccess: true
  },
  {
    path: '/user-collection',
    title:  '我的收藏-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-collection')
  },
  {
    path: '/search',
    title:  '搜索页-康美日记',
    exact: true,
    asyncComponent: () => System.import('./search'),
    openAccess: true
  },
  {
    path: '/serviceSearch',
    title:  '搜索页-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-search'),
    openAccess: true
  },
  {
    path: '/order-sku-list/:sid',
    title:  '确认订单-商品清单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./order-sku-list')
  },
  {
    path: '/points-order-sku-list',
    title:  '确认积分订单-商品清单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./points-order-sku-list')
  },
  {
    path: '/receive-address',
    title:  '收货地址列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./receive-address')
  },
  {
    path: '/receive-address-edit/:addressId',
    title: document.title,
    exact: true,
    asyncComponent: () => System.import('./receive-address-edit')
  },
  {
    path: '/logistics-input/:rid',
    title:  '填写物流信息-康美日记',
    exact: true,
    asyncComponent: () => System.import('./logistics-input')
  },
  {
    path: '/order-invoice/:cid',
    title:  '确认订单-索取发票-康美日记',
    exact: true,
    asyncComponent: () => System.import('./order-invoice')
  },
  {
    path: '/logistics-select',
    title:  '物流公司选择-康美日记',
    exact: true,
    asyncComponent: () => System.import('./logistics-select')
  },
  {
    path: '/refund-list',
    title:  '退货退款列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./refund-list')
  },
  {
    path: '/return-detail/:rid',
    title:  '退货退款详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./return-detail')
  },
  {
    path: '/finance-refund-record/:rid',
    title:  '退款记录-康美日记',
    exact: true,
    asyncComponent: () => System.import('./finance-refund-record')
  },
  {
    path: '/order-detail/:tid',
    title:  '订单详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./order-detail')
  },
  {
    path: '/append-list',
    title:  '订单附件-康美日记',
    exact: true,
    asyncComponent: () => System.import('./append-list')
  },
  {
    path: '/user-integral',
    title:  '我的积分-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user-integral')
  },
  {
    path: '/order-confirm-success',
    title:  '订单提交成功-康美日记',
    exact: true,
    asyncComponent: () => System.import('./order-confirm-success')
  },
  {
    path: '/points-order-confirm-success',
    title:  '积分订单提交成功-康美日记',
    exact: true,
    asyncComponent: () => System.import('./points-order-confirm-success')
  },
  {
    path: '/pay-detail/:tid',
    title:  '付款记录-康美日记',
    exact: true,
    asyncComponent: () => System.import('./pay-detail')
  },
  {
    path: '/goods-cate',
    title:  '商品分类-康美日记',
    exact: true,
    asyncComponent: () => System.import('./goods-cate'),
    hasBottom: true,
    openAccess: true
  },
  {
    path: '/classify-main',
    title:  '分类-康美日记',
    exact: true,
    asyncComponent: () => System.import('./classify-main'),
    hasBottom: true,
    openAccess: true
  },
  {
    path: '/fill-payment/:tid',
    title:  '填写付款单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./fill-payment')
  },
  {
    path: '/pay-online/:tid?',
    title:  '线上支付-康美日记',
    exact: true,
    asyncComponent: () => System.import('./pay-online')
  },
  {
    path: '/pay-success/:tid/:parentTid?',
    title:  '订单支付成功-康美日记',
    exact: true,
    asyncComponent: () => System.import('./pay-online/pay-sucess')
  },
  {
    path: '/ship-record/:tid/:type',
    title:  '发货记录-康美日记',
    exact: true,
    asyncComponent: () => System.import('./ship-record')
  },
  {
    path: '/ship-list/:oid/:tid',
    title:  '发货商品清单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./ship-list')
  },
  {
    path: '/logistics-info/:oid/:tid/:type',
    title:  '物流信息-康美日记',
    exact: true,
    asyncComponent: () => System.import('./logistics-info')
  },
  {
    path: '/return-logistics-info/:rid',
    title:  '退货物流信息-康美日记',
    exact: true,
    asyncComponent: () => System.import('./return-logistics-info')
  },
  {
    path: '/invoice-info/:tid/:type',
    title:  '发票信息-康美日记',
    exact: true,
    asyncComponent: () => System.import('./invoice-info')
  },
  {
    path: '/loading',
    title:  '加载中-康美日记',
    exact: true,
    asyncComponent: () => System.import('./loading'),
    withoutLogin: true
  },
  {
    path: '/usersafe-password-next',
    title:  '修改密码第二步-康美日记',
    exact: true,
    asyncComponent: () => System.import('./usersafe-password-next'),
    withoutLogin: true
  },
  {
    path: '/goods-failure',
    title:  '商品详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./goods-failure'),
    openAccess: true
  },
  {
    path: '/net-trouble',
    title:  '网络故障-康美日记',
    exact: true,
    asyncComponent: () => System.import('./net-trouble'),
    withoutLogin: true
  },
  {
    path: '/seller-account',
    title:  '卖家收款账号-康美日记',
    exact: true,
    asyncComponent: () => System.import('./seller-account')
  },
  {
    path: '/fill-payment-success/:tid',
    title:  '付款单提交成功-康美日记',
    exact: true,
    asyncComponent: () => System.import('./fill-payment/fill-payment-success')
  },
  {
    path: '/return-sku-list/:rid',
    title:  '退单商品列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./return-sku-list')
  },
  {
    path: '/order-detail-skus/:tid',
    title:  '订单详情-商品清单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./order-detail/order-detail-skus')
  },
  {
    path: '/points-order-detail-skus/:tid',
    title:  '积分订单详情-商品清单-康美日记',
    exact: true,
    asyncComponent: () =>
      System.import('./points-order-detail/points-order-detail-skus')
  },
  {
    path: '/distribute-order-detail-skus/:tid',
    title:  '客户订单详情-商品清单-康美日记',
    exact: true,
    asyncComponent: () =>
      System.import('./customer-order-detail/order-detail-skus')
  },

  {
    path: '/store-main/:sid',
    title:  '店铺首页-康美日记',
    exact: true,
    asyncComponent: () => System.import('./store-main'),
    openAccess: true
  },
  {
    path: '/service-store-main/:sid',
    title:  '店铺首页-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-store-main'),
    openAccess: true
  },
  {
    path: '/store-list',
    title:  '店铺列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./store-list'),
    openAccess: true
  },
  {
    path: '/service-store-list',
    title:  '店铺列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-store-list'),
    openAccess: true
  },
  {
    path: '/store-goods-search/:sid',
    title:  '店铺搜索-康美日记',
    exact: true,
    asyncComponent: () => System.import('./shop-search'),
    openAccess: true
  },
  {
    path: '/store-goods-cates/:sid',
    title:  '店铺分类-康美日记',
    exact: true,
    asyncComponent: () => System.import('./shop-goods-cate'),
    openAccess: true
  },
  {
    path: '/store-goods-list/:sid',
    title:  '店铺商品-康美日记',
    exact: true,
    asyncComponent: () => System.import('./shop-goods-list'),
    openAccess: true
  },
  {
    path: '/store-profile/:sid',
    title:  '店铺档案-康美日记',
    exact: true,
    asyncComponent: () => System.import('./store-profile'),
    openAccess: true
  },
  {
    path: '/payment-delivery',
    title:  '支付配送-康美日记',
    exact: true,
    asyncComponent: () => System.import('./payment-delivery')
  },
  {
    path: '/member-shop/:sid',
    title:  '店铺会员-康美日记',
    exact: true,
    asyncComponent: () => System.import('./member-shop'),
    openAccess: true
  },
  {
    path: '/store-attention',
    title:  '关注店铺-康美日记',
    exact: true,
    asyncComponent: () => System.import('./store-attention')
  },
  {
    path: '/error',
    title:  '找不到页面-康美日记',
    exact: true,
    asyncComponent: () => System.import('./error'),
    withoutLogin: true
  },
  {
    path: '/chose-service/:sid',
    title:  '在线客服-康美日记',
    exact: true,
    asyncComponent: () => System.import('./chose-service')
  },
  {
    path: '/linked-account',
    title:  '关联账号-康美日记',
    exact: true,
    asyncComponent: () => System.import('./linked-account')
  },
  {
    path: '/wechat-login',
    title:  '微信登录-康美日记',
    exact: true,
    asyncComponent: () => System.import('./wechat-login'),
    withoutLogin: true
  },
  {
    path: '/my-coupon',
    title:  '我的优惠券-康美日记',
    exact: true,
    asyncComponent: () => System.import('./my-coupon')
  },
  {
    path: '/use-coupon',
    title:  '使用优惠券-康美日记',
    exact: true,
    asyncComponent: () => System.import('./use-coupon')
  },
  {
    path: '/coupon-center',
    title:  '领券中心-康美日记',
    exact: true,
    asyncComponent: () => System.import('./coupon-center'),
    openAccess: true
  },
  {
    path: '/coupon-promotion',
    title:  '优惠券凑单页-康美日记',
    exact: true,
    asyncComponent: () => System.import('./coupon-promotion')
  },
  {
    path: '/about-us',
    title:  '关于我们-康美日记',
    exact: true,
    asyncComponent: () => System.import('./about-us')
  },
  {
    path: '/download',
    title:  'app下载-康美日记',
    exact: true,
    asyncComponent: () => System.import('./download'),
    withoutLogin: true
  },
  {
    path: '/class-equity/:id',
    title:  '等级权益-康美日记',
    exact: true,
    asyncComponent: () => System.import('./user/class-equity'),
    withoutLogin: true
  },
  {
    path: '/evaluation-list/:goodsId',
    title:  '更多评价-康美日记',
    exact: true,
    asyncComponent: () => System.import('./evaluation-list'),
    withoutLogin: true
  },
  {
    path: '/invite-friends',
    title:  '邀请朋友-康美日记',
    exact: true,
    asyncComponent: () => System.import('./invite-friends'),
    withoutLogin: false
  },
  {
    path: '/balance/home',
    title:  '余额-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance/home')
  },
  {
    path: '/balance/deposit',
    title:  '提现-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance/deposit')
  },
  {
    path: '/balance/applicat-form',
    title:  '提现申请单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance/applicat-form')
  },
  {
    path: '/balance/successful/:drawCashId',
    title:  '提现提交成功-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance/successful')
  },
  {
    path: '/balance/cash-record',
    title:  '提现记录-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance/cash-record')
  },
  {
    path: '/balance/cash-detail/:dcId',
    title:  '提现详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance/cash-detail')
  },
  {
    path: '/balance/account-detail',
    title:  '账户明细-康美日记',
    exact: true,
    asyncComponent: () => System.import('./balance/account-detail')
  },
  {
    path: '/sales-perform',
    title:  '我的销售业绩-康美日记',
    exact: true,
    asyncComponent: () => System.import('./sales/sales-perform'),
    withoutLogin: true
  },
  {
    path: '/sales-rank',
    title:  '销售排行-康美日记',
    exact: true,
    asyncComponent: () => System.import('./sales/sales-rank'),
    withoutLogin: true
  },
  {
    path: '/shop-index',
    title:  '店铺管理-康美日记',
    exact: true,
    asyncComponent: () => System.import('./shop/shop-index'),
    withoutLogin: true
  },
  {
    path: '/shop-edit',
    title:  '店铺精选-康美日记',
    exact: true,
    asyncComponent: () => System.import('./shop/shop-edit'),
    withoutLogin: true
  },
  {
    path: '/shop-goods',
    title:  '店铺选品-康美日记',
    exact: true,
    asyncComponent: () => System.import('./shop/shop-goods'),
    withoutLogin: true
  },
  {
    path: '/service-shop-index',
    title:  '店铺管理-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-shop/shop-index'),
    withoutLogin: true
  },
  {
    path: '/service-shop-edit',
    title:  '店铺精选-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-shop/shop-edit'),
    withoutLogin: true
  },
  {
    path: '/service-shop-goods',
    title:  '店铺选品-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-shop/shop-goods'),
    withoutLogin: true
  },
  {
    path: '/social-c/my-customer/:id?',
    title:  '我的用户-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/my-customer'),
    withoutLogin: false
  },
  {
    path: '/graphic-material/:mid',
    title:  '发圈素材-康美日记',
    exact: true,
    asyncComponent: () => System.import('./graphic-material'),
    openAccess: true
  },
  {
    path: '/store-bags',
    title:  '开店礼包-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/store-bags'),
    openAccess: true
  },
  {
    path: '/register-c/:employeeId?/:inviteeId',
    title:  '注册-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/register'),
    withoutLogin: true
  },
  {
    path: '/shop-index-c/:inviteeId?',
    title:  '店铺精选-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/shop-index-c'),
    hasBottom: false,
    withoutLogin: true
  },
  {
    path: '/shop-goods-list-c',
    title:  '店铺精选-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/goods-list-c'),
    withoutLogin: true
  },
  {
    path: '/search-c',
    title:  '搜索页-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/search-c'),
    openAccess: true
  },
  {
    path: '/store-bags-goods-detail/:id',
    title:  '商品详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/store-bags-goods-detail')
  },
  //从店铺精选点击进入的商品详情（都是分销商品）
  {
    path: '/shop-index/goods-detail/:id/:goodsId/:skuId',
    title:  '商品详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/goods-detail'),
    openAccess: true
  },
  {
    path: '/spellgroup-detail/:id',
    title:  '拼团详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./spellgroup-detail'),
    openAccess: true
  },
  {
    path: '/group-buy-detail/:gid',
    title:  '参团详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./group-buy-detail'),
    openAccess: true
  },
  {
    path: '/group-order-list',
    title:  '我的拼购-康美日记',
    exact: true,
    asyncComponent: () => System.import('./group-order-list'),
    openAccess: true
  },
  {
    path: '/group-goods-list',
    title:  '拼团-康美日记',
    exact: true,
    asyncComponent: () => System.import('./group-goods-list'),
    openAccess: true
  },
  {
    path: '/groupon-rule',
    title:  '玩法介绍-康美日记',
    exact: true,
    asyncComponent: () => System.import('./groupon-rule'),
    openAccess: true
  },
  {
    path: '/groupon-center',
    title:  '拼购-康美日记',
    exact: true,
    asyncComponent: () => System.import('./groupon-center'),
    openAccess: true
  },
  {
    path: '/service-groupon-center',
    title:  '拼购-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-groupon-center'),
    openAccess: true
  },
  {
    path: '/groupon-selection',
    title:  '热拼排行-康美日记',
    exact: true,
    asyncComponent: () => System.import('./groupon-selection'),
    openAccess: true
  },
  {
    path: '/groupon-search-list',
    title:  '拼购-康美日记',
    exact: true,
    asyncComponent: () => System.import('./groupon-search-list'),
    openAccess: true
  },
  {
    path: '/service-groupon-search-list',
    title:  '拼购-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-groupon-search-list'),
    openAccess: true
  },

  {
    path: '/evaluation-detail',
    title:  '评价详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./evaluation-detail'),
    withoutLogin: true
  },
  {
    path: '/evaluate/evaluate-center',
    title:  '评价中心-康美日记',
    exact: true,
    asyncComponent: () => System.import('./evaluate/evaluate-center'),
    withoutLogin: true
  },
  {
    path: '/evaluate/evaluate-drying',
    title:  '评价晒单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./evaluate/evaluate-drying'),
    withoutLogin: true
  },
  {
    path: '/points-mall',
    title:  '积分商城-康美日记',
    exact: true,
    asyncComponent: () => System.import('./points-mall'),
    withoutLogin: true
  },
  {
    path: '/flash-sale',
    title:  '整点秒杀-康美日记',
    exact: true,
    asyncComponent: () => System.import('./flash-sale'),
    withoutLogin: true
  },
  {
    path: '/not-develop',
    title:  '敬请期待-康美日记',
    exact: true,
    asyncComponent: () => System.import('./not-develop'),
    withoutLogin: true
  },
  {
    path: '/points-order-list',
    title:  '积分订单列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./points-order-list'),
    hasBottom: true
  },
  {
    path: '/points-order-ship-record/:tid/:type',
    title:  '积分订单发货记录-康美日记',
    exact: true,
    asyncComponent: () => System.import('./points-order-ship-record')
  },
  {
    path: '/points-order-detail/:tid',
    title:  '积分订单详情-康美日记',
    exact: true,
    asyncComponent: () => System.import('./points-order-detail')
  },
  {
    path: '/distribution-center',
    title:  '分销中心-康美日记',
    exact: true,
    asyncComponent: () => System.import('./distribution-center'),
    hasBottom: true
  },
  {
    path: '/distribution-rule',
    title:  '分销员规则介绍-康美日记',
    exact: true,
    asyncComponent: () => System.import('./distribution-rule'),
    hasBottom: false
  },
  {
    path: '/reward-center',
    title:  '奖励中心-康美日记',
    exact: true,
    asyncComponent: () => System.import('./reward-center'),
    hasBottom: true
  },
  {
    path: '/distribution-register',
    title:  '分销注册-康美日记',
    exact: true,
    asyncComponent: () => System.import('./distribution-register'),
    withoutLogin: true
  },
  {
    path: '/material-circle',
    title:  '发现-康美日记',
    exact: true,
    asyncComponent: () => System.import('./material-circle')
  },
  {
    path: '/distribution-goods-list',
    title:  '推广商品列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./distribution-goods-list'),
    hasBottom: true
  },
  {
    path: '/service-distribution-goods-list',
    title:  '推广商品列表-康美日记',
    exact: true,
    asyncComponent: () => System.import('./service-distribution-goods-list'),
    hasBottom: true
  },
  {
    path: '/flash-sale-goods-panic-buying',
    title:  '秒杀活动商品抢购排队页面-康美日记',
    exact: true,
    asyncComponent: () => System.import('./flash-sale-goods-panic-buying'),
    withoutLogin: true
  },
  {
    path: '/flash-sale-order-confirm',
    title:  '秒杀商品确认订单-康美日记',
    exact: true,
    asyncComponent: () => System.import('./flash-sale-order-confirm')
  },
  {
    path: '/flash-sale-order-invoice/:cid',
    title:  '秒杀商品确认订单-索取发票-康美日记',
    exact: true,
    asyncComponent: () => System.import('./flash-sale-order-invoice')
  },
  {
    path: '/share',
    title: '分享-康美日记',
    exact: true,
    asyncComponent: () => System.import('./share'),
    openAccess: true
  },
  {
    path: '/social-c/my-subordinates',
    title:  '我的下级-康美日记',
    exact: true,
    asyncComponent: () => System.import('./social-c/my-subordinates')
  },
];
routers.push(errorRouter);
export { routers };
