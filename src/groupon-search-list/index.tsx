import React from 'react';
import { StoreProvider } from 'plume2';
import { GrouponBottom } from 'wmkit';
import { GrouponSearch } from 'biz';

import AppStore from './store';
import SelectList from './component/select-list';

import './css/style.css';
@StoreProvider(AppStore)
export default class GroupSerchList extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '拼购';

    // 搜索关键字
    let queryString = '';

    // 拼团商品列表带搜索条件目前仅有一个来源：从搜索页面进行搜索
    // 解析url中的参数,形如 ?a=1&b=2&c=3
    let search = this.props.location.search;
    if (search) {
      search = search.substring(1, search.length); //截取后a=1&b=2&c=3
      let searchArray = search.split('&');
      searchArray.forEach((kv) => {
        const split = kv.split('=');
        if (split[0] === 'q') {
          queryString = decodeURIComponent(split[1]) || '';
        }
      });
    }
    this.store.init(queryString);
  }

  render() {
    return (
      <div>
        <div className="groupon-center">
          <GrouponSearch keyWords={this.store.state().get('keyWords')} />
          <SelectList />
          <GrouponBottom currTab={'热拼排行'} />
        </div>
      </div>
    );
  }
}
