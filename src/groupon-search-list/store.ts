import { Store } from 'plume2';
import { config } from 'config';
import GrouponCenterActor from './actor/groupon-center-actor';
import { Confirm, history } from 'wmkit';
export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new GrouponCenterActor()];
  }

  /**
   * 初始化加载数据
   * @returns {Promise<void>}
   */
  init = (queryString) => {
    this.transaction(() => {
      //搜索关键字
      this.onFieldChange({ field: 'keyWords', value: queryString });
      this.initToRefresh(true);
    });
  };

  /**
   * 数据变更
   * @param {any} field
   * @param {any} value
   */
  onFieldChange = ({ field, value }) => {
    this.dispatch('group: field: change', { field, value });
  };

  /**
   * 存储wmlistview组件的初始化方法,用于刷新
   * @param _init
   */
  initToRefresh = (flag) => {
    this.onFieldChange({ field: 'toRefresh', value: flag });
  };
}
