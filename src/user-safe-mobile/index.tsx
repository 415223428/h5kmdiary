import React from 'react'
import {StoreProvider} from 'plume2';
import AppStore from './store'
import SafeMobileItem from './component/safe-mobile-item'
const next=require('./img/next.png')
@StoreProvider(AppStore, {debug: __DEV__})
export default class UserSafeMobile extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    return (
      <div className="content register-content">
        <SafeMobileItem/>
        <div className="register-btn" style={{padding:'0'}}>
          {/* <button className="btn btn-primary" onClick={() => {
            this.store.doNext()
          }}>下一步
          </button> */}
          <img src={next} alt="" style={{width:'100%'}} 
               onClick={() => {
                this.store.doNext()
          }}/>
        </div>
      </div>
    )
  }
}
