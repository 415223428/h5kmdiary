import {Store, IOptions} from 'plume2';
import * as webapi from './webapi'
import {Alert, history, ValidConst} from 'wmkit'
import {config, cache} from 'config';
import SafeMobileActor from './actor/safe-mobile-actor'


/**
 * 手机号码校验*/
function _testTel(tel: string) {
  const regex = ValidConst.phone
  if (tel == "") {
    Alert({
      text: '请填写手机号'
    })
    return false
  }
  else if (!regex.test(tel)) {
    Alert({
      text: '无效的手机号！'
    })
    return false
  } else {
    return true
  }
}

export default class AppStore extends Store {
  bindActor() {
    return [
      new SafeMobileActor
    ]
  }
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  /**
   * 初始化原绑定手机号
   * @returns {Promise<void>}
   */
  init = async () => {
    const res = await webapi.findCustomerMobile()
    if (res.code == config.SUCCESS_CODE) {
      this.dispatch("mobile:init", res.context)
    }
  }


  /**
   * 获取验证码
   * @param value
   */
  getCode = (value: string) => {
    this.dispatch('mobile:code', value)
  }


  /**
   * 发送验证码
   * @returns {Promise<Result<ReturnResult>>}
   */
  sendCode = async () => {
    const mobile = this.state().get('mobile')
    return webapi.sendCode(mobile).then(res => {
      const {code, message} = res
      if (code == config.SUCCESS_CODE) {
        this.dispatch('mobile:isValid', true)
        Alert({
          text: '验证码已发送，请注意查收！',
          time:1000
        })
      } else {
        Alert({
          text: message
        })
        return Promise.reject(message)
      }
    })
  }


  /**
   * 下一步 验证输入的验证码对不对
   * @returns {Promise<boolean>}
   */
  doNext= async() => {
    const mobile = this.state().get('mobile')
    const verifiedCode= this.state().get('code')
    if(_testTel(mobile)){
      if(verifiedCode==""){
        Alert({
          text:"请填写验证码！",
          time:1000
        })
      } else {
        const regex = /^\d{6}$/
        if (!regex.test(verifiedCode)){
          Alert({
            text:"验证码错误！",
            time:1000
          })
          return
        }

        const {code,message, context}= await webapi.testCode(mobile,verifiedCode)
        if(code==config.SUCCESS_CODE){
          /**验证码校验成功，跳转到下一个页面*/
          history.push({
            pathname:'/user-safe-mobile-next',
          })
          localStorage.setItem(cache.OLD_VERIFY_CODE, context as any)
        }else{
          Alert({
            text:message,
            time:1000
          })
        }
      }
    }
  }
}
