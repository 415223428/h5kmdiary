import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import { Tips } from 'wmkit';
import ShopGoodsList from './component/shop-goods-list';
const signMark=require('./img/sign-mark.png')
const fenLei=require('./img/fenlei.png')
const list=require('./img/list.png')
require('./css/style.css')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class ShopEdit extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {}

  componentWillUnmount() {
    // console.log('--------组件被销毁-------');
    // this.store.save();

  }

  render() {
    const isLarge = this.store.state().get('isLarge');
    return (
      <div className="content shop" style={{background: '#fafafa'}}>
        <div className="shop-tip-box scroll-fixed-box"
             style={{display:'flex',flexDirection:'row',backgroundColor:'#FFF5F5'}}  
        >
          <div style={{backgroundColor:'#FFF5F5',height:'40.01px',display:'flex',alignItems:'center'}}> 
            <img src={signMark} alt=""
                style={{width:'0.3rem',height:'0.3rem',margin:' 0 0.25rem 0 0.2rem',alignItems:'center',justifyContent:'center'}}
            />
            <span style={{color:'rgb(153, 153, 153)',fontSize:'13px',fontStretch:'Regular',fontFamily:'PingFang-SC-Regular'}}>
              长按商品并拖动排序
            </span>
          </div>
          
          {/* <Tips
            styles="hint-box"
            // iconName="icon-jinggao"
            text="长按商品并拖动排序"
          /> */}
          <div className="list-tab" onClick={() => this.store.setIsLarge()}>
            {/* <i
              className={'iconfont ' + (isLarge ? 'icon-fenlei' : 'icon-datu')}
            /> */}
           
            {isLarge?(
               <img src={list} alt="" style={{color:'#666',width:'0.3rem'}}/>
            ):(
              <img src={fenLei} alt="" style={{color:'#666',width:'0.3rem'}}/>
            )} 
            {/* <span>{isLarge ? '列表' : '大图'}</span>*/}
          </div>
        </div>
        <ShopGoodsList />
      </div>
    );
  }
}
