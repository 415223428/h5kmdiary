import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };

/**
 * 删除分销商品
 */
export const delCommodityDistribution = (goodsInfoId: String) => {
  return Fetch<TResult>('/distributor-goods/delete', {
    method: 'POST',
    body: JSON.stringify({ goodsInfoId: goodsInfoId })
  });
};

/**
 * 商品排序
 */
export const changeSort = (goodsInfos) => {
  return Fetch<TResult>('/distributor-goods/modify-sequence', {
    method: 'POST',
    body: JSON.stringify({distributorGoodsInfoDTOList:goodsInfos})
  });
};