import { IOptions, Store } from 'plume2';
import { fromJS } from 'immutable';
import ShopEditActor from './actor/shop-edit-actor';
import { config } from 'config';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new ShopEditActor()];
  }

  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  setIsLarge = () => {
    this.dispatch('shop-edit:setIsLarge');
    this.dispatch('shop-edit:clean');
  };

  /**
   * 商品列表ListView查询得到的数据返回处理
   * @param data
   */
  handleDataReached = (data: any) => {
    if (data.code !== config.SUCCESS_CODE) {
      return;
    }

    const { content } = data.context.esGoodsInfoPage;
    let goosinfo = this.state().get('goodsinfo');
    if (!goosinfo.isEmpty()) {
      const newGoodsInfo = goosinfo.concat(fromJS(content));
      this.dispatch('shop-edit:goodsinfo', newGoodsInfo);
    } else {
      this.dispatch('shop-edit:goodsinfo', fromJS(content));
    }
  };

  //删除分销商品
  delCommodityDistribution = async (goodsInfoId, index) => {
    const { code, context, message } = await webapi.delCommodityDistribution(goodsInfoId);
    if (code == config.SUCCESS_CODE) {
      let goodsinfo = this.state().get('goodsinfo');
      const newGoodsinfo = goodsinfo.delete(index);
      this.dispatch('shop-edit:goodsinfo', newGoodsinfo);
      return code;
    } else {
      return message;
    }
  };

  setGoodsInfo = (arr) => {
    this.dispatch('shop-edit:goodsinfo', fromJS(arr));
    this.save();
  }

  save = () => {
    let goosinfo = this.state().get('goodsinfo');
    let goodsArray = new Array();
    goosinfo.map((value, index, array) => {
      const goods = {
        'id': value.getIn(['goodsInfo', 'distributionGoodsInfoId']),
        'goodsInfoId': value.getIn(['goodsInfo','goodsInfoId']),
        'goodsId': value.getIn(['goodsInfo','goodsId']),
        'sequence': index,
        'storeId': value.getIn(['goodsInfo','storeId'])
      };
      goodsArray.push(goods);
    })
    webapi.changeSort(goodsArray);
  };
}
