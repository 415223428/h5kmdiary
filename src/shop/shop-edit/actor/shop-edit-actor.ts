import { Action, Actor, IMap } from 'plume2';
import { Map } from 'immutable';

export default class ShopEditActor extends Actor {
  defaultState() {
    return {
      isLarge: false,
      goodsinfo: []
    };
  }

  @Action('shop-edit:setIsLarge')
  setIsLarge(state: IMap) {
    return state.set('isLarge', !state.get('isLarge'));
  }

  @Action('shop-edit:setList')
  setList(state: IMap, arr) {
    return state.set('items', arr);
  }

  @Action('shop-edit:goodsinfo')
  setGoodsinfo(state: IMap, arr) {
    return state.set('goodsinfo', arr);
  }

  @Action('shop-edit:clean')
  cleanGoodsinfo(state: IMap) {
    return state.set('goodsinfo', Map());
  }
}
