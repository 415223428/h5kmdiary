import React from 'react';
import {
  SortableContainer,
  SortableElement,
  arrayMove
} from 'react-sortable-hoc';
import GoodsItem from './goods-item';
import ShopGoodsItemSmall from './shop-goods-item-small';
import { Relax } from 'plume2';
import { Alert, Blank, ListView, noop } from 'wmkit';
import { fromJS } from 'immutable';
import { config, url } from 'config';
import wrapUrl from '../../../../web_modules/wmkit/image-util/wrap-url';
import { div } from '../../../../web_modules/wmkit/common/util';
const listNone=require('../img/no-order-form.png')
const SortItem = SortableElement(({ children }) =>{ return children });
const LargeImgList = SortableContainer(({ children }) => {
  return <div className="content-small">{children}</div>;
});
const InfoItemList = SortableContainer(({ children }) => {
  return <div className="content-child">{children}</div>;
});
@Relax
export default class ShopGoodsList extends React.Component<any, any> {
  _listView: any;

  constructor(props) {
    super(props);
    this.state = {
      height: 'calc(100vh - 0.8rem)'
    };
  }

  props: {
    relaxProps?: {
      items: any;
      isLarge: Boolean;
      handleDataReached: (data: Object) => void;
      goodsinfo: any;
      setGoodsInfo: Function;
      delCommodityDistribution: Function;
    };
  };

  static relaxProps = {
    items: 'items',
    isLarge: 'isLarge',
    handleDataReached: noop,
    goodsinfo: 'goodsinfo',
    setGoodsInfo: noop,
    delCommodityDistribution: noop
  };

  render() {
    const { isLarge, handleDataReached } = this.props.relaxProps;
    return (
      <div className="content" style={{background:'rgb(250,250,250)'}}>
        {isLarge ? (
          <LargeImgList
            useWindowAsScrollContainer={true}
            axis="xy"
            pressDelay={200}
            onSortEnd={this.onSortEnd}
            onSortStart={this.onSortStart}
          >
            <ListView
              className="content small-view-box"
              url={'/goods/shop/sku-list'}
              // height: this.state.height
              style={{ height: '100%',background:'#fff',borderRadius:'0.2rem'}}
              dataPropsName={'context.esGoodsInfoPage.content'}
              otherProps={['goodsIntervalPrices']}
              pageSize={36}
              renderRow={(item: any, index) => {
                return (
                    <SortItem key={`item-${index}`} index={index}
                    >
                      <ShopGoodsItemSmall
                        index={index}
                        handleDelete={this.del}
                        goodsItem={fromJS(item)}
                      />
                    </SortItem>
                );
              }}
              renderEmpty={() => 
                <Blank img={listNone} content="没有搜到任何商品～" />           
              }
              ref={(_listView) => (this._listView = _listView)}
              onDataReached={handleDataReached}
            />
          </LargeImgList>
        ) : (
          <InfoItemList
            useWindowAsScrollContainer={true}
            pressDelay={200}
            onSortEnd={this.onSortEnd}
            onSortStart={this.onSortStart}
          >
            <ListView
              className="content shop-sort-box"
              url={'/goods/shop/sku-list'}
              pageSize={10}
              // height: this.state.height
              style={{ height: '100%',borderRadius:'0.2rem'}}
              dataPropsName={'context.esGoodsInfoPage.content'}
              otherProps={['goodsIntervalPrices']}
              renderRow={(item: any, index) => {
                return (
                  <SortItem key={`item-${index}`} index={index}>
                    <GoodsItem
                      index={index}
                      handleDelete={this.del}
                      goodsItem={fromJS(item)}
                    />
                  </SortItem>
                );
              }}
              renderEmpty={() => 
               <Blank img={listNone} content="没有搜到任何商品～" />
              }
              ref={(_listView) => (this._listView = _listView)}
              onDataReached={handleDataReached}
            />
          </InfoItemList>
        )}
      </div>
    );
  }

  del = async (index, goodsInfoId) => {
    const { delCommodityDistribution } = this.props.relaxProps;
    const res = await delCommodityDistribution(goodsInfoId, index);
    if (res == config.SUCCESS_CODE) {
      this._listView._updateDataSource(index);
    } else {
      Alert({ text: res });
    }
  };

  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState({
      height: 'calc(100vh - 0.8rem)'
    });
    const { goodsinfo, setGoodsInfo } = this.props.relaxProps;
    let _goodsinfo = goodsinfo.toJS();
    _goodsinfo = arrayMove(_goodsinfo, oldIndex, newIndex);
    setGoodsInfo(_goodsinfo);
    this._listView._changeArray(oldIndex, newIndex);
  };

  onSortStart = (sort, event) => {
    this.setState({
      height: 'auto'
    });
  };
}
