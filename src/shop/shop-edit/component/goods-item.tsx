import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMImage, _, Alert } from 'wmkit';
import { GoodsNum, MarketingLabel } from 'biz';
import { config } from 'config';

const del=require('../img/del.png')
export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    const { index,handleDelete } = this.props;
    const goodsItem = this.state.goodsItem;

    //是否失效
    const invalid = false;
    // 社交电商相关内容显示与否
    const social = true;
    //禁用分享赚
    const socialDisabled = false;

    let goodsInfo;
    if (goodsItem){
      // sku信息
      goodsInfo = goodsItem.get('goodsInfo');
    }


    return goodsItem? (
      <div
        className="goods-list"
        style={{padding:' 0.16rem 0 0 0'}}
        // onClick={() => history.push('/goods-detail/' + 'id')}
      >
        <div className="img-box" style={{transform:'scale(1.1,1.1)',margin:' 0.15rem 0.2rem 0.2rem 0.3rem'}}>
          <WMImage src={goodsInfo.get('goodsInfoImg')} width="100%" height="100%" />
        </div>
        <div className="detail b-1px-b" style={{paddingTop:'0.12rem'}}>
          <div style={{color:'#333',fontWeight:'bold',fontFamily:'PingFang-SC-Bold'}} className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>


          <div className="marketing">
            {goodsInfo.get('companyType') == 0 && (
              <div className="self-sales">自营</div>
            )}
            {/*{(marketingLabels || couponLabels) && (*/}
              {/*<MarketingLabel*/}
                {/*marketingLabels={marketingLabels}*/}
                {/*couponLabels={couponLabels}*/}
              {/*/>*/}
            {/*)} */}
          </div>

          <div className="bottom" style={{flexDirection:'column',alignItems:'flex-start',height:'100%'}}>
            <div className="bottom-price">
              <span className="price" style={{fontSize:'0.4rem'}}>
                <i className="iconfont icon-qian" style={{marginLeft:'-0.09rem'}}/>
                {/* {this._calShowPrice(goodsItem, buyCount)} */}
                {_.addZero(goodsInfo.get('marketPrice'))}
              </span>
              {social && (
                <span
                  className={
                    invalid ? 'commission commission-disabled' : 'commission'
                  }
                  
                >
                  &nbsp;赚 
                  <span style={{color:'#FF4D4D'}}>
                    <i className="iconfont icon-qian" style={{fontSize:'0.24rem'}}/>
                    {_.addZero(goodsInfo.get('distributionCommission'))}
                  </span>
                </span>
              )}
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            {social ? (
              !invalid && (
                <div className="social-btn-box">
                  <div
                    // className={
                    //   socialDisabled
                    //     ? 'social-btn social-btn-disabled'
                    //     : 'social-btn'
                    // }
                    onClick={(e) => {
                      handleDelete(index,goodsInfo.get('goodsInfoId'));
                      e.stopPropagation();
                    }}
                    style={{margin:'0.12rem 0',width:'1.2rem'}}
                  >
                    <img src={del} alt=""/>
                  </div>
                </div>
              )
            ) : (
              <GoodsNum
                value={1}
                max={3}
                disableNumberInput={invalid}
                goodsInfoId={'id'}
                onAfterClick={this._afterChangeNum}
              />
            )}
          </div>
        </div>
      </div>
    ) : null;
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
  };

  // _del = async (goodsInfoId) => {
  //   const { handleDelete } = this.props;
  //   const res = await handleDelete(goodsInfoId);
  //   if (res == config.SUCCESS_CODE){
  //     this.setState({'goodsItem':''},()=>{
  //       Alert({ text: '删除成功' });
  //     });
  //   } else {
  //     Alert({ text: res });
  //   }
  // };
}
