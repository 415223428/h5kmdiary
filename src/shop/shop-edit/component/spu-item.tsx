import React from 'react';
import { Link } from 'react-router-dom';
import { fromJS } from 'immutable';
import { history, WMImage, _, AddCart } from 'wmkit';
import { MarketingLabel } from 'biz';

export default class SpuItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const { index, handleDelete } = this.props;
    //是否失效
    const invalid = true;
    // 社交电商相关内容显示与否
    const social = true;
    //禁用分享赚
    const socialDisabled = false;

    return (
      <div
        className="goods-list spu-goods invalid-goods"
        onClick={() => history.push('/goods-detail/' + 'id')}
      >
        <div className="img-box">
          <WMImage src={''} width="100%" height="100%" />
        </div>
        <div className="detail b-1px-b">
          <div className="title">商品名称</div>
          <p className="gec">栖居A 60*40</p>

          {social ? (
            <div className="marketing" />
          ) : (
            <div className="marketing">
              {/* {goodsInfo.get('companyType') == 0 && (
                <div className="self-sales">自营</div>
              )}
              {(marketingLabels || couponLabels) && (
                <MarketingLabel
                  marketingLabels={marketingLabels}
                  couponLabels={couponLabels}
                />
              )} */}
            </div>
          )}

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {/* {goodsInfo.get('priceType') == 1
                  ? _.addZero(intervalMinPrice)
                  : _.addZero(salePrice)} */}
                14.00
              </span>
              {social && (
                <span
                  className={
                    invalid ? 'commission commission-disabled' : 'commission'
                  }
                >
                  /&nbsp;赚12
                </span>
              )}
              {invalid && <div className="out-stock">缺货</div>}
            </div>
            {social ? (
              !invalid && (
                <div className="social-btn-box">
                  <div
                    className={
                      socialDisabled
                        ? 'social-btn social-btn-disabled'
                        : 'social-btn'
                    }
                    onClick={(e) => {
                      handleDelete(index);
                      e.stopPropagation();
                    }}
                  >
                    删除
                  </div>
                </div>
              )
            ) : (
              <AddCart
                disableAdd={invalid}
                skuId={'id'}
                spuAddCartFunc={() => {}}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}
