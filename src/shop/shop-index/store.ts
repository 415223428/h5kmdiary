import { Store, IOptions } from 'plume2';
import { fromJS } from 'immutable';

import { config, cache } from 'config';
import {Alert, Confirm, history} from 'wmkit';

import ShopIndexActor from './actor/shop-index-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new ShopIndexActor()];
  }
 
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  init = async () => {
    const res = await Promise.all([
      webapi.queryDistributeInfo(),
      webapi.queryDistributeSetting()
    ]);
    if (
      res[0].code == config.SUCCESS_CODE &&
      res[1].code == config.SUCCESS_CODE
    ) {
      this.dispatch(
        'InvitActor:baseInfo',{customerInfo: fromJS(res[0].context).get('distributionCustomerVO'),
          settingInfo:  fromJS(res[1].context).get('distributionSettingSimVO')}
      );
    }
  };

  memberInfo = async () => {
    const res = (await Promise.all([
      webapi.shareInfo()
    ])) as any;
      this.dispatch('member:init', {
        customer: res[0].context,
      });
  };
  /**
   * 邀请好友弹层
   */
  changeInvitState = async () => {
    this.dispatch('InvitActor:changeInvitState');
  };

  changeShareVisible = () => {
    this.dispatch('InvitActor:changeShareVisible');
  };

  saveCheckedSku = (sku) => {
    this.dispatch('InvitActor:saveCheckedSku', sku);
  };

  fetchDistributionSetting = async () => {
    let inviteeId =
      JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '';
    if (inviteeId) {
      const { code, context, message } = await webapi.fetchDistributionSetting(
        inviteeId
      );
      //存储分享店铺的图片
      if (code == config.SUCCESS_CODE) {
        this.dispatch(
          'InvitActor:shopShareImg',
          (context as any).distributionSettingSimVO.shopShareImg
        );
      }
    }
  };

    //改变分享页面的开关
    changefenxiang = () => {
      this.dispatch('goods-actor:isfenxiang');
    };
    cloasefenxiang = () => {
      this.dispatch('goods-actor:closeWechatShare');
      this.dispatch('goods-actor:closefenxiang');
    };
  
    iscloseImg = () => {
      this.dispatch('goods-actor:closeImg');
    };
  
    isopenImg = () => {
      this.dispatch('goods-actor:openImg');
    };
  
    //传入对应的分享页面Id
    sendId = (fenxiangId) => {
      this.dispatch('goods-actor:fenxiangId', fenxiangId);
    }

    openWechatShare=()=>{
      this.dispatch('goods-actor:openWechatShare');
    }

    getgoodsDetail = async (goodsInfo,url)=>{
      const loginData = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));
      let customerId = loginData.customerId;
      const goodsId = goodsInfo.get('goodsId')
      const skuId = goodsInfo.get('goodsInfoId')
      const {code,context} = await webapi.initGoodDetail(customerId, goodsId, skuId);
      
      if(code === config.SUCCESS_CODE){
        history.push({
          pathname: '/share',
          state: {
            goodsInfo ,
            url,
            lineDrawingPrice:(context as any).goods.linePrice,
          }
        });
      }
    }
}
