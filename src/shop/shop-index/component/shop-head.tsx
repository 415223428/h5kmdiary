import React from 'react';
import { StoreProvider } from 'plume2';
import wx from 'weixin-js-sdk';
import { ShareModal, BStoreClose } from 'biz';
import { cache } from 'config';
import AppStore from '../store';

import { Alert } from 'wmkit';
import { IMap, Relax } from 'plume2';
import { history } from 'wmkit';
// import { url } from '../../../../web_modules/config/url';
import '../css/style.css';
// import { div } from '../../../../web_modules/wmkit/common/util';
const shopHeadBg = require('../img/shop-bg.png');
const defaultImg = require('../../../../web_modules/images/default-img.png');
const selectGoods =require('../img/select.png')
const share=require('../img/share.png')
const edit=require('../img/edit.png')
/**
 *
 */
@Relax
export default class ShopHead extends React.Component<any, any> {
  store: AppStore;
  props: {
    relaxProps?: {
      customerInfo: IMap;
      settingInfo: IMap;
      customer:any;
    };
  };

  static relaxProps = {
    customerInfo: 'customerInfo',
    settingInfo: 'settingInfo',
    customer:'customer',
  };

  render() {
    const { customerInfo, settingInfo ,customer} = this.props.relaxProps;
    let headImg = customerInfo.get('headImg')
      ? customerInfo.get('headImg')
      : defaultImg;
    let customerName = `${customer.get('customerId')}`;
    customerName = customerName.length<32?customerName:customer.get('customerName')
    return (
      <div
        className="shop-head scroll-fixed-box"
        style={{
          background: 'url(' + shopHeadBg + ') 0 0 no-repeat',
          backgroundSize: '100% 100%'
        }}
      >
        <div>
          <i
            className="iconfont icon-jiantou"
            style={{ color: 'transparent' }}
          />
        </div>
        <div className="shop-head-bottom">
          <div className="shop-head-info">
            <div className="shop-head-info-img">
              <img src={headImg} alt="" />
            </div>
            <div>
              <p className="shop-head-info-name">
                {/* {customerInfo && customerInfo.get('customerName')}的{settingInfo && */}
                {customerInfo && customerName}的{settingInfo &&
                  settingInfo.get('shopName')}
              </p>
              <span className="shop-head-info-tag" 
                    style={{background:'linear-gradient(#F5E190,#FFF8CC,#F0CB75)',color:'#B37400',borderRadius:'0.2rem',fontSize:'0.17rem'}}>
                {customerInfo && customerInfo.get('distributorLevelName')}
              </span>
            </div>
          </div>
          <div className="shop-head-btns" style={{display:'flex'}}>
            <a
              className="shop-head-btn"
              href="javascript:;"
              onClick={() => this._shareStore()}
            >
              <img src={share} alt=""/>              
              <span>分享</span>
            </a>
            <div style={{borderLeft:'0.001rem solid #999',borderRight:'0.001rem solid #999'}}>
              
              <a
                className="shop-head-btn"
                href="javascript:;"
                onClick={() => history.push('/shop-edit')}
              >
                <img src={edit} alt=""/>
                <span>编辑</span>
              </a>
            </div>
            <a
              className="shop-head-btn"
              href="javascript:;"
              onClick={() => history.push('/shop-goods')}
            >
              {/* <i className="iconfont icon-xuanpin" /> */}
              <img src={selectGoods} alt="" />
              <span>选品</span>
            </a>
          </div>
        </div>

         {/*分享赚弹框*/}
        {/* <ShareModal
          shareType={1}
          goodsInfo={this.store.state().get('checkedSku')}
          visible={this.store.state().get('shareVisible')}
          changeVisible={this.store.changeShareVisible}
        /> */}

        {/*分销员禁用状态提示*/}
        {/* <BStoreClose /> */}

        {/* 分享店铺 */}
        {/* {invitState && <ShopShare />} */}
      </div>
    );
  }

    //去小程序原生页面分享店铺
  _shareStore = () => {
    //小程序打开，去邀请
    if ((window as any).isMiniProgram) {
      const baseInfo = this.store
        .state()
        .get('baseInfo')
        .toJS();
      let params = {
        baseInfo: baseInfo,
        headImg: this.store
          .state()
          .get('customerInfo')
          .get('headImg'),
        inviteeId:
          JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '',
        token: (window as any).token
      };
      //这边获取下商品的数据等
      wx.miniProgram.navigateTo({
        url: `../share-store/share-store?data=${JSON.stringify(params)}`
      });
    } else {
      Alert({
        text: '请到小程序端进行该操作！'
      });
    }
  };
}
