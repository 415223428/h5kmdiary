import React from 'react';
import { Relax } from 'plume2';

const emptyImg = require('../img/shop-empty-img.png');

/**
 *
 */
@Relax
export default class ShopEmpty extends React.Component<any, any> {
  props: {
    relaxProps?: {};
  };

  static relaxProps = {};

  render() {
    return (
      <div className="content shop-empty">
        <img src={emptyImg} alt="" />
        <p>店主比较懒，还没有添加商品哦~</p>
      </div>
    );
  }
}
