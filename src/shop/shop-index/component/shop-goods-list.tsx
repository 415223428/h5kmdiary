import React from 'react';
import { Relax, IMap } from 'plume2';
import GoodsItem from './goods-item';
import { Blank, ListView, noop } from 'wmkit';
import { fromJS } from 'immutable';

const noneImg = require('../img/goods-list-none.png');

/**
 *
 */
@Relax
export default class ShopGoodsList extends React.Component<any, any> {
  _listView: any;
  props: {
    relaxProps?: {
      customer: any;
      changeShareVisible: Function;
      saveCheckedSku: Function;
      changefenxiang: Function;
      sendId: Function;
      isfenxiang: boolean;
      cloasefenxiang: Function;
      closeImg: boolean;
      iscloseImg: Function;
      isopenImg: Function;
      fenxiangId: String;
      baseInfo:IMap;
      customerInfo:IMap;
      closeChoose:String;
      openWechatShare:Function;
      isWechatShare:boolean;
      getgoodsDetail:Function;
      linePrice:number;
    };
  };

  static relaxProps = {
    customer: 'customer',
    changeShareVisible: noop,
    saveCheckedSku: noop,
    changefenxiang: noop,
    sendId: noop,
    cloasefenxiang: noop,
    iscloseImg: noop,
    isopenImg: noop,
    openWechatShare:noop,
    getgoodsDetail:noop,

    isfenxiang: 'isfenxiang',
    closeImg: 'closeImg',
    fenxiangId: 'fenxiangId',
    baseInfo:'baseInfo',
    customerInfo:'customerInfo',
    closeChoose:'closeChoose',
    isWechatShare:'isWechatShare',
    linePrice:'linePrice'
  };

  render() {
    let { changeShareVisible, 
      saveCheckedSku, 
      changefenxiang,
      sendId,
      customer,
      isfenxiang,
      cloasefenxiang,
      closeImg,
      iscloseImg,
      isopenImg,
      fenxiangId,
      baseInfo,
      customerInfo,
      closeChoose,
      openWechatShare,
      isWechatShare, 
      getgoodsDetail,
      linePrice,
    } = this.props.relaxProps;
    return (
      <ListView
        className='listView'
        url={'/goods/shop/sku-list'}
        // height: 'calc(100vh - 88px)'
        style={{ height: '100%'}}
        dataPropsName={'context.esGoodsInfoPage.content'}
        otherProps={['goodsIntervalPrices']}
        renderRow={(item: any) => {
          return (
            <GoodsItem
              saveSku={(sku) => saveCheckedSku(sku)}
              goodsItem={fromJS(item)}
              changeShareVisible={() => changeShareVisible()}
              listView={true}
              sendId={sendId}
              changefenxiang={() => changefenxiang()}
              customer={customer}
              baseInfo={baseInfo}
              customerInfo={customerInfo}
              getgoodsDetail={getgoodsDetail}
              linePrice={linePrice}
            />
          );
        }}
        renderEmpty={() => (
          <Blank img={noneImg} content="店主比较懒，还没有添加商品哦~" style={{borderRadius:'0.3rem',paddingBottom:'4.4rem',background:'#fff'}}/>
        )}
        ref={(_listView) => (this._listView = _listView)}
      />
    );
  }
}
