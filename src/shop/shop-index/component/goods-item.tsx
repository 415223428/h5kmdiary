import React from 'react';
import { msg } from 'plume2';
import { Link } from 'react-router-dom';
import { history, WMImage, _, WMkit, Alert } from 'wmkit';
import { GoodsNum } from 'biz';
import { cache } from 'config';
import GoodsShareImg from './goods-share-img'
import { url } from '../../../../web_modules/config/url';
const shareQuan = require('../img/share-quan.png')
const shareEarn = require('../img/share-earn.png')
const queHuo = require('../img/quehuo.png')

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = props;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  render() {
    let { changeShareVisible,
      customer,
      goodsItem,
      baseInfo,
      customerInfo,
      getgoodsDetail,
      linePrice
    } = this.props;
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    //是否失效
    const stock = goodsInfo.get('stock');
    const count = goodsInfo.get('count') || 0;
    const invalid = stock <= 0 || (count > 0 && count > stock);
    // 社交电商相关内容显示与否
    const social = true;
    //禁用分享赚
    const socialDisabled = false;

    let params = {
      baseInfo: baseInfo,
      customerInfo: customerInfo
        .get('headImg'),
      inviteeId:
        JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '',
      token: (window as any).token
    };
    // 预估点数
    let pointNum = goodsInfo.get('kmPointsValue');
    const inviteCode = sessionStorage.getItem('inviteCode');

    const url = `shop-index/goods-detail/${params.inviteeId}/${goodsItem.get('goodsInfo').get('goodsId')}/${goodsItem.get('goodsInfo').get('goodsInfoId')}/?channel=shop&inviteCode=${inviteCode}`;
    return (
      <div>
        {!invalid ?
          <div
            // className={`goods-list ${invalid ? 'invalid-goods' : ''}`}
            className='goods-list'
            onClick={() =>
              this.goToGoodsDetail(goodsInfo.get('goodsId'), goodsInfo.get('goodsInfoId'))
            }
          >
            <div className="img-box" style={{ margin: '0.15rem 0.2rem 0.2rem 0.3rem' }}>
              <WMImage
                src={goodsInfo.get('goodsInfoImg')}
                width="100%"
                height="100%"
              />
            </div>
            <div className="detail b-1px-b" style={{ padding: '0.12rem 0.16rem 0.16rem' }}>
              <div style={invalid ? { color: '#999', fontWeight: 'bold', fontFamily: 'PingFang-SC-Bold' } :
                { color: '#333', fontWeight: 'bold', fontFamily: 'PingFang-SC-Bold' }}
                className="title">{goodsInfo.get('goodsInfoName')}
              </div>
              <p className="gec" style={invalid ? { color: '#999' } : {}}>{goodsInfo.get('specText')}</p>

              <div className="marketing">
                {goodsInfo.get('companyType') == 0 && (
                  <div className="self-sales">自营</div>
                )}
                {/*{(marketingLabels || couponLabels) && (*/}
                {/*<MarketingLabel*/}
                {/*marketingLabels={marketingLabels}*/}
                {/*couponLabels={couponLabels}*/}
                {/*/>*/}
                {/*)}*/}
              </div>

              <div className="bottom" style={{ flexDirection: 'column', alignItems: 'flex-start', height: '100%' }}>
                <div className="bottom-price" style={{ alignItems: 'baseline' }}>
                  <span className={invalid ? '' : 'price'} style={invalid ? { fontSize: '0.4rem', color: '#999' } : { fontSize: '0.4rem' }}>
                    <i className="iconfont icon-qian" style={{ marginLeft: '-0.09rem' }} />
                    {/* {this._calShowPrice(goodsItem, buyCount)} */}
                    {_.addZero(goodsInfo.get('marketPrice'))}
                  </span>
                  {social && (
                    <span
                      className={
                        invalid ? 'commission commission-disabled' : 'commission'
                      }
                      style={{ display: 'flex', alignItems: 'center' }}
                    >
                      &nbsp;赚
                  <span style={invalid ? { color: '#999', fontSize: '0.25rem' } : { color: '#FF4D4D', fontSize: '0.25rem' }}>
                        <i className="iconfont icon-qian" />
                        {_.addZero(goodsInfo.get('distributionCommission'))}
                      </span>
                    </span>
                  )}
                  {localStorage.getItem('loginSaleType') == '1' ? <span style={{ marginLeft: '.25rem', fontSize: '.26rem', color: '#FF4D4D', }}><span style={{ color: '#333' }}>预估点数</span>{pointNum}</span> : null}
                </div>
                {invalid && <div className="Qhuo"><img src={queHuo} /></div>}
                {social ? (
                  !invalid && (
                    <div className="social-btn-box">
                      <div
                        // className="social-btn social-btn-ghost"
                        onClick={async (e) => {
                          e.stopPropagation();
                          const result = await WMkit.getDistributorStatus();
                          if ((result.context as any).distributionEnable) {
                            history.push(
                              `/graphic-material/${goodsInfo.get('goodsInfoId')}`
                            );
                          } else {
                            let reason = (result.context as any).forbiddenReason;
                            msg.emit('bStoreCloseVisible', {
                              visible: true,
                              reason: reason
                            });
                          }
                        }}
                      >
                        <img src={shareQuan} />
                      </div>
                      <div
                        onClick={
                          (e) => {
                            e.stopPropagation();
                            // sendId(goodsItem.get('id'));
                            // changefenxiang();
                            getgoodsDetail(goodsInfo,url);
                          }
                        }
                      >
                        <img src={shareEarn} />
                      </div>
                    </div>

                  )
                ) : (
                    <GoodsNum
                      value={1}
                      max={3}
                      disableNumberInput={invalid}
                      goodsInfoId={'id'}
                      onAfterClick={this._afterChangeNum}
                    />
                  )}
              </div>
            </div>
          </div> :
          null}
      </div>
    );
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  goToGoodsDetail = (goodsId, goodsInfoId) => {
    //小程序打开，去邀请
    // if ((window as any).isMiniProgram) {
    history.push(
      `/shop-index/goods-detail/0/${goodsId}/${goodsInfoId}`
    )
    // } else {
    //   Alert({
    //     text: '请到小程序端进行该操作!'
    //   });
    // }
  }

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.setIn(['goodsInfo', 'buyCount'], num)
    });
  };
}
