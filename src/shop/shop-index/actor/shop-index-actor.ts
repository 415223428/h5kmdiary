import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class ShopIndexActor extends Actor {
  defaultState() {
    return {
      customer: {},
      goodsList: [{ id: 0 }],
      // 分享店铺弹层初始化状态
      invitState: false,
      baseInfo: fromJS({}),
      shareVisible: false,
      //当前的分享赚sku
      checkedSku: fromJS({}),
      //分销员信息
      customerInfo: fromJS({}),
      //分销设置
      settingInfo: fromJS({}),     
      isfenxiang:false,
      closeImg:true,
      fenxiangId:'',
      closeChoose:true,
      isWechatShare:false,
      linePrice:'',
    };
  }

  /**
   * 分享店铺弹层显示
   */
  @Action('InvitActor:changeInvitState')
  changeInvitState(state) {
    return state.set('invitState', !state.get('invitState'));
  }

  /**
   * 分销员基本信息
   */
  @Action('InvitActor:baseInfo')
  setBaseInfo(state, { customerInfo, settingInfo }) {
    return state
      .setIn(['baseInfo', 'customerName'], customerInfo.get('customerName'))
      .setIn(['baseInfo', 'shopName'], settingInfo.get('shopName'))
      .setIn(['baseInfo', 'headImg'], customerInfo.get('headImg'))
      .setIn(
        ['baseInfo', 'distributorName'],
        settingInfo.get('distributorName')
      )
      .set('customerInfo', customerInfo)
      .set('settingInfo', settingInfo);
  }

  @Action('InvitActor:changeShareVisible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }

  @Action('InvitActor:saveCheckedSku')
  saveCheckedSku(state, sku) {
    return state.set('checkedSku', sku);
  }

  @Action('InvitActor:shopShareImg')
  shopShareImg(state, img) {
    return state.setIn(['baseInfo', 'shopShareImg'], img);
  }

    //打开分享页面
    @Action('goods-actor:isfenxiang')
    isfenxiang(state){
      return state.set('isfenxiang',true)
    }
    
  //关闭分享页面
    @Action('goods-actor:closefenxiang')
    closefenxiang(state){
      return state.set('isfenxiang',false)
    }
  
    //关闭img
    @Action('goods-actor:closeImg')
    closeImg(state){
      return state.set('closeImg',false)
      .set('closeChoose',false)
    }
    
    //打开Img
    @Action('goods-actor:openImg')
    openImg(state){
      return state.set('closeImg',true)
      .set('closeChoose',true)
    }
  
    @Action('goods-actor:fenxiangId')
    senId(state,fenxiangId){
      return state.set('fenxiangId',fenxiangId)
    }

    @Action('goods-actor:openWechatShare')
    openWechatShare(state){
      return state.set('isWechatShare',true)
    }

    @Action('goods-actor:closeWechatShare')
    closeWechatShare(state){
      return state.set('isWechatShare',false)
    }
    
    @Action('member:init')
    init(state: IMap, { customer }) {
      return state
        .set('customer', fromJS(customer))
    }

    @Action('goods-actor:linePrice')
    getlinePrice( state ,value){
      return state.set('linePrice',value)
    }
}
