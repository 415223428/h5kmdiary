import React from 'react';
import { StoreProvider } from 'plume2';
import wx from 'weixin-js-sdk';
import { ShareModal, BStoreClose } from 'biz';
import { cache } from 'config';
import AppStore from './store';
import ShopHead from './component/shop-head';
import ShopEmpty from './component/shop-empty';
import ShopGoodsList from './component/shop-goods-list';
import ShopShare from './component/shop-share';
import { Alert } from 'wmkit';
const style = require('./css/style.css');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class ShopIndex extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {}

  componentWillMount(): void {
    this.store.init();
    this.store.memberInfo();
    this.store.fetchDistributionSetting();
  }

  render() {
    const goodsList = this.store.state().get('goodsList');
    const invitState = this.store.state().get('invitState');
    return (
      <div className="content shop" style={{background:'#FAFAFA'}}>
        <ShopHead />
        <div className="shop-list-box" style={{background:'rgb(250, 250, 250)',height:'100%'}}>
          <div className="shop-title-box b-1px-b" 
               style={{fontSize:'0.27rem',color:'#999999',background:'rgb(250, 250, 250)',paddingBottom:'0.25rem'}}>
            <span >店铺精选</span>
          </div>
          {!goodsList || goodsList.size == 0 ? <ShopEmpty /> : null}
          {goodsList && goodsList.size > 0 ? <ShopGoodsList/> : null}
        </div>
        {/* {goodsList && goodsList.size > 0 ? (
          <div
            className="shop-bottom-btn b-1px-t"
            onClick={() => this._shareStore()}
          >
            <a>分享店铺</a>
          </div> 
        ) : null} */}

        {/*分享赚弹框*/}
        {/* <ShareModal
          shareType={1}
          goodsInfo={this.store.state().get('checkedSku')}
          visible={this.store.state().get('shareVisible')}
          changeVisible={this.store.changeShareVisible}
        /> */}

        {/*分销员禁用状态提示*/}
        {/* <BStoreClose /> */}

        {/* 分享店铺 */}
        {/* {invitState && <ShopShare />} */}
      </div>
    );
  }

  //去小程序原生页面分享店铺
  _shareStore = () => {
    //小程序打开，去邀请
    // if ((window as any).isMiniProgram) {
      const baseInfo = this.store
        .state()
        .get('baseInfo')
        .toJS();
      let params = {
        baseInfo: baseInfo,
        headImg: this.store
          .state()
          .get('customerInfo')
          .get('headImg'),
        inviteeId:
          JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '',
        token: (window as any).token
      };
      //这边获取下商品的数据等
      wx.miniProgram.navigateTo({
        url: `../share-store/share-store?data=${JSON.stringify(params)}`
      });
    // } else {
    //   Alert({
    //     text: '请到小程序端进行该操作！'
    //   });
    // }
  };
}
