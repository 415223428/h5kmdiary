import { Fetch } from 'wmkit';

type TResult = { code: string; message: string; context: any };

/**
 * 获取分销员的基本信息
 */
export const queryDistributeInfo = () =>
  Fetch<TResult>('/distribute/distributor-info', { method: 'GET' });

export const queryDistributeSetting = () =>
  Fetch<TResult>('/distribute/setting-invitor', { method: 'GET' });

export const fetchDistributionSetting = (param) => {
  return Fetch<TResult>('/distribute/setting', {
    method: 'POST',
    body: JSON.stringify({
      inviteeId: param
    })
  });
};
export const shareInfo = () => {
  return Fetch('/customer/customerCenter')
}

/**
 * 初始化商品详情
 * @param id
 * @returns {Promise<Result<TResult>>}
 */
export const initGoodDetail = async (id, goodsId, skuId) => {
  return Fetch<Result<any>>(
    `/goods/shop/goods-detail/${id}/${goodsId}/${skuId}`,
    {
      method: 'GET'
    }
  );
};

