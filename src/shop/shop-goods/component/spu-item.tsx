import React from 'react';
import { Link } from 'react-router-dom';
import { fromJS } from 'immutable';
import { history, WMImage, _, AddCart } from 'wmkit';
import { MarketingLabel } from 'biz';

export default class SpuItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const { goods, listView, spuAddCartFunc, changeShareVisible } = this.props;
    // spu下第一个上架的sku信息
    const goodsInfo = goods
      .get('goodsInfos')
      .find((skuInfo) => skuInfo.get('addedFlag') === 1);
    // 商品是否要设置成无效状态(spu下所有sku的库存等于0 或者 起订量大于剩余库存)
    const invalid = goods.get('goodsInfos').every((sku) => {
      const stock = sku.get('stock'); // 库存
      const count = sku.get('count') || 0; // 起订量
      return stock <= 0 || (count > 0 && count > stock);
    });
    // 会员价
    const salePrice = goodsInfo.get('salePrice') || 0;
    // 最低的区间价
    const intervalMinPrice = goodsInfo.get('intervalMinPrice') || 0;
    // 营销标签
    const marketingLabels = goodsInfo.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsInfo.get('couponLabels');

    let containerClass = listView
      ? invalid
        ? 'goods-list spu-goods invalid-goods'
        : 'goods-list spu-goods'
      : invalid
        ? 'goods-box spu-goods invalid-goods'
        : 'goods-box spu-goods';

    // 社交电商相关内容显示与否
    const social = true;
    //禁用分享赚
    const socialDisabled = false;

    return (
      <div
        className={containerClass}
        onClick={() =>
          history.push('/goods-detail/' + goodsInfo.get('goodsInfoId'))
        }
      >
        <div className="img-box">
          <WMImage
            src={goodsInfo.get('goodsInfoImg')}
            width="100%"
            height="100%"
          />
        </div>
        <div className="detail b-1px-b">
          <div className="title">{goodsInfo.get('goodsInfoName')}</div>
          <p className="gec">{goodsInfo.get('specText')}</p>
          {!social && (
            <div className="marketing">
              {goodsInfo.get('companyType') == 0 && (
                <div className="self-sales">自营</div>
              )}
              {(marketingLabels || couponLabels) && (
                <MarketingLabel
                  marketingLabels={marketingLabels}
                  couponLabels={couponLabels}
                />
              )}
            </div>
          )}

          <div className="bottom">
            <div className="bottom-price">
              <span className="price">
                <i className="iconfont icon-qian" />
                {goodsInfo.get('priceType') == 1
                  ? _.addZero(intervalMinPrice)
                  : _.addZero(salePrice)}
              </span>
              {invalid && <div className="out-stock">缺货</div>}
              {!invalid &&
                social && <span className="commission">/&nbsp;赚12</span>}
            </div>
            <div className="social-btn-box">
              <div className="social-btn social-btn-ghost" onClick={(e) => {}}>
                从店铺删除
              </div>
              {/*
              <div className="social-btn" onClick={(e) => {}}>
                加入店铺
              </div>
              */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
