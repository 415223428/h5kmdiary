import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
import GoodsCate from '../../../goods-cate';

@Relax
export default class GoodsTabCate extends Component<any, any> {
  props: {
    relaxProps?: {
      setCateId: Function;
      selectedCate: any;
    };
    hide: boolean;
  };

  static relaxProps = {
    setCateId: noop,
    selectedCate: 'selectedCate'
  };

  render() {
    const { setCateId, selectedCate } = this.props.relaxProps;

    return (
      <div>
        <GoodsCate
          source="goodsList"
          hide={this.props.hide}
          cateId={selectedCate.get('cateId')}
          handleClick={(cateId, cateName) => setCateId(cateId, cateName)}
        />
      </div>
    );
  }
}
