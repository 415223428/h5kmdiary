import React, { Component } from 'react';
import { Relax ,IMap} from 'plume2';
import { noop } from 'wmkit';

const styles = require('../css/style.css');
const Up=require('../img/up.png')
const Down=require('../img/down.png')
const White=require('../img/white.png')

let tab = [
  {
    name: 'goodsCate',
    label: '分类'
  },
  {
    name: 'goodsPrice',
    label: '价格'
  },
  {
    name: 'goodsSort',
    label: '排序'
  },
  {
    name: 'goodsFilter',
    label: '筛选'
  }
];
const sorts = {
  default: '综合',
  dateTime: '最新',
  salesNum: '销量',
  price: '价格',
  evaluateNum: '评论数',
  praise: '好评',
  collection: '收藏'
};

@Relax
export default class GoodsTab extends Component<any, any> {
  props: {
    relaxProps?: {
      openShade: Function;
      closeShade: Function;
      tabName: string;
      selectedCate: any;
      sortType: any;
      setSort: Function;
    };
  };

  static relaxProps = {
    openShade: noop,
    closeShade: noop,
    setSort: noop,

    tabName: 'tabName',
    selectedCate: 'selectedCate',
    sortType: 'sortType',
  };
  render() {
    const {
      openShade,
      closeShade,
      tabName,
      selectedCate,
      sortType,
      setSort,
    } = this.props.relaxProps;
    // 排序字段
    const type = sortType.get('type');
    const sort=sortType.get('sort')
    return (
      <div className={styles.menuBar}>
        <div className={styles.menuContent + ' b-1px-t'}>
          {tab.map((v) => {
            // 是否是当前已展开的tab
            const match = v.name === tabName;

            return (
              <div
                className="item"
                key={v.name}
                onClick={() => {
                  // 在相同的tab上点击时，认为是要关闭tab
                  v.name!=='goodsPrice'?
                  match ? closeShade() : openShade(v.name):setSort('price');
                }}
                style={{fontSize:'1.4rem',fontFamily:'PingFang-SC-Medium',color:'#333333'}}
              >
                <span className={match ? v.name==='goodsPrice'?'comment':'tab-text' : 'comment'} >
                  {v.name === 'goodsCate'
                    ? selectedCate.get('cateName')
                    : v.name == 'goodsSort'
                      ? '排序'
                      : v.label}
                </span>
                {
                 v.name === 'goodsFilter' ? (<i className="iconfont icon-shaixuan" />)
                 :
                  v.name === 'goodsPrice' ?(
                   <img id='priceImg' src={type == 'price'?sort=='asc'?Up:Down:White} 
                        alt="" style={{width:'0.18rem'}}/>) 
                  :
                  match ? 
                    (<i className="iconfont icon-jiantou2 tab-text"
                        style={{ transform: 'rotate(180deg)' }}/>) 
                  :(<i className="iconfont icon-jiantou2" />)
                }
                {/* {v.name === 'goodsFilter' ? (<i className="iconfont icon-shaixuan" />)
                  :
                    v.name === 'goodsPrice' ?(
                    <img src={White} alt="" style={{width:'0.18rem'}}/>) 
                    :
                    match ? 
                      v.name==='goodsPrice'?(<img src={Up} alt="" style={{width:'0.18rem'}}/>) 
                        :(<i className="iconfont icon-jiantou2 tab-text"
                          style={{ transform: 'rotate(180deg)' }}/>) 
                      : 
                    v.name==='goodsPrice'?(<img src={Down} alt="" style={{width:'0.18rem'}}/>) 
                    :(<i className="iconfont icon-jiantou2" />)
                } */}
                
             </div>
            );
          })}
        </div>
      </div>
    );
  }
}
