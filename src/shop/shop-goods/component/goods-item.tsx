import React from 'react';
import { Link } from 'react-router-dom';
import { WMImage, _, Alert } from 'wmkit';
import { MarketingLabel } from 'biz';
const queHuo =require('../img/quehuo.png')
const del=require('../img/del.png')
const join=require('../img/join.png')

export default class GoodsItem extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = { ...props, buttonWaiting: true };
  }

  render() {
    const {
      goodsItem,
      listView,
    } = this.state;
    // skuId
    const id = goodsItem.get('id');
    // sku信息
    const goodsInfo = goodsItem.get('goodsInfo');
    const stock = goodsInfo.get('stock');
    // 商品是否要设置成无效状态
    // 起订量
    const count = goodsInfo.get('count') || 0;
    // 库存等于0或者起订量大于剩余库存
    const invalid = stock <= 0 || (count > 0 && count > stock);
    const buyCount = invalid ? 0 : goodsInfo.get('buyCount') || 0;
    // 营销标签
    const marketingLabels = goodsInfo.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsInfo.get('couponLabels');

    let containerClass = listView
      ? invalid
        ? 'goods-list invalid-goods'
        : 'goods-list'
      : invalid
        ? 'goods-box invalid-goods'
        : 'goods-box';

    // 社交电商相关内容显示与否
    const social = true;
    //禁用分享赚
    const socialDisabled = false;

    return (
      <div
        key={id}
        className={containerClass}
        style={{padding:' 0.16rem 0 0 0'}}
        // onClick={() => history.push('/goods-detail/' + goodsItem.get('id'))}
        >
          <div className="img-box">
            <WMImage
              src={goodsInfo.get('goodsInfoImg')}
              width="100%"
              height="100%"
            />
          </div>
          <div className="detail b-1px-b">
            <div className="title">{goodsInfo.get('goodsInfoName')}</div>
            <p className="gec">{goodsInfo.get('specText')}</p>


            <div className="marketing">
              {goodsInfo.get('companyType') == 0 && (
                <div className="self-sales">自营111</div>
              )}
              {/*{(marketingLabels || couponLabels) && (*/}
              {/*<MarketingLabel*/}
              {/*marketingLabels={marketingLabels}*/}
              {/*couponLabels={couponLabels}*/}
              {/*/>*/}
              {/*)}*/}
            </div>


          <div className="bottom" style={{flexDirection:'column',alignItems:'flex-start',height:'100%'}}>
            <div className="bottom-price">
              <span className="price "  style={{fontSize:'0.37rem'}}>
                <i className="iconfont icon-qian" style={{marginLeft:'-0.09rem'}}/>
                {_.addZero(goodsInfo.get('marketPrice'))}
              </span>
              {invalid && <div style={{height:'0.32rem',marginLeft:'0.2rem'}}><img style={{width:'0.84rem'}} src={queHuo}/></div>}
              {!invalid &&
                social && <span className="commission">&nbsp;赚
                 <span style={{color:'#FF4D4D'}}>
                    <i className="iconfont icon-qian" style={{fontSize:'0.24rem'}}/>
                    {_.addZero(goodsInfo.get('distributionCommission'))}
                  </span>
                </span>}
                {localStorage.getItem('loginSaleType') == '1'?<span style={{marginLeft: '.26rem',fontSize: '.24rem',color: '#FF4D4D',}}><span style={{color:'#333'}}>预估点数</span>{goodsInfo.get('kmPointsValue')}</span>:null}
            </div>
            <div className="social-btn-box">
              {goodsInfo.get('joinDistributior') == 1 ? (
                <div
                  // className="social-btn social-btn-ghost"
                  onClick={(e) => {this.delCommodity(goodsInfo.get('goodsInfoId'))}}
                >
                  <img src={del} alt="" style={{width:'1.5rem'}}/>
                </div>
              ) : (
                <div
                  // className="social-btn"
                  onClick={(e) => {
                    this._addCommodity();
                  }}
                >
                  <img src={join} alt="" />
                </div>
                  )}
              </div>
            </div>
          </div>
        </div>
    );
  }

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsItem, buyCount) => {
    const goodsInfo = goodsItem.get('goodsInfo');
    let showPrice;
    // 阶梯价,根据购买数量显示对应的价格
    if (goodsInfo.get('priceType') === 1) {
      const intervalPriceArr = goodsInfo
        .get('intervalPriceIds')
        .map((id) =>
          goodsItem
            .getIn(['_otherProps', 'goodsIntervalPrices'])
            .find((pri) => pri.get('intervalPriceId') === id)
        )
        .sort((a, b) => b.get('count') - a.get('count'));
      if (buyCount > 0) {
        // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
        showPrice = intervalPriceArr
          .find((pri) => buyCount >= pri.get('count'))
          .get('price');
      } else {
        showPrice = goodsInfo.get('intervalMinPrice') || 0;
      }
    } else {
      showPrice = goodsInfo.get('salePrice') || 0;
    }
    return _.addZero(showPrice);
  };

  //添加分销商品
  _addCommodity = async () => {
    let goodsInfo = this.state.goodsItem.get('goodsInfo');
    const { addCommodityDistribution, buttonWaiting, getCountsByCustomerId } = this.state;
    const countRes = await getCountsByCustomerId(goodsInfo.get('storeId'));
    if (countRes) {
      if (buttonWaiting) {
        this.setState({ buttonWaiting: false });
        const res = await addCommodityDistribution(goodsInfo.get('goodsId'),
          goodsInfo.get('goodsInfoId'),
          goodsInfo.get('storeId'));
        if (res) {
          const g = this.state.goodsItem.updateIn(['goodsInfo', 'joinDistributior'], value => value = 1);
          this.setState({ goodsItem: g }, () => {
            this.setState({ buttonWaiting: true });
          });
        }
      } else {
        Alert({ text: '数据提交中，请稍后' });
      }
    }
  };

  //删除分销商品
  delCommodity = async (goodsInfoId) => {
    let goodsInfo = this.state.goodsItem.get('goodsInfo');
    const { delCommodityDistribution, buttonWaiting } = this.state;
    if (buttonWaiting) {
      this.setState({ buttonWaiting: false });
      const res = await delCommodityDistribution(goodsInfoId);
      if (res) {
        const g = this.state.goodsItem.updateIn(['goodsInfo', 'joinDistributior'], value => value = 0);
        this.setState({ goodsItem: g }, () => {
          this.setState({ buttonWaiting: true });
        });
      }
    } else {
      Alert({ text: '数据提交中，请稍后' });
    }
  }


}
