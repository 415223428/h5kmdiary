import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { noop, history } from 'wmkit';

@Relax
export default class GoodsSearchBar extends React.Component<any, any> {
  props: {
    relaxProps?: {
      queryString: string;
    };
  };

  static relaxProps = {
    queryString: 'queryString'
  };

  render() {
    const { queryString } = this.props.relaxProps;

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div className="input-box" onClick={() =>
                //key: distribute 表示是分销员选品的搜索
                history.push({
                  pathname: '/search',
                  state: { queryString: queryString, key: 'distribute' }
                })
              }
            >
              <i className="iconfont icon-sousuo"></i>
              <input
                style={{textAlign:'center'}}
                type="text"
                value={queryString}
                placeholder="搜索"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
