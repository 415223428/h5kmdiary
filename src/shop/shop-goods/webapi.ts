import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };

/**
 * 获取商品默认展示方式
 *   spu 或者 sku列表
 *   大图 或者 小图
 * @returns {Promise<Result<TResult>>}
 */
export const queryGoodsShowType = () =>
  Fetch<TResult>('/config/goodsDisplayDefault', { method: 'GET' });

/**
 * 根据skuId查询spu相关信息
 * @param id
 */
export const querySpu = async (id) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  return isLoginOrNotOpen
    ? Fetch<TResult>(`/goods/spu/${id}`)
    : Fetch<TResult>(`/goods/unLogin/spu/${id}`);
};

/**
 * 批量加入购物车
 */
export const purchase = (buyGoodsInfos) => {
  return Fetch<TResult>('/site/batchAdd', {
    method: 'POST',
    body: JSON.stringify({ goodsInfos: buyGoodsInfos.toJS() })
  });
};

/**
 * 根据分类id查询商品属性
 * @param params
 */
export const queryProps = (cateId) =>
  Fetch<TResult>(`/goods/props/${cateId}`, { method: 'GET' });

/**
 * 分销员添加分销商品
 */
export const addCommodityDistribution = (
  goodsId: String,
  goodsInfoId: String,
  storeId: String
) => {
  return Fetch<TResult>('/distributor-goods/add', {
    method: 'POST',
    body: JSON.stringify({ goodsId: goodsId, goodsInfoId: goodsInfoId, storeId: storeId })
  });
};

/**
 * 删除分销商品
 */
export const delCommodityDistribution = (goodsInfoId: String) => {
  return Fetch<TResult>('/distributor-goods/delete', {
    method: 'POST',
    body: JSON.stringify({ goodsInfoId: goodsInfoId })
  });
};


/**
 * 分销员添加分销商品
 */
export const getCountsByCustomerId = (
  storeId: String
) => {
  return Fetch<TResult>('/distributor-goods/count', {
    method: 'GET',
  });
};