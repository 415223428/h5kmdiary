import React from 'react';
import { fromJS } from 'immutable';
import { IMap, StoreProvider } from 'plume2';

import { ListView, history, Blank } from 'wmkit';

import AppStore from './store';
import OrderListTop from './component/order-list-top';
import OrderItem from './component/order-list-item';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class OrderList extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    let state = history.location.state ? history.location.state : null;
    if (state) {
      this.store.changeTopActive(state.status);
    }
  }

  render() {
    let form = (this.store.state().get('form') || fromJS([])).toJS();
    let fetchOrders = this.store.fetchOrders;

    return (
      <div>
        <OrderListTop />
        <ListView
          url="/trade/customer/page"
          params={form}
          style={{ height: window.innerHeight - 88 }}
          renderRow={(order: IMap, index: number) => {
            return <OrderItem order={order} index={index} />;
          }}
          renderEmpty={() => (
            <Blank
              img={require('./img/list-none.png')}
              content="您暂时还没有订单哦"
              isToGoodsList={true}
            />
          )}
          onDataReached={(res) => fetchOrders(res)}
        />
      </div>
    );
  }
}
