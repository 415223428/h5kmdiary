/**
 * Created by chenpeng on 2017/7/18.
 */
import { Action, Actor, IMap } from 'plume2';
import { fromJS, List } from 'immutable';

export default class FormActor extends Actor {
  defaultState() {
    return {
      // 列表数据
      form: fromJS({
        flowState: '', //订单流程状态
        payState: '', //订单付款状态
        customerOrderListAllType: true //小b端我的客户列表是否是查询全部
      }),
      orders: [] //订单列表
    };
  }

  /**
   * 设置列表数据
   */
  @Action('order-list-form:setForm')
  setForm(state: IMap, params: IMap) {
    return state.set('form', fromJS(params));
  }

  /**
   * 更新订单数据
   */
  @Action('order-list-form:setOrders')
  updateOrders(state: IMap, params: Array<any>) {
    return state.update('orders', (value: List<any>) => {
      return value.push(...params);
    });
  }

  /**
   * 切换tab时清除订单数据
   */
  @Action('order-list-form:clearOrders')
  clearOrders(state: IMap) {
    return state.set('orders', List());
  }
}
