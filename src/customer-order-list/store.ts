import { fromJS } from 'immutable';

import { IOptions, Store } from 'plume2';

import OrderListTopActor from './actor/order-list-top-actor';
import FormActor from './actor/order-list-form-actor';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new OrderListTopActor(), new FormActor()];
  }

  /**
   * 点击顶部tab
   * @param key tab的key值
   */
  changeTopActive = (key: string) => {
    if (this.state().get('key') === key) {
      return false;
    }

    const [state, value] = key.split('-');
    let form = {};
    form[state] = value;
    if(key==''){
      form['customerOrderListAllType'] = true
    } else {
      form['customerOrderListAllType'] = false
    }

    this.transaction(() => {
      this.dispatch('top:active', key);
      this.dispatch('order-list-form:clearOrders');
      this.dispatch('order-list-form:setForm', fromJS(form));
    });
  };

  /**
   * 设置订单列表
   * @param res
   */
  fetchOrders = (res: any) => {
    if (res.context) {
      this.dispatch('order-list-form:setOrders', fromJS(res.context.content));
    }
  };
}
