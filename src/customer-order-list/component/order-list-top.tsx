import * as React from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';
import "../css/style.css"
import OrderListTopItem from './order-list-top-item';

/**
 * 订单tab datasource
 */
export const tabStatus = [
  { label: '全部', key: '' },
  { label: '待发货', key: 'flowState-AUDIT' },
  { label: '待收货', key: 'flowState-DELIVERED' },
  { label: '已完成', key: 'flowState-COMPLETED' },
  { label: '已作废', key: 'flowState-VOID' }
];

@Relax
export default class OrderListTop extends React.Component<any, any> {
  props: {
    relaxProps?: {
      key: string;
      changeTopActive: Function;
    };
  };

  static relaxProps = {
    key: 'key',
    changeTopActive: noop
  };

  render() {
    const { key } = this.props.relaxProps;

    return (
      <div style={{ height: 40 }}>
        <div className="layout-top">
          <div className="layout-content customer-order-top " style={{width: 'auto'}}>
            {tabStatus.map((o) => (
              <OrderListTopItem
                key={o.key}
                label={o.label}
                active={o.key === key}
                tabKey={o.key}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
