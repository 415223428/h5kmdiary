import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';

import { noop, _, WMkit } from 'wmkit';
import { IList } from 'typings/globalType';

const noneImg = require('../img/none.png');

/**
 * 订单状态
 * @type {{INIT: string; AUDIT: string; DELIVERED_PART: string; DELIVERED: string; CONFIRMED: string; COMPLETED: string; VOID: string}}
 */
const FLOW_STATE = {
  INIT: '待审核',
  AUDIT: '待发货',
  DELIVERED_PART: '待发货',
  DELIVERED: '待收货',
  CONFIRMED: '已收货',
  COMPLETED: '已完成',
  VOID: '已作废'
};

@Relax
export default class OrderListItem extends Component<any, any> {
  props: {
    relaxProps?: {
      orders: IList;
    };
    order: any;
    index: number;
  };

  static relaxProps = {
    orders: 'orders'
  };

  render() {
    let { order } = this.props;
    const gifts = order.gifts || [];
    // const isShowPurchase = order.buyer.id == WMkit.getUserId();
    const isShowPurchase = order.buyer.id == order.inviteeId;

    // 社交电商相关内容显示与否
    return (
      <div className="ships address-box">
        <div className="order-item order-item-border-top" style={{borderRadius:"10px"}}>
          <Link to={`/customer-order-detail/${order.id}`}>
            <div className="order-head">
              <div className="order-head-num">
                {/* <span>{order.platform == 'CUSTOMER' ? '订' : '代'}</span> */}
                <img src={require("../img/order-form.png")} alt="" className="order-form-icon"></img>
                <div className="order-storeName">
                  {order.supplier.isSelf == true ? (
                    <div className="self-sales">自营</div>
                  ) : null}
                  <Link to={`/store-main/${order.supplier.storeId}`}>
                    {order.supplier.storeName}
                  </Link>
                </div>
              </div>
              <div className="status">
                {FLOW_STATE[order.tradeState.flowState]}
              </div>
            </div>
            <div className="limit-img ship-img">
              <div className="img-content">
                {order.tradeItems
                  .concat(gifts)
                  .filter((val, index) => index < 4)
                  .map((item) => (
                    <img
                      className="img-item"
                      src={item.pic ? item.pic : noneImg}
                    />
                  ))}
              </div>
              <div className="right-context">
                <div className="total-num">
                  共{order.tradeItems.concat(gifts).length}种
                </div>
                <i className="iconfont icon-jiantou1" />
              </div>
            </div>
          </Link>
          <div className="bottom">
            <div className="price" style={{ display: 'flex' }}>
              <div style={{fontSize:"19px"}}>
                <i className="iconfont icon-qian" />
                {_.addZero(order.tradePrice.totalPrice)}
                {order.commission
                  ? order.commission != 0 && (
                      <span className="commission">
                        /预计可赚&nbsp;
                        <span style={{color:"red"}}>
                        ￥{_.addZero(order.commission)}
                        </span>
                      </span>
                    )
                  : null}
              </div>
            </div>

            {isShowPurchase && <div className="self-buy-icon">自购</div>}
          </div>
        </div>
        {/* <div className="bot-line" /> */}
        
        {/* <img src={require("../img/self-support.png")} alt=""></img> */}
      </div>
    );
  }
}
