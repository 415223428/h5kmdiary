import * as React from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';

@Relax
export default class OrderListTopItem extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeTopActive: Function;
    };
    label: string;
    active: boolean;
    tabKey: string;
  };

  static relaxProps = {
    changeTopActive: noop
  };

  render() {
    const { label, active, tabKey } = this.props;
    const { changeTopActive } = this.props.relaxProps;

    return (
      <div
        className={`layout-item  ${active ? 'cur' : null}`}
        style={{marginLeft:".1rem",width:"1.2rem"}}
        onClick={() => {
          changeTopActive(tabKey);
        }}
      >
        {label}
      </div>
    );
  }
}
