import React from 'react';
import { StoreProvider } from 'plume2';
import UserInfoItem from './component/user-info-item';
import AppStore from './store';
import { Button } from 'wmkit';
import Bottom from '../purchase-order/components/bottom';
import './css/user-info.css'

const LongButton = Button.Long;
const LongBlueButton = Button.LongBlue;
const save=require('./img/save.png')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class UserInfo extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
    this.store.initCustomersuperior();
  }

  render() {
    
    return (
      <div className="content register-content user-info-box">
        <UserInfoItem />
        <div className="register-btn user-info-btn" style={{padding:'0'}}>
          {/* <div style={{marginBottom: 12}}>
            <LongButton text="退出账号" onClick={() => { this.store.loginOut() }}/>
          </div> */}
          {/* <LongBlueButton
            text="保存"
            onClick={() => {
              this.store.saveUserInfo();
            }}
          /> */}
          <img src={save} alt="" style={{width:'100%'}}
               onClick={() => {
                this.store.saveUserInfo();
            }}/>
        </div>
      </div>
    );
  }
}
