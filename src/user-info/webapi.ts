import {Fetch} from 'wmkit'


/**
 * 会员基本信息
 * @returns
 */
export const fetchCustomerBase = () => {
  return Fetch(`/customer/customerBase`)
}

export const fetchCustomersuperior = () => {
  return Fetch(`/customer/get-my-Superior`)
}


/**
 * 修改会员基本信息
 * @returns
 */
export const updateCustomerBase = (customer) => {
  return Fetch('/customer/customerBase', {
    method: 'PUT',
    body: JSON.stringify(customer)
  })
}
