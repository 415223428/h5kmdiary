import { Store, IOptions } from 'plume2';
import * as webapi from './webapi';
import { Alert, history, FormRegexUtil, WMkit } from 'wmkit';
import { config } from 'config';

import UserInfoActor from './actor/user-info-actor';

export default class AppStore extends Store {
  bindActor() {
    return [new UserInfoActor()];
  }

  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  /**
   * 会员基本信息初始化
   * @returns {Promise<void>}
   */
  init = async () => {
    const res = await webapi.fetchCustomerBase();
    if (res.code !== config.SUCCESS_CODE) {
      Alert({ text: res.message, time: 1000 });
      return;
    }
    this.transaction(() => {
      this.dispatch('user:init', res.context);
    });
  };

  initCustomersuperior = async () =>{
    const res = await webapi.fetchCustomersuperior();
    if(res.code == 'K-000000'){
      this.dispatch('customersuperior:init',res.context)
    }
  }

  /**
   * 保存会员基本信息
   * @returns {Promise<void>}
   */
  saveUserInfo = async () => {
    const customer = this.state().get('customerInfo');
    let flag = FormRegexUtil(customer.get('customerName'), '客户名称', {
      required: true,
      minLength: 2,
      maxLength: 15
    });
    if (!flag) return;
    flag = FormRegexUtil(customer.get('contactName'), '联系人', {
      required: true,
      minLength: 2,
      maxLength: 15
    });
    if (!flag) return;
    flag = FormRegexUtil(customer.get('contactPhone'), '联系电话', {
      required: true,
      regexType: 'mobile'
    });
    if (!flag) return;
    //所在地区和详细地址不是必填，但如果填写所在地区，则详细地址不能为空，反之亦然
    if (customer.get('provinceId')) {
      if (!customer.get('customerAddress') || customer.get('customerAddress').length == 0) {
        Alert({
          text: '请填写详细地址'
        });
        return;
      } else if (
        customer.get('customerAddress').length < 5 ||
        customer.get('customerAddress').length > 60
      ) {
        Alert({
          text: '地址长度为5-60个字符！'
        });
        return;
      }
    } else {
      if (customer.get('customerAddress')&&customer.get('customerAddress').length > 0) {
        Alert({
          text: '请填写所在地区'
        });
        return;
      }
    }

    const { code, message } = await webapi.updateCustomerBase(customer);
    this.messageByResult(code, message);
  };

  /**
   * 操作结果的处理
   * @param res
   */
  messageByResult(code, message) {
    if (code === config.SUCCESS_CODE) {
      Alert({
        text: '提交成功',
        time: 1000
      });
      history.push('/user-center');
    } else {
      Alert({
        text: message,
        time: 1000
      });
    }
  }

  /**
   * 改变客户信息
   * @param key
   * @param value
   */
  onChange = (key, value) => {
    this.dispatch('user:changeValue', { key: key, value: value });
  };

  /**
   * 地址选择
   * @param value
   */
  getArea = (value: number[]) => {
    this.dispatch('user:area', value);
  };

  /**
   * 退出当前账户
   */
  loginOut = () => {
    WMkit.clearLoginCache();
    if (WMkit.isLoginOrNotOpen()) {
      history.push('/login');
    } else {
      history.push('/user-center');
    }
  };
}
