import {Action, Actor, IMap} from "plume2";
import { fromJS } from 'immutable'

export default class UserInfoActor extends Actor {
  defaultState() {
    return {
      //会员基本信息
      customerInfo: {
        customerDetailId:"",
        customerId:"",
        customerName:"",//客户名称
        contactName: "",//联系人
        contactPhone: "",//联系电话
        provinceId: "",
        cityId: "",
        areaId: "",
        customerAddress: ""
      },
      customersuperior:{}
    }
  }


  /**
   * 存储会员基本信息
   * @param state
   * @param userInfo
   * @returns {Map<string, V>}
   */
  @Action('user:init')
  init(state: IMap, userInfo) {
    return state.set('customerInfo', fromJS(userInfo))
  }

  @Action('customersuperior:init')
  customersuperior(state: IMap, value) {
    return state.set('customersuperior', value)
  }


  /**
   * 改变地址
   * @param state
   * @param area
   * @returns {any}
   */
  @Action('user:area')
  getArea(state: IMap,area:number[]){
    const [provinceId,cityId,areaId] = area
    return state.withMutations(state => {
      state.setIn(['customerInfo','provinceId'],provinceId)
        .setIn(['customerInfo','cityId'],cityId)
        .setIn(['customerInfo','areaId'],areaId)
    })
  }


  /**
   * 表单值改变
   * @param state
   * @param area
   * @returns {any}
   */
  @Action('user:changeValue')
  onChange(state: IMap,changeValue){
    return state.setIn(['customerInfo', changeValue['key']],changeValue['value'])
  }
}
