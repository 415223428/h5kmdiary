import React from 'react';
import { Relax } from 'plume2';
import '../css/user-info.css'
import {
  FormInput,
  FormText,
  Picker,
  noop,
  FormSelect,
  FormItem,
  FindArea
} from 'wmkit';

@Relax
export default class UserInfoItem extends React.Component<any, any> {
  props: {
    relaxProps?: {
      customer: any;
      onChange: Function; //改变客户信息
      getArea: Function; //改变客户地址
      customersuperior: any;
    };
  };

  static relaxProps = {
    customer: 'customerInfo',
    onChange: noop,
    getArea: noop,
    customersuperior: 'customersuperior',
  };

  render() {
    const { customer, getArea, customersuperior } = this.props.relaxProps;
    let provinceId = customer.get('provinceId');
    let cityId = customer.get('cityId');
    let areaId = customer.get('areaId');
    let area = provinceId
      ? cityId
        ? [provinceId.toString(), cityId.toString(), areaId.toString()]
        : [provinceId.toString()]
      : [];
    //拼接省市区名字
    let areaName = FindArea.addressInfo(provinceId, cityId, areaId);
    //对应显示的样式
    let textStyle = provinceId ? { color: '#333' } : {};
    console.log(customersuperior);

    return (
      <div
        className="register-box user-info-box"
        style={{ marginBottom: 142, paddingBottom: 0 }}
      >
        <FormInput
          label="名称"
          placeHolder="请输入名称"
          defaultValue={customer.get('customerName')}
          maxLength={15}
          onChange={(e) => {
            this.changeValue('customerName', e.target.value.trim());
          }}
        />
        <Picker
          extra="所在地区"
          title="选择地址"
          format={(values) => {
            return values.join('/');
          }}
          value={area}
          className="addr-picker"
          onOk={(val) => getArea(val)}
        >
          <FormSelect
            labelName="所在地区"
            placeholder="请选择所在地区"
            textStyle={textStyle}
            selected={{ key: area, value: areaName }}
          />
        </Picker>
        <FormText
          label="详细地址"
          placeholder="请填写详细地址"
          value={customer.get('customerAddress')}
          maxLength={40}
          onChange={(e) => {
            this.changeValue('customerAddress', e.target.value.trim());
          }}
        />
        <FormInput
          label="联系人"
          placeHolder="请输入联系人姓名"
          defaultValue={customer.get('contactName')}
          maxLength={15}
          onChange={(e) => {
            this.changeValue('contactName', e.target.value.trim());
          }}
        />
        <FormInput
          label="联系方式"
          placeHolder="请输入联系方式"
          defaultValue={customer.get('contactPhone')}
          maxLength={11}
          type="tel"
          onChange={(e) => {
            this.changeValue('contactPhone', e.target.value.trim());
          }}
        />

        <FormItem
          labelName="账号"
          placeholder={customer.get('customerAccount')}
          textStyle={{ color: '#999' }}
        />
        <FormItem
          labelName="业务员"
          placeholder={customer.get('employeeName')}
          textStyle={{ color: '#999' }}
        />
        <FormItem
          labelName="我的上级"
          placeholder={customersuperior?customersuperior.customerName:''}
          textStyle={{ color: '#999' }}
        />
      </div>
    );
  }

  /**
   * 改变store中对应的值input
   */
  changeValue = (key, value) => {
    const { onChange } = this.props.relaxProps;
    onChange(key, value);
  };
}
