import React from 'react';
import { StoreProvider } from 'plume2';
import { GrouponBottom } from 'wmkit';
import { GrouponSearch } from 'biz';

import AppStore from './store';
import SelectList from './component/select-list';
import SelectListTop from './component/select-list-top';

import './css/style.css';
@StoreProvider(AppStore)
export default class GroupSelection extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '拼购';
    this.store.init();
  }

  render() {
    return (
      <div>
        <div className="groupon-center">
          <GrouponSearch keyWords={this.store.state().get('keyWords')} state="rank"/>
          <SelectListTop />
          <SelectList />
          <GrouponBottom currTab={'热拼排行'} />
        </div>
      </div>
    );
  }
}
