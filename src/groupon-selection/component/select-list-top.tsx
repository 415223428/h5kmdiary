import * as React from 'react';
import { Relax } from 'plume2';
import { IList } from 'typings/globalType';
import SelectListTopItem from './select-list-top-item';

@Relax
export default class SelectListTop extends React.Component<any, any> {
  props: {
    relaxProps?: {
      chooseCateId: string;
      grouponCates: IList;
    };
  };

  static relaxProps = {
    chooseCateId: 'chooseCateId',
    grouponCates: 'grouponCates'
  };

  render() {
    const { chooseCateId, grouponCates } = this.props.relaxProps;

    return (
      <div style={{ height: '40px'}}>
        <div
          className={'layout-top'}
          style={{ position: 'fixed', top: '48px' }}
        >
          <div className="layout-content" style={{borderBottom:'1px solid #e6e6e6', width:'100%'}}>
            {grouponCates.map((o) => (
              <SelectListTopItem
                key={o.get('grouponCateId')}
                label={o.get('grouponCateName')}
                active={
                  chooseCateId
                    ? o.get('grouponCateId') === chooseCateId
                    : o.get('defaultCate') == 1
                }
                defaultCate={o.get('defaultCate')}
                tabKey={o.get('grouponCateId')}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
