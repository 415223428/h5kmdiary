import * as React from 'react';
import { Relax } from 'plume2';
import { fromJS } from 'immutable';
import { ListView, Blank } from 'wmkit';
import { div } from 'wmkit/common/util';
import { GrouponListItem } from 'biz';

@Relax
export default class SelectList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      //请求参数
      chooseCateId: string;
      keyWords: string;
      sticky: boolean;
    };
  };

  static relaxProps = {
    //请求参数
    chooseCateId: 'chooseCateId',
    keyWords: 'keyWords',
    sticky: 'sticky'
  };

  render() {
    const { chooseCateId, keyWords, sticky } = this.props.relaxProps;

    return (
      <div>
        <ListView
          style={{ height: 'auto' }}
          url="/groupon/center/list"
          params={{
            grouponCateId: chooseCateId,
            goodsName: keyWords,
            sticky: sticky
          }}
          isPagination={true}
          renderRow={(item) => {
            return (
              <GrouponListItem
                goodInfo={fromJS(item)}
                key={fromJS(item).get('goodsId')}
              />
            );
          }}
          renderEmpty={() => (
            <Blank
              img={require('../img/empty.png')}
              content="啊哦，暂时没有拼团活动~"
            />
          )}
        />
      </div>
    );
  }
}
