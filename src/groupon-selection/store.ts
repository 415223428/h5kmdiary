import { Store } from 'plume2';
import { fromJS } from 'immutable';
import { config } from 'config';
import * as webapi from './webapi';
import GrouponCenterActor from './actor/groupon-center-actor';
export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new GrouponCenterActor()];
  }

  /**
   * 初始化加载数据
   * @returns {Promise<void>}
   */
  init = async () => {
    // 查询拼团分类
    this.getGrouponCate();
  };

  /**
   * 数据变更
   * @param {any} field
   * @param {any} value
   */
  onFieldChange = ({ field, value }) => {
    this.dispatch('group: field: change', { field, value });
  };

  /**
   * 查询拼团分类
   * @returns {Promise<void>}
   */
  getGrouponCate = async () => {
    const res = (await webapi.getGrouponCateList()) as any;
    const { code, context } = res;
    if (code == config.SUCCESS_CODE) {
      let cateList = context ? context.grouponCateVOList : [];
      this.onFieldChange({
        field: 'grouponCates',
        value: fromJS(cateList)
      });
    }
  };

  /**
   * 拼团分类选中事件
   * @param key tab的key值
   */
  changeTopActive = (key: string, defaultCate: number) => {
    if (key && this.state().get('chooseCateId') === key) {
      return false;
    }
    // keyWords = ''
    this.onFieldChange({ field: 'keyWords', value: '' });
    // 精选分类下的设值
    if (defaultCate == 1) {
      // 精选 = true
      this.onFieldChange({ field: 'sticky', value: true });
      // chooseCateId = ''
      this.onFieldChange({ field: 'chooseCateId', value: '' });
    } else {
      // 精选 = false
      this.onFieldChange({ field: 'sticky', value: false });
      // chooseCateId = key
      this.onFieldChange({ field: 'chooseCateId', value: key });
    }
  };
}
