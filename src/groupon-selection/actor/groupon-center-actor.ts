import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class GrouponCenterActor extends Actor {
  defaultState() {
    return {
      // 拼团分类列表
      grouponCates: [],

      //搜索关键字
      queryString: '',

      // 选中的拼团分类ID
      chooseCateId: '',
      // 关键字搜索
      keyWords: '',
      // 是否精选分类
      sticky: true
    };
  }

  /**
   * 数据变化
   * @param state
   * @param context
   * @returns {*}
   */
  @Action('group: field: change')
  initList(state: IMap, { field, value }) {
    return state.set(field, fromJS(value));
  }
}
