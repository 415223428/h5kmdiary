import { Fetch } from 'wmkit';

/**
 * 查询活动分类
 */
export const getGrouponCateList = () => {
  return Fetch('/groupon/cate/list', {
    method: 'GET'
  });
};
