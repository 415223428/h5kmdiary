import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { WMImage, Alert, _, history } from 'wmkit';

export default class SuccessContent extends Component<any, any> {
  render() {
    const { results } = this.props;

    return (
      <div className="list-none list-height">
        <WMImage
          src={require('./img/result-suc.png')}
          width="200px"
          height="200px"
        />
        <div>
          <p>订单提交成功！</p>
          <h5>您的订单已提交审核</h5>
          <h5>您可在订单列表查看处理进度。</h5>
        </div>
        <div>
          <div
            className="bot-box"
            style={{ height: '400px', overflowY: 'auto' }}
          >
            {results.map((r, i) => {
              return (
                <div style={{ paddingBottom: 5 }}>
                  <div className="line" />
                  <div className="item">
                    订单{i + 1}编号
                    <span>{r.tid}</span>
                  </div>
                  <div className="item">
                    订单金额
                    <span>
                      <i className="iconfont icon-qian" />
                      {_.addZero(r.price)}
                    </span>
                  </div>
                </div>
              );
            })}
          </div>
          <div className="bot-line" style={{ marginBottom: '1.6rem' }} />
        </div>
        <div className="bottom-btn">
          <div className="half">
            <button
              className="btn btn-ghost"
              onClick={() => history.push('/order-list')}
            >
              查看订单
            </button>
          </div>
          <div className="half">
            <button className="btn btn-ghost" onClick={this._backToMain}>
              返回首页
            </button>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 返回首页
   * @private
   */
  _backToMain() {
    history.push('/');
  }
}
