import {Store, IOptions} from 'plume2';
import {Alert , FormRegexUtil , history} from "wmkit";

import * as webapi from './webapi';
import UserAccountEdit from './actor/user-account-edit';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }


  bindActor() {
    return [
      new UserAccountEdit
    ]
  }


  init = async (accountId) => {
    if (accountId) {
      const {code, context} = await webapi.fetchAccountBean(accountId);
      if (code == 'K-000000') {
        this.dispatch("customer:account", context);
      }
    }
  };


  /**
   * form表单，文本框输入值
   * @param key
   * @param value
   */
  setValue = (key, value) => {
    this.dispatch('accountEdit:setValue', {key: key, value: value})
  };


  /**
   * 保存按钮
   * @param accountId
   * @returns {Promise<void>}
   */
  doSaveAccount = async (accountId) => {
    const {customerBankName,customerAccountName,customerAccountNo} = this.state().get("account");
    if (!FormRegexUtil(customerBankName, "开户行", {required: true, maxLength: 50}))return;
    if (!FormRegexUtil(customerAccountName, "账户名称", {required: true, maxLength: 50}))return;
    if (!FormRegexUtil(customerAccountNo, "账号", {required: true, maxLength: 30}))return;
    let accountBean = new Object();
    accountBean['customerBankName'] = customerBankName;
    accountBean['customerAccountName'] = customerAccountName;
    accountBean['customerAccountNo'] = customerAccountNo;
    if (accountId != -1) {
      accountBean['customerAccountId'] = accountId;
      const {code, message} = await webapi.updateAccountBean(accountBean);
      if (code == "K-000000") {
        Alert({
          text: '修改成功'
        });
        history.go(-1);
      } else {
        Alert({
          text: message
        })
      }
    } else {
      const {code, message} = await webapi.insertAccountBean(accountBean);
      if (code == "K-000000") {
        Alert({
          text: '新增成功'
        });
        history.go(-1);
      } else {
        Alert({
          text: message
        })
      }
    }
  }
}
