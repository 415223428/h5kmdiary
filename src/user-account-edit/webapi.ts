import {Fetch} from 'wmkit'

/**
 * 根据accountId查询accountBean
 * @param params
 * @returns {Promise<IAsyncResult<T>>}
 */
export const fetchAccountBean = (accountId) => {
  return Fetch<Result<any>>('/customer/account/' + accountId)
};


/**
 * 新增accountBean
 * @param params
 * @returns {Promise<IAsyncResult<T>>}
 */
export const insertAccountBean = (accountBean) => {
  return Fetch<Result<any>>('/customer/account/', {
    method: 'POST',
    body: JSON.stringify(accountBean)
  })
};


/**
 * 修改accountBean
 * @param params
 * @returns {Promise<IAsyncResult<T>>}
 */
export const updateAccountBean = (accountBean) => {
  return Fetch<Result<any>>('/customer/account/', {
    method: 'PUT',
    body: JSON.stringify(accountBean)
  })
};

