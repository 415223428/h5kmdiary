/**
 * Created by hht on 2017/7/17.
 */
import {Map} from 'immutable'

import {Actor, Action, IMap} from 'plume2'

export default class UserAccountEdit extends Actor {
  defaultState() {
    return {
      account: {
        customerBankName:'',
        customerAccountName:'',
        customerAccountNo:''
      },
    }
  }

  /**
   * 账号详情
   * @param state
   * @param res
   * @returns {Map<string, V>}
   */
  @Action('customer:account')
  accountList(state: IMap, res) {
    return state.set('account', res);
  }


  /**
   * 表单组装参数
   * @param state
   * @param res
   */
  @Action('accountEdit:setValue')
  setValue(state, res) {
    let accountBean = state.get('account');
    accountBean[res['key']] = res['value'];
    return state.set('account', accountBean);
  }

}