import React from 'react'

import {StoreProvider} from 'plume2';

import FormItem from './component/form-item'
import AppStore from './store'
const save=require('./img/save.png')
@StoreProvider(AppStore, {debug: __DEV__})
export default class UserAccountEdit extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const {accountId} = this.props.match.params;
    if (accountId != -1) {
      this.store.init(accountId);
      document.title = '编辑银行账户'
    }else{
      document.title = '新增银行账户'
    }
  }


  render() {
    return (
      <div className="content register-content">
        <FormItem/>
        <div className="register-btn" style={{background:'linear-gradient(to right, #FF6A4D, #FF1A1A)',padding:'0'}}>
          {/* <button className="btn btn-primary"
                  onClick={() => this.store.doSaveAccount(this.props.match.params.accountId)}>保存
          </button> */}
          <img src={save} alt="" style={{width:'100%'}} onClick={() => this.store.doSaveAccount(this.props.match.params.accountId)}/>
        </div>
      </div>
    )
  }
}
