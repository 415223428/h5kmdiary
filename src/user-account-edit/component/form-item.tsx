import React from 'react'
import {Link} from 'react-router-dom'

import {IMap, Relax} from "plume2";
import {FormInput, noop} from "wmkit";


@Relax
export default class FormItem extends React.Component<any, any> {

  props: {
    relaxProps?: {
      accountBean: IMap
      setValue: Function
    }
  };


  static relaxProps = {
    accountBean: 'account',
    setValue: noop,
  };


  constructor(props: any) {
    super(props);
  }


  render() {
    const {accountBean} = this.props.relaxProps;
    return (
      <div className="register-box" style={{paddingBottom:'0',width:'100%'}}>
        <FormInput label="开户行" placeHolder="请输入您开户行的名称" maxLength={50} defaultValue={accountBean["customerBankName"]}
                   onChange={(e) => this.setValue('customerBankName', e.target.value)}/>
        <FormInput label="账户名称" placeHolder="请输入您的账户名称" maxLength={50} defaultValue={accountBean["customerAccountName"]}
                   onChange={(e) => this.setValue('customerAccountName', e.target.value)}/>
        <FormInput label="账号" type="number" pattern="[0-9]*" placeHolder="请输入您的账号" maxLength={30} defaultValue={accountBean["customerAccountNo"]}
                   onChange={(e) => this.setValue('customerAccountNo', e.target.value)}/>
      </div>
    )
  }


  setValue = (key, value) => {
    const {setValue} = this.props.relaxProps;
    setValue(key, value);
  }

}
