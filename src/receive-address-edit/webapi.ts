import { Fetch } from 'wmkit'
type TResult = { code: string; message: string ; context: any}


/**
 * 通过ID查询收货地址
 * @param addressId
 * @returns {Promise<Result<TResult>>}
 */
export const findAddressById = (addressId) => {
  return Fetch<TResult>(`/customer/address/${addressId}`, {
    method: 'GET'
  })
}


/**
 * 新增收货地址
 * @param address
 */
export const saveAddress = (address) => {
  return Fetch('/customer/address', {
    method: 'POST',
    body: JSON.stringify(address)
  })
}


/**
 * 修改收货地址
 * @param address
 * @returns {Promise<Result<T>>}
 */
export const editAddress = (address) => {
  return Fetch<TResult>('/customer/addressInfo', {
    method: 'PUT',
    body: JSON.stringify(address)
  })
}

