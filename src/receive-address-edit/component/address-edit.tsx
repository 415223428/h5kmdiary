import React from 'react';
import { Relax } from 'plume2';
import '../css/receive-address-edit.css'
import {
  Switch,
  FormText,
  FormInput,
  FormSelect,
  noop,
  FindArea,
  Picker
} from 'wmkit';
const select=require('../img/select.png')
const noSelect=require('../img/no-select.png')
@Relax
export default class EditFrom extends React.Component<any, any> {
  props: {
    relaxProps?: {
      address: any;
      onChange: Function; //改变收货人单条信息
      getArea: Function; //改变收货人地址
    };
  };

  static relaxProps = {
    address: 'address',
    onChange: noop,
    getArea: noop
  };

  render() {
    const { address, getArea } = this.props.relaxProps;
    let provinceId = address.get('provinceId');
    let cityId = address.get('cityId');
    let areaId = address.get('areaId');
    //拼接省市区
    let area = provinceId
      ? cityId
        ? [provinceId.toString(), cityId.toString(), areaId.toString()]
        : [provinceId.toString()]
      : null;
    //拼接省市区名字
    let areaName = FindArea.addressInfo(provinceId, cityId, areaId);
    //对应显示的样式
    let textStyle = provinceId ? { color: '#333' } : {};
    return (
      <div className="register-box re-address-box">
        <FormInput
          label="收货人"
          placeHolder="请输入收货人姓名"
          maxLength={8}
          defaultValue={address.get('consigneeName')}
          onChange={(e) => {
            this.changeValue('consigneeName', e.target.value.trim());
          }}
        />
        <FormInput
          label="手机号码"
          placeHolder="请输入手机号码"
          type="tel"
          defaultValue={address.get('consigneeNumber')}
          maxLength={11}
          onChange={(e) => {
            this.changeValue('consigneeNumber', e.target.value.trim());
          }}
        />
        <Picker
          extra="所在地区"
          title="选择地址"
          format={(values) => {
            return values.join('/');
          }}
          value={area}
          onOk={(val) => getArea(val)}
        >
          <FormSelect
            labelName="所在地区"
            placeholder="请选择所在地区"
            textStyle={textStyle}
            selected={{ key: area, value: areaName }}
          />
        </Picker>
        <FormText
          label="详细地址"
          placeholder="请填写详细地址"
          value={address.get('deliveryAddress')}
          maxLength={40}
          onChange={(e) => {
            this.changeValue('deliveryAddress', e.target.value.trim());
          }}
        />
        <div className="row form-item">
          <span className="form-text">设为默认</span>
          <Switch
            switched={address.get('isDefaltAddress') == 1}
            onSwitch={() => {
              this.changeDefault(
                'isDefaltAddress',
                address.get('isDefaltAddress')
              );
            }}
          />

        </div>
      </div>
    );
  }

  /**
   * 改变store中对应的值input
   */
  changeValue = (key, value) => {
    const { onChange } = this.props.relaxProps;
    onChange(key, value);
  };

  /**
   * 改变store中对应的值checkbox
   */
  changeDefault = (key, value) => {
    if (value === 1) {
      value = 0;
    } else {
      value = 1;
    }
    this.props.relaxProps.onChange(key, value);
  };
}
