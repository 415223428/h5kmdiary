import { Store } from "plume2";
import { Alert, FormRegexUtil, history, storage } from 'wmkit'
import { config, cache } from 'config'
import { fromJS } from 'immutable'

import * as webApi from './webapi'
import AddressEditActor from './actor/address-edit-actor'

export default class AppStore extends Store {
  bindActor() {
    return [
      new AddressEditActor
    ]
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  /**
   * 初始化地址
   * @returns {Promise<void>}
   */
  init = async (addressId) => {
    const address = await webApi.findAddressById(addressId)
    this.transaction(() => {
      this.dispatch('address: fetch', address.context)
    })
  }


  /**
   * 新增收货地址
   */
  saveAddress = async (addressId) => {
    const address = this.state().get('address')

    let flag = FormRegexUtil(address.get("consigneeName"), "收货人", { required: true, minLength: 2, maxLength: 8 })
    if (!flag)
      return
    flag = FormRegexUtil(address.get("consigneeNumber"), "手机号码", { required: true, regexType: 'mobile' })
    if (!flag)
      return
    flag = FormRegexUtil(address.get("provinceId"), "所在地区", { required: true })
    if (!flag)
      return
    flag = FormRegexUtil(address.get("deliveryAddress"), "详细地址", { required: true, minLength: 5, maxLength: 40 })
    if (!flag)
      return

    if (addressId != -1) {
      const { code, message, context } = await webApi.editAddress(address);
      this.messageByResult(code, message, context)
    } else {
      const { code, message, context } = await webApi.saveAddress(address);
      this.messageByResult(code, message, context)
    }
  }


  /**
   * 操作结果的处理
   * @param res
   */
  messageByResult(code, message, context) {
    if (code === config.SUCCESS_CODE) {
      Alert({
        text: "保存成功！"
      });
      //跳转返回初始化
      if (storage('session').get(cache.ORDER_CONFIRM)) { //选择地址页面
        let { defaultAddr, orderConfirm, pointsOrderConfirm, comeFrom } =
          JSON.parse(storage('session').get(cache.ORDER_CONFIRM))
        if (comeFrom == 'firstAddress') {
          storage('session').set(cache.ORDER_CONFIRM, {
            defaultAddr: context, orderConfirm, comeFrom: comeFrom
          })
          history.push('/order-confirm')
          return
        } else if(comeFrom == 'firstFlashSaleAddress'){
          storage('session').set(cache.ORDER_CONFIRM, {
            defaultAddr: context, orderConfirm, comeFrom: comeFrom
          })
          history.push('/flash-sale-order-confirm')
          return
        } else if (comeFrom.startsWith('firstDefaultInvoiceAddr_')) {
          const storeId = comeFrom.substr(comeFrom.indexOf('_') + 1)
          comeFrom = 'invoice'
          orderConfirm = fromJS(orderConfirm).map(m => {
            if (m.get('storeId') == storeId) {
              m = m.set('defaultInvoiceAddr', context)
            }
            return m
          }).toJS()
          storage('session').set(cache.ORDER_CONFIRM, {
            defaultAddr: context, orderConfirm, comeFrom: comeFrom
          })
          history.push({ pathname: '/order-confirm' })
          return
        } else if (comeFrom == 'firstPointsAddress') {
          storage('session').set(cache.ORDER_CONFIRM, {
            defaultAddr: context,
            pointsOrderConfirm,
            comeFrom: comeFrom
          });
          const params = {
            pointsGoodsId: pointsOrderConfirm.pointsGoodsId,
            num: pointsOrderConfirm.num
          };
          history.push({
            pathname: '/points-order-confirm',
            state: { params }
          });
          return;
        } else {
          let firstAddress = defaultAddr
          if (defaultAddr.deliveryAddressId == context.deliveryAddressId) {
            firstAddress = context
          }
          orderConfirm = orderConfirm && fromJS(orderConfirm).map(m => {
            if (m.getIn(['defaultInvoiceAddr', 'deliveryAddressId'])
              == context.deliveryAddressId) {
              m = m.set('defaultInvoiceAddr', context)
            }
            return m
          }).toJS()
          storage('session').set(cache.ORDER_CONFIRM, JSON.stringify({
            defaultAddr: firstAddress, orderConfirm, pointsOrderConfirm, comeFrom: comeFrom
          }))
        }
      }
      history.go(-1)
    } else {
      Alert({
        text: message,
        time: 1500
      });
    }
  }


  /**
   * 改变收货人单条信息
   * @param key
   * @param value
   */
  onChange = (key, value) => {
    this.dispatch('address:changeValue', { key: key, value: value })
  };


  /**
   * 地址选择
   * @param value
   */
  getArea = (value: number[]) => {
    this.dispatch('address:area', value)
  }

}