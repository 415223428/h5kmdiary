import {Action, Actor, IMap} from "plume2";
import { fromJS } from 'immutable'

export default class AddressEditActor extends Actor {
  defaultState() {
    return {
      //收货地址列表
      address: {
        consigneeName:"",
        consigneeNumber:"",
        provinceId:"",
        cityId:"",
        areaId:"",
        deliveryAddress:"",
        isDefaltAddress:0
      }
    }
  }


  /**
   * 存储会员收货地址
   * @param state
   * @param address
   * @returns {Map<string, V>}
   */
  @Action('address: fetch')
  init(state: IMap, address) {
    return state.set('address', fromJS(address))
  }


  @Action('address:area')
  getArea(state,area:number[]){
    const [provinceId,cityId,areaId] = area
    return state.setIn(['address','provinceId'],provinceId)
      .setIn(['address','cityId'],cityId)
      .setIn(['address','areaId'],areaId)
  }


  /**
   * 表单值改变
   * @param state
   * @param area
   * @returns {any}
   */
  @Action('address:changeValue')
  onChange(state,changeValue){
    return state.setIn(['address', changeValue['key']],changeValue['value'])
  }
}
