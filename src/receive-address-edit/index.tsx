import React from 'react'
import EditForm from './component/address-edit'
import {StoreProvider} from "plume2";
import AppStore from './store'
import './css/receive-address-edit.css'

const back = "<"
const save=require('./img/save.png')

@StoreProvider(AppStore, {debug: __DEV__})
export  default class AddressForm extends React.Component<any, any> {
  store: AppStore

  componentDidMount() {
    const {addressId} = this.props.match.params;
    if (addressId != "-1") {
      this.store.init(addressId);
      document.title = '编辑收货地址-康美日记'
    }else{
      document.title = '添加收货地址-康美日记'
    }
  }

  render() {
    return (
      <div>
        <EditForm/>
        <div className="register-btn re-address-btn" style={{padding:'0',lineHeight: '0.01rem'}}>
          {/* <button className="btn btn-primary" onClick={() => this._addAddress(this.props.match.params.addressId)}>保存</button> */}
          <img src={save} alt="" style={{height:'1rem'}} onClick={() => this._addAddress(this.props.match.params.addressId)}/>
        </div>
      </div>
    )
  }


  /**
   * 新增收货地址
   * @private
   */
  _addAddress = (addressId) => {
    this.store.saveAddress(addressId)
  }
}
