import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };
/**
 * 初始化发圈素材列表
 * @param id
 * @returns {Promise<Result<TResult>>}
 */
export const init = async (skuId) => {
  return Fetch<Result<any>>('/distribution/goods-matter/page', {
    method: 'POST',
    body: JSON.stringify({
      goodsInfoId: skuId
    })
  });
};
