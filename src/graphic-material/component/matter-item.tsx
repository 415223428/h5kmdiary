import React, { Component } from 'react';
import { Relax } from 'plume2';
import moment from 'moment';
import wx from 'weixin-js-sdk';
import { noop, WMImage, NormalVideo, history } from 'wmkit';
import 'video.js/dist/video-js.css';
import ReactClipboard from 'react-clipboardjs-copy';
import { Const } from 'config';

export default class MatterItem extends Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeigth: 0,
      key: this.props.matterItem.get('id')
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  componentDidMount() {
    const { key } = this.state;
    let p = document.getElementById(key);
    this.setState({
      scrollHeigth: p.scrollHeight
    });
  }

  render() {
    const { matterItem } = this.props;
    return (
      <div className="graphic">
        <div className="graphic-item">
          <img
            className="avatar"
            src="https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2656778362,978133463&fm=27&gp=0.jpg"
            alt=""
          />
          <div className="graphic-content">
            <p className="gr-title">官方精选</p>
            <div className="rowFlexBetween gr-time">
              <span>
                {matterItem.get('updateTime')
                  ? moment(matterItem.get('updateTime')).format(
                      Const.DATE_FORMAT
                    )
                  : '-'}
              </span>
              {/* <span>{matterItem.get('recommendNum')}次分享</span> */}
            </div>
            <p
              id={this.state.key}
              className={
                this.props.visible && this.state.scrollHeigth > 105
                  ? 'gr-info gr-info-more'
                  : 'gr-info'
              }
            >
              {matterItem.get('recommend')}
            </p>
            <a className="gr-allText" onClick={() => this.props.onSpread()}>
              {this.state.scrollHeigth > 105
                ? this.props.visible
                  ? '收起'
                  : '全文'
                : null}
            </a>
            <div className="pictureVideo">
              {matterItem
                .get('matter')
                .split(',')
                .map((v, index) => {
                  return (
                    <WMImage
                      key={index}
                      src={v}
                      alt=""
                      width="90px"
                      height="90px"
                    />
                  );
                })}
            </div>

            <div className="rowFlexBetween gr-save">
              {(window as any).isMiniProgram && (
                <ReactClipboard
                  text={matterItem.get('recommend')}
                  onSuccess={() => this._shareMoments(matterItem)}
                  onError={(e) => console.log(e)}
                >
                  <a href="javascript:;" className="save">
                    <i className="iconfont icon-xiazaitupian" />&nbsp;保存图片和文案
                  </a>
                </ReactClipboard>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }

  //携带图片到小程序原生页面分享
  _shareMoments = (matterItem) => {
    if ((window as any).isMiniProgram) {
      let params = {
        materialList: matterItem.get('matter'),
        token: (window as any).token,
        id: matterItem.get('id'),
        matterType: 0
      };

      wx.miniProgram.navigateTo({
        url: `../material/material?data=${JSON.stringify(params)}`
      });
    }
  };
}
