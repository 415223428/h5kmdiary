import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop, Check, WMImage } from 'wmkit';

@Relax
export default class SharePop extends Component<any, any> {
  props: {
    relaxProps?: {
      visiblePop: boolean;
      changePop: Function;
      changeMoments: Function;
    };
  };

  static relaxProps = {
    visiblePop: 'visiblePop',
    changePop: noop,
    changeMoments: noop
  };

  render() {
    const { visiblePop, changePop, changeMoments } = this.props.relaxProps;
    const showWechat = false;
    return (
      visiblePop && (
        <div className="poper">
          <div className="mask" onClick={() => changePop()} />
          <div className="poper-content share-content">
            <div className="rowFlexBetween share-title">
              <span>文案已复制，分享时请粘贴</span>
              <a href="javascript:;" onClick={() => changePop()}>
                <i className="iconfont icon-guanbi" />
              </a>
            </div>
            <ul className="share-graphic">
              {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((v, k) => (
                <li key={k}>
                  <WMImage
                    src={
                      'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2656778362,978133463&fm=27&gp=0.jpg'
                    }
                    alt=""
                    width="100%"
                    height="100%"
                  />
                  <Check
                    className="check-share"
                    checked={true}
                    onCheck={() => {}}
                  />
                </li>
              ))}
            </ul>
            <div className="text">请选择分享的图片</div>
            {showWechat ? (
              <div className="share-bottom" onClick={() => {}}>
                <img
                  className="share-icon"
                  src={require('../img/wechat.png')}
                  alt=""
                />
                <p>分享给微信好友</p>
              </div>
            ) : (
              <div className="share-bottom" onClick={() => changeMoments()}>
                <img
                  className="share-icon"
                  src={require('../img/moments.png')}
                  alt=""
                />
                <p>分享至朋友圈</p>
              </div>
            )}
          </div>
        </div>
      )
    );
  }
}
