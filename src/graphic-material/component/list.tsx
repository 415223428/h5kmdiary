import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import wx from 'weixin-js-sdk';
import { noop, WMImage, NormalVideo, history, ListView, Blank } from 'wmkit';
import { fromJS } from 'immutable';
import MatterItem from './matter-item';
import 'video.js/dist/video-js.css';

const noneImg = require('../img/list-none.png');
@Relax
export default class MatterList extends Component<any, any> {
  _listView: any;
  props: {
    goodsInfoId: string;
    relaxProps?: {
      visible: boolean;
      changeText: Function;
      changePop: Function;
      visibleMap: IMap;
    };
  };

  static relaxProps = {
    visible: 'visible',
    changeText: noop,
    changePop: noop,
    visibleMap: 'visibleMap'
  };

  render() {
    const { visibleMap } = this.props.relaxProps;
    return (
      <ListView
        url="/distribution/goods-matter/page"
        style={{ height: window.innerHeight }}
        params={{
          goodsInfoId: this.props.goodsInfoId,
          sortColumn: 'updateTime',
          sortRole: 'desc'
        }}
        dataPropsName="context.distributionGoodsMatterPage.content"
        renderRow={(matterItem: IMap) => {
          return (
            <MatterItem
              key={fromJS(matterItem).get('id')}
              visible={visibleMap.get(fromJS(matterItem).get('id'))}
              onSpread={() => this._onSpread(fromJS(matterItem))}
              matterItem={fromJS(matterItem)}
            />
          );
        }}
        renderEmpty={() => <Blank img={noneImg} content="该商品没有发圈素材" />}
        ref={(_listView) => (this._listView = _listView)}
      />
    );
  }

  //携带图片到小程序原生页面分享
  _shareMoments = () => {
    if ((window as any).isMiniProgram) {
      let params = [
        'https://wanmi-b2b.oss-cn-shanghai.aliyuncs.com/201902271516103496.jpeg',
        'https://wanmi-b2b.oss-cn-shanghai.aliyuncs.com/201902271516103496.jpeg',
        'https://wanmi-b2b.oss-cn-shanghai.aliyuncs.com/201902271516103496.jpeg',
        'https://wanmi-b2b.oss-cn-shanghai.aliyuncs.com/201902271516103496.jpeg'
      ];
      wx.miniProgram.navigateTo({
        url: `../material/material?data=${JSON.stringify(params)}`
      });
    }
  };

  _onSpread = (matterItem) => {
    const { changeText } = this.props.relaxProps;
    changeText(matterItem.get('id'));
  };
}
