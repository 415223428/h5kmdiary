import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop, Check } from 'wmkit';

@Relax
export default class MomentsSuccess extends Component<any, any> {
  props: {
    relaxProps?: {
      momentsSuccess: boolean;
      changeMoments: Function;
    };
  };

  static relaxProps = {
    momentsSuccess: 'momentsSuccess',
    changeMoments: noop
  };

  render() {
    const { momentsSuccess, changeMoments } = this.props.relaxProps;
    return (
      momentsSuccess && (
        <div className="poper">
          <div className="mask" onClick={() => changeMoments()} />
          <div className="poper-confirm share-content">
            <div className="rowFlexBetween share-title">
              <span />
              <a href="javascript:;" onClick={() => changeMoments()}>
                <i className="iconfont icon-guanbi" />
              </a>
            </div>
            <div className="copyText">
              <Check checked={true} onCheck={() => {}} />
              <span>素材文案已复制</span>
            </div>
            <div className="copyText">
              <Check checked={true} onCheck={() => {}} />
              <span>图片已保存至相册</span>
            </div>
            <div className="copyText">
              <Check checked={true} onCheck={() => {}} />
              <span>快去微信发送朋友圈吧！</span>
            </div>
            <div className="moment-btn">立即分享</div>
          </div>
        </div>
      )
    );
  }
}
