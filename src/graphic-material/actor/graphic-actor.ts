import { Actor, Action } from 'plume2';
import { fromJS } from 'immutable';

export default class GraphicActor extends Actor {
  defaultState() {
    return {
      visible: false,
      visiblePop: false, //分享图文信息Pop
      momentsSuccess: false, //朋友圈分享成功
      //每一个item推荐文字的展开隐藏
      visibleMap: fromJS({})
    };
  }

  @Action('change:textVisible')
  changeCateVisible(state, id) {
    //获取原来的状态值
    const oldState = state.get('visibleMap').get(id);
    return state.setIn(['visibleMap', id], !oldState);
  }

  @Action('change: popVisible')
  changePopVisible(state) {
    return state.set('visiblePop', !state.get('visiblePop'));
  }

  @Action('change: momentsVisible')
  changeMomentsVisible(state) {
    return state
      .set('visiblePop', false)
      .set('momentsSuccess', !state.get('momentsSuccess'));
  }
}
