import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import MatterList from './component/list';
import SharePop from './component/share-pop';
import MomentsSuccess from './component/moments-success';
/**
 * 图文素材
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class GraphicMaterial extends Component<any, any> {
  store: AppStore;

  render() {
    const { mid } = this.props.match.params;
    return (
      <div>
        <MatterList goodsInfoId={mid} />
      </div>
    );
  }
}
