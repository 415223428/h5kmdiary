import { Store } from 'plume2';
import GraphicActor from './actor/graphic-actor';
import * as webapi from './webapi';
import { config } from '../../web_modules/config/config-test3';
export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new GraphicActor()];
  }

  // init = async (skuId) => {
  //   const { code, message, context } = await webapi.init(skuId);
  //   // if (code == config.SUCCESS_CODE) {
  //   //   this.dispatch
  //   // }
  // };

  /**
   * 全文or收起
   */
  changeText = (id) => {
    this.dispatch('change:textVisible', id);
  };

  /**
   * 分享图文信息Pop
   */
  changePop = () => {
    this.dispatch('change: popVisible');
  };

  /**
   * 朋友圈分享成功
   */
  changeMoments = () => {
    this.dispatch('change: momentsVisible');
  };
}
