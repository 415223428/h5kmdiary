import React, { Component } from 'react';

import { WMImage, history } from 'wmkit';

export default class GoodsEmpty extends Component<any, any> {
  render() {
    const pointsGoodsId = this.props.location.pointsGoodsId;
    return (
      <div className="list-none">
        <WMImage
          src={require('./imgages/goodsnone.png')}
          width="5rem"
          height="4rem"
        />
        <p>该商品不存在，或是已经失效了哦</p>
        <div className="half">
          <button
            className="btn btn-ghost"
            onClick={() =>
              pointsGoodsId
                ? history.replace('/points-mall')
                : history.replace('/')
            }
            style={{borderRadius:".4rem"}}
          >
            去首页
          </button>
        </div>
      </div>
    );
  }
}
