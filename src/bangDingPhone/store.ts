import { Store } from 'plume2';
import parse from 'url-parse';

import { Alert, history, WMkit, storage, _, wxAuth } from 'wmkit';
import { cache, config } from 'config';
import bangDingPhoneActor from './actor/bangDingPhone-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new bangDingPhoneActor()];
  }

  constructor(props) {
    super(props);
    (window as any)._store = this;
  }

  init = async () => {
    // this.dispatch('bangDingPhone:init');
    // this.fetchLogo()
  };

  setPhone = (phone) => {
    this.dispatch('bangDingPhone:phone', phone);
  };
  setCode = (code) => {
    this.dispatch('bangDingPhone:code', code);
  };

  // 提交绑定的编号
  submitPhone = async () => {
    if (this.state().get('phone') === "" || this.state().get('code') === "") {
      Alert({
        text: "手机号或者验证码不能为空"
      });
    } else {
      console.log("%c***********************", "color:blue;font-size:50px");
      console.log("%c" + this.state().get('phone'), "color:blue;font-size:50px");
      console.log("%c" + this.state().get('code'), "color:blue;font-size:50px");
      console.log("%c" + sessionStorage.getItem("customerId"), "color:blue;font-size:50px");
      if (sessionStorage.getItem("customerId") == null) {
        console.log("%csessionStorage找不到customerId", "color:red;font-size:50px");
        return
      }
      // 校验验证码
      if (WMkit.testVerificationCode(this.state().get('code'))) {
        const res1 = await webapi.VerificationCode(
          this.state().get('phone'),
          this.state().get('code')
        );
        // 验证码成功
        console.log(res1);

        if (res1.code == "K-000000") {
        // 绑定
        const res = await webapi.confirmBound({
          customerAccount: this.state().get('phone'),
          customerId: sessionStorage.getItem("customerId"),
          // code:this.state().get('code'),
        });
        console.log(res);
        console.log("%c**********************", "color:blue;font-size:50px");
        if (res.code == "K-000000") {
          Alert({
            text: res.context
          });
          history.push('/login');
          // 产品要直接登陆
          // let result = null;
          // // if (WMkit.testTel(account) && WMkit.testPass(password)) {
          // if ( WMkit.testPass(password)) {
          //   const base64 = new WMkit.Base64();
          //   const res = await webapi.login(
          //     base64.urlEncode(account),
          //     base64.urlEncode(password)
          //   );
          //   result = res;
          // }
          // const { context } = result;
          // /* 登录成功时*/
          // if ((result as any).code == config.SUCCESS_CODE) {
          //   //获取审核状态
          //   _.switchLogin(context);
          // }
        } else {
          Alert({
            text: res.message
          });
        }
        }else{
          Alert({
            text: res1.message
          });
        }
      }
    }
  };

  /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/
  /** ** ** ** ** ** ** ** ** ** ** ** ** * 验证码登录 * ** ** ** ** ** ** ** ** ** ** ** ** **/
  /** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

  /**
   * 发送验证码给手机号码
   * @returns {Promise<Result<ReturnResult>>}
   */
  sendCode = (mobile) => {
    return webapi.sendCode(mobile).then((res) => {
      if (res.code === config.SUCCESS_CODE) {
        Alert({
          text: '验证码已发送，请注意查收！',
          time: 1000
        });
      } else {
        Alert({
          text: res.message,
          time: 1000
        });
        return Promise.reject(res.message);
      }
    });
  };
}


