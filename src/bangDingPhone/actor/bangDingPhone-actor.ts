import { Actor, Action, IMap } from 'plume2';

import { fromJS, Map } from 'immutable';
export default class bangDingPhoneActor extends Actor {
  defaultState() {
    return {
      phone:'',
      code:'',
    };
  }

  /**
   * 页面初始化
   * @param state
   */
  @Action('bangDingPhone:init')
  init(state) {
    // return state
    //   .set('fullBannerImages', state.get('fullBannerImages'))
      // .set('time', state.get('time'))
      // .set('fullBannerUrl', state.get('fullBannerUrl'))
      // .set('fullBannerSwitchFlag', state.get('fullBannerSwitchFlag'))
  }

  /**
   * 监听phone状态值
   * @param state
   * @param pass
   */
  @Action('bangDingPhone:phone')
  changePsw(state, phone: string) {
    return state.set('phone', phone);
  }
  /**
   * 监听code状态值
   * @param state
   * @param pass
   */
  @Action('bangDingPhone:code')
  setCode(state, code: string) {
    return state.set('code', code);
  }
}
