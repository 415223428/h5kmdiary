import React from 'react';
import { Link } from 'react-router-dom';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const styles = require('./css/style.css');
import { Check, WMkit,Button, history } from 'wmkit';
const confirmPng = require('./img/confirm.png');
const huoqucode = require('./img/huoqucode.png');
const TimerButton = Button.Timer;

@StoreProvider(AppStore, { debug: __DEV__ })
export default class bangDingPhone extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '绑定手机号-康美日记';
    this.store.init();
  }

  constructor(props: any) {
    super(props);
  }

  render() {
    const pcLogo = this.store.state().toJS().fullBannerImages;
    const phone = this.store.state().get('phone')
    // console.log("%c" + showModalConfirm, "color:red;font-size:50px");
    return (
      <div>
        {
          <div className="bangDingPhone">
            <div className="myHeader">
              绑定手机号
            </div>
            <div className="form">
              <div className="myInput">
                <input
                  type="tel"
                  pattern="[0-9]*"
                  maxLength={11}
                  placeholder="输入手机号码"
                  onChange={
                    (e) => this.store.setPhone(e.target.value)
                  }
                  value={phone}
                />
              </div>
              <div className="myInput">
                <input
                type="text"
                onChange={
                  (e) => this.store.setCode(e.target.value)
                }
                placeholder="请输入验证码"
                style={{
                  width: "calc(100% - 2rem)",
                }} />
                {/* <span>
                  <img src={huoqucode} alt="" onClick={() => {
                    this.store.sendCode(phone)
                  }} />
                </span> */}
                <TimerButton
              text="获取验证码"
              resetWhenError={true}
              shouldStartCountDown={() => this._beforeSendCode(phone)}
              onClick={() => this.store.sendCode(phone)}
              defaultStyle={{
                fontSize: '0.26rem',
                padding: '0.11rem',
                fontFamily: 'PingFang SC',
                fontWeight: '500',
                color: 'rgba(255,77,77,1)',
                border: '1px solid rgba(255, 77, 77, 1)',
                borderRadius: '4px',
                margin:'auto 0 auto auto',
              }}
            />
              </div>
            </div>
            <div className="myFooterBtn">
              <img src={confirmPng} alt="" onClick={() => {
                this.store.submitPhone()
              }} />
            </div>
          </div>
        }
      </div>
    );
  }
   /**
   * 发送验证码前校验手机号是否填写或正确
   * @returns {boolean}
   * @private
   */
  _beforeSendCode = (account) => {
    return WMkit.testTel(account);
  };
}
