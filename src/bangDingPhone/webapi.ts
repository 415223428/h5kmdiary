import { Fetch } from 'wmkit';
type TResult = { code: string; message: string; context: any };

/**
 * 验证码登录  发送验证码
 * @type {Promise<AsyncResult<T>>}
 */
export const sendCode = (mobile) => {
  return Fetch(`/authen/verification/${mobile}`, {
    method: 'POST'
  });
};



/**
 * confirmBound
 * @type {Promise<AsyncResult<T>>}
 */
export const confirmBound = (res) => {
  return Fetch(`/authenKMcustomerid`, {
    method: 'POST',
    body: JSON.stringify(res)
  });
};


/**
 * 验证 验证码
 * @param account
 * @param verificationCode
 * @returns {Promise<Result<T>>}
 */
export const VerificationCode = (
  account: string,
  verificationCode: string
) => {
  return Fetch('/verificationCode', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: account,
      verificationCode: verificationCode
    })
  });
};



/**
 * 登录系统
 * @param   account,passwordFetch
 * @returns
 */
export const login = (account: string, pass: string) => {
  console.log("%c" + account, "color:red;font-size:50px");
  console.log("%c" + pass, "color:red;font-size:50px");
  return Fetch('/login', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: account,
      customerPassword: pass
    })
  });
};
