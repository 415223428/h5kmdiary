/**
 * Created by chenpeng on 2017/7/4.
 */
import { Store } from 'plume2';

import moment from 'moment';
import { cache, Const } from 'config';
import {
  Confirm,
  FindArea,
  Alert,
  history,
  storage,
  FormRegexUtil
} from 'wmkit';

import PointsOrderConfirmActor from './actor/points-order-confirm-actor';
import * as webApi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new PointsOrderConfirmActor()];
  }

  constructor(props) {
    super(props);
    //debug
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  /**
   * 确认订单初始化
   * @returns {Promise<void>}
   */
  confirmInit = async (params) => {
    //初始化商品信息
    const res = (await webApi.getExchangeItem(params)) as any;
    if (res.code == 'K-000000') {
      this.dispatch(
        'points-order-confirm: store: fetch',
        res.context.pointsTradeConfirmItem
      );
      this.dispatch(
        'points-order-confirm: price: fetch',
        res.context.totalPoints
      );
    } else {
      this.clearSessionStorage();
      Confirm({
        text: res.message,
        okBtn: '确定',
        confirmCb: () => history.push('/points-mall')
      });
    }
    //跳转返回初始化
    if (storage('session').get(cache.ORDER_CONFIRM)) {
      //选择地址页面
      const { defaultAddr, pointsOrderConfirm } = JSON.parse(
        storage('session').get(cache.ORDER_CONFIRM)
      );
      this.dispatch('points-order-confirm: back: init', {
        pointsOrderConfirm,
        defaultAddr
      });
      this.clearSessionStorage();
    } else {
      this.dispatch('points-order-confirm: init', params);
      this.initDefaultAddress();
    }
  };

  /**
   * 清除SessionStorage
   */
  clearSessionStorage = () => {
    storage('session').del(cache.ORDER_CONFIRM);
  };

  /**
   * 存储SessionStorage
   * @param comeFrom 来自哪里
   */
  saveSessionStorage = (comeFrom) => {
    const { defaultAddr, pointsOrderConfirm } = this.state().toJS();
    storage('session').set(cache.ORDER_CONFIRM, {
      defaultAddr,
      pointsOrderConfirm,
      comeFrom
    });
  };

  /**
   * 保存买家备注
   * @param remark
   */
  saveBuyerRemark = (remark) => {
    this.dispatch('points-order-confirm: remark', remark);
  };

  /**
   * 初始化收货地址
   * @returns {Promise<void>}
   */
  initDefaultAddress = async () => {
    const addrRes = await webApi.fetchCustomerDefaultAddr();
    this.dispatch('points-order-confirm: addr: fetch', addrRes.context);
  };

  /**
   * 提交订单
   * @returns {Promise<void>}
   */
  submit = async () => {
    //校验输入密码
    let payPassword = this.state().get('payPwd');
    if (payPassword == '') {
      Alert({
        text: '请输入支付密码！'
      });
      return;
    }
    let checkPayPwd = await this.checkCustomerPayPwd(payPassword);
    if (!checkPayPwd) {
      this.dispatch('points-order-confirm:checkPayPwdRes', false);
      await this.getLoginCustomerInfo();
      return;
    } else {
      this.dispatch('points-order-confirm:checkPayPwdRes', true);
    }
    const { defaultAddr, pointsOrderConfirm } = this.state().toJS();
    if (!defaultAddr || !defaultAddr.deliveryAddressId) {
      Alert({ text: '请选择收货地址!' });
      return;
    }
    const addrDetail =
      FindArea.addressInfo(
        defaultAddr ? defaultAddr.provinceId : '',
        defaultAddr ? defaultAddr.cityId : '',
        defaultAddr ? defaultAddr.areaId : ''
      ) + (defaultAddr ? defaultAddr.deliveryAddress : '');

    if (
      !FormRegexUtil(pointsOrderConfirm.buyerRemark.trim(), '订单备注', {
        minLength: 0
      })
    ) {
      return;
    }

    const params = {
      consigneeId: defaultAddr.deliveryAddressId,
      consigneeAddress: addrDetail,
      consigneeUpdateTime: defaultAddr.updateTime
        ? moment(defaultAddr.updateTime).format(Const.SECONDS_FORMAT)
        : null,
      buyerRemark: pointsOrderConfirm.buyerRemark, //订单备注
      pointsGoodsId: pointsOrderConfirm.pointsGoodsId, //积分商品Id
      num: pointsOrderConfirm.num //购买数量
    };

    const { code, message, context } = await webApi.commit(params);
    if (code == 'K-000000') {
      //下单成功,清除
      this.clearSessionStorage();
      history.replace({
        pathname: '/points-order-confirm-success',
        state: { result: context }
      });
    } else if(code == 'K-010208'){
      Confirm({
        text: '当前积分不足',
        okBtn: '确定',
        confirmCb: () => history.push('/points-mall')
      });
    } else {
      Confirm({
        text: message,
        okBtn: '确定',
        confirmCb: () => history.push('/points-mall')
      });
    }
  };

  checkPassword = async () => {
    const { defaultAddr } = this.state().toJS();
    if (!defaultAddr || !defaultAddr.deliveryAddressId) {
      Alert({ text: '请选择收货地址!' });
      return;
    }
    //用户支付密码是否可用
    const result = await this.isPayPwdValid();
    if (result) {
      this.showPassword(true);
    }
  };

  /**
   * 支付密码弹框显示
   */
  showPassword = (val) => {
    this.dispatch('points-order-confirm:showPassword', val);
  };

  /**
   * 输入密码修改
   * @param payPwd
   */
  changePayPwd = async (payPwd) => {
    this.dispatch('points-order-confirm:changePayPwd', payPwd);
  };

  /**
   * 校验会员支付密码是否可用
   * @param payPassword
   */
  isPayPwdValid = async () => {
    const res = await webApi.isPayPwdValid();
    if (res.code == 'K-000000') {
      return true;
    } else if (res.code == 'K-010206') {
      Confirm({
        text: '您还没有设置支付密码，暂时无法使用积分支付',
        okBtn: '设置支付密码',
        confirmCb: () => history.push('/balance-pay-password')
      });
      return false;
    } else if (res.code == 'K-010207'){
      Alert({
        text: res.message,
        time: 1000
      });
      return false;
    }
  };

  /**
   * 校验输入支付密码
   * @param payPassword
   */
  checkCustomerPayPwd = async (payPassword) => {
    const res = await webApi.checkCustomerPayPwd(payPassword);
    if (res.code == 'K-000000') {
      return true;
    } else {
      Alert({
        text: res.message,
        time: 1000
      });
      return false;
    }
  };

  /**
   * 获取当前登陆人信息
   */
  getLoginCustomerInfo = async () => {
    const res = (await webApi.getLoginCustomerInfo()) as any;
    if (res.code == 'K-000000') {
      this.dispatch(
        'points-order-confirm:countPayPwdTime',
        res.context.payErrorTime
      );
    }
  };
}
