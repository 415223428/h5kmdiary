import React, { Component } from 'react';
import * as _ from "wmkit/common/util";

export default class AmountStatistics extends Component<any, any> {
  props: {
    store: any;
  };

  render() {
    const { tradeItem } = this.props.store;

    return (
      <div className="total-price mb10">
        <div className="total-list">
          <span>订单金额</span>
          <span className="price-color">
            {_.mul(tradeItem.points,tradeItem.num)}积分
          </span>
        </div>

        <div className="total-list">
          <span>商品金额</span>
          <span>
            {_.mul(tradeItem.points,tradeItem.num)}积分
          </span>
        </div>

        <div className="total-list">
          <span>配送费用</span>
          <span>
            <i className="iconfont icon-qian" />
            0.00
          </span>
        </div>
      </div>
    );
  }
}
