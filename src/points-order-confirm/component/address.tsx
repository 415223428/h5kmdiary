import React, { Component } from 'react';

import { Relax, IMap } from 'plume2';
import { FindArea, history, noop } from 'wmkit';
import { Link } from 'react-router-dom';

@Relax
export default class Address extends Component<any, any> {
  props: {
    relaxProps?: {
      defaultAddr: IMap; //默认地址
      saveSessionStorage: Function;
    };
  };

  static relaxProps = {
    defaultAddr: 'defaultAddr',
    saveSessionStorage: noop
  };

  render() {
    const { defaultAddr } = this.props.relaxProps;
    const addressDatail =
      FindArea.addressInfo(
        defaultAddr ? defaultAddr.get('provinceId') : '',
        defaultAddr ? defaultAddr.get('cityId') : '',
        defaultAddr ? defaultAddr.get('areaId') : ''
      ) + (defaultAddr ? defaultAddr.get('deliveryAddress') : '');

    return (
      <div className="mb10">
        {defaultAddr && defaultAddr.get('deliveryAddressId') ? (
          <div className="address-info" onClick={() => this._goToNext()}>
            <i className="iconfont icon-dz posit" />
            <div className="address-content">
              <div className="address-detail">
                <div className="name">
                  <span>收货人：{defaultAddr.get('consigneeName')}</span>
                  <span>{defaultAddr.get('consigneeNumber')}</span>
                </div>
                <p>收货地址：{addressDatail}</p>
              </div>
              <i className="iconfont icon-jiantou1" />
            </div>
          </div>
        ) : (
          <div className="add-address" onClick={() => this._newFirst()}>
            <i className="iconfont icon-add11" />
            <span>您的收货地址为空，点击新增收货地址</span>
          </div>
        )}
        <div className="seperate_line" />
      </div>
    );
  }

  /**
   * 下一步
   * @private
   */
  _goToNext = () => {
    this.props.relaxProps.saveSessionStorage('pointsAddress');
    history.push({ pathname: '/receive-address' });
  };

  /**
   * 新增第一个地址
   * @private
   */
  _newFirst = () => {
    this.props.relaxProps.saveSessionStorage('firstPointsAddress');
    history.push('/receive-address-edit/-1');
  };
}
