import React, { Component } from 'react';
import SkuList from './sku-list';
import Invoice from './invoice';
import AmountStatistics from './amount-statistics';
import Remark from './remark';
import { IMap } from 'typings/globalType';
import { Relax } from 'plume2';

@Relax
export default class StoreItem extends Component<any, any> {
  props: {
    relaxProps?: {
      store: IMap;
    };
  };

  static relaxProps = {
    store: 'store'
  };

  render() {
    const { store } = this.props.relaxProps;
    const storeInfo = store.toJS();
    return (
      <div>
        <SkuList store={storeInfo} />
        <Invoice />
        <Remark />
        <AmountStatistics store={storeInfo} />
      </div>
    );
  }
}
