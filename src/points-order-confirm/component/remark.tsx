import React, { Component } from 'react';
import { Relax } from 'plume2';

import { FormText, noop } from 'wmkit';
import { IMap } from 'typings/globalType';

@Relax
export default class Remark extends Component<any, any> {
  props: {
    relaxProps?: {
      pointsOrderConfirm: IMap; // 积分订单提交项目
      saveBuyerRemark: Function; //保存买家备注
    };
  };

  static relaxProps = {
    pointsOrderConfirm: 'pointsOrderConfirm',
    saveBuyerRemark: noop
  };

  render() {
    const { pointsOrderConfirm, saveBuyerRemark } = this.props.relaxProps;
    const buyerRemark = pointsOrderConfirm
      ? pointsOrderConfirm.get('buyerRemark')
      : '';
    return (
      <div>
        <FormText
          label={'订单备注'}
          placeholder="点此输入订单备注（100字以内）"
          value={buyerRemark}
          onChange={(e) => saveBuyerRemark(e.target.value)}
          maxLength={100}
        />
      </div>
    );
  }
}
