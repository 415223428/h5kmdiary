import React, { Component } from 'react';

import { Relax } from 'plume2';
import { noop, _ } from 'wmkit';

@Relax
export default class ToolBar extends Component<any, any> {
  props: {
    relaxProps?: {
      checkPassword: Function;
      totalPoints: number;
    };
  };

  static relaxProps = {
    checkPassword: noop,
    totalPoints: 'totalPoints'
  };

  render() {
    const { totalPoints } = this.props.relaxProps;
    return (
      <div className="bottom-fixed">
        <div className="order-bottom">
          <p>
            订单金额：<span>{totalPoints}积分</span>
          </p>
          <button onClick={() => this._submit()}>提交订单</button>
        </div>
      </div>
    );
  }

  /**
   * 提交订单
   * @private
   */
  _submit = () => {
    const { checkPassword } = this.props.relaxProps;
    checkPassword();
  };
}
