import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { history, noop } from 'wmkit';
const styles = require('../css/style.css');
const noneImg = require('../img/imgnone.png');

@Relax
export default class SkuList extends Component<any, any> {
  props: {
    store: IMap;
    relaxProps?: {
      saveSessionStorage: Function;
    };
  };

  static relaxProps = {
    saveSessionStorage: noop
  };

  render() {
    const { store } = this.props;
    const { supplier, tradeItem } = store as any;

    return (
      <div>
        <div className="sku-head">
          <span>订单{1}： </span>
          {supplier.isSelf == true && <div className="self-sales">自营</div>}
          <span> {supplier.storeName}</span>
        </div>
        <div className="limit-img" onClick={() => this._goToNext()}>
          <div className="img-content">
            <img
              src={tradeItem.pic ? tradeItem.pic : noneImg}
              className="img-item"
            />
          </div>
          <div className="right-context">
            <div className="total-num">
              <div>1</div>
            </div>
            <i className="iconfont icon-jiantou1" />
          </div>
        </div>
        <div className="pay-delivery">
          <span>支付配送</span>
          <div className="ways-item">
            <div>
              <p>在线支付</p>
              <p>快递配送</p>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 查看商品清单
   * @private
   */
  _goToNext = () => {
    const { tradeItem } = this.props.store as any;
    this.props.relaxProps.saveSessionStorage('sku-list');
    history.push({
      pathname:'/points-order-sku-list',
      state: {
        params:{
          pointsGoodsId: tradeItem.pointsGoodsId,
          num: tradeItem.num
        }
      }
    });
  };
}
