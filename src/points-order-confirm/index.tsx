import React, { Component } from 'react';
import { StoreProvider } from 'plume2';

import AppStore from './store';
import Address from './component/address';
import ToolBar from './component/tool-bar';
import StoreItem from './component/store-item';
import PaymentPassword from './component/payment-password';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class PointsOrderConfirm extends Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
    this._handleScroll = this._handleScroll.bind(this);
  }

  componentWillMount() {
    window.addEventListener('scroll', this._handleScroll);
    const params =
      this.props.location.state && this.props.location.state.params;
    this.store.confirmInit(params);
  }

  componentDidUpdate() {
    if ((window as any).y || (window as any).y == 0) {
      window.scrollTo(0, (window as any).y);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this._handleScroll);
  }

  render() {
    return (
      <div style={styles.pointsOrderConfirm} onScroll={this._handleScroll}>
        <Address />
        {this.store
          .state()
          .get('store')
          .get('supplier') && <StoreItem />}
        <ToolBar />
        {this.store.state().get('visible') && <PaymentPassword />}
      </div>
    );
  }

  _handleScroll = (e) => {
    (window as any).y = window.scrollY;
  };
}

const styles = {
  pointsOrderConfirm: {
    fontSize: '.28rem',
    background: '#fafafa'
  } as any
};
