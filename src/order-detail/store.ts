import { Store, IOptions } from 'plume2';

import { Alert, Confirm, history, _, WMkit } from 'wmkit';
import { config, cache } from 'config';

import DetailActor from './actor/detail-actor';
import {
  cancelOrder,
  fetchOrderDetail,
  fetchOrderReturnList,
  fetchTradeSkus,
  getTradeDetail,
  defaultPaylOrder
} from './webapi';
import * as webapi from '../order-list/webapi';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new DetailActor()];
  }

  /**
   * 初始化订单详情
   */
  init = async (tid) => {
    const res = await fetchOrderDetail(tid);
    if (res.code == config.SUCCESS_CODE) {
      this.dispatch('detail-actor:init', res.context);
    } else if (res.code == 'K-050100') {
      Alert({
        text: '订单不存在',
        time: 1000,
        cb: () => history.push('/order-list')
      });
    }

    // 有邀请人ID并且是店铺内购买的订单
    if (WMkit.inviteeId() && WMkit.isShop()) {
      this.queryShopInfo(WMkit.inviteeId());
    }
  };

  /**
   * 查询店铺-小店名称
   * @param distributorId
   * @returns {Promise<void>}
   */
  queryShopInfo = async (distributorId) => {
    const propsRes = (await webapi.queryShopInfo(distributorId)) as any;
    if (propsRes.code == config.SUCCESS_CODE) {
      this.dispatch('detail-actor:shopName', propsRes.context);
    }
  };

  /**
   * 确认取消订单
   */
  cancelConfirm = async (tid) => {
    const res = await cancelOrder(tid);
    if (res.code == config.SUCCESS_CODE) {
      Alert({ text: '取消成功', cb: () => location.reload() });
    } else {
      Alert({ text: res.message });
    }
  };

  /**
   * 取消订单
   * @param tid
   */
  _cancelOrder = (tid) => {
    Confirm({
      title: '取消订单',
      text: '您确定要取消该订单?',
      cancelBtn: '取消',
      okBtn: '确定',
      confirmCb: () => this.cancelConfirm(tid)
    });
  };

  /**
   * 申请退单
   */
  applyRefund = async (tid) => {
    let { message, context } = await getTradeDetail(tid);

    let tradeDetail = context;

    let errMsg;
    let canApply = false;

    if (tradeDetail) {
      const flowState = tradeDetail['tradeState']
        ? tradeDetail['tradeState']['flowState']
        : '';
      const payState = tradeDetail['tradeState']
        ? tradeDetail['tradeState']['payState']
        : '';
      const deliverStatus = tradeDetail['tradeState']
        ? tradeDetail['tradeState']['deliverStatus']
        : '';

      // 获取该订单所有的待处理及已完成的退单列表
      let orderReturnListRes = await fetchOrderReturnList(tid);

      if (orderReturnListRes.context) {
        canApply = true;

        // 如果有未处理完的，则不允许再次申请
        (orderReturnListRes.context as any).forEach((v) => {
          if (
            v.returnFlowState != 'REFUNDED' &&
            v.returnFlowState != 'COMPLETED' &&
            v.returnFlowState != 'REJECT_REFUND' &&
            v.returnFlowState != 'REJECT_RECEIVE' &&
            v.returnFlowState != 'VOID'
          ) {
            // 有未处理完的
            canApply = false;
            errMsg = '该订单关联了处理中的退单，不可再次申请';
          }
        });

        // 没有待处理的申请
        if (canApply) {
          // 退款申请，如果有已完成的则不允许再次申请
          if (
            flowState == 'AUDIT' &&
            payState == 'PAID' &&
            deliverStatus == 'NOT_YET_SHIPPED'
          ) {
            (orderReturnListRes.context as any).forEach((v) => {
              // 已完成申请的
              if (v.returnFlowState == 'COMPLETED') {
                canApply = false;
                errMsg = '无可退商品';
              }
            });
          } else {
            if (
              tradeDetail['tradeItems'] &&
              tradeDetail['tradeItems'].filter((v) => v.canReturnNum > 0)
                .length == 0
            ) {
              // 退货申请，如果没有可退商品则不允许申请
              canApply = false;
              errMsg = '无可退商品';
            } else if (tradeDetail['payInfo']['payTypeId'] == '0') {
              // 在线支付需判断退款金额
              let totalApplyPrice = 0;
              (orderReturnListRes.context as any).forEach((v) => {
                // 计算已完成的申请单退款总额
                if (v.returnFlowState == 'COMPLETED') {
                  totalApplyPrice = _.add(
                    totalApplyPrice,
                    v.returnPrice.applyStatus
                      ? v.returnPrice.applyPrice
                      : v.returnPrice.totalPrice
                  );
                }
              });

              if (
                totalApplyPrice >= tradeDetail['tradePrice']['totalPrice'] &&
                tradeDetail['tradePrice']['totalPrice'] !== 0
              ) {
                canApply = false;
                errMsg = '无可退金额';
              }
            }
          }
        }
      } else {
        errMsg = '系统异常';
      }
    } else {
      errMsg = message;
    }

    //  可以申请，进入申请页面，否则提示错误信息
    if (canApply) {
      // 已完结订单，则为退货申请，否则认为是退款申请
      let isReturn =
        tradeDetail['tradeState'].flowState == 'COMPLETED' ? true : false;

      // 退货，则进入退货申请页面，否则进入退款申请页面
      if (isReturn) {
        history.push(`/returnsFirst/${tid}`);
      } else {
        history.push(`/applyRefund/${tid}`);
      }
    } else {
      Alert({ text: errMsg });
    }
  };

  toDelivery = () => {
    history.push('/ship-record');
  };

  /**
   * 获取商品清单
   */
  getTradeSkus = async (tid: string) => {
    const res = (await fetchOrderDetail(tid)) as any;
    if (res.code == config.SUCCESS_CODE) {
      this.dispatch('detail-actor:setSkus', res.context.tradeItems);
      this.dispatch('detail-actor:isSelf', res.context.supplier.isSelf);
      this.dispatch('detail-actor:setGifts', res.context.gifts);
      let customerId =
        JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '';
      if (res.context.inviteeId == customerId) {
        // 订单返利给自己时，为自购
        this.dispatch('detail-actor:selfBuy', true);
      }
    }
  };

  /**
   * 0元支付
   */
  defaultPay = async (tid: string) => {
    const res = await defaultPaylOrder(tid);
    if (res.code == 'K-000000') {
      //跳转到付款成功页
      history.push({
        pathname: `/fill-payment-success/${tid}`
      });
    } else {
      Alert({ text: res.message });
    }
  };
}
