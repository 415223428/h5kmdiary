/**
 * Created by chenpeng on 2017/7/25.
 */
import { Fetch } from 'wmkit';

type TResult = { code: string; message: string; context: any };
/**
 * 订单详情
 */
export const fetchOrderDetail = (tid: string) => {
  return Fetch(`/trade/${tid}`);
};

/**
 * 取消订单
 */
export const cancelOrder = (tid: string) => {
  return Fetch<Result<any>>(`/trade/cancel/${tid}`);
};

/**
 * 获取订单详情
 */
export const getTradeDetail = (tid: string) => {
  return Fetch(`/return/trade/${tid}`);
};

/**
 * 查询退单列表
 */
export const fetchOrderReturnList = (tid) => {
  return Fetch(`/return/findByTid/${tid}`);
};

/**
 * 获取订单商品清单
 */
export const fetchTradeSkus = (tid: string) => {
  return Fetch(`/trade/goods/${tid}`);
};

/**
 * 0元订单支付
 */
export const defaultPaylOrder = (tid: string) => {
  return Fetch(`/trade/default/pay/${tid}`);
};

/**
 * 查询店铺精选-小店名称
 * @param distributorId
 */
export const queryShopInfo = (distributorId) =>
  Fetch<TResult>(`/goods/shop-info/${distributorId}`, { method: 'GET' });
