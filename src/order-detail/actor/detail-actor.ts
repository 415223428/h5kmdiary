/**
 * Created by chenpeng on 2017/7/25.
 */
import { fromJS } from 'immutable';
import { Action, Actor, IMap } from 'plume2';

export default class DetailActor extends Actor {
  defaultState() {
    return {
      detail: {}, //订单详情
      skus: [], // 商品清单
      gifts: [], //赠品清单
      // 小B店铺名字
      inviteeShopName: '',
      // 是否自购订单
      selfBuy: false,
      // 是否自营店铺
      isSelf: false
    };
  }

  /**
   * 初始化订单
   */
  @Action('detail-actor:init')
  init(state: IMap, res: object) {
    return state.update('detail', (detail) => detail.merge(res));
  }

  /**
   * 设置商品清单
   */
  @Action('detail-actor:setSkus')
  setSkus(state: IMap, param: any) {
    return state.set('skus', fromJS(param));
  }

  /**
   * 设置赠品清单
   */
  @Action('detail-actor:setGifts')
  setGifts(state: IMap, param: any) {
    return state.set('gifts', fromJS(param));
  }

  /**
   * 小B店铺名字
   */
  @Action('detail-actor:shopName')
  setShopName(state: IMap, shopName) {
    return state.set('inviteeShopName', shopName);
  }

  /**
   * 是否自购订单
   */
  @Action('detail-actor:selfBuy')
  setSelfBuy(state: IMap, selfBuy) {
    return state.set('selfBuy', selfBuy);
  }

  /**
   * 是否自营店铺
   */
  @Action('detail-actor:isSelf')
  setIsSelf(state: IMap, selfBuy) {
    return state.set('isSelf', selfBuy);
  }
}
