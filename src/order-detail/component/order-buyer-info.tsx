import React, { Component } from 'react'
import {IMap, Relax} from 'plume2'

import {OrderWrapper} from 'wmkit'


@Relax
export default class OrderBuyerInfo extends Component<any, any> {

  props: {
    relaxProps?: {
      detail: IMap
    },
  };

  static relaxProps = {
    detail: 'detail'
  }


  render() {
    const {detail} = this.props.relaxProps
    let orderWrapper = OrderWrapper(detail)

    return (
      <div >
        <div className="address-info address-context" style={{borderRadius: "10px 10px 0 0",boxShadow:"rgba(4, 0, 0, 0.1) 1px 2px 7px 0px"}}>
          {/* <i className="iconfont icon-dz posit"></i> */}
          <img src={require("../img/position.png")} alt="" className="posit" style={{width:".30rem",height:".38rem"}}></img>
          <div className="address-content">
            <div className="address-detail">
              <div className="name">
                <span style={{flex:'none'}}>收货人：{orderWrapper.buyerName()}</span>
                <span>{orderWrapper.buyerPhone()}</span>
              </div>
              <p>收货地址：{orderWrapper.buyerAddress()}</p>
            </div>
          </div>
        </div>
        {/* <div className="seperate_line"></div> */}
        <img src={require('../img/line.png')} alt=""  style={{width:"100%",height:".2rem"}}></img>
      </div>
    )
  }
}
