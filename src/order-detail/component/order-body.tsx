import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';

import {skusQL} from "../ql";
import {
  FormItem,
  FormSelect,
  OrderWrapper,
  history,
  ImageListScroll,
  WMkit
} from 'wmkit';

const noneImg = require('../img/none.png');

@Relax
export default class OrderBody extends Component<any, any> {
  props: {
    relaxProps?: {
      detail: IMap;
      inviteeShopName: string;
    };
  };

  static relaxProps = {
    detail: 'detail',
    inviteeShopName: 'inviteeShopName'
  };

  render() {
    const { detail, inviteeShopName } = this.props.relaxProps;
    let orderWrapper = OrderWrapper(detail);
    const {skuName} = orderWrapper.tradeItems();
    let allPointNum =0
    console.log(skuName);
    console.log('---------------------11/10');
    console.log(detail.toJS().tradePrice?detail.toJS().tradePrice.kmPointsValue:0);
    allPointNum = detail.toJS().tradePrice?detail.toJS().tradePrice.kmPointsValue:0
    return (
      <div>
        <div style={{ boxShadow: "rgba(4, 0, 0, 0.1) 1px 2px 7px 0px" }}>
          <div className="sku-head">
          {WMkit.inviteeId() && WMkit.isShop() ? (
            <Link to={`/shop-index-c/${orderWrapper.getInviteeId()}`}>
              {orderWrapper.storeBagsFlag() == 1
                ? inviteeShopName
                : `${orderWrapper.distributorName()}的${orderWrapper.shopName()}`}
            </Link>
          ) : (
            <Link to={`/store-main/${orderWrapper.storeId()}`}>
              {orderWrapper.isSelf() ? (
                <div className="self-sales" style={{ marginLeft: 5 }}>
                  自营
                </div>
              ) : null}
              <span style={{color:"rgba(51,51,51,1)"}}>
              {orderWrapper.storeName()}
              </span>
            </Link>
          )}
        </div>
          <Link to={`/order-detail-skus/${orderWrapper.orderNo()}`}>
            <div className="limit-img" >
              <div className="img-content" style={{width:"100%"}}>
                {orderWrapper
                  .tradeItems()
                  .concat(orderWrapper.gifts())
                  .toJS()
                  .map((v, k) => {
                    if (k < 4) {
                      return (
                        <div key={k} style={{display:"flex",alignItems:"center"}}>
                          <img className="img-item" style={{width:"1.6rem",height:"1.6rem"}} src={v.pic ? v.pic : noneImg} />
                          {/* <div style={{color:"rgba(102,102,102,1"}}>{detail.get("tradeItems").get(k).get("skuName")}</div> */}
                          {
                            orderWrapper
                            .tradeItems()
                            .concat(orderWrapper.gifts())
                            .toJS().length==1&&
                            <div className='cardList-goods-detail'>
                              <span>{detail.get("tradeItems").get(k).get("skuName")}</span>
                              <span>{detail.get("tradeItems").get(k).get("specDetails")}</span>
                            </div>
                          }
                        </div>
                      );
                    }
                  })}
              </div>
              <div className="right-context">
                <div className="total-num">
                  <div>
                    共{
                      orderWrapper.tradeItems().concat(orderWrapper.gifts()).size
                    }种
                </div>
                </div>
                <i className="iconfont icon-jiantou1" />
              </div>
            </div>
          </Link>
          <div className="mb10" >
            <FormSelect
              labelName="付款记录"
              placeholder=""
              selected={{ key: '1', value: orderWrapper.orderPayState() }}
              onPress={() =>
                this._toPayRecord(
                  orderWrapper.orderNo(),
                  orderWrapper.orderPayState()
                )
              }
            />
            <FormSelect
              labelName="发货记录"
              placeholder=""
              selected={{ key: '1', value: orderWrapper.orderDeliveryState() }}
              onPress={() =>
                this._toShipRecord(
                  orderWrapper.orderNo(),
                  orderWrapper.orderDeliveryState()
                )
              }
              style={{ borderRadius: "0 0 10px 10px", boxShadow: "1px 2px 7px 0px rgba(4,0,0,0.1)" }}
              iconVisible={this._isIconVisible(orderWrapper.orderDeliveryState())}
            />
          </div>
        </div>
        <div className="top-border sku-list-head with-box-shadow">
          <FormSelect
            labelName="发票信息"
            placeholder=""
            selected={{ key: '1', value: orderWrapper.orderInvoice() }}
            onPress={() => this._toInvoice(orderWrapper.orderNo())}
            iconVisible={this._isInvoiceVisible(orderWrapper.orderInvoice())}
            style={{ borderRadius: "10px 10px 0 0", display: 'flex',alignItems: 'center'}}
          />
        </div>
        <div className="mb10 with-box-shadow">
          {/* {detail.toJS().tradeItems?detail.toJS().tradeItems.map((v, i) => (
            console.log('vvvvvvvvvvvvvvvvvvvvvvv'),
            console.log(v.kmPointsValue),
            console.log('iiiiiiiiiiiiiiiiiiiiiiiiii'),
            <div style={{display:'none'}}>{allPointNum+=v.kmPointsValue}</div>
          )):null} */}
          {localStorage.getItem('loginSaleType') == '1'?
            <div className="order-wrap" style={{padding: '16px 14px',display:'flex',justifyContent:'space-between',color:'#333'}}>
              <div>订单预估点数</div>
              <div>{allPointNum}</div>
            </div>:null
          }
          <FormItem
            labelName="订单备注"
            placeholder={orderWrapper.buyerRemark()}
          />
          <FormItem
            labelName="卖家备注"
            placeholder={orderWrapper.sellerRemark()}
            style={{ borderRadius: "0 0 10px 10px" }}
          />

        </div>
        <div className="mb10 box-radius ku-list-head sku-list-foot with-box-shadow">
          {detail.get('tradeItems') && detail.get('tradeItems').get(0).get('isFlashSaleGoods') ? (
            ''
          ) : (
              <ImageListScroll
                imageList={orderWrapper.encloses()}
                labelName="订单附件"
                style={{ borderRadius: "10px" }}
              />
            )}
        </div>
        <div className="total-price mb10 with-box-shadow box-radius">
          <div className="total-list">
            <span>应付金额</span>
            <span className="price-color">
              <i className="iconfont icon-qian" />
              {orderWrapper.totalPrice()}
            </span>
          </div>
          <div className="total-list">
            <span>商品总额</span>
            <span>
              <i className="iconfont icon-qian" />
              {orderWrapper.goodsPrice()}
            </span>
          </div>
          {/*非店铺内购买商品，展示原本营销优惠*/}
          {!(WMkit.inviteeId() && WMkit.isShop()) ? (
            <div>
              {orderWrapper.reductionPrice() != 0 && (
                <div className="total-list">
                  <span>满减优惠</span>
                  <span>
                    -<i className="iconfont icon-qian" />
                    {orderWrapper.reductionPrice()}
                  </span>
                </div>
              )}
              {orderWrapper.discountPrice() != 0 && (
                <div className="total-list">
                  <span>满折优惠</span>
                  <span>
                    -<i className="iconfont icon-qian" />
                    {orderWrapper.discountPrice()}
                  </span>
                </div>
              )}
              {orderWrapper.pointsPrice() != 0 && (
                <div className="total-list">
                  <span>积分抵扣</span>
                  <span>
                    -<i className="iconfont icon-qian" />
                    {orderWrapper.pointsPrice()}
                  </span>
                </div>
              )}
              {orderWrapper.couponPrice() != 0 && (
                <div className="total-list">
                  <span>优惠券</span>
                  <span>
                    -<i className="iconfont icon-qian" />
                    {orderWrapper.couponPrice()}
                  </span>
                </div>
              )}
            </div>
          ) : null}
          <div className="total-list">
            <span>配送费用</span>
            <span>
              <i className="iconfont icon-qian" />
              {orderWrapper.deliveryPrice()}
            </span>
          </div>
          {/*{!WMkit.isShop() &&
          !WMkit.inviteeId() &&
          orderWrapper.getInviteeId() &&
          orderWrapper.buyerId() == orderWrapper.getInviteeId() &&
          orderWrapper.commission() != 0 ? (
            <div className="total-list total-rebate b-1px-t">
              <span>预计返利</span>
              <span>
                <i className="iconfont icon-qian" />
                {orderWrapper.commission()}
              </span>
            </div>
          ) : null}*/}
        </div>
      </div>
    );
  }

  /**
   * 付款记录页
   */
  _toPayRecord = (tid: string, pay: string) => {
    history.push({
      pathname: `/pay-detail/${tid}`
    });
  };

  /**
   * 发票记录
   */
  _toInvoice = (id: string) => {
    const { detail } = this.props.relaxProps;
    let orderWrapper = OrderWrapper(detail);
    if (orderWrapper.orderInvoice() == '不需要发票') {
      return;
    } else {
      history.push({
        pathname: `/invoice-info/${id}/0`
      });
    }
  };

  _toShipRecord = (id: string, state: string) => {
    if (state == '未发货') {
      return;
    } else {
      history.push(`/ship-record/${id}/0`);
    }
  };

  /**
   * 发货记录icon是否显示
   * @param state
   * @returns {boolean}
   * @private
   */
  _isIconVisible = (state: string) => {
    if (state === '未发货') {
      return false;
    }
    return true;
  };

  /**
   * 发票信息是否显示
   */
  _isInvoiceVisible = (invoice: string) => {
    if (invoice === '不需要发票') {
      return false;
    }
    return true;
  };
  /*  _showImage=()=>{
        System.import('..../wmkit/image-util/zoom-image').then((zoomImage) => {
          ZoomImage = zoomImage;
        });
      ZoomImage.renderZoomImage({src})
     }*/
}
