import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';

import { OrderWrapper } from 'wmkit';

@Relax
export default class OrderBaseInfo extends Component<any, any> {
  props: {
    relaxProps?: {
      detail: IMap;
    };
  };

  static relaxProps = {
    detail: 'detail'
  };

  render() {
    const { detail } = this.props.relaxProps;
    let orderWrapper = OrderWrapper(detail);

    return (
      <div className="order-status"style={{boxShadow:"1px 2px 7px 0px rgba(4,0,0,0.1)",borderRadius:"10px",margin:"0 .2rem .2rem"}}>
        <div className="status">
          <span>订单状态</span>
          <span className="result">{orderWrapper.orderState()}</span>
        </div>
        {orderWrapper.isVoidTrade() ? (
          <div className="status">
            <span className="text">作废原因</span>
            <span className="text textFlex">
              {orderWrapper.obsoleteReason()}
            </span>
          </div>
        ) : (
          ''
        )}
        <div className="status">
          <span className="text">订单号</span>
          <span className="text">
            {' '}
            {orderWrapper.platform() != 'CUSTOMER' && <span>代</span>}{' '}
            {orderWrapper.orderNo()}
          </span>
        </div>
        <div className="status">
          <span className="text">下单时间</span>
          <span className="text">{orderWrapper.createTime()}</span>
        </div>
      </div>
    );
  }
}
