import { IOptions, Store, msg } from 'plume2';
import { _, Alert, history, WMkit } from 'wmkit';
import { cache, config } from 'config';
import { fromJS } from 'immutable';

import * as webapi from './webapi';
import { mergePurchase } from 'biz';
import RegisterActor from './actor/register-actor';

let isSumit = false;
export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new RegisterActor()];
  }

  getRegisterLimitType = async () => {
    const { code, context } = await webapi.getRegisterLimitType();
    if (code == config.SUCCESS_CODE) {
      this.dispatch('set:reisterLimitType', context);
      if (!fromJS(context).get('openFlag')) {
        this.submit();
      }
    }
  };

  submit = () => {
    if (isSumit) {
      return;
    }
    isSumit = true;
    const openFlag = this.state().get('openFlag');
    const inviteCode = this.state().get('inviteCode');
    const reisterLimitType = this.state().get('reisterLimitType');
    if (
      openFlag &&
      reisterLimitType == 1 &&
      !WMkit.testInviteCode(inviteCode)
    ) {
      isSumit = false;
      return;
    }
    if (openFlag && inviteCode && !WMkit.testInviteCode(inviteCode)) {
      isSumit = false;
      return;
    }
    const registerInfo: any = JSON.parse(
      sessionStorage.getItem(cache.REGISTER_INFO)
    );
    const fromPage = registerInfo.fromPage;
    if (fromPage) {
      this.registerModal(registerInfo);
    } else {
      this.register(registerInfo);
    }
  };


  /**
   * 注册页面-注册
   * @param registerInfo
   * @returns {Promise<void>}
   */
  register = async (registerInfo) => {
    const { code, message, context } = await webapi.register(
      registerInfo.customerAccount,
      registerInfo.customerPassword,
      registerInfo.verifyCode,
      registerInfo.patchca,
      registerInfo.uuid,
      registerInfo.employeeId,
      registerInfo.shareUserId,
      this.state().get('inviteCode')
    );
    if (code == config.SUCCESS_CODE) {
      isSumit = false;
      WMkit.clearLoginCache();
      //清除本地缓存的审核未通过的或者正在审核中的账户信息
      localStorage.removeItem(cache.PENDING_AND_REFUSED);
      sessionStorage.removeItem(cache.REGISTER_INFO);
      //是否直接可以登录 0 否 1 是
      if (!(context as any).isLoginFlag) {
        Alert({
          text: '注册成功，请完善账户信息'
        });
        //提示完善账户信息
        history.push(`/improve-information/${(context as any).customerId}`);
      } else {
        //直接跳转到主页
        localStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
        sessionStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
        Alert({
          text: '登录成功'
        });
        setTimeout(() => {
          history.push('/');
          _.showRegisterModel((context as any).couponResponse, true);
        }, 1000);
      }
    } else {
      isSumit = false;
      Alert({
        text: message
      });
    }
  };

  /**
   * 弹窗注册-注册
   * @param registerInfo
   * @returns {Promise<void>}
   */
  registerModal = async (registerInfo) => {
    const { code, message, context } = await webapi.registerModal(
      registerInfo.customerAccount,
      registerInfo.customerPassword,
      registerInfo.verifyCode,
      registerInfo.inviteeId,
      registerInfo.shareUserId,
      this.state().get('inviteCode')
    );
    if (code == config.SUCCESS_CODE) {
      isSumit = false;
      //清除本地缓存的审核未通过的或者正在审核中的账户信息
      localStorage.removeItem(cache.PENDING_AND_REFUSED);
      sessionStorage.removeItem(cache.REGISTER_INFO);
      //是否直接可以登录 0 否 1 是
      if (!(context as any).isLoginFlag) {
        Alert({
          text: '注册成功，请完善账户信息'
        });
        //存储customerId
        // this.dispatch('change:customerDetailField', {
        //   field: 'customerId',
        //   value: (context as any).customerId
        // });
        // this.dispatch('show:showImproveInfo');
        history.push(`/improve-information/${(context as any).customerId}`);
      } else {
        //存储token
        window.token = (context as any).token;
        localStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
        sessionStorage.setItem(cache.LOGIN_DATA, JSON.stringify(context));
        // 登录成功，发送消息，查询分销员信息 footer/index.tsx
        msg.emit('userLoginRefresh');
        // b.登陆成功,需要合并登录前和登陆后的购物车,传入回调函数
        mergePurchase(this.state().get('callBack'));
        // c.登陆成功,关闭弹框
        Alert({ text: '登录成功' });
        /* this.dispatch('loginModal:toggleVisible', {
          visible: false,
          callBack: null
        });*/
        setTimeout(() => {
          history.push('/main');
          _.showRegisterModel((context as any).couponResponse, true);
        }, 1000);
      }
    } else {
      isSumit = false;
      Alert({
        text: message
      });
    }
  };

  setInviteCode = (inviteCode: string) => {
    this.dispatch('set:inviteCode', inviteCode);
  };

  addSuperiorByInviteCode = async (inviteCode) => {
    
    const { code, context ,message} = await webapi.addInviteCode(inviteCode);
    if (code === 'K-000000') {
      Alert({
        text:'添加成功'
      })
      history.push('/user-center')
    }else if(message == '参数不正确'){
      Alert({
        text:'请输入邀请码'
      })
    }else
    {
      Alert({
        text:message
      })
    }
  };
}
