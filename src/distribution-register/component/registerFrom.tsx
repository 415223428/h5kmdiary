import React, { Component } from 'react';
import { Relax } from 'plume2';
import { Button } from 'wmkit';
import noop from 'wmkit/noop';
const SubmitButton = Button.LongBlue;
const submit1=require('../img/submit.png')

@Relax
export default class RegisterFrom extends Component<any, any> {
  props: {
    isaddInviteCode:boolean;
    relaxProps?: {
      inviteCode: string;
      setInviteCode: Function;
      submit: Function;
      addSuperiorByInviteCode:Function;
    };
  };

  static relaxProps = {
    inviteCode: 'inviteCode',
    setInviteCode: noop,
    submit: noop,
    addSuperiorByInviteCode:noop,
  };

  componentWillMount(){
    const otherinviteCode = sessionStorage.getItem('otherinviteCode')||'';
    this.props.relaxProps.setInviteCode(otherinviteCode||"");
  }

  render() {
    const {isaddInviteCode} = this.props;
    
    const { inviteCode, setInviteCode, submit ,addSuperiorByInviteCode} = this.props.relaxProps;
    return (
      <div className="register-from">
        <p style={{
          fontSize: '.32rem',
          fontFamily: 'PingFang SC',
          fontWeight: 500,
          color: 'rgba(0,0,0,1)',
        }}>请输入邀请码</p>
        <input
          type="text"
          style={{
            width: '3.1rem',
            fontSize: '.28rem',
            lineHeight: '0.68rem',
            paddingBottom:0,
            fontFamily: 'PingFang SC',
            fontWeight: 'bold',
            color: 'rgba(102,102,102,1)'
          }}
          placeholder="请输入您的邀请码(选填)"
          maxLength={8}
          value={inviteCode}
          onChange={(e) => setInviteCode(e.target.value)}
        />
        <div style={{
          width: '4.6rem'
        }}>
          {
            isaddInviteCode?
            <img src={submit1} alt="" style={{width:'5rem'}} onClick={() => addSuperiorByInviteCode(inviteCode)}/>
            :
          <img src={submit1} alt="" style={{width:'5rem'}} onClick={() => submit()}/>
          }
          {/* <SubmitButton text="提交" onClick={() => submit()}>
            提交
          </SubmitButton> */}
        </div>
      </div>
    );
  }
}
