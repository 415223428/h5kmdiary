import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import RegisterFrom from './component/registerFrom';
const styles = require('./css/style.css');
@StoreProvider(AppStore, { debug: __DEV__ })
export default class DistributionRegister extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    this.store.getRegisterLimitType();
  }

  render() {
    const addInviteCode =
    this.props.location.state && this.props.location.state.addInviteCode;
    
    const openFlag = this.store.state().get('openFlag');
    return openFlag ? (
      <div className={styles.distributionRegister}>
        <RegisterFrom isaddInviteCode={addInviteCode}/>
      </div>
    ) : null;
  }
}
