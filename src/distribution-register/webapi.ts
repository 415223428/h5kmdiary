/**
 * Created by feitingting on 2017/7/13.
 */
import { Fetch } from 'wmkit';

/**
 * 登录系统
 * @param mobile
 * @param code
 * @param password
 * @returns {Promise<Result<T>>}
 */
export const register = (
  mobile: string,
  password: string,
  code: string,
  patchca: string,
  uuid: string,
  employeeId: string,
  shareUserId: string,
  inviteCode: string
) => {
  return Fetch('/register', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: mobile,
      customerPassword: password,
      verifyCode: code,
      patchca: patchca,
      uuid: uuid,
      employeeId: employeeId,
      shareUserId: shareUserId,
      inviteCode: inviteCode
    })
  });
};

export const registerModal = (
  account,
  password,
  telCode,
  inviteeId,
  shareUserId,
  inviteCode
) => {
  return Fetch('/register/modal', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: account,
      customerPassword: password,
      verifyCode: telCode,
      inviteeId: inviteeId,
      shareUserId: shareUserId,
      inviteCode: inviteCode
    })
  });
};

/**
 * 获取注册限制
 */
export const getRegisterLimitType = () =>
  Fetch(`/getRegisterLimitType`, { method: 'POST' });

export const addInviteCode =(inviteCode)=>{
 return Fetch('/customer/addSuperiorByInviteCode',{
    method:'POST',
    body:JSON.stringify({
      inviteCode
    })
  });
}
