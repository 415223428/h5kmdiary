import { WMkit, history, Alert, wxShare } from 'wmkit';
import { cache, config } from 'config';
import { fromJS } from 'immutable';
import { Fetch } from 'wmkit';
import { msg } from 'plume2';
import { putPurchase } from 'biz';

const props = {
  renderHost: config.RENDER_HOST,
  ossHost: config.OSS_HOST,
  systemCode: 'd2cStore',
  envCode: 'test1',
  storeId: '',
  platform: 'weixin',
  pageType: 'index',
  uid: '000000',
  onDataLoaded: ({ title, shareInfo }) => {
    document.title = '首页-康美日记';
    let shareTitle = shareInfo ? shareInfo.title : '';
    if (shareTitle == '') {
      shareTitle = '首页-康美日记';
    }
    const data = {
      url: location.href,
      title: shareTitle,
      imgUrl: shareInfo ? shareInfo.imgSrc : '',
      desc: shareInfo ? shareInfo.desc : '',
      afterSuccess: '',
      onCancel: ''
    };
    wxShare(data);
  },
  api: {
    getUserInfo: async () => {
      const loginData = JSON.parse(
        localStorage.getItem(cache.LOGIN_DATA) || '{}'
      );
      const customerName =
        (loginData.customerDetail && loginData.customerDetail.customerName) ||
        '';
      return { nickName: customerName };
    },
    logout: async () => {
      WMkit.logout();
    },
    go_link: async (data) => {
      if (!data) return;
      if (typeof data == 'string') data = JSON.parse(data);
      const { wechat } = data;
      const { pathname, params, isHref } = wechat;
      if (isHref) {
        location.href = pathname;
      } else {
        history.push({
          pathname,
          ...params
        });
      }
    },
    goGoodsDetail: (skuId) => {
      history.push('/goods-detail/' + skuId);
    },
    getIndexGoodsList: async (data) => {
      const req = fromJS(data).toJS();
      let request;
      if (req.catId) {
        request = { pageSize: req.pageSize, sortFlag: 0, cateId: req.catId };
      } else {
        request = { pageSize: req.pageSize, sortFlag: 0 };
      }
      const storeId = localStorage.getItem('X-STIE-STOREID');
      if (storeId) {
        request.storeId = storeId;
        if (req.catId) {
          const storeCateIds = [req.catId];
          request.storeCateIds = storeCateIds;
          delete request.cateId;
        }
      }
      let url = '/goods/skuListFront';
      if (WMkit.isLoginOrNotOpen()) {
        url = '/goods/skus';
      }
      const res = await Fetch(url, {
        method: 'POST',
        body: JSON.stringify(request)
      });
      if (res.code === config.SUCCESS_CODE) {
        const dataList = fromJS(
          (res.context as any).esGoodsInfoPage.content
        ).map((item) => {
          let img = item.get('goodsInfo').get('goodsInfoImg');
          if (img == '') {
            const spuList = (res.context as any).goodsList;
            for (let index in spuList) {
              if (
                item.get('goodsInfo').get('goodsId') == spuList[index].goodsId
              ) {
                img = spuList[index].goodsImg;
              }
            }
          }

          let price =
            item.get('goodsInfo').get('priceType') === 1
              ? Number(
                item.get('goodsInfo').get('intervalMinPrice')
                  ? item.get('goodsInfo').get('intervalMinPrice')
                  : 0
              ).toFixed(2)
              : Number(
                item.get('goodsInfo').get('salePrice')
                  ? item.get('goodsInfo').get('salePrice')
                  : 0
              ).toFixed(2);

          if (item.getIn(['goodsInfo', 'distributionGoodsAudit']) == '2') {
            price = item.get('goodsInfo').get('marketPrice');
          }

          return {
            img: img,
            name: item.get('goodsInfo').get('goodsInfoName'),
            title: item.get('goodsInfo').get('goodsInfoName'),
            price: price,
            id: item.get('goodsInfo').get('goodsInfoId'),
            pid: item.get('goodsInfo').get('goodsId'),
            specName:
              item.get('specDetails') &&
              item
                .get('specDetails')
                .map((spec) => {
                  return spec.get('detailName');
                })
                .join(' '),
            priceType: item.get('goodsInfo').get('priceType'),
            salePrice: item.get('goodsInfo').get('salePrice'),
            intervalMinPrice: item.get('goodsInfo').get('intervalMinPrice')
          };
        });
        return { dataList: dataList };
      }
    },
    getInfoAndMarkets: async (data) => {
      const goodsIds = fromJS(data)
        .get('goodsList')
        .map((item) => {
          return item.goodsId;
        });
      let url = '/goods/skuListFront';
      if (WMkit.isLoginOrNotOpen()) {
        url = '/goods/skus';
      }
      const res = await Fetch(url, {
        method: 'POST',
        body: JSON.stringify({
          sortFlag: 0,
          goodsInfoIds: goodsIds
        })
      });
      if (res.code === config.SUCCESS_CODE) {
        let dataList = Array();
        fromJS((res.context as any).esGoodsInfoPage.content).map((item) => {
          let img = item.get('goodsInfo').get('goodsInfoImg');
          if (img == '') {
            const spuList = (res.context as any).goodsList;
            for (let index in spuList) {
              if (
                item.get('goodsInfo').get('goodsId') == spuList[index].goodsId
              ) {
                img = spuList[index].goodsImg;
              }
            }
          }

          let currentPrice =
            item.get('goodsInfo').get('priceType') === 1
              ? Number(
                item.get('goodsInfo').get('intervalMinPrice')
                  ? item.get('goodsInfo').get('intervalMinPrice')
                  : 0
              ).toFixed(2)
              : Number(
                item.get('goodsInfo').get('salePrice')
                  ? item.get('goodsInfo').get('salePrice')
                  : 0
              ).toFixed(2);

          if (item.getIn(['goodsInfo', 'distributionGoodsAudit']) == '2') {
            currentPrice = item.get('goodsInfo').get('marketPrice');
          }

          dataList[item.get('goodsInfo').get('goodsInfoId')] = {
            imgSrc: img,
            skuName: item.get('goodsInfo').get('goodsInfoName'),
            spuName: item.get('goodsInfo').get('goodsInfoName'),
            currentPrice: currentPrice,
            skuId: item.get('goodsInfo').get('goodsInfoId'),
            pid: item.get('goodsInfo').get('goodsId'),
            specName:
              item.get('specDetails') &&
              item
                .get('specDetails')
                .map((spec) => {
                  return spec.get('detailName');
                })
                .join(' '),
            priceType: item.get('goodsInfo').get('priceType'),
            salePrice: item.get('goodsInfo').get('salePrice'),
            intervalMinPrice: item.get('goodsInfo').get('intervalMinPrice')
          };
        });
        return { ...dataList };
      }
    },
    addCart: async (perem) => {
      const { goodsId } = perem;
      if (!WMkit.isLoginOrNotOpen()) {
        if (putPurchase(goodsId, 1)) {
          msg.emit('purchaseNum');
          Alert({
            text: '加入成功'
          });
        } else {
          Alert({
            text: '加入失败'
          });
        }
        return;
      }
      const saveToPurchase = (request: any) => {
        return Fetch('/site/purchase', {
          method: 'POST',
          body: JSON.stringify({
            goodsInfoId: request,
            goodsNum: 1,
            verifyStock: false
          })
        });
      };
      const fetchPurchaseCount = () => {
        return Fetch('/site/countGoods');
      };
      const _fetchPurChaseNum = async () => {
        const { context, message, code } = await fetchPurchaseCount();
        this.setState({ purchaseNum: context });
      };
      const result = (await saveToPurchase(goodsId)) as any;
      if (result.code === 'K-000000') {
        msg.emit('purchaseNum');
        Alert({
          text: '加入成功'
        });
      } else {
        Alert({
          text: result.message
        });
      }
    },
    xsiteNavigator: async (obj) => {
      if (obj) {
        if (obj.type == 'link') {
          if (obj.linkKey == 'goodsList') {
            _goLink(`/goods-detail/${obj.info.skuId}`, obj.target);
          } else if (obj.linkKey == 'categoryList') {
            const { pathName } = obj.info;
            let node = '';
            if (pathName.indexOf(',') != -1) {
              node = pathName.split(',');
              node = node[node.length - 1];
              node = encodeURIComponent(node);
            } else {
              node = encodeURIComponent(pathName);
            }
            _goLink(
              `/goodsList?cid=${obj.info.cataId}&cname=${node}`,
              obj.target
            );
          } else if (obj.linkKey == 'pageList') {
            const storeId = localStorage.getItem('X-STIE-STOREID');
            if (storeId) {
              _goLink(
                `/page/${obj.info.pageType}/${obj.info.pageCode}/${storeId}`,
                obj.target
              );
            } else {
              _goLink(
                `/page/${obj.info.pageType}/${obj.info.pageCode}`,
                obj.target
              );
            }
          } else if (obj.linkKey == 'userpageList') {
            _goLink(obj.info.link, obj.target);
          } else if (obj.linkKey == 'custom') {
            if (obj.target) {
              window.open(obj.info.content);
            } else {
              location.href = obj.info.content;
            }
          }
        }
      }
    }
  }
};

const _goLink = (link, target) => {
  if (target) {
    window.open(link);
  } else {
    history.push(link);
  }
};

export { props };
