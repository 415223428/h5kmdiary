import React, { Component } from 'react';
import { Relax } from 'plume2';
import GoodsEvaluationItem from './goods-evaluation-item';

@Relax
export default class GoodsEvaluation extends Component<any, any> {
  static relaxProps = {};

  constructor(props) {
    super(props);
  }

  render() {
    const {} = this.props.relaxProps;
    return (
      <div className="evaluation-list b-1px-b">
        <GoodsEvaluationItem />
      </div>
    );
  }
}
