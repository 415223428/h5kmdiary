import React, { Component } from 'react';
import { Relax } from 'plume2';
import { Star, noop } from 'wmkit';

@Relax
export default class GoodsEvaluationItem extends Component<any, any> {
  contentRef;

  props: {
    relaxProps?: {};
  };

  static relaxProps = {};

  constructor(props) {
    super(props);
    this.state = {
      isLong: false,
      isExpand: false,
      zanFlag: false,
      imageData: [
        {
          img:
            'https://img.alicdn.com/imgextra/i1/1645354849/TB2m0zrsiFTMKJjSZFAXXckJpXa_!!1645354849.jpg_430x430q90.jpg'
        },
        {
          img:
            'https://img.alicdn.com/imgextra/i3/1645354849/TB2haIIqMoQMeJjy0FoXXcShVXa_!!1645354849.jpg_430x430q90.jpg'
        },
        {
          img:
            'https://img.alicdn.com/imgextra/i1/1645354849/TB2m0zrsiFTMKJjSZFAXXckJpXa_!!1645354849.jpg_430x430q90.jpg'
        },
        {
          img:
            'https://gd3.alicdn.com/imgextra/i3/1757476878/TB2y5H0ryRnpuFjSZFCXXX2DXXa_!!1757476878.jpg_400x400.jpg'
        }
      ]
    };
  }

  componentDidMount() {
    let _height = this.contentRef.offsetHeight;
    if (_height > 18 * 5 - 5) {
      this.setState({
        isLong: true
      });
    }
  }

  render() {
    const {} = this.props.relaxProps;
    let { isLong, isExpand } = this.state;
    return (
      <div className="evaluation-item">
        <div className="evaluation-item-head">
          <div className="evaluation-item-head-user">
            <img
              src="http://web-img.qmimg.com/qianmicom/u/cms/syy/201803/260942417ii0.png"
              alt=""
            />
            <div className="evaluation-item-head-info">
              <span className="evaluation-text">某某用户</span>
              <Star star={3} />
            </div>
          </div>
          <div className="evaluation-item-head-time">2018-10-12</div>
        </div>
        <div
          className="evaluation-item-body"
          style={isExpand ? {} : { maxHeight: 18 * 5 + 'px' }}
        >
          <p
            className={
              'evaluation-item-text ' + (isExpand ? '' : 'expand-text')
            }
            ref={(dom) => {
              this.contentRef = dom;
            }}
          >
            评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容
            评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容评价内容
          </p>
        </div>
        {isLong && (
          <div className="evaluation-item-expand">
            <span onClick={() => this.setState({ isExpand: !isExpand })}>
              <span>{isExpand ? '收起' : '展开'}</span>
              <i
                className="iconfont icon-xiala-copy"
                style={{ transform: 'rotate(' + (isExpand ? 180 : 0) + 'deg)' }}
              />
            </span>
          </div>
        )}
        <ul className="evaluation-big-image">
          {this.state.imageData.map((v, i) => {
            return (
              <li key={i}>
                <img src={v.img} alt="" />
              </li>
            );
          })}
        </ul>
        <div className="evaluation-item-bottom">
          <div className="evaluation-item-bottom-info">
            <div className="evaluation-item-bottom-info-text">
              <span>规格1</span>
              <span>规格2</span>
            </div>
            <p className="evaluation-item-bottom-info-text">
              购买时间：2017-01-01
            </p>
          </div>
          <div className="evaluation-item-bottom-praise">
            <i
              className={
                this.state.zanFlag
                  ? 'iconfont icon-zan red-color'
                  : 'iconfont icon-zan'
              }
              onClick={this._clickZan}
            />
            <span>13</span>
          </div>
        </div>
        <div className="evaluation-advert">
          <div className="left-advert-img">
            <img
              src="http://web-img.qmimg.com/qianmicom/u/cms/syy/201803/260942417ii0.png"
              alt=""
            />
          </div>
          <div className="right-advert-content">
            <span className="up-text">
              沙滩条纹短裤沙滩条纹短裤沙滩滩滩裤沙
            </span>
            <span className="center-text">
              <span>规格值1</span>
              <span className="center-spn">规格值2</span>
              <span>规格值3</span>
            </span>
            <span className="down-money">￥ 122.00</span>
          </div>
        </div>
      </div>
    );
  }
  _clickZan = () => {
    this.setState({
      zanFlag: !this.state.zanFlag
    });
  };
}
