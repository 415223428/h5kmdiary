import React from 'react';
import { StoreProvider } from 'plume2';

import AppStore from './store';
import GoodsEvaluation from './component/goods-evaluation';
import './css/style.css';
@StoreProvider(AppStore, { debug: __DEV__ })
export default class EvaluationDetail extends React.Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    window.scroll(0, 0);
  }

  render() {
    return (
      <div className="goods-detail">
        <GoodsEvaluation />
      </div>
    );
  }
}
