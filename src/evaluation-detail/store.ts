import { IOptions, Store, msg } from 'plume2';

import EvaluationListActor from './actor/evaluation-list-actor';
import { config } from 'config';
import * as webapi from './webapi';
import * as WMkit from 'wmkit/kit';
import { Alert } from 'wmkit';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new EvaluationListActor()];
  }

  /**
   * 商品评价点赞
   * @param goodsEvaluateId
   */
  addCustomerGoodsEvaluatePraise = async (goodsEvaluateId) => {
    if (WMkit.isLogin()) {
      const { code, message } = await webapi.addCustomerGoodsEvaluatePraise({
        goodsEvaluateId: goodsEvaluateId
      });
      if (code == config.SUCCESS_CODE) {
        Alert({ text: '点赞成功!' });
        location.reload();
      } else {
        Alert({ text: message });
      }
    } else {
      //显示登录弹框
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          location.reload();
        }
      });
    }
  };
}
