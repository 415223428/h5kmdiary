import React from 'react';
import { StoreProvider } from 'plume2';
import SearchBar from './component/search-bar';
import AppStore from './store';

/**
 * 商品搜索
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class Search extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const key = this.props.location.state && this.props.location.state.key;
    const queryString =
      this.props.location.state && this.props.location.state.queryString;
    if (key) {
      this.store.changeKeyword(queryString);
      //根据跳转过来的类别,选中tab页,以及搜索对应的历史
      this.store.tabActive(key);
    } else {
      this.store.changeKeyword(queryString);
      this.store.getHistory();
    }
  }

  render() {
    return (
      <div className="content">
        <SearchBar />
      </div>
    );
  }
}
