import React from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class Tabs extends React.Component<any, any> {
  props: {
    relaxProps?: {
      key: string;
      tabActive: Function;
      changeKeyword: Function;
    };
  };

  static relaxProps = {
    key: 'key',
    tabActive: noop,
    changeKeyword: noop
  };

  render() {
    const { key } = this.props.relaxProps;

    return (
      <div className="tab-bar b-1px-tb">
        <a
          href="javascript:;"
          onClick={() => this._tabActive('goods')}
          className={`${key == 'goods' ? 'cur' : ''}`}
        >
          商品
        </a>
        <a
          href="javascript:;"
          onClick={() => this._tabActive('supplier')}
          className={`${key == 'supplier' ? 'cur' : ''}`}
        >
          商家
        </a>
      </div>
    );
  }

  _tabActive = (k) => {
    const { tabActive, changeKeyword } = this.props.relaxProps;
    changeKeyword('');
    tabActive(k);
  };
}
