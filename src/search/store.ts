import { Store } from 'plume2';
import { fromJS } from 'immutable';
import { WMkit } from 'wmkit';
import { config } from 'config';
import * as webapi from './webapi';
import SearchActor from './actor/search-actor';

export default class AppStore extends Store {
  bindActor() {
    return [new SearchActor()];
  }

  constructor(props) {
    super(props);
    if (__DEV__) {
      //debug
      (window as any)._store = this;
    }
  }

  /**
   * 变更搜索关键字
   */
  changeKeyword = (keyword: string) => {
    this.dispatch('search:keyword', keyword);
  };

  /**
   * 查询搜索历史
   * @returns {Promise<void>}
   */
  getHistory = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      const key = this.state().get('key');
      if (key == 'goods') {
        //如果是查询商品历史
        const res = await webapi.getGoodsHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS(res.context).reverse());
        }
      } else if (key == 'supplier') {
        //如果是查询店铺历史
        const res = await webapi.getStoreHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS(res.context).reverse());
        }
      } else if (key == 'distribute') {
        //查询分销员选品搜索历史
        const res = await webapi.getDistributeHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS(res.context).reverse());
        }
      } else if (key == 'groupon') {
        //查询拼团商品搜索历史
        const res = await webapi.getGrouponHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS(res.context).reverse());
        }
      } else if (key == 'distributeGoods') {
        //查询分销推广商品搜索历史
        const res = await webapi.getDistributeGoodsHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS(res.context).reverse());
        }
      }
    }
  };

  /**
   * 添加搜索记录
   * @param {string} keyword
   */
  addHistory = async (keyword: string) => {
    if (WMkit.isLoginOrNotOpen()) {
      const key = this.state().get('key');
      if (key == 'goods') {
        //如果是添加商品历史
        await webapi.addGoodsHistory(keyword);
      } else if (key == 'supplier') {
        //如果是添加店铺历史
        await webapi.addStoreHistory(keyword);
      } else if (key == 'distribute') {
        //添加 分销员选品 搜索历史
        await webapi.addDistributeHistory(keyword);
      } else if (key == 'groupon') {
        //添加 拼团商品 搜索历史
        await webapi.addGrouponHistory(keyword);
      } else if (key == 'distributeGoods') {
        //添加 分销推广商品 搜索历史
        await webapi.addDistributeGoodsHistory(keyword);
      }
    }
  };

  /**
   * 清除搜索记录
   */
  clearHistory = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      const key = this.state().get('key');
      if (key == 'goods') {
        //如果是清除商品历史
        const res = await webapi.clearGoodsHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS([]));
        }
      } else if (key == 'supplier') {
        //如果是清除店铺历史
        const res = await webapi.clearStoreHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS([]));
        }
      } else if (key == 'distribute') {
        //清除分销员选品搜索历史
        const res = await webapi.clearDistributeHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS([]));
        }
      } else if (key == 'groupon') {
        //清除拼团商品搜索历史
        const res = await webapi.clearGrouponHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS([]));
        }
      } else if (key == 'distributeGoods') {
        //清除分销推广商品搜索历史
        const res = await webapi.clearDistributeGoodsHistory();
        if (res.code === config.SUCCESS_CODE) {
          this.dispatch('search:history', fromJS([]));
        }
      }
    }
  };

  /**
   * tab切换
   */
  tabActive = async (tabKey: string) => {
    this.dispatch('tab:history', tabKey);
    await this.getHistory();
  };
}
