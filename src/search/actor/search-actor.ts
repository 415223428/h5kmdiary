import { Action, Actor, IMap } from "plume2";


export default class SearchActor extends Actor {


  defaultState() {
    return {
      searchHistory: [], //当前显示的搜索历史列表
      key: 'goods',
      keyword: '',
    }
  }


  @Action('search:keyword')
  changeKeyword(state: IMap, keyword: string) {
    return state.set('keyword', keyword)
  }


  @Action('search:history')
  initHistory(state: IMap, searchHistory: string[]) {
    return state.set('searchHistory', searchHistory)
  }


  @Action('tab:history')
  click(state, key: String) {
    return state.set('key', key)
  }
}
