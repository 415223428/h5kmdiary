import React, { Component } from 'react';
import { Relax } from 'plume2';
import { Alert, noop, UploadImage, WMImage } from 'wmkit';
import { IList } from 'typings/globalType';

@Relax
export default class Enclosure extends Component<any, any> {
  props: {
    storeId: number;
    relaxProps?: {
      orderConfirm: IList;

      addImage: Function;
      removeImage: Function;
    };
  };

  static relaxProps = {
    orderConfirm: 'orderConfirm',

    addImage: noop,
    removeImage: noop
  };

  render() {
    const { orderConfirm } = this.props.relaxProps;
    const index = orderConfirm.findIndex(
      (f) => f.get('storeId') == this.props.storeId
    );
    const enclosures = orderConfirm.getIn([index, 'enclosures']);
    if (!enclosures) {
      return null;
    }
    return (
      <div className="order-attachment">
        <h4>订单附件</h4>
        <div className="image-scroll">
          {enclosures.toJS().map((v, index) => {
            return (
              <div className="refund-file delete-img">
                <WMImage
                  src={v.fileData ? v.fileData : v.image}
                  width="53px"
                  height="53px"
                />
                <a
                  href="javascript:void(0)"
                  onClick={() => this.removeImage(index)}
                >
                  ×
                </a>
              </div>
            );
          })}
          {enclosures.count() >= 10 ? null : (
            <div className="refund-file">
              <UploadImage
                onSuccess={this._uploadImage}
                onLoadChecked={this._uploadCheck}
                repeat
                className="upload"
                size={5}
                fileType=".jpg,.png,.jpeg,.gif"
              >
                <div className="upload-item">
                  <div className="upload-content">
                    <i className="iconfont icon-add11" />
                  </div>
                </div>
              </UploadImage>
            </div>
          )}
        </div>
        <p className="file-tips">
          仅支持jpg、jpeg、png、gif文件，最多上传10张，大小不超过5M
        </p>
      </div>
    );
  }

  /**
   * 上传附件
   */
  _uploadImage = (result, file, data) => {
    console.log('_uploadImage---->');
    let { context } = result;
    const image = {
      image: context[0],
      status: 'done',
      fileData: data
    };
    if (context) {
      this.props.relaxProps.addImage({
        storeId: this.props.storeId,
        image
      });
    }
  };

  /**
   * 删除附件
   */
  removeImage = (index) => {
    this.props.relaxProps.removeImage({
      index,
      storeId: this.props.storeId
    });
  };

  /**
   * 上传验证
   * @returns {boolean}
   * @private
   */
  _uploadCheck = () => {
    const { orderConfirm } = this.props.relaxProps;
    const index = orderConfirm.findIndex(
      (f) => f.get('storeId') == this.props.storeId
    );
    const enclosures = orderConfirm.getIn([index, 'enclosures']);
    if (enclosures.count() >= 10) {
      Alert({ text: '最多只能上传10张图片' });
      return false;
    }
    return true;
  };
}
