import React, { Component } from "react";

import { Relax, IMap } from "plume2";
import { FormRegexUtil, noop, _ } from 'wmkit'
import { IList } from "typings/globalType";
const pay=require('../img/pay.png')
const nopay=require('../img/nopay.png')
@Relax
export default class ToolBar extends Component<any, any> {

  props: {
    relaxProps?: {
      buyerRemark: string
      sperator: boolean
      invoice: IMap
      defaultInvoiceAddr: IMap
      totalPrice: number
      goodsTotalPrice:number;
      submit: Function
    }
  }

  static relaxProps = {
    buyerRemark: 'buyerRemark',
    sperator: 'sperator',
    invoice: 'invoice',
    defaultInvoiceAddr: 'defaultInvoiceAddr',
    totalPrice: 'totalPrice',
    goodsTotalPrice:'goodsTotalPrice',
    submit: noop
  }


  render() {
    const { totalPrice ,goodsTotalPrice} = this.props.relaxProps
    
    return (
      <div className="bottom-fixed">
        <div className="order-bottom tool-bar-foot">
          <p>应付：<span><i className="iconfont icon-qian" />{_.addZero(totalPrice)}</span></p>
          {/* <button style={{width:'2rem',fontSize:'0.28rem'}} onClick={() => this._submit()}>去支付</button> */}
          {goodsTotalPrice == 0 ? 
          <img src={nopay} alt="" style={{height:'100%'}}/> 
          :<img src={pay} alt="" style={{height:'100%'}} onClick={() => this._submit()}/> }
        </div>
      </div>
    )
  }


  /**
   * 提交订单
   * @private
   */
  _submit = () => {
    const { submit } = this.props.relaxProps

    submit(false)
  }
}
