import React, { Component } from 'react';

import { Relax, IMap } from 'plume2';
import { _ } from 'wmkit';
import SkuList from './sku-list';
import Invoice from './invoice';
import Enclosure from './enclosure';
import AmountStatistics from './amount-statistics';
import Remark from './remark';

export default class StoreItem extends Component<any, any> {
  props: {
    store: IMap;
    index: number;
    single: boolean;
    couponTotal: number;
    totalCommission: number;
    storeBagsFlag: boolean;
    useCoupons: Function;
    pointDiscount:number;
  };

  render() {
    const {
      store,
      index,
      single,
      useCoupons,
      couponTotal,
      totalCommission,
      storeBagsFlag,
      pointDiscount,
    } = this.props as any;
    const { tradeItems } = store as any;
    const { tradePrice } = store as any;
    let allPointNum = 0
    
    return (
      <div>
        <div className="address-box">
        <SkuList store={store} index={index} />
        {/* 预估点数11/5 */}
        {/* {tradeItems.map((v, i) => (
          console.log('vvvvvvvvvvvvvvvvvvvvvvv'),
          console.log(v.kmPointsValue),
          // console.log(allPointNum+=v.distributionGoodsAudit),
          console.log('iiiiiiiiiiiiiiiiiiiiiiiiii'),
          <div style={{display:'none'}}>{allPointNum+=v.kmPointsValue}</div>
        ))} */}
        {localStorage.getItem('loginSaleType') == '1'?
          <div className="order-wrap" style={{padding: '16px 14px',display:'flex',justifyContent:'space-between',color:'#333'}}>
            <div>订单预估点数</div>
            <div>{_.addZero(tradePrice.kmPointsValue*(tradePrice.goodsPrice/tradePrice.totalPrice - couponTotal - pointDiscount))}</div>
          </div>:null
        }
        <Invoice
          storeId={store.supplier.storeId}
          supplierId={store.supplier.supplierId}
        />
        <Remark storeId={store.supplier.storeId} />
        </div>
        {/* <Enclosure storeId={store.supplier.storeId} /> */}
        <AmountStatistics
          store={store}
          index={index}
          single={single}
          useCoupons={useCoupons}
          couponTotal={couponTotal}
          totalCommission={totalCommission}
          storeBagsFlag={storeBagsFlag}
        />
      </div>
    );
  }
}
