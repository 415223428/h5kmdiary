import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';

import { Link } from 'react-router-dom';
import { FormSelect, history, FindArea, Switch, noop, FormItem } from 'wmkit';
import { IList } from 'typings/globalType';
import { fromJS } from 'immutable';

const INVOICE_TYPE = {
  '0': '普通发票',
  '1': '增值税专用发票',
  '-1': '不需要发票'
};

@Relax
export default class Invoice extends Component<any, any> {
  props: {
    storeId: number;
    supplierId: number;
    relaxProps?: {
      defaultAddr: IMap;
      orderConfirm: IList;

      saveSessionStorage: Function;
    };
  };

  static relaxProps = {
    invoice: 'invoice',
    orderConfirm: 'orderConfirm',

    saveSessionStorage: noop
  };

  render() {
    const { orderConfirm, defaultAddr } = this.props.relaxProps;
    const index = orderConfirm.findIndex(
      (f) => f.get('storeId') == this.props.storeId
    );
    const invoice = orderConfirm.getIn([index, 'invoice']);
    const iSwitch = orderConfirm.getIn([index, 'iSwitch']);

    return (
      <div className='span-word'>
        <div className="order-wrap">
          {iSwitch == 1 ? (
            <FormSelect
              // style={{display:'flex',alignItems:'center'}}
              labelName={'发票信息'}
              placeholder={INVOICE_TYPE[invoice.get('type')]}
              onPress={() => this._goChooseInvoice()}
            />
          ) : (
            <FormItem labelName="发票信息" placeholder="不支持开票"/>
          )}
        </div>
      </div>
    );
  }

  /**
   * 选择发票信息
   * @private
   */
  _goChooseInvoice = () => {
    const { saveSessionStorage } = this.props.relaxProps;
    history.push(`/order-invoice/${this.props.supplierId}`);
    saveSessionStorage('invoice');
  };
}
