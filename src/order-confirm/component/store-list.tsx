import React, { Component } from 'react';

import { Relax } from 'plume2';

import { IList } from 'typings/globalType';
import {
  OrderMaxPointDiscountQL,
  OrderMaxPointQL,
  OrderUsePointDiscountQL
} from '../ql';
import StoreItem from './store-item';
import { noop } from 'wmkit';

@Relax
export default class StoreList extends Component<any, any> {
  props: {
    relaxProps?: {
      stores: IList;
      couponTotal: number;
      totalCommission: number;
      storeBagsFlag: boolean;
      useCoupons: Function;
      confirmInitinvoice: Function;
      pointDiscount: number;
    };
  };

  static relaxProps = {
    stores: 'stores',
    couponTotal: 'couponTotal',
    totalCommission: 'totalCommission',
    storeBagsFlag: 'storeBagsFlag',
    useCoupons: noop,
    confirmInitinvoice: noop,
    //积分抵扣金额
    pointDiscount: OrderUsePointDiscountQL,
  };

  render() {
    const {
      stores,
      couponTotal,
      useCoupons,
      totalCommission,
      storeBagsFlag,
      pointDiscount
    } = this.props.relaxProps;
    return (
      <div>
        {stores
          .toJS()
          .map((store, index) => (
            <StoreItem
              key={index}
              store={store}
              index={index}
              single={stores.size == 1}
              useCoupons={useCoupons}
              couponTotal={couponTotal}
              totalCommission={totalCommission}
              storeBagsFlag={storeBagsFlag}
              pointDiscount={pointDiscount}
            />
          ))}
      </div>
    );
  }
}
