import React, { Component } from 'react';

import { _, history, WMkit } from 'wmkit';
import { fromJS } from 'immutable';
import { Switch } from 'antd-mobile';
import noop from 'wmkit/noop';
import {
  OrderMaxPointDiscountQL,
  OrderMaxPointQL,
  OrderUsePointDiscountQL
} from '../ql';
import { Relax, IMap } from 'plume2';
import { IList } from 'typings/globalType';

const TYPES = {
  '0': '满减优惠',
  '1': '满折优惠',
  '2': '满赠优惠'
};
@Relax
export default class AmountStatistics extends Component<any, any> {
  props: {
    index: number;
    store: any;
    single: boolean;
    couponTotal: number;
    totalCommission: number;
    storeBagsFlag: boolean;
    useCoupons: Function;
    relaxProps?: {
      couponPageInfoBack: boolean,
      confirmInit: Function;
      onChooseCoupon: Function;
      totalPoint: number;
      pointConfig: any;
      setUsePoint: Function;
      changeSwitch: Function;
      pointDiscount: number;
      maxPoint: number;
      maxPointDiscount: number;
      showPoint: boolean;
      usePoint: number;
      onSubmit: Function;
      integralInput: string;
      saveSessionStorage: Function;
      payOptions: IList;
      payType: number;
      getCoupons: Function;
      confirmInitinvoice: Function;
      fetchInvoiceSwitch: Function;
      enableCoupons: IMap;
      useCoupn: Function;
      _calcFreight: Function;
      freightFunc: Function;
      InviteMsg: any;
    };
  };
  static relaxProps = {
    couponPageInfoBack: 'couponPageInfoBack',
    totalPoint: 'totalPoint',
    pointConfig: 'pointConfig',
    showPoint: 'showPoint',
    usePoint: 'usePoint',
    integralInput: 'integralInput',
    payOptions: 'payOptions',
    payType: 'payType',
    InviteMsg: 'InviteMsg',
    useCoupn: noop,
    onChooseCoupon: noop,
    setUsePoint: noop,
    confirmInitinvoice: noop,
    confirmInit: noop,
    onSubmit: noop,
    changeSwitch: noop,
    saveSessionStorage: noop,
    maxPoint: OrderMaxPointQL,
    // 积分可抵扣的最大金额
    maxPointDiscount: OrderMaxPointDiscountQL,
    //积分抵扣金额
    pointDiscount: OrderUsePointDiscountQL,
    getCoupons: noop,
    fetchInvoiceSwitch: noop,
    enableCoupons: 'enableCoupons',
    _calcFreight: noop,
    freightFunc: noop,
  };
  componentWillReceiveProps(nextProps) {
    const { couponTotal } = this.props
    const { tradePrice } = this.props.store;
    const {
      onChooseCoupon,
      enableCoupons,
      onSubmit,
      couponPageInfoBack,
      useCoupn
    } = nextProps.relaxProps;
    if (nextProps.couponTotal || couponPageInfoBack) {
      return;
    }
    
    let stores = 0;
    let storesChosen;
    let storescouponCodeId;
    let storesstoreId;
    let storesChooseCoupon = [];

    if (!couponTotal && enableCoupons) {
      enableCoupons.get('stores').map(v=>{
      v.get('coupons')
      .map((coupon) => {
        if (coupon.get('denomination') > stores) {
          if (coupon.get('denomination') <= tradePrice.totalPrice) {
            stores = coupon.get('denomination'),
            storesChosen = !coupon.get('chosen'),
            storescouponCodeId = coupon.get('couponCodeId'),
            storesstoreId = coupon.get('storeId')
          }
        } 
      })} )
      onChooseCoupon(
        storesChosen,
        storescouponCodeId,
        storesstoreId
      );
      stores <= tradePrice.totalPrice ?
        useCoupn() :
        null
    };

    let denomination = 0;
    let chosen;
    let couponCodeId;
    let storeId;
    if (!couponTotal && enableCoupons) {
      enableCoupons
        .get('commonCoupons')
        .map((coupon) => {
          if (coupon.get('denomination') > denomination) {
            if (coupon.get('denomination') <= tradePrice.totalPrice) {
              denomination = coupon.get('denomination'),
                chosen = !coupon.get('chosen'),
                couponCodeId = coupon.get('couponCodeId'),
                storeId = coupon.get('storeId')
            }
          }
        })
        
      onChooseCoupon(
        chosen,
        couponCodeId,
        storeId
      );
      denomination <= tradePrice.totalPrice ?
        useCoupn() :
        null
    };
  }
  componentWillMount() {
    this.props.relaxProps.getCoupons();
    // this.props.relaxProps._calcFreight();
    // this.props.relaxProps.confirmInit();
    this.props.relaxProps.confirmInitinvoice();
    // this.props.relaxProps.freightFunc();
  }
  render() {
    const {
      single,
      couponTotal,
      useCoupons,
      totalCommission,
      storeBagsFlag,
    } = this.props;
    const {
      confirmInit,
      onChooseCoupon,
      pointConfig,
      totalPoint,
      maxPoint,
      maxPointDiscount,
      setUsePoint,
      pointDiscount,
      changeSwitch,
      showPoint,
      integralInput,
      payOptions,
      payType,
      enableCoupons,
      onSubmit,
      useCoupn,
      InviteMsg
    } = this.props.relaxProps;
    
    const { discountsPrice, tradePrice ,tradeItems} = this.props.store;
    const prices = (fromJS(discountsPrice) || fromJS([])).groupBy((item) =>
      item.get('type')
    );
    const opening = totalPoint
      ? _.sub(pointConfig.get('overPointsAvailable'), totalPoint) > 0
      : false;
    const openStatus = pointConfig && pointConfig.get('status');
    const payIndex = payOptions.findIndex((f) => f.get('id') == payType);
    // const distributionFlag =tradeItems[0].distributionGoodsAudit==2;
    const distributionFlag =tradeItems.map(v=>v.distributionGoodsAudit == 2)
    console.log(distributionFlag);
    console.log(JSON.stringify(InviteMsg));
    return (
      <div className="total-price address-box">
        {single && (
          <div className="pay-delivery" onClick={this._goToWays}>
            <span>支付配送</span>
            <div className="ways-item">
              <div>
                <p>{payType >= 0 && payOptions.getIn([payIndex, 'name'])}</p>
                <p>快递配送</p>
              </div>
              <i className="iconfont icon-jiantou1" />
            </div>
          </div>
        )}
        {!WMkit.isShop() &&
          !storeBagsFlag &&
          single && (
            <div className="total-list" onClick={() => useCoupons()}>
              <span style={{ flex: 1 }}>使用优惠券</span>
              <span>
                <i className="iconfont icon-qian" />
                {_.addZero(couponTotal)}
              </span>
              <i className="iconfont icon-jiantou1" />
            </div>
          )}

        {single &&
          openStatus == '1' && (
            <div className="integralBox">
              <div className="total-list integralItem">
                <div className="form-text">积分</div>
                {/* <Switch
                  checked={true}
                  disabled={opening || maxPoint == 0}
                  onChange={(e) => {
                    changeSwitch(e);
                  }}
                /> */}
              </div>

              {/* {!opening && showPoint ? ( */}
              <span>
                使用
                  <input
                  type="text"
                  className="integral-input"
                  value={integralInput}
                  onChange={(e) => setUsePoint(e.target.value, maxPoint)}
                />
                积分&nbsp;抵扣
                  <span className="new-theme-text">
                  <i className="iconfont icon-qian" />
                  {pointDiscount}
                </span>
              </span>
              {/* ) : ( */}
              <p>
                共<span className="new-theme-text">{totalPoint}</span>积分&nbsp;
                  {opening ? (
                  '达到' +
                  pointConfig.get('overPointsAvailable') +
                  '积分后可用于下单抵扣'
                ) : (
                    <span>
                      最多可用{maxPoint}积分抵扣<span className="new-theme-text">
                        <i className="iconfont icon-qian" />
                        {maxPointDiscount}
                      </span>
                    </span>
                  )}
              </p>
              {/* )} */}
              {showPoint && (
                <p>
                  共<span className="new-theme-text">{totalPoint}</span>积分&nbsp;最多可用{
                    maxPoint
                  }积分抵扣<span className="new-theme-text">
                    <i className="iconfont icon-qian" />
                    {maxPointDiscount}
                  </span>
                </p>
              )}
              {/* <div className="gray-bg" /> */}
            </div>
          )}
        {
           distributionFlag&&InviteMsg.customerId?
            <div>
              <div className="total-list">
                <span>受益人编号</span>
                {InviteMsg?InviteMsg.customerId:''}
              </div>
              <div className="total-list">
                <span>受益人手机号</span>
                {InviteMsg?WMkit._hideAccount(`${InviteMsg.customerAccount}`):''}
              </div>
            </div>
            : null
        }
        {single && (

          <div className="total-list">
            <span>配送费用</span>
            <span>
              <i className="iconfont icon-qian" />
              {_.addZero(tradePrice.deliveryPrice)}
            </span>
          </div>
        )}

        <div className="total-list">
          <span>商品金额</span>
          <span>
            <i className="iconfont icon-qian" />
            {_.addZero(tradePrice.goodsPrice)}
          </span>
        </div>
        {prices
          .map((val, key) => {
            const price = val
              .map((v) => v.get('amount'))
              .reduce((a, b) => (a += b));
            return (
              price > 0 && (
                <div className="total-list" key={key}>
                  <span>{TYPES[key]}</span>
                  <span>
                    -<i className="iconfont icon-qian" />
                    {_.addZero(price)}
                  </span>
                </div>
              )
            );
          })
          .toArray()}
        {single &&
          !WMkit.isShop() &&
          !storeBagsFlag && (
            <div className="total-list">
              <span>优惠券</span>
              <span>
                -<i className="iconfont icon-qian" />
                {_.addZero(couponTotal)}
              </span>
            </div>
          )}
        {single &&
          pointConfig &&
          pointConfig.get('status') == '1' && (
            <div className="total-list">
              <span>积分抵扣</span>
              <span>
                -<i className="iconfont icon-qian" />
                {_.addZero(pointDiscount)}
              </span>
            </div>
          )}
        <div className="total-list">
          <span>订单金额</span>
          <span className="price-color">
            <i className="iconfont icon-qian" />
            {_.addZero(tradePrice.totalPrice - couponTotal - pointDiscount)}
          </span>
        </div>
        {single &&
          WMkit.isDistributorLogin() &&
          totalCommission > 0 && (
            <div className="total-list total-rebate b-1px-t">
              <span>预计返利</span>
              <span>
                <i className="iconfont icon-qian" />
                {totalCommission && totalCommission.toFixed(2)}
              </span>
            </div>
          )}
      </div>
    );
  }

  /**
   * 支付配送方式
   */
  _goToWays = () => {
    this.props.relaxProps.saveSessionStorage('payment');
    history.push('/payment-delivery');
  };
}
