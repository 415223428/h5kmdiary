/**
 * Created by chenpeng on 2017/7/5.
 */
import { Fetch } from 'wmkit';

/**
 * 查询用户默认地址信息
 * @returns {Promise<AsyncResult<TResult>>}
 */
export const fetchCustomerDefaultAddr = () => {
  return Fetch<Result<any>>('/customer/addressinfo');
};

/**
 * 获取订单商品列表
 * @returns {Promise<Result<TResult>>}
 */
export const fetchWillBuyGoodsList = () => {
  return Fetch<Result<any>>('/trade/purchase');
};

/**
 * 提交
 * @param params
 * @returns {Promise<Result<T>>}
 */
export const commit = (params) => {
  return Fetch<Result<any>>('/trade/commit', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 在线支付是否开启
 * @returns {Promise<Result<any>>}
 */
export const fetchOnlinePayStatus = () => {
  return Fetch('/pay/gateway/isopen/H5', {
    method: 'GET'
  });
};

/**
 * 查询当前商家是否支持开发票
 * @param companyInfoIds
 */
export const fetchInvoiceSwitch = (companyInfoIds) => {
  const params = {
    companyInfoIds
  };
  
  return Fetch(`/account/invoice/switch`, {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 根据参数查询运费
 * @param params
 * @returns {Promise<Result<T>>}
 */
export const fetchFreight = (params) => {
  return Fetch('/trade/getFreight', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 查询店铺是否是有效状态
 * @returns {Promise<Result<T>>}
 */
export const fetchShopStatus = () => {
  return Fetch('/distribute/check/status');
};

/**
 * 查询会员数据
 * @returns
 */
export const fetchCustomerCenterInfo = () => {
  return Fetch('/customer/customerCenter');
};

/**
 * 查询会员数据
 * @returns
 */
export const fetchPointsConfig = () => {
  return Fetch('/pointsConfig');
};

/**
 * 0元订单批量支付
 */
export const defaultPayBatch = (tradeIds) => {
  return Fetch('/pay/default/', {
    method: 'POST',
    body: JSON.stringify({
      tradeIds
    })
  });
};

export const checkoutCoupons = (couponCodeIds) => {
  return Fetch('/coupon-code/checkout-coupons', {
    method: 'POST',
    body: JSON.stringify({
      couponCodeIds
    })
  });
};

export const getInviteName = (inviteId) =>{
  return Fetch(`/getcustomerbyid/${inviteId}`,{
    method:'POST',
  })
};

/**查询受益人账号手机
 * @param buyer 购物人
 * @param shareCustomerId 邀请人id
 */
export const getbeneficiary = (buyer,shareCustomerId) =>{
  return Fetch(`/trade/getbeneficiary`,{
    method:'POST',
    body:JSON.stringify({
      buyer,
      shareCustomerId
    })
  })
};
