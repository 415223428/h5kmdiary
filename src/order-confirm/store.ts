/**
 * Created by chenpeng on 2017/7/4.
 */
import { Store } from 'plume2';
import * as webapi from './webapi';
import moment from 'moment';
import { cache, config, Const } from 'config';
import {
  _,
  Alert,
  Confirm,
  FindArea,
  FormRegexUtil,
  history,
  storage,
  WMkit
} from 'wmkit';
import { fromJS } from 'immutable';
import wx from 'weixin-js-sdk';
import provinces from 'wmkit/area/provinces.json'
// import areas from 'wmkit/areas.json'
import areas from 'wmkit/area/areas.json'
import cities from 'wmkit/area/cities.json'

import OrderConfirmActor from './actor/order-confirm-actor';
import * as webApi from './webapi';
import { getShareUserId } from 'biz';

export default class AppStore extends Store {
  freightFunc = WMkit.delayFunc(() => {
    this._calcFreight();
  }, 500);

  bindActor() {
    return [new OrderConfirmActor()];
  }

  constructor(props) {
    super(props);
    //debug
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  /**
   * 选择支付方式
   * @param payId
   */
  onSelectPayInfo = (params) => {
    const { storeId, payId } = params;
    if (storeId) {
      const orderConfirm = this.state().get('orderConfirm');
      const index = orderConfirm.findIndex((f) => f.get('storeId') == storeId);
      const payType = orderConfirm.getIn([index, 'payType']);
      if (payId == payType) {
        return;
      }
    }
    this.dispatch('order-confirm-actor: payment', { payId, storeId });
  };

  /**
   * 确认订单初始化
   * @returns {Promise<void>}
   */
  confirmInit = async () => {
    await this.getPayOptions();
    //初始化积分信息
    await this.initPointConfig();
    //初始化商品信息
    const storeRes = (await webApi.fetchWillBuyGoodsList()) as any;

    if (storeRes.code == 'K-000000') {
      const storeBagsFlag = storeRes.context.storeBagsFlag == 1;
      if (!storeBagsFlag) {
        //检查小店分销状态(小店外购买返回true)
        const status = (await webApi.fetchShopStatus()) as any;
        if (!status.context) {
          Confirm({
            text: '很抱歉，商品已失效，请重新选择',
            okBtn: '确定',
            confirmCb: () => history.push('/user-center')
          });
          return;
        }
      }
      storeRes.context.storeBagsFlag = storeBagsFlag;
      this.dispatch('order-confirm: stores: fetch', storeRes.context);
      this.dispatch('order-confirm: price: fetch', {
        totalPrice: storeRes.context.totalPrice,
        goodsTotalPrice: storeRes.context.goodsTotalPrice,
        discountsTotalPrice: storeRes.context.discountsTotalPrice
      });
    } else if (storeRes.code == 'K-080302') {
      Confirm({
        text: '很抱歉，商品已失效，请重新选择',
        okBtn: '确定',
        confirmCb: () => history.push('/user-center')
      });
    } else {
      this.clearSessionStorage();
      Confirm({
        text: storeRes.message,
        okBtn: '确定',
        confirmCb: () => history.push('/purchase-order')
      });
    };
    //跳转返回初始化
    if (storage('session').get(cache.ORDER_CONFIRM)) {
      //选择地址页面
      const { defaultAddr, orderConfirm } = JSON.parse(
        storage('session').get(cache.ORDER_CONFIRM)
      );
      this.dispatch('order-confirm-actor: back: init', {
        orderConfirm,
        defaultAddr
      });
      this.freightFunc();
      this.clearSessionStorage();
    } else {
      this.dispatch('order-confirm: init', storeRes.context);
      this.initDefaultAddress();
      this.onSelectPayInfo({});
    }
    let couponPageInfo = null;
    // 使用优惠券页返回初始化
    if (storage('session').get(cache.ORDER_CONFIRM_COUPON)) {
      // 选择优惠券页
      couponPageInfo = JSON.parse(
        storage('session').get(cache.ORDER_CONFIRM_COUPON)
      );
      storage('session').set(
        cache.ORDER_CONFIRM_COUPON,
        JSON.stringify(this.state().toJS())
      );
      this.dispatch('set:coupon:page:back');
      this.dispatch('set:coupon:page:info', fromJS(couponPageInfo));
      storage('session').del(cache.ORDER_CONFIRM_COUPON);
    }
    // 支付配送页返回初始化
    if (storage('session').get(cache.ORDER_CONFIRM_PAYTYPE)) {
      const payment = JSON.parse(
        storage('session').get(cache.ORDER_CONFIRM_PAYTYPE)
      );
      // 选择优惠券页
      this.dispatch('order-confirm-actor:set-pay-type', payment.payType);
      storage('session').del(cache.ORDER_CONFIRM_PAYTYPE);
    }

    // 跳转后处理优惠券信息
    this.dispatch('calc:coupon:info');
    this.initTotalPoint();
    if (storage('session').get(cache.ORDER_POINT)) {
      const { usePoint, integralInput, deductionAmount } = JSON.parse(
        storage('session').get(cache.ORDER_POINT)
      );
      //如果选择了优惠券并且抵扣之后的金额大于订单支付最大金额(不包括运送费)
      if (
        couponPageInfo &&
        _.sub(couponPageInfo.couponTotalPrice, deductionAmount) > 0
      ) {
        storage('session').del(cache.ORDER_POINT);
      } else {
        this.dispatch('order-confirm-actor:changeSwitch');
        this.dispatch('order-confirm-actor:setUsePoint', {
          usePoint,
          integralInput
        });
      }
    }
    this.fetchInvoiceSwitch(
      storeRes.context.tradeConfirmItems.map((m) => m.supplier.supplierId)
    );
  };


  /**
 * 确认订单初始化
 * @returns {Promise<void>}
 */
  confirmInitinvoice = async () => {
    let couponPageInfo = null;
    // 使用优惠券页返回初始化
    if (storage('session').get(cache.ORDER_CONFIRM_COUPON)) {
      // 选择优惠券页
      couponPageInfo = JSON.parse(
        storage('session').get(cache.ORDER_CONFIRM_COUPON)
      );
      storage('session').set(
        cache.ORDER_CONFIRM_COUPON,
        JSON.stringify(this.state().toJS())
      );
      this.dispatch('set:coupon:page:back');
      this.dispatch('set:coupon:page:info', fromJS(couponPageInfo));
      storage('session').del(cache.ORDER_CONFIRM_COUPON);
    }
    // 支付配送页返回初始化
    if (storage('session').get(cache.ORDER_CONFIRM_PAYTYPE)) {
      const payment = JSON.parse(
        storage('session').get(cache.ORDER_CONFIRM_PAYTYPE)
      );
      // 选择优惠券页
      this.dispatch('order-confirm-actor:set-pay-type', payment.payType);
      storage('session').del(cache.ORDER_CONFIRM_PAYTYPE);
    }

    // 跳转后处理优惠券信息
    this.dispatch('calc:coupon:info');
    this.initTotalPoint();
    if (storage('session').get(cache.ORDER_POINT)) {
      const { usePoint, integralInput, deductionAmount } = JSON.parse(
        storage('session').get(cache.ORDER_POINT)
      );
      //如果选择了优惠券并且抵扣之后的金额大于订单支付最大金额(不包括运送费)
      if (
        couponPageInfo &&
        _.sub(couponPageInfo.couponTotalPrice, deductionAmount) > 0
      ) {
        storage('session').del(cache.ORDER_POINT);
      } else {
        this.dispatch('order-confirm-actor:changeSwitch');
        this.dispatch('order-confirm-actor:setUsePoint', {
          usePoint,
          integralInput
        });
      }
    }
  };

  /**
   * 计算运费
   */
  _calcFreight = () => {
    //1.组装收货地址
    const defaultAddr = this.state().get('defaultAddr');
    if (!defaultAddr || !defaultAddr.get('provinceId')) {
      return;
    }
    let consignee = {
      provinceId: defaultAddr.get('provinceId'),
      cityId: defaultAddr.get('cityId')
    };
    const orderConfirm = this.state().get('orderConfirm');

    let checkGoodsInfos = this.state().getIn([
      'couponPageInfo',
      'checkGoodsInfos'
    ]);

    //2.组装完整的请求参数(用于计算运费)
    const tradeParamsList = this.state()
      .get('stores')
      .map((st, i) => {
        const amountList = st.get('discountsPrice');
        let amountTotal = 0;
        if (amountList && amountList.size) {
          amountTotal = amountList.reduce(
            (a, b) => _.add(a, b.get('amount')),
            0
          );
        }

        if (checkGoodsInfos) {
          st = st.update('tradeItems', (skus) => {
            return skus.map((sku) => {
              const checkGoodsInfo = checkGoodsInfos.find(
                (item) => item.get('goodsInfoId') == sku.get('skuId')
              );
              // 优惠总价追加优惠券金额
              amountTotal = _.add(
                amountTotal,
                _.sub(sku.get('splitPrice'), checkGoodsInfo.get('splitPrice'))
              );
              // sku修改为优惠券后的均摊价
              return sku.set('splitPrice', checkGoodsInfo.get('splitPrice'));
            });
          });
        }

        return {
          supplier: {
            storeId: st.get('supplier').get('storeId'),
            freightTemplateType: st.get('supplier').get('freightTemplateType')
          },
          consignee,
          deliverWay: orderConfirm.get(i).get('deliverWay'),
          tradePrice: {
            totalPrice: _.sub(
              st.get('tradePrice').get('goodsPrice'),
              amountTotal
            )
          },
          oldTradeItems: st.get('tradeItems').toJS(),
          oldGifts: st.get('gifts') ? st.get('gifts').toJS() : []
        };
      });

    //3.根据返回的各店铺运费结果,更新状态
    webApi.fetchFreight(tradeParamsList).then((r: any) => {
      if (this.state().get('grouponFreeDelivery')) {
        // 如果拼团活动是包邮活动
        r.context.map((item) => (item.deliveryPrice = 0));
      }
      this.dispatch('order-confirm-actor:changeDeliverFee', r.context);
    });
  };

  /**
   * 清除SessionStorage
   */
  clearSessionStorage = () => {
    storage('session').del(cache.ORDER_CONFIRM);
  };

  /**
   * 存储SessionStorage
   * @param comeFrom 来自哪里
   */
  saveSessionStorage = (comeFrom) => {
    const { defaultAddr, orderConfirm } = this.state().toJS();
    storage('session').set(cache.ORDER_CONFIRM, {
      defaultAddr,
      orderConfirm,
      comeFrom
    });
    this.savePayTypeCache();
    this.saveCouponCache();
    this.savePointCache();
  };

  /**
   * 保存买家备注
   * @param remark
   */
  saveBuyerRemark = ({ remark, storeId }) => {
    this.dispatch('order-confirm-actor: remark', {
      remark,
      storeId
    });
  };

  /**
   * 初始化收货地址
   * @returns {Promise<void>}
   */
  initDefaultAddress = async () => {
    const addrRes = await webApi.fetchCustomerDefaultAddr();
    this.dispatch('order-confirm-actor: addr: fetch', addrRes.context);
    this.freightFunc();
  };



  /**
 * 查询省
 * @param code
 * @returns {string}
 */
  findProviceName(code: string) {
    for (let p of provinces) {
      if (p.code == code) {
        return p.name
      }
    }
    return ""
  }


  /**
   * 查询区
   * @param code
   * @returns {string}
   */
  findArea(code: string) {
    for (let a of areas) {
      if (code == a.code) {
        return a.name
      }
    }
    return ''
  }


  /**
   * 查询市
   * @param code
   * @returns {string}
   */
  findCity(code: string) {
    for (let c of cities) {
      if (code == c.code) {
        return c.name
      }
    }
    return ''
  }

  /**
   * 提交订单
   * @returns {Promise<void>}
   */
  submit = async (forceCommit) => {
    const {
      defaultAddr,
      orderConfirm,
      commonCodeId,
      usePoint,
      integralInput,
      payType
    } = this.state().toJS();
    let storeCommitInfoList = [];
    if (!defaultAddr || !defaultAddr.deliveryAddressId) {
      Alert({ text: '请选择收货地址!' });
      return;
    }
    const addrDetail =
      FindArea.addressInfo(
        defaultAddr ? defaultAddr.provinceId : '',
        defaultAddr ? defaultAddr.cityId : '',
        defaultAddr ? defaultAddr.areaId : ''
      ) + (defaultAddr ? defaultAddr.deliveryAddress : '');

    orderConfirm.forEach((o, i) => {
      if (
        !FormRegexUtil(o.buyerRemark.trim(), `订单${i}订单备注`, {
          minLength: 0
        })
      ) {
        return;
      }
      if (o.payType === -1) {
        Alert({
          text: `订单${i}请选择支付方式!`
        });
        return;
      }
      if (
        o.invoice.type != -1 &&
        o.sperator &&
        (!o.defaultInvoiceAddr || !o.defaultInvoiceAddr.deliveryAddressId)
      ) {
        Alert({
          text: `订单${i}请选择发票收货地址!`
        });
        return;
      }

      const deliveryAddress = defaultAddr ? addrDetail : '';
      const invoiceAddrDetail = o.defaultInvoiceAddr
        ? FindArea.addressInfo(
          o.defaultInvoiceAddr ? o.defaultInvoiceAddr.provinceId : '',
          o.defaultInvoiceAddr ? o.defaultInvoiceAddr.cityId : '',
          o.defaultInvoiceAddr ? o.defaultInvoiceAddr.areaId : ''
        ) + (o.defaultInvoiceAddr ? o.defaultInvoiceAddr.deliveryAddress : '')
        : '';

      storeCommitInfoList.push({
        storeId: o.storeId, // 店铺Id
        payType: payType, //支付类型，必传
        invoiceType: o.invoice.type, //开票类型，必传 0：普通发票 1：增值税专用发票 -1：无
        generalInvoice:
          o.invoice.type == 0
            ? {
              flag: o.invoice.flag,
              title: o.invoice.title,
              identification: o.invoice.identification
            }
            : {}, //普通发票与增票参数至少一项必传
        specialInvoice:
          o.invoice.type == 1
            ? {
              id: o.VATInvoice.customerInvoiceId
            }
            : {}, //增值税专用发票与普票至少一项必传
        specialInvoiceAddress: o.sperator, //是否单独的收货地址
        invoiceAddressId:
          o.invoice.type != '-1' && o.sperator
            ? o.defaultInvoiceAddr.deliveryAddressId
            : defaultAddr.deliveryAddressId, //发票的收货地址ID,必传
        invoiceAddressDetail:
          o.invoice.type != '-1' && o.sperator
            ? invoiceAddrDetail
            : deliveryAddress, //收货地址详细信息（不包含省市区）
        invoiceAddressUpdateTime:
          o.invoice.type != '-1' && o.sperator
            ? o.defaultInvoiceAddr.updateTime
              ? moment(o.defaultInvoiceAddr.updateTime).format(
                Const.SECONDS_FORMAT
              )
              : null
            : defaultAddr.updateTime
              ? moment(defaultAddr.updateTime).format(Const.SECONDS_FORMAT)
              : null,
        invoiceProjectId: o.invoice.type != '-1' ? o.invoice.projectKey : '', //开票项目id，必传
        invoiceProjectName: o.invoice.type != '-1' ? o.invoice.projectName : '', //开票项目名称，必传
        invoiceProjectUpdateTime: o.invoice.projectUpdateTime
          ? moment(o.invoice.projectUpdateTime).format(Const.SECONDS_FORMAT)
          : null, //开票项目修改时间
        buyerRemark: o.buyerRemark, //订单备注
        encloses: o.enclosures
          .filter((v) => v.status == 'done')
          .map((v) => v.image)
          .join(','), //附件, 逗号隔开
        deliverWay: o.deliverWay, //配送方式，默认快递
        couponCodeId: o.couponCodeId // 选择的店铺优惠券id
      });
    });
    if (integralInput && integralInput != '') {
      let reg = /^\d+$/;
      if (!reg.test(integralInput)) {
        Alert({
          text: '请填写正确的积分!'
        });
        return;
      }
    }
    if (this.findArea(defaultAddr.areaId) == '' ||
      this.findCity(defaultAddr.cityId) == '' ||
      this.findProviceName(defaultAddr.provinceId) == '') {
      Alert({
        text: '请填写正确的地址!'
      });
      return;
    }
    const params = {

      consigneeId: defaultAddr.deliveryAddressId,
      provinceName: this.findProviceName(defaultAddr.provinceId),
      cityName: this.findCity(defaultAddr.cityId),
      areaName: this.findArea(defaultAddr.areaId),
      consigneeAddress: addrDetail,
      consigneeUpdateTime: defaultAddr.updateTime
        ? moment(defaultAddr.updateTime).format(Const.SECONDS_FORMAT)
        : null,
      storeCommitInfoList,
      commonCodeId, //订单来源 //收货地址id，必传 //收货地址详细信息(包含省市区)，必传 //收货地址修改时间
      orderSource: 'WECHAT', // 需要校验营销活动
      forceCommit,
      shareUserId: getShareUserId()
    };
    if (_.sub(usePoint, 0) > 0) {
      params['points'] = usePoint;
    }

    wx.miniProgram.getEnv(function (res) {
      (window as any).isMiniProgram = res.miniprogram;
      if (res.miniprogram) {
        params.orderSource = 'LITTLEPROGRAM';
      }
    });

    const { code, message, context } = (await webApi.commit(params)) as any;
    if (code == 'K-000000') {
      //下单成功,清除
      this.clearSessionStorage();
      storage('session').del(cache.ORDER_CONFIRM_COUPON);

      // 0元订单直接支付
      const totalPrice = context.map((i) => i.price).reduce((a, b) => a + b, 0);

      if (totalPrice == 0) {
        // 0元订单且订单审核状态为不需要审核时直接跳转支付成功页
        let directPayFlag = context && context[0].tradeState.auditState == 'CHECKED';
        if (directPayFlag) {
          const res = await webApi.defaultPayBatch(
            context.map((item) => item.tid)
          );
          if (res.code == 'K-000000') {
            //跳转到支付成功页
            history.push({
              pathname: `/pay-success/undefined/${context[0].parentTid}`
            });
          } else {
            Alert({ text: res.message });
          }
          return;
        }
      }

      // 当选择在线支付，且每个单子都为已审核、先款后货时，进行合并支付
      const payType = this.state().get('payType');
      let directPayFlag = true;
      if (payType == 0 && context && context.length > 0) {
        for (let i = 0; i < context.length; i++) {
          if (
            context[i].tradeState.auditState !== 'CHECKED' ||
            context[i].paymentOrder !== 'PAY_FIRST'
          ) {
            directPayFlag = false;
            break;
          }
        }
        if (directPayFlag) {
          history.replace({
            pathname: '/pay-online',
            state: { results: context }
          });
          return;
        }
      }

      history.replace({
        pathname: '/order-confirm-success',
        state: { results: context }
      });
    } else if (code == 'K-999999') {
      Confirm({
        title: '优惠失效提醒',
        text: message,
        cancelBtn: '重新下单',
        okBtn: '继续下单',
        confirmCb: () => this.submit(true),
        cancel: () => history.go(-1)
      });
    } else if (code == 'K-080301' || code == 'K-080302') {
      Confirm({
        text: '很抱歉，商品已失效，请重新选择',
        okBtn: '确定',
        confirmCb: () => history.push('/user-center')
      });
    } else if (code == 'K-010208') {
      Confirm({
        text: '当前积分不足',
        okBtn: '确定',
        confirmCb: () => history.push('/purchase-order')
      });
    } else {
      Confirm({
        text: message,
        okBtn: '确定',
        confirmCb: () => history.push('/purchase-order')
      });
    }
  };

  /**
   * 上传附件
   */
  addImage = ({ image, storeId }) => {
    this.dispatch('order-confirm-actor: addImage', { image, storeId });
  };

  /**
   * 删除附件
   */
  removeImage = ({ index, storeId }) => {
    this.dispatch('order-confirm-actor: removeImage', { index, storeId });
  };

  /**
   * 获取可用支付选项
   */
  getPayOptions = async () => {
    const { code, context } = await webApi.fetchOnlinePayStatus();
    if (code === config.SUCCESS_CODE && context == true) {
      const data = [
        { id: '0', name: '在线支付' },
        { id: '1', name: '线下支付' }
      ];
      this.dispatch('order-confirm-actor: options', fromJS(data));
    }
  };

  /**
   * 查询是否支持开票
   */
  fetchInvoiceSwitch = async (companyInfoIds) => {
    const { code, message, context } = (await webApi.fetchInvoiceSwitch(
      companyInfoIds
    )) as any;

    if (code == 'K-000000') {
      this.dispatch('order-confirm-actor: invoice: switch', fromJS(context));
    } else {
      message.error(message);
    }
  };

  /**
   * 使用优惠券
   */
  useCoupons = () => {
    this.saveSessionStorage('coupon');
    const coupons = this.state().get('coupons');
    const couponPageInfo = this.state().get('couponPageInfo');
    const orderConfirm = this.state().get('orderConfirm');
    // console.log('传值');
    // console.log(coupons);
    // console.log(orderConfirm.map((item) => item.get('storeId')));
    history.push({
      pathname: '/use-coupon',
      state: {
        coupons,
        couponPageInfo,
        storeIds: orderConfirm.map((item) => item.get('storeId'))
      }
    });
  };

  /**
   * 初始化积分相关数据
   * @returns {Promise<void>}
   */
  initPointRelateInfo = async () => {
    const res = await Promise.all([
      webApi.fetchCustomerCenterInfo(),
      webApi.fetchPointsConfig()
    ]);
    if (
      res[0].code == config.SUCCESS_CODE &&
      res[1].code == config.SUCCESS_CODE
    ) {
      this.transaction(() => {
        this.dispatch(
          'order-confirm-actor:initTotalPoint',
          (res[0].context as any).pointsAvailable
        );
        this.dispatch(
          'order-confirm-actor:initpointConfig',
          fromJS(res[1].context)
        );
      });
    }
  };

  /**
   * 订单提交页切换是否使用积分
   */
  changeSwitch = (useFlag: boolean) => {
    //如果关闭积分,清除缓存
    if (!useFlag) {
      storage('session').del(cache.ORDER_POINT);
    }
    this.dispatch('order-confirm-actor:changeSwitch');
  };

  /**
   * 保存使用积分
   *
   * @param integralInput 输入的积分
   * @param pointWorth 积分转换单位
   */
  setUsePoint = (integralInput, maxPoint) => {
    // 使用的积分
    let usePoint = 0;
    if (integralInput != '') {
      let reg = /^\d+$/;
      if (!reg.test(integralInput)) {
        Alert({
          text: '请填写正确的积分！'
        });
        this.dispatch('order-confirm-actor:setUsePoint', {
          usePoint,
          integralInput
        });
        return;
      }
    }
    usePoint = integralInput;
    // 如果大于最大限制，自动已最大计算
    if (integralInput > maxPoint) {
      usePoint = maxPoint;
      integralInput = maxPoint.toString();
    }
    this.dispatch('order-confirm-actor:setUsePoint', {
      usePoint,
      integralInput
    });
  };

  /**
   * 初始化积分余额
   */
  initTotalPoint = async () => {
    const { code, context } = await webApi.fetchCustomerCenterInfo();
    if (code === config.SUCCESS_CODE) {
      this.dispatch(
        'order-confirm-actor:initTotalPoint',
        (context as any).pointsAvailable
      );
    }
  };
  /**
   * 初始化积分设置
   */
  initPointConfig = async () => {
    const { code, context } = await webApi.fetchPointsConfig();
    if (code === config.SUCCESS_CODE) {
      this.dispatch('order-confirm-actor:initpointConfig', fromJS(context));
    }
  };

  /**
   * 保存支付方式缓存
   */
  savePayTypeCache = () => {
    const { payType, openGroupon } = this.state().toJS();
    if (payType != null) {
      storage('session').set(
        cache.ORDER_CONFIRM_PAYTYPE,
        JSON.stringify({ payType, openGroupon })
      );
    }
  };

  /**calc:coupon:info
   * 保存优惠券缓存
   */
  saveCouponCache = () => {
    const couponPageInfo = this.state().get('couponPageInfo');
    if (couponPageInfo != null) {
      storage('session').set(
        cache.ORDER_CONFIRM_COUPON,
        JSON.stringify(couponPageInfo.toJS())
      );
    }
  };

  /**
   * 保存积分缓存
   */
  savePointCache = () => {
    //当前按钮选中的状态的时候
    if (this.state().get('showPoint')) {
      //存储缓存
      storage('session').set(
        cache.ORDER_POINT,
        JSON.stringify({
          usePoint: this.state().get('usePoint'),
          integralInput: this.state().get('integralInput'),
          deductionAmount: _.sub(
            this.state().get('totalPrice'),
            this.state().get('totalDeliveryPrice')
          )
        })
      );
    }
  };

  /**
   * 初始化优惠券
   */
  getCoupons = () => {
    // this.saveSessionStorage('coupon');
    const coupons = this.state().get('coupons');
    const couponPageInfo = this.state().get('couponPageInfo');
    const orderConfirm = this.state().get('orderConfirm');
    this.dispatch('couponInit', {
      coupons,
      storeIds: orderConfirm.map((item) => item.get('storeId')),
      couponPageInfo
    });
  };

  /**
 * 选择优惠券
 */
  onChooseCoupon =async (flag, couponCodeId, storeId) => {
    if (storeId == -1) {
      if (flag) {
        // 1.如果是选择平台券
        this.dispatch('choose:common:coupon', couponCodeId);
      } else {
        // 2.如果是取消选择平台券
        this.dispatch('clean:common:coupon');
      }
    } else {
      // 3.设置店铺券
      this.dispatch('choose:store:coupon', { flag, couponCodeId, storeId });
    }

    // 4.根据已选的券，查询不满足门槛的平台券，以及优惠券优惠总价
    const enableCoupons = this.state()
      .get('enableCoupons')
      .toJS();
    let couponCodeIds = [];
    // 已选平台券
    couponCodeIds = couponCodeIds.concat(
      enableCoupons.commonCoupons
        .filter((c) => c.chosen == true)
        .map((c) => c.couponCodeId)
    );

    let storeCoupons = [];
    enableCoupons.stores.forEach(
      (store) => (storeCoupons = storeCoupons.concat(store.coupons))
    );

    // 已选店铺券
    couponCodeIds = couponCodeIds.concat(
      storeCoupons.filter((c) => c.chosen == true).map((c) => c.couponCodeId)
    );
    const res = (await webapi.checkoutCoupons(couponCodeIds)) as any;
    if (res.code == config.SUCCESS_CODE) {
      // 5.重新设置选择的平台券和优惠券优惠总价
      let { unreachedIds, couponTotalPrice, checkGoodsInfos } = res.context;
      const chosenCommon = this.state()
        .getIn(['enableCoupons', 'commonCoupons'])
        .find((c) => c.get('chosen') == true);
      this.dispatch('clean:common:coupon');
      this.dispatch('set:unreached:ids', {
        unreachedIds: fromJS(unreachedIds),
        couponTotalPrice,
        checkGoodsInfos: fromJS(checkGoodsInfos)
      });
      if (
        chosenCommon &&
        !unreachedIds.includes(chosenCommon.get('couponCodeId'))
      ) {
        this.dispatch('choose:common:coupon', chosenCommon.get('couponCodeId'));
      }
    }
  };
  //获取自动选择优惠券信息
  useCoupn = async () => {
    const couponPageInfo = this.state().toJS()
    await this.dispatch('set:coupon:page:info', fromJS(couponPageInfo));
    await this.dispatch('calc:coupon:set');
    // this._calcFreight();
  }

  onSubmit = async () => {
    storage('session').set(
      cache.ORDER_CONFIRM_COUPON,
      JSON.stringify(this.state().toJS())
    );
  };


  getInviteName = async () => {
    const customerId = sessionStorage.getItem(cache.INVITEE_ID);
    // if(customerId){
    const { code, context } = await webApi.getInviteName(customerId);
    if (code == 'K-000000') {
      this.dispatch('order:customerMsg', context)
    }
  }

  getbeneficiary = async () => {
    const inviteId = sessionStorage.getItem(cache.INVITEE_ID)||'';
    const buyer:any = localStorage.getItem(cache.LOGIN_DATA)||'';
    const res = await webApi.getbeneficiary(JSON.parse(buyer).customerId,inviteId||'');
    this.dispatch('order:customerMsg', res.context);
    }

}
