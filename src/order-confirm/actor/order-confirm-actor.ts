import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';
import { _ } from 'wmkit';
import { Const } from 'config';

/**
 * Created by chenpeng on 2017/7/4.
 */
export default class OrderConfirmActor extends Actor {
  defaultState() {
    return {
      couponPageInfoBack: false,
      defaultAddr: {}, //默认地址
      payOptions: [{ id: '1', name: '线下支付' }], // 可用的支付方式列表 [0：在线支付   1：线下转账]
      orderConfirm: [], // 订单确认项目 多个店铺多个确认项目
      stores: [], // 店铺列表
      totalPrice: 0, //订单总价
      goodsTotalPrice: 0, //商品总价
      discountsTotalPrice: 0, //优惠总价
      totalDeliveryPrice: 0, //配送费用总价
      coupons: [], // 优惠券列表
      couponPageInfo: null, // 使用优惠券页信息
      commonCodeId: null, // 使用的平台优惠券
      couponTotal: null, // 优惠券优惠总额
      pointConfig: fromJS({}), //积分配置
      totalPoint: 0, //会员总积分
      showPoint: false, //打开积分按钮
      usePoint: 0, //使用积分数
      usePointExtra: 0, //使用积分抵扣金额
      openGroupon: null, // 是否开团购买
      grouponFreeDelivery: null, // 拼团活动是否包邮
      payType: 0,// 支付方式
      //说明文档的显示隐藏
      showHelp: false,

      // 页签key（0:可用优惠券 1:不可用优惠券）
      tabKey: 0,

      // 可用优惠券
      enableCoupons: {
        //平台优惠券
        commonCoupons: [],
        //按店铺分组优惠券，结构
        // [
        //   {
        //     storeId: '',
        //     storeName: '',
        //     coupons: []
        //   }
        // ]
        stores: []
      },
      enableCount: 0,

      // 确认订单中的店铺顺序
      storeIds: [],

      // 不可用优惠券
      disableCoupons: {
        unReachPrice: [],
        noAvailableSku: [],
        unReachTime: []
      },
      disableCount: 0,

      // 选择后，没有达到门槛的平台券id
      unreachedIds: [],
      // 优惠券优惠总价
      couponTotalPrice: 0,
      InviteMsg: {},
    };
  }

  /**
   * 存储会员默认地址,没有则获取第一个
   * @param state
   * @param addr
   * @returns {Map<string, V>}
   */
  @Action('order-confirm-actor: addr: fetch')
  saveAddr(state: IMap, addr) {
    return state.set('defaultAddr', fromJS(addr));
  }

  /**
   * 存储增值税专用发票信息
   * @param state
   * @param invoice
   * @returns {Map<string, V>}
   */
  @Action('order-confirm: invoice: fetch')
  fetchInvoice(state: IMap, { invoice, storeId }) {
    const index = state
      .get('orderConfirm')
      .findIndex((f) => f.get('storeId') == storeId);
    return state.setIn(['orderConfirm', index, 'VATInvoice'], fromJS(invoice));
  }

  /**
   * 店铺列表
   * @param state
   * @param stores
   * @returns {Map<string, V>}
   */
  @Action('order-confirm: stores: fetch')
  fetchOrderStores(
    state: IMap,
    {
      tradeConfirmItems,
      couponCodes,
      shopName,
      totalCommission,
      inviteeName,
      storeBagsFlag,
      openGroupon,
      grouponFreeDelivery
    }
  ) {
    return state
      .set('stores', fromJS(tradeConfirmItems))
      .set('coupons', fromJS(couponCodes))
      .set('shopName', shopName)
      .set('totalCommission', totalCommission)
      .set('inviteeName', inviteeName)
      .set('storeBagsFlag', storeBagsFlag)
      .set('openGroupon', openGroupon)
      .set('grouponFreeDelivery', grouponFreeDelivery);
  }

  /**
   * 提交确认项初始化
   * @param state
   */
  @Action('order-confirm: init')
  orderConfirmInit(state: IMap, stores) {
    const tradeConfirmItems = stores.tradeConfirmItems;
    return state.set(
      'orderConfirm',
      fromJS(
        tradeConfirmItems.map((m) => {
          return {
            storeId: m.supplier.storeId, //店铺Id
            supplierId: m.supplier.supplierId, //商家Id
            deliverWay: '1', //配送方式 0：其他   1：快递
            payType: -1, //支付方式 0：在线支付   1：线下转账
            buyerRemark: '', //买家备注
            iSwitch: 0, // 是否允许开票 0:不允许  1:允许
            invoice: {
              type: -1, //类型 0：普通发票 1：增值税专用发票 -1：无
              flag: '1', //0:个人 1:单位
              title: '', //抬头
              identification: '', //纳税人识别号
              projectKey: '00000000000000000000000000000000', //开票项目
              projectName: '明细' //开票项目名称
            },
            sperator: false, //是否使用单独发票收货地址
            enclosures: [], //订单附件
            defaultInvoiceAddr: {}, //默认发票收货地址
            VATInvoice: {} //增值税专用发票
          };
        })
      )
    );
  }

  /**
   * 存储买家备注
   * @param state
   * @param remark
   * @returns {IMap}
   */
  @Action('order-confirm-actor: remark')
  saveRemark(state: IMap, { remark, storeId }) {
    const index = state
      .get('orderConfirm')
      .findIndex((f) => f.get('storeId') == storeId);
    return state.setIn(['orderConfirm', index, 'buyerRemark'], remark);
  }

  /**
   * 返回初始化
   * @param state
   * @param defaultAddr
   * @param orderConfirm
   * @returns {IMap}
   */
  @Action('order-confirm-actor: back: init')
  backInit(state: IMap, { defaultAddr, orderConfirm }) {
    return state
      .set('defaultAddr', fromJS(defaultAddr))
      .set('orderConfirm', fromJS(orderConfirm));
  }

  /**
   * 修改各店铺运费/应付金额,修改所有订单总运费/总应付金额
   * @param state
   * @param freightList
   * @returns {Map<string, any>}
   */
  @Action('order-confirm-actor:changeDeliverFee')
  changeDeliverFee(state: IMap, freightList) {
    const totalDeliveryPrice = freightList.reduce(
      (a, b) => _.add(a, b.deliveryPrice),
      0
    );

    return state
      .set(
        'stores',
        state.get('stores').map((st, i) => {
          let discountsTotalPrice = 0;
          const discountsPriceList = st.get('discountsPrice');
          if (discountsPriceList && discountsPriceList.size) {
            discountsTotalPrice = discountsPriceList.reduce(
              (a, b) => _.add(a, b.get('amount')),
              0
            );
          }
          const deliveryPrice = freightList[i].deliveryPrice;
          return st.update('tradePrice', (tradePrice) =>
            tradePrice
              .set('deliveryPrice', deliveryPrice)
              .set(
                'totalPrice',
                _.add(
                  _.sub(tradePrice.get('goodsPrice'), discountsTotalPrice),
                  deliveryPrice
                )
              )
          );
        })
      )
      .set(
        'totalPrice',
        _.sub(
          _.add(
            _.sub(
              state.get('goodsTotalPrice'),
              state.get('discountsTotalPrice')
            ),
            totalDeliveryPrice
          ),
          state.get('usePointExtra')
        )
      )
      .set('totalDeliveryPrice', totalDeliveryPrice);
  }

  /**
   * 上传附件
   */
  @Action('order-confirm-actor: addImage')
  addImage(state: IMap, { image, storeId }) {
    const index = state
      .get('orderConfirm')
      .findIndex((f) => f.get('storeId') == storeId);
    return state.setIn(
      ['orderConfirm', index, 'enclosures'],
      state.getIn(['orderConfirm', index, 'enclosures']).push(fromJS(image))
    );
  }

  /**
   * 删除附件
   */
  @Action('order-confirm-actor: removeImage')
  removeImage(state: IMap, { index, storeId }) {
    const i = state
      .get('orderConfirm')
      .findIndex((f) => f.get('storeId') == storeId);
    return state.setIn(
      ['orderConfirm', i, 'enclosures'],
      state.getIn(['orderConfirm', i, 'enclosures']).remove(index)
    );
  }

  /**
   * 设置支付方式
   * @param state
   * @param status true:选中
   * @returns {Map<K, V>}
   */
  @Action('order-confirm-actor: payment')
  payType(state: IMap, { payId, storeId }) {
    if (storeId) {
      const index = state
        .get('orderConfirm')
        .findIndex((f) => f.get('storeId') == storeId);
      state = state.setIn(['orderConfirm', index, 'payType'], payId);
    } else {
      let pId = state.get('payOptions').count() > 1 ? '0' : '1';
      if (payId == 1 || payId == 0) {
        pId = payId;
      }
      const orderConfirm = state
        .get('orderConfirm')
        .map((o) => o.set('payType', pId));
      state = state.set('orderConfirm', orderConfirm);
    }
    return state;
  }

  /**
   * 设置支付方式
   * @param state
   * @param status true:选中
   * @returns {Map<K, V>}
   */
  @Action('order-confirm-actor: options')
  payOptions(state: IMap, options) {
    return state.set('payOptions', options);
  }

  /**
   * 初始化是否支持开票
   */
  @Action('order-confirm-actor: invoice: switch')
  initInvoiceSwitch(state: IMap, params) {
    const orderConfirm = state.get('orderConfirm').map((o) => {
      const index = params.findIndex(
        (p) => p.get('companyInfoId') == o.get('supplierId')
      );
      if (index >= 0) {
        o = o.set('iSwitch', params.getIn([index, 'supportInvoice']));
      }
      return o;
    });
    return state.set('orderConfirm', orderConfirm);
  }

  /**
   * 订单/商品/优惠 总价
   * @param state
   * @param param1
   */
  @Action('order-confirm: price: fetch')
  fetchPrice(
    state: IMap,
    { totalPrice, goodsTotalPrice, discountsTotalPrice }
  ) {
    return state
      .set('totalPrice', totalPrice)
      .set('goodsTotalPrice', goodsTotalPrice)
      .set('discountsTotalPrice', discountsTotalPrice);
  }

  /**
   * 接收使用优惠券页的数据
   */
  @Action('set:coupon:page:info')
  setCouponPageInfo(state, pageInfo) {
    return state.set('couponPageInfo', pageInfo);
  }

  @Action('set:coupon:page:back')
  setCouponPageInfoBack(state) {
    return state.set('couponPageInfoBack', true);
  }

  /**
   * 根据优惠券页信息设置优惠券相关信息
   */
  @Action('calc:coupon:info')
  calcCouponInfo(state) {
    const pageInfo = state.get('couponPageInfo');

    let stores = state.get('stores');
    if (!pageInfo) return state;

    // 设置平台券id
    const chosenCommon = pageInfo
      .getIn(['enableCoupons', 'commonCoupons'])
      .find((c) => c.get('chosen') == true);

    if (chosenCommon) {
      state = state.set('commonCodeId', chosenCommon.get('couponCodeId'));
    }

    // 设置店铺券id
    state = state.update('orderConfirm', (items) =>
      items.map((item) => {
        const store = pageInfo
          .getIn(['enableCoupons', 'stores'])
          .find((store) => store.get('storeId') == item.get('storeId'));
        if (!store) return item;
        const chosenCoupon = store
          .get('coupons')
          .find((coupon) => coupon.get('chosen') == true);
        item = item.set('couponCodeId', null);
        if (!chosenCoupon) return item;
        return item.set('couponCodeId', chosenCoupon.get('couponCodeId'));
      })
    );

    // 设置优惠总金额
    const couponTotalPrice = pageInfo.get('couponTotalPrice');
    if (couponTotalPrice) {
      const discountsPrice = state.get('discountsTotalPrice');
      state = state
        .set('couponTotal', couponTotalPrice)
        .set('discountsTotalPrice', discountsPrice + couponTotalPrice);
      // 先计算一次总价，如果有运费的话还会再计算一次
      state = state.set(
        'totalPrice',
        _.sub(
          _.sub(state.get('goodsTotalPrice'), state.get('discountsTotalPrice')),
          state.get('usePointExtra')
        )
      );
    }
    return state.set('stores', stores);
  }
  /**
   * 初始化用户总积分
   *
   * @param state
   * @param totalPoint
   * @returns {*}
   */
  @Action('order-confirm-actor:initTotalPoint')
  initTotalPoint(state, totalPoint) {
    return state.set('totalPoint', totalPoint);
  }

  /**
   * 初始化积分设置
   */
  @Action('order-confirm-actor:initpointConfig')
  initPointSet(state, pointConfig) {
    return state.set('pointConfig', pointConfig);
  }

  /**
   * 订单确认页切换是否使用积分
   * @param state
   */
  @Action('order-confirm-actor:changeSwitch')
  changeSwitch(state) {
    //切换时重置订单总额
    const totalPrice = _.add(
      _.sub(state.get('goodsTotalPrice'), state.get('discountsTotalPrice')),
      state.get('totalDeliveryPrice')
    );
    state = state.set('totalPrice', totalPrice);
    return state
      .set('showPoint', !state.get('showPoint'))
      .set('usePoint', 0)
      .set('usePointExtra', 0)
      .set('integralInput', '');
  }

  /**
   * 修改积分抵扣金额(使用两个,一个用来数值计算,一个用来切换时候默认值是空字符串,值都是一样的)
   *
   * @param state
   * @param usePoint
   * @returns {*}
   */
  @Action('order-confirm-actor:setUsePoint')
  setUsePoint(state, { usePoint, integralInput }) {
    const usePointExtra = _.div(usePoint, Const.pointRatio);
    const totalPrice = _.sub(
      _.add(
        _.sub(state.get('goodsTotalPrice'), state.get('discountsTotalPrice')),
        state.get('totalDeliveryPrice')
      ),
      usePointExtra
    );
    return state
      .set('totalPrice', totalPrice)
      .set('usePoint', usePoint)
      .set('usePointExtra', usePointExtra)
      .set('integralInput', integralInput);
  }

  /**
   * 设置支付配送方式
   */
  @Action('order-confirm-actor:set-pay-type')
  setPayType(state, payType) {
    return state.set('payType', payType);
  }
  /**
   * 获取优惠券信息
   */
  @Action('couponInit')
  couponInit(state, { coupons, storeIds, couponPageInfo }) {
    let couponTotalPrice = 0;
    if (couponPageInfo) {
      coupons = this.mapCouponInit(coupons, couponPageInfo);
      couponTotalPrice = couponPageInfo.get('couponTotalPrice')
    }

    // 1.设值优惠券范围字符串
    coupons = coupons.map((coupon) => {
      let scopeTypeStr = '商品：';
      //范围名称
      let goodsName = '仅限';
      switch (coupon.get('scopeType')) {
        case 0:
          goodsName = '全部商品';
          break;
        case 1:
          scopeTypeStr = '品牌：';
          if (coupon.get('brandNames')) {
            coupon.get('brandNames').forEach((value) => {
              goodsName = `${goodsName}[${value}]`;
            });
          }
          break;
        case 2:
          scopeTypeStr = '品类：';
          if (coupon.get('goodsCateNames')) {
            coupon.get('goodsCateNames').forEach((value) => {
              goodsName = `${goodsName}[${value}]`;
            });
          }
          break;
        case 3:
          scopeTypeStr = '分类：';
          if (coupon.get('storeCateNames')) {
            coupon.get('storeCateNames').forEach((value) => {
              goodsName = `${goodsName}[${value}]`;
            });
          }
          break;
        default:
          goodsName = '部分商品';
          break;
      }
      return coupon.set('scopeTypeStr', `${scopeTypeStr}${goodsName}`);
    });

    // 2.构建可用优惠券
    const enables = coupons.filter((coupon) => coupon.get('status') == 0);
    // 2.1.构建平台优惠券
    let commonCoupons = enables.filter(
      (item) => item.get('platformFlag') == 1
    );
    // 2.2.构建店铺优惠券
    let stores = fromJS([]);
    enables
      .filter((coupon) => coupon.get('platformFlag') != 1)
      .forEach((coupon) => {
        const index = stores.findIndex(
          (store) => store.get('storeId') == coupon.get('storeId')
        );
        if (index == -1) {
          stores = stores.push(
            fromJS({
              storeId: coupon.get('storeId'),
              storeName: coupon.get('storeName'),
              coupons: [coupon]
            })
          );
        } else {
          stores = stores.updateIn([index, 'coupons'], (coupons) =>
            coupons.push(coupon)
          );
        }
      });

    // 2.3.重新排序店铺
    let sortedStores = fromJS([]);
    storeIds.forEach((storeId) => {
      const store = stores.find((store) => store.get('storeId') == storeId);
      if (store) sortedStores = sortedStores.push(store);
    });

    // 3.构建不可用优惠券
    const disables = coupons
      .filter((coupon) => coupon.get('status') != 0)
      .map((item) => item.set('disabled', true));
    const unReachPrice = disables.filter((item) => item.get('status') == 1);
    const noAvailableSku = disables.filter((item) => item.get('status') == 2);
    const unReachTime = disables.filter((item) => item.get('status') == 3);

    // 4.设值
    return state
      .set(
        'enableCoupons',
        fromJS({
          commonCoupons,
          stores
        })
      )
      .set(
        'disableCoupons',
        fromJS({
          unReachPrice,
          noAvailableSku,
          unReachTime
        })
      )
      .set('enableCount', enables.size)
      .set('disableCount', disables.size)
      .set('couponTotalPrice', couponTotalPrice);
  }
	/**
	 * 初始化优惠券选择状态
	 * @param coupons 所有优惠券
	 * @param couponPageInfo 选中的优惠券
	 */
  mapCouponInit = (coupons, couponPageInfo) => {
    //通用券
    const commonCouponsInit = couponPageInfo.getIn(['enableCoupons', 'commonCoupons']);
    //店铺券
    const storesInit = couponPageInfo.getIn(['enableCoupons', 'stores']).flatMap(s => s.get('coupons'));
    if (commonCouponsInit) {
      coupons = coupons.map((coupon) => {
        const commonItem = commonCouponsInit.find(c => c.get('couponCodeId') == coupon.get('couponCodeId'));
        if (commonItem) {
          coupon = coupon.set('status', commonItem.get('status')).set('disabled', commonItem.get('disabled'))
            .set('useStatus', commonItem.get('useStatus')).set('couponCanUse', commonItem.get('couponCanUse'))
            .set('chosen', commonItem.get('chosen') || false);
        }
        return coupon;
      })
    }
    if (storesInit) {
      coupons = coupons.map((coupon) => {
        const commonItem = storesInit.find(c => c.get('couponCodeId') == coupon.get('couponCodeId'));
        if (commonItem) {
          coupon = coupon.set('status', commonItem.get('status')).set('disabled', commonItem.get('disabled'))
            .set('useStatus', commonItem.get('useStatus')).set('couponCanUse', commonItem.get('couponCanUse'))
            .set('chosen', commonItem.get('chosen') || false);
        }
        return coupon;
      })
    }
    return coupons;
  }

  /**
 * 选择平台优惠券
 */
  @Action('choose:common:coupon')
  chooseCommonCoupon(state, couponCodeId) {
    return state.updateIn(['enableCoupons', 'commonCoupons'], (coupons) => {
      return coupons.map((coupon) => {
        coupon = coupon.set('chosen', false).set('disabled', true);
        if (coupon.get('couponCodeId') == couponCodeId) {
          coupon = coupon.set('chosen', true).set('disabled', false);
        }
        return coupon;
      });
    });
  }
  /**
 * 选择店铺优惠券
 */
  @Action('choose:store:coupon')
  chooseStoreCoupon(state, { flag, couponCodeId, storeId }) {
    return state.updateIn(['enableCoupons', 'stores'], (stores) => {
      const index = stores.findIndex(
        (store) => store.get('storeId') == storeId
      );
      return stores.updateIn([index, 'coupons'], (coupons) => {
        return coupons.map((coupon) => {
          coupon = coupon.set('chosen', false).set('disabled', 1);
          if (coupon.get('couponCodeId') == couponCodeId) {
            coupon = coupon.set('chosen', 1).set('disabled', false);
          }
          return coupon;
        });
      });
      // const index = stores.findIndex(
      //   (store) => store.get('storeId') == storeId
      // );
      // return stores.map((coupon) => {
      //   coupon = coupon.set('chosen', false).set('disabled', flag);
      //   if (coupon.get('couponCodeId') == couponCodeId) {
      //     coupon = coupon.set('chosen', flag).set('disabled', false);
      //   }
      //   return coupon;
      // });
    });
  }

  /**
 * 清空平台券
 */
  // @Action('clean:common:coupon')
  // cleanCommonCoupon(state) {
  //   return state.updateIn(['enableCoupons', 'commonCoupons'], (coupons) => {
  //     return coupons.map((coupon) =>
  //       coupon.set('chosen', false).set('disabled', false)
  //     );
  //   });
  // }

  /**
 * 设置没到门槛的平台优惠券
 */
  @Action('set:unreached:ids')
  setUnreachedIds(state, { unreachedIds, couponTotalPrice, checkGoodsInfos }) {
    return state
      .set('unreachedIds', fromJS(unreachedIds))
      .set('couponTotalPrice', couponTotalPrice)
      .updateIn(['enableCoupons', 'commonCoupons'], (coupons) =>
        coupons.map((coupon) => {
          if (unreachedIds.includes(coupon.get('couponCodeId'))) {
            coupon = coupon.set('disabled', true);
          }
          return coupon;
        })
      )
      .set('checkGoodsInfos', checkGoodsInfos);
  }

  @Action('calc:coupon:set')
  couponSet(state) {
    const pageInfo = state.get('couponPageInfo');

    let stores = state.get('stores');
    if (!pageInfo) return state;

    // 设置平台券id
    const chosenCommon = pageInfo
      .getIn(['enableCoupons', 'commonCoupons'])
      .find((c) => c.get('chosen') == true);

    if (chosenCommon) {
      state = state.set('commonCodeId', chosenCommon.get('couponCodeId'));
    }

    // 设置店铺券id
    state = state.update('orderConfirm', (items) =>
      items.map((item) => {

        const store = pageInfo
          .getIn(['enableCoupons', 'stores'])
          .find((store) => store.get('storeId') == item.get('storeId'));
        if (!store) return item;
        const chosenCoupon = store
          .get('coupons')
          .find((coupon) => coupon.get('chosen') == true);
        item = item.set('couponCodeId', null);
        if (!chosenCoupon) return item;
        return item.set('couponCodeId', chosenCoupon.get('couponCodeId'));
      })
    );

    // 设置优惠总金额
    const couponTotalPrice = pageInfo.get('couponTotalPrice');
    if (couponTotalPrice) {
      const discountsPrice = state.get('discountsTotalPrice');
      state = state
        .set('couponTotal', couponTotalPrice)
        .set('discountsTotalPrice', couponTotalPrice);
      // 先计算一次总价，如果有运费的话还会再计算一次
      state = state.set(
        'totalPrice',
        _.sub(
          _.sub(state.get('goodsTotalPrice'), state.get('discountsTotalPrice')),
          state.get('usePointExtra')
        )
      );
    }
    return state.set('stores', stores);
  }

  //获取最终获益人的信息
  @Action('order:customerMsg')
  getInviteMsg(state, value) {
    return state.set('InviteMsg', value);
  }
}
