import React, { Component } from 'react';
import { StoreProvider } from 'plume2';

import AppStore from './store';
import Address from './component/address';
import ToolBar from './component/tool-bar';
import StoreList from './component/store-list';
import AllAmount from './component/all-amount';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class OrderConfirm extends Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
    this._handleScroll = this._handleScroll.bind(this);
  }

  componentWillMount() {
    window.addEventListener('scroll', this._handleScroll);
    this.store.confirmInit();
    this.store.getbeneficiary();
  }

  componentDidUpdate() {
    if ((window as any).y || (window as any).y == 0) {
      window.scrollTo(0, (window as any).y);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this._handleScroll);
  }

  render() {
    return (
      <div style={styles.orderConfirm} onScroll={this._handleScroll}>
        <Address />
        <StoreList />
        {this.store
          .state()
          .get('stores')
          .count() > 1 && <AllAmount />}
        <ToolBar />
      </div>
    );
  }

  _handleScroll = (e) => {
    (window as any).y = window.scrollY;
  };
}

const styles = {
  orderConfirm: {
    fontSize: '.28rem',
    background: '#fafafa'
  } as any
};
