import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const style = require('./css/style.css');

@StoreProvider(AppStore)
export default class AboutUs extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '分销员规则介绍';
    this.store.init();
  }

  render() {
    return (
      <div className="distribution-rule">
        <div
          className="about-box"
          dangerouslySetInnerHTML={{
            __html: this.store.state().get('context')
          }}
        />
      </div>
    );
  }
}
