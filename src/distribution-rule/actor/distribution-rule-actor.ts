import { Action, Actor, IMap } from 'plume2';

export default class DistributionRuleActor extends Actor {
  defaultState() {
    return {
      context: ''
    };
  }

  /**
   * 分销员等级规则
   *
   * @param {IMap} state
   * @param {string} context
   * @returns
   * @memberof DistributionRuleActor
   */
  @Action('init')
  init(state: IMap, context) {
    return state.set('context', context);
  }
}
