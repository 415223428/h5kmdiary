import { Store } from 'plume2';
import { config } from 'config';
import * as webapi from './webapi';
import DistributionRuleActor from './actor/distribution-rule-actor';
export default class AppStore extends Store {
  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new DistributionRuleActor()];
  }

  init = async () => {
    const res = (await webapi.fetchInvitorInfo()) as any;
    const { code, context } = res;
    if (code == config.SUCCESS_CODE) {
      let rules =
        context && context.distributionSettingSimVO
          ? context.distributionSettingSimVO.distributorLevelDesc
          : '';
      this.dispatch('init', rules);
    }
  };
}
