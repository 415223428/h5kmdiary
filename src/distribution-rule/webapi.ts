import { Fetch } from 'wmkit';

/**
 * 获取分销设置和邀请人的信息
 */
export const fetchInvitorInfo = () => {
  return Fetch(
    '/distribute/setting-invitor',
    {},
    {
      showTip: false
    }
  );
};
