import { Fetch, WMkit } from 'wmkit';
type TResult = { code: string; message: string; context: any };

/**
 * 店铺档案
 * @returns
 */
export const fetchStore = (id) => {
  const params = {
    storeId: id
  };
  return Fetch(`/store/storeDocument`, {
    method: 'POST',
    body: JSON.stringify(params)
  });
};

/**
 * 查询店铺会员详情信息
 */
export const fetchStoreVip = (storeId) => {
  return WMkit.isLoginOrNotOpen()
    ? Fetch<TResult>('/store/storeVip', {
        method: 'POST',
        body: JSON.stringify({ storeId })
      })
    : Fetch<TResult>('/store/storeVipFront', {
        method: 'POST',
        body: JSON.stringify({ storeId })
      });
};



/**
 * 获取客户在第三方店铺消费情况
 */
export const  thirdPartyConsumption = (id) => {
  return Fetch(`/store/customer/level/${id}`);
};

/**
 * 获取客户在自营的成长值
 */
export const proprietaryGrowthValue = () => {
  return Fetch('/customer/level/rights');
};



/**
 * 获取自营等级列表
 */
export const proprietaryGradeList = () => {
  return Fetch('/customer/level/rightsList');
};



/**
 * 获取第三方店铺等级列表
 */
export const thirdPartyGradeList = (id) => {
  return Fetch(`/store/level/list/${id}`);
};

/**
 * 查询成长值是否开启
 */
export const growthValueIsOpen = () => {
  return Fetch(`/growthValue/isOpen`);
};











