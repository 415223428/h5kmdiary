import { IOptions, msg, Store } from 'plume2';
import TabActor from './actor/tab';
import * as webApi from './webapi';
import { config } from 'config';
import StoreProfileActor from './actor/store-profile-actor';
import { fromJS } from 'immutable';
import { WMkit } from 'wmkit';
import { evaluateWebapi } from 'biz';

export default class AppStore extends Store {
  bindActor() {
    return [new TabActor(), new StoreProfileActor()];
  }

  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  /**
   * tab切换
   */
  tabActive = (tabKey: string) => {
    this.dispatch('tab:history', tabKey);
  };

  /**
   * 初始化
   */
  init = async (id) => {
    evaluateWebapi
      .isShow()
      .then((res) => this.dispatch('store-profile:isShow', res));
    const res = (await webApi.fetchStore(id)) as any;
    if (res.code !== config.SUCCESS_CODE) {
      msg.emit('storeCloseVisible', true);
      return false;
    }
    /**店铺pv/uv埋点*/
    (window as any).myPvUvStatis(null, res.context.companyInfoId);
    this.dispatch('store:init', res.context);

    this.initMember(id);
    let result: any;
    let data: object;

    //没登录的时候，直接return掉不执行
    if (!WMkit.isLogin()) {
      return;
    }
    //如果是自营
    if (res.context.companyType === 0) {
      result = await Promise.all([
        webApi.proprietaryGrowthValue(),
        webApi.proprietaryGradeList(),
        webApi.growthValueIsOpen()
      ]);
    } else {
      result = await Promise.all([
        webApi.thirdPartyConsumption(id),
        webApi.thirdPartyGradeList(id)
      ]);
    }

    //用户信息成长值or 消费多少笔
    if (
      result[0].code == config.SUCCESS_CODE &&
      result[1].code == config.SUCCESS_CODE
    ) {
      this.dispatch('userInfo:init', result[0].context);
      if (res.context.companyType === 0) {
        data = result[1].context.customerLevelVOList.map((value) => {
          return {
            customerLevelName: value.customerLevelName,
            customerLevelDiscount: value.customerLevelDiscount,
            growthValue: value.growthValue
          };
        });
        this.dispatch('member:levelList', fromJS(data));
      } else {
        data = result[1].context.storeLevelVOList.map((value) => {
          return {
            customerLevelName: value.levelName,
            customerLevelDiscount: value.discountRate,
            amountConditions: value.amountConditions,
            orderConditions: value.orderConditions
          };
        });
        this.dispatch('member:levelList', fromJS(data));
      }
    }
    //tab 会员后台打开或者 是第三方就 显示
    if (
      (result[2] &&
        result[2].code == config.SUCCESS_CODE &&
        result[2].context.open) ||
      !result[2]
    ) {
      this.dispatch('userInfo:growthValueIsOpen');
    }
  };

  /**
   * 初始化信息店铺会员信息
   */
  initMember = async (storeId) => {
    const res = (await webApi.fetchStoreVip(storeId)) as any;
    if (res.code == config.SUCCESS_CODE) {
      /**店铺pv/uv埋点*/
      (window as any).myPvUvStatis(null, res.context.store.companyInfoId);

      this.dispatch('member:initParams', res.context);
    } else {
      msg.emit('storeCloseVisible', true);
    }
  };

  //二维码弹窗是否开启
  openClosePop = async (state) => {
    this.dispatch('erweima:getErweima', state);
  };
}
