import { Action, Actor } from "plume2";


export default class SearchActor extends Actor {


  defaultState() {
    return {
      key: 'goods'
    }
  }


  @Action('tab:history')
  click(state, key: String) {
    return state.set('key', key)
  }
}
