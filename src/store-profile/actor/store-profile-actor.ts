import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';
import { IList } from '../../../typings/globalType';

export default class StoreProfileActor extends Actor {
  defaultState() {
    return {
      //会员基本信息
      storeInfo: {
        storeName: '',
        companyType: '',
        supplierName: '',
        socialCreditCode: '',
        companyName: '',
        address: '',
        legalRepresentative: '',
        registeredCapital: '',
        foundDate: '',
        businessTermStart: '',
        businessTermEnd: '',
        businessScope: '',
        provinceId: '',
        cityId: '',
        areaId: '',
        businessLicence: '',
        catePicList: [],
        brandPicList: []
      },
      // 会员的当前等级
      level: {},
      // 不同等级的折扣信息
      levelList: [],
      userInfo: {}, //用户信息
      growthValueIsOpen: false, //是否打开成长值
      //二维码弹窗是否打开
      erweimaState: false,
      //是否展示评价相关信息
      isShow: false
    };
  }

  /**
   * 存储会员基本信息
   * @param state
   * @param storeInfo
   * @returns {Map<string, V>}
   */
  @Action('store:init')
  init(state: IMap, storeInfo) {
    return state.set('storeInfo', fromJS(storeInfo));
  }

  /**
   * 店铺等级信息
   */
  @Action('member:initParams')
  initParams(state: IMap, res) {
    return state.set('level', fromJS(res.level));
  }

  /**
   * 用户消费信息
   */
  @Action('userInfo:init')
  userInfo(state: IMap, res: object) {
    return state.set('userInfo', fromJS(res));
  }

  /**
   * 是否成长值关闭了
   */
  @Action('userInfo:growthValueIsOpen')
  growthValueIsOpen(state: IMap) {
    return state.set('growthValueIsOpen', true);
  }

  /**
   * 等级列表
   */
  @Action('member:levelList')
  levelList(state: IMap, levelList: IList) {
    return state.set('levelList', levelList);
  }

  /**
   * 获取二维码状态
   */
  @Action('erweima:getErweima')
  getErweima(state: IMap, res) {
    return state.set('erweimaState', res);
  }

  @Action('store-profile:isShow')
  setIsShow(state: IMap, res: boolean) {
    return state.set('isShow', res);
  }
}
