import React from 'react';
import { Relax } from 'plume2';

let ZoomImage = null;
const noneImg = require('../img/none.png');

@Relax
export default class Picture extends React.Component<any, any> {
  props: {
    relaxProps?: {
      key: string;
      store: any;
    };
  };

  static relaxProps = {
    key: 'key',
    store: 'storeInfo'
  };

  constructor(props: any) {
    super(props);
    System.import('../../../web_modules/wmkit/image-util/zoom-image').then(
      (zoomImage) => {
        ZoomImage = zoomImage;
      }
    );
  }

  componentWillUnmount(){
    const picture = document.getElementsByClassName('view-large')[0];
    {
      picture?
      picture.remove():null
    }
  }

  render() {
    const { key, store } = this.props.relaxProps;

    return key == 'supplier' ? (
      <div style={{ background: '#fafafa' }}>
        {store.get('businessLicence') ? (
          <div className="pic-content">
            <h4>营业执照</h4>
            <div className="pic-list">
              <div
                className="b-1px"
                onClick={() => this._zoomImage(store.get('businessLicence'))}
              >
                <img src={store.get('businessLicence')} alt="" />
              </div>
            </div>
          </div>
        ) : null}

        {store.get('catePicList') && store.get('catePicList').size > 0 ? (
          <div className="pic-content">
            <h4>经营资质</h4>
            <div className="pic-list">
              {store
                .get('catePicList')
                .toJS()
                .map((pic) => (
                  <div className="b-1px" onClick={() => this._zoomImage(pic)}>
                    <img src={pic} alt="" />
                  </div>
                ))}
            </div>
          </div>
        ) : null}

        {store.get('brandPicList') && store.get('brandPicList').size > 0 ? (
          <div className="pic-content" ref="welcome">
            <h4>品牌经营资质</h4>
            <div className="pic-list">
              {store
                .get('brandPicList')
                .toJS()
                .map((pic) => (
                  <div className="b-1px" onClick={() => this._zoomImage(pic)}>
                    <img src={pic} alt="" />
                  </div>
                ))}
            </div>
          </div>
        ) : null}
      </div>
    ) : null;
  }

  _zoomImage = (src: string) => {
    ZoomImage.renderZoomImage({ src });
  };
}
