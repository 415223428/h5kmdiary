import React, { Fragment } from 'react';
import { Relax } from 'plume2';
import { IMap, IList } from 'typings/globalType';
import { cache } from 'config';
import '../css/vip-style.css';
import { WMkit } from 'wmkit';
const server=require('../img/server.png')
@Relax
export default class MemberRights extends React.Component<any, any> {
  props: {
    relaxProps?: {
      key: string;
      level: IMap;
      levelList: IList;
      store: any;
      userInfo: IMap;
    };
  };

  static relaxProps = {
    key: 'key',
    level: 'level', // 会员等级
    levelList: 'levelList', // 店铺的会员体系
    store: 'storeInfo', // 店铺信息
    userInfo: 'userInfo' //用户消费信息
  };

  render() {
    const { key, level, levelList, store, userInfo } = this.props.relaxProps;
    let customerName = '';
    if (WMkit.isLoginOrNotOpen()) {
      customerName = JSON.parse(localStorage.getItem(cache.LOGIN_DATA))
        .customerDetail.customerName;
    }
    //是否为自营店铺
    let isSelf = store.get('companyType') === 0 ? true : false;
    console.log(level);
    
    return key == 'member' ? (
      <div style={{ height: 'calc(100vh - 40px)', background: '#fafafa' }}>
        <div className="member-header">
          {level && level.get('customerLevelId') ? (
            [
              <div key={0} className="store-vip">
                <div className="vip-box">
                  <div className="welfare">
                    <span>
                      {isSelf
                        ? parseFloat(
                            (
                              userInfo.get('customerLevelDiscount') * 10
                            ).toString()
                          ).toFixed(1)
                        : (
                            parseFloat(
                              userInfo.getIn(['storeLevelVO', 'discountRate'])
                            ) * 10
                          ).toFixed(1)}
                    </span>折
                  </div>
                  <p>
                    {' '}
                    {isSelf
                      ? this.showLevel(userInfo.get('customerLevelName'))
                      : userInfo.getIn(['storeLevelVO', 'levelName'])}
                  </p>
                </div>
              </div>,
              <p key={1} className="vip-text">
                {customerName}
              </p>,
              <p key={2} className="vip-text">
                {isSelf
                  ? <div>成长值:<span style={{color:"#43F1C9"}}>{userInfo.get('customerGrowthValue') || 0}</span></div> 
                  : <div>
                    当前购物<span style={{color:"#43F1C9"}}>{userInfo.get('totalOrder') ||0}笔 消费</span>
                    <span style={{color:"#43F1C9"}}>{userInfo.get('totalAmount') || 0}元</span>
                  </div>}
                   {/* `当前购物${userInfo.get('totalOrder') ||
                       0}笔 消费 ${userInfo.get('totalAmount') || 0}元`} */}
              </p>
            ]
          ) : (
            <div className="not-store-vip">
              <div className="vip-box">
                <p>您还不是该店铺会员噢~</p>
                <p>您可联系商家成为会员</p>
              </div>
            </div>
          )}
        </div>

        <div>
          <div className="rights-box b-1px-t">
            <h2>会员权益</h2>
            <p>
              {isSelf
                ? '您享受平台会员等级对应的商品折扣率，会员等级根据成长值自动升级，成长值获取规则请至个人中心查看'
                : '成为该店铺的会员，可以享受该店铺会员等级对应的商品折扣率，当商家未按照会员等级设置价格或者单独指定了您的采购价时，商品折扣率不生效。店铺会员满足购物笔数和购物金额的条件会自动升级。'}
            </p>
            <table>
              <tr>
                <th style={{ width: '50%' }}>会员等级</th>
                <th style={{ width: '50%' }}>折扣</th>
                {isSelf ? (
                  <th style={{ width: '50%' }}>所需成长值</th>
                ) : (
                  <th style={{ width: '50%' }}>升级规则</th>
                )}
              </tr>
              {levelList &&
                levelList.map((v, k) => {
                  return (
                    <tr key={k}>
                      <td>{this.showLevel(v.get('customerLevelName'))}</td>
                      <td>
                        {(v.get('customerLevelDiscount') * 10).toFixed(1)}
                      </td>
                      {isSelf ? (
                        <td>{v.get('growthValue')}</td>
                      ) : (
                        <td>{this.renderColumn(v.toJS())}</td>
                      )}
                    </tr>
                  );
                })}
            </table>
          </div>
          <div 
          // className="contact-supplier"
          >
            <a 
              // href={`tel:${store.get('contactMobile')}`} 
              style={{justifyContent:'center',display:'flex'}}>
             <img src={server} alt="" style={{height: '.8rem',width:' 4.6rem',marginTop:'0.6rem'}} 
                  onClick={()=>{
                    WMkit.getQiyuCustomer();
                    window.location.href = (window as any).ysf('url');
                  (window as any).openqiyu();
                  }}/>
            </a>
            {/* <img src={server} alt="" style={{height: '.8rem',width:' 4.6rem'}}
                onClick={()=>{
                  store.get('contactMobile')
                }}/> */}
          </div>
        </div>
      </div>
    ) : null;
  }

  renderColumn = (rowInfo) => {
    let orderConditions = rowInfo.orderConditions
      ? '购物满' + rowInfo.orderConditions + '笔'
      : '';
    let amountConditions = rowInfo.amountConditions
      ? '消费' + rowInfo.amountConditions + '元'
      : '';
    if (orderConditions == '' || amountConditions == '') {
      return (
        <Fragment>
          <p>{orderConditions}</p>
          <p>{amountConditions}</p>
        </Fragment>
      );
    }
    return (
      <Fragment>
        <p>{orderConditions}</p>
        <p>或{amountConditions}</p>
      </Fragment>
    );
  };

  showLevel = (levelName)=>{
    let levelnumber;
    levelName==='黑钻级'?
    levelnumber = 'LV5' :
    levelName==='钻石级'?
    levelnumber = 'LV4' :
    levelName==='白金级'?
    levelnumber = 'LV3' :
    levelName==='白银级'?
    levelnumber = 'LV2' :
    levelName==='银杏级'?
    levelnumber = 'LV1' :null
    return levelnumber;
  }
}
