import React from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class Tabs extends React.Component<any, any> {
  props: {
    relaxProps?: {
      key: string;
      tabActive: Function;
      growthValueIsOpen:boolean;
    };
  };

  static relaxProps = {
    key: 'key',
    tabActive: noop,
    growthValueIsOpen:"growthValueIsOpen"
  };

  render() {
    const { key ,growthValueIsOpen} = this.props.relaxProps;

    return (
      <div style={{ height: 40 }}>
        <div className="tab-bar b-1px-tb tab-fixed" style={{display: 'flex',alignItems: 'center'}}>
          <a
            href="javascript:;"
            onClick={() => this._tabActive('goods')}
            className={`${key == 'goods' ? 'cur' : ''}`}
          >
            店铺档案
          </a>
          <a
            href="javascript:;"
            onClick={() => this._tabActive('supplier')}
            className={`${key == 'supplier' ? 'cur' : ''}`}
          >
            企业自传资质
          </a>
          {
            growthValueIsOpen &&  <a
              href="javascript:;"
              onClick={() => this._tabActive('member')}
              className={`${key == 'member' ? 'cur' : ''}`}
            >
              会员权益
            </a>
          }

        </div>
      </div>
    );
  }

  _tabActive = (k) => {
    const { tabActive } = this.props.relaxProps;
    tabActive(k);
  };
}
