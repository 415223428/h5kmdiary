import React from 'react';
import { Relax } from 'plume2';
import { noop, WMImage } from 'wmkit';

@Relax
export default class ErweimaPop extends React.Component<any, any> {
  props: {
    relaxProps?: {
      openClosePop: Function;
    };
  };

  static relaxProps = {
    openClosePop: noop
  };

  render() {
    const { openClosePop } = this.props.relaxProps;

    return (
      <div className="erweima-pop" onClick={() => openClosePop(false)}>
        <div className="white-layer">
          <span className="store-name">某某某的店铺</span>
          <div className="erweima">
            <WMImage src={null} alt="" width="100%" height="100%" />
          </div>
          <span className="scan">邀请好友来扫一扫，把店铺分享给他</span>
        </div>
      </div>
    );
  }
}
