import React from 'react';
import { Relax } from 'plume2';
import { _, FindArea, FormItem, noop ,WMkit} from 'wmkit';
import ErweimaPop from '../component/erweima-pop';

@Relax
export default class Profile extends React.Component<any, any> {
  props: {
    relaxProps?: {
      key: string;
      store: any;
      openClosePop: Function;
      erweimaState: boolean;
      isShow: boolean;
    };
  };

  static relaxProps = {
    key: 'key',
    store: 'storeInfo',
    openClosePop: noop,
    erweimaState: 'erweimaState',
    isShow: 'isShow'
  };

  render() {
    const {
      key,
      store,
      erweimaState,
      openClosePop,
      isShow
    } = this.props.relaxProps;

    let follow;
    const followSum = store.get('followCount');
    if (followSum > 10000) {
      follow = _.div(followSum, 10000).toFixed(1) + '万';
    } else {
      follow = followSum;
    }

    let sumCompositeScore: number,
      sumGoodsScore: number,
      sumServerScore: number,
      sumLogisticsScoreScore: number;
    if (store.get('storeEvaluateSumVO')) {
      sumCompositeScore = store.getIn([
        'storeEvaluateSumVO',
        'sumCompositeScore'
      ]);
      sumGoodsScore = store.getIn(['storeEvaluateSumVO', 'sumGoodsScore']);
      sumServerScore = store.getIn(['storeEvaluateSumVO', 'sumServerScore']);
      sumLogisticsScoreScore = store.getIn([
        'storeEvaluateSumVO',
        'sumLogisticsScoreScore'
      ]);
    } else {
      sumCompositeScore = 5;
      sumGoodsScore = 5;
      sumServerScore = 5;
      sumLogisticsScoreScore = 5;
    }

    return key == 'goods' ? (
      <div>
        {isShow ? (
          <div className="profile-div">
            <ul className="current-ul">
              <li className="current-li">
                <span className="up-text">{follow}</span>
                <span className="down-text">关注人数</span>
                <span className="bottom-text">
                  商品质量
                  <span style={this._col(sumGoodsScore)}>
                    {sumGoodsScore.toFixed(2)}
                  </span>
                </span>
              </li>
              <li className="line1" />
              <li className="current-li">
                <span className="up-text">{store.get('goodsInfoCount')}</span>
                <span className="down-text">全部商品</span>
                <span className="bottom-text">
                  服务态度
                  <span style={this._col(sumServerScore)}>
                  {/* <span style={{color:"#FF4D4D"}}> */}
                    {sumServerScore.toFixed(2)}
                  </span>
                </span>
              </li>
              <li className="line2" />
              <li className="current-li">
                <span className="up-text" style={this._col(sumCompositeScore)}>
                  {sumCompositeScore.toFixed(2)}
                </span>
                <span className="down-text">综合评分</span>
                <span className="bottom-text">
                  发货速度
                  <span style={this._col(sumLogisticsScoreScore)}>
                    {sumLogisticsScoreScore.toFixed(2)}
                  </span>
                </span>
              </li>
            </ul>
          </div>
        ) : null}

        {!(window as any).isMiniProgram && (
          <div className="profile-item">
            <span className="left-spn">联系卖家</span>
            <img src={require('../img/service.png')} alt=""
                 onClick={() => {
                  WMkit.getQiyuCustomer();
                   window.location.href = (window as any).ysf('url');
                   (window as any).openqiyu();
                 }}
            />
          </div>
        )}
        <div className="profile-item" onClick={() => openClosePop(true)}>
          <span className="left-spn">店铺二维码</span>
          {/* <i className="iconfont icon-erweima1" /> */}
          <img src={require('../img/erweima.png')}></img>
        </div>
        {/* 二维码弹窗 */}
        {erweimaState && <ErweimaPop />}

        <FormItem labelName="店铺名称" placeholder={store.get('storeName')} />
        <FormItem
          labelName="店铺类型"
          placeholder={store.get('companyType') == '0' ? '自营' : '第三方商家'}
        />
        <FormItem
          labelName="商家名称"
          placeholder={
            store.get('supplierName') ? store.get('supplierName') : '无'
          }
        />
        <FormItem
          labelName="所在地区"
          placeholder={
            store.get('areaId')
              ? FindArea.addressInfo(
                  store.get('provinceId'),
                  store.get('cityId'),
                  store.get('areaId')
                )
              : '无'
          }
        />
        <FormItem
          labelName="统一社会信用代码"
          placeholder={
            store.get('socialCreditCode') ? store.get('socialCreditCode') : '无'
          }
        />
        <FormItem
          labelName="企业名称"
          placeholder={
            store.get('companyName') ? store.get('companyName') : '无'
          }
        />
        <FormItem
          labelName="住所"
          placeholder={store.get('address') ? store.get('address') : '无'}
        />
        <FormItem
          labelName="法定代表人"
          placeholder={
            store.get('legalRepresentative')
              ? store.get('legalRepresentative')
              : '无'
          }
        />
        <FormItem
          labelName="注册资本"
          placeholder={
            store.get('registeredCapital')
              ? store.get('registeredCapital') + '万元'
              : '无'
          }
        />
        <FormItem
          labelName="成立日期"
          placeholder={
            store.get('foundDate')
              ? _.formatChnDate(store.get('foundDate')).split(' ')[0]
              : '无'
          }
        />
        <FormItem
          labelName="营业期限自"
          placeholder={
            store.get('businessTermStart')
              ? _.formatChnDate(store.get('businessTermStart')).split(' ')[0]
              : '无'
          }
        />
        <FormItem
          labelName="营业期限至"
          placeholder={
            store.get('businessTermEnd')
              ? _.formatChnDate(store.get('businessTermEnd')).split(' ')[0]
              : '无'
          }
        />
        <FormItem
          labelName="经营范围"
          placeholder={store.get('businessScope')}
          style={{height:"100%"}}
        />
      </div>
    ) : null;
  }

  _col = (v: number) => {
    if (v < 3) {
      return { color: '#2DF114' };
    } else {
      return { color: '#f11039' };
    }
  };
}
