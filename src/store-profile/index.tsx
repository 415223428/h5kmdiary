import React from 'react';
import { StoreProvider } from 'plume2';
import Tabs from './component/tab';
import Profile from './component/profile';
import Picture from './component/picture';
import MemberRights from './component/member-rights';
import AppStore from './store';
import './css/style.css';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class StoreProfile extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { sid } = this.props.match.params;
    this.store.init(sid);
  }

  render() {
    return (
      <div>
        <Tabs />
        {/* 店铺档案 */}
        <Profile />
        {/* 企业自传资质 */}
        <Picture />
        {/* 店铺会员 */}
        <MemberRights />
      </div>
    );
  }
}
