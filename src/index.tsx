import React from 'react';
import ReactDOM from 'react-dom';
import 'wmkit/common.css';
import '../web_modules/wmkit/wm_sta.js';
import { Route, Router } from 'react-router-dom';
import 'babel-polyfill';
import { routers } from './router';
import Footer from './footer';
import { BStoreClose, RegisterCoupon, StoreClose } from 'biz';
import wx from 'weixin-js-sdk';
import { history, routeWithSubRoutes, WMkit, WMLoginModal } from 'wmkit';
import Fetch from 'wmkit/fetch';
import { config } from 'config';
import { StoreProvider } from 'plume2';
import AppStore from "../web_modules/wmkit/login-modal/store"


const D2CWechat = () => (
  <Router history={history}>
    <Route component={Container} />
  </Router>
);


const urlShareString = 'shareUserId=';

@StoreProvider(AppStore, { debug: __DEV__ })
class Container extends React.Component<any, any> {
  store: AppStore;
  state = {
    visible: true,
    path: '/main',
    // isALogin:true,
    token: {},
  };
  parseQueryString(url) {
    var obj = {};
    var keyvalue = [];
    var key = "",
      value = "";
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    for (var i in paraString) {
      keyvalue = paraString[i].split("=");
      key = keyvalue[0];
      value = keyvalue[1];
      obj[key] = value;
    }
    return obj;
  }

  componentWillMount() {
    const { token,Bill }: any = this.parseQueryString(location.href);
    if (token) {
      window.token = token;

      this.store.toDoLogin();
    }

    if(Bill){
      this.props.history.push('./order-list');
    }

    (window as any).goback = () => {
      this.props.history.goBack()
    };
    if (typeof localStorage === 'object') {
      try {
        localStorage.setItem('localStorage666', '1');
        localStorage.removeItem('localStorage666');
      } catch (e) {
        alert(
          '浏览器可能开启了无痕浏览模式，影响本网站功能的正常使用，请切换为普通模式'
        );
      }
    }

    Fetch('/system/baseConfig').then((res) => {
      if (res.code == config.SUCCESS_CODE) {
        const ico = (res.context as any).pcIco
          ? JSON.parse((res.context as any).pcIco)
          : null;
        if (ico) {
          let linkEle = document.getElementById('icoLink') as any;
          linkEle.href = ico[0].url;
          linkEle.type = 'image/x-icon';
        }
      }
    });
    let isMiniP = false;
    wx.miniProgram.getEnv(function (res) {
      // if (res.miniprogram) {
      //   isMiniP = true
      // }
      (window as any).isMiniProgram = res.miniprogram;
    });


  }


  render() {
    /**是否包含分享人标识**/
    if (location.href.indexOf(urlShareString) > -1) {
      let param = location.href.split('shareUserId=')[1];
      const shareUserId = param.split('&')[0];
      localStorage.setItem('shareUserId', shareUserId);
      sessionStorage.setItem('shareUserId', shareUserId);
    }
    if (location.href.indexOf('appOpenId') > -1) {
      let appOpenId = location.href.split('appOpenId=')[1];
      console.log(appOpenId);
      console.log('1111111111111++++++++');
      
      localStorage.setItem('appOpenId', appOpenId);
      sessionStorage.setItem('appOpenId', appOpenId);
    }

    /**收集非商品页pv/uv*/
    if (!this._isRelateGoodsPage()) {
      (window as any).myPvUvStatis();
    }

    const routes = routeWithSubRoutes(routers, this.handlePathMatched);

    return (
      <div>
        {routes}
        {this.state.visible ? <Footer path={this.state.path} /> : null}
        <StoreClose />
        <BStoreClose />
        <RegisterCoupon />
        <WMLoginModal />
      </div>
    );
  }

  handlePathMatched = ({ path, hasBottom }) => {
    hasBottom || path === '/main'
      ? this.setState({ visible: true, path: path })
      : this.setState({ visible: false, path: path });
  };

  /**
   * 是否为 商品相关的页面
   */
  _isRelateGoodsPage = () => {
    //商品详情,店铺主页,店铺分类,店铺档案,店铺会员,店铺搜索页,店铺商品列表
    const urlArr = [
      'goods-detail/',
      'store-main/',
      'store-goods-cates/',
      'store-profile/',
      'member-shop/',
      'store-goods-search/',
      'store-goods-list/'
    ];
    //判断当前链接是否为商品相关页面
    return urlArr.findIndex((url) => location.href.indexOf(url) > -1) > -1;
  };
}

ReactDOM.render(<D2CWechat />, document.getElementById('app'));
