import { Actor, Action, IMap } from 'plume2';
export default class ShanPingActor extends Actor {
  defaultState() {
    return {
      fullBannerImages:require('../img/logo.png'),
      fullBannerUrl:'',
      time: 5,
      fullBannerSwitchFlag:true,
    };
  }

  /**
   * 页面初始化
   * @param state
   */
  @Action('shanPing:init')
  init(state) {
    return state
      .set('fullBannerImages', state.get('fullBannerImages'))
      .set('time', state.get('time'))
      .set('fullBannerUrl', state.get('fullBannerUrl'))
      .set('fullBannerSwitchFlag',state.get('fullBannerSwitchFlag'))
  }

  // 设置倒计时
  @Action('init:time')
  setTime(state, time) {
    return state.set('time', time);
  }

   // 设置背景logo
   @Action('init:logo')
   setLogo(state, res) {
     return state
     .set('fullBannerImages', res.fullBannerImages)
     .set('time', res.time)
     .set('fullBannerUrl', res.fullBannerUrl)
     .set('fullBannerSwitchFlag', res.fullBannerSwitchFlag)
   }

   //关闭闪屏
   @Action('shanPing:isShowImg')
   setisShowImg(state,isShowImg){
    return state.set('fullBannerSwitchFlag',!isShowImg)
   }
}
