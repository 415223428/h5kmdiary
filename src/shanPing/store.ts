import { Store } from 'plume2';
import parse from 'url-parse';

import { Alert, history, WMkit, storage, _, wxAuth } from 'wmkit';
import { cache, config } from 'config';
import ShanPingActor from './actor/shanPing-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new ShanPingActor()];
  }

  constructor(props) {
    super(props);
    (window as any)._store = this;
  }

  init = async () => {
    // this.dispatch('shanPing:init');
    this.fetchLogo()
    this.count(5)
  };

  // 倒计时
  count = async(s) =>  {
    const count1 = this.count
    // const {isShowImg} = this.state.
    const isShowImg = await webapi.isShowImg();
    if (s !== -1) {
      this.dispatch('init:time', s);
      s--
    } else {
      s = 5;
      // this.jump()
      this.isShowImg(isShowImg);
      return;
    }
    setTimeout(() => {
      count1(s)
    }, 1000);
  }

  // 点击跳过
  jump = () => {
    console.log('jump');
    // history.push('./main');
  };

  // 请求logo
  fetchLogo = async () => {
    const res = await webapi.isShowImg();
    console.log(res);
    if(res['status'] == '404'){
      this.dispatch('shanPing:init');
    }else{
      this.dispatch('init:logo', res);
    }
  };

  isShowImg = (isShowImg)=>{
    this.dispatch('shanPing:isShowImg',isShowImg)
  }
}


