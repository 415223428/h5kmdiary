import React from 'react';
import { Link } from 'react-router-dom';
import { StoreProvider } from 'plume2';
import AppStore from './store';
const styles = require('./css/style.css');
const logo = require('./img/logo.png');
import { WMkit, history } from 'wmkit';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class ShanPing extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
  }

  constructor(props: any) {
    super(props);
  }

  render() {
    const pcLogo = require('./img/loading.png');
    return (
      <div>
          <div className='jumpMain'>
          <img src={pcLogo} alt="" />
          <div>系统升级维护中. . .</div>
          <div>给您带来的不便，敬请谅解</div>
        </div> 
      </div>
    );
  }
}
