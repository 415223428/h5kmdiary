import { Store } from 'plume2';

import { Confirm, history } from 'wmkit';
import { config } from 'config';
import CouponActor from './actor/coupon-actor';

export default class AppStore extends Store {
  bindActor() {
    return [new CouponActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  /**
   * 对请求数据的处理
   * @param result
   */
  dealData = (result) => {
    const { code, context, message } = result;

    if (code == config.SUCCESS_CODE) {
      this.dispatch('coupon: list: params', {
        field: 'unUseCount',
        value: context.unUseCount
      });
      this.dispatch('coupon: list: params', {
        field: 'usedCount',
        value: context.usedCount
      });
      this.dispatch('coupon: list: params', {
        field: 'overDueCount',
        value: context.overDueCount
      });
    } else {
      Confirm({
        text: message,
        confirmCb: function() {
          history.push('/user-center');
        },
        okBtn: '确定'
      });
    }
  };

  /**
   * 我的优惠券tab页签切换
   * @param index
   */
  setUseStatus = (index) => {
    this.dispatch('coupon: tab: change', index);
  };

  /**
   * 我的优惠券优惠券类型选择
   * @param index
   */
  setCouponType = (index) => {
    this.dispatch('coupon: type: change', index);
    this.changeDrapMenu();
  };

  /**
   * 说明文档的显示隐藏
   */
  changeHelp = () => {
    this.dispatch('change:changeHelp');
  };

  /**
   * 下拉菜单的显示隐藏
   */
  changeDrapMenu = () => {
    this.dispatch('change:changeDrapMenu');
  };

  /**
   * 优惠券说明
   * @param desc
   */
  setCouponDesc = (desc) => {
    this.dispatch('coupon: desc', desc);
    this.changeHelp();
  };
}
