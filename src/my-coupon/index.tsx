import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import Header from './component/header';
// import Search from './component/search';
import Tab from './component/tab';
import CouponList from './component/coupon-list';
import CouponCate from './component/coupon-cate';
import HelpMask from './component/help-mask';

require('./css/style.css')
/**
 * 我的优惠券
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class MyCoupon extends React.Component<any, any> {
  store: AppStore;

  render() {
    return (
      <div className="container">
        <div style={{ height: '1.7rem' }}>
          <div className="top-box">
            <Header />
            {/* <Search /> */}
            <Tab />
          </div>
        </div>
        <CouponList />
        <CouponCate />
        <HelpMask />
      </div>
    );
  }
}
