import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { history, noop } from 'wmkit';

@Relax
export default class Header extends React.Component<any, any> {
  props: {
    relaxProps?: {
      changeDrapMenu: Function;
      showDrapMenu: boolean;
    };
  };

  static relaxProps = {
    changeDrapMenu: noop,
    showDrapMenu: 'showDrapMenu'
  };

  render() {
    const { changeDrapMenu, showDrapMenu } = this.props.relaxProps;

    return (
      <div>
        <div className="headerBox">
          <a
            className="header-title"
            href="javascript:;"
            onClick={() => {
              changeDrapMenu();
            }}
            style={{color:'#333',fontSize:'0.32rem'}}
          >
            我的优惠券
            <i
              className="iconfont icon-jiantou2"
              style={showDrapMenu ? { transform: 'rotate(180deg)',color:'#999',fontSize:'0.28rem' } : {color:'#999',fontSize:'0.28rem'}}
            />
          </a>
          <a
            className="header-right"
            onClick={() => {
              history.push('/coupon-center');
            }}
            style={{fontSize:'0.26rem',color:'#333'}}
          >
            领券中心
          </a>
        </div>
      </div>
    );
  }
}
