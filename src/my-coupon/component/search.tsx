import React from 'react';
import { Relax } from 'plume2';

@Relax
export default class Search extends React.Component<any, any> {
  render() {
    return (
      <div className="coupon-search">
        <input
          type="text"
          className="search-input"
          placeholder="请输入优惠券"
        />
        <a href="javascript" className="search-btn" onClick={() => {}}>
          兑换
        </a>
      </div>
    );
  }
}
