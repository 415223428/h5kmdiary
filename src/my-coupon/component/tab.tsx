import React from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class Tab extends React.Component<any, any> {
  props: {
    relaxProps?: {
      //我的优惠券未使用总数
      unUseCount: number;

      //我的优惠券已使用总数
      usedCount: number;

      //我的优惠券已过期总数
      overDueCount: number;

      //使用状态,0(未使用)，1(使用)，2(已过期)
      useStatus: number;

      //我的优惠券tab页签切换
      setUseStatus: Function;
    };
  };

  static relaxProps = {
    //我的优惠券未使用总数
    unUseCount: 'unUseCount',

    //我的优惠券已使用总数
    usedCount: 'usedCount',

    //我的优惠券已过期总数
    overDueCount: 'overDueCount',

    //使用状态,0(未使用)，1(使用)，2(已过期)
    useStatus: 'useStatus',

    //我的优惠券tab页签切换
    setUseStatus: noop
  };
  render() {
    const {
      unUseCount,
      usedCount,
      overDueCount,
      useStatus,
      setUseStatus
    } = this.props.relaxProps;
    return (
      <div className="tab">
        <a
          href="javascript:;"
          className={useStatus == 0 ? 'actived' : ''}
          onClick={() => setUseStatus(0)}
        >
          未使用({unUseCount})
        </a>
        <a
          href="javascript:;"
          className={useStatus == 1 ? 'actived' : ''}
          onClick={() => setUseStatus(1)}
        >
          已使用({usedCount})
        </a>
        <a
          href="javascript:;"
          className={useStatus == 2 ? 'actived' : ''}
          onClick={() => setUseStatus(2)}
        >
          已过期({overDueCount})
        </a>
      </div>
    );
  }
}
