import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';

import { couponDescQL } from '../ql';
/**
 * 使用说明弹窗
 */
@Relax
export default class HelpMask extends Component<any, any> {
  props: {
    relaxProps?: {
      showHelp: boolean;
      couponDesc: string;
      changeHelp: Function;
    };
  };

  static relaxProps = {
    showHelp: 'showHelp',
    couponDesc: couponDescQL,
    changeHelp: noop
  };

  render() {
    const { showHelp, couponDesc, changeHelp } = this.props.relaxProps;

    return (
      showHelp && (
        <div className="help-container">
          <div className="help-box">
            <div
              className="help-top"
              dangerouslySetInnerHTML={{
                __html: '使用说明：<br />' + couponDesc
              }}
            />
          </div>
          <img
            src={require('../img/close.png')}
            className="close-img"
            onClick={() => changeHelp()}
          />
        </div>
      )
    );
  }
}
