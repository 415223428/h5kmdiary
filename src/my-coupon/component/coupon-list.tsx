import React from 'react';
import { Relax } from 'plume2';
import { fromJS } from 'immutable';
import { _, Blank, history, ListView, noop } from 'wmkit';

const COUPON_TYPE = {
  0: '通用券',
  1: '店铺券',
  2: '运费券'
};
const common = require('../img/common.png');
const store = require('../img/store.png');
const expired = require('../img/expired.png');
const use=require('../img/use.png')
const commonCoupon=require('../img/common-coupon.png')
const storeCoupon=require('../img/store-coupon.png')
const storeCouponD=require('../img/store-coupon-dark.png')

@Relax
export default class CouponList extends React.Component<any, any> {
  _listView: any;

  props: {
    relaxProps?: {
      useStatus: number;
      couponType: number;
      setCouponDesc: Function;
      dealData: Function;
      showHelp: boolean;
    };
  };

  static relaxProps = {
    useStatus: 'useStatus',
    couponType: 'couponType',
    setCouponDesc: noop,
    dealData: noop,
    showHelp: 'showHelp'
  };

  render() {
    const { useStatus, couponType, dealData, showHelp } = this.props.relaxProps;

    return (
      <div className="list">
        <ListView
          url="/coupon-code/my-coupon"
          style={
            showHelp
              ? { height: 'calc(100vh - 1.7rem)', overflowY: 'hidden' }
              : { height: 'calc(100vh - 1.7rem)' }
          }
          params={{
            useStatus: useStatus,
            couponType: couponType
          }}
          isPagination={true}
          renderRow={(item) => this.couponItem(fromJS(item))}
          onDataReached={(result) => dealData(result)}
          renderEmpty={() => (
            <Blank
              img={require('../img/no-coupon.png')}
              content="啊哦，什么券都没有~"
            />
          )}
          ref={(_listView) => (this._listView = _listView)}
        />
      </div>
    );
  }

  /**
   * 渲染优惠券项
   * @param coupon
   * @param useStatus
   * @param setCouponDesc
   * @returns {any}
   */
  couponItem = (coupon) => {
    const { useStatus, setCouponDesc } = this.props.relaxProps;
    return (
      <div
        key={coupon.get('couponCodeId')}
        className={`item ${
          useStatus == 0
            ? coupon.get('couponType') == 1
              ? ' store-item '
              : coupon.get('couponType') == 2
                ? ' freight-item '
                : ''
            : ' gray-item '
        }`}
        style={
          useStatus==0? 
          coupon.get('couponType')==0 ? {background:`url(${common}) top center`,backgroundSize:'cover'}
          : coupon.get('couponType')==1 ? {background:`url(${store}) top center`,backgroundSize:'cover'}
          : {background:`url(${common}) top center`,backgroundSize:'cover'}
          : {background:`url(${expired}) top center`,backgroundSize:'cover'}
        }
      >
        <div className="left">
          <p className="money">
            ￥
            <span className="money-text">{coupon.get('denomination')}</span>
          </p>
          <p className="left-tips">
            {coupon.get('fullBuyType') == 0
              ? '无门槛'
              : `满${coupon.get('fullBuyPrice')}可用`}
          </p>

          {useStatus == 0 &&
            coupon.get('nearOverdue') && (
              <img src={require('../img/expiring.png')} alt="" />
            )}
        </div>
        <div className="right">
          <div className="right-top" >
            {/* <i
              style={
                useStatus==0? 
                coupon.get('couponType')==0 ? {background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))'}
                : coupon.get('couponType')==1 ? {background:'linear-gradient(135deg,rgba(255,139,8,1),rgba(247,187,88,1))'}
                : {background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))'}
                : {background:'#ccc'}
              }
            >
            {COUPON_TYPE[coupon.get('couponType')]}
            </i> */}
             {  useStatus==0? 
                  coupon.get('couponType')==0?(<img src={commonCoupon}/>):
                    coupon.get('couponType')==1?(<img src={storeCoupon}/>):(<img src={storeCoupon}/>)
                :coupon.get('couponType')==0?(<i style={{background:'#ccc'}}>通用券</i>):
                  coupon.get('couponType')==1?(<img src={storeCouponD}/>):(<i style={{background:'#ccc'}}>运费券</i>)}
            <p>
              <span>
                {coupon.get('couponType') == 1
                  ? `仅${coupon.get('storeName')}可用`
                  : '全平台可用'}
              </span>
            </p>
            {coupon.get('couponDesc') && (
              <a
                href="javascript:;"
                className="iconfont icon-help"
                onClick={() => {
                  setCouponDesc(coupon.get('couponDesc'));
                }}
              />
            )}
          </div>
          <div className="range-box">
            限<span>{this._getGoodScope(coupon)}</span>可用
          </div>
          <div className="bottom-box">
            <p>
              <span>{`${_.formatDay(coupon.get('startTime'))}——${_.formatDay(
                coupon.get('endTime')
              )}`}</span>
            </p>
            {useStatus == 0 &&
              coupon.get('couponType') != 2 && (
                // <a
                //   href="javascript:;"
                //   onClick={() => {
                //     history.push({
                //       pathname: '/coupon-promotion',
                //       state: {
                //         couponId: coupon.get('couponId'),
                //         activityId: coupon.get('activityId')
                //       }
                //     });
                //   }}
                //   className={coupon.get('couponCanUse') ? 'use-now' : ''}
                // >
                //   {coupon.get('couponCanUse') ? '立即使用' : '查看可用商品'}
                // </a>
                <img src={use} alt="" 
                     style={{height:'0.5rem'}}
                     onClick={() => {
                      history.push({
                        pathname: '/coupon-promotion',
                        state: {
                          couponId: coupon.get('couponId'),
                          activityId: coupon.get('activityId')
                        }
                      });
                    }}/>

              )}
          </div>
          {useStatus != 0 && (
            <div className="gray-state">
              {/* {useStatus == 1 ? '已使用' : useStatus == 2 ? '已过期' : ''} */}
              <img 
                src={useStatus == 1 ? require('../img/used.png') : useStatus == 2 ? require('../img/cant-use.png') : '' } 
                alt="" 
                style={{width:'1.2rem',height:'1.2rem'}}
              />
            </div>
          )}
        </div>
      </div>
    );
  };

  /**
   * 优惠券的使用范围
   * @private
   */
  _getGoodScope = (coupon) => {
    //营销类型(0,1,2,3,4) 0全部商品，1品牌，2平台(boss)类目,3店铺分类，4自定义货品（店铺可用）
    let scopeTypeStr = '商品：';
    //范围名称
    let goodsName = '仅限';

    switch (coupon.get('scopeType')) {
      case 0:
        goodsName = '全部商品';
        break;
      case 1:
        scopeTypeStr = '品牌：';
        if (coupon.get('brandNames').size > 0) {
          coupon.get('brandNames').forEach((value) => {
            goodsName = `${goodsName}[${value}]`;
          });
        }
        break;
      case 2:
        scopeTypeStr = '品类：';
        if (coupon.get('goodsCateNames').size > 0) {
          coupon.get('goodsCateNames').forEach((value) => {
            goodsName = `${goodsName}[${value}]`;
          });
        }
        break;
      case 3:
        scopeTypeStr = '分类：';
        if (coupon.get('storeCateNames').size > 0) {
          coupon.get('storeCateNames').forEach((value) => {
            goodsName = `${goodsName}[${value}]`;
          });
        }
        break;
      default:
        goodsName = '部分商品';
        break;
    }
    return `${scopeTypeStr}${goodsName}`;
  };
}
