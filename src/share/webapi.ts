import { Fetch ,WMkit} from 'wmkit';
type TResult = { code: string; message: string; context: any };

export const isShowImg = () => {
    return Fetch<TResult>('/system/advertisingConfig', {
      method: 'GET',
      body: JSON.stringify({})
    });
  }; 

  //获取用户名字
  export const shareInfo = () => {
    return Fetch('/customer/customerCenter')
  }
  
  export const init = async (id, pointsGoodsId) => {
    const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
    return pointsGoodsId
      ? Fetch<Result<any>>(`/goods/points/spu/${pointsGoodsId}`)
      : isLoginOrNotOpen
        ? Fetch<Result<any>>(`/goods/spu/${id}`)
        : Fetch<Result<any>>(`/goods/unLogin/spu/${id}`);
    // return Fetch<Result<any>>(`/goods/sku/${id}`);
  };