import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class ShareActor extends Actor {
  defaultState() {
    return {
      customer:{},
      isShow: false,
      isfenxiang:false,
      closeImg:true,
      fenxiangId:'',
      closeChoose:true,
      isWechatShare:false,
      goodAllInfo:{},
    };
  }
//打开分享页面
  @Action('goods-actor:isfenxiang')
  isfenxiang(state){
    return state.set('isfenxiang',true)
  }
//关闭分享页面
  @Action('goods-actor:closefenxiang')
  closefenxiang(state){
    return state.set('isfenxiang',false)
  }
  //关闭img
  @Action('goods-actor:closeImg')
  closeImg(state){
    return state
    .set('closeImg',false)
    .set('closeChoose',false)
  }
  //打开Img
  @Action('goods-actor:openImg')
  openImg(state){
    return state
    .set('closeImg',true)
    .set('closeChoose',true)

  }

  @Action('goods-actor:fenxiangId')
  senId(state,fenxiangId){
    return state.set('fenxiangId',fenxiangId)
  }
  
  @Action('member:init')
  init(state: IMap, { customer }) {
    return state
      .set('customer', fromJS(customer))
  }
  
  @Action('goods-actor:openWechatShare')
  openWechatShare(state){
    return state.set('isWechatShare',true)
  }

  @Action('goods-actor:closeWechatShare')
  closeWechatShare(state){
    return state.set('isWechatShare',false)
  }

  @Action('share:setgoods')
  setgoods(state,goodAllInfo){
    return state.set('goodAllInfo',goodAllInfo)
  }
}
