import React from 'react';
import { Link } from 'react-router-dom';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import GoodsShareImg from './component/goods-share-img'
import { noop, history, WMkit, _, Fetch } from 'wmkit'
require('./css/style.css')

@StoreProvider(AppStore, { debug: __DEV__ })
export default class Share extends React.Component<any, any> {
  store: AppStore;

  componentDidMount(){
    let state = history.location.state ? history.location.state : null;
    this.store.memberInfo();
    this.store.init(state.id,state.pointsGoodsId);
  }
  render() {
    let state = history.location.state ? history.location.state : null;
    return (
      <GoodsShareImg
      goodsInfo={state.goodsInfo}
      url={state.url}
      lineDrawingPrice={state.lineDrawingPrice}
      />
    )
  }
}
