import { Store } from 'plume2';
import parse from 'url-parse';

import { Alert, history, WMkit, storage, _, wxAuth } from 'wmkit';
import { cache, config } from 'config';
import ShareActor from './actor/share-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new ShareActor()];
  }

  constructor(props) {
    super(props);
    (window as any)._store = this;
  }
  //改变分享页面的开关
  changefenxiang = () => {
    this.dispatch('goods-actor:isfenxiang');
  };
  cloasefenxiang = () => {

    this.dispatch('goods-actor:closefenxiang');
    this.dispatch('goods-actor:closeWechatShare');
  };

  iscloseImg = () => {
    this.dispatch('goods-actor:closeImg');
  };

  isopenImg = () => {
    this.dispatch('goods-actor:openImg');
  };

  //传入对应的分享页面Id
  sendId = (fenxiangId) => {
    this.dispatch('goods-actor:fenxiangId', fenxiangId);
  }

  openWechatShare=()=>{
    this.dispatch('goods-actor:openWechatShare');
  }

  memberInfo = async () => {
    const res = (await Promise.all([
      webapi.shareInfo()
    ])) as any;
      this.dispatch('member:init', {
        customer: res[0].context,
      });
  };

  init = async (id, pointsGoodsId) =>{
    const res = await webapi.init(id, pointsGoodsId);
    if(res.code === config.SUCCESS_CODE){
      this.dispatch('share:setgoods',res.context);
    }
  }
}


