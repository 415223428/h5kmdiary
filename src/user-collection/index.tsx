import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import CollectionTop from './component/collection-top';
import CollectionList from './component/collection-list';
import CollectionBottom from './component/collection-bottom';
import CollectionCart from './component/collection-cart';


@StoreProvider(AppStore, {debug: __DEV__})
export default class UserCollection extends React.Component<any, any> {
  store: AppStore;

  componentWillMount(): void {
    this.store.init();
  }

  render() {
    const edit = this.store.state().get('edit')

    return (
      <div>
        <CollectionTop />

        <div style={{paddingBottom: edit ? 50 : 0, background:'#fafafa'}}>
          <CollectionList/>
        </div>

        <CollectionBottom/>

        <CollectionCart/>
      </div>
    )
  }
}
