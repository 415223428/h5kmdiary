import {IMap, QL} from 'plume2'

export const selectAllQL = QL('selectAllQL', [
  'goodsIds',
  (goodsIds: IMap) => {
    return goodsIds.filter((value) => value == false).count() == 0;
  }
])
