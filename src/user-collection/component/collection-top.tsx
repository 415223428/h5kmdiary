import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop, Check } from 'wmkit';

import { selectAllQL } from '../ql';
const styles = require('../css/style.css');

@Relax
export default class CollectionTop extends Component<any, any> {
  props: {
    relaxProps?: {
      //收藏总数
      totalCount: number;
      //是否编辑
      edit: boolean;
      //改变状态（编辑 | 完成）
      updateEdit: Function;
      //是否全选
      selectAll: boolean;
      //全选事件
      onCheckAll: Function;
    };
  };

  static relaxProps = {
    totalCount: 'totalCount',
    edit: 'edit',
    updateEdit: noop,
    onCheckAll: noop,
    selectAll: selectAllQL
  };

  render() {
    let {
      totalCount,
      edit,
      updateEdit,
      selectAll,
      onCheckAll
    } = this.props.relaxProps;

    //如果等于0
    if (totalCount === 0) {
      return null;
    }

    return (
      <div style={{ height: '0.8rem',background:'#fafafa' }}>
        <div className={styles.editHead + ' ' + 'layout-top'}>
          <div className={styles.myStore}>
            <div className={edit ? 'show' : 'hide'} style={{ marginRight: 10 }}>
              <Check
                checked={selectAll}
                onCheck={() => onCheckAll(!selectAll)}
                checkStyle={{width:'0.38rem',height:'0.38rem',lineHeight:'0.38rem'}}
              />
            </div>
            <div style={{display:'flex'}}>我的收藏&nbsp;&nbsp;<p style={{color:'#FF4D4D'}}>{totalCount}</p></div>
          </div>
          <span onClick={() => updateEdit(!edit)}>
            {edit ? '完成' : '编辑'}
          </span>
        </div>
      </div>
    );
  }
}
