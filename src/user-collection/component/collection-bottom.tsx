import React, {Component} from 'react'
import {Relax} from 'plume2'

import {IList} from 'typings/globalType'
import {noop, Check, Alert, Confirm, Button} from 'wmkit'

import {selectAllQL} from '../ql'
const SubmitButton = Button.Submit
const del=require('../img/del.png')
@Relax
export default class CollectionBottom extends Component<any, any> {
  props: {
    relaxProps?: {
      //是否编辑
      edit: boolean
      //是否含有失效商品
      invalidFlag: boolean
      //是否全选
      selectAll: boolean
      //选中的skuIds
      goodsIds: IList
      //删除
      onDelete: Function
      //全选事件
      onCheckAll: Function
      //清楚失效商品
      onCleanInvalid: Function
    }
  };

  static relaxProps = {
    edit: 'edit',
    invalidFlag: 'invalidFlag',
    onDelete: noop,
    goodsIds: 'goodsIds',
    selectAll: selectAllQL,
    onCheckAll: noop,
    onCleanInvalid: noop
  }

  render() {

    let {edit, invalidFlag, selectAll, onCheckAll, onCleanInvalid, goodsIds} = this.props.relaxProps;
    let checkedGoodsIds = goodsIds.filter(checked => checked);

    return (
      <div style={{height: 48}} className={edit ? 'show' : 'hide'}>
        <div className="bottom-bar-total" style={{justifyContent: 'space-between'}}>
          <div className="check-all">
            <div style={{marginRight: '10px'}}>
              <Check checked={selectAll} onCheck={() => onCheckAll(!selectAll)} checkStyle={{width:'0.38rem',height:'0.38rem',lineHeight:'0.38rem'}}/>
            </div>
            <span>全选</span>
          </div>
          <div style={{display: 'flex',alignItems: 'center'}}>
            <SubmitButton defaultStyle={{background: '#e5e5e5', width: '1.9rem', border:'none', color:'#FFF'}}
                          text={'清除失效商品'}
                          onClick={onCleanInvalid}
                          disabled={!invalidFlag}/>
           { checkedGoodsIds.count() == 0?
                <SubmitButton text={'删除'} 
                          disabled={checkedGoodsIds.count() == 0} 
                  onClick={() => this._deleteAll()}
                  defaultStyle={{border:'none',width:'1.9rem',background:'#E5E5E5'}}/>
              :
                <img src={del} alt="" style={{width:'1.9rem'}} onClick={() => this._deleteAll()}/>}
          </div>
        </div>
      </div>
    )
  }


  /**
   * 将选中商品移除我的收藏
   * @private
   */
  _deleteAll = () => {
    let {goodsIds, onDelete} = this.props.relaxProps;
    let checkedGoodsIds = goodsIds.filter(checked => checked);
    if (checkedGoodsIds.count() == 0) {
      Alert({text: '请选择需要取消收藏的商品'})
      return;
    }
    Confirm({
      title: '删除商品',
      text: '您确定要删除所选商品？',
      cancelBtn: '取消',
      okBtn: '确定',
      confirmCb: () => {
        onDelete(goodsIds.filter(checked => checked).keySeq().toJS())
      }
    })
  }
}
