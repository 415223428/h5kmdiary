import React, { Component } from 'react';
import { WMImage, history } from 'wmkit';
import CollectionCart from './collection-cart';

export default class CollectEmpty extends Component<any, any> {
  render() {
    return (
      <div className="list-none">
        <WMImage
          src={require('../img/no-collection.png')}
          width="3.17rem"
          height="2rem"
        />
        <p style={{color:'#999',fontSize:'0.26rem',marginTop:'0.6rem'}}>您还没有收藏商品哦</p>
        <div className="half">
          <button
            className="btn btn-ghost"
            onClick={() => {
              history.push('/goodsList');
            }}
            style={{
              display:'flex',borderRadius:'0.35rem',alignItems:'center',justifyContent:'center',border:'1px solid #333',padding:'0',width:'2.5rem',height:'0.7rem',
              fontSize:'0.28rem'
            }}
          >
            逛逛商品
          </button>
        </div>
        <CollectionCart />
      </div>
    );
  }
}
