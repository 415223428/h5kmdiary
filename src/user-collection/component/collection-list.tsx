import React, { Component } from 'react';
import { fromJS } from 'immutable';
import { Relax, IMap } from 'plume2';

import { noop, ListView } from 'wmkit';
import { config } from 'config';

import CollectionListItem from './collection-list-item';
import CollectEmpty from './none';

@Relax
export default class CollectionList extends Component<any, any> {
  _listView: any;

  props: {
    relaxProps?: {
      initGoodInfoIds: Function;
      //收藏总数
      updateTotalCount: Function;
    };
  };

  static relaxProps = {
    initGoodInfoIds: noop,
    updateTotalCount: noop
  };

  render() {
    return (
      <ListView
        url="/goods/goodsFollows"
        style={{ height: 'calc(100vh - 40px)', width:'7.1rem', borderRadius:'0.2rem',boxShadow:'0 0.06rem 0.2rem 0 rgba(0, 0, 0, 0.04)',margin:'0 auto',background:'#fff' }}
        onDataReached={(res) => this._fetchAfter(res)}
        renderRow={(goodsItem: object) => (
          <CollectionListItem goodsItem={fromJS(goodsItem)} key={fromJS(goodsItem).get('id')}/>
        )}
        otherProps={['goodsIntervalPrices']}
        renderEmpty={() => (
          <div style={{width:'100vw',height:'100vh',background:'#fff',position:'fixed',left:'0'}}>
            <CollectEmpty />
          </div>
        )}
        ref={(_listView) => (this._listView = _listView)}
      />
    );
  }

  //获取时修改数据
  _fetchAfter = (res) => {
    if (res && res.code === config.SUCCESS_CODE) {
      const { initGoodInfoIds, updateTotalCount } = this.props.relaxProps;
      let goodsInfos = res.context.goodsInfos;

      //添加goodsId
      if (goodsInfos.totalElements > 0) {
        //设置总数
        updateTotalCount(goodsInfos.totalElements);

        let goodsIdMap = {};
        goodsInfos.content.forEach((element) => {
          goodsIdMap[element.goodsInfoId] = false;
        });
        //存放skuIds数据
        initGoodInfoIds(goodsIdMap);
      }
    }
  };
}
