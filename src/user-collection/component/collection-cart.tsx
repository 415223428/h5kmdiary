import React, { Component } from 'react';
import { Relax, msg } from 'plume2';
import { history, WMkit } from 'wmkit';
import * as webapi from '../webapi';
const car =require('../img/car.png')
@Relax
export default class CollectionCart extends Component<any, any> {
  props: {
    relaxProps?: {
      //收藏夹数量
      totalCount: number;
      edit: boolean;
    };
  };

  static relaxProps = {
    totalCount: 'totalCount',
    edit: 'edit'
  };

  constructor(props) {
    super(props);
    //sku种类数量
    this.state = { purchaseNum: 0 };
  }

  componentWillMount() {
    msg.on('purchaseNum', this._fetchPurChaseNum);
  }

  componentDidMount() {
    WMkit.isLogin() && this._fetchPurChaseNum();
  }

  componentWillUnmount() {
    msg.off('purchaseNum', this._fetchPurChaseNum);
  }

  render() {
    let { edit, totalCount } = this.props.relaxProps;

    let style = edit ? 'bottom-cart hide' : 'bottom-cart show';

    return (
      <div 
        className={style} 
        onClick={() => history.push('/purchase-order')}
        
      >
        {/* <i className="iconfont icon-gouwuche" style={{color:'#fff'}}/> */}
        <img src={car} alt="" style={{width: '120%'}}/>
        <div 
          className="tag" 
          style={{borderRadius:' 50%',background: '#fff',position: 'absolute',color:' #FF4D4D',fontSize: '0.3rem'}}
        >{this.state.purchaseNum}
        </div>
      </div>
    );
  }

  //查询购物车数量
  _fetchPurChaseNum = async () => {
    const { context, message, code } = await webapi.fetchPurchaseCount();
    this.setState({ purchaseNum: context });
  };
}
