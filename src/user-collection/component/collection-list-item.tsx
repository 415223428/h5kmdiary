import React, { Component } from 'react';

import { IMap, Relax } from 'plume2';
import { Icon} from 'antd-mobile';
import { _, Check, history, noop, WMImage } from 'wmkit';
import { GoodsNum, MarketingLabel } from 'biz';
const selfSale=require('../img/ziying.png')
@Relax
export default class CollectionListItem extends Component<any, any> {
  props: {
    relaxProps?: {
      edit: boolean;
      //选中的goodsId
      goodsIds: IMap;
      //选中
      initGoodInfoIds: Function;
      isShow: boolean;
    };
    goodsItem: any;
  };

  static relaxProps = {
    edit: 'edit',
    goodsIds: 'goodsIds',
    initGoodInfoIds: noop,
    isShow: 'isShow'
  };

  constructor(props) {
    super(props);
    this.state = {
      scrollHeight: 0,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }
  componentDidMount() {
    const id = this.props.goodsItem.get('goodsInfoId');
    let scrollHeight = document.getElementById(id).scrollHeight;
    this.setState({
      scrollHeight
    });
  }
  render() {
    const { edit, goodsIds, isShow } = this.props.relaxProps;
    // sku信息
    const { goodsItem } = this.props;
    const id = goodsItem.get('goodsInfoId');
    //选中状态
    let checked = goodsIds.get(id);

    //goodsStatus 商品状态 0：正常 1：缺货 2：失效
    // 库存等于0或者起订量大于剩余库存
    const noStock = goodsItem.get('goodsStatus') == 1;
    //已删除或已下架 为失效
    const invalid = goodsItem.get('goodsStatus') == 2;

    // 商品是否要设置成缺货状态
    const stock = goodsItem.get('stock');

    //购买量
    const buyCount = noStock ? 0 : goodsItem.get('buyCount') || 0;

    // 营销标签
    const marketingLabels = goodsItem.get('marketingLabels');
    // 优惠券标签
    const couponLabels = goodsItem.get('couponLabels');
    const social = goodsItem.get('distributionGoodsAudit') == 2 ? true : false;

    //⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇评价相关数据处理⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇
    //好评率
    let favorableRate = '100';
    if (goodsItem.get('goodsEvaluateNum') && goodsItem.get('goodsEvaluateNum') != 0){
      favorableRate = _.mul(_.div(goodsItem.get('goodsFavorableCommentNum'),goodsItem.get('goodsEvaluateNum')),100).toFixed(0);
    }

    //评论数
    let evaluateNum = '暂无';
    const goodsEvaluateNum = goodsItem.get('goodsEvaluateNum');
    if (goodsEvaluateNum) {
      if (goodsEvaluateNum<10000){
        evaluateNum = goodsEvaluateNum;
      } else {
        const i = _.div(goodsEvaluateNum,10000).toFixed(1);
        evaluateNum =  i+'万+';
      }
    }

    //销量
    let salesNum='暂无';
    const goodsSalesNum = goodsItem.get('goodsSalesNum');
    if (goodsSalesNum){
      if (goodsSalesNum<10000){
        salesNum = goodsSalesNum;
      } else {
        const i = _.div(goodsSalesNum,10000).toFixed(1);
        salesNum =  i+'万+';
      }
    }
    // 预估点数
    let pointNum = goodsItem.get('kmPointsValue');
    //⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆评价相关数据处理⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆⬆
    return (
      <div key={id} className="my-store" style={{padding:'0.3rem'}}>
        <div className={edit ? 'show' : 'hide'} style={{ marginRight: 10 }}>
          <Check checked={checked} onCheck={() => this._onChecked(id)} checkStyle={{width:'0.38rem',height:'0.38rem',lineHeight:'0.38rem'}}/>
        </div>
        <div
          className={
            noStock || invalid ? 'goods-list  invalid-goods' : 'goods-list'
          }
          onClick={() =>
            !invalid ? history.push('/goods-detail/' + id) : null
          }
        >
          <div className="img-box" style={{objectFit:'scale-down'}}>
            <WMImage
              src={goodsItem.get('goodsInfoImg')}
              // width="100%"
              height="80%"
              style={{objectFit:'cover'}}
            />
          </div>
          <div className="detail" style={{marginLeft:'0.3rem'}}>
            <div
              id={id}
              className="title col-goods-info"
              style={!invalid?
                        this.state.scrollHeight>35?{height:'0.65rem',textOverflow:'ellipsis'}:{color:'#333'}
                :this.state.scrollHeight>35?{color:'#999',height:'0.65rem',textOverflow:'ellipsis'}:{color:'#999'}
              }
            >
              {goodsItem.get('goodsInfoName')}
            </div>
            <p 
              className="gec"
              style={!invalid?{fontWeight:'bold',color:'#333',fontSize:'0.26rem'}
              :{fontWeight:'bold',fontSize:'0.26rem',color:'#999'}}
            >
            {goodsItem.get('specText')}
            </p>

            {/* 评价 */}
            {
              invalid?
                null
                :
                (
                  isShow?
                    (
                      <div className="goods-evaluate">
                        <span className="goods-evaluate-spn" style={{fontSize:'0.22rem'}}>{salesNum}销量</span>
                        <span className="goods-evaluate-spn mar-lr-28" style={{fontSize:'0.22rem'}}>{evaluateNum}评价</span>
                        <span className="goods-evaluate-spn" style={{fontSize:'0.22rem'}}>{favorableRate}%好评</span>
                        {localStorage.getItem('loginSaleType') == '1'?<span className="goods-evaluate-spn">预估点数{pointNum}</span>:null}
                      </div>
                    )
                    :
                    (
                      <div className="goods-evaluate">
                        <span className="goods-evaluate-spn" style={{fontSize:'0.22rem'}}>{salesNum}销量</span>
                      </div>
                    )
                )
            }

            <div className="marketing" style={{display: 'flex',flexDirection: 'row',alignItems:'center'}}>
              {goodsItem.get('companyType') == 0 ? (
                <div 
                  className="self-sale-img"
                  style={{display:'flex',alignItems:'center',marginRight:'.1rem'}}
                >
                  <img src={selfSale} alt="" style={{width:' 0.6rem'}}/></div>
              ) : (
                ''
              )}
              {!social &&
                (marketingLabels || couponLabels) && (
                  <MarketingLabel
                    marketingLabels={marketingLabels}
                    couponLabels={couponLabels}
                  />
                )}
            </div>
            {invalid ? (
              <div className="out-stock" style={{ marginLeft: 0 }}>
                失效
              </div>
            ) : (
              <div className="bottom">
                <div className="bottom-price">
                  <span className="price">
                    <div style={{fontSize:'0.32rem',color:'#FF4D4D'}}>
                      <i className="iconfont icon-qian" style={{fontSize:'0.32rem'}}/>
                      {this._calShowPrice(goodsItem, buyCount)}
                    </div>
                  </span>
                  {noStock && <div className="out-stock">缺货</div>}
                </div>

                {!edit &&
                  !invalid && (
                    <GoodsNum
                      value={buyCount}
                      max={stock}
                      disableNumberInput={noStock}
                      goodsInfoId={id}
                      onAfterClick={this._afterChangeNum}
                    />
                  )}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 点击选中事件
   * @param id
   * @private
   */
  _onChecked = (goodsId) => {
    let { goodsIds, initGoodInfoIds } = this.props.relaxProps;
    let goodsMap = {};
    goodsMap[goodsId] = !goodsIds.get(goodsId);
    initGoodInfoIds(goodsMap);
  };

  /**
   * 根据设价方式,计算显示的价格
   * @returns 显示的价格
   * @private
   */
  _calShowPrice = (goodsInfo, buyCount) => {
    let showPrice;
    if (
      goodsInfo.get('distributionGoodsAudit') &&
      goodsInfo.get('distributionGoodsAudit') == 2
    ) {
      showPrice = _.addZero(goodsInfo.get('marketPrice') || 0);
    } else {
      // 阶梯价,根据购买数量显示对应的价格
      if (goodsInfo.get('priceType') === 1) {
        const intervalPriceArr = goodsInfo
          .get('intervalPriceIds')
          .map((id) =>
            goodsInfo
              .getIn(['_otherProps', 'goodsIntervalPrices'])
              .find((pri) => pri.get('intervalPriceId') === id)
          )
          .sort((a, b) => b.get('count') - a.get('count'));
        if (buyCount > 0) {
          // 找到sku的阶梯价,并按count倒序排列从而找到匹配的价格
          showPrice = intervalPriceArr
            .find((pri) => buyCount >= pri.get('count'))
            .get('price');
        } else {
          showPrice = goodsInfo.get('intervalMinPrice') || 0;
        }
      } else {
        showPrice = goodsInfo.get('salePrice') || 0;
      }
    }
    return _.addZero(showPrice);
  };

  /**
   * 数量修改后的方法,用于修改购买数量,响应变化对应的阶梯价格
   * @private
   */
  _afterChangeNum = (num) => {
    this.setState({
      goodsItem: this.state.goodsItem.set('buyCount', num)
    });
  };
}
