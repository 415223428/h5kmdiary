import { IOptions, Store } from 'plume2';
import { config } from 'config';
import { Alert } from 'wmkit';
import { evaluateWebapi } from 'biz';
import * as webapi from './webapi';
import EditorActor from './actor/editor-actor';


export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }


  bindActor() {
    return [
      new EditorActor
    ]
  }

  init = () => evaluateWebapi.isShow().then(res=>this.dispatch('collection:isShow',res));


  /**
   * 改变收藏数量
   * @param totalCount
   */
  updateTotalCount = async (totalCount) => {
    this.dispatch('collection:totalCount', totalCount)
  }


  /**
   * 存放skuIds数据
   * @param goodsIdMap
   */
  initGoodInfoIds = (goodsIdMap) => {
    this.dispatch('collection:initGoodInfoIds', goodsIdMap)
  }


  /**
   * 改变编辑状态
   * @param edit
   */
  updateEdit = async (edit) => {
    //如果是编辑页面
    if (edit) {
      //查询是否含有失效商品
      const res = await webapi.invalidGoodsFollow()
      this.dispatch('collection:invalidFlag', res.context)
    }
    this.dispatch('collection:edit', edit)

  }


  /**
   * 全选
   * @param checked
   */
  onCheckAll = (checked) => {
    this.dispatch('collection:checkAll', checked)
  }


  /**
   * 删除收藏商品
   * @param goodIds
   * @returns {Promise<void>}
   */
  onDelete = async (goodIds) => {
    const res = await webapi.deleteGoodsFollow(goodIds);
    if (res.code == config.SUCCESS_CODE) {
      Alert({text: '删除成功！', cb: () => location.reload()})
    } else {
      Alert({text: '操作失败', time: 1000})
    }
  }


  /**
   * 清楚失效商品
   * @returns {Promise<void>}
   */
  onCleanInvalid = async () => {
    const res = await  webapi.deleteInvalidGoodsFollow();
    if (res.code == config.SUCCESS_CODE) {
      Alert({text: '清除成功', cb: () => location.reload()})
    } else {
      Alert({text: '操作失败', time: 1000})
    }
  }
}