import { Fetch } from 'wmkit';

/**
 * 查询购物车数量
 */
export const fetchPurchaseCount = () => {
  return Fetch(`/site/countGoods`);
};

/**
 * 移除我的收藏
 * @param goodsInfoIds
 * @returns {Promise<Result<T>>}
 */
export const deleteGoodsFollow = (goodsInfoIds) => {
  return Fetch('/goods/goodsFollow', {
    method: 'DELETE',
    body: JSON.stringify({
      goodsInfoIds: goodsInfoIds
    })
  });
};

/**
 * 是否含有失效商品
 * @returns {Promise<Result<T>>}
 */
export const invalidGoodsFollow = () => {
  return Fetch(`/goods/hasInvalidGoods`);
};

/**
 * 清除失效商品
 * @returns {Promise<Result<T>>}
 */
export const deleteInvalidGoodsFollow = () => {
  return Fetch('/goods/goodsFollows', {
    method: 'DELETE'
  });
};
