import { Action, Actor, IMap } from 'plume2';


export  default class EditorActor extends Actor {
  defaultState() {
    return {
      //编辑 | 完成
      edit: false,
      //是否含有失效商品
      invalidFlag: false,
      //收藏总数
      totalCount: 0,
      //选中的skuId
      goodsIds: {},
      //评价相关信息是否展示
      isShow: false
    }
  }


  /**
   * 改变收藏总数
   * @param state
   * @param newState
   * @returns {Map<K, V>}
   */
  @Action('collection:totalCount')
  updateTotalCount(state: IMap, totalCount) {
    return state.set('totalCount', totalCount);
  }


  /**
   * 存放skuId数据
   * @param state
   * @returns {Map<string, boolean>}
   */
  @Action('collection:initGoodInfoIds')
  initGoodInfoIds(state: IMap, goodsIdMap) {
    return state.mergeDeep({goodsIds: goodsIdMap})
  }


  /**
   * 是否含有失效商品
   * @param state
   * @param newState
   * @returns {Map<K, V>}
   */
  @Action('collection:invalidFlag')
  setInvalidFlag(state: IMap, invalidFlag) {
    return state.set('invalidFlag', invalidFlag);
  }


  /**
   * 是否编辑
   * @param state
   * @param newState
   * @returns {Map<K, V>}
   */
  @Action('collection:edit')
  mergeState(state: IMap, edit) {
    return state.set('edit', edit);
  }


  /**
   * 全部选中
   * @param state
   * @param checked
   * @returns {Map<string, any>}
   */
  @Action('collection:checkAll')
  onCheckAll(state: IMap, checked) {
    return state.update('goodsIds', goodsIds => goodsIds.map(v => checked))
  }

  @Action('collection:isShow')
  setisShow(state: IMap, flag: boolean) {
    return state.set('isShow', flag)
  }
}