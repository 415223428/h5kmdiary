import { Actor, Action, IMap } from 'plume2';

export default class OrderListTopActor extends Actor {
  defaultState() {
    return {
      // 商品订单 or 其他订单
      headTabKey: 0,
      // 选中的订单状态tab key
      key: ''
    };
  }

  /**
   * 设置商品订单key or 其他订单key
   */
  @Action('top:headActive')
  topHeadActive(state: IMap, headTabKey: string) {
    return state.set('headTabKey', headTabKey);
  }

  /**
   * 设置tab
   */
  @Action('top:active')
  topActive(state: IMap, key: string) {
    return state.set('key', key);
  }
}
