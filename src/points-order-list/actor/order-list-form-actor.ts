/**
 * Created by chenpeng on 2017/7/18.
 */
import { Action, Actor, IMap } from 'plume2';
import { fromJS, List } from 'immutable';

export default class FormActor extends Actor {
  defaultState() {
    return {
      // 列表数据
      form: {
        //订单流程状态
        flowState: '',
        //订单付款状态
        payState: '',
        // 邀请人ID
        inviteeId: '',
        // 分销渠道
        channelType: '',
        //订单类型
        orderType: 'POINTS_ORDER'
      },
      //订单列表
      orders: [],
      // 小B店铺名字
      inviteeShopName: ''
    };
  }

  /**
   * 设置列表数据
   */
  @Action('order-list-form:setForm')
  setForm(state: IMap, params: IMap) {
    return state.set('form', fromJS(params));
  }

  /**
   * 更新订单数据
   */
  @Action('order-list-form:setOrders')
  updateOrders(state: IMap, params: Array<any>) {
    return state.update('orders', (value: List<any>) => {
      return value.push(...params);
    });
  }

  /**
   * 切换tab时清除订单数据
   */
  @Action('order-list-form:clearOrders')
  clearOrders(state: IMap) {
    return state.set('orders', List());
  }

  /**
   * 小B店铺名字
   */
  @Action('order-list-form:shopName')
  setShopName(state: IMap, shopName) {
    return state.set('inviteeShopName', shopName);
  }
}
