import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
@Relax
export default class Header extends React.Component<any, any> {
  props: {
    relaxProps?: {
      headTabKey: number;
      changeTopHeadActive: Function;
    };
  };

  static relaxProps = {
    headTabKey: 'headTabKey',
    changeTopHeadActive: noop
  };

  render() {
    return (
      <div style={{ height: '1rem' }}>
        <div className="points-order-header">
          <div>
            <span style={{ color: '#ffffff' }}>积分订单</span>
          </div>
        </div>
      </div>
    );
  }
}
