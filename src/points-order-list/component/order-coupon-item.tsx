import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Relax} from 'plume2';
import {fromJS} from 'immutable';
import {Const} from 'config';
import moment from 'moment';

/**
 * 订单状态
 * @type {{INIT: string; AUDIT: string; DELIVERED_PART: string; DELIVERED: string; CONFIRMED: string; COMPLETED: string; VOID: string}}
 */
const FLOW_STATE = {
  AUDIT: '待发货',
  DELIVERED_PART: '待发货',
  DELIVERED: '待收货',
  COMPLETED: '已完成'
};

const COUPON_TYPE = {
  0: '通用券',
  1: '店铺券',
  2: '运费券'
};

@Relax
export default class OrderCouponItem extends Component<any, any> {
  props: {
    order: any;
  };

  render() {
    const { order } = this.props;
    const couponInfo = fromJS(order.tradeCouponItem.couponInfoVO);

    return (
      <div className="ships">
        <div className="order-item">
          <div className="order-head">
            <div className="status">
              {FLOW_STATE[order.tradeState.flowState]}
            </div>
          </div>
          <div className="limit-img ship-img">
            <div className="img-content">
              <div className="good-content">
                <div className="goodPrice">
                  <p style={{marginTop:'0.4rem'}}>￥
                    <span style={{fontSize:'0.5rem'}}>{couponInfo.get('denomination')}</span>
                    {' '}</p>
                  <p>{this._buildFullBuyPrice(couponInfo)}</p>
                </div>
                <div className="right-good-group">
                  <div>{this._buildStorename(couponInfo)}</div>
                  {couponInfo.get('couponType') != 2 && (
                      <div style={{paddingTop:'5px'}}>
                        限<span>{this._buildScope(couponInfo)}</span>可用
                      </div>
                  )}
                   <div className="good-date">券有效期：{this._buildRangDay(couponInfo)}</div>
                </div>
              </div>
            </div>
          </div>
          <div className="bottom">
            <div className="price">
              订单积分：{order.tradePrice.points || 0}
            </div>
          </div>
        </div>
        <div className="bot-line" />
      </div>
    );
  }

  /***
   * 满减金额
   */
  _buildFullBuyPrice = (coupon) => {
    return coupon.get('fullBuyType') === 0
      ? '无门槛'
      : `满${coupon.get('fullBuyPrice')}可用`;
  };

  /**
   * 优惠券使用店铺名称（暂时只有平台券）
   */
  _buildStorename = (coupon) => {
    let text = '';
    if (coupon.get('platformFlag') === 1) {
      text = '全平台可用';
    }
    return `${text}`;
  };

  /**
   * 优惠券使用范围
   */
  _buildScope = (coupon) => {
    let text = '';
    let scopeType = '';
    if (coupon.get('scopeType') == 0) {
      scopeType = '商品：';
      text = '全部商品';
    } else if (coupon.get('scopeType') == 1) {
      scopeType = '品牌：';
      text = '仅限';
      coupon.get('scopeNames').forEach((value) => {
        let name = value ? '[' + value + ']' : '';
        text = `${text}${name}`;
      });
    } else if (coupon.get('scopeType') == 2) {
      scopeType = '品类：';
      text = '仅限';
      coupon.get('scopeNames').forEach((value) => {
        let name = value ? '[' + value + ']' : '';
        text = `${text}${name}`;
      });
    } else if (coupon.get('scopeType') == 3) {
      scopeType = '分类：';
      text = '仅限';
      coupon.get('scopeNames').forEach((value) => {
        let name = value ? '[' + value + ']' : '';
        text = `${text}${name}`;
      });
    } else {
      scopeType = '商品：';
      text = '部分商品';
    }

    return `${scopeType}${text}`;
  };

  /***
   * 生效时间
   */
  _buildRangDay = (coupon) => {
    return coupon.get('rangeDayType') === 1
      ? `领取后${coupon.get('effectiveDays')}天内有效`
      : `${moment(coupon.get('startTime')).format(Const.DATE_FORMAT)}至${moment(
          coupon.get('endTime')
        ).format(Const.DATE_FORMAT)}`;
  };
}
