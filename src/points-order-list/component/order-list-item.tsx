import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';

import { noop, history, _ } from 'wmkit';
import { IList } from 'typings/globalType';

const noneImg = require('../img/none.png');

/**
 * 订单状态
 * @type {{INIT: string; AUDIT: string; DELIVERED_PART: string; DELIVERED: string; CONFIRMED: string; COMPLETED: string; VOID: string}}
 */
const FLOW_STATE = {
  AUDIT: '待发货',
  DELIVERED_PART: '待发货',
  DELIVERED: '待收货',
  COMPLETED: '已完成'
};

@Relax
export default class OrderListItem extends Component<any, any> {
  props: {
    relaxProps?: {
      getOpeBtnArr: Function;
      orders: IList;
      cancelOrder: Function;
      applyRefund: Function;
      defaultPay: Function;
      inviteeShopName: string;
    };
    order: any;
    index: number;
  };

  static relaxProps = {
    getOpeBtnArr: noop,
    orders: 'orders',
    cancelOrder: noop,
    applyRefund: noop,
    defaultPay: noop,
    inviteeShopName: 'inviteeShopName'
  };

  render() {
    let { order, index } = this.props;
    let { getOpeBtnArr, orders } = this.props.relaxProps;
    const opeBtnArr = getOpeBtnArr(orders.get(index));
    const gifts = order.gifts || [];

    return (
      <div className="ships">
        <div className="order-item">
          <Link to={`/points-order-detail/${order.id}`}>
            <div className="order-head">
              <div className="status">
                {FLOW_STATE[order.tradeState.flowState]}
              </div>
            </div>
            <div className="limit-img ship-img">
              <div className="img-content">
                {order.tradeItems
                  .concat(gifts)
                  .filter((val, index) => index < 4)
                  .map((item) => (
                    <img
                      className="img-item"
                      src={item.pic ? item.pic : noneImg}
                    />
                  ))}
              </div>
              <div className="right-context">
                <div className="total-num">
                  共{order.tradeItems.concat(gifts).length}种
                </div>
                <i className="iconfont icon-jiantou1" />
              </div>
            </div>
          </Link>
          <div className="bottom">
            <div className="price">
              订单积分：{order.tradePrice.points || 0}
            </div>
            <div className="botton-box">
              {opeBtnArr
                ? opeBtnArr.available.map((availableButton) => {
                    return (
                      <div
                        className={'btn btn-ghost btn-small'}
                        onClick={() => {
                          this._operationButtons(order.id, availableButton);
                        }}
                      >
                        {availableButton}
                      </div>
                    );
                  })
                : null}
            </div>
          </div>
        </div>
        <div className="bot-line" />
      </div>
    );
  }

  /**
   * 订单按钮event handler
   */
  _operationButtons = async (tid, button) => {
    if (button == '确认收货') {
      history.push(`/points-order-ship-record/${tid}/0`);
    }
  };
}
