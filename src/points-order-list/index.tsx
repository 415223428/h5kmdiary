import React from 'react';
import {fromJS} from 'immutable';
import {StoreProvider} from 'plume2';

import {history, ListView, PointsBlank} from 'wmkit';

import AppStore from './store';
import Header from './component/header';
import OrderListTop from './component/order-list-top';
import OrderItem from './component/order-list-item';

import './css/style.css';
import OrderCouponItem from './component/order-coupon-item';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class PointsOrderList extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    let state = history.location.state ? history.location.state : null;
    this.store.changeTopActive(state ? state.status : '');
  }

  render() {
    const form = (this.store.state().get('form') || fromJS([])).toJS();
    form['orderType'] = 'POINTS_ORDER';
    const fetchOrders = this.store.fetchOrders;
    const headTabKey = this.store.state().get('headTabKey');

    return (
      <div>
        {/*积分订单*/}
        <Header />

        {/*订单状态tab*/}
        {headTabKey == 0 && <OrderListTop />}

        {/*订单数据*/}
        {headTabKey == 0 && (
          <ListView
            url="/points/trade/page"
            params={form}
            className="order-env"
            renderRow={(order: any, index: number) => {
              if (order.pointsOrderType == 'POINTS_COUPON') {
                return <OrderCouponItem order={order} />;
              } else {
                return <OrderItem order={order} index={index} />;
              }
            }}
            renderEmpty={() => (
              <PointsBlank
                img={require('./img/list-none.png')}
                content="您暂时还没有订单哦"
                isToGoodsList={true}
              />
            )}
            onDataReached={(res) => fetchOrders(res)}
          />
        )}
      </div>
    );
  }
}
