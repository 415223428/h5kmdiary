import { Fetch } from 'wmkit';

/**
 * 提交
 */
export const applyAccount = (payOrder: any) => {
  return Fetch('/trade/pay/offline', {
    method: 'POST',
    body: JSON.stringify(payOrder)
  });
};

/**
 * 订单付款记录
 */
export const fetchPayOrder = (tid: string) => {
  return Fetch(`/trade/payOrder/${tid}`);
};
