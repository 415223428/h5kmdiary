/**
 * Created by chenpeng on 2017/7/22.
 */
import {IOptions, Store} from "plume2";
import {Map} from 'immutable'

import {Alert, history, storage} from "wmkit";
import {cache, config} from 'config'

import {applyAccount, fetchPayOrder} from "./webapi";
import PaymentActor from "./actor/payment-actor";


export default class AppStore extends Store{

	constructor(props: IOptions) {
		super(props);
		if (__DEV__) {
			(window as any)._store = this;
		}
	}


	bindActor() {
		return [ new PaymentActor]
	}


	/**
	 * 初始化订单支付成功
	 */
	initSuccess = async (tid: string) => {
		const { code, context, message} =  await fetchPayOrder(tid)

		if(code === config.SUCCESS_CODE) {
			this.dispatch('fill-payment-success:setPayOrder', context)
		}else {
			Alert({text: message})
		}
	}

	initBySession = async () => {
		if(storage('session').get(cache.PAYMENT_REMARK)){
			this.dispatch('payment-actor:setRemark', storage('session').get(cache.PAYMENT_REMARK));
		}

		if(storage('session').get(cache.PAYMENT_ENCLOSES)){
			this.dispatch('fill-payment-success: addImage', storage('session').get(cache.PAYMENT_ENCLOSES));
		}

		if(storage('session').get(cache.PAYMENT_TIME)){
			this.onSelectDate(new Date(storage('session').get(cache.PAYMENT_TIME).replace(/-/g,'/')))
		}
	}

	/**
	 * 打开/关闭选择付款时间
	 */
	isOpenDate = (isOpen: boolean) => {
		this.dispatch('payment-actor:isOpenDate', isOpen)
	}


	/**
	 * 选择付款时间
	 */
	onSelectDate = (time: Date) => {
		storage('session').set(cache.PAYMENT_TIME, time.Format('yyyy-MM-dd 00:00:00'))
		this.dispatch('payment-actor:onSelectDate', {time:time, isOpen: false})
	}


	/**
	 * 设置选中的收款账号
	 */
	setSelectedAccount = () => {
		if(storage('session').get(cache.SELLER_ACCOUNT)) {
			this.dispatch('payment-actor:setSelectedAccount', JSON.parse(storage('session').get(cache.SELLER_ACCOUNT)))
			storage('session').del(cache.SELLER_ACCOUNT)
		}
	}


	/**
	 * 设置备注
	 */
	setRemark = (remark:string) => {
		storage('session').set(cache.PAYMENT_REMARK, remark);
		this.dispatch('payment-actor:setRemark', remark)
	}


	/**
	 * 提交
	 */
	applyPay = async (tid: string) => {
		const sellerAccount = this.state().get('sellerAccount')

		if(sellerAccount ? sellerAccount.size == 0 : true) {
			Alert({text:'请选择收款账号'})
			return false
		}

		if(sellerAccount ? sellerAccount.time != null : true) {
			Alert({text:'请选择付款时间'})
			return false
		}

		if(this.state().get('remark').length > 100) {
			Alert({text: '付款备注(100字以内)'})
			return false
		}

		if(!this.state().get('payOrder').get('encloses')){
			Alert({text: '请上传附件'})
			return false
		}

		
		let payRecord = Map()
		payRecord = payRecord.set('accountId', sellerAccount.get('accountId'))
		payRecord = payRecord.set('tid', tid)
		payRecord = payRecord.set('createTime', this.state().get('time').Format('yyyy-MM-dd'))
		payRecord = payRecord.set('remark', this.state().get('remark'))

		let encloses = this.state().get('payOrder').get('encloses');
		payRecord = payRecord.set('encloses',encloses);
		const res = await applyAccount(payRecord)
		if(res.code == config.SUCCESS_CODE) {
			history.push({
				pathname: `/fill-payment-success/${tid}`
			})
		}else {
			Alert({text: res.message})
		}
	}

	/**
	 * 上传附件
	 */
	addImage = ( image ) => {
		storage('session').set(cache.PAYMENT_ENCLOSES, image)
		this.dispatch('fill-payment-success: addImage', image)
	}


	/**
	 * 删除附件
	 */
	removeImage = () => {
		storage('session').del(cache.PAYMENT_ENCLOSES)
		this.dispatch('fill-payment-success: removeImage')
	}
}
