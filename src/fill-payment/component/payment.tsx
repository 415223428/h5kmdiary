import React, { Component } from 'react';
import DatePicker from 'react-mobile-datepicker';
import parse from 'url-parse';
import { IMap, Relax } from 'plume2';

import {
  FormInput,
  Tips,
  FormSelect,
  history,
  noop,
  _,
  UploadImage,
  WMImage,
  Alert
} from 'wmkit';

let currentDate = new Date();
const maxDate = new Date(
  currentDate.getFullYear(),
  currentDate.getMonth(),
  currentDate.getDate() + 1
);

@Relax
export default class Payment extends Component<any, any> {
  props: {
    orderId: string;
    relaxProps?: {
      time: Date;
      isOpen: boolean;
      remark: string;
      isOpenDate: Function;
      onSelectDate: Function;
      setSelectedAccount: Function;
      sellerAccount: IMap;
      setRemark: Function;
      payOrder: IMap;
      addImage: Function;
      removeImage: Function;
    };
  };

  static relaxProps = {
    time: 'time',
    remark: 'remark',
    isOpen: 'isOpen',
    isOpenDate: noop,
    onSelectDate: noop,
    setSelectedAccount: noop,
    sellerAccount: 'sellerAccount',
    setRemark: noop,
    payOrder: 'payOrder',
    addImage: noop,
    removeImage: noop
  };

  componentDidMount() {
    const { setSelectedAccount } = this.props.relaxProps;

    setSelectedAccount();
  }

  render() {
    let url = parse(history.location.search, true);
    let query = url.query;
    let { from } = query;

    const {
      time,
      isOpen,
      isOpenDate,
      onSelectDate,
      sellerAccount,
      setRemark,
      payOrder,
      remark
    } = this.props.relaxProps;
    const { orderId } = this.props;

    return (
      <div className="register-box">
        <Tips
          border={true}
          iconName="icon-jinggao-copy"
          text="若您已线下支付，请在此填写付款单"
        />
        <FormSelect
          labelName={'收款账号'}
          placeholder={
            sellerAccount && sellerAccount.size > 0
              ? sellerAccount.get('bankName') +
                ' ' +
                sellerAccount.get('bankNo')
              : '点击选择商家的收款账号'
          }
          onPress={() => {
            history.push({
              pathname: `/seller-account`,
              search: `?from=${from}&tid=${orderId}`
            });
          }}
        />
        <FormSelect
          labelName={'付款时间'}
          placeholder={
            time ? time.Format('yyyy-MM-dd 00:00:00') : '点击选择付款时间'
          }
          onPress={() => isOpenDate(!isOpen)}
        />
        <div className="order-attachment">
          <h4>附件</h4>
          <div className="image-scroll">
            {payOrder.get('encloses') ? (
              <div className="refund-file delete-img">
                <WMImage
                  src={payOrder.get('encloses')}
                  width="53px"
                  height="53px"
                />
                <a href="javascript:void(0)" onClick={() => this.removeImage()}>
                  ×
                </a>
              </div>
            ) : null}
            {payOrder.get('encloses') ? null : (
              <div className="refund-file">
                <UploadImage
                  onSuccess={this._uploadImage}
                  onLoadChecked={this._uploadCheck}
                  repeat
                  className="upload"
                  size={5}
                  fileType=".jpg,.png,.jpeg,.gif"
                >
                  <div className="upload-item">
                    <div className="upload-content">
                      <i className="iconfont icon-add11" />
                    </div>
                  </div>
                </UploadImage>
              </div>
            )}
          </div>
          <p className="file-tips">上传订单付款凭证，如汇款单等</p>
          <p className="file-tips">
            仅支持jpg、jpeg、png、gif文件，最多上传1张，大小不超过5M
          </p>
        </div>

        <FormInput
          label={'备注'}
          placeHolder={'点击输入付款备注（100字以内）'}
          onChange={(e) => setRemark(e.target.value)}
          maxLength={100}
          defaultValue={remark}
        />
        <DatePicker
          value={time}
          isOpen={isOpen}
          theme="ios"
          onSelect={(val) => onSelectDate(val)}
          onCancel={() => isOpenDate(false)}
          max={maxDate}
        />
      </div>
    );
  }

  /**
   * 上传附件
   */
  _uploadImage = (result) => {
    let { context } = result;
    if (context) {
      this.props.relaxProps.addImage(context[0]);
    }
  };

  /**
   * 删除附件
   */
  removeImage = () => {
    this.props.relaxProps.removeImage();
  };

  /**
   * 上传验证
   * @returns {boolean}
   * @private
   */
  _uploadCheck = () => {
    const { payOrder } = this.props.relaxProps;
    const enclosures = payOrder.get('encloses');
    if (enclosures) {
      Alert({ text: '最多只能上传1张图片' });
      return false;
    }
    return true;
  };
}
