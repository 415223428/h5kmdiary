import React from 'react'
import parse from 'url-parse'
import {StoreProvider} from "plume2";

import {Button, history} from 'wmkit'

import AppStore from "./store";
import Payment from "./component/payment";

const LongBlueButton = Button.LongBlue
const submit=require('./img/submit.png')
@StoreProvider(AppStore, {debug: __DEV__})
export default class FillPayment extends React.Component<any, any> {
  store: AppStore


  componentDidMount() {
    this.store.initBySession();
  }

  componentWillUnmount() {
    let url = parse(history.location.search, true)
    let query = url.query
    let { from } = query

    if(from == 'order-detail' && history.action == 'POP') {
      const {tid} = this.props.match.params
      history.push(`/order-detail/${tid}`)
    }else if(from == 'order-list' && history.action == 'POP') {
      history.push('/order-list')
    }
  }


  render() {

    const {tid} = this.props.match.params

    return (
      <div>
        <Payment orderId={tid} />
        <div className="register-btn"style={{padding:'0'}} >
         <img src={submit} alt="" style={{width:'100%'}} onClick={() =>this.store.applyPay(tid)}/>
        </div>
      </div>
    )
  }
}
