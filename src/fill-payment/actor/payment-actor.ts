/**
 * Created by chenpeng on 2017/7/22.
 */
import {Action, Actor, IMap} from "plume2";
import {fromJS} from 'immutable'

export default class PaymentActor extends Actor {
	defaultState() {
		return {
			time: new Date,
			isOpen: false,
			sellerAccount: {},
			remark: '',
			payOrder: {} //付款记录
		}
	}


	/**
	 * 打开/关闭选择付款时间
	 */
	@Action('payment-actor:isOpenDate')
	isOpenDate(state: IMap, isOpen: boolean){
		return state.set('isOpen', isOpen)
	}


	/**
	 * 选择日期
	 */
	@Action('payment-actor:onSelectDate')
	onSelectDate(state: IMap, time: any){
		return fromJS(time)
	}


	/**
	 * 设置选中的收款账号
	 */
	@Action('payment-actor:setSelectedAccount')
	setSelectedAccount(state: IMap, account: any) {
		return state.set('sellerAccount', fromJS(account))
	}


	/**
	 * 设置备注
	 */
	@Action('payment-actor:setRemark')
	setRemark(state: IMap, remark: string) {
		return state.set('remark', remark)
	}


	/**
	 * 设置付款记录
	 */
	@Action('fill-payment-success:setPayOrder')
	setPayOrder(state: IMap, payOrder: any) {
		return state.set('payOrder', fromJS( payOrder))
	}

	/**
	 * 上传附件
	 */
	@Action('fill-payment-success: addImage')
	addImage(state: IMap, image) {
		return state.setIn(['payOrder', 'encloses'], image)
	}

	/**
	 * 删除附件
	 */
	@Action('fill-payment-success: removeImage')
	removeImage(state: IMap) {
		return state.setIn(['payOrder', 'encloses'], null)
	}
}