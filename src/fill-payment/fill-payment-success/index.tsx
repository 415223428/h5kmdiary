import React from 'react';
import { StoreProvider } from 'plume2';
import { Link } from 'react-router-dom';

import { WMImage, history, _ } from 'wmkit';

import AppStore from '../store';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class FillPaymentSuccess extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { tid } = this.props.match.params;
    this.store.initSuccess(tid);
  }

  render() {
    const payOrder = this.store.state().get('payOrder');
    const orderCode = payOrder.get('orderCode');

    return (
      <div
        className="list-none"
        style={{ paddingBottom: 100, paddingTop: '10%', overflowY: 'auto' }}
      >
        <WMImage
          src={require('./img/result-suc.png')}
          width="200px"
          height="200px"
        />
        <div>
          <p>付款单提交成功！</p>
          <p>请等待商家确认。</p>
        </div>
        <div>
          <div className="bot-box">
            <div className="line" />
            <div className="item">
              订单编号
              <span>{orderCode}</span>
            </div>
            <div className="item">
              订单金额
              <span>
                <i className="iconfont icon-qian" />
                {_.addZero(payOrder.get('payOrderPrice'))}
              </span>
            </div>
            <div className="item">
              付款流水号
              <span>{payOrder.get('receivableNo')}</span>
            </div>
          </div>
          <div className="bot-line" />
        </div>
        <div className="bottom-btn">
          <div className="half">
            <button
              className="btn btn-ghost"
              onClick={() => this._toOrderList(orderCode)}
            >
              查看订单
            </button>
          </div>
          <div className="half">
            <Link to="/">
              <button className="btn btn-ghost">返回首页</button>
            </Link>
          </div>
        </div>
      </div>
    );
  }

  /**
   * to查看订单
   */
  _toOrderList = (tid: string) => {
    history.push(`/order-detail/${tid}`);
  };
}
