import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class GoodsActor extends Actor {
  defaultState() {
    return {
      customer:{},
      // 列表/大图视图模式(true:小图 false:大图)
      listView: true,
      // sku/spu视图模式(0:sku 1:spu)
      goodsShowType: 0,
      // 批发类型-规格选择弹框显示与否
      wholesaleVisible: false,
      // 零售类型-规格选择弹框显示与否
      retailVisible: false,
      // 分享弹出显示与否
      shareVisible: false,
      // 用于记录点击打开过规格弹框的的spu信息
      chosenSpu: {},
      //用来存放点击分享赚时候对应的sku
      checkedSku: fromJS({}),
      //是否展示评价相关信息
      isShow: false,
      isfenxiang:false,
      closeImg:true,
      fenxiangId:'',
      closeChoose:true,
      isWechatShare:false,
      stock:0
    };
  }

  /**
   * 切换商品列表视图模式
   */
  @Action('goodsActor:changeLayout')
  changeLayout(state) {
    return state.set('listView', !state.get('listView'));
  }

  /**
   * 切换商品列表-大图/小图
   * (true:小图 false:大图)
   */
  @Action('goodsActor:changeImageShowType')
  changeImageShowType(state, bool) {
    return state.set('listView', bool);
  }

  /**
   * 切换商品列表-sku/spu
   * (0:sku 1:spu)
   */
  @Action('goodsActor:changeGoodsShowType')
  changeGoodsShowType(state, value) {
    return state.set('goodsShowType', value);
  }

  /**
   * 切换批发类型-规格选择框显示与否
   */
  @Action('goodsActor:changeWholesaleVisible')
  changeWholesaleVisible(state, value) {
    return state.set('wholesaleVisible', value);
  }
  /**
   * 切换零售类型-规格选择框显示与否
   */
  @Action('goodsActor:changeRetailVisible')
  changeRetailVisible(state, value) {
    return state.set('retailVisible', value);
  }

  /**
   * 切换分享赚弹框显示与否
   */
  @Action('goodsActor:changeShareVisible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }

  /**
   * 设置选中的spu信息
   * @param state
   * @param context
   * @returns {any}
   */
  @Action('goodsActor:setSpuContext')
  setSpuContext(state, context) {
    return state.set('chosenSpu', context);
  }

  /**
   * 存放点击分享赚时候的sku信息
   * @param state
   * @param sku
   */
  @Action('goods-list:saveSku')
  saveSku(state, sku) {
    return state.set('checkedSku', sku);
  }

  @Action('goods:isShow')
  setIsShow(state, flag) {
    return state.set('isShow', flag);
  }
//打开分享页面
  @Action('goods-actor:isfenxiang')
  isfenxiang(state){
    return state.set('isfenxiang',true)
  }
//关闭分享页面
  @Action('goods-actor:closefenxiang')
  closefenxiang(state){
    return state.set('isfenxiang',false)
  }
  //关闭img
  @Action('goods-actor:closeImg')
  closeImg(state){
    return state
    .set('closeImg',false)
    .set('closeChoose',false)
  }
  //打开Img
  @Action('goods-actor:openImg')
  openImg(state){
    return state
    .set('closeImg',true)
    .set('closeChoose',true)

  }

  @Action('goods-actor:fenxiangId')
  senId(state,fenxiangId){
    return state.set('fenxiangId',fenxiangId)
  }
  @Action('member:init')
  init(state: IMap, { customer }) {
    return state
      .set('customer', fromJS(customer))
  }
  @Action('goods-actor:openWechatShare')
  openWechatShare(state){
    return state.set('isWechatShare',true)
  }

  @Action('goods-actor:closeWechatShare')
  closeWechatShare(state){
    return state.set('isWechatShare',false)
  }
  @Action('goodsactor:stock')
  goodsActorStock(state,stock){
    return state.set('stock',stock)
  }
}
