import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';

const styles = require('../css/style.css');

let tab = [
  {
    name: 'goodsCate',
    label: '分类'
  },
  {
    name: 'price',
    label: '价格'
  },
  {
    name: 'goodsSort',
    label: '排序'
  },
  {
    name: 'goodsFilter',
    label: '筛选'
  }
];
const sorts = {
  default: '综合',
  dateTime: '最新',
  salesNum: '销量',
  price: '价格',
  evaluateNum: '评论数',
  praise: '好评',
  collection: '收藏'
};

@Relax
export default class GoodsTab extends Component<any, any> {
  props: {
    relaxProps?: {
      openShade: Function;
      closeShade: Function;
      tabName: string;
      selectedCate: any;
      sortType: any;
      setSort: Function;
    };
  };

  static relaxProps = {
    openShade: noop,
    closeShade: noop,
    tabName: 'tabName',
    selectedCate: 'selectedCate',
    sortType: 'sortType',
    setSort: noop,
  };

  render() {
    const {
      openShade,
      closeShade,
      tabName,
      selectedCate,
      sortType,
      setSort
    } = this.props.relaxProps;
    // 排序字段
    const sort = sortType.get('type');
    const Issort = sortType.get('sort');

    return (
      <div className={styles.menuBar}>
        <div className={styles.menuContent + ' b-1px-t'} style={{ position: "unset" }}>
          {tab.map((v) => {
            // 是否是当前已展开的tab
            const match = v.name === tabName;

            return (
              <div
                className="item"
                key={v.name}
              // onClick={() => {
              //   // 在相同的tab上点击时，认为是要关闭tab
              //   match ? closeShade() : openShade(v.name);
              // }}
              >
                {v.name === 'goodsCate'
                  ?
                  <span className={match ? 'new-theme-text' : ''}
                    onClick={() => {
                      // 在相同的tab上点击时，认为是要关闭tab
                      match ? closeShade() : openShade(v.name);
                    }}
                  >
                    {selectedCate.get('cateName')}
                  </span>
                  : v.name == 'goodsSort'
                    ?
                    <span className={match ? 'new-theme-text' : ''}
                      onClick={() => {
                        // 在相同的tab上点击时，认为是要关闭tab
                        match ? closeShade() : openShade(v.name);
                      }}
                    >
                      {sorts[sort] || '排序'}
                    </span>
                    : v.name == 'price' ?
                      <span className={match ? 'new-theme-text' : ''} 
                      onClick={() => setSort('price')}
                      >
                        价格
                        </span>
                      :
                      <span className={match ? 'new-theme-text' : ''}
                        onClick={() => {
                          // 在相同的tab上点击时，认为是要关闭tab
                          match ? closeShade() : openShade(v.name);
                        }}>
                        {v.label}
                      </span>
                }


                {
                  v.name === 'price' ?
                    (<div className="sort-item" onClick={() => setSort('price')}>
                      {
                        sort == 'price' && Issort == 'asc' ?
                        <img src={require('../img/asc.png')} alt="" style={{ width: ".16rem", height: ".18rem" }}></img>
                        : 
                        sort == 'price' && Issort == 'desc' ?
                        <img src={require('../img/desc.png')} alt="" style={{ width: ".16rem", height: ".18rem" }}></img>
                        :
                        <img src={require('../img/empty.png')} alt="" style={{ width: ".16rem", height: ".18rem" }}></img>
                      }
                    </div>
                    ) :                 
                    // v.name === 'goodsFilter' ? 
                    // (
                    //   <i className="iconfont icon-shaixuan" />
                    // ) :
                    match ? (
                      <i
                        className="iconfont icon-jiantou2 new-theme-text"
                        style={{ transform: 'rotate(180deg)' }}
                      />
                    ) : (
                      <i className="iconfont icon-jiantou2" />
                    )
                }
              </div>

            );
          })}
        </div>
      </div>

    );
  }
}
