import React from 'react'
import {StoreProvider} from 'plume2'
import {history, FormSelect} from 'wmkit'
import AppStore from './store'
import './css/user-finance.css'

@StoreProvider(AppStore, {debug: __DEV__})
export default class UserFinance extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init();
  }

  render() {
    return (
      <div className="register-box user-fin-box" style={{paddingBottom:0}}>
        <FormSelect labelName={'银行账户'} onPress={() => {history.push('/user-account')}}/>
        <FormSelect labelName={'增票资质'} onPress={() => {history.push('/user-invoice')}}/>
        {this.store.state().get('showEmailList') && <FormSelect labelName={'财务邮箱'} onPress={() => {history.push('/user-email')}}/>}
      </div>
    )
  }
}
