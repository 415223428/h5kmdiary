import {Fetch} from 'wmkit';


/**
 * 查询银联企业支付配置
 */
export const queryUnionB2bConfig = () => {
  return Fetch<Result<any>>('/pay/queryUnionB2bConfig/', {
    method: 'GET'
  })
}

