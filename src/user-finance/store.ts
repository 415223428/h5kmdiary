import {Store, IOptions} from 'plume2';
import * as webApi from './webapi'
import EmailActor from "./email-actor";

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new EmailActor]
  }

  init = async () => {
    const {context} = await webApi.queryUnionB2bConfig() as any;
    if (context.payGateway.isOpen == 1) {
      this.dispatch('email:list:show', true);
    }
  };
}
