import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import moment from 'moment';
import { Const } from 'config';
import { FormItem, _ } from 'wmkit';
import AppStore from '../store';

@Relax
export default class FinanceRefundRecord extends Component<any, any> {
  store: AppStore;

  props: {
    relaxProps?: {
      refundRecord: any;
      pointsIsOpen: boolean;
    };
  };

  static relaxProps = {
    refundRecord: 'refundRecord',
    pointsIsOpen: 'pointsIsOpen'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { refundRecord, pointsIsOpen } = this.props.relaxProps;
    //退款状态
    const returnFlowState = refundRecord.get('returnFlowState');
    //退款方式
    const payType = refundRecord.get('payType');
    //收款账户
    const returnAccount = refundRecord.get('returnAccount');
    //应退金额
    const returnPrice = refundRecord.get('returnPrice');
    //收款账户名称
    const returnAccountName = refundRecord.get('returnAccountName');

    //商家退款账户
    const customerAccountName = refundRecord.get('customerAccountName');
    //实退金额
    const actualReturnPrice = refundRecord.get('actualReturnPrice');
    //退款时间
    const refundBillTime = refundRecord.get('refundBillTime');
    //备注
    const comment = refundRecord.get('comment');
    //流水号
    const refundBillCode = refundRecord.get('refundBillCode');
    return (
      <div>
        <FormItem
          labelName="应退金额"
          placeholder={
            <div className="price-color">
              <i className="iconfont icon-qian" />
              {_.addZero(returnPrice)}
            </div>
          }
          textStyle={{ color: 'red' }}
        />
        <FormItem
          labelName="退款方式"
          placeholder={payType == 0 ? '在线支付' : '线下支付'}
        />
        <div className={payType == 0 ? 'hide' : 'show-block'}>
          <FormItem
            labelName="商家退款账户"
            placeholder={
              <div>
                <div>
                  {returnAccountName && returnAccountName.split(' ')[0]}
                </div>
                <div>
                  {returnAccountName && returnAccountName.split(' ')[1]}
                </div>
                <div>
                  {returnAccountName == null || returnAccountName == ''
                    ? '无'
                    : ''}
                </div>
              </div>
            }
          />
        </div>
        <FormItem
          labelName="收款账户"
          placeholder={
            <div>
              <div>
                {customerAccountName && customerAccountName.split(' ')[0]}
              </div>
              <div>
                {customerAccountName && customerAccountName.split(' ')[1]}
              </div>
              <div>
                {customerAccountName == null || customerAccountName == ''
                  ? '无'
                  : ''}
              </div>
            </div>
          }
        />
        <FormItem
          labelName="实退金额"
          placeholder={
            returnFlowState == 'COMPLETED' ? (
              <div>
                <i className="iconfont icon-qian" />
                {_.addZero(actualReturnPrice)}
              </div>
            ) : (
              '无'
            )
          }
        />
        <FormItem
          labelName="退款时间"
          placeholder={
            refundBillTime == null || refundBillTime == ''
              ? '无'
              : moment(refundBillTime).format(Const.SECONDS_FORMAT)
          }
        />
        <FormItem
          labelName="备注"
          placeholder={comment == '' || comment == null ? '无' : comment}
        />
        <FormItem
          labelName="流水号"
          placeholder={returnFlowState == 'COMPLETED' ? refundBillCode : '无'}
        />

        {pointsIsOpen && (
          <FormItem
            labelName="应退积分"
            placeholder={refundRecord.get('applyPoints')}
          />
        )}
        {pointsIsOpen && (
          <FormItem
            labelName="实退积分"
            placeholder={refundRecord.get('actualPoints')}
          />
        )}
      </div>
    );
  }
}
