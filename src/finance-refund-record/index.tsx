import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import FinanceRefundRecord from './component/finance-refund-record';

/**
 * 退款记录页面
 */
@StoreProvider(AppStore)
export default class RefundRecord extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { rid } = this.props.match.params;
    const returnFlowState = this.props.location.returnFlowState;
    this.store.init(rid, returnFlowState);
    this.store.basicRules();
  }

  constructor(props) {
    super(props);
  }

  render() {
    return <FinanceRefundRecord />;
  }
}
