import React from 'react';
import { Fetch } from 'wmkit';
import { config } from 'config';

export default class Download extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      // 背景图
      bgUrl: '',
      // 提示框可见性
      modalVisible: false
    };
  }

  componentDidMount() {
    this._init();
  }

  render() {
    return (
      <div
        className="download"
        style={{
          backgroundImage: `url(${this.state.bgUrl})`
        }}
      >
        {this.state.modalVisible && (
          <div className="tip_dialog">
            <div className="tip">
              <p>
                1.点击右上角&nbsp;
                <img className="dots" src={require('./img/dots.png')} alt="" />
                &nbsp;按钮
              </p>
              <p>2.选择“在浏览器中打开”下载App</p>
            </div>

            <div className="close_box">
              <a
                href="javascript:;"
                onClick={() => this.setState({ modalVisible: false })}
                className="close_btn"
              >
                我知道了
              </a>
            </div>
          </div>
        )}
      </div>
    );
  }

  _init = async () => {
    // 1.查询当前操作系统
    let os = (function() {
      let ua = navigator.userAgent,
        isWeixin = /(?:micromessenger)/.test(ua.toLowerCase()),
        isQB = /(?:MQQBrowser|QQ)/.test(ua),
        isWindowsPhone = /(?:Windows Phone)/.test(ua),
        isSymbian = /(?:SymbianOS)/.test(ua) || isWindowsPhone,
        isAndroid = /(?:Android)/.test(ua),
        isFireFox = /(?:Firefox)/.test(ua),
        isChrome = /(?:Chrome|CriOS)/.test(ua),
        isIpad = /(?:iPad|PlayBook)/.test(ua),
        isTablet =
          /(?:iPad|PlayBook)/.test(ua) || (isFireFox && /(?:Tablet)/.test(ua)),
        isSafari = /(?:Safari)/.test(ua),
        isIPhone = /(?:iPhone)/.test(ua) && !isTablet,
        isOpen = /(?:Opera Mini)/.test(ua),
        isUC = /(?:UCWEB|UCBrowser)/.test(ua),
        isPc = !isIPhone && !isAndroid && !isSymbian;
      return {
        isWeixin: isWeixin,
        isQB: isQB,
        isTablet: isTablet,
        isIPhone: isIPhone,
        isAndroid: isAndroid,
        isPc: isPc,
        isOpen: isOpen,
        isUC: isUC,
        isIpad: isIpad
      };
    })();

    // 2.查询APP分享信息
    const res = (await Fetch('/mobile-setting/get-app-share-setting')) as any;
    if (res.code == config.SUCCESS_CODE) {
      this.setState({ bgUrl: res.context.downloadImg });
      if (os.isWeixin) {
        this.setState({ modalVisible: true });
      } else if (os.isIPhone) {
        window.location.href =
          'itms-services://?action=download-manifest&url=' + res.context.iosUrl;
      } else if (os.isAndroid) {
        window.location.href = res.context.androidUrl;
      } else {
        alert('请用IOS或者Android系统下载');
      }
    }
  };
}
