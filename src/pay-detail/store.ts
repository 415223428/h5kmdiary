import {Store} from "plume2";

import PayDetailActor from "./actor/pay-detail-actor";
import {fetchPayDetail, defaultPaylOrder} from "./webapi";
import {Alert, history} from "wmkit";


export default class AppStore extends Store {
	bindActor() {
		return [new PayDetailActor]
	}


	/**
	 * 初始化付款记录
	 */
	init = async (tid: string) => {
		const res = await fetchPayDetail(tid)
		if(res.code == 'K-000000') {
			this.dispatch('pay-detail-actor:set', res.context)
		}
	}

  /**
   * 0元支付
   */
  defaultPay = async (tid: string) => {
    const res = await defaultPaylOrder(tid);
    if (res.code == 'K-000000') {
      //跳转到付款成功页
      history.push({
        pathname: `/fill-payment-success/${tid}`
      });
    } else {
      Alert({ text: res.message });
    }
  };

}