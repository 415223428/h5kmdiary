/**
 * Created by chenpeng on 2017/7/27.
 */
import {fromJS} from 'immutable'
import {Action, Actor, IMap} from "plume2";

export default class PayDetailActor extends Actor {


	defaultState() {
		return {
			payDetail:{}   //付款记录
		}
	}


	/**
	 * 设置付款记录
	 */
	@Action('pay-detail-actor:set')
	setPayDetail(state: IMap, pay){
		return state.set('payDetail', fromJS(pay))
	}
}
