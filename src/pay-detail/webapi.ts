/**
 * Created by chenpeng on 2017/7/27.
 */
import {Fetch} from "wmkit";

/**
 * 付款记录
 */
export const fetchPayDetail = (tid: string) => {
	return Fetch(`/trade/payOrder/${tid}`)
}

/**
 * 0元订单支付
 */
export const defaultPaylOrder = (tid: string) => {
  return Fetch(`/trade/default/pay/${tid}`)
}
