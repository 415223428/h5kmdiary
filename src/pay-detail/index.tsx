import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import { fromJS, Map } from 'immutable';
import { FormItem, _, history, ImageListScroll, storage, WMImage } from 'wmkit';
import { cache } from 'config';
import AppStore from './store';
const pay=require('./img/pay.png')
const PAY_TYPE = {
  0: '线上支付',
  1: '线下支付'
};

const PAY_STATUS = {
  0: '已付款',
  1: '未付款',
  2: '待确认'
};

@StoreProvider(AppStore, { debug: __DEV__ })
export default class PayDetail extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { tid } = this.props.match.params;
    this.store.init(tid);
  }

  render() {
    const { tid } = this.props.match.params;
    let payDetail = this.store.state().get('payDetail');

    let imageList = Array();
    if (payDetail.get('encloses')) {
      // JSON.parse(payDetail.get("encloses"))[0].url
      imageList.push({ image: payDetail.get('encloses') });
    }
    return (
      <div>
        <FormItem
          labelName="应付积分"
          placeholder={
            <div>
              <i className="iconfont" style={{ color: 'red' }} />
              {payDetail.get('payOrderPoints')
                ? payDetail.get('payOrderPoints')
                : 0}
            </div>
          }
          textStyle={{ color: 'red' }}
        />
        <FormItem
          labelName="付款记录"
          placeholder={PAY_STATUS[payDetail.get('payOrderStatus')] || '无'}
          style={{ height: "1.1rem" }}
        />
        <FormItem
          labelName="付款记录"
          placeholder={PAY_STATUS[payDetail.get('payOrderStatus')] || '无'}
          style={{ height: "1.1rem" }}
        />
        <FormItem
          labelName="付款方式"
          placeholder={PAY_TYPE[payDetail.get('payType')]}
          style={{ height: "1.1rem" }}
        />
        <FormItem
          labelName="付款时间"
          placeholder={
            payDetail.get('createTime')
              ? _.formatDate(payDetail.get('createTime'))
              : '无'
          }
          style={{ height: "1.1rem" }}
        />
        {/*<WMImage src={payDetail.get('encloses')} width="56px" height="56px" />*/}
        <FormItem
          labelName="备注"
          placeholder={payDetail.get('comment') || '无'}
          style={{ height: "1.1rem" }}
        />
        {payDetail.get('payOrderStatus') &&
          payDetail.get('payOrderStatus') == '1' ? (
            <div style={{ height: 48 }}>
              <div className="order-button-wrap "
                style={{
                  padding:'0',
                  height:".98rem",
                }}
              >
                <div
                  // className="btn btn-ghost-red btn-small"
                  onClick={() => this._toPay(tid, payDetail.get('payType'))}
                >
                  <img src={pay} alt="" style={{width:'100%'}}/>
              </div>
              </div>
            </div>
          ) : null}
      </div>
    );
  }

  /**
   * 去支付
   */
  _toPay = (tid: string, payType: string) => {
    const payDetail = this.store.state().get('payDetail');
    if (payDetail && payDetail.get('totalPrice') == '0.00') {
      //0元订单直接调默认支付接口
      this.store.defaultPay(tid);
    }

    if (payType == '1') {
      this._clearCache();
      history.push({
        pathname: `/fill-payment/${tid}`,
        search: 'from=order-detail'
      });
    } else if (payType == '0') {
      history.push(`/pay-online/${tid}`);
    }
  };

  _clearCache = async () => {
    storage('session').del(cache.PAYMENT_ENCLOSES);
    storage('session').del(cache.PAYMENT_REMARK);
    storage('session').del(cache.PAYMENT_TIME);
  };
}
