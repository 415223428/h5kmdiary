/**
 * Created by chenpeng on 2017/7/11.
 */
import { Fetch } from 'wmkit';
type TResult = { code: string; message: string; context: any };

/**
 * 获取增值税专用发票
 * @returns {Promise<Result<TResult>>}
 */
export const fetchVATInvoice = (companyInfoId) => {
  return Fetch<TResult>(`/customer/invoiceInfo/${companyInfoId}`);
};

/**
 * 获取普通发票的开票项目
 * @returns {Promise<Result<TResult>>}
 */
export const fetchProjects = (companyInfoId) => {
  return Fetch<TResult>(`/account/invoiceProjects/${companyInfoId}`);
};
