import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import { Tips } from 'wmkit';
import AppStore from './store';
import InvoiceTab from './component/invoice-tab';
import InvoicePaper from './component/invoice-paper';
import InvoiceAddedValue from './component/invoice-add-value';
import InvoiceSave from './component/invoice-save';
const styles = require('./css/style.css');
@StoreProvider(AppStore, { debug: __DEV__ })
export default class RequestInvoice extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { cid } = this.props.match.params;
    this.store.init(cid);
  }

  render() {
    const { cid } = this.props.match.params;
    const configFlag = this.store.state().get('configFlag');

    return (
      <div style={{ background: '#fafafa', paddingBottom: 85 }}>
        {configFlag ? (
          <Tips
            iconName="icon-jinggao-copy"
            border={true}
            text="如需增值税专用发票，需提前提交增票资质给工作人员审核"
          />
        ) : null}
        <InvoiceTab companyInfoId={cid} />
        <InvoicePaper companyInfoId={cid} />
        <InvoiceAddedValue companyInfoId={cid} />
        <InvoiceSave companyInfoId={cid} />
      </div>
    );
  }
}
