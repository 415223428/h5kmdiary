import React, { Component } from "react";
import { Relax, IMap } from "plume2";
import { cache } from 'config'
import { history, noop, Alert } from 'wmkit'
import { fromJS } from 'immutable'

@Relax
export default class InvoiceSave extends Component<any, any> {

  props: {
    companyInfoId: number
    relaxProps?: {
      sperator: boolean
      defaultInvoiceAddr: IMap

      save: Function
    }
  }

  static relaxProps = {
    sperator: 'sperator',
    defaultInvoiceAddr: 'defaultInvoiceAddr',

    save: noop
  }


  render() {
    return (
      <div className="register-btn">
        <button onClick={() => this._onSave(this.props.companyInfoId)} className="btn btn-primary">保存</button>
      </div>
    )
  }


  /**
   * 保存
   * @private
   */
  _onSave = (companyInfoId) => {
    const { sperator, defaultInvoiceAddr } = this.props.relaxProps
    if (sperator && !defaultInvoiceAddr.get('deliveryAddressId')) {
      Alert({ text: '请选择单独的发票收货地址' });
      return
    }
    this.props.relaxProps.save({ companyInfoId, page: '/flash-sale-order-confirm' })
  }
}
