import React from 'react'
import { Relax, IMap } from 'plume2'

import { Switch, noop, FormSelect, history, FindArea } from 'wmkit'
@Relax
export default class InvoiceAddress extends React.Component<any, any>{

  constructor(props) {
    super(props)
  }

  props: {
    companyInfoId: number
    relaxProps?: {
      sperator: boolean
      invoice: IMap
      defaultInvoiceAddr: IMap
      defaultAddr: IMap
      storeId: number

      saveTemp: Function
      onSwitchSperator: Function
    }
  }


  static relaxProps = {
    sperator: 'sperator',
    invoice: 'invoice',
    defaultInvoiceAddr: 'defaultInvoiceAddr',
    defaultAddr: 'defaultAddr',
    storeId: 'storeId',

    saveTemp: noop,
    onSwitchSperator: noop,
  }


  render() {
    const { sperator, invoice, onSwitchSperator,
      defaultInvoiceAddr, defaultAddr, storeId } = this.props.relaxProps

    const addressDatail = FindArea.addressInfo(
        defaultInvoiceAddr ? defaultInvoiceAddr.get('provinceId') : '',
        defaultInvoiceAddr ? defaultInvoiceAddr.get('cityId') : '',
        defaultInvoiceAddr ? defaultInvoiceAddr.get('areaId') : '')
        + (defaultInvoiceAddr ? defaultInvoiceAddr.get('deliveryAddress') : '')
    return (
      <div style={{ marginTop: 10 }}>
        <div className="switch-input">
          <span className="form-text">使用单独的发票收货地址</span>
          <Switch switched={sperator} onSwitch={() => onSwitchSperator(!sperator)} />
        </div>
        {
          sperator ?
            defaultInvoiceAddr && defaultInvoiceAddr.get('deliveryAddressId') ?
              <div className="address-info" onClick={() => this._goToChoose({
                storeId,
                companyInfoId: this.props.companyInfoId
              })}>
                <i className="iconfont icon-dz posit"></i>
                <div className="address-content">
                  <div className="address-detail">
                    <div className="name">
                      <span>收货人：{defaultInvoiceAddr.get('consigneeName')}</span>
                      <span>{defaultInvoiceAddr.get('consigneeNumber')}</span>
                    </div>
                    <p>收货地址：{addressDatail}</p>
                  </div>
                  <i className="iconfont icon-jiantou1"></i>
                </div>
              </div>
              :
              <FormSelect
                labelName={defaultAddr && defaultAddr.get('deliveryAddressId')
                  ? '点击选择发票收货地址' : '您的收货地址为空，点击新增收货地址'}
                onPress={() => this._goToChoose({
                  storeId,
                  companyInfoId: this.props.companyInfoId
                })} />
            : null
        }
      </div>
    )
  }


  /**
   * 初次进入页面选择地址
   * @private
   */
  _goToChoose = ({ storeId, companyInfoId }) => {
    const { defaultAddr, saveTemp } = this.props.relaxProps
    saveTemp({
      companyInfoId,
      comeFroms: defaultAddr && defaultAddr.get('deliveryAddressId')
        ? `invoiceAddress_${storeId}` : `firstDefaultInvoiceAddr_${storeId}`,
      page: (defaultAddr && defaultAddr.get('deliveryAddressId'))
        ? '/receive-address' : '/receive-address-edit/-1'
    })
  }


  /**
   * 下一步
   * @private
   */
  _goToNext = (companyInfoId) => {
    const { saveTemp } = this.props.relaxProps
    saveTemp({
      companyInfoId,
      comeFroms: 'invoiceAddress',
      page: '/receive-address'
    })
  }
}