import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { fromJS } from 'immutable';
import { noop, FormText, RadioHook, RadioBox } from 'wmkit';
import { IList } from 'typings/globalType';
import InvoiceAddress from '../component/invoice-address';

const data = [{ id: '0', name: '个人' }, { id: '1', name: '单位' }];

@Relax
export default class InvoicePaper extends Component<any, any> {
  props: {
    companyInfoId: number;
    relaxProps?: {
      key: string;
      cInvoice: IMap;
      projects: IList;

      initProjectActive: Function;
      initPaperFlag: Function;
      saveInvoiceTitle: Function;
      saveIdentification: Function;
    };
  };

  static relaxProps = {
    key: 'key',
    cInvoice: 'cInvoice',
    projects: 'projects',

    initProjectActive: noop,
    initPaperFlag: noop,
    saveInvoiceTitle: noop,
    saveIdentification: noop
  };

  render() {
    const {
      key,
      cInvoice,
      initPaperFlag,
      saveInvoiceTitle,
      projects,
      initProjectActive,
      saveIdentification
    } = this.props.relaxProps;
    const flag = cInvoice.get('flag');
    const projectKey = cInvoice.get('projectKey');
    return (
      <div>
        {key === 'paper' ? (
          <div style={{ marginTop: 10, paddingBottom: 85 }}>
            <div>
              <div className="order-wrap top-border">
                <RadioHook
                  checked={flag}
                  data={fromJS(data)}
                  onCheck={(v) => initPaperFlag(v)}
                />
              </div>
              {flag == 1 ? (
                <div>
                  <FormText
                    label="发票抬头"
                    placeholder="请填写单位名称"
                    onChange={(e) => saveInvoiceTitle(e.target.value)}
                    value={cInvoice.get('title')}
                    maxLength={50}
                    fontMini={true}
                  />
                  <FormText
                    label="纳税人识别号"
                    placeholder="填写错误将不能作为税收凭证或报销"
                    onChange={(e) => saveIdentification(e.target.value)}
                    value={cInvoice.get('identification')}
                    maxLength={20}
                    fontMini={true}
                  />
                </div>
              ) : null}
            </div>
            <div className="invoice-list">
              <h2>开票项目</h2>
              <div className="invoice-scroll">
                <RadioBox
                  data={projects.toJS()}
                  onCheck={(val) => initProjectActive(val)}
                  checked={projectKey}
                  style={{ width: '29.3%' }}
                />
              </div>
            </div>
            <InvoiceAddress companyInfoId={this.props.companyInfoId} />
          </div>
        ) : null}
      </div>
    );
  }
}
