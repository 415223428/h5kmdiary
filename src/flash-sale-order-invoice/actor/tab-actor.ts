import {Action, Actor, IMap} from "plume2";
/**
 * Created by chenpeng on 2017/7/7.
 */

export default class TabActor extends Actor {
  defaultState() {
    return {
      key: ''//tab-key
    }
  }


  /**
   * 初始化tab项
   * @param state
   * @param key
   * @returns {Map<string, string>}
   */
  @Action('tab:init')
  init(state: IMap, key: string) {
    return state.set('key', key)
  }
}
