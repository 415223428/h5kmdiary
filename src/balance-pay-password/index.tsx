import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import FormItem from './component/safepass-form-item';
import './balance-pay-password.css'

const next=require('./img/next.png')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class UserSafePassword extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    //判断是忘记密码还是设置密码
    const forget =
      this.props.location.state && this.props.location.state.forget;
    this.store.init(forget);
  }

  render() {
    return (
      <div className="content register-content">
        <FormItem />
        <div className="register-btn balance-password-btn" style={{padding: '0', width: '100%'}}>
          {/* <button
          style={{background:'linear-gradient(to right,#FF6A4D, #FF1A1A)'}}
            className="btn btn-primary"
            onClick={() => {
              this.store.doNext();
            }}
          >
            下一步
          </button> */}
          <img src={next} alt="" style={{width:'100%'}}
              onClick={() => {
                this.store.doNext();
              }}/>
        </div>
      </div>
    );
  }
}
