import React from 'react'
import {Link} from 'react-router-dom'
import {Relax} from "plume2";
import {noop, Button, FormInput, Alert, WMkit, FormItem, ValidConst} from 'wmkit'
import AppStore from '../store'
import '../balance-pay-password.css'
const TimerButton = Button.Timer

@Relax
export default class SafeFormItem extends React.Component<any, any> {
  store: AppStore


  props: {
    relaxProps?: {
      code: string,
      mobile: string,
      buttonValue: string,
      getMobile: Function,
      getCode: Function,
      sendCode: Function
    }
  };


  static relaxProps = {
    code: 'code',
    mobile: 'mobile',
    buttonValue: 'buttonValue',
    getMobile: noop,
    getCode: noop,
    sendCode: noop
  }


  constructor(props: any) {
    super(props)
  }


  render() {
    const {mobile, code, getMobile, getCode, buttonValue, sendCode} = this.props.relaxProps
    return (
      <div className="register-box balance-password-box" style={{padding:'0', paddingBottom: '85px'}}>
        {/*如果已登录，则是修改密码，否则是忘记密码*/}
        {
          WMkit.isLogin() ? <FormItem labelName="手机号" placeholder={mobile}/> :
            <FormInput label="手机号" type="number" placeHolder="请输入您的手机号" pattern="[0-9]*" defaultValue={mobile}
                       onChange={(e) => getMobile(e.target.value)} maxLength={11}/>
        }
        <FormInput label="验证码" type="number" placeHolder="请输入验证码" pattern="[0-9]*" defaultValue={code}
                   onChange={(e) => getCode(e.target.value)} maxLength={6} other={<TimerButton text={buttonValue}
                                                                                               shouldStartCountDown={() => this._sendCode(mobile)}
                                                                                               resetWhenError
                                                                                               onClick={() => sendCode()}/>}/>
        <div className="register-tips balance-tips">
          <p>提示：</p>

          <p>1.为了保障您的账户信息安全，在您变更账户重要信息时，需要对您的身份进行验证。感谢您的理解和支持！</p>

          <p>2.如出现收不到短信的情况，可能是由于通信网络异常造成，请您稍后重新尝试操作！</p>
        </div>
      </div>
    )
  }


  _sendCode = (tel: string) => {
    const regex = ValidConst.phone
    if (tel == "") {
      Alert({
        text: "请填写手机号！"
      })
      return false
    } else if (!regex.test(tel)) {
      Alert({
        text: "无效的手机号！"
      })
      return false
    } else {
      return true
    }
  }
}
