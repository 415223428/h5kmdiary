import { Fetch, WMkit } from 'wmkit';

type TResult = { code: string; message: string; context: any };
/**
 * 初始化商品详情
 * @param id
 * @returns {Promise<Result<TResult>>}
 */
export const init = async (id) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  return isLoginOrNotOpen
    ? Fetch<Result<any>>(`/goods/groupon/goods-detail/spu/${id}`)
    : Fetch<Result<any>>(`/goods/unLogin/groupon/goods-detail/spu/${id}`);
  // return Fetch<Result<any>>(`/goods/sku/${id}`);
};

/**
 * 根据品牌id查询品牌详情
 * @param brandId
 */
export const fetchBrand = (brandId: string) =>
  Fetch(`/goods/goodsBrand/${brandId}`, { method: 'GET' });

/**
 * 根据分类id查询属性信息
 * @param cateId
 */
export const fetchPropList = (cateId: string) =>
  Fetch(`/goods/props/all/${cateId}`, { method: 'GET' });

/**
 * 加入收藏
 * @param id
 */
export const intoFollow = (id) => {
  return Fetch<Result<any>>('/goods/goodsFollow', {
    method: 'POST',
    body: JSON.stringify({
      goodsInfoId: id
    })
  });
};

/**
 * 移出收藏
 * @param id
 * @returns {Promise<Result<TResult>>}
 */
export const outFollow = (id) => {
  return Fetch<Result<any>>('/goods/goodsFollow', {
    method: 'DELETE',
    body: JSON.stringify({
      goodsInfoIds: [id]
    })
  });
};

/**
 * 查询商品是否被收藏
 * @param id
 * @returns {Promise<Result<TResult>>}
 */
export const fetchFollow = (id) => {
  return Fetch<Result<any>>('/goods/goodsFollows', {
    method: 'POST',
    body: JSON.stringify({
      goodsInfoId: id
    })
  });
};

/**
 * 加入购物车
 * @param id
 * @param num
 * @returns {Promise<Result<TResult>>}
 */
export const purchase = (id, num) => {
  return Fetch<Result<any>>('/site/purchase', {
    method: 'POST',
    body: JSON.stringify({
      goodsInfoId: id,
      goodsNum: num
    })
  });
};

/**
 * 查询购物车数量
 * @returns {Promise<Result<T>>}
 */
export const fetchPurchaseCount = () => {
  return Fetch('/site/countGoods');
};

/**
 * 获取店铺基本信息
 * @param id 店铺Id
 */
export const fetchStoreInfo = (id) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  const params = {
    storeId: id
  };
  return isLoginOrNotOpen
    ? Fetch('/store/storeInfo', {
        method: 'POST',
        body: JSON.stringify(params)
      })
    : Fetch('/store/unLogin/storeInfo', {
        method: 'POST',
        body: JSON.stringify(params)
      });
};

/**
 * 获取促销活动详情
 * @param id
 */
export const fetchMarketingDetail = (id) => {
  return WMkit.isLoginOrNotOpen()
    ? Fetch<TResult>(`/gift/${id}`)
    : Fetch<TResult>(`/gift/unLogin/${id}`);
};

/**
 * 获取客服信息
 */
export const fetchOnlineServiceList = (storeId) => {
  return Fetch(`/customerService/qq/detail/${storeId}/1`);
};

/**
 * 获取商品关联的优惠券信息
 * @param {*} goodsInfoId
 */
export const fetchCouponInfos = (goodsInfoId) => {
  const url = WMkit.isLoginOrNotOpen()
    ? `/coupon-info/goods-detail/${goodsInfoId}`
    : `/coupon-info/front/goods-detail/${goodsInfoId}`;
  return Fetch(url);
};

/**
 * 领取优惠券
 * @param {*} couponId 优惠券Id
 * @param {*} activityId 活动Id
 */
export const receiveCoupon = (couponId, activityId) => {
  return Fetch('/coupon-code/fetch-coupon', {
    method: 'POST',
    body: JSON.stringify({
      couponInfoId: couponId,
      couponActivityId: activityId
    })
  });
};

/**
 * 查询spu详情
 * @param id
 */
export const initSpu = (id) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  return isLoginOrNotOpen
    ? Fetch(`/goods/spu/${id}`)
    : Fetch(`/goods/unLogin/spu/${id}`);
};

/**
 * 查询店铺信息
 */
export const findStore = async (storeId) => {
  const isLoginOrNotOpen = WMkit.isLoginOrNotOpen();
  return isLoginOrNotOpen
    ? Fetch<Result<any>>('/store/storeInfo', {
        method: 'POST',
        body: JSON.stringify({
          storeId
        })
      })
    : Fetch<Result<any>>('/store/unLogin/storeInfo', {
        method: 'POST',
        body: JSON.stringify({
          storeId
        })
      });
};

/**
 * 查询店铺是否是有效状态
 * @returns {Promise<Result<T>>}
 */
export const fetchShopStatus = () => {
  return Fetch('/distribute/check/status');
};

/**
 * 获取服务时间
 */
export const queryServerTime = () => {
  return Fetch('/system/queryServerTime');
};

/**
 * 获取团实例信息
 */
export const initGrouponNotice = (grouponNo) => {
  return Fetch<Result<any>>('/groupon/grouponInstanceInfo/' + grouponNo);
};
export const shareInfo = () => {
  return Fetch('/customer/customerCenter')
}
