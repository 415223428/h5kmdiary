import React, { Component } from 'react';
import { List, fromJS } from 'immutable';
import { Relax, IMap } from 'plume2';
import { history } from 'wmkit';

const windowWidth = window.innerWidth.toString() + 'px';

@Relax
export default class GroupPlay extends Component<any, any> {
  props: {
    relaxProps?: {};
  };

  static relaxProps = {};

  render() {
    return (
      <div className="group-play" onClick={() => history.push('/groupon-rule')}>

        <div className="pull-text" style={{
          background:'#fff'
        }}>
          <div className="line-border" style={{
            width: '50%',
            background: '#333333'
          }} ></div>
          <div className="over-text">
            <span style={{
              fontWeight: 'bold',
              color: '#333333'
            }}>拼团玩法</span>
          </div>
        </div>



        <div className="group-box">
          <span className="group-title">拼团玩法</span>
          <span className="see-more">
            <span className="more">玩法详情</span>
            <i className="iconfont icon-jiantou1" />
          </span>
        </div>
        <div className="play-list">
          <div className="step">
            <div className="left-number">1</div>
            <div className="right-content">
              <span className="text1">选择商品</span>
              <span className="text2">付款开团/参团</span>
            </div>
          </div>
          <span>></span>
          <div className="step">
            <div className="left-number">2</div>
            <div className="right-content">
              <span className="text1">邀请并等待</span>
              <span className="text2">好友支持参团</span>
            </div>
          </div>
          <span>></span>
          <div className="step">
            <div className="left-number">3</div>
            <div className="right-content">
              <span className="text1">达到人数</span>
              <span className="text2">顺利成团</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
