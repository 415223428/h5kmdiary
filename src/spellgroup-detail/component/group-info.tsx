import React, { Component } from 'react';
import { List, fromJS } from 'immutable';
import moment from 'moment';
import { Relax, IMap } from 'plume2';
import { NumberInput, _, WMkit, CountDown, history } from 'wmkit';
import { Const } from 'config';
import noop from 'wmkit/noop';
const windowWidth = window.innerWidth.toString() + 'px';

@Relax
export default class GroupInfo extends Component<any, any> {
  props: {
    relaxProps?: {
      grouponActivity: IMap;
      goodsInfo: IMap;
      goods: IMap;
      serverTime: any;
      countOver: Function;
      loading: any;
    };
  };

  static relaxProps = {
    grouponActivity: 'grouponActivity',
    goodsInfo: 'goodsInfo',
    goods: 'goods',
    serverTime: 'serverTime',
    countOver: noop,
    loading: 'loading'
  };

  render() {
    const {
      grouponActivity,
      goodsInfo,
      goods,
      serverTime,
      countOver,
      loading
    } = this.props.relaxProps;
    //划线价
    const lineShowPrice = this._originPriceInfo(
      goods.get('linePrice'),
      goodsInfo
    );
    const alreadyGrouponNum = grouponActivity.get('alreadyGrouponNum');
    //拼团结束时间
    const endTime = moment(grouponActivity.get('endTime'));
    return (
      !loading && (
        <div className="group-info spell-box">

          <div className="left-info">
            <div className="up-price">
              <div className="now-price">
                <i className="iconfont icon-qian" />
                {_.addZero(goodsInfo.get('grouponPrice'))}
              </div>
              <div className="old-price">
                <i className="iconfont icon-qian" />
                {_.addZero(lineShowPrice)}
              </div>
              {localStorage.getItem('loginSaleType') == '1'?<span style={{marginLeft:'.4rem',color:'#fff'}}>预估点数{goodsInfo.get('kmPointsValue')}</span>:null}
            </div>
            <div className="down-content">
              <div className="left-btn">
                {grouponActivity.get('grouponNum')}人团
              </div>
              <div className="right-personal">
                {alreadyGrouponNum}
                人已拼团
              </div>
            </div>
          </div>
          <div className="right-info">
            <span className="text1">距离开团结束</span>
            <CountDown
              visible={endTime && serverTime}
              endHandle={() => {
                history.replace('/mian');
              }}
              timeOffset={moment
                .duration(endTime.diff(serverTime))
                .asSeconds()
                .toFixed(0)}
            // timeOffset={6}
            />
          </div>
        </div>
      )
    );
  }

  /**
   * 获取是否展示划线价,以及划线价
   *   a.若划线价存在,则展示
   *   b.若划线价不存在
   *     b.1.登录前,不展示
   *     b.2.登陆后,展示sku市场价
   * @private
   */
  _originPriceInfo = (linePrice, goodsInfoIm) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        return goodsInfoIm.get('marketPrice');
      } else {
        return goodsInfoIm.get('marketPrice');
      }
    }
  };
}
