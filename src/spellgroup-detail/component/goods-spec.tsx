import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';

import { NumberInput, noop, Alert } from 'wmkit';
import { IList } from 'typings/globalType';

@Relax
export default class GoodsDetailSpec extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      goods: IMap;
      buyNum: number;
      changeNum: Function;
      goodsSpecDetails: IList;
      changeWholesaleVisible: Function;
      changeRetailSaleVisible: Function;
      goodsInfos: IList;
      grouponDetailOptStatus: number;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    goods: 'goods',
    buyNum: 'buyNum',
    changeNum: noop,
    goodsSpecDetails: 'goodsSpecDetails',
    changeWholesaleVisible: noop,
    changeRetailSaleVisible: noop,
    goodsInfos: 'goodsInfos',
    grouponDetailOptStatus: 'grouponDetailOptStatus'
  };

  render() {
    const {
      goodsInfo,
      goods,
      buyNum,
      goodsSpecDetails,
      changeWholesaleVisible,
      changeRetailSaleVisible,
      goodsInfos,
      grouponDetailOptStatus
    } = this.props.relaxProps;

    const goodsUnit = goods.get('goodsUnit') || '';
    const valid =
      goodsInfo.get('stock') <= 0 ||
      (goodsInfo.get('priceType') == '0' &&
        goodsInfo.get('stock') < goodsInfo.get('count'));
    const buyCount = valid ? 0 : buyNum || 0;
    return (
      // 拼团只有零售弹框
      <div className="gec-box">
        <div
          className="detail-gec b-1px-t"
          onClick = {() => {grouponDetailOptStatus == 0 &&changeRetailSaleVisible(true)}}
        >
          规格
          <div className="gec">
            <span  style={{
            visibility:'hidden'
          }}>
              {goods.get('saleType') == 0
                ? goodsInfos && goodsInfos.size > 0
                  ? goodsInfos.size + '种'
                  : '无'
                : goodsInfo.get('specText')
                ? goodsInfo.get('specText')
                : '无'}
            </span>
          </div>
            {/* 开团-可以开团 */}
            {grouponDetailOptStatus == 0 &&
            <i className="iconfont icon-more"/>
            }
        </div>
        {/* <div className="detail-gec b-1px-b">
          数量
          <div className="gec">
            <NumberInput disableNumberInput={valid} min={0}
              max={goodsInfo.get('stock')} value={buyCount}
              onChange={(val) => this._changeNum(val)}
              onAddClick={(addDisabled, nextValue) => {
                const stock = goodsInfo.get('stock')
                if (addDisabled && nextValue > stock) {
                  Alert({ text: `库存数量：${stock}！` })
                }
              }}
            />
            <span
              style={{ marginLeft: 10 }}>{(!valid && goodsInfo.get('stock'))
                ? `库存${goodsInfo.get('stock')}${goodsUnit}` : '缺货'}</span>
          </div>
        </div> */}
      </div>
    );
  }

  _changeNum(num) {
    let nums = num;
    const { goodsInfo, changeNum } = this.props.relaxProps;
    if (num > goodsInfo.get('stock')) {
      nums = goodsInfo.get('stock');
      Alert({
        text: '库存数量' + nums + '!'
      });
    }
    changeNum(nums);
    this.setState({});
  }
}
