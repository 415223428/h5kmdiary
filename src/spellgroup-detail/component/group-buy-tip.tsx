import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { history, NumberInput, noop } from 'wmkit';

@Relax
export default class GroupBuyTip extends Component<any, any> {
  constructor(props) {
    super(props);
  }
  props: {
    relaxProps?: {
      grouponInst: IMap;
    };
  };
  static relaxProps = {
    grouponInst: 'grouponInst'
  };

  render() {
    const { grouponInst } = this.props.relaxProps;
    return (
      <div
        className="group-buy-tip"
        onClick={() =>
          history.push('/group-buy-detail/' + grouponInst.get('grouponNo'))
        }
      >
        {grouponInst.get('customerName') ? (
          <div className="tip-btn">
            <span className="group-buy-tip-text">
              <i className="iconfont icon-laba" />
              <span className="group-buy-tip-text1">
                {grouponInst.get('customerName')}邀请你来参团
              </span>
            </span>
          </div>
        ) : null}
      </div>
    );
  }
}
