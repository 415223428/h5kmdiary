import React, { Component } from 'react';
import { Relax } from 'plume2';
import GoodsEvaluationItem from './goods-evaluation-item';

const windowHeight = window.innerHeight.toString() + 'px';

@Relax
export default class GoodsEvaluation extends Component<any, any> {
  static relaxProps = {};

  constructor(props) {
    super(props);
  }

  render() {
    const {} = this.props.relaxProps;
    return (
      <div className="content evaluation b-1px-b">
        <div className="evaluation-top b-1px-b">
          <span className="evaluation-text">评价 (12万+)</span>
          <span className="evaluation-text text-grey">99%好评</span>
        </div>
        <div className="evaluation-list">
          <GoodsEvaluationItem />
        </div>
        <div className="evaluation-bottom">
          <a className="evaluation-bottom-btn" href="javascript:;">
            查看全部评价
          </a>
        </div>
      </div>
    );
  }
}
