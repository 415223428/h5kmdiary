import React, { Component } from 'react';
import { Relax, IMap, msg } from 'plume2';

import { Button, noop, history, _, WMkit } from 'wmkit';

const SubmitButton = Button.Submit;
const buyself=require('../images/buy-self.png')
const pintuan=require('../images/pintuan.png')
const endPT=require('../images/end-pintuan.png')
@Relax
export default class GoodsDetailTitle extends Component<any, any> {
  props: {
    relaxProps?: {
      purchaseCount: number;
      store: IMap;
      onlineServiceFlag: boolean;
      goods: IMap;
      updatePurchaseCount: () => {};
      changeRetailSaleVisible: Function;
      grouponActivity: IMap;
      grouponDetailOptStatus: number;
      goodsInfo: IMap;
      tradeGroupon: IMap;
    };
  };

  static relaxProps = {
    purchaseCount: 'purchaseCount',
    store: 'store',
    onlineServiceFlag: 'onlineServiceFlag',
    goods: 'goods',
    updatePurchaseCount: noop,
    changeRetailSaleVisible: noop,
    grouponActivity: 'grouponActivity',
    grouponDetailOptStatus: 'grouponDetailOptStatus',
    goodsInfo: 'goodsInfo',
    tradeGroupon: 'tradeGroupon'
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    // 商品详情中购物车数量角标更新方法
    msg.on('purchaseNum', this.props.relaxProps.updatePurchaseCount);
  }

  componentWillUnmount() {
    msg.off('purchaseNum', this.props.relaxProps.updatePurchaseCount);
  }

  render() {
    const {
      purchaseCount,
      store,
      onlineServiceFlag,
      changeRetailSaleVisible,
      goods,
      goodsInfo,
      grouponActivity,
      grouponDetailOptStatus,
      tradeGroupon
    } = this.props.relaxProps;
    //划线价
    const lineShowPrice = this._originPriceInfo(
      goods.get('linePrice'),
      goodsInfo
    );
    return (
      <div className="detail-bottom-height">
        <div className="spell-group-bottom" style={{ height: '1rem' }}>
          <div className="item-box" style={{
            height:"100%"
          }}>
            { !(window as any).isMiniProgram && (
                <div
                style={{
                  height:"100%"
                }}
                  className="bar-item"
                  // style={{ minWidth: 70 }}
                  onClick={() => {
                    if (WMkit.isLoginOrNotOpen()) {
                      WMkit.getQiyuCustomer();
                      // history.push(`/chose-service/${store.get('storeId')}`);
                      (window as any).ysf('product', {
                        show: 1, // 1为打开， 其他参数为隐藏（包括非零元素）
                        title: goods.get('goodsName'),
                        desc: goods.get('goodsSubtitle'),
                        picture: goods.get('goodsImg'),
                        note: `￥${goodsInfo.get('grouponPrice')}`,
                        url: `https://m.kmdiary.com${location.pathname}`
                      });
                      window.location.href = (window as any).ysf('url');
                      (window as any).openqiyu();
                    } else {
                      msg.emit('loginModal:toggleVisible', {
                        callBack: () => {
                          // history.push(
                          //   `/chose-service/${store.get('storeId')}`
                          // );
                          WMkit.getQiyuCustomer();
                          (window as any).ysf('product', {
                            show: 1, // 1为打开， 其他参数为隐藏（包括非零元素）
                            title: goods.get('goodsName'),
                            desc: goods.get('goodsSubtitle'),
                            picture: goods.get('goodsImg'),
                            note: `￥${goodsInfo.get('grouponPrice')}`,
                            url: `https://m.kmdiary.com${location.pathname}`
                          });
                          window.location.href = (window as any).ysf('url');
                          (window as any).openqiyu();
                        }
                      });
                    }
                  }}
                >
                  {/* <i className="iconfont icon-kefu2" /> */}
                  <img src={require('../images/server.png')} alt="" style={{
                    width:'.33rem',
                    height:'.31rem',
                    margin: '0.12rem auto 0.12rem'
                  }}/>
                  <span style={{
                  fontSize: '.22rem',
                  fontFamily: 'PingFang SC',
                  fontWeight: 500,
                  color: 'rgba(102,102,102,1)',
                }}>客服</span>
                </div>
               )}
            <div
              className="bar-item"
              style={{
                height:"100%"
              }}
              onClick={() => {
                if (store.get('storeId')) {
                  this._goStore(store.get('storeId'));
                }
              }}
            >
              {/* <i className="iconfont icon-shouye01" /> */}
              <img src={require('../images/home.png')} alt=""style={{
                    width:'.32rem',
                    height:'.32rem',
                    margin: '0.1rem auto 0.14rem'
                  }}/>
              {/* <span>店铺</span> */}
              <span style={{
                  fontSize: '.22rem',
                  fontFamily: 'PingFang SC',
                  fontWeight: 500,
                  color: 'rgba(102,102,102,1)',
                }}>首页</span>
            </div>
            <div
              className="bar-item"
              style={{
                height:"100%"
              }}
              onClick={() => history.push('/purchase-order')}
            >
              <div className="cart-content">
                {purchaseCount > 0 && (
                  <div className="num-tips" style={{
                    width: '0.28rem',
                    height: '0.28rem',
                    top: '0.1rem',
                    right: '-0.2rem',
                  }}>{purchaseCount}</div>
                )}
                {/* <i className="iconfont icon-gouwuche" /> */}
                <img src={require('../images/cart.png')} alt=""style={{
                    width:'.39rem',
                    height:'.33rem',
                    margin: '0.1rem auto 0.14rem'
                  }}/>
              </div>
              <span className="red" style={{
                  fontSize: '.22rem',
                  fontFamily: 'PingFang SC',
                  fontWeight: 500,
                  color: 'rgba(102,102,102,1)',
                }}>购物车</span>
            </div>
          </div>
          {/* <SubmitButton
            defaultStyle={{
              lineHeight: 'normal',
              backgroundColor: '#000',
              borderColor: '#000',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              background:'linear-gradient(135deg,rgba(246,150,38,1),rgba(248,196,70,1))',
              border:'none',
              width:'2.22rem'
            }}
            text={this._renderSimpleHtml(lineShowPrice)}
            onClick={() => {
              this._go(1, goodsInfo.get('goodsInfoId'));
            }}
          /> */}


          {/* 活动已结束 */}
          {/* {grouponDetailOptStatus == 2 && (
            <SubmitButton
              defaultStyle={{
                lineHeight: 'normal',
                backgroundColor: '#eee',
                borderColor: '#eee',
                color: '#666',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))',
                width:'2.22rem'
              }}
              text="拼团已结束"
              onClick={() => {
                this._go(4, tradeGroupon.get('grouponNo'));
              }}
            />
          )} */}
          {/* 开团-可以开团 */}
          {/* {grouponDetailOptStatus == 0 && (
            <SubmitButton
              defaultStyle={{
                lineHeight: 'normal',
                backgroundColor: '#ff1f4e',
                borderColor: '#ff1f4e',

                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))',
                width:'2.22rem'
              }}
              text={this._renderGrouponHtml(goodsInfo, grouponActivity)}
              onClick={() => {
                if (!WMkit.isLoginOrNotOpen()) {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      changeRetailSaleVisible(true);
                    }
                  });
                } else {
                  changeRetailSaleVisible(true);
                }
              }}
            />
          )} */}
          {/* 开团-查看团详情 */}
          {/* {grouponDetailOptStatus == 1 && (
            <SubmitButton
              defaultStyle={{
                lineHeight: 'normal',
                backgroundColor: '#ff4d4d',
                borderColor: '#ff4d4d',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',

              }}
              text="查看团详情"
              onClick={() => {
                this._go(2, tradeGroupon.get('grouponNo'));
              }}
            />
          )} */}
          <div style={{display:'flex',flexDirection:'row'}}>
            <div style={{position:'relative'}}>
              <img src={buyself} alt="" style={{width:'2.22rem'}}
                onClick={() => {
                  this._go(1, goodsInfo.get('goodsInfoId'));
                }}/>
              <div style={{position:'absolute',left:'50%',top:'0.1rem',transform:'translate(-50%, 0)',textAlign:'center',width:'2.2rem'}}>
                {this._renderSimpleHtml(lineShowPrice)}
              </div>
            </div>
            {grouponDetailOptStatus == 2 &&(
            <div>
              <img src={endPT} alt="" style={{width:'2.22rem'}}
                    onClick={() => {
                      this._go(4, tradeGroupon.get('grouponNo'));
                    }}/>
            </div>)}
            {grouponDetailOptStatus == 0 &&(
              <div style={{position:'relative'}}>
                <img src={pintuan} alt="" style={{width:'2.22rem'}}
                     onClick={() => {
                        if (!WMkit.isLoginOrNotOpen()) {
                          msg.emit('loginModal:toggleVisible', {
                            callBack: () => {
                              changeRetailSaleVisible(true);
                            }
                          });
                        } else {
                          changeRetailSaleVisible(true);
                        }
                }}/>
                <div style={{position:'absolute',left:'50%',top:'0.1rem',transform:'translate(-50%, 0)',textAlign:'center',width:'2.2rem'}}>
                  {this._renderGrouponHtml(goodsInfo, grouponActivity)}
                </div>
              </div>
            )}
            {grouponDetailOptStatus == 1 && (
              <SubmitButton
                defaultStyle={{
                  lineHeight: 'normal',
                  backgroundColor: '#ff4d4d',
                  borderColor: '#ff4d4d',
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',

                }}
                text="查看团详情"
                onClick={() => {
                  this._go(2, tradeGroupon.get('grouponNo'));
                }}
              />
            )}
          </div>
        </div>
      </div>
    );
  }

  _renderSimpleHtml = (lineShowPrice) => {
    let html = null;
    html = (
      // <div>
      //   <div>￥{_.addZero(lineShowPrice)}</div>
      //   <div>单独购买</div>
      // </div>
      <span style={{fontSize:'0.26rem',color:'#fff',fontWeight:'bold'}}>￥{_.addZero(lineShowPrice)}</span>
    );

    return html;
  };

  _renderGrouponHtml = (goodsInfo, grouponActivity) => {
    let html = null;
    html = (
      // <div style={{
      //   width:'2.22rem'
      // }}>
      //   <div>￥{_.addZero(goodsInfo.get('grouponPrice'))}</div>
      //   {/* <div>{grouponActivity.get('grouponNum')}人拼</div> */}
      //   <div>发起拼团</div>
      // </div>
      <span style={{fontSize:'0.26rem',color:'#fff',fontWeight:'bold'}}>￥{_.addZero(goodsInfo.get('grouponPrice'))}</span>
    );
    return html;
  };

  _go = (type, optid) => {
    // 单独购买
    if (type == 1) {
      history.push('/goods-detail/' + optid);
    }
    // 查看团详情
    if (type == 2) {
      if (!WMkit.isLoginOrNotOpen()) {
        msg.emit('loginModal:toggleVisible', {
          callBack: () => {
            history.push('/group-buy-detail/' + optid);
          }
        });
      } else {
        history.push('/group-buy-detail/' + optid);
      }
    }
  };
  /**
   * 获取是否展示划线价,以及划线价
   *   a.若划线价存在,则展示
   *   b.若划线价不存在
   *     b.1.登录前,不展示
   *     b.2.登陆后,展示sku市场价
   * @private
   */
  _originPriceInfo = (linePrice, goodsInfoIm) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        return goodsInfoIm.get('marketPrice');
      } else {
        return goodsInfoIm.get('marketPrice');
      }
    }
  };

  _goStore = (storeId) => {
    // history.push(`/store-main/${storeId}`);
    // 首页
    history.push(`/`);
  };
}
