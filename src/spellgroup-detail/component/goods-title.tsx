import React, { Component } from 'react';
import { Relax, IMap, msg } from 'plume2';
import { noop, WMImage, WMkit,history } from 'wmkit';
import { IList } from 'typings/globalType';
import {cache} from 'config'
const share=require('../images/share.png')
@Relax
export default class GoodsDetailTitle extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      follow: boolean;
      changeFollow: Function;
      init: Function;
      url: String;
      goods: IMap;
      goodsInfos: IList;
      changefenxiang:Function;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    follow: 'follow',
    changeFollow: noop,
    init: noop,
    url: 'url',
    goods: 'goods',
    goodsInfos: 'goodsInfos',
    changefenxiang:noop,
  };

  render() {
    const { goodsInfo, follow, goods,changefenxiang} = this.props.relaxProps;
    return (
      <div className="detail-box">
        <div className="title-left">
          {/*商品编号功能产品要删除的*/}
          {/*<div className="sku-title">{goodsInfo.get('goodsInfoNo')}</div>*/}
          <div className="sku-det">{goods.get('goodsName')}</div>
          {goods.get('goodsSubtitle') && (
            <div className="sku-det sec-title">
              {goods.get('goodsSubtitle')}
            </div>
          )}
        </div>
        <div
          className="right-bar-item b-1px-l"
          style={{ minWidth: 70,color: '#333333'}}
          onClick={() => this.collect()}
        >
          {follow ? (
            <img src={require('../images/heart.png')} alt="" style={{
              width: '0.34rem',
              height: '0.34rem',
            }} />
          ) : (
            <img src={require('../images/heartblank.png')} alt="" style={{
              width: '0.34rem',
              height: '0.34rem',
            }} />
          )}
          <span>{follow ? '已收藏' : '收藏'}</span>
        </div>
        <div
          className="right-bar-item b-1px-l"
          style={{ minWidth: '1rem', color: '#333333' }}
          onClick={() => this.shareMain()}
        >
          <img src={require('../images/share.png')} alt="" 
          style={{
            width: '0.34rem',
            height: '0.34rem',
          }} 
          />
          <span>分享</span>
        </div>
      </div>
    );
  }
  collect() {
    const {
      follow,
      changeFollow,
      goodsInfo,
      init,
      url,
      goods,
      goodsInfos
    } = this.props.relaxProps;
    let saleType = goods.get('saleType');
    if (WMkit.isLoginOrNotOpen()) {
      changeFollow(
        !follow,
        saleType == 0
          ? goodsInfos.get(0).get('goodsInfoId')
          : goodsInfo.get('goodsInfoId')
      );
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () =>{
          init(
            saleType == 0
              ? goodsInfos.get(0).get('goodsInfoId')
              : goodsInfo.get('goodsInfoId'),
            url
          )
        }
        //callBack:null
      });
    }
  }
  shareMain(){
    const {
      goodsInfo,
      init,
      url,
      goods,
      goodsInfos,
      changefenxiang
    } = this.props.relaxProps;
    const inviteeId = JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '';
    const inviteCode = sessionStorage.getItem('inviteCode');
    const newUrl = `spellgroup-detail/${goodsInfo.get('goodsInfoId')}/?channel=mall&inviteeId=${inviteeId}&inviteCode=${inviteCode}`;
    let saleType = goods.get('saleType');
    const lineShowPrice = this._wholeSalePriceInfo(
      goods.get('linePrice'),
      goodsInfos
    );
    if (WMkit.isLoginOrNotOpen()) {
      // changefenxiang()
      history.push({
        pathname: '/share',
        state: {
          goodsInfo ,
          url:newUrl,
          lineDrawingPrice:lineShowPrice
        }
      });
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          init(
            saleType == 0
              ? goodsInfos.get(0).get('goodsInfoId')
              : goodsInfo.get('goodsInfoId'),
            url
          )
        }
        //callBack:null
      });
    }
  };
  
    //批发销售类型，计算划线价
  _wholeSalePriceInfo = (linePrice, goodsInfos) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        // 已登录时,找出最高的市场价
        let maxMarketPrice = null;
        goodsInfos.forEach((info, index) => {
          if (index === 0) {
            maxMarketPrice = info.get('marketPrice');
          } else {
            maxMarketPrice =
              info.get('marketPrice') > maxMarketPrice
                ? info.get('marketPrice')
                : maxMarketPrice;
          }
        });
        return maxMarketPrice;
      } else {
        return null;
      }
    }
  };
}
