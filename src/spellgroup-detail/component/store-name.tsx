import React from 'react'
import { Relax } from 'plume2'
import { history } from 'wmkit'

const defaultImg = require('../images/defalutShop.png')

@Relax
export default class StoreName extends React.Component<any, any> {

  props: {
    relaxProps?: {
      store: any
    }
  }

  static relaxProps = {
    // 店铺信息
    store: 'store'
  }

  render() {
    const { store } = this.props.relaxProps
    const { storeId, storeName, storeLogo, companyType } = store.toJS()
    return (
      <div className="detail-store b-1px-tb" onClick={() =>
        history.push(`/store-main/${storeId}`)
      }>
        <div className="img-box"><img src={storeLogo ? storeLogo : defaultImg} alt={storeName} /></div>
        {
          companyType === 0 && <div className="self-sales">自营</div>
        }
        <span>{storeName}</span>
      </div>
    )
  }
}