import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { IList } from 'typings/globalType';
import { noop, history, WMImage } from 'wmkit';
import { marketAll, marketAllByOne } from '../kit';

//促销类型
const marketingType = {
  0: '减',
  1: '折',
  2: '赠'
};

//促销类型样式
const _TYPE = {
  '0': { text: '减', color: '' },
  '2': { text: '赠', color: 'red' },
  '1': { text: '折', color: 'orange' }
};

/**
 * 促销遮罩
 */
@Relax
export default class PromotionShade extends Component<any, any> {
  props: {
    relaxProps?: {
      show: boolean;
      showModal: Function;
      closeModal: Function;
      levelList: IList;
      giftList: IList;
      goodsInfo: IMap;
      goods: IMap;
      goodsInfos: IList;
    };
  };

  static relaxProps = {
    show: 'show',
    showModal: noop,
    closeModal: noop,
    levelList: 'levelList',
    giftList: 'giftList',
    goodsInfo: 'goodsInfo',
    goodsInfos: 'goodsInfos',
    goods: 'goods'
  };

  render() {
    const {
      show,
      showModal,
      closeModal,
      levelList,
      giftList,
      goodsInfo,
      goodsInfos,
      goods
    } = this.props.relaxProps;
    //去重后的所有促销活动
    let marketingList = [];
    let isHaveGift = false;
    if (goodsInfo.size > 0 && goods.size > 0) {
      if (goods.get('saleType') == 1) {
        marketingList = goodsInfo.size == 0 ? [] : marketAllByOne(goodsInfo);
        if (marketingList.length > 0) {
          marketingList.forEach((label, index) => {
            if (label.marketingType == 2) {
              isHaveGift = true;
            }
          });
        }
      } else if (goodsInfos.size > 0) {
        marketingList = marketAll(goodsInfos);
        isHaveGift = true;
      }
    }

    return show ? (
      <div style={show ? { display: 'block' } : { display: 'none' }}>
        <div
          style={{
            position: 'fixed',
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,.5)',
            zIndex: 9999
          }}
          onClick={() => showModal()}
        />
        <div className="panel-bottom">
          <div className="panel-title b-1px-b">
            <h1>促销</h1>
            <i className="iconfont icon-guanbi" onClick={() => closeModal()} />
          </div>
          {goodsInfo.size > 0 &&
            goodsInfo.get('marketingLabels').size > 1 && (
              <div className="panel-info b-1px-b">
                <i className="iconfont icon-caution" />
                <p>满折/满减/满赠仅可任选其一，可在购物车修改</p>
              </div>
            )}
          {//展示所有促銷活動
          marketingList.length > 0 &&
            marketingList.map((label, index) => {
              return (
                <div
                  className="panel-promotion b-1px-b"
                  key={index}
                  onClick={() => {
                    history.push(`/goods-list-promotion/${label.marketingId}`);
                  }}
                >
                  <div className="promotion-info">
                    <div className={`tag ${_TYPE[label.marketingType].color}`}>
                      {marketingType[label.marketingType]}
                    </div>
                    <span>{label.marketingDesc}</span>
                  </div>
                  <i className="iconfont icon-jiantou1" />
                </div>
              );
            })}
          {
            <div className="panel-body">
              {isHaveGift && giftList.size > 0 && levelList.size > 0
                ? levelList.toJS().map((level, levelIndex) => {
                    //满金额赠还是满数量赠
                    let countOrAmount = level.fullAmount
                      ? level.fullAmount + '元'
                      : level.fullCount + '件';
                    //赠几件(0:全增,1:赠一种)
                    let giftCount = level.giftType ? '1种' : '全部';
                    return (
                      <div className="gift b-1px-b" key={levelIndex}>
                        <span className="gift-title">
                          满{countOrAmount}可获以下赠品，可选{giftCount}
                        </span>
                        {level.fullGiftDetailList.map((full) => {
                          //赠品数量
                          let count = full.productNum;
                          return giftList
                            .toJS()
                            .filter(
                              (gift) => gift.goodsInfoId == full.productId
                            )
                            .map((v, giftkey) => {
                              return (
                                <div
                                  key={giftkey}
                                  className="goods-list"
                                  onClick={() =>
                                    v.goodsStatus != '2'
                                      ? (window.location.href = `/goods-detail/${
                                          v.goodsInfoId
                                        }`)
                                      : null
                                  }
                                >
                                  <div className="img-box">
                                    <WMImage
                                      src={v.goodsInfoImg}
                                      width="100%"
                                      height="100%"
                                    />
                                  </div>
                                  <div className="detail">
                                    <a className="title">{v.goodsInfoName}</a>
                                    <p className="gec">{v.specText}</p>
                                    <div className="bottom">
                                      <div className="bottom-state">
                                        <span className="price">
                                          <i className="iconfont icon-qian" />
                                          {v.marketPrice}
                                        </span>
                                        {v.goodsStatus == '2' && (
                                          <div className="out-stock">失效</div>
                                        )}
                                        {v.goodsStatus != '2' &&
                                          v.stock == 0 && (
                                            <div className="out-stock">
                                              缺货
                                            </div>
                                          )}
                                      </div>
                                      <span className="num">×{count}件</span>
                                    </div>
                                  </div>
                                </div>
                              );
                            });
                        })}
                      </div>
                    );
                  })
                : null}
            </div>
          }
        </div>
      </div>
    ) : null;
  }
}
