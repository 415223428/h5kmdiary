import React from 'react';
import { history, _, CountDown } from 'wmkit';
import moment from 'moment';

// 默认头像
const defaultImg = require('../../../web_modules/images/default-headImg.png');
const cantuan=require('../images/cantuan.png')
export default class JoinGroupItem extends React.Component<any, any> {
  render() {
    const { item, serverTime, countOver } = this.props;
    //拼团结束时间
    const endTime = moment(item.get('endTime'));
    return (
      <li
        onClick={() =>
          history.push('/group-buy-detail/' + item.get('grouponNo'))
        }
      >
        <div className="left-pointer">
          <img src={this._img(item)} />
        </div>
        <div className="center-phone">{this._name(item)}</div>
        <div className="right-group">
          <div className="left-info">
            <span className="text1">
              还差<span>{this._num(item)}</span>人成团
            </span>
            <span className="text2">
              <CountDown
                colorStyle={{ color: '#ff1f4e' }}
                endHandle={countOver}
                showTimeDays={true}
                visible={endTime && serverTime}
                labelText={'距结束:'}
                timeOffset={moment
                  .duration(endTime.diff(serverTime))
                  .asSeconds()
                  .toFixed(0)}
              />
            </span>
          </div>
          <div 
          // className="right-btn"
          >
            <img src={cantuan} alt="" style={{width:'1.44rem', height:'0.48rem'}}/>
          </div>
        </div>
      </li>
    );
  }

  /**
   * 头像
   */
  _img = (item) => {
    let text = '';
    if (!item.get('headimgurl')) {
      text = defaultImg;
    } else {
      text = item.get('headimgurl');
    }
    return `${text}`;
  };

  /**
   * 名称
   */
  _name = (item) => {
    let text = '';
    if (!item.get('customerName')) {
      text = '用户';
    } else {
      text = item.get('customerName');
    }
    return `${text}`;
  };

  /**
   * 名称
   */
  _num = (item) => {
    return item.get('grouponNum') - item.get('joinNum');
  };
}
