import React, { Component } from 'react';
import { Relax } from 'plume2';
import { Star } from 'wmkit';

@Relax
export default class GoodsEvaluationItem extends Component<any, any> {
  contentRef;
  static relaxProps = {};

  constructor(props) {
    super(props);
  }

  render() {
    const {} = this.props.relaxProps;
    return (
      <div className="evaluation-item b-1px-b">
        <div className="evaluation-item-head">
          <div className="evaluation-item-head-user">
            <img
              src="http://web-img.qmimg.com/qianmicom/u/cms/syy/201803/260942417ii0.png"
              alt=""
            />
            <span className="evaluation-text">某某用户</span>
          </div>
          <div>
            <Star star={3} />
          </div>
        </div>
        <div className="evaluation-item-body">
          <p className="evaluation-text">评价内容</p>
        </div>
        <div className="evaluation-item-img">
          <ul className="evaluation-item-img-list">
            <li className="evaluation-item-img-item">
              <img
                src="http://web-img.qmimg.com/qianmicom/u/cms/syy/201803/260942417ii0.png"
                alt=""
              />
            </li>
            <li className="evaluation-item-img-item">
              <img
                src="http://web-img.qmimg.com/qianmicom/u/cms/syy/201803/260942417ii0.png"
                alt=""
              />
            </li>
            <li className="evaluation-item-img-item">
              <img
                src="http://web-img.qmimg.com/qianmicom/u/cms/syy/201803/260942417ii0.png"
                alt=""
              />
            </li>
            <li className="evaluation-item-img-item">
              <img
                src="http://web-img.qmimg.com/qianmicom/u/cms/syy/201803/260942417ii0.png"
                alt=""
              />
            </li>
          </ul>
          <div className="evaluation-item-img-tag">
            <span>共6张</span>
          </div>
        </div>
      </div>
    );
  }
}
