import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { cache } from 'config';
import { _, WMkit } from 'wmkit';

/**
 * 零售销售类型-价格展示
 */
@Relax
export default class GoodsDetailPriceRetail extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      goods: IMap;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    goods: 'goods'
  };

  render() {
    const { goodsInfo, goods } = this.props.relaxProps;
    //划线价
    const lineShowPrice = this._originPriceInfo(
      goods.get('linePrice'),
      goodsInfo
    );
    return (
      <div className="market-price">
        <div className="b-1px-t">
          <div className="price-container">
            <div className="price-box">
              <div className="price-item">
                <i className="iconfont icon-qian" />
                {_.addZero(goodsInfo.get('salePrice'))}&nbsp;&nbsp;
                {!!lineShowPrice && (
                  <span className="line-price">
                    ￥{_.addZero(lineShowPrice)}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 获取是否展示划线价,以及划线价
   *   a.若划线价存在,则展示
   *   b.若划线价不存在
   *     b.1.登录前,不展示
   *     b.2.登陆后,展示sku市场价
   * @private
   */
  _originPriceInfo = (linePrice, goodsInfoIm) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        return goodsInfoIm.get('marketPrice');
      } else {
        return null;
      }
    }
  };
}

const styles = {
  commission: {
    marginLeft: 2,
    fontSize: 12,
    color: '#333'
  }
};
