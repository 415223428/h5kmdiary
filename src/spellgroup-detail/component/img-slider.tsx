import React, { Component } from 'react';

import { Relax, IMap } from 'plume2';
import { Slider, WMImage, storage } from 'wmkit';
import { cache } from 'config';
import { IList } from 'typings/globalType';
import 'video.js/dist/video-js.css';

const defaultImg = require('../images/none.png');
const windowWidth = window.innerWidth.toString() + 'px';
@Relax
export default class ImgSlider extends Component<any, any> {
  props: {
    relaxProps?: {
      goodsInfo: IMap;
      goods: IMap;
      images: IList;
      showVideo: boolean;
      toggleShowVideo: Function;
    };
  };

  static relaxProps = {
    goodsInfo: 'goodsInfo',
    goods: 'goods',
    images: 'images',
    showVideo: 'showVideo',
    toggleShowVideo: () => {}
  };

  //删除首图
  componentWillUnmount() {
    storage('session').del(cache.GOODS_FIRST_IMAGE);
  }

  render() {
    const { goodsInfo, images, goods, showVideo } = this.props.relaxProps;
    //加载中
    //之后如果有交互,从商品列表页面进入,首先加载传递过来的图片,此处代码保留,暂定
    let loading = false;
    if (loading) {
      const goodsFirstImage = storage('session').get(cache.GOODS_FIRST_IMAGE);
      //有首图的情况下
      return goodsFirstImage ? (
        <div className="slider" style={{ height: windowWidth }}>
          <WMImage
            src={goodsFirstImage}
            width={windowWidth}
            height={windowWidth}
          />
        </div>
      ) : (
        <div
          className="slider loading-inset"
          style={{ height: window.innerWidth }}
        />
      );
    }

    let sliderImages = images;
  
    //图片为空的SKU统一都变为默认图片
    sliderImages.map((v, index) => {
      if (v == '') {
        sliderImages = sliderImages.set(index, defaultImg);
      }
    });

    return (
      //无图片的时候
      <div className="slider" style={{ height: windowWidth }}>     
       <Slider
          videoPlay={goods.get('goodsVideo')}
          data={sliderImages.isEmpty()?sliderImages.push(defaultImg):sliderImages}
          height={windowWidth}
          width={windowWidth}
          zoom={true}
          videoJsOptions={{
            autoplay: false,
            controls: true,
            playsinline: true, //ios端行内播放视频
            allowFullScreen: false,
            language: 'chz', //设置语言
            sources: [
              {
                src: goods.get('goodsVideo'),
                type: 'video/mp4'
              }
            ],
            controlBar: {
              fullscreenToggle: false //是否显示全屏按钮
            }
          }}
        />
      </div>
    )
  }
}
