import React, { Component } from 'react';
import { Relax } from 'plume2';
import { noop } from 'wmkit';
import { IList, IMap } from 'typings/globalType';
import CouponItem from './coupon-item';

/**
 * 领券弹窗
 */
@Relax
export default class CouponMask extends Component<any, any> {
  props: {
    relaxProps?: {
      couponInfos: IList;
      store: IMap;
      showCouponMask: boolean;

      changeCouponMask: Function;
      receiveCoupon: Function;
    };
  };

  static relaxProps = {
    couponInfos: 'couponInfos',
    store: 'store',
    showCouponMask: 'showCouponMask',

    changeCouponMask: noop,
    receiveCoupon: noop
  };

  render() {
    const {
      showCouponMask,
      changeCouponMask,
      couponInfos,
      store,

      receiveCoupon
    } = this.props.relaxProps;

    return showCouponMask ? (
      <div
        style={{
          position: 'fixed',
          left: 0,
          bottom: 0,
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0,0,0,.5)',
          zIndex: 9999,
          display: 'flex',
          flexDirection: 'row'
        }}
      >
        <div
          style={{ flex: 1, display: 'flex' }}
          onClick={() => changeCouponMask()}
        />
        <div className="panel-bottom">
          <div className="panel-title b-1px-b">
            <h1>领券</h1>
            <i
              className="iconfont icon-guanbi"
              onClick={() => changeCouponMask()}
            />
          </div>
          <p className="coupon-panel-title">可领取的优惠券</p>
          <div className="panel-body">
            <div className="coupon-panel-list">
              {couponInfos.map((coupon, i) => {
                return (
                  <CouponItem
                    coupon={coupon}
                    storeName={store.get('storeName')}
                    key={i}
                    receiveCoupon={receiveCoupon}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    ) : null;
  }
}
