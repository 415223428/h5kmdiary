import React, { Component } from 'react';
import { Relax, IMap } from 'plume2';
import { IList } from 'typings/globalType';
import { _, WMkit, LevelTag } from 'wmkit';

/**
 * 批发销售类型-价格展示
 */
@Relax
export default class GoodsDetailPriceWholesale extends Component<any, any> {
  props: {
    relaxProps?: {
      goods: IMap;
      goodsInfo: IMap;
      goodsInfos: IList;
      goodsIntervalPrices: IList;
      goodsSpecs: IList;
    };
  };

  static relaxProps = {
    goods: 'goods',
    goodsInfo: 'goodsInfo',
    goodsInfos: 'goodsInfos',
    goodsIntervalPrices: 'goodsIntervalPrices',
    goodsSpecs: 'goodsSpecs'
  };

  render() {
    const {
      goods,
      goodsInfo,
      goodsInfos,
      goodsIntervalPrices,
      goodsSpecs
    } = this.props.relaxProps;
    //设价类型（0：客户，1：订货量 3：市场价）
    const priceType = goods.get('priceType');
    //是否允许独立设价
    const allowPriceSet = goods.get('allowPriceSet');
    //划线价
    const lineShowPrice = this._wholeSalePriceInfo(
      goods.get('linePrice'),
      goodsInfos
    );
    //最低,最高价格
    const { minPrice, maxPrice } = this._getMinMaxPrice(goods, goodsInfos);
    const unit = goods.get('goodsUnit');
    return (
      <div className="market-price">
        <div className="b-1px-t">
          <div className="price-container">
            <div className="price-box">
              {/*
                * 1.阶梯价 且 (不允许独立设价 或 无规格商品) => 展示阶梯价
                * 2.其他情况 => 展示min价格 或 min ~ max价格 或 
                */}
              {priceType == 1 &&
              (allowPriceSet == 0 || (!goodsSpecs || goodsSpecs.size == 0)) ? (
                this._showSpuIntervalPrices(goodsInfo, goodsIntervalPrices)
                  .toJS()
                  .map((p, k) => {
                    return (
                      <div key={k} className="box-item">
                        <div className="price-item">
                          <i className="iconfont icon-qian" />
                          {_.addZero(p.price)}
                        </div>
                        <div className="area-item">
                          <i className="iconfont icon-icondayudengyu" />
                          {p.count}
                          {unit}
                        </div>
                      </div>
                    );
                  })
              ) : minPrice == maxPrice ? (
                <div className="price-item">
                  <i className="iconfont icon-qian" />
                  {_.addZero(minPrice)}
                  &nbsp;&nbsp;<span className="line-price">
                    {!!lineShowPrice && '￥' + _.addZero(lineShowPrice)}
                  </span>
                </div>
              ) : (
                <div className="price-item">
                  <i className="iconfont icon-qian" />
                  {_.addZero(minPrice)}~
                  <i className="iconfont icon-qian" />
                  {_.addZero(maxPrice)}
                  &nbsp;&nbsp;<span className="line-price">
                    {!!lineShowPrice && '￥' + _.addZero(lineShowPrice)}
                  </span>
                </div>
              )}
            </div>
          </div>
          <div>
            {priceType == 1 && (
              <LevelTag style={{ marginTop: 10 }} text="阶梯价" />
            )}
          </div>
        </div>
      </div>
    );
  }

  //批发销售类型，计算划线价
  _wholeSalePriceInfo = (linePrice, goodsInfos) => {
    if (linePrice) {
      return linePrice;
    } else {
      if (WMkit.isLoginOrNotOpen()) {
        // 已登录时,找出最高的市场价
        let maxMarketPrice = null;
        goodsInfos.forEach((info, index) => {
          if (index === 0) {
            maxMarketPrice = info.get('marketPrice');
          } else {
            maxMarketPrice =
              info.get('marketPrice') > maxMarketPrice
                ? info.get('marketPrice')
                : maxMarketPrice;
          }
        });
        return maxMarketPrice;
      } else {
        return null;
      }
    }
  };

  /**
   * 批发类型,订货量设价,不允许独立设价时展示spu统一的阶梯价格
   * @private
   */
  _showSpuIntervalPrices = (goodsInfo, goodsIntervalPrices) => {
    const prices = goodsIntervalPrices
      .filter((pri) => pri.get('type') === 0)
      .map((interPri) => {
        return {
          id: interPri.get('intervalPriceId'),
          price: interPri.get('price'),
          count: interPri.get('count')
        } as any;
      })
      .sortBy((pri) => pri.count) as any;
    return prices;
  };

  /**
   * 获取最低,最高价
   * @param goods
   * @param goodsInfos
   * @private
   */
  _getMinMaxPrice = (goods, goodsInfos) => {
    let minPrice = 0;
    let maxPrice = 0;
    if (goods.get('priceType') == 1) {
      //是否有按订货量区间设价
      goodsInfos.forEach((info, index) => {
        if (index == 0) {
          minPrice = info.get('intervalMinPrice');
          maxPrice = info.get('intervalMaxPrice');
        } else {
          minPrice =
            info.get('intervalMinPrice') < minPrice
              ? info.get('intervalMinPrice')
              : minPrice;
          maxPrice =
            info.get('intervalMaxPrice') > maxPrice
              ? info.get('intervalMaxPrice')
              : maxPrice;
        }
      });
    } else {
      goodsInfos.forEach((info, index) => {
        if (index == 0) {
          minPrice = info.get('salePrice');
          maxPrice = info.get('salePrice');
        } else {
          minPrice =
            info.get('salePrice') < minPrice ? info.get('salePrice') : minPrice;
          maxPrice =
            info.get('salePrice') > maxPrice ? info.get('salePrice') : maxPrice;
        }
      });
    }
    return { minPrice, maxPrice };
  };
}
