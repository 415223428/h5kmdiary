import React, { Component } from 'react';
import { List, fromJS } from 'immutable';
import { Relax, IMap } from 'plume2';
import { NumberInput, noop } from 'wmkit';
import { IList } from 'typings/globalType';
import JoinGroupItem from './join-group-item';
const windowWidth = window.innerWidth.toString() + 'px';

@Relax
export default class JoinGroup extends Component<any, any> {
  props: {
    relaxProps?: {
      grouponInsts: IList;
      serverTime: any;
      toggleSpecModal: Function;
      toggleWaitGroupModal: Function;
      countOver: Function;
    };
  };

  static relaxProps = {
    grouponInsts: 'grouponInsts',
    serverTime: 'serverTime',
    toggleSpecModal: noop,
    toggleWaitGroupModal: noop,
    countOver: noop
  };

  render() {
    const {
      grouponInsts,
      serverTime,
      toggleSpecModal,
      toggleWaitGroupModal,
      countOver
    } = this.props.relaxProps;
    return (
      <div className="join-group">
        <div className="group-box">
          <span className="group-title">直接参与下面的团</span>
          <span className="see-more" onClick={() => toggleWaitGroupModal()}>
            <span className="more">查看更多</span>
            <i className="iconfont icon-jiantou1" />
          </span>
        </div>
        <ul className="group-list">
          {grouponInsts.map((item, i) => {
            return (
              <JoinGroupItem
                item={item}
                key={i}
                serverTime={serverTime}
                countOver={countOver}
              />
            );
          })}
        </ul>
      </div>
    );
  }
}
