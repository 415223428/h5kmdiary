import React, { Component } from 'react';
import { msg } from 'plume2';

import { history, WMkit } from 'wmkit';

const faquan=require('../images/faquan.png')
export default class HeaderSocial extends Component<any, any> {
  render() {
    const { skuId } = this.props;
    return (
      <div className="header-social">
        <div
          // className="moments-btn"
          onClick={async (e) => {
            e.stopPropagation();
            const result = await WMkit.getDistributorStatus();
            if ((result.context as any).distributionEnable) {
              history.push(`/graphic-material/${skuId}`);
            } else {
              //禁用原因
              let reason = (result.context as any).forbiddenReason;
              msg.emit('bStoreCloseVisible', {
                visible: true,
                reason: reason
              });
            }
          }}
        >
          <img src={faquan} alt="" style={{height:'0.48rem'}}/>
        </div>
      </div>
    );
  }
}
