import React, { Component } from 'react';
import { List, fromJS } from 'immutable';
import { Relax, IMap } from 'plume2';
import { NumberInput, ListView, noop } from 'wmkit';
import { history, _, CountDown } from 'wmkit';
import moment from 'moment';
const windowWidth = window.innerWidth.toString() + 'px';

// 默认头像
const defaultImg = require('../images/default-img.png');
@Relax
export default class WaitGroupModal extends Component<any, any> {
  static relaxProps = {
    grouponActivity: 'grouponActivity',
    toggleWaitGroupModal: noop
  };

  render() {
    const { grouponActivity, toggleWaitGroupModal } = this.props.relaxProps;
    const serverTime = moment();
    return (
      <div className="wait-group-modal">
        <img className="head" src={require('../images/head.png')} />
        <ListView
          style={{ height: '6.5rem' }}
          className="wait-group-list"
          url="/groupon/instance/page"
          getServerTime={true}
          params={{
            grouponActivityId: grouponActivity.get('grouponActivityId')
          }}
          dataPropsName="context.grouponInstanceVOS.content"
          renderRow={(item: any, index, otherPropsObject, serverTime) => {
            return (
              <li  onClick={() =>
                  history.push('/group-buy-detail/' + item.grouponNo)
              }>
                <div className="left-pointer">
                  <img src={this._img(fromJS(item))} />
                </div>
                <div className="center-phone">{item.customerName}</div>
                <div className="right-group">
                  <div className="left-info">
                    <span className="text1">
                      还差<span>{item.grouponNum - item.joinNum}</span>人成团
                    </span>
                    <span className="text2">
                      <CountDown
                        colorStyle={{ color: '#ff1f4e' }}
                        showTimeDays={true}
                        visible={moment(item.endTime) && serverTime}
                        labelText={'距结束:'}
                        timeOffset={moment
                          .duration(moment(item.endTime).diff(serverTime))
                          .asSeconds()
                          .toFixed(0)}
                      />
                    </span>
                  </div>
                  <img src={require('../images/joingroup.png')} style={{width: '1.4rem'}}/>
                </div>
              </li>
            );
          }}
        />
        <div className="close">
          <img
            className="close-img"
            onClick={() => toggleWaitGroupModal()}
            src={require('../images/close.png')}
          />
        </div>
      </div>
    );
  }

  /**
   * 头像
   */
  _img = (item) => {
    let text = '';
    if (!item.get('headimgurl')) {
      text = defaultImg;
    } else {
      text = item.get('headimgurl');
    }
    return `${text}`;
  };
}
