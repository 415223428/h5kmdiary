import { IMap } from 'plume2';
import { List } from 'immutable';
import { _, WMkit } from 'wmkit';
import { cache } from 'config';

/**
 * 根据满减>满折>满赠的优先级，返回需要显示的促销活动
 */
export const marketOne = (goodsInfo) => {
  return goodsInfo
    .get('marketingLabels')
    .sortBy((marketing) => marketing.get('marketingType'))
    .toJS();
};

/**
 * 展示某个规格下的促销活动
 * @param goodsInfos
 */
export const marketAllByOne = (goodsInfo) => {
  let hash = {};
  let allSkuMarketings = List();
  goodsInfo.size > 0 &&
    goodsInfo
      .get('marketingLabels')
      .sortBy((marketing) => marketing.get('marketingType'))
      .map((marketing) => {
        allSkuMarketings = allSkuMarketings.push(marketing);
      });
  const newArr = allSkuMarketings.toJS().reduceRight((item, next) => {
    hash[next.marketingId]
      ? ''
      : (hash[next.marketingId] = true && item.push(next));
    return item;
  }, []);
  return newArr;
};

/**
 * 展示所有规格下的促销活动，并去重
 * @param goodsInfos
 */
export const marketAll = (goodsInfos) => {
  let hash = {};
  let allSkuMarketings = List();
  goodsInfos.size > 0 &&
    goodsInfos.map((goodsInfo) => {
      goodsInfo.get('marketingLabels').map((marketing) => {
        allSkuMarketings = allSkuMarketings.push(marketing);
      });
    });
  const newArr = allSkuMarketings.toJS().reduceRight((item, next) => {
    hash[next.marketingId]
      ? ''
      : (hash[next.marketingId] = true && item.push(next));
    return item;
  }, []);
  return newArr;
};

//封装入参给小程序分享的公共方法
export const miniProgramShareParams = (
  goodsInfos,
  goodsInfo,
  goods,
  images,
  skuId,
  shareType
) => {
  const saleType = goods.get('saleType');
  // 零售类型: 分享的信息与实际选中规格的sku保持一致
  let goodsInfoActual = goodsInfo;
  if (saleType === 0) {
    // 批发类型: 分享第一个上架的sku信息
    goodsInfoActual = goodsInfos.find(
      (skuInfo) => skuInfo.get('addedFlag') === 1
    );
  }
  let imgs = images || List();
  if (!imgs.isEmpty()) {
    imgs = imgs && imgs.get(0);
  }
  //商品名称
  let goodsInfoName = goodsInfoActual.get('goodsInfoName');
  //规格
  let specText = goodsInfoActual.get('specText');
  // 市场价与划线价
  let marketPrice = goodsInfoActual.get('marketPrice') || 0;
  let lineShowPrice = goods.get('linePrice');
  if (lineShowPrice) {
    lineShowPrice = _.addZero(lineShowPrice);
  }
  marketPrice = _.addZero(marketPrice);
  return {
    goodsInfoName: goodsInfoName,
    goodsInfoImg: imgs,
    specText: specText,
    marketPrice: marketPrice,
    lineShowPrice: lineShowPrice ? '￥' + lineShowPrice : null,
    skuId: skuId,
    shareType: shareType,
    inviteeId:
      shareType == 0
        ? ''
        : WMkit.inviteeId()
          ? WMkit.inviteeId()
          : JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId
  };
};
