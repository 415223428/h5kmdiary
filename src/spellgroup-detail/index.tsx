import React from 'react';
import { StoreProvider } from 'plume2';
import { WMkit, WMGrouponChoose } from 'wmkit';
import { BStoreClose } from 'biz';
import { cache } from 'config';
import AppStore from './store';
import ImgSlider from './component/img-slider';
import ImgRetail from './component/img-retail';
import Title from './component/goods-title';
import GoodsPriceRetail from './component/goods-price-retail';
import Spec from './component/goods-spec';
import Desc from './component/goods-desc';
import Bottom from './component/goods-bottom';
import StoreName from './component/store-name';
import Promotion from './component/goods-promotion';
import PromotionShade from './component/promotion-shade';
import './css/style.css';
import GoodsCoupon from './component/goods-coupon';
import CouponMask from './component/coupon-mask';
import HeaderSocial from './component/header-social';
import GoodsShare from './component/goods-share';
import GroupInfo from './component/group-info';
import JoinGroup from './component/join-group';
import GroupPlay from './component/group-play';
import WaitGroupModal from './component/wait-group-modal';
import GroupBuyTip from './component/group-buy-tip';
import Notice from './component/notice';
import wx from 'weixin-js-sdk';
import GoodsShareImg from './component/goods-share-img';
@StoreProvider(AppStore, { debug: __DEV__ })
export default class SpellgroupDetail extends React.Component<any, any> {
  store: AppStore;

  constructor(props) {
    super(props);
    //这边判断是否是微信小程序环境
    console.log('wx====>', wx);
    wx.miniProgram.getEnv(function(res) {
      (window as any).isMiniProgram = res.miniprogram;
    });
    //从分销链接进来且为店铺外分享，展示全站商品规格
    if (
      location.href.indexOf('inviteeId') != -1 &&
      location.href.indexOf('mall') != -1
    ) {
      let searchObj = WMkit.searchToObj(this.props.location.search);
      //并分销员会员ID和分销渠道存入缓存，分销渠道为商城
      WMkit.setShareInfoCache((searchObj as any).inviteeId, '1');
    }
  }

  componentWillMount() {
    window.scroll(0, 0);
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    //已登录或未开放，调用原来的
    if (WMkit.isLoginOrNotOpen()) {
      this.store.memberInfo();
      this.store.init(id, this.props.match.url).then((res) => {
        document.title = this.store
          .state()
          .getIn(['goodsInfo', 'goodsInfoName']);
      });
    } else {
      //未登录并且已经开放的，调用新的
      this.store.initWithOutLogin(id, this.props.match.url).then((res) => {
        document.title = this.store
          .state()
          .getIn(['goodsInfo', 'goodsInfoName']);
      });
    }
    this.store.notice();
  }

  render() {
    //商品名称传递到小程序里
    if (this.store.state().get('goods').size > 0) {
      wx.miniProgram.getEnv((res) => {
        if (res.miniprogram) {
          wx.miniProgram.postMessage({
            data: {
              type: 'goodsShare',
              goodsName: this.store
                .state()
                .get('goods')
                .get('goodsName'),
              skuId: this.store
                .state()
                .get('spuContext')
                .get('skuId'),
              token: window.token
            }
          });
        }
      });
    }
    const saleType = this.store.state().getIn(['goods', 'saleType']);
    //当商品允许分销且分销审核通过，视为分销商品，不展示促销和优惠券
    const distributionFlag =
      this.store
        .state()
        .get('goodsInfo')
        .get('distributionGoodsAudit') == '2';
    const joinGroup = this.store.state().get('grouponInsts');
    const tips = this.store.state().get('tips');
    const isDistributor = JSON.parse(localStorage.getItem(cache.LOGIN_DATA));

    return (
      <div className="goods-detail spell-box">
        {/* 分享图片 */}
        {/* {isDistributor?
        <GoodsShareImg
          />:null} */}
        {/*是分销员且不是通过分享链接进来的且当前商品为分销商品才展示发圈素材和分享*/}
        {WMkit.isDistributor() &&
          !sessionStorage.getItem(cache.INVITEE_ID) &&
          distributionFlag && (
            <HeaderSocial skuId={this.props.match.params.id} />
          )}

        {/* 非分销规格才展示优惠券 */}
        {<CouponMask />}
        <Notice />
        {/* <GroupBuyTip /> */}
        {tips && <GroupBuyTip />}
        {/*轮播动图*/}
        {saleType === 0 ? <ImgSlider /> : <ImgRetail />}
        {/* 拼团信息 */}
        <GroupInfo />
        {/*商品名称,收藏*/}
        <Title />

        {/*优惠券信息*/}
        {!distributionFlag && this.store.state().get('goodsInfo').size > 0 ? (
          this.store
            .state()
            .get('goodsInfo')
            .get('couponLabels').size == 0 ? null : (
            <GoodsCoupon />
          )
        ) : null}

        {/*规格*/}
        <Spec />

        {/* 评价 */}
        {/*<GoodsEvaluation />*/}

        {/*店铺*/}
        <StoreName />

        {/* 直接参团 */}
        {joinGroup.size > 0 && <JoinGroup />}
        {this.store.state().get('waitGroupModal') && <WaitGroupModal />}
        {/* 参团玩法 */}
        <GroupPlay />
        {/*商品详情*/}
        <Desc />

        {/*零售销售类型-规格选择弹框*/}
        <WMGrouponChoose
          openGroupon={true}
          grouponData={this.store
            .state()
            .get('grouponDetails')
            .toJS()}
          data={this.store
            .state()
            .get('spuContext')
            .toJS()}
          visible={this.store.state().get('retailSaleVisible')}
          changeSpecVisible={this.store.changeRetailSaleVisible}
          dataCallBack={this.store.changeSpecVisibleAndRender}
        />

        {/*底部加入购物车、立即购买等按钮*/}
        <Bottom />

        {/*如果是微信小程序环境,就显示分享按钮*/}
        {/* {(window as any).isMiniProgram && <GoodsShare />} */}

        {/*分销员禁用状态提示*/}
        <BStoreClose />
      </div>
    );
  }
}
