import { Actor, Action, IMap } from 'plume2';
import { fromJS } from 'immutable';

export default class GoodsActor extends Actor {
  defaultState() {
    return {
      customer: {},
      //sku信息
      goodsInfo: [],
      //spu信息
      goods: {},
      //分类拥有的所有属性信息
      goodsProps: [],
      //商品关联的具体属性值信息
      goodsPropDetailRels: [],
      //品牌信息
      goodsBrand: {},
      //商品规格
      goodsSpecs: [],
      //商品规格值
      goodsSpecDetails: [],
      //会员等级价
      goodsLevelPrices: [],
      //会员设价
      goodsCustomerPrices: [],
      //订货量设价
      goodsIntervalPrices: [],
      //spu图片集合(前10张,如果sku没有图片)
      images: [],
      //购买数量
      buyNum: 0,
      //是否收藏
      follow: false,
      //购物车数量
      purchaseCount: 0,
      // 店铺信息
      store: {},
      //是否显示促销底部弹框
      show: false,
      //满赠规则
      levelList: [],
      //赠品情况
      giftList: [],
      // 在线客服可见状态
      onlineServiceFlag: false,
      url: '',
      //显示播放的视频
      showVideo: false,
      //图文详情tab
      storeGoodsTabs: [],
      //图文详情tab标签,默认为-1
      tabKey: -1,
      //所有sku信息
      goodsInfos: fromJS([]),
      //批发销售类型下的规格弹框显示
      wholesaleVisible: false,
      //零售销售类型下的规格弹框显示
      retailSaleVisible: false,
      //spu所有返回值
      spuContext: {},
      renderVideo: true,
      renderSlider: true,
      //图文详情的内容
      storeGoodsTabContent: [],
      //领券弹层的状态
      showCouponMask: false,
      // 分享弹层可见状态
      shareVisible: false,
      loading: true,
      isfenxiang:false,
      closeImg:true,
      closeChoose:true,
      isWechatShare:false,
    };
  }

  /**
   * 商品详情信息
   * @param state
   * @param goodsInfo
   * @param goods
   * @param goodsSpecs
   * @param goodsSpecDetails
   * @param goodsLevelPrices
   * @param goodsCustomerPrices
   * @param goodsIntervalPrices
   * @param images
   * @returns {IMap}
   */
  @Action('goods-detail: info')
  goodsDetail(
    state: IMap,
    {
      skuId,
      goodsInfos,
      goods,
      goodsPropDetailRels,
      goodsSpecs,
      goodsSpecDetails,
      goodsLevelPrices,
      goodsCustomerPrices,
      goodsIntervalPrices,
      images
    }
  ) {
    //4.根据当前sku信息,标记每个规格项的默认选中值
    let goodsInfoList = fromJS(goodsInfos);
    if (goodsSpecDetails) {
      goodsInfos = goodsInfoList.map((goodsInfo) => {
        //遍历该规格项对应的所有规格值
        let specStr = goodsSpecDetails
          .filter((specDetail) =>
            goodsInfo.get('mockSpecDetailIds').contains(specDetail.specDetailId)
          )
          .map((spec) => {
            return spec.detailName;
          })
          .join(' ');
        return goodsInfo.set('specText', specStr);
      });
    }
    let goodsInfo = fromJS(goodsInfos).find((item) => {
      return item.get('goodsInfoId') == skuId;
    });
    if(!goodsInfo){
      //如果goodsInfo不存在
      goodsInfo = fromJS(goodsInfos).get(0);
    }
    let allImgsIm;

    //批发销售--spu+sku图片拼接
    if (goods.saleType == 0) {
      allImgsIm = fromJS(images).map((i) => i.get('artworkUrl'));
      goodsInfoList
        .toJS()
        .filter((i) => i.goodsInfoImg)
        .forEach((v) => {
          if (allImgsIm.size >= 10) {
            //最多显示10张图片
            return;
          }
          allImgsIm = allImgsIm.push(v.goodsInfoImg);
        });
    }

    //零售销售--当前sku+spu的图片,sku图片为空时，取spu的第一张图片作为sku的图片，spu也没有图片时，就是空数组
    if (goods.saleType == 1) {
      allImgsIm = goodsInfo.get('goodsInfoImg')
        ? fromJS([]).push(goodsInfo.get('goodsInfoImg'))
        : images.length > 0
          ? fromJS([]).push(images[0].artworkUrl)
          : fromJS([]).push('');
      if (allImgsIm.size > 0) {
        fromJS(images).map((image) => {
          if (allImgsIm.size >= 10) {
            return;
          }
          allImgsIm = allImgsIm.push(image.get('artworkUrl'));
        });
      }
    }

    return state
      .set('goodsInfo', fromJS(goodsInfo))
      .set(
        'spuContext',
        fromJS({
          skuId,
          goodsInfos,
          goods,
          goodsPropDetailRels,
          goodsSpecs,
          goodsSpecDetails,
          goodsLevelPrices,
          goodsCustomerPrices,
          goodsIntervalPrices,
          images
        })
      )
      .set('goodsInfos', fromJS(goodsInfos))
      .set('goods', fromJS(goods))
      .set('goodsPropDetailRels', fromJS(goodsPropDetailRels))
      .set('goodsSpecs', fromJS(goodsSpecs))
      .set('goodsSpecDetails', fromJS(goodsSpecDetails))
      .set('goodsLevelPrices', fromJS(goodsLevelPrices))
      .set('goodsCustomerPrices', fromJS(goodsCustomerPrices))
      .set('goodsIntervalPrices', fromJS(goodsIntervalPrices))
      .set('images', allImgsIm)
      .set('showVideo', fromJS(goods).get('goodsVideo') == '' ? false : true);
  }

  @Action('goods-detail:props')
  setGoodsProps(state: IMap, goodsProps) {
    return state.set('goodsProps', goodsProps);
  }

  @Action('goods-detail:brand')
  setGoodsBrand(state: IMap, goodsBrand) {
    return state.set('goodsBrand', goodsBrand);
  }

  /**
   * 改变商品数量
   * @param state
   * @param num
   * @returns {IMap}
   */
  @Action('goods-detail: change: num')
  changeNum(state: IMap, num) {
    return state.set('buyNum', num);
  }

  /**
   * 存储收藏状态
   * @param state
   * @param status
   * @returns {Map<string, V>}
   */
  @Action('goods-detail: follow')
  follow(state: IMap, status) {
    return state.set('follow', status);
  }

  /**
   * 购物车数量
   * @param state
   * @param count
   * @returns {Map<string, V>}
   */
  @Action('goods-detail: purchase: count')
  purchaseCount(state: IMap, count) {
    return state.set('purchaseCount', count);
  }

  /**
   * 弹框的显示或隐藏
   * @param state
   */
  @Action('goods-detail:modal')
  changeModal(state: IMap) {
    return state.set('show', !state.get('show'));
  }

  /**
   * 关闭弹框
   * @param state
   */
  @Action('goods-detail:modal:close')
  closeModal(state: IMap) {
    return state.set('show', false);
  }

  @Action('goods-detail:marketing')
  marketingDetai(state, gifts) {
    //如有满赠
    if (gifts.levelList.length > 0) {
      return state
        .set('levelList', fromJS(gifts.levelList))
        .set('giftList', fromJS(gifts.giftList));
    }
    return state;
  }

  /**
   * 设置在线客服可见状态
   */
  @Action('goods-detail:setOnlineServiceFlag')
  setOnlineServiceFlag(state: IMap, onlineServiceFlag: boolean) {
    return state.set('onlineServiceFlag', onlineServiceFlag);
  }

  /**
   * 领券弹框的显示或隐藏
   * @param state
   */
  @Action('change:changeCouponMask')
  changeCouponMask(state: IMap) {
    return state.set('showCouponMask', !state.get('showCouponMask'));
  }

  @Action('goods-detail:url')
  saveUrl(state, url) {
    return state.set('url', url);
  }

  @Action('goodsDetail:toggleShowVideo')
  toggleShowVideo(state) {
    return state.set('showVideo', !state.get('showVideo'));
  }

  //图文详情
  @Action('goods-detail:storeGoodsTabs')
  storeGoodsTabs(state, result) {
    return state.set('storeGoodsTabs', fromJS(result));
  }

  //图文详情tab切换
  @Action('goodsDetail:changeTabKey')
  changeTabKey(state, index) {
    return state.set('tabKey', index);
  }

  //初始化店铺信息
  @Action('goodsDetail:initStore')
  initStore(state: IMap, { storeId, storeName, storeLogo, companyType }) {
    return state.set(
      'store',
      fromJS({
        storeId,
        storeName,
        storeLogo,
        companyType
      })
    );
  }

  //批发销售类型规格弹框显示隐藏
  @Action('goodsDetail:changeWholesaleVisible')
  changeWholesaleVisible(state) {
    return state.set('wholesaleVisible', !state.get('wholesaleVisible'));
  }

  //零售销售类型规格弹框显示隐藏
  @Action('goodsDetail:changeRetailSaleVisible')
  changeRetailSaleVisible(state, value) {
    return state.set('retailSaleVisible', value);
  }

  /**
   * 切换分享赚弹框显示与否
   */
  @Action('goodsActor:changeShareVisible')
  changeShareVisible(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }

  //选择规格重新刷新详情页
  @Action('goodsDetail:closeAndRenderRetail')
  closeAndRenderRetail(state, goodsInfo) {
    let newImages = fromJS([]);
    //重置images,使得当前的sku图片是第一张，若无sku图片，将spu的第一张认为是，spu也没有的话，返回空数组
    if (state.get('images').size >= 0) {
      newImages = state
        .get('images')
        .set(
          0,
          goodsInfo.get('goodsInfoImg')
            ? goodsInfo.get('goodsInfoImg')
            : state.get('images').size > 1
              ? state.get('images').get(1)
              : ''
        );
    }
    return state.set('goodsInfo', goodsInfo).set('images', newImages);
  }

  @Action('goodsDetail:renderVideo')
  renderVideo(state) {
    return state
      .set('renderVideo', !state.get('renderVideo'))
      .set('renderSlider', !state.get('renderSlider'));
  }

  //图文详情内容
  @Action('goods-detail:storeGoodsTabContent')
  storeGoodsTabContent(state, result) {
    return state.set('storeGoodsTabContent', fromJS(result));
  }

  @Action('change:shareModal')
  changeShareModal(state) {
    return state.set('shareVisible', !state.get('shareVisible'));
  }

  @Action('loading :end')
  loadingEnd(state) {
    return state.set('loading', false);
  }

  @Action('loading :start')
  loadingStart(state) {
    return state.set('loading', true);
  }

  
  @Action('goods-actor:isfenxiang')
  isfenxiang(state){
    return state.set('isfenxiang',true)
  }

  @Action('goods-actor:closefenxiang')
  closefenxiang(state){
    return state.set('isfenxiang',false)
  }
  
  @Action('goods-actor:closeImg')
  closeImg(state){
    return state.set('closeImg',false)
    .set('closeChoose',false)
  }

  @Action('goods-actor:openImg')
  openImg(state){
    return state.set('closeImg',true)
    .set('closeChoose',true)
  }

  @Action('goods-actor:openWechatShare')
  openWechatShare(state){
    return state.set('isWechatShare',true)
  }

  @Action('goods-actor:closeWechatShare')
  closeWechatShare(state){
    return state.set('isWechatShare',false)
  }
  @Action('member:init')
  init(state: IMap, { customer }) {
    return state
      .set('customer', fromJS(customer))
  }
}
