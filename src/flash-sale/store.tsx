import { Store, IOptions } from 'plume2';
import { fromJS } from 'immutable';
import { config } from 'config';
import * as webApi from './webapi';
import FlashSaleActor from './actor/flash-sale-actor';
import Swiper from 'swiper/dist/js/swiper.js';
import 'swiper/dist/css/swiper.min.css';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new FlashSaleActor()];
  }

  /**
   * 秒杀商品分类
   */
  initCateList = async () => {
    const res: any = await webApi.getCateList();
    if (res && res.code == config.SUCCESS_CODE) {
      this.dispatch(
        'flash-sale: cateList',
        fromJS(res.context.flashSaleCateVOList)
      );
    }
  };

  /**
   * 秒杀商品场次
   */
  initSceneList = async () => {
    const res: any = await webApi.getSceneList();
    if (res && res.code == config.SUCCESS_CODE) {
      //找到当前是第几个
      let index = res.context.flashSaleActivityVOList.findIndex((item) => {
        return (
          item.activityDate == res.context.recentDate &&
          item.activityTime == res.context.recentTime
        );
      });
      res.context.activityStatus = null;
      if (res.context.flashSaleActivityVOList.length > 0) {
        res.context.activityStatus =
          res.context.flashSaleActivityVOList[index].status;
      }
      this.dispatch('flash-sale: sceneList', res.context);
      //获取等级数据
      const that = this;
      new Swiper('.swiper-container-flash', {
        initialSlide: index,
        slidesPerView: 5,
        centeredSlides: true
        // slideToClickedSlide: true,
      });
    }
  };

  /**
   * 选择秒杀商品分类
   * @param cateId
   */
  setCateId = (cateId) => {
    this.dispatch('flash-sale: setCateId', cateId);
  };

  /**
   * 选择秒杀商品场次
   * @param cateId
   */
  setScene = (activityDate, activityTime, status) => {
    this.dispatch('flash-sale: setScene', {
      activityDate,
      activityTime,
      status
    });
  };
}
