import React, { Component } from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import CateList from './component/cate-list';
import ProductList from './component/product-list';
import SceneList from './component/scene-list';
import Blank from 'wmkit/blank';

const style = require('./css/style.css');
const tuoyuan = require('./img/tuoyuan.png')
@StoreProvider(AppStore, { debug: __DEV__ })
export default class PointsMall extends Component<any, any> {
  store: AppStore;


  componentDidMount() {
    this.store.initSceneList();
    this.store.initCateList();
  }

  render() {
    const { state, flashGoodsCount } = this.props;
    const mainStyle = { position: 'relative', backgroundColor: '#fff', zIndex: '99' }
    const defaultStyle = { backgroundColor: '#fafafa' }
    let sceneList = this.store.state().get('sceneList');
    return sceneList ? (
      sceneList.size > 0 ? (
        <div style={state == 'main' ? mainStyle : defaultStyle} id="flashTitle">
          {/* {
            state == 'main' && (
              <div style={{ 
                height: '0.9rem',
                background: '#fff', 
                fontSize: '0.32rem', 
                display: 'flex', 
                width: '100%', 
                justifyContent: 'center', 
                alignItems: 'center' 
                }}
                id='flashText'
                >
                <img src={tuoyuan} alt="" style={{ width: '0.16rem', height: '0.16rem', borderRadius: '50%' }} />
                <span style={{ color: '#333', fontWeight: 'bold', margin: '0 0.16rem' }}>限时秒杀</span>
                <img src={tuoyuan} alt="" style={{ width: '0.16rem', height: '0.16rem', borderRadius: '50%' }} />

              </div>
            )
          } */}
          {/* 场次 */}
          <SceneList state={state}/>
          {/* 分类 */}
          {state != "main" && <CateList />}
          {/* 商品列表 */}
          <ProductList flashGoodsCount={flashGoodsCount} />
        </div>
      ) : (
          <div id="flashTitle" style={{ height: '100vh' }}>
            <Blank img={require('./img/blank.png')} content="敬请期待" />
          </div>
        )
    ) : null;
  }
}
