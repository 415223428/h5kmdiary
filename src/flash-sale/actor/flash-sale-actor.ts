import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

/**
 * Created by yinxianzhi on 2019/05/10.
 */
export default class FlashSaleActor extends Actor {
  defaultState() {
    return {
      sceneList: null, //场次
      cateList: [], //秒杀商品分类
      cateId: null, //选中的秒杀商品分类
      activityDate: null, //选中的活动日期
      activityTime: null, //选中的活动时间
      activityStatus: '' //选中的活动状态
    };
  }

  /**
   * 秒杀商品分类
   */
  @Action('flash-sale: cateList')
  cateList(state: IMap, cateList) {
    return state.set('cateList', fromJS(cateList));
  }

  /**
   * 秒杀商品场次
   */
  @Action('flash-sale: sceneList')
  sceneList(state: IMap, sceneList) {
    return state
      .set('sceneList', fromJS(sceneList.flashSaleActivityVOList))
      .set('activityDate', sceneList.recentDate)
      .set('activityTime', sceneList.recentTime)
      .set('activityStatus', sceneList.activityStatus);
  }

  /**
   * 设置分类
   */
  @Action('flash-sale: setCateId')
  setCateId(state, cateId) {
    return state.set('cateId', cateId);
  }

  /**
   * 设置场次
   */
  @Action('flash-sale: setScene')
  setScene(state, { activityDate, activityTime, status }) {
    return state
      .set('activityDate', activityDate)
      .set('activityTime', activityTime)
      .set('activityStatus', status)
      .set('cateId', null);
  }
}
