import { Fetch } from 'wmkit';

/**
 * 获取秒杀商品分类信息
 */
export const getCateList = () => {
  return Fetch('/flashsale/cateList');
};

/**
 * 获取秒杀商品场次信息
 */
export const getSceneList = () => {
  return Fetch('/flashsale/sceneList');
};

