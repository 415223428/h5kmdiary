import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';
import { WMImage } from 'wmkit';
import { IList } from 'typings/globalType';
import 'swiper/dist/css/swiper.min.css';
import history from 'wmkit/history';
import noop from 'wmkit/noop';
import { Flex } from 'antd-mobile';
import { div } from 'wmkit/common/util';
@Relax
export default class CateList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      setCateId: Function;
      cateList: IList;
      cateId: number;
    };
  };

  static relaxProps = {
    setCateId: noop,
    cateList: 'cateList',
    cateId: 'cateId'
  };

  render() {
    const { cateList, setCateId, cateId } = this.props.relaxProps;
    // let  resultArr = [];
    const len = cateList.size;

    let num = Math.floor(len / 4) ;
    let otherNum = len % 4;

    //判断num是否为偶数
    if(num % 2 === 0){
      num /= 2
    }else{
      num = Math.ceil( num / 2);
      otherNum = num === 1? 0 : otherNum
    }

    let spliceNum = num*4 + otherNum;
    // resultArr[0] = cateList.slice(0,spliceNum);
    // resultArr[1] = cateList.slice(spliceNum);
    // resultArr = cateList;

    return (
      <div className="integral-list" id="cateList">
        <div className="cate-list">
          <div 
            className="cate-content"  
            style={{display:'flex',justifyContent: 'flex-start',flexWrap:'wrap',borderBottom:'1px solid #e6e6e6', background:'#fff',height:'100%'}}>
            {
              // resultArr.map((cateList,key) =>   //所有数据按两行开始渲染
                // <div key={key} style={{padding:'5px 0'}}>
                <div style={{padding:'.15rem'}}>
                  {
                     cateList.map((val ,key) => {
                       return (
                        <div
                          style={{width:'1.62rem',height:'0.6rem',border:'0.02rem solid #333',margin:'0.15rem 0 .15rem .15rem',
                          borderRadius:'0.2885rem',lineHeight:'0.6rem',fontSize: '0.26rem',fontWeight:'bold'}}
                          className={cateId == val.get('cateId') ? 'cate-item cartTab cur' : 'cate-item cartTab'}
                          onClick={() => setCateId(val.get('cateId'))}
                          key={val.get('cateId')}
                        >
                          {val.get('cateName')}
                        </div>)
                     })
                  }
                </div>
              // )
            }
          </div>
        </div>
      </div>
    );
  }
}
