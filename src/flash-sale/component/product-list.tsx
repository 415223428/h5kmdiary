import React from 'react';
import { Link } from 'react-router-dom';
import { Relax, msg, IMap } from 'plume2';
import { noop, WMkit, Blank, WMImage, ListView, Alert } from 'wmkit';
import { IList } from 'typings/globalType';
import history from 'wmkit/history';
import * as _ from 'wmkit/common/util';
import MarketingLabel from '../../../web_modules/biz/marketing-label';
import GoodsNum from '../../../web_modules/biz/goods-num';
import MemberRights from 'src/member-shop/component/member-rights';

const qiang=require('../img/qiang.png')
const start=require('../img/start-soon.png')

@Relax
export default class ProductList extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  props: {
    relaxProps?: {
      cateId: number;
      activityDate: string;
      activityTime: string;
      activityStatus: string;
    };
    flashGoodsCount:number;
  };

  static relaxProps = {
    cateId: 'cateId',
    activityDate: 'activityDate',
    activityTime: 'activityTime',
    activityStatus: 'activityStatus'
  };

  render() {
    const {
      cateId,
      activityDate,
      activityTime,
      activityStatus
    } = this.props.relaxProps;
    if (!activityDate || !activityTime || !activityStatus) {
      return null;
    }
    const {flashGoodsCount} = this.props;

    
    return (
      <div className="list-ul">
        <ListView
          url={'/flashsale/goodsList'}
          params={{
            activityDate,
            activityTime,
            cateId
          }}
          pageSize={flashGoodsCount?flashGoodsCount:10}
          // style={{ height: 'calc(100vh - 34.67vw)' }}
          renderRow={this._goodsRow}
          dataPropsName={'context.flashSaleGoodsVOPage.content'}
          renderEmpty={() => (
            <Blank
              img={require('../img/list-none.png')}
              content="没有符合您要求的商品~"
            />
          )}
        />
      </div>
    );
  }

  /**
   * 列表数据
   */
  _goodsRow = (flashSaleGoods) => {
    const { activityStatus } = this.props.relaxProps;
    let name = '马上抢';
    let isOver = false;
    if (activityStatus == '已结束') {
      name = '已结束';
      isOver = true;
    } else if (activityStatus == '即将开始' || activityStatus == '次日预告') {
      name = '即将开始';
    } else if (flashSaleGoods.stock <= 0) {
      name = '已抢完';
      isOver = true;
    }

    return (
      <div
        key={flashSaleGoods.id}
        className="goods-list"
        onClick={() =>
          history.push('/goods-detail/' + flashSaleGoods.goodsInfoId)
        }
        style={{padding: '0.16rem 0 0.3rem 0.16rem', borderBottom:'1px solid #e6e6e6'}}
      >
        <div className="img-box" style={{width:'2.2rem',height:'2.2rem',margin: '0 0.3rem 0 .1rem'}}>
          <WMImage
            src={flashSaleGoods.goodsInfo.goodsInfoImg}
            // width="100%"
            height="90%"
           style={{objectFit: 'cover'}}
          />
        </div>
        <div className="detail" style={{ position: 'relative' }}>
          <div className="title" style={{color:'#333',fontWeight:'bold',margin: '0.2rem 0 0 0.1rem'}}>{flashSaleGoods.goodsInfo.goodsInfoName}</div>
          <p className="gec">{flashSaleGoods.goodsInfo.specText}</p>

          <div className="bottom">
            <div style={{display:'flex',alignItems:'center', position:'absolute', bottom:'30%'}}>
              <div className="price" style={{ fontSize: '0.4rem',color:'#FF4D4D' }}>
                ￥{_.addZero(flashSaleGoods.price)}
              </div>
              <div className="line-price" style={{marginLeft:'0.2rem'}}>
                ￥{_.addZero(flashSaleGoods.goodsInfo.marketPrice)}
              </div>
              {localStorage.getItem('loginSaleType') == '1'?<span style={{marginLeft: '.26rem',fontSize: '.24rem',color: '#FF4D4D',}}><span style={{color:'#333'}}>预估点数</span>{flashSaleGoods.goodsInfo.kmPointsValue}</span>:null}
            </div>
            {/* {activityStatus != '已结束' && (
              <div style={styles.saleNum}>
                <div style={styles.showInfo}>
                  <div
                    className={'progresss'}
                    style={{
                      width:
                        (flashSaleGoods.stock + flashSaleGoods.salesVolume
                          ? flashSaleGoods.salesVolume /
                            (flashSaleGoods.stock + flashSaleGoods.salesVolume)
                          : 1) * 1.8 + 'rem',
                      height: '0.2rem',
                      backgroundColor: '#43F1C9'
                    }}
                  />
                </div>
                <span style={styles.spanCss}>
                  已抢
                  {flashSaleGoods.stock + flashSaleGoods.salesVolume
                    ? (
                        (flashSaleGoods.salesVolume /
                          (flashSaleGoods.stock + flashSaleGoods.salesVolume)) *
                        100
                      ).toFixed(0)
                    : 100}%
                </span>
              </div>
            )} */}
          </div>
          <div>
            {
              isOver?
                <button style={styles.myGrayBottom}>
                  {name}
                </button>
                :
                <img style={{width:'1.44rem',height:'.48rem',float:'right'}} src={name=='马上抢'?qiang:start} alt=""/>
            }
          </div>

          {/* <button style={isOver ? styles.myGrayBottom : styles.myBottom}>
            {name}
          </button> */}
          {/* 马上抢 */}
        </div>
      </div>
    );
  };
}

//更改样式
const styles = {
  myBottom: {
    position: 'absolute',
    right: '0.1rem',
    // top: '0.95rem',
    bottom: '0.1rem',
    width: '1.44rem',
    height: '0.48rem',
    borderRadius: '0.24rem',
    color: '#fff',
    // backgroundColor: '#F91A53'
    background: 'linear-gradient(to right, #FF6A4D, #FF1A1A)',
    fontSize:'0.26rem'
  },
  myGrayBottom: {
    position: 'absolute',
    right: '0.1rem',
    // top: '0.95rem',
    bottom: '0.1rem',
    width: '1.44rem',
    height: '0.48rem',
    borderRadius: '0.24rem',
    color: '#fff',
    backgroundColor: '#e6e6e6',
    fontSize:'0.26rem'
  },
  spanCss: {
    width: '0.8rem',
    fontSize: '0.2rem',
    color: '#333',
    marginLeft: '2rem'
  },
  saleNum: {
    position: 'absolute',
    width: '3.5rem',
    // right: '-0.2rem',
    bottom: '0.16rem'
  },
  showInfo: {
    position: 'absolute',
    // left: '1rem',
    top: 5,
    width: '1.8rem',
    height: '0.2rem',
    backgroundColor: '#C9F7F5',
    borderRadius: 5,
    marginLeft:'0.1rem'
  }
} as any;
