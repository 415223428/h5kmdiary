import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';
import { WMImage } from 'wmkit';
import { IList } from 'typings/globalType';
import 'swiper/dist/css/swiper.min.css';
import { NavBar, Icon } from 'antd-mobile';
import history from 'wmkit/history';
import noop from 'wmkit/noop';
const tuoyuan = require('../img/tuoyuan.png')
@Relax
export default class SceneList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      setScene: Function;
      sceneList: IList;
      activityDate: string;
      activityTime: string;
    };
    state:string;
  };

  static relaxProps = {
    setScene: noop,
    sceneList: 'sceneList',
    activityDate: 'activityDate',
    activityTime: 'activityTime'
  };

  render() {
    const {
      sceneList,
      setScene,
      activityDate,
      activityTime
    } = this.props.relaxProps;
    const {state}=this.props;
    return (
      <>
        {/*<NavBar*/}
        {/*mode="light"*/}
        {/*className={'navbarBack'}*/}
        {/*icon={<Icon type="left" />}*/}
        {/*onLeftClick={() => window.history.back()}*/}
        {/*>限时抢购</NavBar>*/}
        {/* <div className="integral-list1" style={{borderBottom: '1px solid #e6e6e6', height:'1.2rem'}} id="sceneList"> */}
        {/* <div className="cate-list">
            <div className="cate-content"> */}

        <div className="swiper-container-flash" style={{ borderBottom: '1px solid #e6e6e6', backgroundColor: '#fff' ,overflowX:"auto"}} id="swiper-sceneList">
          {
            state == 'main' && (
              <div style={{
                height: '0.9rem',
                background: '#fff',
                fontSize: '0.32rem',
                display: 'flex',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center'
              }}
              >
                <img src={tuoyuan} alt="" style={{ width: '0.16rem', height: '0.16rem', borderRadius: '50%' }} />
                <span style={{ color: '#333', fontWeight: 'bold', margin: '0 0.16rem' }}>限时秒杀</span>
                <img src={tuoyuan} alt="" style={{ width: '0.16rem', height: '0.16rem', borderRadius: '50%' }} />

              </div>
            )
          }
          <div
            className="swiper-wrapper"
            style={{ backgroundColor: '#fff', width: '1.2rem' }}
          >
            {sceneList.map((val, index) => {
              return (
                <div
                  style={{
                    opacity: 0.5, display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  className={
                    activityDate == val.get('activityDate') &&
                      activityTime == val.get('activityTime')
                      ? 'swiper-slide cate-item cur'
                      : 'swiper-slide cate-item'
                  }
                  // className="swiper-slide cate-item"
                  onClick={() => {
                    setScene(
                      val.get('activityDate'),
                      val.get('activityTime'),
                      val.get('status')
                    );
                  }}
                  key={index}
                >
                  <p className="item-p time" style={{ fontSize: '0.32rem', lineHeight: '0.4rem' }}>{val.get('activityTime')}</p>
                  <p className="item-p status" style={{ fontSize: '0.24rem', lineHeight: '0.4rem' }}>
                    {val.get('status')}
                  </p>
                </div>
              );
            })}
          </div>
        </div>
        {/* </div>
          </div> */}
        {/* </div> */}
      </>
    );
  }
}
