import { Action, Actor, IMap } from 'plume2';

export default class EvaluateCenterActor extends Actor {
  defaultState() {
    return {
      //导航数据
      navData: {},
      //当前的id
      isId: 0,
      //评价数据
      evaluateData: []
    };
  }

  //获取导航数据 待评价数量
  @Action('EvaluateCenter:waitNum')
  setWaitNum(state, value) {
    return state.setIn(['navData','wait'], value);
  }

  //待评价服务数量
  @Action('EvaluateCenter:storeWaitNum')
  setStoreWaitNum(state, value) {
    return state.setIn(['navData','storeWaitNum'], value);
  }

  //选中的id
  @Action('EvaluateCenter:getIsId')
  getIsId(state: IMap, num) {
    return state.set('isId', num);
  }

  //获取评价数据
  @Action('EvaluateCenter:getEvaluateData')
  getEvaluateData(state, value) {
    return state.set('evaluateData', value);
  }
}
