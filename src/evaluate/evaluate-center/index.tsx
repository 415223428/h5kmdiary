import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import Nav from './component/nav';
import CompletedList from './component/evaluate-completedList';
import StoreWaitList from './component/evaluate-storeWaitList';
import WaitList from './component/evaluate-waitList';
import './css/style.css';

/**
 * 评价中心 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class EvaluateCenter extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { tab = 0 } = this.props.location;
    this.store.init(tab);
  }

  render() {
    const isId = this.store.state().get('isId');
    let html = null;
    if (isId == 0) {
      html = <WaitList/>;
    } else if (isId == 1) {
      html = <StoreWaitList/>;
    } else if (isId == 2) {
      html = <CompletedList/>;
    }
    return (
      <div className="evaluate-center">
        <Nav />
        {html}
      </div>
    );
  }
}
