import { Store } from 'plume2';
import { config } from 'config';

import EvaluateCenterActor from './actor/evaluate-center-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  bindActor() {
    return [new EvaluateCenterActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  /**
   * 初始化数据
   * @returns {Promise<void>}
   */
  init = async (tabNum: number) => {
    this._navClick(tabNum);
    Promise.all([
      webapi.queryGoodsTobeEvaluateNum(),
      webapi.queryStoreTobeEvaluateNum()
    ]).then(res=>{
      if (res[0].code == config.SUCCESS_CODE){
        this.dispatch('EvaluateCenter:waitNum',res[0].context)
      }
      if (res[1].code == config.SUCCESS_CODE){
        this.dispatch('EvaluateCenter:storeWaitNum',res[1].context)
      }
    });
  };

  //导航点击事件
  _navClick = (id) => {
    this.dispatch('EvaluateCenter:getIsId', id);
  };
}
