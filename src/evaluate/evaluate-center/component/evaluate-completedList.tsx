import React from 'react';
import { Blank, history, ListView, WMImage } from 'wmkit';
import moment from 'moment';
import { Const } from 'config';

const noneImg = require('../img/list-none.png');
const look= require('../img/look.png')
export default class EvaluateCompletedList extends React.Component<any, any> {
  render() {
    const dataUrl = '/goodsEvaluate/page';
    return (
      <ul className="eval-center-list">
        <ListView
          style={{background:'#fff'}}
          url={dataUrl}
          dataPropsName={'context.goodsEvaluateVOPage.content'}
          renderRow={this._row}
          renderEmpty={() => <Blank img={noneImg} content="暂无已评价消息哦" />}
        />
      </ul>
    );
  }

  _row = (evaluateData) => {
    return (
      <li key={evaluateData.evaluateId}>
        <div className="eval-img">
          <WMImage
            src={evaluateData.goodsImg}
            alt=""
            width="100%"
            height="100%"
          />
        </div>
        <div className="right-brief">
          <span className="eval-title">{evaluateData.goodsInfoName}</span>
          <span className="eval-spec">{evaluateData.specDetails}</span>
          <div className="btn-box">
            {/*<div className="eval-btn">修改评价</div>*/}
            <span className="time" style={{marginLeft:0,color:'#999'}}>
              购买时间:{moment(evaluateData.buyTime).format(Const.DATE_FORMAT)}
            </span>
            <div
              // style={{padding: '0',border: '0',}}
              className="eval-btn mar-left-18"
              style={{border: 'none',width: '1.82rem'}}
              onClick={() => {
                history.push({
                  pathname: '/evaluate/evaluate-drying',
                  state: {
                    storeId: evaluateData.storeId,
                    orderId: evaluateData.orderNo,
                    goodsInfoId: evaluateData.goodsInfoId,
                    evaluateType: 'detail'
                  }
                });
              }}
            >
              <img src={look} alt="" style={{width: '1.82rem',height:'.48rem'}}/>
              {/* <img src={look} alt="" style={{width:'100%'}}/> */}
            </div>
          </div>
        </div>
      </li>
    );
  };
}
