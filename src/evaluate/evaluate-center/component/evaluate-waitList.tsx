import React from 'react';
import { Blank, history, ListView, WMImage } from 'wmkit';
import moment from 'moment';
import { Const } from 'config';

const noneImg = require('../img/list-none.png');
const evashow=require('../img/eva-show.png')
export default class EvaluateList extends React.Component<any, any> {
  render() {
    const dataUrl = '/goodsTobeEvaluate/pageGoodsTobeEvaluate';

    return (
      <ul className="eval-center-list">
        <ListView
        style={{background:'#fff'}}
          url={dataUrl}
          dataPropsName={'context.goodsTobeEvaluateVOPage.content'}
          renderRow={this._row}
          renderEmpty={() => (
            <Blank img={noneImg} content="暂无待评价消息哦" />
          )}
        />
      </ul>
    );
  }

  _row = (evaluateData) => {
    return (
      <li key={evaluateData.id}>
        <div className="eval-img">
          <WMImage
            src={evaluateData.goodsImg}
            alt=""
            width="100%"
            height="100%"
          />
        </div>
        <div className="right-brief">
          <span className="eval-title">{evaluateData.goodsInfoName}</span>
          <span className="eval-spec">{evaluateData.goodsSpecDetail}</span>
          <div className="btn-box">
            <span className="time" style={{marginLeft:0,color:'#999'}}>
              购买时间:{moment(evaluateData.buyTime).format(Const.DATE_FORMAT)}
            </span>
            <div
              // style={{padding: '0',border: '0',}}
              className="eval-btn"
              style={{border: 'none',width: '1.82rem'}}
              onClick={() => {
                history.push({
                  pathname: '/evaluate/evaluate-drying',
                  state: {
                    storeId: evaluateData.storeId,
                    orderId: evaluateData.orderNo,
                    goodsInfoId: evaluateData.goodsInfoId,
                    evaluateType: 'goodsEvaluate'
                  }
                });
              }}
            >
              <img src={evashow} alt="" style={{width: '1.82rem',height:'.48rem'}}/>
              {/* <img src={evashow} alt="" style={{width:'100%'}}/> */}
            </div>
          </div>
        </div>
      </li>
    );
  };
}
