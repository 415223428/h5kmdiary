import React from 'react';
import { IMap, Relax } from 'plume2';
import { noop } from 'wmkit';

@Relax
export default class Nav extends React.Component<any, any> {
  props: {
    relaxProps?: {
      init: Function;
      navData: IMap;
      isId: number;
    };
  };

  static relaxProps = {
    init: noop,
    navData: 'navData',
    isId: 'isId'
  };

  render() {
    const { isId, navData, init } = this.props.relaxProps;
    return (
      <ul className="eval-center-nav">
        <li
          className={isId == 0 ? 'eval-center-li' : ''}
          onClick={() => init(0)}
        >
          <span>待评价 {navData.get('wait')}</span>
        </li>

        <li
          className={1 == isId ? 'eval-center-li' : ''}
          onClick={() => init(1)}
        >
          <span>服务评价 {navData.get('storeWaitNum')}</span>
        </li>

        <li
          className={2 == isId ? 'eval-center-li' : ''}
          onClick={() => init(2)}
        >
          <span>已评价</span>
        </li>
      </ul>
    );
  }
}
