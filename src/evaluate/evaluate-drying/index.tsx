import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import FindAnonymousEval from './component/find-anonymous-eval';
import AnonymousEval from './component/anonymous-eval';
import './css/style.css';
import { history } from 'wmkit';

/**
 * 评价中心 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class EvaluateDrying extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const {
      storeId,
      orderId,
      goodsInfoId,
      evaluateType
    } = history.location.state;
    this.store.init(storeId, orderId, goodsInfoId, evaluateType);
  }

  render() {
    const evaluateType = this.store.state().get('evaluateType');
    return (
      <div className="evaluate-drying">
         {/*已评价 */}
        {
          evaluateType=='detail'?
            <FindAnonymousEval />
            :
            <AnonymousEval />
        }

        {/* 待评价、服务评价 */}
      </div>
    );
  }
}
