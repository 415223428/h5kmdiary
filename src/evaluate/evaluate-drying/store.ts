import { Store } from 'plume2';
import { Alert, history, WMkit } from 'wmkit';
import { config } from 'config';
import * as webapi from './webapi';
import AnonyActor from './actor/anony-actor';

export default class AppStore extends Store {
  bindActor() {
    return [new AnonyActor()];
  }

  constructor(props) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  init = (storeId, orderId, goodsInfoId, evaluateType) => {
    this.dispatch('evaluate:evaluateType', evaluateType);
    if (evaluateType=='detail'){
      webapi.getEvaluateDetail(storeId, orderId, goodsInfoId).then(res => {
        if (res.code==config.SUCCESS_CODE){
          this.dispatch('evaluate:read:baseInfo', res.context);
        }
      })
    } else {
      goodsInfoId = goodsInfoId?goodsInfoId:'-1';
      webapi.getBaseInfo(storeId, orderId, goodsInfoId).then(res => {
        if (res.code==config.SUCCESS_CODE){
          this.transaction(() => {
            this.dispatch('evaluate:baseInfo', res.context);
          });
        }
      })
    }
  };

  /**
   * 星星评分
   * @param flag 0：商品符合度  1：店家服务态度  2：物流发货速度  3：商品评分
   * @param val 分数
   */
  score = (flag, val) => {
    this.dispatch('evaluate:score',{flag: flag, val: val});
  };

  /**
   * 输入订单备注
   * @param value
   */
  enterRemark = ({ remark, storeId }) => {
    this.dispatch('order: remark: change', { remark, storeId });
  };

  /**
   * 评价文本内容
   * @param text
   */
  changeTextArea = (text) => {
    this.dispatch('evaluate:textarea', text);
  };

  /**
   * 晒单图片
   * @param fileList
   */
  editImages = (fileList) => {
    this.dispatch('evaluate:enclosures', fileList);
  };

  /**
   * 上传附件
   */
  addImage = (image) => {
    this.dispatch('evaluate:addImage', image);
  };

  /**
   * 删除附件
   */
  removeImage = (index) => {
    this.dispatch('evaluate:removeImage', index);
  };

  /**
   * 是否匿名提交
   * 0：否，1：是
   */
  isAnonymous = (val) => {
    this.dispatch('evaluate:isAnonymous', val);
  };

  save = WMkit.onceFunc(() => {

    const serverEvaluateFlag = this.state().get('isShow');
    const evaluateType = this.state().get('evaluateType');

    const storeEvaluate = this.state().get('storeEvaluate');
    const orderEvaluate = this.state().get('orderEvaluate');
    const enclosures = this.state().get('enclosures');
    const storeBaseInfo = this.state().get('storeInfo');
    const orderBaseInfo = this.state().get('orderBaseInfo');
    const tid = this.state().get('tid');

    const storeEvaluateJS = storeEvaluate.toJS();
    const orderEvaluateJS = orderEvaluate.toJS();
    const {goodsScore, serverScore, logisticsScore} = storeEvaluateJS;
    const {evaluateScore, evaluateContent} = orderEvaluateJS;

    if (evaluateType == 'goodsEvaluate'){
      //待评价标签点击进来并且服务没有被评价过
      if (serverEvaluateFlag == 0) {
        if (!goodsScore && !serverScore && !logisticsScore ){
          if (!evaluateScore){
            Alert({text: '商品评分不可为空'});
            return false;
          } else if (!evaluateContent || evaluateContent.trim()=='') {
            Alert({text: '评价晒图文本不可为空'});
            return false;
          }
        } else if (!evaluateScore){
          Alert({text: '商品评分不可为空'});
          return false;
        } else if (!evaluateContent || evaluateContent.trim()=='') {
          Alert({text: '评价晒图文本不可为空'});
          return false;
        } else if (!goodsScore) {
          Alert({text: '商品质量不可为空'});
          return false;
        } else if (!serverScore) {
          Alert({text: '店家服务态度不可为空'});
          return false;
        } else if (!logisticsScore) {
          Alert({text: '物流发货速度不可为空'});
          return false;
        }
      } else if (serverEvaluateFlag!=0){//待评价标签点击进来且服务已评价
        if (!evaluateScore){
          Alert({text: '商品评分不可为空'});
          return false;
        } else if (!evaluateContent || evaluateContent.trim()=='') {
          Alert({text: '评价晒图文本不可为空'});
          return false;
        }
      }
    } else if (evaluateType == 'storeEvaluate'){ //评价服务标签点击进来的时候
      if (!goodsScore) {
        Alert({text: '商品质量不可为空'});
        return false;
      } else if (!serverScore) {
        Alert({text: '店家服务态度不可为空'});
        return false;
      } else if (!logisticsScore) {
        Alert({text: '物流发货速度不可为空'});
        return false;
      }
    }

    let imageList = [];
    enclosures.map((v,k)=>{
      const name = v.get('name');
      let imgName;
      if (name.length>20){
        imgName = name.toString().substring(name.length-15,name.length);
      } else {
        imgName = name;
      }
      let img = {
        imageName: imgName,
        artworkUrl: v.get('image')
      };
      imageList.push(img);
    });

    const evaluateAddRequest = {
      storeEvaluateAddRequestList: {
        ...storeEvaluateJS,
        storeId: storeBaseInfo.get('storeId'),
        storeName: storeBaseInfo.get('storeName'),
        orderNo: tid
      },
      goodsEvaluateAddRequest: {
        ...orderEvaluateJS,
        storeId: orderBaseInfo.get('storeId'),
        storeName: storeBaseInfo.get('storeName'),
        goodsId: orderBaseInfo.get('spuId'),
        goodsInfoId: orderBaseInfo.get('skuId'),
        goodsInfoName: orderBaseInfo.get('skuName'),
        orderNo: tid,
        specDetails: orderBaseInfo.get('specDetails')
      },
      goodsEvaluateImageAddRequest: imageList
    };

    webapi.save(evaluateAddRequest).then(res => {
      if (res.code == config.SUCCESS_CODE){
        history.push({
          pathname: '/evaluate/evaluate-center',
          state: {
            tab: 1
          }
        });

      } else {
        Alert({text: res.message})
      }
    })
  }, 1000);
}
