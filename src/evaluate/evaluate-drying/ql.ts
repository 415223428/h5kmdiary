import { QL } from 'plume2';


/**
 * 计算输入字数
 * @type {plume2.QueryLang}
 */
export const textareaSizeQL = QL('textareaSizeQL', [
  ['orderEvaluate','evaluateContent'],
  (textarea) => {
    return textarea ? textarea.length : 0
  }
])