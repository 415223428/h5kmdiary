import { Fetch } from 'wmkit';


/**
 * 店铺综合评分、订单信息
 * @returns
 */
export const getBaseInfo = (storeId, orderId, goodsInfoId) => {
  return Fetch('/goodsEvaluate/getGoodsAndStore',{
    method: 'POST',
    body: JSON.stringify({storeId: storeId, tid: orderId, skuId: goodsInfoId})
  })
};


/**
 * 保存评价
 * @returns
 */
export const save = (evaluateAddRequest) => {
  return Fetch('/goodsEvaluate/add', {
    method: 'POST',
    body: JSON.stringify(evaluateAddRequest)
  })
};

/**
 * 评价详情
 * @returns
 */
export const getEvaluateDetail = (storeId, orderId, goodsInfoId) => {
  return Fetch('/goodsEvaluate/getEvaluate',{
    method: 'POST',
    body: JSON.stringify({storeId: storeId, tid: orderId, skuId: goodsInfoId})
  })
};
