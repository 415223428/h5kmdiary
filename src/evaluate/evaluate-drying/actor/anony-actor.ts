import { Action, Actor, IMap } from 'plume2';
import { fromJS } from 'immutable';

/**
 * Created by chenpeng on 2017/7/4.
 */
export default class AnonyActor extends Actor {
  defaultState() {
    return {
      //晒单图片
      enclosures: [],
      //店铺评价
      storeBaseInfo: {},
      //订单信息
      orderBaseInfo: {},
      //店铺评分
      storeEvaluate: {},
      //订单评分
      orderEvaluate: {},
      //店铺信息
      storeInfo: {},
      //是否显示
      isShow: 0,
      //评价类型
      evaluateType: '',
      //店铺信息
      storeVO: {},
      //订单ID
      tid: '',
      //订单创建时间
      createTime: '',
    }

  }

  @Action('evaluate:evaluateType')
  seteEvaluateType(state: IMap, evaluateType) {
    return state.set('evaluateType', evaluateType);
  }

  /**
   * 店铺、商品信息
   * @param state
   * @param baseinfo
   */
  @Action('evaluate:baseInfo')
  setBaseInfo(state: IMap, baseinfo) {
    return state.set('storeBaseInfo', fromJS(baseinfo.storeEvaluateSumVO))
      .set('orderBaseInfo', fromJS(baseinfo.tradeVO))
      .set('isShow', baseinfo.storeTobe)
      .set('storeInfo', fromJS(baseinfo.storeVO))
      .set('tid',baseinfo.tid)
      .set('createTime', baseinfo.createTime);
  }

  /**
   * 是否展示
   * @param state
   * @param show
   */
  @Action('evaluate:isShow')
  setIsShow(state: IMap, show){
    const {server,goods} = show;
    return state.setIn(['isShow','server'], server)
      .setIn(['isShow','goods'], goods);
  }

  /**
   * 星星评分
   * @param state
   * @param score
   */
  @Action('evaluate:score')
  setScore(state:IMap, score) {
    const {flag, val} = score;
    switch (flag) {
      case 0:
        return state.setIn(['storeEvaluate','goodsScore'], val);
      case 1:
        return state.setIn(['storeEvaluate','serverScore'], val);
      case 2:
        return state.setIn(['storeEvaluate','logisticsScore'], val);
      case 3:
        return state.setIn(['orderEvaluate','evaluateScore'], val);
    }
  }

  /**
   * 评价内容
   * @param state
   * @param textarea
   */
  @Action('evaluate:textarea')
  setTextArea(state: IMap, textarea) {
    return state.setIn(['orderEvaluate','evaluateContent'], textarea);
  }

  /**
   * 晒单照片
   * @param state
   * @param fileList
   */
  @Action('evaluate:enclosures')
  setEnclosures(state: IMap, fileList){
    return state.set('enclosures', fromJS(fileList));
  }

  /**
   * 是否匿名提交
   * @param state
   * @param isAnonymous
   */
  @Action('evaluate:isAnonymous')
  setAnonymous(state: IMap, isAnonymous) {
    return state.setIn(['orderEvaluate','isAnonymous'], isAnonymous);
  }

  /**
   * 上传附件
   */
  @Action('evaluate:addImage')
  addImage(state: IMap, image) {
    return state.set('enclosures', state.get('enclosures').push(fromJS(image)));
  }

  /**
   * 删除附件
   */
  @Action('evaluate:removeImage')
  removeImage(state: IMap, index: number) {
    return state.set('enclosures', state.get('enclosures').remove(index));
  }

  /**
   * 评价详情
   * @param state
   * @param baseinfo
   */
  @Action('evaluate:read:baseInfo')
  setEvaluateDetail(state: IMap, baseinfo) {
    return state.set('storeBaseInfo', fromJS(baseinfo.storeEvaluateSumVO))
      .set('orderBaseInfo', fromJS(baseinfo.tradeVO))
      .set('storeEvaluate', fromJS(baseinfo.storeEvaluateVO))
      .set('orderEvaluate', fromJS(baseinfo.goodsEvaluateVO))
      .set('enclosures', fromJS(baseinfo.goodsEvaluateImageVOS))
      .set('storeVO', fromJS(baseinfo.storeVO))
      .set('createTime', baseinfo.createTime);
  }
}
