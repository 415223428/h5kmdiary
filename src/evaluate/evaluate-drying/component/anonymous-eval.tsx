import React from 'react';
import { IMap, Relax } from 'plume2';
import { Check, noop, Star, UploadImage, WMImage, Button } from 'wmkit';
import { textareaSizeQL } from '../ql';
import { IList } from 'typings/globalType';
import moment from 'moment';
import { Const } from 'config';

const LongBlueButton = Button.LongBlue;
const submit=require('../img/submit.png')

@Relax
export default class AnonymousEval extends React.Component<any, any> {
  props: {
    relaxProps?: {
      storeBaseInfo: IMap;
      orderBaseInfo: IMap;
      isShow: number;
      score: Function;
      changeTextArea: Function;
      addImage: Function;
      removeImage: Function;
      textareaSize: number;
      enclosures: IList;
      orderEvaluate: IMap;
      isAnonymous: Function;
      storeInfo: IMap;
      storeEvaluate: IMap;
      evaluateType: string;
      save: Function;
      createTime: string;
    };
  };

  static relaxProps = {
    storeBaseInfo: 'storeBaseInfo',
    orderBaseInfo: 'orderBaseInfo',
    isShow: 'isShow',
    score: noop,
    changeTextArea: noop,
    addImage: noop,
    removeImage: noop,
    textareaSize: textareaSizeQL,
    enclosures: 'enclosures',
    orderEvaluate: 'orderEvaluate',
    isAnonymous: noop,
    storeInfo: 'storeInfo',
    storeEvaluate: 'storeEvaluate',
    evaluateType: 'evaluateType',
    save: noop,
    createTime: 'createTime'
  };

  render() {
    const {
      orderBaseInfo,
      isShow,
      score,
      changeTextArea,
      textareaSize,
      enclosures,
      orderEvaluate,
      storeInfo,
      storeEvaluate,
      evaluateType,
      save,
      createTime
    } = this.props.relaxProps;
    const isAnonymous = orderEvaluate.get('isAnonymous') == 1 ? true : false;
    return (
      <div>
        {evaluateType == 'goodsEvaluate' ? (
          <div>
            <div className="eval-drying-score">
              <div className="score-img">
                <WMImage
                  src={orderBaseInfo.get('pic')}
                  alt=""
                  width="100%"
                  height="100%"
                />
              </div>
              <div className="right-score">
                <div className="eval-container">
                  <div className="score" style={{marginBottom:'0.1rem'}}>{orderBaseInfo.get('skuName')}</div>
                  <div className="name-time" style={{marginBottom:'0.1rem'}}>
                    <span className="left-span">
                      {orderBaseInfo.get('specDetails')}
                    </span>
                  </div>
                  {createTime && (
                    <div>
                      <span className="right-span" style={{color:'#999'}}>
                        购买时间:{moment(createTime).format(Const.DATE_FORMAT)}
                      </span>
                    </div>
                  )}
                </div>
              </div>
            </div>

            <div className="down-content">
              <div className="eval-container">
                <span className="left-eval-text">商品评分</span>
                <Star
                  star={orderEvaluate.get('evaluateScore')}
                  handleClick={(value) => {
                    score(3, value);
                  }}
                />
              </div>
            </div>

            <div className="eval-drying-Anony">
              <div className="writing-eval">
                <textarea
                  className="current-text"
                  placeholder="分享心得体验，给大家多点参考吧"
                  maxLength={500}
                  value={orderEvaluate.get('evaluateContent')}
                  onChange={(e) => changeTextArea(e.target.value.trim())}
                />
                <p className="count-p">{textareaSize}/500</p>
              </div>
              <div className="image-scroll border-bottom">
                {enclosures.map((v, i) => {
                  return (
                    <div
                      className="refund-file delete-img"
                      key={'enclosures' + i}
                    >
                      <WMImage
                        src={v.get('image')}
                        width="53px"
                        height="53px"
                      />
                      <a
                        href="javascript:void(0)"
                        onClick={() => this.removeImage(i)}
                      >
                        ×
                      </a>
                    </div>
                  );
                })}
                {enclosures.count() < 9 ? (
                  <div className="refund-file">
                    <UploadImage
                      onSuccess={this._uploadImage}
                      onLoadChecked={null}
                      repeat
                      className="upload"
                      size={5}
                      fileType=".jpg,.png,.jpeg,.gif"
                    >
                      <div className="upload-item">
                        <div className="upload-content">
                          <i className="iconfont icon-add11" />
                        </div>
                      </div>
                    </UploadImage>
                  </div>
                ) : null}
                <p className="file-tips" style={{color:'#999'}}>
                  仅支持jpg、jpeg、png、gif文件，最多上传9张，大小不超过5M
                </p>
              </div>
              <div className="evaluate-check">
                <Check checked={isAnonymous} onCheck={() => this._subType()} />
                <span className="check-name" onClick={() => this._subType()}>
                  匿名提交
                </span>
              </div>
            </div>
          </div>
        ) : null}

        {isShow == 0 ? (
          <div className="store-message">
            <div className="up-content">
              <div className="left-pointer">
                <img src={storeInfo.get('storeLogo')} alt="" />
              </div>
              <span className="right-title">{storeInfo.get('storeName')}</span>
            </div>
            <div className="down-content">
              <span className="eval-title">请对店铺服务进行评价</span>
              <div className="eval-container">
                <span className="left-eval-text">商品质量</span>
                <Star
                  star={storeEvaluate.get('goodsScore')}
                  handleClick={(value) => {
                    score(0, value);
                  }}
                />
              </div>
              <div className="eval-container">
                <span className="left-eval-text">服务态度</span>
                <Star
                  star={storeEvaluate.get('serverScore')}
                  handleClick={(value) => {
                    score(1, value);
                  }}
                />
              </div>
              <div className="eval-container">
                <span className="left-eval-text">发货速度</span>
                <Star
                  star={storeEvaluate.get('logisticsScore')}
                  handleClick={(value) => {
                    score(2, value);
                  }}
                />
              </div>
            </div>
          </div>
        ) : null}
        <div
          // className="register-btn"
          style={{ padding:0,lineHeight:'0.1rem'}}
        >
          {/* <LongBlueButton
            text="提交"
            onClick={() => {
              save();
            }}
          /> */}
          <img src={submit} alt="" style={{width: '100%'}}
               onClick={() => {
                 save();
               }}
          />
        </div>
      </div>
    );
  }

  /**
   * 上传附件
   */
  _uploadImage = (result, file, data) => {
    let { context } = result;
    const image = {
      image: context[0],
      name: file.name
    };
    if (context) {
      this.props.relaxProps.addImage(image);
    }
  };

  /**
   * 删除附件
   */
  removeImage = (index) => {
    this.props.relaxProps.removeImage(index);
  };

  /**
   * 是否匿名提交
   * @private
   */
  _subType = () => {
    const { orderEvaluate, isAnonymous } = this.props.relaxProps;
    const Anonymous = orderEvaluate.get('isAnonymous') == 1 ? true : false;
    if (Anonymous) {
      isAnonymous(0);
    } else {
      isAnonymous(1);
    }
  };
}
