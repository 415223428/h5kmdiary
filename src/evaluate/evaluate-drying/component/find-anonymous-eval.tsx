import React from 'react';
import { IMap, Relax } from 'plume2';
import { Check, Star, WMImage } from 'wmkit';
import { IList } from 'typings/globalType';
import moment from 'moment';
import { Const } from 'config';

@Relax
export default class AnonymousEval extends React.Component<any, any> {
  props: {
    relaxProps?: {
      orderBaseInfo: IMap;
      enclosures: IList;
      orderEvaluate: IMap;
      storeEvaluate: IMap;
      storeVO: IMap;
      createTime: string;
    };
  };

  static relaxProps = {
    orderBaseInfo: 'orderBaseInfo',
    enclosures: 'enclosures',
    orderEvaluate: 'orderEvaluate',
    storeEvaluate: 'storeEvaluate',
    storeVO: 'storeVO',
    createTime: 'createTime'
  };

  render() {
    const {
      orderBaseInfo,
      enclosures,
      orderEvaluate,
      storeEvaluate,
      storeVO,
      createTime
    } = this.props.relaxProps;
    return (
      <div>
        {orderBaseInfo == null ? null : (
          <div>
            <div className="eval-drying-score">
              <div className="score-img">
                <WMImage
                  src={orderBaseInfo.get('pic')}
                  alt=""
                  width="100%"
                  height="100%"
                />
              </div>
              <div className="right-score">
                <div className="eval-container">
                  <div className="score" style={{marginBottom:'0.1rem'}}>{orderBaseInfo.get('skuName')}</div>
                  <div className="name-time" style={{marginBottom:'0.1rem'}}>
                    <span className="left-span">
                      {orderBaseInfo.get('specDetails')}
                    </span>
                  </div>
                  {createTime && (
                    <div >
                      <span className="right-span">
                        购买时间:{moment(createTime).format(Const.DATE_FORMAT)}
                      </span>
                    </div>
                  )}
                </div>
              </div>
            </div>

            <div className="down-content">
              <div className="eval-container">
                <span className="left-eval-text">商品评分</span>
                <Star star={orderEvaluate.get('evaluateScore')} />
              </div>
            </div>

            <div className="eval-drying-Anony">
              <span className="eval-explain">
                {orderEvaluate.get('evaluateContent')}
              </span>
              <div className="image-scroll border-bottom">
                {enclosures.map((v, i) => {
                  return (
                    <div
                      className="refund-file delete-img"
                      key={'enclosures' + i}
                    >
                      <WMImage
                        src={v.get('artworkUrl')}
                        width="53px"
                        height="53px"
                      />
                    </div>
                  );
                })}
              </div>
              {orderEvaluate.get('isAnonymous') == 1 && (
                <div className="evaluate-check">
                  <span className="check-name" style={{ marginLeft: 0 }}>
                    匿名提交
                  </span>
                </div>
              )}
            </div>
          </div>
        )}
        {storeEvaluate == null ? null : (
          <div className="store-message">
            <div className="up-content">
              <div className="left-pointer">
                <img src={storeVO.get('storeLogo')} alt="" />
              </div>
              <span className="right-title">{storeVO.get('storeName')}</span>
            </div>
            <div className="down-content">
              <span className="eval-title">请对店铺服务进行评价</span>
              <div className="eval-container">
                <span className="left-eval-text">商品质量</span>
                <Star star={storeEvaluate.get('goodsScore')} />
              </div>
              <div className="eval-container">
                <span className="left-eval-text">服务态度</span>
                <Star star={storeEvaluate.get('serverScore')} />
              </div>
              <div className="eval-container">
                <span className="left-eval-text">发货速度</span>
                <Star star={storeEvaluate.get('logisticsScore')} />
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
