import { Fetch,WMkit } from 'wmkit';

export const sendCode = (phone, id) => {
  return Fetch(`/third/login/bind/sendCode/${phone}/${id}`);
};

export const bindWechat = (id, mobile, verifyCode, shareUserId) => {
  return Fetch('/third/login/wechat/auth/bind', {
    method: 'POST',
    body: JSON.stringify({
      id: id,
      phone: mobile,
      verifyCode: verifyCode,
      shareUserId: shareUserId,
        inviteeId: WMkit.inviteeId()
    })
  });
};
