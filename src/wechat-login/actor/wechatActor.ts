import { Actor, Action } from 'plume2';
import { fromJS } from 'immutable';
export default class WechatActor extends Actor {
  defaultState() {
    return {
      mobile: '',
      code: '',
      buttonValue: '发送验证码',
      info: {
        id: '',
        name: '',
        headImgUrl: ''
      }
    };
  }

  @Action('INIT')
  init(state, data) {
    return state.set('info', fromJS(data));
  }

  /**
   * 手机号码状态值
   * @param state
   * @param mobile
   */
  @Action('safepass:mobile')
  getMobile(state, mobile: string) {
    return state.set('mobile', mobile);
  }

  /**
   * 验证码状态值
   * @param state
   * @param code
   */
  @Action('safepass:code')
  getCode(state, code: string) {
    return state.set('code', code);
  }
}
