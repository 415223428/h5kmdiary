import {Store} from 'plume2';
import {_, Alert, ValidConst} from 'wmkit';
import * as webapi from './webapi';
import {config} from 'config';
import WechatActor from './actor/wechatActor';
import {getShareUserId} from 'biz';

export default class AppStore extends Store {
  bindActor() {
    return [new WechatActor()];
  }

  constructor(props) {
    super(props);
    (window as any)._store = this;
  }

  init = (id, name, headImgUrl) => {
    this.dispatch('INIT', { id, name, headImgUrl });
  };

  getMobile = (value: string) => {
    this.dispatch('safepass:mobile', value);
  };

  getCode = (value: string) => {
    this.dispatch('safepass:code', value);
  };

  sendCode = async () => {
    const mobile = this.state().get('mobile');
    const id = this.state().getIn(['info', 'id']);

    return webapi.sendCode(mobile, id).then((res) => {
      const { code, message } = res;
      if (code == config.SUCCESS_CODE) {
        Alert({
          text: '验证码已发送，请注意查收！'
        });
      } else {
        Alert({
          text: message
        });
        return Promise.reject(message);
      }
    });
  };

  bind = async () => {
    const mobile = this.state().get('mobile');
    const verifyCode = this.state().get('code');
    const id = this.state().getIn(['info', 'id']);
    const shareUserId = getShareUserId();

    if (_testTel(mobile)) {
      if (verifyCode == '') {
        Alert({
          text: '请填写验证码！'
        });
        return false;
      } else {
        const { code, message, context } = await webapi.bindWechat(
          id,
          mobile,
          verifyCode,
          shareUserId
        );
        if (code == config.SUCCESS_CODE) {
          _.switchLogin(context);
        } else {
          Alert({
            text: message
          });
          return false;
        }
      }
    } else {
    }
  };
}

/**
 * 手机号码校验*/
function _testTel(tel: string) {
  const regex = ValidConst.phone;
  if (tel == '') {
    Alert({
      text: '请填写手机号！'
    });
    return false;
  } else if (!regex.test(tel)) {
    Alert({
      text: '无效的手机号！'
    });
    return false;
  } else {
    return true;
  }
}
