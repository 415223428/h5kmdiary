import React from 'react';
import { IMap, Relax } from 'plume2';
import { noop, Button, FormInput, Alert, ValidConst } from 'wmkit';
import AppStore from '../store';
import '../css/wechat-login.css'
const TimerButton = Button.Timer;

@Relax
export default class WechatLoginForm extends React.Component<any, any> {
  store: AppStore;
  props: {
    relaxProps?: {
      code: string;
      mobile: string;
      buttonValue: string;
      info: IMap;
      getMobile: Function;
      getCode: Function;
      sendCode: Function;
    };
  };

  static relaxProps = {
    code: 'code',
    mobile: 'mobile',
    buttonValue: 'buttonValue',
    info: 'info',
    getMobile: noop,
    getCode: noop,
    sendCode: noop
  };

  constructor(props: any) {
    super(props);
  }

  render() {
    const {
      mobile,
      code,
      getMobile,
      getCode,
      buttonValue,
      sendCode,
      info
    } = this.props.relaxProps;
    return (
      <div className="register-box wechat-login-box">
        <div className="register-tips wechat-login-tip">
          为了您的账户安全，请绑定一个手机号：
        </div>

        <FormInput
          label="手机号"
          type="number"
          placeHolder="请输入您的手机号"
          pattern="[0-9]*"
          defaultValue={mobile}
          onChange={(e) => getMobile(e.target.value)}
          maxLength={11}
        />

        <FormInput
          label="验证码"
          type="number"
          placeHolder="请输入验证码"
          pattern="[0-9]*"
          defaultValue={code}
          onChange={(e) => getCode(e.target.value)}
          maxLength={6}
          other={
            <TimerButton
              text={buttonValue}
              shouldStartCountDown={() => this._sendCode(mobile)}
              resetWhenError
              onClick={() => sendCode()}
            />
          }
        />
      </div>
    );
  }

  _sendCode = (tel: string) => {
    const regex = ValidConst.phone;
    if (tel == '') {
      Alert({
        text: '请填写手机号！'
      });
      return false;
    } else if (!regex.test(tel)) {
      Alert({
        text: '无效的手机号！'
      });
      return false;
    } else {
      return true;
    }
  };
}
