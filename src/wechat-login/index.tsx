import React from 'react';
import { StoreProvider } from 'plume2';
import AppStore from './store';
import WechatLoginForm from './component/wechat-login-form';
import './css/wechat-login.css'

@StoreProvider(AppStore, { debug: __DEV__ })
export default class WechatLogin extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { id, name, headImgUrl } =
      this.props.location.state && this.props.location.state.info;
    this.store.init(id, name, headImgUrl);
  }

  render() {
    return (
      <div className="register-content wechat-login">
        <WechatLoginForm />
        {/* <p className="bottom-tip">手机号已注册，请更换手机号</p> */}
        <button
          className="btn btn-primary wechat-login-btn"
          onClick={() => {
            this.store.bind();
          }}
        >
          绑定
        </button>
      </div>
    );
  }
}
