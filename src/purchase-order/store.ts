import { msg, Store } from 'plume2';
import { List, fromJS } from 'immutable';

import { Alert, history, Confirm, _, WMkit, storage } from 'wmkit';
import { putPurchase, delPurchase, putSkuMarketingCache } from 'biz';

import { config, cache } from 'config';
import EditActor from './actor/edit-actor';
import SkuActor from './actor/sku-actor';
import * as webapi from './webapi';
import SpuActor from './actor/spu-actor';
import StoreInfoActor from './actor/storeInfo-actor';
import IntervalPriceActor from './actor/intervalPrice-actor';
import LoadingActor from './actor/loading-actor';

export default class AppStore extends Store {
  timer: any;

  bindActor() {
    return [
      new EditActor(),
      new SkuActor(),
      new SpuActor(),
      new StoreInfoActor(),
      new IntervalPriceActor(),
      new LoadingActor()
    ];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  //;;;;;;;;;;;;;action;;;;;;;;;;;;;;;;;;;;;;;
  init = async () => {
    // 清除确认订单页面选择的优惠券信息和积分填写信息
    storage('session').del(cache.ORDER_CONFIRM_COUPON);
    storage('session').del(cache.ORDER_POINT);
    // 是否已登录
    this.dispatch('purchase:setLoginFlag', !!window.token);

    //检查这个店铺是否是有效状态
    const status = await webapi.fetchShopStatus() as any;
    if (!status.context) {
      Confirm({
        text: '该店铺已失效，不可在店铺内购买商品',
        okBtn: '查看个人中心',
        confirmCb: () => history.push('/user-center')
      });
      return;
    }

    // 勾选的sku
    let skuIds = [];
    if (this.state().get('skus')) {
      this.state()
        .get('skus')
        .forEach((sku) => {
          if (sku.get('checked')) {
            skuIds.push(sku.get('goodsInfoId'));
          }
        });
    }

    let purInfo;
    //初始化购物车
    if (WMkit.isLoginOrNotOpen()) {
      const { context, code, message } = (await webapi.fetchPurchaseOrder(
        skuIds
      )) as any;
      purInfo = context;
      if (code !== 'K-000000') {
        Alert({ text: message });
        return;
      }
    } else {
      const purCache = localStorage.getItem(WMkit.purchaseCache());
      const skuMarArr =
        JSON.parse(localStorage.getItem(cache.SKU_MARKETING_CACHE)) || [];
      if (purCache) {
        const purArr = JSON.parse(purCache) as Array<any>;
        const { context, code, message } = await webapi.fetchPurchaseFromCache(
          purArr,
          skuIds,
          skuMarArr
        );
        if (code !== 'K-000000') {
          Alert({ text: message });
          return;
        }
        purInfo = context;
        // 将后端过滤后的信息 覆盖 前端缓存,保持数据一致
        const cacheTmp = purInfo.goodsInfos.map((info) => {
          return {
            goodsInfoId: info.goodsInfoId,
            goodsNum: info.buyCount,
            invalid: info.goodsStatus == 2
          };
        });
        localStorage.setItem(
          WMkit.purchaseCache(),
          JSON.stringify(cacheTmp || [])
        );
        localStorage.setItem(
          cache.SKU_MARKETING_CACHE,
          JSON.stringify(purInfo.goodsMarketings || [])
        );
      } else {
        purInfo = {
          companyInfos: [],
          goodsInfos: [],
          goodsIntervalPrices: [],
          goodses: []
        };
      }
    }

    // 赠品详细信息，格式{goodsInfoId: goodsInfo}
    let giftGoodsInfos = fromJS({});

    let storeMarketingMap =
      (purInfo.storeMarketingMap && fromJS(purInfo.storeMarketingMap)) ||
      fromJS({});
    let goodsMarketingMap =
      (purInfo.goodsMarketingMap && fromJS(purInfo.goodsMarketingMap)) ||
      fromJS({});
    let goodsMarketings =
      (purInfo.goodsMarketings && fromJS(purInfo.goodsMarketings)) || List();

    goodsMarketingMap.forEach((marketings, key) => {
      marketings.forEach((marketing) => {
        if (marketing.getIn(['goodsList', 'goodsInfos'])) {
          marketing.getIn(['goodsList', 'goodsInfos']).forEach((goodsInfo) => {
            // 商品单位
            let goodsUnit = '';

            // 从spu列表中获取当前sku的单位
            if (marketing.getIn(['goodsList', 'goodses'])) {
              marketing.getIn(['goodsList', 'goodses']).forEach((goods) => {
                if (goodsInfo.get('goodsId') === goods.get('goodsId')) {
                  goodsUnit = goods.get('goodsUnit') || '';
                }
              });
            }

            if (!giftGoodsInfos.get(goodsInfo.get('goodsInfoId'))) {
              giftGoodsInfos = giftGoodsInfos.set(
                goodsInfo.get('goodsInfoId'),
                goodsInfo.set('goodsUnit', goodsUnit)
              );
            }
          });
        }
      });
    });

    // sku营销
    let skuMarketingDict = fromJS({});

    goodsMarketingMap.forEach((marketings, key) => {
      skuMarketingDict = skuMarketingDict.set(key, fromJS([]));

      marketings.forEach((marketing, marketingIndex) => {
        let title = '';

        let checked = goodsMarketings.some(
          (goodsMarketing) =>
            goodsMarketing.get('goodsInfoId') === key &&
            goodsMarketing.get('marketingId') === marketing.get('marketingId')
        );

        // 满赠
        if (marketing.get('marketingType') === 2) {
          marketing.get('fullGiftLevelList').forEach((level, index) => {
            if (index === 0) {
              title += '满';
            } else {
              title += ',';
            }

            // 满金额
            if (marketing.get('subType') === 4) {
              title += level.get('fullAmount');
            } else if (marketing.get('subType') === 5) {
              // 满数量
              title += level.get('fullCount') + '件';
            }
          });

          title += '获赠品，赠完为止';
        } else if (marketing.get('marketingType') === 0) {
          marketing.get('fullReductionLevelList').forEach((level, index) => {
            if (index === 0) {
              title += '满';
            } else {
              title += ',';
            }

            // 满金额
            if (marketing.get('subType') === 0) {
              title += level.get('fullAmount');
            } else if (marketing.get('subType') === 1) {
              // 满数量
              title += level.get('fullCount') + '件';
            }

            title += '减' + level.get('reduction');
          });
        } else if (marketing.get('marketingType') === 1) {
          marketing.get('fullDiscountLevelList').forEach((level, index) => {
            if (index === 0) {
              title += '满';
            } else {
              title += ',';
            }

            // 满金额
            if (marketing.get('subType') === 2) {
              title += level.get('fullAmount');
            } else if (marketing.get('subType') === 3) {
              // 满数量
              title += level.get('fullCount') + '件';
            }

            title += '享' + _.mul(level.get('discount'), 10) + '折';
          });
        }

        skuMarketingDict = skuMarketingDict.set(
          key,
          skuMarketingDict
            .get(key)
            .push(marketing.set('checked', checked).set('alllevelDesc', title))
        );
      });
    });

    this.transaction(() => {
      // if (loadGoodses) {
      this.dispatch('purchase:skus', purInfo.goodsInfos);
      this.dispatch('purchase:showDistributeCommission', purInfo.selfBuying);
      this.dispatch(
        'purchase:distributeCommission',
        purInfo.distributeCommission
      );
      this.dispatch('purchaseOrder: spus', purInfo.goodses);
      this.dispatch('purchase: intervalPrice', purInfo.goodsIntervalPrices);
      this.dispatch('purchaseOrder: storeInfo', purInfo.companyInfos);
      this.dispatch('purchase: loading', false);
      this.dispatch('purchase:stores', purInfo.stores);
      // }
      //店铺是否有优惠券活动
      this.dispatch('purchase:storeCouponMap', purInfo.storeCouponMap);
      this.dispatch('purchase:storeMarketing', storeMarketingMap);
      this.dispatch('purchase:goodsMarketingMap', goodsMarketingMap);
      this.dispatch('purchase:giftGoodsInfos', giftGoodsInfos);
      this.dispatch('purchase:skuMarketingDict', skuMarketingDict);
      this.dispatch('purchaseOrder: setBottomPrice', {
        totalPrice: purInfo.totalPrice,
        tradePrice: purInfo.tradePrice,
        discountPrice: purInfo.discountPrice
      });
    });
  };

  /**
   * 合并登录前,登陆后的购物车信息
   */
  mergePurchaseAndInit = () => {
    // 初始化购物车信息
    this.init().then(() => {
      // 校验已勾选的sku是否满足购买数量
      this.state()
        .get('skus')
        .filter((sku) => sku.get('checked'))
        .forEach((sku) => this.validSku(sku.get('goodsInfoId')));
    });
  };

  /**
   * 展示编辑状态
   */
  showEdit = (status: boolean) => {
    // 若从编辑状态 -> 非编辑状态,需要将非正常状态的sku的选中状态去除
    if (!status) {
      const skuIds = this.state()
        .get('skus')
        .filter((sku) => sku.get('goodsStatus') !== 0)
        .map((sku) => sku.get('goodsInfoId'));
      this.dispatch('purchase:checkSpu', { skuIds, checked: true });
    }
    this.dispatch('purchase:edit', status);
    this.init();
  };

  /**
   * 选中sku
   * @param skuId
   */
  checkSku = (skuId: string) => {
    this.transaction(() => {
      const edit = this.state().get('edit');
      this.validSku(skuId);
      this.dispatch('purchase:check', skuId);
    });
  };

  /**
   * 选中spu
   * @param spuId
   */
  checkSpu = (spuId: string, checked: boolean) => {
    let skuIs = this.state()
      .get('spus')
      .find((spu) => spu.get('goodsId') === spuId)
      .get('goodsInfoIds');
    if (!checked) {
      skuIs.forEach((skuId) => this.validSku(skuId));
    }

    let validSkus = this.state().get('skus');
    if (this.state().get('edit')) {
      // 编辑状态下,找出非失效的skuList
      validSkus = validSkus.filter((sku) => sku.get('goodsStatus') !== 2);
    } else {
      // 非编辑状态下,找出有效有库存的skuList(非失效,非0库存的)
      validSkus = validSkus.filter((sku) => sku.get('goodsStatus') === 0);
    }
    // 只处理包含在validSkus中的skuId
    skuIs = skuIs.filter((id) =>
      validSkus.some((s) => s.get('goodsInfoId') == id)
    );
    this.dispatch('purchase:checkSpu', { skuIds: skuIs, checked: checked });
  };

  /**
   * 选中店铺
   * @param {string} storeId
   * @param {boolean} checked
   */
  checkStore = (storeId: string, checked: boolean) => {
    const spuIds = this.state()
      .get('stores')
      .find((store) => store.get('storeId') === storeId)
      .get('goodsIds');
    spuIds.forEach((spuId) => this.checkSpu(spuId, checked));

    this.init();
  };

  /**
   * 选中所有
   */
  checkAll = (checked: boolean) => {
    this.transaction(() => {
      this.dispatch('purchase:checkAll', {
        checked,
        edit: this.state().get('edit')
      });
      if (!checked) {
        const skus = this.state().get('skus');
        skus.map((sku) => {
          this.validSku(sku.get('goodsInfoId'));
        });
      }

      this.init();
    });
  };

  /**
   * 修改商品数量
   * @param skuNum
   * @param skuId
   * @returns {Promise<void>}
   */
  changeSkuNum = async (skuNum: number, skuId: string, stock: number) => {
    if (!skuNum) {
      return;
    }

    this.transaction(() => {
      this.dispatch('purchase:skuNum', { skuId: skuId, number: skuNum });
    });

    if (WMkit.isLoginOrNotOpen()) {
      const { code, message } = await webapi.changeSkuNum(skuNum, skuId);
      if (code != config.SUCCESS_CODE) {
        Alert({ text: message });
        return;
      }
    } else {
      if (!putPurchase(skuId, skuNum)) {
        Alert({ text: '操作失败' });
        return;
      }
    }

    this.validSku(skuId);
    // //先校验库存
    // if(skuNum>stock){
    //   this.dispatch('purchase:skuError', { skuId: skuId, error:`库存${stock}`})
    // }else{
    //   //校验起订量等
    //   this.validSku(skuId)
    // }

    // 重新获取营销信息
    this.init();
  };

  /**
   * 清除失效商品
   * @returns {Promise<void>}
   */
  cleanInvalidGoods = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      const { message, code } = await webapi.cleanInvalidGoods();
      if (code !== config.SUCCESS_CODE) {
        Alert({ text: message });
        return;
      }
    } else {
      const purArr = JSON.parse(
        localStorage.getItem(WMkit.purchaseCache())
      ) as Array<any>;
      if (purArr) {
        const skuIds = purArr
          .filter((sku) => sku.invalid)
          .map((sku) => sku.goodsInfoId);
        if (!delPurchase(skuIds)) {
          Alert({ text: '操作失败' });
          return;
        }
      }
    }

    Alert({ text: '清空失效商品成功' });
    //更新购物车全局数量
    msg.emit('purchaseNum');
    this.init();
  };

  /**
   * 删除购物车商品
   */
  deletePurchase = async () => {
    //获取所有选中的商品
    const skuIds = this.state()
      .get('skus')
      .filter((sku) => sku.get('checked') && sku.get('goodsStatus') !== 2)
      .map((sku) => sku.get('goodsInfoId'));
    if (skuIds.count() == 0) {
      Alert({ text: '请选择要删除的商品' });
      return;
    }

    if (WMkit.isLoginOrNotOpen()) {
      const { code } = await webapi.deletePurchaseCount(skuIds.toJS());
      if (code !== config.SUCCESS_CODE) {
        Alert({ text: '删除失败，请重试！' });
        return;
      }
    } else {
      if (!delPurchase(skuIds.toJS())) {
        Alert({ text: '删除失败，请重试！' });
        return;
      }
    }

    Alert({ text: '删除成功！' });
    this.showEdit(false);
    this.init();
    //更新购物车全局数量
    msg.emit('purchaseNum');
  };

  /**
   * 移入购物车
   * @returns {Promise<void>}
   */
  addToFollow = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      //获取所有选中的商品
      const skuIds = this.state()
        .get('skus')
        .filter((sku) => sku.get('checked') && sku.get('goodsStatus') !== 2)
        .map((sku) => sku.get('goodsInfoId'));
      if (skuIds.count() == 0) {
        Alert({ text: '请选择要移入收藏夹的商品' });
        return;
      }

      const { code, message } = await webapi.addToFollow(skuIds.toJS());
      if (code === config.SUCCESS_CODE) {
        Alert({ text: '商品已移入收藏，请在我的收藏中查看' });

        this.init();
        //更新购物车全局数量
        msg.emit('purchaseNum');
      } else {
        Alert({ text: message });
      }
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: this.mergePurchaseAndInit
      });
    }
  };

  /**
   * 去下单
   * @returns {Promise<void>}
   */
  toConfirm = async () => {
    if (WMkit.isLoginOrNotOpen()) {
      const skus = this.state()
        .get('skus')
        .filter((sku) => {
          //缺货状态显示
          return sku.get('goodsStatus') === 0 && sku.get('checked');
        });
      let skuList = new Array();
      skus.forEach((sku) => {
        skuList.push({
          skuId: sku.get('goodsInfoId'),
          num: sku.get('buyCount')
        });
      });

      let tradeMarketingList = new Array();

      let storeMarketing = this.state().get('storeMarketing');
      skuList.forEach((sku) => {
        let goodsInfoId = sku.skuId;

        // 商品参与的营销列表
        let goodsMarketings = this.state()
          .get('goodsMarketingMap')
          .get(goodsInfoId);
        let skuMarketingDict = this.state().get('skuMarketingDict');

        if (goodsMarketings && goodsMarketings.count()) {
          // 商品当前使用的营销
          let marketing = skuMarketingDict
            .get(goodsInfoId)
            .filter((goodsMarketing) => goodsMarketing.get('checked'))
            .first();

          // 营销请求参数列表，一个营销一条数据
          let tradeMarketings = tradeMarketingList.filter(
            (tradeMarketing) =>
              tradeMarketing['marketingId'] === marketing.get('marketingId')
          );

          if (tradeMarketings.length > 0) {
            tradeMarketings[0]['skuIds'].push(goodsInfoId);
          } else {
            let marketingLevelId = '';
            let giftSkuIds = new Array();

            // 满赠营销
            if (marketing.get('marketingType') === 2) {
              // 获取领取赠品的等级及领取的赠品
              this.state()
                .get('selectedMarketingGifts')
                .forEach((selectedMarketingGift) => {
                  if (
                    selectedMarketingGift.get('marketingId') ===
                    marketing.get('marketingId')
                  ) {
                    marketingLevelId = selectedMarketingGift.get('giftLevelId');

                    giftSkuIds.push(selectedMarketingGift.get('goodsInfoId'));
                  }
                });
            } else if (marketing.get('marketingType') === 0) {
              // 满减
              storeMarketing
                .get(marketing.get('storeId').toString())
                .forEach((v) => {
                  // 满足满减营销
                  if (
                    v.get('marketingId') === marketing.get('marketingId') &&
                    v.get('lack') === 0
                  ) {
                    marketingLevelId = v.getIn([
                      'fullReductionLevel',
                      'reductionLevelId'
                    ]);
                  }
                });
            } else if (marketing.get('marketingType') === 1) {
              // 满折
              storeMarketing
                .get(marketing.get('storeId').toString())
                .forEach((v) => {
                  // 满足满折营销
                  if (
                    v.get('marketingId') === marketing.get('marketingId') &&
                    v.get('lack') === 0
                  ) {
                    marketingLevelId = v.getIn([
                      'fullDiscountLevel',
                      'discountLevelId'
                    ]);
                  }
                });
            }

            if (marketingLevelId !== '') {
              let tradeMarketing = {
                marketingId: marketing.get('marketingId'),
                marketingLevelId: marketingLevelId,
                skuIds: [goodsInfoId],
                giftSkuIds: giftSkuIds
              };

              tradeMarketingList.push(tradeMarketing);
            }
          }
        }
      });

      this.confirm(skus, skuList, tradeMarketingList, false);
    } else {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => this.mergePurchaseAndInit()
      });
    }
  };

  confirm = async (skus, skuList, tradeMarketingList, forceConfirm) => {
    const { code, message } = await webapi.toConfirm(
      skuList,
      tradeMarketingList,
      forceConfirm
    );

    if (code === config.SUCCESS_CODE) {
      history.push('/order-confirm');
      (window as any).y = '';
    } else {
      if (code === 'K-999999') {
        Confirm({
          title: '优惠失效提醒',
          text: message,
          okBtn: '继续下单',
          cancelBtn: '重新下单',
          confirmCb: async () => {
            await this.confirm(skus, skuList, tradeMarketingList, true);
          },
          cancel: async () => {
            await this.init();
          }
        });
      } else {
        Confirm({
          text: message,
          okBtn: '确定',
          confirmCb: async () => {
            await this.init();
          }
        });
      }
    }
  };

  /**
   * 校验sku数量 规则如下, 少于限订 更新限订 大于起订跟库存，显示起订跟库存
   * @param skuNum
   * @param skuId
   * @returns {string}
   */
  validSku = (skuId: string) => {
    let msgArray = List.of();
    const sku = this.state()
      .get('skus')
      .find((sku) => sku.get('goodsInfoId') === skuId);
    const skuNum = sku.get('buyCount');
    const min = sku.get('count');
    const max = sku.get('maxCount');
    const stock = sku.get('stock');

    //没有库存，商品失效都不做check
    if (sku.get('goodsStatus') !== 0) {
      return;
    }

    if (min && skuNum < min) {
      msgArray = msgArray.push(`${min}起订`);
    }

    //当大于起订跟库存 去起订跟库存最小值
    if ((max && skuNum > max) || skuNum > stock) {
      const maxNum = Math.min(max, stock);
      if (maxNum == max) {
        msgArray = msgArray.push(`限订${maxNum}`);
      } else {
        msgArray = msgArray.push(`库存${stock}`);
      }
    }

    this.dispatch('purchase:setMessage', {
      skuId: skuId,
      message: msgArray.count() > 0 ? msgArray.join('|') : ''
    });
  };

  /**
   * 删除赠品
   */
  deleteGift = (gift) => {
    this.dispatch('purchase:deleteGift', gift);
  };

  /**
   * 打开弹窗
   */
  open = (param) => {
    this.dispatch('purchase:popover:open', param);
  };

  /**
   * 关闭弹窗
   */
  close = () => {
    this.dispatch('purchase:popover:close');
  };

  /**
   * 领取赠品
   */
  chooseGift = (selectedMarketingGifts) => {
    this.transaction(() => {
      this.dispatch('purchase:selectedMarketingGifts', selectedMarketingGifts);
      this.close();
    });
  };

  /**
   * 用户在购物车中选择sku准备参加的营销活动
   * @param goodsInfoId skuId
   * @param marketingId 营销活动id
   */
  chooseSkuMarketing = async (goodsInfoId, marketingId) => {
    if (WMkit.isLoginOrNotOpen()) {
      const { code, message } = await webapi.modifyGoodsMarketing(
        goodsInfoId,
        marketingId
      );
      if (code == config.SUCCESS_CODE) {
        this.init();
      } else {
        Alert({ text: message });
      }
    } else {
      // 未登录时,在前端存储,用户针对sku选择的营销活动信息
      if (putSkuMarketingCache(goodsInfoId, marketingId)) {
        this.init();
      } else {
        Alert({ text: '操作失败' });
      }
    }
  };

  /**
   * 领券弹框的显示或隐藏
   * @param state
   */
  changeCouponMask = async (storeSpus) => {
    let code = '';
    if (!this.state().get('showCouponMask')) {
      code = (await this.setGoodInfoIds(storeSpus)) as any;
    }
    if (this.state().get('showCouponMask') || code === config.SUCCESS_CODE) {
      this.dispatch('change:changeCouponMask');
    }
  };

  /**
   * 设置查询优惠券的参数
   * @param storeSpus
   */
  setGoodInfoIds = async (storeSpus) => {
    const skus = this.state().get('skus');
    let goodsInfoIds = skus
      .filter(
        (sku) =>
          sku.get('goodsStatus') !== 2 && storeSpus.includes(sku.get('goodsId'))
      )
      .map((sku) => sku.get('goodsInfoId'));

    this.dispatch('purchase: coupon: goodInfoIds', goodsInfoIds);
    let code = await this.showCouponList();
    return new Promise((resolve) => {
      resolve(code);
    });
  };

  /**
   * 查询购物车中各店铺所选商品可领取的优惠券
   * @param storeId
   * @returns {Promise<void>}
   */
  showCouponList = async () => {
    let goodsInfoIds = this.state().get('goodsInfoIds');

    const { context, code, message } = await webapi.fetchCouponForGoodsList(
      goodsInfoIds
    );
    if (code == config.SUCCESS_CODE) {
      this.dispatch('purchase: coupon: list', context);
    } else {
      Alert({
        text: message
      });
    }
    return new Promise((resolve) => {
      resolve(code);
    });
  };

  /**
   * 领取优惠券
   * @param couponId
   * @param activityId
   * @returns {Promise<void>}
   */
  receiveCoupon = async (couponId, activityId) => {
    if (window.token) {
      const { code, message } = await webapi.receiveCoupon(
        couponId,
        activityId
      );
      if (code !== config.SUCCESS_CODE) {
        Alert({
          text: message
        });
      }
      this.showCouponList();
    } else {
      //未登录，先关闭优惠券弹框，再调出登录弹框
      this.dispatch('change:changeCouponMask');
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          this.dispatch('purchase: loading', true);
          this.init();
        }
      });
    }
  };
  // 通告栏
  initAdver = async () => {
    const { context } = await webapi.getAdver();
    this.dispatch(
      'imgShow:imgShow',
      (context as any).cartSwitchFlag
    );
    this.dispatch(
      'storeinfo:cartContent',
      (context as any).cartContent
    );
  };

  cartContentRun = () => {
    let intervalcode = this.state().get('intervalcode');
    intervalcode = setInterval(() => {
      
      let cartContent = this.state().get('cartContent');
      let end = this.state().get('cartContentEnd');
      if(cartContent.length == 0){
        this.dispatch(
          'storeinfo:cartContent',
          end
        );
        this.dispatch(
          'storeinfo:cartcontentend',
         ""
        );
      }else{
      this.dispatch(
        'storeinfo:cartcontentend',
        end+cartContent.substring(0, 1)
      );
      this.dispatch(
        'storeinfo:cartContent',
        cartContent.substring(1, cartContent.length)
      );}
    },300);
    this.dispatch('storeinfo:intervalcode', intervalcode);
  }

  cartContentStop = () => {
    let intervalcode = this.state().get('intervalcode');
    clearInterval(intervalcode);
  }
}
