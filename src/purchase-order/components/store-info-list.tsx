import React from 'react';
import { IMap, Relax, msg } from 'plume2';
import { List } from 'immutable';
import '../css/style.css'

import { Check, noop, history, CouponLabel, WMkit } from 'wmkit';
import { IList } from 'typings/globalType';

import SpuItemList from './spu-item-list';
import { checkAllQL } from '../ql';
import InvalidSkuList from './invaild-sku-list';
import HeadPromotion from './head-promotion';
import GiftItemList from './gift-item-list';

const styles = require('../css/style.css');

@Relax
export default class StoreInfoList extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      spus: List<any>;
      skus: List<any>;
      checkAllQL: boolean;
      checkAll: Function;
      cleanInvalidGoods: Function;
      stores: List<any>;
      checkStore: Function;
      edit: boolean;
      storeMarketing: IMap;
      changeCouponMask: Function;
      init: Function;
      storeCouponMap: IList;
      showCouponMask: boolean;
    };
  };

  static relaxProps = {
    spus: 'spus',
    skus: 'skus',
    checkAllQL: checkAllQL,
    checkAll: noop,
    cleanInvalidGoods: noop,
    stores: 'stores',
    checkStore: noop,
    edit: 'edit',
    storeMarketing: 'storeMarketing',
    changeCouponMask: noop,
    init: noop,
    storeCouponMap: 'storeCouponMap',
    showCouponMask: 'showCouponMask'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      checkAllQL,
      checkAll,
      cleanInvalidGoods,
      skus,
      stores,
      checkStore,
      edit,
      storeMarketing,
      changeCouponMask,
      storeCouponMap,
      showCouponMask
    } = this.props.relaxProps;

    const cleanStatus =
      skus
        .filter((sku) => sku.get('delFlag') === 1 || sku.get('addedFlag') === 0)
        .count() > 0;

    return (
      <div
        className="purchase-container address-box list-mt"
        style={
          showCouponMask
            ? { overflowY: 'hidden', height: window.innerHeight - 100 }
            : { marginBottom:'2.1rem'}
        }
      >
        {stores &&
          stores
            .map((store) => {
              //店铺的优惠券活动
              let storeCouponFlag =
                storeCouponMap && storeCouponMap.toJS()[store.get('storeId')];
              //获取店铺的spu
              const storeSpus = store.get('goodsIds');

              // 店铺商品是否需要展示
              const showStoreGoodsFlag = skus.toSeq().some((sku) => {
                return (
                  sku.get('goodsStatus') !== 2 &&
                  storeSpus.includes(sku.get('goodsId'))
                );
              });

              // 判断店铺的商品是否都被选中了
              const storeInfoCheckList = skus.toSeq().filter((sku) => {
                return (
                  (sku.get('goodsStatus') === 0 ||
                    (edit && sku.get('goodsStatus') !== 2)) &&
                  storeSpus.includes(sku.get('goodsId'))
                );
              });
              const storeChecked =
                storeInfoCheckList.count() > 0 &&
                storeInfoCheckList.every((sku) => sku.get('checked'));
              //分销-店铺精选
              const isShop = WMkit.isShop();
              return (
                <div
                  className="storeItems"
                  key={store.get('storeId')}
                  style={{marginBottom:'0',borderRadius:'0.2rem',boxShadow:'0 0.06rem 0.2rem 0 rgba(0, 0, 0, 0.04)',paddingBottom:'0.1rem',background:'#fff'}}
                >
                  {/*店铺商品数量为0，则不展示*/}
                  {showStoreGoodsFlag &&
                    !isShop && (
                      <div className="store-info" style={{height:'0.97rem',borderRadius:'0.2rem 0.2rem 0 0'}}>
                        <Check
                          checked={storeChecked}
                          onCheck={() =>
                            checkStore(store.get('storeId'), storeChecked)
                          }
                          checkStyle={{width:'0.38rem',height:'0.38rem'}}
                        />
                        <div className="store-name-sales">
                          <span
                            onClick={() =>
                              history.push(
                                `/store-main/${store.get('storeId')}`
                              )
                            }
                          >
                            {store.get('storeName')}
                          </span>
                          {/*店铺是否自营*/}
                          {store.get('companyType') === 0 && (
                            // <div className="self-sales" style={{marginLeft:'0.2rem',borderRadius:'0.04rem'}}>自营</div>
                            <img src={require('../img/self-support.png')} style={{marginLeft:'0.2rem',width:'0.6rem'}} alt=""/>
                          )}
                        </div>
                        {/* {!edit &&
                          storeCouponFlag && (  */}
                            <div onClick={() => changeCouponMask(storeSpus)}>
                              {/* <CouponLabel text="优惠券" /> */}
                              <img src={require('../img/quan.png')} alt="" style={{width:".55rem",height:".36rem"}}></img>
                            </div>
                          {/* )} */}
                      </div>
                    )}
                  {storeMarketing.get(store.get('storeId').toString()) ? (
                    <HeadPromotion
                      marketings={storeMarketing.get(
                        store.get('storeId').toString()
                      )}
                    />
                  ) : null}
                  <div>
                    {/*spu维度展示商品*/}
                    <SpuItemList storeId={store.get('storeId')} />
                    {/*展示赠品商品*/}
                    <GiftItemList storeId={store.get('storeId')} />
                  </div>
                </div>
              );
            })
            .toArray()}
        <InvalidSkuList />
      </div>
    );
  }
}
