import React from 'react';
import { Relax } from 'plume2';
import { history, noop } from 'wmkit';

const storeCoupon=require('../img/store-coupon.png')
const commonCoupon=require('../img/common-coupon.png')

const COUPON_TYPE = {
  0: '通用券',
  1: '店铺券',
  2: '运费券'
};

export default class CouponItem extends React.Component<any, any> {
  render() {
    const { coupon, storeName } = this.props;
    return (
      <div className={`item ${this._buildCouponStyle(coupon)}`}>
        <div className="left">
          <p className="money">
            ￥
            <span className="money-text">{coupon.get('denomination')}</span>
          </p>
          <p className="left-tips">
            {coupon.get('fullBuyType') === 0
              ? '无门槛'
              : `满${coupon.get('fullBuyPrice')}可用`}
          </p>
          {coupon.get('couponWillEnd') && (
            <img src={require('../img/expiring.png')} alt="" />
          )}
        </div>
        <div
          className={`right ${!coupon.get('hasFetched') &&
            coupon.get('leftFlag') &&
            'now-get'}`}
        >
          <div className="right-info">
            {this._storeNameBox(coupon, storeName)}
            <div className="bottom-box">
              {coupon.get('rangeDayType') === 1
                ? `领取后${coupon.get('effectiveDays')}天内有效`
                : `${coupon.get('couponStartTime')}至${coupon.get(
                    'couponEndTime'
                  )}`}
            </div>
          </div>
          {(coupon.get('hasFetched') || coupon.get('leftFlag')) && (
            <div className="operat-box colum-center">
              {coupon.get('hasFetched') && (
                <img src={require('../img/get-coupon.png')} alt="" />
              )}
              <a
                href="javascript:;"
                style={{color:'#ff4d4d',border:'1px solid #ff4d4d'}}
                onClick={() => this._handleClick(coupon)}
                className={
                  !coupon.get('hasFetched') && coupon.get('leftFlag')
                    ? 'get-now'
                    : ''
                }
              >
                {this._txtBox(coupon)}
              </a>
            </div>
          )}
          {!coupon.get('hasFetched') &&
            !coupon.get('leftFlag') && (
              <div className="operat-box colum-center">
                <img src={require('../img/empty-coupon.png')} alt="" />
                {/* <div className="gray-state">已抢光</div> */}
              </div>
            )}
        </div>
      </div>
    );
  }

  /**
   * 平台/店铺名称展示
   *
   * @memberof CouponItem
   */
  _storeNameBox = (coupon, storeName) => {
    return (
      <div className="right-top">
        { coupon.get('couponType')==0?(<img style={{width:'.8rem',height:'.3rem',margin:'0.1rem 0.1rem 0.1rem 0'}} src={commonCoupon}/>):
                   coupon.get('couponType')==1?(<img style={{width:'.8rem',height:'.3rem',margin:'0.1rem 0.1rem 0.1rem 0'}} src={storeCoupon}/>)
                   :(<img style={{width:'.8rem',height:'.3rem',margin:'0.1rem 0.1rem 0.1rem 0'}} src={storeCoupon}/>)}
        <p>
          <span>
            {coupon.get('platformFlag') === 1 ? '全平台可用' : storeName}
          </span>
        </p>
      </div>
    );
  };

  /**
   * 按钮文本展示
   */
  _txtBox = (coupon) => {
    if (!coupon.get('hasFetched') && coupon.get('leftFlag')) {
      return '立即领取';
    }
    if (coupon.get('hasFetched')) {
      return !coupon.get('couponStarted') ? '查看可用商品' : '立即使用';
    }
  };

  /**
   * 点击事件
   */
  _handleClick = (coupon) => {
    const { receiveCoupon } = this.props;
    if (coupon.get('hasFetched')) {
      history.push({
        pathname: '/coupon-promotion',
        state: {
          couponId: coupon.get('couponId'),
          activityId: coupon.get('activityId')
        }
      });
    } else {
      receiveCoupon(coupon.get('couponId'), coupon.get('activityId'));
    }
  };

  /**
   * 构建优惠券样式
   *
   * @memberof CouponItem
   */
  _buildCouponStyle = (coupon) => {
    let style = '';
    if (coupon.get('couponType') === 0) {
      style = '';
    } else if (coupon.get('couponType') === 1) {
      style = 'store-item';
    } else if (coupon.get('couponType') === 2) {
      style = 'freight-item';
    }
    return `${style}`;
  };
}
