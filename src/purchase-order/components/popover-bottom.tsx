import React, { Component } from 'react';
import { fromJS, Map, List } from 'immutable';
import { Check, Alert, WMImage } from 'wmkit';
import { IMap, Relax } from 'plume2';
import noop from 'wmkit/noop';
import { IList } from 'typings/globalType';

// 定义是否有底部确定按钮，这边牵扯到底部商品的滚动距离
let hasBottom = true;

/**
 * 通用促销底部弹出
 */
@Relax
export default class PopoverBottom extends Component<any, any> {
  props: {
    relaxProps?: {
      popoverParam: IMap;
      close: Function;
      chooseGift: Function;
      chooseSkuMarketing: Function;
      skuMarketingDict: IMap;
      goodsMarketingMap: IMap;
      giftGoodsInfos: IList;
    };
  };

  static relaxProps = {
    popoverParam: 'popoverParam',
    close: noop,
    chooseGift: noop,
    chooseSkuMarketing: noop,
    skuMarketingDict: 'skuMarketingDict',
    goodsMarketingMap: 'goodsMarketingMap',
    giftGoodsInfos: 'giftGoodsInfos'
  };

  constructor(props) {
    super(props);

    const popoverParam = this.props.relaxProps.popoverParam;

    const selectedMarketingGifts =
      popoverParam.get('selectedMarketingGifts') || List();

    this.state = {
      selectedMarketingGifts: selectedMarketingGifts
    };
  }

  render() {
    const {
      close,
      chooseGift,
      popoverParam,
      chooseSkuMarketing,
      skuMarketingDict,
      goodsMarketingMap,
      giftGoodsInfos
    } = this.props.relaxProps;

    hasBottom = popoverParam.get('hasBottom');
    const goodsInfoId = popoverParam.get('goodsInfoId');
    const skuMarketings = skuMarketingDict.get(goodsInfoId) || List();
    let marketing = popoverParam.get('marketing') || Map();
    const maxValidLevel = marketing.get('fullGiftLevel') || Map();

    let title = '';

    let operateType = popoverParam.get('type');

    if (operateType === 'showGift') {
      title = '查看赠品';
    } else if (operateType === 'chooseGift') {
      title = '领取赠品';
    }
    if (operateType === 'chooseGoodsMarketing') {
      title = '修改促销';

      let marketings = goodsMarketingMap.get(goodsInfoId) || List();

      marketings.forEach((value) => {
        if (value.get('marketingType') === 2) {
          marketing = value;
        }
      });
    }

    const hasFullGift = marketing.get('fullGiftLevelList');

    const count = this.state.selectedMarketingGifts
      .filter(
        (gift) => gift.get('marketingId') === marketing.get('marketingId')
      )
      .count();

    return (
      <div>
        <div
          style={{
            position: 'fixed',
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,.5)',
            zIndex: 9999
          }}
        />
        <div className="panel-bottom" style={title=='查看赠品'?{borderRadius:'0.2rem 0.2rem 0 0'}:null}>
          <div className="panel-title b-1px-b">
            <h1 style={{color:'#333'}}>{title}</h1>
            {/* <i className="iconfont icon-guanbi" onClick={() => close()} /> */}
            <img src={require('../img/close.png')} onClick={() => close()} style={{width:'0.4rem',height:'0.4rem'}} />
          </div>
          {operateType === 'chooseGoodsMarketing' ? (
            <div className="panel-info b-1px-b" style={{background:'#FFF5F5'}}>
              {/* <i className="iconfont icon-jinggao" /> */}
              <img src={require('../img/notice.png')} style={{width:'0.3rem',height:'0.3rem'}}/>
              <p style={{color:'#333',fontSize:'0.26rem'}}>满折/满减/满赠仅可任选其一，可在购物车修改</p>
            </div>
          ) : null}
          {operateType === 'chooseGoodsMarketing' &&
            skuMarketings
              .map((skuMarketing, index) => {
                let marketingId = skuMarketing.get('marketingId');
                let type = skuMarketing.get('marketingType');
                let checked = skuMarketing.get('checked');
                let typeTag = '';

                if (type === 0) {
                  typeTag = '减';
                } else if (type === 1) {
                  typeTag = '折';
                } else if (type === 2) {
                  typeTag = '赠';
                }

                return (
                  <div className="panel-promotion b-1px-b">
                    <div className="promotion-info">
                      <Check
                        checked={checked}
                        onCheck={() => {
                          chooseSkuMarketing(goodsInfoId, marketingId);
                        }}
                      />
                      <div className="tag">{typeTag}</div>
                      <span>{skuMarketing.get('alllevelDesc')}</span>
                    </div>
                  </div>
                );
              })
              .toArray()}

          {hasFullGift ? (
            <div
              className="panel-body"
              style={{ paddingBottom: hasBottom ? 48 : 0 }}
            >
              {marketing
                .get('fullGiftLevelList')
                .map((level, index) => {
                  let rule =
                    marketing.get('subType') === 4
                      ? level.get('fullAmount')
                      : level.get('fullCount') + '件';

                  let notEnough =
                    marketing.get('subType') === 4
                      ? maxValidLevel.get('fullAmount') <
                        level.get('fullAmount')
                      : maxValidLevel.get('fullCount') < level.get('fullCount');

                  return (
                    <div>
                      <div className="gift b-1px-b">
                        <span className="gift-title" style={{fontSize:'0.26rem',color:'#333'}}>
                          满<span style={{color:'#FF4D4D'}}>{rule}</span>可获以下赠品，可选{level.get('giftType') === 0
                            ? '全部'
                            : '一种'}
                        </span>
                        {level
                          .get('fullGiftDetailList')
                          .map((detail, detailIndex) => {
                            let goodsInfo = giftGoodsInfos.get(
                              detail.get('productId')
                            );

                            if (goodsInfo) {
                              let invalid = goodsInfo.get('goodsStatus') == 2;
                              const stock = goodsInfo.get('stock');
                              // 起订量
                              const count = goodsInfo.get('count') || 0;

                              // 库存等于0
                              const noneStock = stock <= 0;

                              const selectedGift = this.state.selectedMarketingGifts
                                .filter(
                                  (gift) =>
                                    gift.get('marketingId') ===
                                      level.get('marketingId') &&
                                    gift.get('giftLevelId') ===
                                      level.get('giftLevelId') &&
                                    gift.get('goodsInfoId') ===
                                      detail.get('productId')
                                )
                                .first();

                              const checked =
                                selectedGift && selectedGift.get('checked');

                              const disabled =
                                notEnough || invalid || noneStock;

                              return (
                                <div key={detailIndex} className="goods-list">
                                  {operateType === 'chooseGift' ? (
                                    <Check
                                      checked={checked}
                                      disable={disabled}
                                      onCheck={() => {
                                        this.chooseGift(
                                          !checked,
                                          level.get('marketingId'),
                                          level.get('giftLevelId'),
                                          detail.get('productId'),
                                          level.get('giftType'),
                                          marketing.get('storeId'),
                                          detail.get('productNum')
                                        );
                                      }}
                                    />
                                  ) : null}
                                  <div
                                    className="img-box"
                                    onClick={() =>
                                      !invalid
                                        ? (window.location.href = `/goods-detail/${goodsInfo.get(
                                            'goodsInfoId'
                                          )}`)
                                        : null
                                    }
                                    style={{width:'2.2rem',height:'2.2rem',objectFit:'scale-down'}}
                                  >
                                    <WMImage
                                      className="img-box"
                                      src={goodsInfo.get('goodsInfoImg')}
                                      alt=""
                                      // width="100%"
                                      height="80%"
                                      style={{objectFit:'cover'}}
                                    />
                                  </div>

                                  <div
                                    className="detail"
                                    onClick={() =>
                                      !invalid
                                        ? (window.location.href = `/goods-detail/${goodsInfo.get(
                                            'goodsInfoId'
                                          )}`)
                                        : null
                                    }
                                    style={{height:'2.2rem',marginLeft:'0.3rem',padding: '0.16rem'}}
                                  >
                                    <a className="title" style={{fontWeight:'bold',color:'#333',fontSize:'0.26rem'}}>
                                      {goodsInfo.get('goodsInfoName')}
                                    </a>
                                    {/* <p className="gec">
                                      {goodsInfo.get('specText')}
                                    </p> */}
                                    <div className="bottom">
                                      <div className="bottom-state">
                                        <span className="price" style={{fontSize:'0.4rem',color:'#FF4D4D'}}>
                                          <i className="iconfont icon-qian" style={{fontSize:'0.4rem'}} />
                                          {goodsInfo.get('marketPrice')}
                                        </span>
                                        {noneStock &&
                                          !invalid && (
                                            <div className="out-stock">
                                              缺货
                                            </div>
                                          )}
                                        {invalid && (
                                          <div className="out-stock">失效</div>
                                        )}
                                      </div>
                                      <span className="num" style={{color:'#999',fontSize:'0.24rem'}}>
                                        ×{detail.get('productNum')}
                                        {goodsInfo.get('goodsUnit')}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              );
                            }
                          })
                          .toArray()}
                      </div>
                    </div>
                  );
                })
                .toArray()}
            </div>
          ) : null}

          {hasBottom && (
            <div className="panel-foot">
              <span>已领{count}种</span>
              <button
                onClick={() => {
                  chooseGift(this.state.selectedMarketingGifts);
                }}
              >
                确定
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }

  /**
   * 领取赠品
   */
  chooseGift = (
    checked,
    marketingId,
    giftLevelId,
    goodsInfoId,
    giftType,
    storeId,
    goodsNum
  ) => {
    const selectedGift = this.state.selectedMarketingGifts
      .filter((gift) => gift.get('marketingId') === marketingId)
      .first();

    // 选取赠品，且为不同等级
    if (
      checked &&
      selectedGift &&
      selectedGift.get('giftLevelId') !== giftLevelId
    ) {
      Alert({ text: '只可参加1个满赠活动哦' });
    } else if (
      checked &&
      selectedGift &&
      giftType === 1 &&
      selectedGift.get('goodsInfoId') !== goodsInfoId
    ) {
      // 赠一个，且已选了一个
      Alert({ text: '只可选择1种赠品哦~' });
    } else {
      // this.dispatch('purchase:selectedMarketingGift', fromJS({checked: checked, marketingId: marketingId, giftLevelId: giftLevelId, goodsInfoId: goodsInfoId, storeId: storeId, goodsNum: goodsNum}))
      let selectedMarketingGift = fromJS({
        checked: checked,
        marketingId: marketingId,
        giftLevelId: giftLevelId,
        goodsInfoId: goodsInfoId,
        storeId: storeId,
        goodsNum: goodsNum
      });

      let index = this.state.selectedMarketingGifts.findIndex((gift) => {
        return (
          gift.get('marketingId') ===
            selectedMarketingGift.get('marketingId') &&
          gift.get('giftLevelId') ===
            selectedMarketingGift.get('giftLevelId') &&
          gift.get('goodsInfoId') === selectedMarketingGift.get('goodsInfoId')
        );
      });

      if (index === -1 || selectedMarketingGift.get('checked')) {
        this.setState({
          selectedMarketingGifts: this.state.selectedMarketingGifts.push(
            selectedMarketingGift
          )
        });
      } else {
        this.setState({
          selectedMarketingGifts: this.state.selectedMarketingGifts.delete(
            index
          )
        });
      }
    }
  };
}
