import React, { Component } from 'react';
import { Relax } from 'plume2';

import { WMImage, history, WMkit } from 'wmkit';

@Relax
export default class CartEmpty extends Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {};
  };

  static relaxProps = {};

  render() {
    return (
      <div className="list-none">
        <WMImage
          src={require('../img/none.png')}
          width="200px"
          height="200px"
        />
        <p>您的购物车是空哒</p>
        <div className="half">
          <button className="btn btn-ghost" onClick={() => this.go()}>
            逛逛商品
          </button>
        </div>
      </div>
    );
  }

  /**
   * 根据分销渠道跳转
   */
  go = () => {
    if (WMkit.isShop()) {
      history.push('/shop-index-c');
    } else {
      history.push('/goodsList');
    }
  };
}
