import React from 'react';
import { Relax } from 'plume2';
import { List } from 'immutable';

import { Check, WMImage, noop } from 'wmkit';
import SkuSimpleList from './sku-simple-list';

@Relax
export default class SpuItem extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      skus: List<any>;
      edit: boolean;
      checkSpu: Function;
      init: Function;
    };
    spu;
  };

  static relaxProps = {
    skus: 'skus',
    edit: 'edit',
    checkSpu: noop,
    init: noop
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { spu } = this.props;
    const { edit, skus, checkSpu, init } = this.props.relaxProps;

    //取出sku
    const skuList = skus.filter(
      (sku) =>
        spu.get('goodsInfoIds').includes(sku.get('goodsInfoId')) &&
        sku.get('goodsStatus') !== 2
    );

    const spuNoStock = skuList.every(
      (sku) => sku.get('goodsStatus') == 1 && !edit
    );
    //spuchecK 值计算
    const spuCheckedList = skuList.filter((sku) => {
      //缺货状态显示
      let noStock = sku.get('goodsStatus') == 1 && !edit;
      return !noStock;
    });

    const spuChecked =
      spuCheckedList.count() > 0 &&
      spuCheckedList.every((sku) => sku.get('checked'));

    return (
      <div className="spu-item">
        <div className={spuNoStock ? `spu-box no-item` : `spu-box`}>
          <Check
            checked={spuChecked}
            onCheck={() => {
              checkSpu(spu.get('goodsId'), spuChecked);

              init();
            }}
            disable={spuNoStock}
            checkStyle={{width:'0.36rem',height:'0.36rem',lineHeight:'0.36rem'}}
          />
          <div className="spu-content">
            <div className="no-img">
              <WMImage
                mode="pad"
                style={styles.img}
                src={spu.get('goodsImg')}
                alt=""
                width="100%"
                height="100%"
              />
              {/*<img src={spu.get('goodsImg')} />*/}
            </div>
            <div className="spu-right">
              <span className={spuNoStock ? `spu-title spu-none` : `spu-title`}>
                {spu.get('goodsName')}
              </span>
              {spuNoStock && (
                <div className="out-stock" style={{ marginLeft: 0 }}>
                  缺货
                </div>
              )}
            </div>
          </div>
        </div>
        <div style={{ marginLeft: 12 }}>
          <SkuSimpleList spu={spu} />
        </div>
      </div>
    );
  }
}

const styles = {
  img: {
    width: '2.2rem',
    height: '2.2rem',
    marginRight: '.36rem'
  }
};
