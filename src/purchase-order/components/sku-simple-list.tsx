import React from 'react';
import { IMap, Relax } from 'plume2';
import { fromJS, List } from 'immutable';
import moment from 'moment';

import { Check, NumberInput, noop, _, history } from 'wmkit';
import { _calculateGoodsPrice } from '../kit';

const styles = require('../css/style.css');

@Relax
export default class SkuSimpleList extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      checkSku: Function;
      changeSkuNum: Function;
      edit: boolean;
      skus: List<any>;
      showDistributeCommission: boolean;
      intervalPrices: List<any>;
      skuMarketingDict: IMap;
      init: Function;
      open: Function;
    };

    spu?: any;
  };

  static relaxProps = {
    checkSku: noop,
    changeSkuNum: noop,
    edit: 'edit',
    skus: 'skus',
    showDistributeCommission: 'showDistributeCommission',
    intervalPrices: 'intervalPrices',
    skuMarketingDict: 'skuMarketingDict',
    init: noop,
    open: noop
  };

  render() {
    let { spu } = this.props;
    const {
      skus,
      edit,
      checkSku,
      changeSkuNum,
      intervalPrices,
      skuMarketingDict,
      init,
      open,
      showDistributeCommission
    } = this.props.relaxProps;

    //取出sku
    const skuList = skus
      .filter(
        (sku) =>
          spu.get('goodsInfoIds').includes(sku.get('goodsInfoId')) &&
          sku.get('goodsStatus') !== 2
      )
      //排序spu下面sku
      .sort((sku1, sku2) => {
        const date1 = moment(sku1.get('createTime'));
        const date2 = moment(sku2.get('createTime'));
        if (date1 > date2) {
          return -1;
        } else if (date1 < date2) {
          return 1;
        } else {
          return 0;
        }
      });

    const elements = skuList
      .map((sku) => {
        let noStock = sku.get('goodsStatus') === 1 && !edit;

        let goodsInfoId = sku.get('goodsInfoId');

        let skuMarketings = skuMarketingDict.get(goodsInfoId);

        // 是否有营销
        let hasMarketing = skuMarketings && skuMarketings.count();

        let selectedMarketing = null;

        if (hasMarketing) {
          skuMarketings.forEach((skuMarketing, index) => {
            if (skuMarketing.get('checked')) {
              selectedMarketing = skuMarketing;
            }
          });
        }

        let hasManyMarketing = skuMarketings && skuMarketings.count() > 1;
        // 社交电商相关内容显示与否
        const social =
          showDistributeCommission && sku.get('distributionGoodsAudit') == 2;
        return (
          <div key={goodsInfoId}>
            <div className={noStock ? 'sku-item no-item' : 'sku-item'}>
              <Check
                checked={sku.get('checked')}
                onCheck={() => {
                  checkSku(sku.get('goodsInfoId'));

                  init();
                }}
                disable={noStock}
                checkStyle={{width:'0.36rem',height:'0.36rem',lineHeight:'0.36rem'}}
              />
              <div
                className="sku-detail"
                onClick={() =>
                  history.push(`/goods-detail/${sku.get('goodsInfoId')}`)
                }
              >
                <div className="sku-content">
                  <span className="sku-font">{sku.get('specText')}</span>
                  <span className="sku-price">
                    <i className="iconfont icon-qian" />
                    {`${_calculateGoodsPrice(sku, intervalPrices).toFixed(2)}`}
                    {social && (
                      <span
                        className={
                          noStock
                            ? 'commission commission-disabled'
                            : 'commission'
                        }
                      >
                        /&nbsp;返利&nbsp;{_.addZero(
                          sku.get('distributionCommission')
                        )}
                      </span>
                    )}
                    {noStock && <span className="out-stock">缺货</span>}
                  </span>
                </div>
                <div className="number-input">
                  {!edit && (
                    <NumberInput
                      disableNumberInput={noStock}
                      value={noStock ? 0 : sku.get('buyCount')}
                      max={9999999}
                      onDelayChange={(value) =>
                        changeSkuNum(
                          value,
                          sku.get('goodsInfoId'),
                          sku.get('stock')
                        )
                      }
                      min={noStock ? 0 : 1}
                      error={sku.get('error')}
                    />
                  )}
                </div>
              </div>
            </div>
            {hasMarketing && selectedMarketing ? (
              <div className={styles.simpleItems}>
                {false && (
                  <div className="couponItem">
                    <div className="couponLeft">
                      <span className="title">领券</span>
                      <span className="info">
                        满168减10，满368减30，满568减50
                      </span>
                    </div>
                  </div>
                )}
                <div className="couponItem">
                  <div className="couponLeft">
                    <span className="title">促销</span>
                    <span className="info">
                      {selectedMarketing.get('alllevelDesc')}
                    </span>
                  </div>

                  {hasManyMarketing && (
                    <div
                      className="couponRight"
                      onClick={() => {
                        let param = {
                          type: 'chooseGoodsMarketing',
                          goodsInfoId: goodsInfoId,
                          hasBottom: false
                        };

                        open(param);
                      }}
                    >
                      <span>修改</span>
                      <i className="iconfont icon-jiantou1" />
                    </div>
                  )}
                </div>
              </div>
            ) : null}
          </div>
        );
      })
      .toArray();

    return <div>{elements}</div>;
  }
}
