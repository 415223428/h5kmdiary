import React from 'react';
import { Relax, msg } from 'plume2';
import { List } from 'immutable';
import { noop, WMkit } from 'wmkit';
var Marquee = require('react-marquee');
const styles = require('../css/style.css');
const sign = require('../img/sign.png')
var Marquee = require('react-marquee');
@Relax
export default class Head extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      edit: boolean;
      ifAdver: number;
      loginFlag: boolean;
      showEdit: (status: boolean) => void;
      skus: List<any>;
      mergePurchaseAndInit: Function;
      cartContent: String,
      cartContentEnd:String,
      cartContentStart:String,
    };
  };

  static relaxProps = {
    //全局编辑状态
    edit: 'edit',
    loginFlag: 'loginFlag',
    showEdit: noop,
    mergePurchaseAndInit: noop,
    skus: 'skus',
    ifAdver: 'ifAdver',
    cartContent: 'cartContent',
    cartContentStart:'cartContentStart',
    cartContentEnd:'cartContentEnd'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      edit,
      loginFlag,
      showEdit,
      skus,
      mergePurchaseAndInit,
      ifAdver,
      cartContent,
      cartContentStart,
      cartContentEnd
    } = this.props.relaxProps;
// const cartContentText  = `${cartContent}   ${cartContentEnd}`
    return (
      <div className={styles.box + ' head-header'} style={{ display: "flex", height: "1.2rem" }}>
        {/*{(skus.count() < 1 && loginFlag && ifAdver)?*/}
        {/*  <div className="notice-bar">*/}
        {/*    <img src={sign} alt="" style={{width: '0.36rem',marginRight: '0.19rem'}}/>*/}
        {/*    {cartContent}*/}
        {/*  </div>:null*/}
        {/*}*/}
        {/* {(skus.count() > 0 || !loginFlag) && ( */}
        <div style={{ width: "100%" }}>
          {skus.count() < 1 && !loginFlag && (
            <div style={{ marginTop: '16px', display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}>
              <div
                style={{
                  width: '1.11rem', height: '.49rem', lineHeight: '.49rem', color: '#000', border: '1px solid #333',
                  backgroundColor: '#fff',
                  borderRadius: '.24rem',
                  fontWeight: 700,
                  textAlign: 'center',
                  fontSize: '.26rem',
                }}
                onClick={() => {
                  msg.emit('loginModal:toggleVisible', {
                    callBack: () => {
                      mergePurchaseAndInit();
                    }
                  });
                }}
                key="1"
              >
                登录
                </div>
              <span style={{ fontSize: '0.26rem', color: '#333' }} key="2">登录后自动同步购物车中已有商品</span>
              {/* <img src={require('../img/service.png')} alt=""
                  onClick={() => {
                    (window as any).openqiyu();
                    window.location.href = (window as any).ysf('url'); }}
                  style={{marginTop:'4px',width: '.41rem',height: '.38rem'}}
                /> */}
            </div>
          )}
          {/*{(skus.count() > 0 && ifAdver)?*/}
          {loginFlag&&ifAdver ?
            <div className="notice-bar">
              <img src={sign} alt="" style={{ width: '0.36rem', marginRight: '0.19rem' }} />
              <div className="notice-text"><Marquee hoverToStop = "true" text ={cartContent} loop = "true"/></div>
            </div>
            : null}
          {/*:null*/}
          {/*}*/}
          <div className="purchase-head cart-header" style={{ display: 'flex', justifyContent: 'space-between', top: '.65rem' }}>
            {/*{skus.count() > 0 && (*/}
            <a
              href="javascript:;"
              className="edit-font"
              style={{ padding: '4px 10px 0' }}
              onClick={() => showEdit(!edit)}
            >
              {edit ? '完成' : '编辑'}
            </a>
            {/*)}*/}
            {!((window as any).isMiniProgram) && (
              // {skus.count() > 0 && !((window as any).isMiniProgram) && (
              <img src={require('../img/service.png')} alt="" className="service"
                onClick={() => {
                  WMkit.getQiyuCustomer();
                  window.location.href = (window as any).ysf('url');
                  (window as any).openqiyu();
                }}
                style={{ marginTop: '4px' }}
              />
            )}
          </div>

        </div>
        {/*)}*/}
      </div>
    );
  }
}
