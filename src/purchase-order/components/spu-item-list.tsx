import React from 'react';
import { Relax } from 'plume2';
import { List } from 'immutable';

import { noop } from 'wmkit';
import SpuItem from './spu-item';
import SkuItem from './sku-item';
import InvalidSkuList from './invaild-sku-list';

/**
 * spu展示项目
 */
@Relax
export default class SpuItemList extends React.Component<any, any> {
  props: {
    relaxProps?: {
      skus: List<any>;
      spus: List<any>;
      checkSpu: Function;
      edit: boolean;
      stores: List<any>;
    };
    storeId: string;
  };

  static relaxProps = {
    skus: 'skus',
    spus: 'spus',
    checkSpu: noop,
    edit: 'edit',
    stores: 'stores'
  };

  render() {
    return <div>{this._renderSpus()}</div>;
  }

  /**
   * 循环spu排序
   * @returns {Array<boolean|any|boolean|any>}
   * @private
   */
  _renderSpus() {
    let { spus, skus, stores } = this.props.relaxProps;
    if (!stores) {
      return null;
    }

    //获取店铺
    const store = stores.find(
      (store) => store.get('storeId') === this.props.storeId
    );
    if (!store) {
      return null;
    }
    //获取店铺的spu
    const storeSpus = store.get('goodsIds');

    return spus
      .toSeq()
      .filter((spu) => storeSpus.includes(spu.get('goodsId')))
      .map((spu) => {
        const renderSpuStatus =
          skus
            .filter(
              (sku) =>
                spu.get('goodsInfoIds').includes(sku.get('goodsInfoId')) &&
                sku.get('goodsStatus') !== 2
            )
            .count() > 1;
            console.log('renderSpuStatus----------------',renderSpuStatus);
            
        if (renderSpuStatus) {
          return <SpuItem spu={spu} key={spu.get('goodsId')} />;
        } else {
          //取出spu中只有一个sku的skuids
          const skuItems = skus.filter(
            (sku) =>
              spu.get('goodsInfoIds').includes(sku.get('goodsInfoId')) &&
              sku.get('goodsStatus') !== 2
          );
          const renderSkuStatus = skuItems.count() === 1;
          const sku = skuItems.get(0);
          return (
            renderSkuStatus && (
              <SkuItem sku={sku} key={sku.get('goodsInfoId')} />
            )
          );
        }
      })
      .toArray();
  }
}
