import React, {Component} from 'react'
import { Check, WMImage } from 'wmkit'
import {IMap, Relax} from "plume2";
import {IList} from "typings/globalType";
import {noop} from "wmkit";


const styles = require('../css/style.css')

@Relax
export default class GiftItemList extends Component<any, any> {


  props: {
    storeId: string,
    relaxProps?: {
      selectedMarketingGifts: IList,
      giftGoodsInfos: IMap
    }
  }


  static relaxProps = {
    selectedMarketingGifts: 'selectedMarketingGifts',
    giftGoodsInfos: 'giftGoodsInfos'
  }


  constructor(props) {
    super(props);
  }


  render() {
    const storeId = this.props.storeId;
    const {selectedMarketingGifts, giftGoodsInfos} = this.props.relaxProps;

    const mergeSelectedMarketingGifts = selectedMarketingGifts.filter(selectedMarketingGift => selectedMarketingGift.get('storeId') === storeId)
        .groupBy(selectedMarketingGift => selectedMarketingGift.get('goodsInfoId')).map((gifts,goodsInfoId) => {
          let goodsNum = 0;
          gifts.forEach(gift => {
            goodsNum += gift.get('goodsNum');
          })

          return gifts.first().set('goodsNum', goodsNum);
        });

    return (
      <div 
      // style={{borderBottom: '1px solid #ebebeb'}}
      >
        {
          mergeSelectedMarketingGifts.map((selectedMarketingGift, index) => {
            const sku = giftGoodsInfos.get(selectedMarketingGift.get('goodsInfoId'));

            let invalid = sku.get('goodsStatus') == 2;
            const stock = sku.get('stock')
            const min = sku.get('count')
            //缺货状态显示
            let noStock = stock <= 0

            return (
                <div key={index} className={styles.item} style={{borderTop: 'none'}}>
                  <div className={styles.check}>
                    <Check checked={true} disable={true}/>
                  </div>
                  <div className={noStock ? `goods-list invalid-goods` : `goods-list`} style={{border: 0}}
                       onClick={() => !invalid ? window.location.href = `/goods-detail/${sku.get('goodsInfoId')}` : null}>
                    <div className="img-box" style={{width: 55, height: 55}}>
                      <WMImage className={styles.img} src={sku.get('goodsInfoImg')} alt="" width="100%"
                               height="100%"/>
                    </div>
                    <div className="detail" style={{minHeight: 55}}>
                      <div className="title" style={{margin: 0}}>
                        <div className="tag-gift">赠品</div>
                        {sku.get('goodsInfoName')}</div>
                      <p className="gec" style={{margin: 0}}>{sku.get('specText')}</p>
                      <div className="bottom" style={{margin: 0}}>
                        <div>
                          <span className="price"><i className="iconfont icon-qian"/>0.00</span>
                          {noStock && <div className="out-stock" style={{marginLeft: 5}}>缺货</div>}
                        </div>
                        <span className="num">×{selectedMarketingGift.get('goodsNum')}</span>
                      </div>
                    </div>
                  </div>
                </div>
            )
          }).toArray()
        }

      </div>
    )
  }

}
