import React from 'react';
import { Relax } from 'plume2';
import { List } from 'immutable';

import { WMImage, noop } from 'wmkit';

import { _calculateGoodsPrice } from '../kit';

const styles = require('../css/style.css');

@Relax
export default class InvalidSkuList extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      //显示返利信息
      showDistributeCommission: boolean;
      skus: List<any>;
      cleanInvalidGoods: Function;
      intervalPrices: List<any>;
    };
  };

  static relaxProps = {
    //显示返利信息
    showDistributeCommission: 'showDistributeCommission',
    skus: 'skus',
    cleanInvalidGoods: noop,
    intervalPrices: 'intervalPrices'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      skus,
      cleanInvalidGoods,
      intervalPrices,
      showDistributeCommission
    } = this.props.relaxProps;
    const renderSkus = skus.filter((sku) => sku.get('goodsStatus') === 2);

    const invalidSkus = renderSkus
      .map((sku) => {
        // 社交电商相关内容显示与否
        const social =
          showDistributeCommission && sku.get('distributionGoodsAudit') == 2;
        return (
          <div className={styles.item} key={sku.get('goodsInfoId')}>
            <div className="goods-list invalid-goods" style={{ border: 0 }}>
              <div
                className="out-stock"
                style={{ marginLeft: 0, marginRight: 5 }}
              >
                失效
              </div>
              <div className="img-box" style={{ width: '2.2rem', height:'2.2rem',objectFit:'scale-down' }}>
                <WMImage
                  className={styles.img}
                  src={sku.get('goodsInfoImg')}
                  alt=""
                  // width="100%"
                  height="80%"
                  style={{objectFit:'cover'}}
                />
              </div>
              <div className="detail" style={{ minHeight: 80 ,paddingTop:'.16rem'}}>
                <div className="title" style={{     height: '.4rem',lineHeight: '.4rem' }}>
                  {sku.get('goodsInfoName')}
                </div>
                <p className="gec">{sku.get('specText')}</p>
                <div className="bottom">
                  <span className="price">
                    <i className="iconfont icon-qian" />
                    {`${_calculateGoodsPrice(sku, intervalPrices).toFixed(2)}`}
                    {social && (
                      <span className="commission commission-disabled">
                        /&nbsp;返利{sku
                          .get('distributionCommission')
                          .toFixed(2)}
                      </span>
                    )}
                  </span>
                </div>
              </div>
            </div>
          </div>
        );
      })
      .toArray();

    return (
      <div>
        {renderSkus.count() > 0 && (
          <div className="invalid-head">
            <h4>失效商品{renderSkus.count()}件</h4>
            <a onClick={() => cleanInvalidGoods()}>清空失效宝贝</a>
          </div>
        )}
        {invalidSkus}
      </div>
    );
  }
}
