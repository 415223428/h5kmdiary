import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';
import { IList } from 'typings/globalType';
import { noop, Check, history, _ } from 'wmkit';
import { Const } from 'config';

@Relax
export default class HeadPromotion extends Component<any, any> {
  props: {
    marketings?: IList;
    form?: any;
    relaxProps?: {
      chooseGift: Function;
      selectedMarketingGifts: IList;
      open: Function;
      edit: boolean;
    };
  };

  static relaxProps = {
    chooseGift: noop,
    selectedMarketingGifts: 'selectedMarketingGifts',
    open: noop,
    edit: 'edit'
  };

  constructor(props) {
    super(props);
  }

  render() {
    let marketings = this.props.marketings;
    const { open, edit, selectedMarketingGifts } = this.props.relaxProps;

    return (
      <div>
        {marketings
          .map((marketing) => {
            let isFullGift = marketing.get('marketingType') === 2;
            let isFullReduction = marketing.get('marketingType') === 0;
            let isFullDiscount = marketing.get('marketingType') === 1;

            let tag = Const.marketingType[marketing.get('marketingType')];
            let title = '';
            let buttonTitle = '';
            let rule = '';

            if (isFullGift) {
              rule =
                marketing.get('subType') === 4
                  ? marketing.getIn(['fullGiftLevel', 'fullAmount'])
                  : marketing.getIn(['fullGiftLevel', 'fullCount']) + '件';

              title =
                marketing.get('lack') > 0
                  ? `满${rule}获赠品，还差${marketing.get('lack')}${
                      marketing.get('subType') === 4 ? '元' : '件'
                    }`
                  : `您已满足满${rule}获赠品`;

              buttonTitle = marketing.get('lack') > 0 ? '查看赠品' : '领取赠品';
            } else if (isFullReduction) {
              rule =
                marketing.get('subType') === 0
                  ? marketing.getIn(['fullReductionLevel', 'fullAmount'])
                  : marketing.getIn(['fullReductionLevel', 'fullCount']) + '件';
              let reduction = marketing.getIn([
                'fullReductionLevel',
                'reduction'
              ]);

              title =
                marketing.get('lack') > 0
                  ? `满${rule}减${reduction}，还差${marketing.get('lack')}${
                      marketing.get('subType') === 0 ? '元' : '件'
                    }`
                  : `您已满足满${rule}减${reduction}`;
            } else if (isFullDiscount) {
              rule =
                marketing.get('subType') === 2
                  ? marketing.getIn(['fullDiscountLevel', 'fullAmount'])
                  : marketing.getIn(['fullDiscountLevel', 'fullCount']) + '件';
              let discount = _.mul(
                marketing.getIn(['fullDiscountLevel', 'discount']),
                10
              );

              title =
                marketing.get('lack') > 0
                  ? `满${rule}享${discount}折，还差${marketing.get('lack')}${
                      marketing.get('subType') === 2 ? '元' : '件'
                    }`
                  : `您已满足满${rule}享${discount}折`;
            }

            let showButton = !edit && isFullGift;

            return (
              <div
                className="cart-promotion"
                key={marketing.get('marketingId')}
                style={{height:'0.8rem'}}
              >
                <div className="promotion-info">
                  <div className="tag">{tag}</div>

                  {/* <img src="赠" alt=""/> */}

                  <div className="gift-info">
                    <span style={{fontSize:'0.26rem',color:'#333'}}>{title}</span>
                    {showButton ? (
                      <div
                        className="btn btn-small btn-round"
                        onClick={() => {
                          let type =
                            marketing.get('lack') > 0
                              ? 'showGift'
                              : 'chooseGift';
                          let param = {
                            type: type,
                            marketing: marketing,
                            selectedMarketingGifts: selectedMarketingGifts,
                            hasBottom: marketing.get('lack') <= 0
                          };
                          open(param);
                        }}
                        style={{
                          background:'linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))',color:'#fff',fontSize:'0.24rem',border:'none',
                          width:'1.3rem',height:'0.44rem', display:'inline-flex',justifyContent:'center',alignItems:'center'
                        }}
                      >
                        {buttonTitle}
                      </div>

                      // <img src="查看赠品" alt=""/>
                    
                    ) : null}
                  </div>
                </div>
                {!edit ? (
                  <div
                    className="getSingle"
                    onClick={() => {
                      history.push(
                        `/goods-list-promotion/${marketing.get('marketingId')}`
                      );
                    }}
                  >
                    去凑单<i className="iconfont icon-jiantou1" />
                  </div>
                ) : null}
              </div>
            );
          })
          .toArray()}
      </div>
    );
  }
}
