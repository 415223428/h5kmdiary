import React from 'react';
import { Relax } from 'plume2';
import { List } from 'immutable';

import { Check, noop, Button, Confirm, _, WMkit } from 'wmkit';
import { checkAllQL, disableOrderQL, sumPriceQL, countSelectedQL } from '../ql';

const styles = require('../css/style.css');
const SubmitButton = Button.Submit;
const xiadan=require('../img/xiadan1.png')
const del=require('../img/del.png')
const save=require('../img/save.png')
@Relax
export default class Bottom extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      //显示返利信息
      showDistributeCommission: boolean;
      edit: boolean;
      skus: List<any>;
      checkAll: Function;
      checkAllQL: boolean;
      disableOrderQL: boolean;
      deletePurchase: Function;
      addToFollow: Function;
      totalPrice: number;
      toConfirm: Function;
      countSelectedQL: number;
      discountPrice: number;
      tradePrice: number;
      distributeCommission: number;
    };
  };

  static relaxProps = {
    //显示返利信息
    showDistributeCommission: 'showDistributeCommission',
    //获取全局的编辑状态
    edit: 'edit',
    skus: 'skus',
    checkAll: noop,
    checkAllQL: checkAllQL,
    disableOrderQL: disableOrderQL,
    deletePurchase: noop,
    addToFollow: noop,
    totalPrice: 'totalPrice',
    toConfirm: noop,
    countSelectedQL: countSelectedQL,
    discountPrice: 'discountPrice',
    tradePrice: 'tradePrice',
    distributeCommission: 'distributeCommission'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      edit,
      skus,
      checkAll,
      checkAllQL,
      disableOrderQL,
      countSelectedQL,
      totalPrice,
      addToFollow,
      toConfirm,
      discountPrice,
      tradePrice,
      showDistributeCommission,
      distributeCommission
    } = this.props.relaxProps;

    //编辑状态
    if (edit && skus.count() > 0) {
      const disabledMutation =
        skus
          .filter((sku) => sku.get('checked') && sku.get('goodsStatus') !== 2)
          .count() === 0;
      // 社交电商相关内容显示与否
      const social = showDistributeCommission;
      //分销-店铺精选
      const isShop = WMkit.isShop();
      return (
        <div>
          <div className="purchase-bottom">
            <div className="check-left">
              <Check
                checked={checkAllQL}
                onCheck={() => checkAll(checkAllQL)}
                checkStyle={{width:'0.38rem',height:'0.38rem',lineHeight:'0.38rem'}}
              />
              <span className="grey">全选</span>
            </div>
            <div className="purchase-btn">
              {!isShop && (
                <SubmitButton
                  defaultStyle={{
                    background: 'linear-gradient(135deg,rgba(247,187,88,1),rgba(255,139,8,1))',
                    // borderColor: '#ff7e90',
                    border: 'none',
                    width: '1.9rem'
                  }}
                  text={'移入收藏'}
                  onClick={addToFollow}
                  disabled={disabledMutation}                 
                />
                // <img src={save} alt="" style={{width: '1.9rem',height: '100%'}} onClick={addToFollow}/>
              )}
             { !isShop?
                  <SubmitButton
                    defaultStyle={{
                      border: 'none',
                      width: '1.9rem'
                    }}
                    text={'删除'}
                    onClick={this._handleDeletePurchaseOrder}
                    disabled={disabledMutation}
                  />
                :
                <img src={del} alt=""  style={{width: '1.9rem',height: '100%'}} 
                      onClick={this._handleDeletePurchaseOrder}/>
              }
            </div>
          </div>
        </div>
      );
    }

    //普通状态
    return (
      <div>
        <div className="purchase-bottom">
          <div className="check-left">
            <Check checked={checkAllQL} onCheck={() => checkAll(checkAllQL)} checkStyle={{width:'0.38rem',height:'0.38rem',lineHeight:'0.38rem'}}/>
            <span className="grey" style={{color:'#999',fontSize:'0.28rem'}}>全选</span>
          </div>
          <div className={styles.total}>
            <span className="totalPrice" style={{fontSize:'0.28rem',fontWeight:'bold',color:'#333'}}>
              合计:
              <span style={{fontSize:'0.28rem',color:'#FF4D4D',fontWeight:'bold'}}>
                ￥
                {_.addZero(tradePrice)}
              </span>
              {showDistributeCommission &&
                distributeCommission > 0 && (
                  <span className="commission">
                    可返利:￥{_.addZero(distributeCommission)}
                  </span>
                )}
            </span>
            <span className={styles.totalPrice} style={{fontSize:'0.2rem',color:'#999'}}> 
              商品总额:
              <span>
                ￥
                {_.addZero(totalPrice)}
              </span>&nbsp;
              {!WMkit.isShop() && (
                <span>
                  优惠:-￥
                  {_.addZero(discountPrice)}
                </span>
              )}
            </span>
          </div>
          {/* {
            countSelectedQL==0?
              (<div style={{width: '1.9rem'}}><span>下单</span></div>)
              :(<div onClick={toConfirm}><img src={xiadan} alt="" style={{width: '1.9rem',height: '100%'}}/><span>({countSelectedQL})</span></div>)
          } */}
          {countSelectedQL==0?
            <SubmitButton
              text='下单'
              onClick={toConfirm}
              disabled={disableOrderQL}
              defaultStyle={{
                border: 'none',
                width: '1.9rem'
              }}
            />
          :
            <div style={{display:'flex',alignItems:'center'}} onClick={()=>{(toConfirm)()}}>
              <img src={xiadan} alt=""style={{width:'1.9rem'}}/>
              <span style={{position: 'absolute',right: '0.3rem',color: '#fff'}}>({countSelectedQL})</span>
            </div>
          }
        </div>
      </div>
    );
  }

  /**
   * 删除购物车
   * @private
   */
  _handleDeletePurchaseOrder = () => {
    const { deletePurchase } = this.props.relaxProps;
    Confirm({
      title: '删除购物车商品',
      text: '您确定要删除所选商品？',
      okBtn: '确定',
      cancelBtn: '取消',
      confirmCb: function() {
        deletePurchase();
      }
    });
  };
}
