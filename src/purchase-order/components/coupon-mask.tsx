import React, { Component } from 'react';
import { Relax } from 'plume2';

import { noop } from 'wmkit';
import { IList } from 'typings/globalType';

import CouponItem from './coupon-item';

/**
 * 领券弹窗
 */
@Relax
export default class CouponMask extends Component<any, any> {
  props: {
    relaxProps?: {
      couponList: IList;
      storeList: IList;
      showCouponMask: boolean;
      changeCouponMask: Function;
      receiveCoupon: Function;
    };
  };

  static relaxProps = {
    showCouponMask: 'showCouponMask',
    couponList: 'couponList',
    storeList: 'storeList',

    changeCouponMask: noop,
    receiveCoupon: noop
  };

  render() {
    const {
      showCouponMask,
      changeCouponMask,
      couponList,
      storeList,
      receiveCoupon
    } = this.props.relaxProps;
    return showCouponMask ? (
      <div
        style={{
          position: 'fixed',
          left: 0,
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0,0,0,.5)',
          zIndex: 9999,
          display: 'flex',
          flexDirection: 'row',
          bottom: 0
        }}
      >
        <div
          style={{ flex: 1, display: 'flex' }}
          onClick={() => changeCouponMask()}
        />
        <div className="panel-bottom">
          <div className="panel-title b-1px-b">
            <h1 style={{fontSize:'0.32rem',color:'#333'}}>领券</h1>
            <img
              src={require('../img/close.png')}
              onClick={() => changeCouponMask()}
              style={{width:'0.4rem',height:'0.4rem'}}
            />
          </div>
          <p className="coupon-panel-title">可领取的优惠券</p>
          <div className="panel-body">
            <div className="coupon-panel-list">
              {couponList.map((coupon, i) => {
                let storeName =
                  storeList && storeList.get(`${coupon.get('storeId')}` as any);
                return (
                  <CouponItem
                    key={i}
                    coupon={coupon}
                    storeName={storeName}
                    receiveCoupon={receiveCoupon}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    ) : null;
  }
}
