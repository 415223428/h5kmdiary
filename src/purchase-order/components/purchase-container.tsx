import React from 'react';
import { Relax } from 'plume2';
import { List } from 'immutable';

import { Loading, Blank } from 'wmkit';

import StoreInfoList from './store-info-list';

@Relax
export default class PurchaseContainer extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      loading: boolean;
      skus: List<any>;
    };
  };

  static relaxProps = {
    loading: 'loading',
    skus: 'skus'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { loading, skus } = this.props.relaxProps;

    const count = skus.count();

    if (loading) {
      return <Loading />;
    }

    return count > 0 ? (
      <StoreInfoList />
    ) : (
      <Blank
        style={{ height: 'calc(100vh - 46px - 1.92rem)',  width:'7.1rem',margin:'0 auto',background:'#fff',borderRadius:'0.2rem'}}
        img={require('../img/none.png')}
        content="您的购物车是空哒"
        isToGoodsList={true}
      />
    );
  }
}
