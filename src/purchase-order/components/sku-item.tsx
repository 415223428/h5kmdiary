import React from 'react';
import { IMap, Relax } from 'plume2';
import { List } from 'immutable';

import { Check, noop, NumberInput, WMImage, _, history } from 'wmkit';
import { _calculateGoodsPrice } from '../kit';

const styles = require('../css/style.css');

@Relax
export default class SkuItem extends React.Component<any, any> {
  props: {
    form?: any;
    relaxProps?: {
      edit: boolean;
      checkSku: Function;
      changeSkuNum: Function;
      intervalPrices: List<any>;
      skuMarketingDict: IMap;
      init: Function;
      open: Function;
      showDistributeCommission: boolean;
    };
    sku;
  };

  static relaxProps = {
    edit: 'edit',
    checkSku: noop,
    changeSkuNum: noop,
    intervalPrices: 'intervalPrices',
    skuMarketingDict: 'skuMarketingDict',
    init: noop,
    open: noop,
    showDistributeCommission: 'showDistributeCommission'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { sku } = this.props;
    const {
      edit,
      checkSku,
      changeSkuNum,
      intervalPrices,
      skuMarketingDict,
      init,
      open,
      showDistributeCommission
    } = this.props.relaxProps;

    const stock = sku.get('stock');
    const min = sku.get('count');
    //缺货状态显示
    let noStock = sku.get('goodsStatus') === 1 && !edit;

    let goodsInfoId = sku.get('goodsInfoId');

    let skuMarketings = skuMarketingDict.get(goodsInfoId);

    // 是否有营销
    let hasMarketing = skuMarketings && skuMarketings.count();

    let selectedMarketing = null;

    if (hasMarketing) {
      skuMarketings.forEach((skuMarketing) => {
        if (skuMarketing.get('checked')) {
          selectedMarketing = skuMarketing;
        }
      });
    }

    let hasManyMarketing = skuMarketings && skuMarketings.count() > 1;

    // 社交电商相关内容显示与否
    const social =
      showDistributeCommission && sku.get('distributionGoodsAudit') == 2;

    return (
      <div>
        <div className={styles.item}>
          <div className={styles.check}>
            <Check
              checked={sku.get('checked')}
              onCheck={() => {
                checkSku(sku.get('goodsInfoId'));

                init();
              }}
              disable={noStock}
              checkStyle={{width:'0.36rem',height:'0.36rem',lineHeight:'0.36rem'}}
            />
          </div>
          <div
            className={noStock ? 'goods-list invalid-goods' : 'goods-list'}
            style={{ border: 0 ,marginTop:'.1rem'}}
            onClick={() =>
              history.push(`/goods-detail/${sku.get('goodsInfoId')}`)
            }
          >
            <div className="img-box" style={{ width: '2.2rem', height: '2.2rem',objectFit:'scale-down' }}>
              <WMImage
                mode="pad"
                className={styles.img}
                src={sku.get('goodsInfoImg')}
                alt=""
                // width="80%"
                height="80%"
                style={{objectFit:'cover'}}
              />
              {/*<img src={sku.get('goodsInfoImg')} />*/}
            </div>
            <div className="detail" style={{ minHeight: 80 }}>
              <div className="title" style={{height:".4rem", lineHeight: '.4rem'}}>{sku.get('goodsInfoName')}</div>
              <p className="gec">{sku.get('specText')}</p>
              {noStock && <div className="out-stock">缺货</div>}
              <div className="price-overflow" style={{marginTop:'0.4rem'}}>
                <span className="price" style={{fontSize:'0.38rem',color:'#FF4D4D',fontWeight:'bold'}}>
                  <i className="iconfont icon-qian" style={{fontWeight:'normal',fontSize:'0.27rem'}}/>
                  {`${_calculateGoodsPrice(sku, intervalPrices).toFixed(2)}`}
                </span>
                {social && (
                  <span
                    className={
                      noStock
                        ? 'commission commission-disabled'
                        : 'commission'
                    }
                  >
                    /&nbsp;返利&nbsp;
                    <span style={{fontSize:'0.23rem',color:'#FF4D4D'}}>￥{_.addZero(
                      sku.get('distributionCommission')
                    )}</span>
                  </span>
                )}
              </div>
              <div className="bottom" style={{justifyContent:'flex-end'}}>             
                {!edit && (
                  <NumberInput
                    disableNumberInput={noStock}
                    value={noStock ? 0 : sku.get('buyCount')}
                    max={9999999}
                    onDelayChange={(value) =>
                      changeSkuNum(
                        value,
                        sku.get('goodsInfoId'),
                        sku.get('stock')
                      )
                    }
                    min={noStock ? 0 : 1}
                    error={sku.get('error')}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
        {hasMarketing && selectedMarketing ? (
          <div className={styles.simpleItems}>
            {false && (
              <div className="couponItem">
                <div className="couponLeft">
                  <span className="title">领券</span>
                  <span className="info">满168减10，满368减30，满568减50</span>
                </div>
              </div>
            )}
            <div className="couponItem">
              <div className="couponLeft">
                <span className="title">促销</span>
                <span className="info">
                  {selectedMarketing.get('alllevelDesc')}
                </span>
              </div>

              {hasManyMarketing && (
                <div
                  className="couponRight"
                  onClick={() => {
                    let param = {
                      type: 'chooseGoodsMarketing',
                      goodsInfoId: goodsInfoId,
                      hasBottom: false
                    };

                    open(param);
                  }}
                >
                  <span>修改</span>
                  <i className="iconfont icon-jiantou1" />
                </div>
              )}
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
