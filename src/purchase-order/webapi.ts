import { Fetch, WMkit } from 'wmkit';

// 购物车数据返回
interface IPurchase {
  /**
   * 商品SKU信息
   */
  goodsInfos: Array<any>;

  /**
   * 商品SPU信息
   */
  goodses: Array<any>;

  /**
   * 商品区间价格列表
   */
  goodsIntervalPrices: Array<any>;

  /**
   * 公司信息（商家）
   */
  companyInfos: Array<any>;

  /**
   * 店铺
   */
  stores: Array<any>;

  /**
   * 店铺营销，storeId作为map的key
   */
  storeMarketingMap: Map<any, any>;

  /**
   * 商品营销信息，skuId作为map的key
   */
  goodsMarketingMap: Map<any, any>;

  /**
   * 商品选中的营销
   */
  goodsMarketings: Array<any>;

  /**
   * 购物车商品总金额
   */
  totalPrice: number;

  /**
   * 购物车商品总金额减去优惠后的金额
   */
  tradePrice: number;

  /**
   * 购物车优惠金额
   */
  discountPrice: number;
}

/**
 * 查询购物车
 * @returns {Promise<Result<IPurchase>>}
 */
export const fetchPurchaseOrder = (skuIds) => {
  return Fetch<IPurchase>('/site/purchases', {
    method: 'POST',
    body: JSON.stringify({ goodsInfoIds: skuIds })
  });
};

/**
 * 查询未登录时,购物车缓存信息
 * @param arr 购物车缓存信息[{skuId, num}]
 * @param skuIds 用户勾选的skuIds
 * @param skuMarArr 用户针对各sku选择的营销活动
 */
export const fetchPurchaseFromCache = (arr, skuIds, skuMarArr) => {
  return Fetch<IPurchase>('/site/front/purchases', {
    method: 'POST',
    body: JSON.stringify({
      goodsInfoDTOList: arr,
      goodsInfoIds: skuIds,
      goodsMarketingDTOList: skuMarArr
    })
  });
};

/**
 * 更新购物车数量
 * @param num
 * @param skuId
 */
export const changeSkuNum = (num: number, skuId: string) => {
  return Fetch('/site/purchase', {
    method: 'PUT',
    body: JSON.stringify({
      goodsNum: num,
      goodsInfoId: skuId,
      //不校验库存
      verifyStock: false
    })
  });
};

/**
 * 清除失效商品
 * @returns {Promise<Result<T>>}
 */
export const cleanInvalidGoods = () => {
  return Fetch('/site/clearLoseGoods', {
    method: 'DELETE',
    body: JSON.stringify({})
  });
};

/**
 * 查询购物车数量
 */
export const fetchPurchaseCount = () => {
  return Fetch('/site/countGoods');
};

/**
 * 删除购物车
 * @returns {Promise<Result<T>>}
 */
export const deletePurchaseCount = (skuIds: Array<String>) => {
  return Fetch('/site/purchase', {
    method: 'DELETE',
    body: JSON.stringify({
      goodsInfoIds: skuIds
    })
  });
};

/**
 * 移入收藏夹
 * @param skuIds
 * @returns {Promise<Result<T>>}
 */
export const addToFollow = (skuIds: Array<String>) => {
  return Fetch('/site/addFollow', {
    method: 'PUT',
    body: JSON.stringify({
      goodsInfoIds: skuIds
    })
  });
};

/**
 * 去下单
 * @param skus
 * @returns {Promise<Result<T>>}
 */
export const toConfirm = (
  skus: Array<any>,
  tradeMarketingList: Array<any>,
  forceConfirm: boolean
) => {
  return Fetch('/trade/confirm', {
    method: 'PUT',
    body: JSON.stringify({
      tradeItems: skus,
      tradeMarketingList: tradeMarketingList,
      forceConfirm: forceConfirm
    })
  });
};

/**
 * 修改商品选择的营销
 * @param {string} goodsInfoId
 * @param {number} marketingId
 * @returns {Promise<Result<any>>}
 */
export const modifyGoodsMarketing = (
  goodsInfoId: string,
  marketingId: number
) => {
  return Fetch(`/site/goodsMarketing/${goodsInfoId}/${marketingId}`, {
    method: 'PUT'
  });
};

/**
 * 查询购物车中各店铺所选商品可领取的优惠券
 * @returns
 */
export const fetchCouponForGoodsList = (goodsInfoIds) => {
  const url = WMkit.isLoginOrNotOpen()
    ? '/coupon-info/goods-list'
    : '/coupon-info/front/goods-list';
  return Fetch(url, {
    method: 'POST',
    body: JSON.stringify(goodsInfoIds)
  });
};

/**
 * 领取优惠券
 * @param {*} couponId 优惠券Id
 * @param {*} activityId 活动Id
 */
export const receiveCoupon = (couponId, activityId) => {
  return Fetch('/coupon-code/fetch-coupon', {
    method: 'POST',
    body: JSON.stringify({
      couponInfoId: couponId,
      couponActivityId: activityId
    })
  });
};

/**
 * 查询店铺是否是有效状态
 * @returns {Promise<Result<T>>}
 */
export const fetchShopStatus = () => {
  return Fetch('/distribute/check/status');
};
// 通告栏
type TResult = { code: string; message: string; context: any };
export const getAdver = () => {
  return Fetch<TResult>('/system/advertisingConfig', {
    method: 'GET',
  });
};
