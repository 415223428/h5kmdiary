import { Actor, Action, IMap } from 'plume2'
import {fromJS} from 'immutable'

export default class SpuActor extends Actor {
  defaultState() {
    return {
      //spu值
      spus: []
    }
  }

  @Action('purchaseOrder: spus')
  spus(state: IMap, spus) {
    return state.set('spus', fromJS(spus))
  }
}