import { Actor, Action, IMap } from 'plume2';
import { fromJS, List } from 'immutable';

export default class SkuActor extends Actor {
  defaultState() {
    return {
      //商品列表
      skus: [],
      skuMarketingDict: {},
      show: false,
      popoverParam: {}
    };
  }

  /**
   * 初始化sku数据
   * @param state
   * @param skus
   * @returns {IMap}
   */
  @Action('purchase:skus')
  skus(state: IMap, skus) {
    let oldGoodsSelected = fromJS({});
    let oldGoodsErrorMsg = fromJS({});

    state.get('skus').forEach((sku) => {
      if (sku.get('checked')) {
        oldGoodsSelected = oldGoodsSelected.set(sku.get('goodsInfoId'), true);
        oldGoodsErrorMsg = oldGoodsErrorMsg.set(
          sku.get('goodsInfoId'),
          sku.get('error')
        );
      }
    });

    return state.set(
      'skus',
      fromJS(skus).map((sku) =>
        sku
          .set('checked', oldGoodsSelected.get(sku.get('goodsInfoId')))
          .set('error', oldGoodsErrorMsg.get(sku.get('goodsInfoId')))
      )
    );
  }

  /**
   * 选中sku
   * @param state
   * @param skuId
   * @returns {Map<K, V>}
   */
  @Action('purchase:check')
  check(state: IMap, skuId) {
    const skuIndex = state
      .get('skus')
      .findIndex((sku) => sku.get('goodsInfoId') === skuId);
    return state.setIn(
      ['skus', skuIndex, 'checked'],
      !state.getIn(['skus', skuIndex, 'checked'])
    );
  }

  /**
   * 选中多个sku
   * @param state
   * @param skuIds
   * @returns {Map<K, V>}
   */
  @Action('purchase:checkSpu')
  checkSkuIds(state: IMap, { skuIds, checked }) {
    return state.withMutations((state) => {
      skuIds.forEach((id) => this.checkSkuById(state, id, checked));
    });
  }

  /**
   * 全部选中
   * @param state
   * @param checked 当前是否已经全选
   * @param edit 当前是否处于编辑状态
   * @returns {Map<K, V>}
   */
  @Action('purchase:checkAll')
  checkAll(state: IMap, { checked, edit }) {
    return state.withMutations((state) => {
      state.set(
        'skus',
        state.get('skus').map((sku) => {
          if (
            (edit && sku.get('goodsStatus') !== 2) ||
            sku.get('goodsStatus') === 0
          ) {
            return sku.set('checked', !checked);
          }
          return sku;
        })
      );
    });
  }

  /**
   *
   * @param state
   * @param skuId
   * @returns {any}
   */
  checkSkuById(state: IMap, skuId: string, checked: boolean) {
    const skuIndex = state
      .get('skus')
      .findIndex((sku) => sku.get('goodsInfoId') === skuId);
    return state.setIn(['skus', skuIndex, 'checked'], !checked);
  }

  /**
   * 设置sku message
   * @param state
   * @param skuId
   * @param message
   * @returns {Map<K, V>}
   */
  @Action('purchase:setMessage')
  setMessge(state: IMap, { skuId, message }) {
    const skuIndex = state
      .get('skus')
      .findIndex((sku) => sku.get('goodsInfoId') === skuId);
    return state.setIn(['skus', skuIndex, 'error'], message);
  }

  /**
   * 修改sku数量
   * @param state
   * @param skuId
   * @param number
   * @returns {Map<K, V>}
   */
  @Action('purchase:skuNum')
  changeSkuNum(state: IMap, { skuId, number }) {
    const skuIndex = state
      .get('skus')
      .findIndex((sku) => sku.get('goodsInfoId') === skuId);
    return state.setIn(['skus', skuIndex, 'buyCount'], number);
  }

  /**
   * 错误信息提示
   * @param {plume2.IMap} state
   * @param {any} skuId
   * @param {any} number
   * @returns {Map<string, any>}
   */
  @Action('purchase:skuError')
  skuError(state: IMap, { skuId, error }) {
    const skuIndex = state
      .get('skus')
      .findIndex((sku) => sku.get('goodsInfoId') === skuId);
    return state.setIn(['skus', skuIndex, 'error'], error);
  }

  /**
   * 商品参与的营销
   * @param state
   * @param skuId
   * @param number
   * @returns {Map<K, V>}
   */
  @Action('purchase:skuMarketingDict')
  skuMarketing(state: IMap, skuMarketingDict) {
    return state.set('skuMarketingDict', skuMarketingDict);
  }

  /**
   * 显示弹窗
   * @param state
   * @param skuId
   * @param number
   * @returns {Map<K, V>}
   */
  @Action('purchase:popover:open')
  open(state: IMap, param) {
    return state.set('show', true).set('popoverParam', fromJS(param));
  }

  /**
   * 关闭弹窗
   * @param state
   * @param skuId
   * @param number
   * @returns {Map<K, V>}
   */
  @Action('purchase:popover:close')
  close(state: IMap) {
    return state.set('show', false);
  }
}
