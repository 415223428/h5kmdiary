/**
 * 全局的编辑状态的actor
 */
import { Actor, Action, IMap } from 'plume2';

export default class EditActor extends Actor {
  defaultState() {
    return { edit: false, loginFlag: true };
  }

  @Action('purchase:edit')
  editMode(state: IMap, status: boolean) {
    return state.set('edit', status);
  }

  /**
   * 设置是否登录
   */
  @Action('purchase:setLoginFlag')
  setLoginFlag(state: IMap, status: boolean) {
    return state.set('loginFlag', status);
  }
}
