import { Actor, Action, IMap } from 'plume2';
import { fromJS, Map } from 'immutable';

export default class StoreInfoActor extends Actor {
  defaultState() {
    return {
      storeInfo: {},
      //店铺列表
      stores: [],
      storeMarketing: {},
      goodsMarketingMap: {},
      giftGoodsInfos: {},
      selectedMarketingGifts: [],
      totalPrice: 0,
      tradePrice: 0,
      discountPrice: 0,
      //显示返利
      showDistributeCommission: false,
      //分销返利
      distributeCommission: 0,
      //领券弹层的状态
      showCouponMask: false,
      // 店铺优惠券活动
      storeCouponMap: [],
      //查询优惠券的参数
      goodsInfoIds: [],
      //优惠券列表
      couponList: [],
      //店铺名称列表
      storeList: [],
      ifAdver:1,
      cartContent:'this is a very very very very very very very very very very very very very very very very long text',
      intervalcode :'',
      cartContentStart:'',
      cartContentEnd:'',
    };
  }

  @Action('purchaseOrder: storeInfo')
  storeInfo(state: IMap, storeInfo) {
    return state.set('storeInfo', fromJS(storeInfo));
  }

  //设置购物车的通告开关
  @Action('imgShow:imgShow')
  ifAdver(state: IMap, ifAdver) {
    return state.set('ifAdver', ifAdver);
  }

  //设置购物车的通告消息
  @Action('storeinfo:cartContent')
  cartContent(state: IMap, cartContent) {
    return state.set('cartContent', cartContent);
  }
  
  @Action('storeinfo:intervalcode')
  intervalcode(state: IMap, intervalcode) {
    return state.set('intervalcode', intervalcode);
  }
  @Action('storeinfo:cartcontentstart')
  cartcontentstart(state: IMap, cartcontentstart) {
    return state.set('cartContentStart', cartcontentstart);
  }
  @Action('storeinfo:cartcontentend')
  cartcontentend(state: IMap, cartcontentend) {
    return state.set('cartContentEnd', cartcontentend);
  }

  /**
   * 设置店铺列表
   * @param {plume2.IMap} state
   * @param storeList
   * @returns {plume2.IMap}
   */
  @Action('purchase:stores')
  storeList(state: IMap, storeList) {
    return state.set('stores', fromJS(storeList));
  }

  /**
   * 设置店铺营销信息
   * @param {plume2.IMap} state
   * @param storeList
   * @returns {plume2.IMap}
   */
  @Action('purchase:storeMarketing')
  storeMarketing(state: IMap, storeMarketing) {
    // 原来营销的map  {markeingId: marketing}
    let oldMarketingMap = Map();
    // 新的营销的map  {markeingId: marketing}
    let newMarketingMap = Map();

    state.get('storeMarketing').forEach((value, key) => {
      value.forEach((marketing) => {
        oldMarketingMap = oldMarketingMap.set(
          marketing.get('marketingId'),
          marketing
        );
      });
    });

    storeMarketing.forEach((value, key) => {
      value.forEach((marketing) => {
        newMarketingMap = newMarketingMap.set(
          marketing.get('marketingId'),
          marketing
        );
      });
    });

    // 只有营销满足等级里的最大一个没有变化时才不清空已选赠品
    state = state.set(
      'selectedMarketingGifts',
      state.get('selectedMarketingGifts').filter((gift) => {
        let marketingId = gift.get('marketingId');

        return (
          oldMarketingMap.getIn(
            [marketingId, 'fullGiftLevel', 'giftLevelId']
          ) ===
          newMarketingMap.getIn([marketingId, 'fullGiftLevel', 'giftLevelId'])
        );
      })
    );

    return state.set('storeMarketing', storeMarketing);
  }

  /**
   * 设置商品营销信息
   * @param {plume2.IMap} state
   * @param storeList
   * @returns {plume2.IMap}
   */
  @Action('purchase:goodsMarketingMap')
  goodsMarketingMap(state: IMap, goodsMarketingMap) {
    return state.set('goodsMarketingMap', fromJS(goodsMarketingMap));
  }

  /**
   * 设置营销赠品信息
   * @param {plume2.IMap} state
   * @param storeList
   * @returns {plume2.IMap}
   */
  @Action('purchase:giftGoodsInfos')
  giftGoodsInfos(state: IMap, giftGoodsInfos) {
    return state.set('giftGoodsInfos', giftGoodsInfos);
  }

  /**
   * 选择的赠品
   * @param {plume2.IMap} state
   * @param storeList
   * @returns {plume2.IMap}
   */
  @Action('purchase:selectedMarketingGifts')
  selectedMarketingGift(state: IMap, selectedMarketingGifts) {
    return state.set('selectedMarketingGifts', selectedMarketingGifts);
  }

  /**
   * 删除赠品
   * @param {plume2.IMap} state
   * @param selectedGift
   * @returns {plume2.IMap}
   */
  @Action('purchase:deleteGift')
  deleteGift(state: IMap, selectedGift) {
    let index = state.get('selectedMarketingGifts').findIndex((gift) => {
      return (
        gift.get('marketingId') === selectedGift.get('marketingId') &&
        gift.get('giftLevelId') === selectedGift.get('giftLevelId') &&
        gift.get('goodsInfoId') === selectedGift.get('goodsInfoId')
      );
    });

    if (index !== -1) {
      state = state.set(
        'selectedMarketingGifts',
        state.get('selectedMarketingGifts').delete(index)
      );
    }

    return state;
  }

  @Action('purchaseOrder: setBottomPrice')
  setBottomPrice(state: IMap, param) {
    return state
      .set('totalPrice', param.totalPrice)
      .set('tradePrice', param.tradePrice)
      .set('discountPrice', param.discountPrice);
  }

  /**
   * 领券弹框的显示或隐藏
   * @param state
   */
  @Action('change:changeCouponMask')
  changeCouponMask(state: IMap) {
    return state.set('showCouponMask', !state.get('showCouponMask'));
  }

  /**
   * 设置店铺优惠券活动
   * @param state
   * @param storeCouponMap
   * @returns {*}
   */
  @Action('purchase:storeCouponMap')
  setStoreCouponMap(state, storeCouponMap) {
    return state.set('storeCouponMap', fromJS(storeCouponMap));
  }

  /**
   * 查询购物车中各店铺所选商品可领取的优惠券
   * @param state
   * @param context
   * @returns {*}
   */
  @Action('purchase: coupon: list')
  setCouponList(state, context) {
    return state
      .set('couponList', fromJS(context.couponViews))
      .set('storeList', fromJS(context.storeMap));
  }

  /**
   * 设置查询优惠券的参数
   * @param state
   * @param goodsInfoIds
   * @returns {*}
   */
  @Action('purchase: coupon: goodInfoIds')
  setGoodsInfoIds(state, goodsInfoIds) {
    return state.set('goodsInfoIds', goodsInfoIds);
  }

  /**
   * 设置是否显示分销返利
   * @param state
   * @param showDistributeCommissions
   * @returns {*}
   */
  @Action('purchase:showDistributeCommission')
  setShowDistributeCommission(state, showDistributeCommission) {
    return state.set('showDistributeCommission', showDistributeCommission);
  }

  /**
   * 设置分销返利
   * @param state
   * @param distributeCommissions
   * @returns {*}
   */
  @Action('purchase:distributeCommission')
  setDistributeCommission(state, distributeCommission) {
    return state.set('distributeCommission', distributeCommission);
  }
}
