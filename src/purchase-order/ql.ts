import { QL } from 'plume2';

import { _ } from 'wmkit';
import { _calculateGoodsPrice } from './kit';

/**
 * 全部选中ql
 * @type {plume2.QueryLang}
 */
export const checkAllQL = QL('checkAllQL', [
  'skus',
  'edit',
  (skus, edit) => {
    const skuList = skus.filter(sku => {
      return sku.get('goodsStatus') === 0 || (edit && sku.get('goodsStatus') !== 2);
    })
    return skuList.count() > 0 && skuList.every(sku => sku.get('checked'))
  }
]);

/**
 * 去下单按钮禁用状态
 * @type {plume2.QueryLang}
 */
export const disableOrderQL = QL('disableOrderQL', [
  'skus',
  (skus) => {
    //有商品
    //有check error
    //有check 无货
    const checkSkus = skus.filter((sku) => {
      //缺货状态显示
      return sku.get('goodsStatus') === 0 && sku.get('checked');
    });
    return checkSkus.count() == 0 || checkSkus.some((sku) => sku.get('error'));
  }
]);

/**
 * 购物车不符合商品条件提示
 * @type {plume2.QueryLang}
 */
export const disableCountQL = QL('disableCountQL', [
  'skus',
  (skus) => {
    const checkSkus = skus.filter((sku) => sku.get('checked'));
    return checkSkus.filter((sku) => sku.get('error')).count();
  }
]);

/**
 * 商品总额价格
 * @type {plume2.QueryLang}
 */
export const sumPriceQL = QL('sumPriceQL', [
  'skus',
  'intervalPrices',
  (skus, intervalPrices) => {
    return skus
      .filter((sku) => {
        //缺货状态显示 要么库存为0 要么起订量比库存大
        return sku.get('goodsStatus') === 0 && sku.get('checked');
      })
      .map((sku) =>
        _.mul(sku.get('buyCount'), _calculateGoodsPrice(sku, intervalPrices))
      )
      .reduce((price1, price2) => {
        return _.add(price1, price2);
      });
  }
]);

// /**
//  * 优惠金额
//  * @type {plume2.QueryLang}
//  */
// export const discountPriceQL = QL('discountPriceQL', [
//   'storeMarketing',
//   (storeMarketing) => {
//     let discountPrice = 0.00;
//
//     storeMarketing.map(marketings => {
//       let storeDiscountPrice = marketings.filter(marketing => (marketing.get('marketingType') === 0 || marketing.get('marketingType') === 1) && marketing.get('lack') === 0)
//           .map(marketing => {
//             // 满减
//             if (marketing.get('marketingType') === 0) {
//               return marketing.getIn(['fullReductionLevel', 'reduction']);
//             } else if (marketing.get('marketingType') === 1) {
//               // 满折
//               return _.mul(marketing.get('totalAmount'), marketing.getIn(['fullDiscountLevel', 'discount']));
//             }
//
//             return 0;
//           }).reduce((price1, price2) => {
//             return _.add(price1, price2)
//           })
//
//       if (storeDiscountPrice) {
//         discountPrice = _.add(discountPrice, storeDiscountPrice);
//       }
//     })
//
//     return discountPrice;
//   }
// ])

/**
 * 统计去下单的数量
 * @type {plume2.QueryLang}
 */
export const countSelectedQL = QL('countSelectedQL', [
  'skus',
  'edit',
  (skus, edit) => {
    return skus
      .filter((sku) => {
        //正常且不为编辑
        return sku.get('goodsStatus') === 0 && !edit && sku.get('checked');
      })
      .count();
  }
]);
