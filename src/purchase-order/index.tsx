import React from 'react';
import { StoreProvider, msg } from 'plume2';
import Head from './components/head';

import AppStore from './store';
import PurchaseContainer from './components/purchase-container';

import { Tips } from 'wmkit';
import { disableCountQL } from './ql';
import PopoverBottom from './components/popover-bottom';
import CouponMask from './components/coupon-mask';
import Bottom from './components/bottom';
@StoreProvider(AppStore, { debug: __DEV__ })
export default class PurchaseOrder extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    document.title = '购物车-康美日记';
    //初始化数据
    this.store.init();
    this.store.initAdver();
    msg.emit('purchaseNum');
  }

  // componentDidMount() {
  //   {
  //     this.store.state().get('loginFlag') ?

  //     this.store.state().get('cartContent').length >= 30?
  //     this.store.cartContentRun():null
  //      : null
  //   }
  // }
  // componentWillUnmount() {
  //   this.store.cartContentStop();
  // }
  constructor(props) {
    super(props);
  }

  render() {
    const countInvalid = this.store.bigQuery(disableCountQL);
    const edit = this.store.state().get('edit');
    const show = this.store.state().get('show');

    return (
      <div style={styles.container}>
        <CouponMask />
        {show && <PopoverBottom />}
        <Head />
        <PurchaseContainer />
        {countInvalid > 0 &&
          !edit && (
            <Tips
              styles="tips-black"
              iconName="icon-caution"
              text={`有${countInvalid}种商品不符合下单条件，无法下单`}
            />
          )}
        <Bottom />
      </div>
    );
  }
}

const styles = {
  container: {
    backgroundColor: '#fafafa',
    height: 'calc(100vh - 1.07rem)'
  } as any
};
