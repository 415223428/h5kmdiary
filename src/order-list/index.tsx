import React from 'react';
import { fromJS } from 'immutable';
import { IMap, StoreProvider } from 'plume2';

import { ListView, history, Blank } from 'wmkit';

import AppStore from './store';
import Header from './component/header';
import OrderListTop from './component/order-list-top';
import OrderItem from './component/order-list-item';
import OtherOrder from './component/other-order';

import './css/style.css';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class OrderList extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    let state = history.location.state ? history.location.state : null;
    this.store.changeTopActive(state ? state.status : '');
  }

  render() {
    const form = (this.store.state().get('form') || fromJS([])).toJS();
    const fetchOrders = this.store.fetchOrders;
    const headTabKey = this.store.state().get('headTabKey');

    return (
      <div>
        {/*商品订单 or 其他订单*/}
        {/* <Header /> */}

        {/*订单状态tab*/}
        {headTabKey == 0 && <OrderListTop />}

        {/*订单数据*/}
        {headTabKey == 0 && (
          <ListView
            url="/trade/page"
            params={form}
            className="order-env"
            style={{marginTop:".80rem"}}
            renderRow={(order: IMap, index: number) => {
              return <OrderItem order={order} index={index} />;
            }}
            renderEmpty={() => (
              <Blank
                img={require('./img/no-order-form.png')}
                content="您暂时还没有订单哦"
                isToGoodsList={true}
                
              />
            )}
            onDataReached={(res) => fetchOrders(res)}
          />
        )}

        {/*其他订单*/}
        {headTabKey == 1 && <OtherOrder />}
      </div>
    );
  }
}
