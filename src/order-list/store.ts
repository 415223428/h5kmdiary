import { fromJS } from 'immutable';

import { IOptions, Store } from 'plume2';
import { history, Alert, Confirm, _, WMkit } from 'wmkit';
import { config } from 'config';

import OrderListTopActor from './actor/order-list-top-actor';
import FormActor from './actor/order-list-form-actor';
import * as webapi from './webapi';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new OrderListTopActor(), new FormActor()];
  }

  /**
   * 切换顶部tab 商品订单 or 其他订单
   * @param key
   */
  changeTopHeadActive = (key: string) => {
    if (key && this.state().get('headTabKey') === key) {
      return false;
    }
    this.dispatch('top:headActive', key);
  };

  /**
   * 点击订单状态tab
   * @param key tab的key值
   */
  changeTopActive = (key: string) => {
    if (key && this.state().get('key') === key) {
      return false;
    }

    let form = {};
    // tab 页切换
    if (key) {
      const [state, value] = key.split('-');
      form[state] = value;
    }

    // 有邀请人ID并且是店铺内购买，则显示店铺内订单，否则是全部订单
    if (WMkit.inviteeId() && WMkit.isShop()) {
      form['inviteeId'] = WMkit.inviteeId();
      form['channelType'] = WMkit.channelType();
      this.queryShopInfo(WMkit.inviteeId());
    }

    this.getServerTime();

    this.transaction(() => {
      this.dispatch('top:active', key);
      this.dispatch('order-list-form:clearOrders');
      this.dispatch('order-list-form:setForm', fromJS(form));
    });
  };

  /**
   * 查询店铺-小店名称
   * @param distributorId
   * @returns {Promise<void>}
   */
  queryShopInfo = async (distributorId) => {
    const propsRes = (await webapi.queryShopInfo(distributorId)) as any;
    if (propsRes.code == config.SUCCESS_CODE) {
      this.dispatch('order-list-form:shopName', propsRes.context);
    }
  };

  /**
   * 确认取消订单
   * @param tid 订单号
   */
  cancelConfirm = async (tid: string) => {
    const res = await webapi.cancelOrder(tid);
    if (res.code == config.SUCCESS_CODE) {
      Alert({ text: '取消成功', cb: () => location.reload() });
    } else {
      Alert({ text: res.message });
    }
  };

  /**
   * 取消订单
   * @param tid 订单号
   */
  cancelOrder = (tid: string) => {
    Confirm({
      title: '取消订单',
      text: '您确定要取消该订单?',
      cancelBtn: '取消',
      okBtn: '确定',
      confirmCb: () => this.cancelConfirm(tid)
    });
  };

  /**
   * 设置订单列表
   * @param res
   */
  fetchOrders = (res: any) => {
    if (res.context) {
      this.dispatch('order-list-form:setOrders', fromJS(res.context.content));
    }
  };

  /**
   * 申请退单
   * @param tid 订单号
   */
  applyRefund = async (tid: string) => {
    let { message, context } = await webapi.getTradeDetail(tid);

    let tradeDetail = context;

    let errMsg;
    let canApply = false;

    if (tradeDetail) {
      const flowState = tradeDetail['tradeState']
        ? tradeDetail['tradeState']['flowState']
        : '';
      const payState = tradeDetail['tradeState']
        ? tradeDetail['tradeState']['payState']
        : '';
      const deliverStatus = tradeDetail['tradeState']
        ? tradeDetail['tradeState']['deliverStatus']
        : '';

      // 获取该订单所有的待处理及已完成的退单列表
      let orderReturnListRes = await webapi.fetchOrderReturnList(tid);

      if (orderReturnListRes.context) {
        canApply = true;

        // 如果有未处理完的，则不允许再次申请
        (orderReturnListRes.context as any).forEach((v) => {
          if (
            v.returnFlowState != 'REFUNDED' &&
            v.returnFlowState != 'COMPLETED' &&
            v.returnFlowState != 'REJECT_REFUND' &&
            v.returnFlowState != 'REJECT_RECEIVE' &&
            v.returnFlowState != 'VOID'
          ) {
            // 有未处理完的
            canApply = false;
            errMsg = '该订单关联了处理中的退单，不可再次申请';
          }
        });

        // 没有待处理的申请
        if (canApply) {
          // 退款申请，如果有已完成的则不允许再次申请
          if (
            flowState == 'AUDIT' &&
            payState == 'PAID' &&
            deliverStatus == 'NOT_YET_SHIPPED'
          ) {
            (orderReturnListRes.context as any).forEach((v) => {
              // 已完成申请的
              if (v.returnFlowState == 'COMPLETED') {
                canApply = false;
                errMsg = '无可退商品';
              }
            });
          } else {
            if (
              tradeDetail['tradeItems'] &&
              tradeDetail['tradeItems'].filter((v) => v.canReturnNum > 0)
                .length == 0
            ) {
              // 退货申请，如果没有可退商品则不允许申请
              canApply = false;
              errMsg = '无可退商品';
            } else if (tradeDetail['payInfo']['payTypeId'] == '0') {
              // 在线支付需判断退款金额
              let totalApplyPrice = 0;
              (orderReturnListRes.context as any).forEach((v) => {
                // 计算已完成的申请单退款总额
                if (v.returnFlowState == 'COMPLETED') {
                  totalApplyPrice = _.add(
                    totalApplyPrice,
                    v.returnPrice.applyStatus
                      ? v.returnPrice.applyPrice
                      : v.returnPrice.totalPrice
                  );
                }
              });

              if (
                totalApplyPrice >= tradeDetail['tradePrice']['totalPrice'] &&
                tradeDetail['tradePrice']['totalPrice'] !== 0
              ) {
                canApply = false;
                errMsg = '无可退金额';
              }
            }
          }
        }
      } else {
        errMsg = '系统异常';
      }
    } else {
      errMsg = message;
    }

    //  可以申请，进入申请页面，否则提示错误信息
    if (canApply) {
      // 已完结订单，则为退货申请，否则认为是退款申请
      let isReturn =
        tradeDetail['tradeState'].flowState == 'COMPLETED' ? true : false;

      // 退货，则进入退货申请页面，否则进入退款申请页面
      if (isReturn) {
        history.push(`/returnsFirst/${tid}`);
      } else {
        history.push(`/applyRefund/${tid}`);
      }
    } else {
      Alert({ text: errMsg });
    }
  };

  /**
   * 0元订单默认支付
   */
  defaultPay = async (tid: string) => {
    const { message, code } = await webapi.defaultPaylOrder(tid);
    if (code == 'K-000000') {
      //跳转到付款成功页
      history.push({
        pathname: `/fill-payment-success/${tid}`
      });
    } else {
      Alert({ text: message });
    }
  };

  /**
   * 订单可用操作按钮
   */
  getOpeBtnArr = (order) => {
    let orderButtons = {
      available: [],
      id: ''
    };

    if (order) {
      const flow = order.getIn(['tradeState', 'flowState']);
      const pay = order.getIn(['tradeState', 'payState']);

      //取消订单
      const cancelButtons = [['INIT', 'NOT_PAID'], ['AUDIT', 'NOT_PAID'],['GROUPON', 'NOT_PAID']];
      //付款
      const payButtons = [
        ['AUDIT', 'NOT_PAID'],
        ['DELIVERED_PART', 'NOT_PAID'],
        ['DELIVERED', 'NOT_PAID'],
        ['CONFIRMED', 'NOT_PAID'],['GROUPON', 'NOT_PAID']
      ];
      //确认收货
      const confirmButtons = [
        ['DELIVERED', 'NOT_PAID'],
        ['DELIVERED', 'PAID'],
        ['DELIVERED', 'UNCONFIRMED']
      ];
      //退货退款
      let canReturnFlag = order.get('canReturnFlag');

      if (order.getIn(['tradeState', 'flowState']) == 'GROUPON') { 
        // 待成团单，不展示退货退款
        canReturnFlag = false;
      }

      let availables = Array();
      if (
        payButtons.filter((button) => this._calc(button)(flow, pay)).length > 0
      ) {
        availables.push('去付款');
      }
      if (
        confirmButtons.filter((button) => this._calc(button)(flow, pay))
          .length > 0
      ) {
        availables.push('确认收货');
      }
      if (
        cancelButtons.filter((button) => this._calc(button)(flow, pay)).length >
        0
      ) {
        availables.push('取消订单');
      }
      if (canReturnFlag) {
        availables.push('退货退款');
      }

      orderButtons['available'] = availables;
      orderButtons['id'] = order.get('id');
    }
    return orderButtons;
  };

  /**
   * 计算订单有效按钮
   */
  _calc = (button: Array<string>) => {
    return function(flow: string, pay: string) {
      return button[0] === flow && button[1] === pay;
    };
  };

  getServerTime = async() =>{
    //获取服务时间
    const serverTime = await webapi.getServerTime();
    //存储服务时间
    this.dispatch('order-list-form:serverTime', serverTime);
  }
}
