import React from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';
import { noop, WMkit } from 'wmkit';
@Relax
export default class Header extends React.Component<any, any> {
  props: {
    relaxProps?: {
      headTabKey: number;
      changeTopHeadActive: Function;
    };
  };

  static relaxProps = {
    headTabKey: 'headTabKey',
    changeTopHeadActive: noop
  };

  render() {
    const { headTabKey, changeTopHeadActive } = this.props.relaxProps;
    return (
      <div style={{ height: 'rem' }}>
        <div className="other-header">
          <div style={{ color: '#ffffff' }}>我的订单</div>
          {/*{WMkit.isShop() ? (
            <div style={{ color: '#ffffff' }}>我的订单</div>
          ) : (
            <div className="outer">
              <div
                className={headTabKey == 0 ? 'titleWrap2 active' : 'titleWrap2'}
                onClick={() => changeTopHeadActive(0)}
              >
                商品订单
              </div>
              <div
                className={headTabKey == 1 ? 'titleWrap active' : 'titleWrap'}
                onClick={() => changeTopHeadActive(1)}
              >
                其他订单
              </div>
            </div>
          )}*/}
        </div>
      </div>
    );
  }
}
