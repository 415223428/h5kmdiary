import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Relax } from 'plume2';

import { noop, history, _, storage, WMkit, CountDown } from 'wmkit';
import { cache } from 'config';
import { IList } from 'typings/globalType';
import moment from 'moment';
import '../css/order.css';
const noneImg = require('../img/none.png');

/**
 * 订单状态
 * @type {{INIT: string; GROUPON: string; AUDIT: string; DELIVERED_PART: string; DELIVERED: string; CONFIRMED: string; COMPLETED: string; VOID: string}}
 */
const flowState = (status, payState) => {
  if (status == 'INIT') {
    return '待审核';
  } else if (status == 'GROUPON') {
    // 是拼团订单 根据支付状态 ? 待支付 : 待发货
    if (payState == 'NOT_PAID') {
      return '待支付';
    } else if (payState == 'UNCONFIRMED') {
      return '待确认';
    } else if (payState == 'PAID') {
      return '待发货';
    }
  } else if (status == 'AUDIT' || status == 'DELIVERED_PART') {
    if(payState == 'NOT_PAID'){
      return '待付款';
    }else if (payState == 'PAID') {
      return '待发货';
    }
  } else if (status == 'DELIVERED') {
    return '待收货';
  } else if (status == 'CONFIRMED') {
    return '已收货';
  } else if (status == 'COMPLETED') {
    return '已完成';
  } else if (status == 'VOID') {
    return '已作废';
  }
};

@Relax
export default class OrderListItem extends Component<any, any> {
  props: {
    relaxProps?: {
      getOpeBtnArr: Function;
      orders: IList;
      cancelOrder: Function;
      applyRefund: Function;
      defaultPay: Function;
      inviteeShopName: string;
      serverTime: any;
    };
    order: any;
    index: number;
  };

  static relaxProps = {
    getOpeBtnArr: noop,
    orders: 'orders',
    cancelOrder: noop,
    applyRefund: noop,
    defaultPay: noop,
    inviteeShopName: 'inviteeShopName',
    serverTime: 'serverTime'
  };

  render() {
    let { order, index } = this.props;
    let {
      getOpeBtnArr,
      orders,
      inviteeShopName,
      serverTime
    } = this.props.relaxProps;
    const opeBtnArr = getOpeBtnArr(orders.get(index));
    const gifts = order.gifts || [];
    const endTime = order.orderTimeOut ? moment(order.orderTimeOut) : null;

    return (
      <div className="ships">
         {/* order-list-box */}
        <div className="order-item order-list-box" style={{borderTop:"0px",background:'#fff',border: 'none'}}>
          <Link to={`/order-detail/${order.id}`}>
            <div className="order-head">
              {WMkit.inviteeId() && WMkit.isShop() ? (
                <div className="order-head-num">
                  <span>买</span>
                  <div className="order-storeName">
                    <Link to={`/shop-index-c/${order.inviteeId}`}>
                      {order.storeBagsFlag == 1
                        ? inviteeShopName
                        : `${order.distributorName}的${order.shopName}`}
                    </Link>
                  </div>
                </div>
              ) : (
                <div className="order-head-num ">
                  {/* <span>{order.platform == 'CUSTOMER' ? '订' : '代'}</span> */}
                  <img src={require("../img/order-form.png")} alt="" className="order-form-icon"></img>
                  <div className="order-storeName">
                    {order.supplier.isSelf == true ? (
                      <div className="self-sales">自营</div>
                    ) : null}
                    <Link to={`/store-main/${order.supplier.storeId}`}>
                      {order.supplier.storeName}
                    </Link>
                  </div>
                </div>
              )}
              <div className="status" style={{color:"rgba(153,153,153,1)"}}>
                {flowState(
                  order.tradeState.flowState,
                  order.tradeState.payState
                )}
              </div>
            </div>
            <div className="limit-img ship-img">
              <div className="img-content order">
                {order.tradeItems
                  .concat(gifts)
                  .filter((val, index) => index < 4)
                  .map((item) => (
                    <img
                      className="img-item"
                      src={item.pic ? item.pic : noneImg}
                    />
                  ))}
              </div>
              <div className="right-context">
                <div className="total-num">
                  共{order.tradeItems.concat(gifts).length}种
                </div>
                <i className="iconfont icon-jiantou1" />
              </div>
            </div>
          </Link>
          <div className="bottom">
            <div className="price">
              <div>
                <i className="iconfont icon-qian" />
                {_.addZero(order.tradePrice.totalPrice)}
                {/*{!WMkit.isShop() &&
                  !WMkit.inviteeId() &&
                  order.inviteeId &&
                  order.buyer.id == order.inviteeId &&
                  order.commission > 0 && (
                    <span className="commission">
                      /&nbsp;预计返利{_.addZero(order.commission)}
                    </span>
                  )}*/}
              </div>
              {endTime &&
              order.tradeState.payState == 'NOT_PAID' &&
              order.tradeState.flowState != 'VOID' && order.tradeState.auditState != 'NON_CHECKED' ? (
                <CountDown
                  visible={endTime && serverTime}
                  timeOffset={moment
                    .duration(endTime.diff(serverTime))
                    .asSeconds()
                    .toFixed(0)}
                  colorStyle={{
                    marginTop:'.19rem',
                    color: '#666',
                    fontSize:'.26rem',
                    justifyContent: 'flex-start'
                  }}
                  timeStyle={{
                    color: '#FF4D4D',
                    fontSize:'.28rem'
                  }}
                  showTimeDays={true}
                  labelText={'支付倒计时:'}
                />
              ) : null}
            </div>
            <div className="botton-box">
              {opeBtnArr
                ? opeBtnArr.available.map((availableButton) => {
                    return (
                      <div
                        className={
                          availableButton == '去付款'
                            ? 'btn btn-ghost-red btn-small order-list-btn-red'
                            : 'btn btn-ghost btn-small order-list-btn'
                        }
                        onClick={() => {
                          this._operationButtons(
                            order.id,
                            availableButton,
                            order.payInfo.payTypeId
                          );
                          (window as any).openqiyu(
                            
                          )
                        }}
                      >
                        {availableButton}
                      </div>
                    );
                  })
                : null}
            </div>
          </div>
        </div>
        {/* <div className="bot-line" /> */}
      </div>
    );
  }

  /**
   * 订单按钮event handler
   */
  _operationButtons = async (tid, button, payTypeId) => {
    let { order } = this.props;
    const { cancelOrder, applyRefund, defaultPay } = this.props.relaxProps;
    //0元订单去付款直接跳转到付款成功页
    if (button == '去付款' && order.tradePrice.totalPrice == 0) {
      //直接调默认支付接口
      defaultPay(tid);
    }
    if (
      button == '去付款' &&
      payTypeId == 0 &&
      order.tradePrice.totalPrice != 0
    ) {
      history.push(`/pay-online/${tid}`);
    } else if (
      button == '去付款' &&
      payTypeId == 1 &&
      order.tradePrice.totalPrice != 0
    ) {
      this._clearCache();
      history.push({
        pathname: `/fill-payment/${tid}`,
        search: 'from=order-detail'
      });
    } else if (button == '取消订单') {
      cancelOrder(tid);
    } else if (button == '确认收货') {
      history.push(`/ship-record/${tid}/0`);
    } else if (button == '退货退款') {
      applyRefund(tid);
    }
  };

  _clearCache = async () => {
    storage('session').del(cache.PAYMENT_ENCLOSES);
    storage('session').del(cache.PAYMENT_REMARK);
    storage('session').del(cache.PAYMENT_TIME);
  };
}
