import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMkit } from 'wmkit';
import { msg } from 'plume2';

export default class OtherOrder extends React.Component<any, any> {
  render() {
    return (
      <div>
        <div className="active-order">
          <span className="title">活动订单</span>
          <ul className="order-list">
            <li>
              <div
                className="order-box"
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        history.push('/group-order-list');
                      }
                    });
                  } else {
                    history.push('/group-order-list');
                  }
                }}
              >
                <i className="iconfont icon-icontuan i1" />
                <span className="text">拼团订单</span>
              </div>
              <div
                className="order-box"
                onClick={() => {
                  if (!WMkit.isLoginOrNotOpen()) {
                    msg.emit('loginModal:toggleVisible', {
                      callBack: () => {
                        history.push('/points-order-list');
                      }
                    });
                  } else {
                    history.push('/points-order-list');
                  }
                }}
              >
                <i className="iconfont icon-jifen1 i2" />
                <span className="text">积分订单</span>
              </div>
            </li>
            <li>
              <div
                className="order-box"
                onClick={() => history.push('/not-develop')}
              >
                <i className="iconfont icon-kanjia i3" />
                <span className="text">砍价订单</span>
              </div>
              <div
                className="order-box"
                onClick={() => history.push('/not-develop')}
              >
                <i className="iconfont icon-draw-lottery i4" />
                <span className="text">抽奖订单</span>
              </div>
            </li>
          </ul>
        </div>
        <div className="active-order">
          <span className="title">服务订单</span>
          <ul className="order-list">
            <li>
              <div
                className="order-box"
                onClick={() => history.push('/not-develop')}
              >
                <i className="iconfont icon-kecheng i1" />
                <span className="text">课程订单</span>
              </div>
              <div
                className="order-box"
                onClick={() => history.push('/not-develop')}
              >
                <i className="iconfont icon-zu i2" />
                <span className="text">租赁订单</span>
              </div>
            </li>
            <li>
              <div
                className="order-box"
                onClick={() => history.push('/not-develop')}
              >
                <i className="iconfont icon-anzhuang i3" />
                <span className="text">安装订单</span>
              </div>
              <div
                className="order-box"
                onClick={() => history.push('/not-develop')}
              >
                <i className="iconfont icon-shezhi i4" />
                <span className="text">维修订单</span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
