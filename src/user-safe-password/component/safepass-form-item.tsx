import React from 'react'
import { Link } from 'react-router-dom'
import { Relax } from "plume2";
import { noop, Button, FormInput, Alert, WMkit, FormItem, ValidConst } from 'wmkit'
import AppStore from '../store'
const TimerButton = Button.Timer
const next= require('../img/next.png')

@Relax
export default class SafeFormItem extends React.Component<any, any> {
  store: AppStore


  props: {
    relaxProps?: {
      code: string,
      mobile: string,
      buttonValue: string,
      getMobile: Function,
      getCode: Function,
      doNext: Function,
      sendCode: Function
    }
  };


  static relaxProps = {
    code: 'code',
    mobile: 'mobile',
    buttonValue: 'buttonValue',
    getMobile: noop,
    getCode: noop,
    sendCode: noop,
    doNext: noop
  }


  constructor(props: any) {
    super(props)
  }


  render() {
    const { mobile, code, getMobile, getCode, buttonValue, sendCode, doNext } = this.props.relaxProps
    return (
      <div className="safePass-box" style={{ width: '100%', height: '100vh', background: '#FAFAFA' }}>
        {/*如果已登录，则是修改密码，否则是忘记密码*/}
        {/* {
          WMkit.isLogin() ? <FormItem labelName="手机号" placeholder={mobile} /> :
            <FormInput label="手机号" type="number" placeHolder="请输入您的手机号" pattern="[0-9]*" defaultValue={mobile}
              onChange={(e) => getMobile(e.target.value)} maxLength={11} />
        } */}
        {
          WMkit.isLogin() ?
            <div className="safePass-item">
              <div className="inputBox">
                {/* <img src={require('../img/phone.png')} alt="" /> */}
                <label htmlFor="" style={{ marginLeft: '.5rem' }}>{mobile}</label>
                {/* <input
                  className="formInput"
                  placeholder={mobile}
                  value={mobile}
                /> */}
              </div>
            </div> :
            <div className="safePass-item">
              <div className="inputBox">
                {/* <img src={require('../img/phone.png')} alt="" /> */}
                <input
                  className="formInput"
                  type="number"
                  pattern="[0-9]*"
                  placeholder="请输入您的手机号"
                  value={mobile}
                  maxLength={11}
                  onChange={(e) => getMobile(e.target.value)}
                />
              </div>
            </div>
        }
        {/* <FormInput label="验证码" type="number" placeHolder="请输入验证码" pattern="[0-9]*" defaultValue={code}
          onChange={(e) => getCode(e.target.value)} maxLength={6} other={<TimerButton text={buttonValue}
            shouldStartCountDown={() => this._sendCode(mobile)}
            resetWhenError
            onClick={() => sendCode()} />} /> */}
        <div className="safePass-item">
          <div className="inputBox eyes-box" style={{ paddingRight: 0 }}>
            <input
              className="formInput sendCodeInput"
              type="text"
              placeholder="请输入短信验证码"
              pattern="/^[0-9]{6}$/"
              value={code}
              onChange={(e) => {
                // if (!(/^[0-9]+$/.test(e.target.value))) {
                //   return;
                // }
                getCode(e.target.value)
              }}
              maxLength={6}
            />
            <span style={{ position: 'absolute', right: '.4rem' }}>
              <TimerButton
                text="获取验证码"
                resetWhenError={true}
                shouldStartCountDown={() => this._sendCode(mobile)}
                onClick={() => sendCode()}
                defaultStyle={{
                  fontSize: '0.26rem',
                  padding: '0.11rem',
                  fontFamily: 'PingFang SC',
                  fontWeight: '500',
                  color: 'rgba(255,77,77,1)',
                  border: '1px solid rgba(255, 77, 77, 1)',
                  borderRadius: '4px',
                  // margin: 'auto .4rem auto auto',
                }}
              />
            </span>

          </div>
        </div>
        <div>
          <div className="register-tips">
            <p>提示：</p>

            <p>1.为了保障您的账户信息安全，在您变更账户重要信息时，需要对您的身份进行验证。感谢您的理解和支持!</p>

            <p>2.如出现收不到短信的情况，可能是由于通信网络异常造成，请您稍后重新尝试操作！</p>
          </div>

          <div className="safePass-btn">
            {/* <button className="btn btn-primary myBtn" onClick={() => doNext()}>下一步
          </button> */}
            <img src={next} alt="" style={{width:'100%'}}  onClick={() => doNext()}/>
          </div>
        </div>
        <div className="footer-copyright">
          <p>粤ICP备19120716号-1</p>
        </div>
      </div>
    )
  }


  _sendCode = (tel: string) => {
    const regex = ValidConst.phone
    if (tel == "") {
      Alert({
        text: "请填写手机号！"
      })
      return false
    } else if (!regex.test(tel)) {
      Alert({
        text: "无效的手机号！"
      })
      return false
    } else {
      return true
    }
  }
}
