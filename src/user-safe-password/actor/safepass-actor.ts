/**
 * Created by feitingting on 2017/7/20.
 */
import {Actor, Action} from "plume2";
export default class SafePassActor extends Actor {


  defaultState() {
    return {
      mobile: "",
      code: "",
      buttonValue: "发送验证码"
    }
  }


  /**
   * 手机号码状态值
   * @param state
   * @param mobile
   */
  @Action('safepass:mobile')
  getMobile(state, mobile: string) {
    return state.set('mobile', mobile)
  }


  /**
   * 验证码状态值
   * @param state
   * @param code
   */
  @Action('safepass:code')
  getCode(state, code: string) {
    return state.set('code', code)
  }
}
