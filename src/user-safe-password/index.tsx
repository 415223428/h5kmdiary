import React from 'react'
import { StoreProvider } from 'plume2'
import AppStore from './store'
import FormItem from './component/safepass-form-item'
const styles = require('./css/style.css');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class UserSafePassword extends React.Component<any, any> {
  store: AppStore;

  componentDidMount() {
    this.store.init()
  }

  render() {
    return (
      <div className="content register-content">
        <FormItem />
        {/* <div className="register-btn">
          <button className="btn btn-primary" onClick={() => {
            this.store.doNext()
          }}>下一步
          </button>
        </div> */}
      </div>
    )
  }
}
