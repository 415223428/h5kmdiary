import {Store} from 'plume2';
import {fromJS} from 'immutable'
import {Alert, history, ValidConst, WMkit} from 'wmkit'
import * as webapi from './webapi'
import {cache, config} from 'config';
import  SafePassActor from './actor/safepass-actor'


/**
 * 手机号码校验*/
function _testTel(tel: string) {
  const regex = ValidConst.phone
  if (tel == "") {
    Alert({
      text: '请填写手机号！'
    })
    return false
  }
  else if (!regex.test(tel)) {
    Alert({
      text: '无效的手机号！'
    })
    return false
  } else {
    return true
  }
}


export default class AppStore extends Store {


  bindActor() {
    return [
      new SafePassActor
    ]
  }


  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }


  /**
   * 初始化，判断当前是否是登录状态
   * 登录，修改密码，查询当前手机号，否则，忘记密码
   * @returns {Promise<void>}
   */
  init = async () => {
    if (WMkit.isLogin()) {
      const res = await webapi.findCustomerMobile()
      if (res.code == config.SUCCESS_CODE) {
        this.dispatch("safepass:mobile", fromJS(res.context).get("customerAccount"))
      }
    }
  };


  getMobile = (value: string) => {
    this.dispatch('safepass:mobile', value)
  }


  getCode = (value: string) => {
    this.dispatch('safepass:code', value)
  }


  sendCode = async () => {
    const mobile = this.state().get('mobile')
    return webapi.sendCode(mobile, !WMkit.isLogin()).then(res => {
      const {code, message} = res
      if (code == "K-000000") {
        Alert({
          text: '验证码已发送，请注意查收！'
        })
      }
      else {
        Alert({
          text: message
        })
        return Promise.reject(message)
      }
    })
  }


  doNext = async () => {
    const mobile = this.state().get('mobile')
    const verticode = this.state().get('code')
    if (_testTel(mobile)) {
      if (verticode == "") {
        Alert({
          text: "请填写验证码！"
        })
        return false
      }
      else {
        const {code, message, context} = await webapi.testCode(mobile, verticode, !WMkit.isLogin())
        if (code == "K-000000") {
          /**验证码校验成功，跳转到下一个页面*/
          history.push({
            pathname: '/usersafe-password-next',
          })
          /**验证码和customerId写入localstorage*/
          localStorage.setItem(cache.FORGET_CODE, verticode)
          localStorage.setItem(cache.CUSTOMER_ID, context as any)
        } else {
          Alert({
            text: message
          })
          return false
        }
      }
    } else {

    }
  }
}
