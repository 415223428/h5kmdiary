import  {Fetch} from 'wmkit'

export const sendCode = (tel: string, isForgetPassword: boolean) => {
  return Fetch('/checkSmsByForgot', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: tel,
      isForgetPassword:isForgetPassword
    })
  })
}


export const testCode = (tel: string, code: string, isForgetPassword: boolean) => {
  return Fetch('/validateSmsByForgot', {
    method: 'POST',
    body: JSON.stringify({
      customerAccount: tel,
      verifyCode: code,
      isForgetPassword:isForgetPassword
    })
  })
}


/**
 * 会员中心查询会员绑定的手机号
 * @returns {Promise<Result<ReturnResult>>}
 */
export const findCustomerMobile = () => {
  return Fetch(`/customer/customerMobile`)
}