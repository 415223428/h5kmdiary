import React from 'react';
import { Link } from 'react-router-dom';
import { history, WMkit } from 'wmkit';
import { msg } from 'plume2';
import { url } from 'config';
const style = require('./css/style.css');

export default class MemberFooter extends React.Component<any, any> {
  props: {
    path: string;
    pointsIsOpen: boolean;
  };

  render() {
    const isLogin = WMkit.isLoginOrNotOpen();
    return (
      <div className="member-footer">
        <div
          className="footer-nav"
          onClick={() => {
            if (isLogin) {
              history.push('/member-center');
            } else {
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  history.push('/member-center');
                }
              });
            }
          }}
        >
          <div
            className={
              this.props.path == url.MEMBER_CENTER
                ? 'l-icon1 active'
                : 'l-icon1'
            }
          />
          <span
            className={
              this.props.path == url.MEMBER_CENTER ? 'text active' : 'text'
            }
          >
            会员中心
          </span>
        </div>
        {this.props.pointsIsOpen && (
          <div
            className="footer-nav"
            onClick={() => history.push('/points-mall')}
          >
            <div
              className={
                this.props.path == url.POINTS_MALL
                  ? 'l-icon2 active'
                  : 'l-icon2'
              }
            />
            <span
              className={
                this.props.path == url.POINTS_MALL ? 'text active' : 'text'
              }
            >
              积分商城
            </span>
          </div>
        )}
        <div
          className="footer-nav"
          onClick={() => history.push('/user-center')}
        >
          <div
            className={
              this.props.path == url.USER_CENTER ? 'l-icon2 active' : 'l-icon2'
            }
          />
          <span
            className={
              this.props.path == url.USER_CENTER ? 'text active' : 'text'
            }
          >
            我的
          </span>
        </div>
        {/*<div className="footer-nav">*/}
        {/*<div className="l-icon3" />*/}
        {/*<span className="text">商城PLUS</span>*/}
        {/*</div>*/}
      </div>
    );
  }
}
