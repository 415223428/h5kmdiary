import { Fetch, WMkit } from 'wmkit';

/**
 * 获取用户的搜索历史
 * @returns {Promise<Result<any>>}
 */
export const getHistory = (id: number) => {
  return Fetch(`/goods/store/history/${id}`, {
    method: 'GET'
  });
};

/**
 * 添加一条搜索历史
 * @param queryString
 * @returns {Promise<Result<any>>}
 */
export const addHistory = (queryString: string, id: number) => {
  return Fetch(`/goods/store/history/${id}`, {
    method: 'POST',
    body: JSON.stringify({ keyword: queryString })
  });
};

/**
 * 清空搜索历史
 * @returns {Promise<Result<any>>}
 */
export const clearHistory = (id: number) => {
  return Fetch(`/goods/store/history/${id}`, {
    method: 'DELETE'
  });
};

/**
 * 获取店铺基本信息
 * @param id 店铺Id
 */
export const fetchStoreInfo = (id) => {
  const params = {
    storeId: id
  };
  return WMkit.isLoginOrNotOpen()
    ? Fetch(`/store/storeInfo`, {
        method: 'POST',
        body: JSON.stringify(params)
      })
    : Fetch(`/store/unLogin/storeInfo`, {
        method: 'POST',
        body: JSON.stringify(params)
      });
};
