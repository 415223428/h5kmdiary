import React from 'react'
import { StoreProvider } from "plume2"
import SearchBar from './component/search-bar'
import AppStore from './store'


/**
 * 商品搜索
 */
@StoreProvider(AppStore, { debug: __DEV__ })
export default class StoreGoodsSearch extends React.Component<any, any> {

  store: AppStore

  // 商品列表过来可能会有关键字，有的话展示在搜索框内
  _queryString: ''


  constructor(props) {
    super(props)
    this._queryString = this.props.location.state && this.props.location.state.queryString
  }


  componentWillMount() {
    const { sid } = this.props.match.params
    // 查询浏览记录
    this.store.getHistory(sid)
  }


  render() {
    const { sid } = this.props.match.params
    return (
      <div className="content">
        <SearchBar storeId={sid} queryString={this._queryString} />
      </div>
    )
  }
}
