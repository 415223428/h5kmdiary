import { msg, Store } from 'plume2';
import { fromJS } from 'immutable';
import { config } from 'config';
import * as webapi from './webapi';
import SearchActor from './actor/search-actor';
import { WMkit } from 'wmkit';
export default class AppStore extends Store {
  bindActor() {
    return [new SearchActor()];
  }

  constructor(props) {
    super(props);
    if (__DEV__) {
      //debug
      (window as any)._store = this;
    }
  }

  /**
   * 查询搜索历史
   * @returns {Promise<void>}
   */
  getHistory = async (id: number) => {
    if (WMkit.isLoginOrNotOpen()) {
      const res = await webapi.getHistory(id);
      if (res.code === config.SUCCESS_CODE) {
        this.dispatch('search:history', fromJS(res.context).reverse());
      }

      const storeRes = (await webapi.fetchStoreInfo(id)) as any;
      if (storeRes.code == 'K-000000') {
        /**店铺pv/uv埋点*/
        (window as any).myPvUvStatis(null, storeRes.context.companyInfoId);
      } else {
        msg.emit('storeCloseVisible', true);
      }
    }
  };

  /**
   * 添加搜索记录
   * @param {string} queryString
   */
  addHistory = (queryString: string, id: number) => {
    if (WMkit.isLoginOrNotOpen()) {
      webapi.addHistory(queryString, id);
    }
  };

  /**
   * 清除搜索记录
   * @returns {Promise<void>}
   */
  clearHistory = async (id: number) => {
    if (WMkit.isLoginOrNotOpen()) {
      const res = await webapi.clearHistory(id);
      if (res.code === config.SUCCESS_CODE) {
        this.dispatch('search:history', fromJS([]));
      }
    }
  };
}
