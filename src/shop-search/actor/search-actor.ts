import { Action, Actor, IMap } from "plume2";


export default class SearchActor extends Actor {

  defaultState() {
    return {
      // 搜索历史
      searchHistory: []
    }
  }


  /**
   * 
   * @param state 搜索历史
   * @param searchHistory 
   */
  @Action('search:history')
  initHistory(state: IMap, searchHistory: string[]) {
    return state.set('searchHistory', searchHistory)
  }
}
