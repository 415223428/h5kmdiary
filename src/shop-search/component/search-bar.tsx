import React from 'react'
import { Link } from 'react-router-dom'
import { Relax } from "plume2"
import { history, noop } from 'wmkit'

@Relax
export default class SearchBar extends React.Component<any, any> {

  _input: any
  // 点击取消后定时500ms。如果页面没有正常跳转，跳转到首页。如果页面正常跳转了，取消定时器。用于处理直接打开搜索页面后点击取消返回的问题
  _timer: any


  props: {
    storeId: number
    relaxProps?: {
      searchHistory: any
      addHistory: Function
      clearHistory: Function
    }
    queryString: string
  }


  static relaxProps = {
    searchHistory: 'searchHistory',
    addHistory: noop,
    clearHistory: noop,
  }


  state: {
    queryString: string
  }

  constructor(props) {
    super(props)
    this.state = {
      queryString: ''
    }
  }


  componentDidMount() {
    this.setState({
      queryString: this.props.queryString || ''
    })
    this._input.focus()
  }


  componentWillUnmount() {
    if (this._timer) {
      clearTimeout(this._timer)
    }
  }


  render() {

    const { searchHistory } = this.props.relaxProps

    return (
      <div className="search-page">
        <div className="search-container">
          <div className="search-box">
            <div className="input-box">
              <i className="iconfont icon-sousuo" onClick={this._goSearch}></i>
              <form action="javascript:;">
                <input
                  autoFocus={true}
                  type="search"
                  placeholder="搜索商品"
                  value={this.state.queryString}
                  maxLength={100}
                  ref={input => this._input = input}
                  onChange={this._inputOnChange}
                  onKeyDown={(e) => {
                    if (e.which == 13) {
                      this._goSearch()
                    }
                  }} />
              </form>
            </div>
            <div>
              <a href="javascript:;" className="search-text" onClick={() => {
                history.goBack()
                this._timer = setTimeout(() => {
                  // 如果500ms后页面还没有跳转，那么就去首页
                  history.replace('/main')
                }, 500)
              }}>取消</a>
            </div>
          </div>
        </div>

        <div className="search-content">
          <div className="search-title">
            <span>搜索历史</span>
            <a href="javascript:;" onClick={this._clearSearchHistory}>清除</a>
          </div>
          <div className="history-list">
            {
              searchHistory.count() > 0 ?
                searchHistory.toJS().map(s => {
                  return <div className="item" onClick={() => {
                    this._clickHistory(s)
                  }}>{s}</div>
                })
                : <span className="no-history">暂无搜索记录</span>
            }

          </div>
        </div>
      </div>
    )
  }


  /**
   * 搜索框内容变化
   * @param e
   * @private
   */
  _inputOnChange = e => {
    this.setState({
      queryString: e.target.value
    })
  }


  /**
   * 进行搜索
   * @private
   */
  _goSearch = () => {
    const { storeId } = this.props
    // 关键字
    let queryString = this.state.queryString.trim()
    if (!queryString) {
      // 不带搜索参数跳转商品列表页
      history.push(`/store-goods-list/${storeId}`)
    } else {
      // 处理历史搜索记录
      this._handleSearchHistory(queryString)
      // 带搜索参数跳转商品列表页
      history.push(`/store-goods-list/${storeId}?q=${encodeURIComponent(queryString)}`)
    }
  }


  /**
   * 点击搜索历史记录中的一条
   * @param queryString
   * @private
   */
  _clickHistory = (queryString) => {
    this.setState({
      queryString
    }, this._goSearch)
  }


  /**
   * 处理搜索历史
   * @param queryString
   * @private
   */
  _handleSearchHistory = (queryString) => {
    const { addHistory } = this.props.relaxProps
    const { storeId } = this.props
    addHistory(queryString, storeId)
  }


  /**
   * 清除历史记录
   * @private
   */
  _clearSearchHistory = () => {
    const { clearHistory } = this.props.relaxProps
    const { storeId } = this.props
    clearHistory(storeId)
  }


}
