/**
 * Created by chenpeng on 2017/7/22.
 */
import {Store} from "plume2";

import SellerAccountActor from "./actor/seller-account-actor";
import {fetchSellerAccounts} from "./webapi";

export default class AppStore extends Store {

	bindActor() {
		return [new SellerAccountActor()]
	}


	/**
	 * 初始化卖家账号
	 */
	init = async () => {
		const res = await fetchSellerAccounts()
		this.dispatch('seller-account-actor:setAccounts', res.context)
	}
}
