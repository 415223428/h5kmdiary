import React,{Component} from "react";
import {StoreProvider} from "plume2";

import AppStore from "./store";

import AccountForm from "./component/account-form";

@StoreProvider(AppStore ,{debug: __DEV__})
export default class SellerAccount extends Component<any, any> {
	store: AppStore

	componentDidMount() {
		this.store.init()
	}

	render() {

		return(
			<div className="content register-content">
				<AccountForm/>
			</div>
		)
	}

}