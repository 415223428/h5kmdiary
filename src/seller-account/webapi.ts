/**
 * Created by chenpeng on 2017/7/22.
 */
import {Fetch} from "wmkit";


/**
 * 获取卖家账号
 */
export const fetchSellerAccounts = () => {
	return Fetch(`/account/offlineValidAccounts`)
}