/**
 * Created by chenpeng on 2017/7/22.
 */
import {Action, Actor, IMap} from "plume2";
import {List, fromJS} from 'immutable'

export default class SellerAccountActor extends Actor {

	defaultState() {
		return {
			sellerAccounts: List()
		}
	}


	/**
	 * 设置卖家账号
	 */
	@Action('seller-account-actor:setAccounts')
	setAccounts(state: IMap, accounts: Array<any>) {
		return state.set('sellerAccounts', fromJS(accounts))
	}

}