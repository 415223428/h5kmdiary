import React,{Component} from "react";
import {Link} from 'react-router-dom'
import parse from 'url-parse'
import {List} from 'immutable'
import {Relax} from "plume2";

import {history, storage} from 'wmkit'
import {cache} from 'config'

const styles = require('../css/style.css');

@Relax
export default class AccountForm extends Component<any, any> {

	props: {
		relaxProps ?: {
			sellerAccounts: List<any>
		}
	}


	static relaxProps = {
		sellerAccounts: 'sellerAccounts'
	}


	render() {

		let {sellerAccounts} = this.props.relaxProps
		let url = parse(history.location.search, true)
		let query = url.query
		let { tid, from } = query

		return (
			<div className={styles.cardBox}>
				{
					sellerAccounts? sellerAccounts.toJS().map((account) => {
						return (
							<div className={styles.card} onClick={ () => this._selectSellerAccount(tid, from, account)}>
								<p className={styles.title}>{account.bankName}</p>
								<p className={styles.name}>{account.accountName}</p>
								<div className={styles.acount}>
									<p className={styles.num}>{account.bankNo}</p>
								</div>
							</div>
						)
					}) : null
				}
			</div>
		)
	}


	/**
	 * 选择卖家账号
	 */
	_selectSellerAccount = (tid: string, from: string, account: object) => {
		storage('session').set(cache.SELLER_ACCOUNT, account)

		history.push({
			pathname: `/fill-payment/${tid}`,
			search:`?from=${from}`,
		})
	}
}