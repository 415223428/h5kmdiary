import React from 'react';
import { StoreProvider } from 'plume2';
import { Button } from 'wmkit';
import AppStore from './store';
import ShipList from './component/ship-list';

const SmallBlueButton = Button.SmallBlue;

@StoreProvider(AppStore, { debug: __DEV__ })
export default class ShipRecord extends React.Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const { tid, type } = this.props.match.params;
    this.store.init(tid, type);
    this.store.orderId(tid);
  }

  constructor(props) {
    super(props);
  }

  render() {
    const status = this.store.state().get('deliveryStatus');
    return (
      <div>
        {/* <div>
          <ShipList />
          <div style={{ height: 48 }}>
            <div className={status ? 'order-button-wrap' : 'hide'}>
              <SmallBlueButton
                text={'确认收货'}
                onClick={() =>
                  this.store.onConfirm(this.props.match.params.tid)
                }
              />
            </div>
          </div>
        </div> */}
        <div className={status ? 'order-button-wrap' : 'hide'}
          style={{
            height: ".98rem",
            background: "linear-gradient(135deg,rgba(255,106,77,1),rgba(255,26,26,1))",
            color: "rgba(255,255,255,1)",
            display: "flex",
            justifyContent: "center"
          }}
          onClick={() => this.store.onConfirm(this.props.match.params.tid)}>
          {/* <SmallBlueButton text={'确认收货'} onClick={() => this.store.onConfirm(this.props.match.params.tid)}/> */}
          <div>
            确认收货
              </div>
        </div>
        <div />
      </div>
    );
  }
}
