import {Action, Actor, IMap} from 'plume2';
import {fromJS} from 'immutable';

/**
 * Created by yinxianzhi on 2019/05/10.
 */
export default class PointsMallActor extends Actor {
  defaultState() {
    return {
      customerInfo: {},
      hotExchange: [], //积分兑换
      sortType: {
        //排序选项
        type: '',
        sort: ''
      },
      canExchange: false, //我能兑换
      cateList: [], //积分商品分类
      cateId: null, //选中的积分商品分类

      pointsCouponListFlag: false, // 积分优惠券列表展示
      pointsCouponId:'', // 即将兑换的积分优惠券id
      latestPointsCouponInfoList: [], // 兑换优惠券后查询到的最新信息

      visible: false, //支付密码弹框展示
      payPwd: '', //支付密码
      checkPayPwdRes: true,
      payPwdTime: 0
    };
  }

  /**
   * 初始化会员数据
   * @param {IMap} state
   * @param customerInfo
   * @returns {Map<string, any>}
   */
  @Action('points-mall: initCustomer')
  initCustomer(state: IMap, customerInfo) {
    return state.set('customerInfo', fromJS(customerInfo));
  }

  /**
   * 积分兑换
   */
  @Action('points-mall: hotExchange')
  hotExchange(state: IMap, hotExchange: IMap) {
    return state.set('hotExchange', hotExchange);
  }

  /**
   * 积分商品分类
   */
  @Action('points-mall: cateList')
  cateList(state: IMap, cateList) {
    return state.set('cateList', fromJS(cateList));
  }

  /**
   * 设置排序
   */
  @Action('points-mall: setSort')
  setSort(state, { type, sort }) {
    return state
      .setIn(['sortType', 'type'], type)
      .setIn(['sortType', 'sort'], sort);
  }

  /**
   * 设置分类id
   */
  @Action('points-mall: setCateId')
  setCateId(state, cateId) {
    return state
      .set('cateId', cateId)
      .set('pointsCouponListFlag', false)
      .set('latestPointsCouponInfoList', fromJS([]));
  }

  /**
   * 设置优惠券列表展示
   */
  @Action('points-mall: setPointsCouponListFlag')
  setPointsCouponListFlag(state) {
    return state
      .set('cateId', null)
      .set('pointsCouponListFlag', true)
      .set('latestPointsCouponInfoList', fromJS([]));
  }

  /**
   * 是否展示我能兑换
   */
  @Action('points-mall: showCanExchange')
  showCanExchange(state: IMap, canExchange) {
    return state.set('canExchange', canExchange);
  }

  /**
   * 最新积分优惠券数据
   */
  @Action('points-mall: setNewPointsCouponInfo')
  setNewPointsCouponInfo(state: IMap, latestPointsCouponInfo) {
    let couponInfos = state.get('latestPointsCouponInfoList');
    const index = couponInfos.findIndex(
      (info) =>
        info.get('pointsCouponId') ==
        latestPointsCouponInfo.get('pointsCouponId')
    );
    if (index > -1) {
      return state.setIn(
        ['latestPointsCouponInfoList', index],
        latestPointsCouponInfo
      );
    } else {
      return state.update('latestPointsCouponInfoList', (list) =>
        list.push(latestPointsCouponInfo)
      );
    }
  }


  /**
   * 设置即将兑换的积分优惠券Id
   */
  @Action('points-mall:setPointsCouponId')
  setPointsCouponId(state, val) {
    return state.set('pointsCouponId', val);
  }

  /**
   * 支付密码输入框是否显示
   */
  @Action('points-mall:showPassword')
  showPassword(state, val) {
    return state.set('visible', val);
  }

  /**
   * 支付密码改变
   */
  @Action('points-mall:changePayPwd')
  changePayPwd(state, val) {
    return state.set('payPwd', val);
  }

  /**
   * 校验密码结果
   */
  @Action('points-mall:checkPayPwdRes')
  checkPayPwdRes(state, checkPayPwdRes) {
    return state.set('checkPayPwdRes', checkPayPwdRes);
  }

  /**
   * 密码错误次数
   */
  @Action('points-mall:countPayPwdTime')
  countPayPwdTime(state, payPwdTime) {
    return state.set('payPwdTime', payPwdTime);
  }
}
