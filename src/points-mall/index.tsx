import React, {Component} from 'react';
import {StoreProvider} from 'plume2';
import AppStore from './store';

import Header from './component/header';
import HotExchange from './component/hot-exchange';
import ProductList from './component/product-list';
import MemberFooter from '../member-footer/index';
import {WMkit} from 'wmkit';
import PaymentPassword from './component/payment-password';

const style = require('./css/style.css');

@StoreProvider(AppStore, { debug: __DEV__ })
export default class PointsMall extends Component<any, any> {
  store: AppStore;

  componentWillMount() {}

  componentDidMount() {
    if (WMkit.isLoginOrNotOpen()) {
      this.store.initCustomerInfo();
    }
    this.store.initHotExchange();
    this.store.initCateList();
  }

  render() {
    return (
      <div style={{ backgroundColor: '#fafafa' }}>
        {/* 头部 */}
        <Header />
        {/* 搜索 */}
        {/*<Reseach />*/}
        {/* 热门兑换 */}
        <HotExchange />
        {/* 商品列表 */}
        <ProductList />
        {/* 底部 */}
        <MemberFooter path={'/points-mall'} pointsIsOpen={true}/>
        {/* 输入支付密码 */}
        {this.store.state().get('visible') && <PaymentPassword />}
      </div>
    );
  }
}
