import React from 'react';
import { Link } from 'react-router-dom';
import { Relax, msg } from 'plume2';
import { WMkit, noop } from 'wmkit';
import history from 'wmkit/history';
@Relax
export default class Header extends React.Component<any, any> {
  props: {
    relaxProps?: {
      customerInfo: any;
      initCustomerInfo: Function;
    };
  };

  static relaxProps = {
    customerInfo: 'customerInfo',
    initCustomerInfo: noop
  };

  render() {
    const { customerInfo, initCustomerInfo } = this.props.relaxProps;
    const isLogin = WMkit.isLoginOrNotOpen();

    return (
      <div className="integral-header">
        <img className="head-pointer" src={require('../img/pointer.png')} />
        <div className="right-info">
          <span
            className="up-title"
            onClick={() => {
              if (isLogin) {
                return;
              }
              msg.emit('loginModal:toggleVisible', {
                callBack: () => {
                  initCustomerInfo();
                }
              });
            }}
          >
            {isLogin ? customerInfo.get('customerName') : '登录/注册 >'}
          </span>
          {isLogin && (
            <div className="down-div">
              <span className="text">
                积分数：{customerInfo.get('pointsAvailable')}
              </span>
              <div
                className="dh-btn"
                onClick={() => history.push('/points-order-list')}
              >
                兑换记录
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
