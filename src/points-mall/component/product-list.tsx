import React from 'react';
import { Link } from 'react-router-dom';
import { IMap, msg, Relax } from 'plume2';
import { Blank, ListView, noop, WMImage, WMkit } from 'wmkit';
import { IList } from 'typings/globalType';
import history from 'wmkit/history';
import { fromJS } from 'immutable';
import CouponItem from './coupon-item';

@Relax
export default class ProductList extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  props: {
    relaxProps?: {
      setSort: Function;
      setCateId: Function;
      setPointsCouponListFlag: Function;
      showCanExchange: Function;
      initCustomerInfo: Function;
      initHotExchange: Function;
      sortType: IMap;
      canExchange: boolean;
      cateList: IList;
      cateId: number;
      pointsCouponListFlag: boolean;
    };
  };

  static relaxProps = {
    setSort: noop,
    setCateId: noop,
    setPointsCouponListFlag: noop,
    showCanExchange: noop,
    initCustomerInfo: noop,
    initHotExchange: noop,
    sortType: 'sortType',
    canExchange: 'canExchange',
    cateList: 'cateList',
    cateId: 'cateId',
    pointsCouponListFlag: 'pointsCouponListFlag'
  };

  render() {
    const {
      setSort,
      sortType,
      initCustomerInfo,
      initHotExchange,
      showCanExchange,
      canExchange,
      cateList,
      setCateId,
      setPointsCouponListFlag,
      cateId,
      pointsCouponListFlag
    } = this.props.relaxProps;
    // 排序字段
    const type = sortType.get('type');
    // 排序方式：升序 降序
    const sort = sortType.get('sort');

    let sortFlag = null;
    if (type == 'points' && sort == 'asc') {
      sortFlag = 0;
    } else if (type == 'points' && sort == 'desc') {
      sortFlag = 1;
    } else if (type == 'price' && sort == 'asc') {
      sortFlag = 2;
    } else if (type == 'price' && sort == 'desc') {
      sortFlag = 3;
    }

    return (
      <div className="integral-list">
        <div className="cate-list">
          <div className="cate-content">
            <div
              style={{ height: '0.8rem' }}
              className={
                cateId == null && pointsCouponListFlag == false
                  ? 'cate-item cur'
                  : 'cate-item'
              }
              onClick={() => setCateId(null)}
            >
              全部
            </div>
            <div
              style={{ height: '0.8rem' }}
              className={pointsCouponListFlag ? 'cate-item cur' : 'cate-item'}
              onClick={() => setPointsCouponListFlag()}
            >
              优惠券
            </div>
            {cateList.map((val) => {
              return (
                <div
                  style={{ height: '0.8rem' }}
                  className={
                    cateId == val.get('cateId') ? 'cate-item cur' : 'cate-item'
                  }
                  onClick={() => setCateId(val.get('cateId'))}
                  key={val.get('cateId')}
                >
                  {val.get('cateName')}
                </div>
              );
            })}
          </div>
        </div>
        <ul className="nav-ul" style={{ top: '0.8rem' }}>
          <li
            className={canExchange ? 'active-li' : ''}
            onClick={() => {
              if (!WMkit.isLoginOrNotOpen()) {
                msg.emit('loginModal:toggleVisible', {
                  callBack: () => {
                    initCustomerInfo();
                    initHotExchange();
                    showCanExchange();
                  }
                });
              } else {
                showCanExchange();
              }
            }}
          >
            我能兑换
          </li>
          <li
            className={type == 'points' ? 'active-li' : ''}
            onClick={() => setSort('points')}
          >
            积分价
            {type == 'points' && (
              <i
                className={
                  sort == 'asc'
                    ? 'iconfont icon-xiala-copy transform-i'
                    : 'iconfont icon-xiala-copy'
                }
              />
            )}
          </li>
          <li
            className={type == 'price' ? 'active-li' : ''}
            onClick={() => setSort('price')}
          >
            市场价
            {type == 'price' && (
              <i
                className={
                  sort == 'asc'
                    ? 'iconfont icon-xiala-copy transform-i'
                    : 'iconfont icon-xiala-copy'
                }
              />
            )}
          </li>
        </ul>
        <div className="list-ul">
          {pointsCouponListFlag == false ? (
            <ListView
              key={'pointsGoods'}
              url={
                canExchange ? '/pointsMall/pageCanExchange' : '/pointsMall/page'
              }
              params={
                !canExchange
                  ? {
                      sortFlag: sortFlag,
                      cateId
                    }
                  : {
                      cateId
                    }
              }
              pageSize={10}
              style={{ height: 'calc(100vh - 34.67vw)' }}
              renderRow={this._goodsRow}
              dataPropsName={'context.pointsGoodsVOPage.content'}
              renderEmpty={() => (
                <Blank
                  img={require('../img/list-none.png')}
                  content="没有符合您要求的商品~"
                />
              )}
            />
          ) : (
            <div className="list center-list">
              <ListView
                key={'pointsCoupon'}
                url={
                  canExchange
                    ? '/pointsMall/pageCanExchangeCoupon'
                    : '/pointsMall/pageCoupon'
                }
                params={
                  !canExchange && {
                    sortFlag: sortFlag
                  }
                }
                pageSize={10}
                style={{ height: 'calc(100vh - 34.67vw)' }}
                renderRow={(item, index) => {
                  return (
                    <CouponItem
                      pointsCoupon={fromJS(item)}
                      couponInfo={fromJS(item.couponInfoVO)}
                      key={index}
                    />
                  );
                }}
                dataPropsName={'context.pointsCouponVOPage.content'}
                renderEmpty={() => (
                  <Blank
                    img={require('../img/coupon-empty.png')}
                    content="没有符合您要求的优惠券~"
                  />
                )}
              />
            </div>
          )}
        </div>
      </div>
    );
  }

  /**
   * 列表数据
   */
  _goodsRow = (pointsGoods) => {
    return (
      <div
        className="goods-info"
        key={pointsGoods.pointsGoodsId}
        onClick={() =>
          history.push({
            pathname: '/goods-detail/' + pointsGoods.goodsInfo.goodsInfoId,
            state: { pointsGoodsId: pointsGoods.pointsGoodsId }
          })
        }
      >
        <div className="left-img">
          <WMImage
            src={
              pointsGoods.goodsInfo.goodsInfoImg || pointsGoods.goods.goodsImg
            }
            width="100%"
            height="100%"
          />
        </div>
        <div className="right-text">
          <span className="text1">{pointsGoods.goodsInfo.goodsInfoName}</span>
          <span className="text2">{pointsGoods.points}积分</span>
          <span className="text3">
            市场价:￥{pointsGoods.goodsInfo.marketPrice
              ? pointsGoods.goodsInfo.marketPrice.toFixed(2)
              : 0}
          </span>
        </div>
      </div>
    );
  };
}
