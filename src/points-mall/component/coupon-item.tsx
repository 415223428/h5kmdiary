import React from 'react';
import moment from 'moment';
import { Const } from 'config';
import {Confirm, WMkit} from 'wmkit';
import {msg, Relax} from 'plume2';
import noop from 'wmkit/noop';

const COUPON_TYPE = {
  0: '通用券',
  1: '店铺券',
  2: '运费券'
};

@Relax
export default class CouponItem extends React.Component<any, any> {
  props: {
    relaxProps?: {
      doFetchCoupon: Function;
      latestPointsCouponInfoList: any;
      showPassword: Function;
      isPayPwdValid: Function;
      initCustomerInfo: Function;
    };
    pointsCoupon: any;
    couponInfo: any;
  };

  static relaxProps = {
    doFetchCoupon: noop,
    latestPointsCouponInfoList: 'latestPointsCouponInfoList',
    showPassword: noop,
    isPayPwdValid: noop,
    initCustomerInfo: noop
  };

  constructor(props) {
    super(props);
    this.state = props;
  }

  render() {
    let { pointsCoupon, couponInfo } = this.props;
    pointsCoupon = this._exchangedEvent(pointsCoupon);
    return (
      <div>
        <div className={`item  ${this._buildItemType(couponInfo)}`}>
          <div className="left">
            <p className="money">
              ￥
              <span className="money-text">
                {couponInfo.get('denomination')}
              </span>
            </p>
            <p className="left-tips">{this._buildFullBuyPrice(couponInfo)}</p>
          </div>
          <div className="right">
            <div className="right-info">
              <div className="right-top">
                <i>{COUPON_TYPE[couponInfo.get('couponType')]}</i>
                <p>
                  <span> {this._buildStorename(couponInfo)}</span>
                </p>
              </div>
              {couponInfo.get('couponType') == 2 && (
                <div className="range-box" />
              )}
              {couponInfo.get('couponType') != 2 && (
                <div className="range-box">
                  限<span> {this._buildScope(couponInfo)}</span>可用
                </div>
              )}
              <div className="bottom-box">{this._buildRangDay(couponInfo)}</div>
            </div>

            {pointsCoupon.get('sellOutFlag') ? (
              <div className="operat-box">
                <div className="gray-state">已抢光</div>
              </div>
            ) : (
              <div className="operat-box colum-center">
                <p>已抢</p>
                <p className="coupon-time">
                  {(
                    (pointsCoupon.get('exchangeCount') /
                      pointsCoupon.get('totalCount')) *
                    100
                  ).toFixed(0)}%
                </p>
                <p>{pointsCoupon.get('points')}积分</p>
                <a
                  href="javascript:;"
                  onClick={() => this._fetchCoupon(pointsCoupon, couponInfo)}
                >
                  立即兑换
                </a>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }

  /**
   * 优惠券样式
   * 0 通用券
   * 1 店铺券
   * 2 运费券
   */
  _buildItemType = (coupon) => {
    let text = '';
    if (coupon.get('couponType') === 0) {
      text = '';
    } else if (coupon.get('couponType') === 1) {
      text = 'store-item';
    } else if (coupon.get('couponType') === 2) {
      text = 'freight-item';
    }
    return `${text}`;
  };

  /***
   * 满减金额
   */
  _buildFullBuyPrice = (coupon) => {
    return coupon.get('fullBuyType') === 0
      ? '无门槛'
      : `满${coupon.get('fullBuyPrice')}可用`;
  };

  /**
   * 优惠券使用店铺名称（暂时只有平台券）
   */
  _buildStorename = (coupon) => {
    let text = '';
    if (coupon.get('platformFlag') === 1) {
      text = '全平台可用';
    }
    return `${text}`;
  };

  /**
   * 优惠券使用范围
   */
  _buildScope = (coupon) => {
    let text = '';
    let scopeType = '';
    if (coupon.get('scopeType') == 0) {
      scopeType = '商品：';
      text = '全部商品';
    } else if (coupon.get('scopeType') == 1) {
      scopeType = '品牌：';
      text = '仅限';
      coupon.get('scopeNames').forEach((value) => {
        let name = value ? '[' + value + ']' : '';
        text = `${text}${name}`;
      });
    } else if (coupon.get('scopeType') == 2) {
      scopeType = '品类：';
      text = '仅限';
      coupon.get('scopeNames').forEach((value) => {
        let name = value ? '[' + value + ']' : '';
        text = `${text}${name}`;
      });
    } else if (coupon.get('scopeType') == 3) {
      scopeType = '分类：';
      text = '仅限';
      coupon.get('scopeNames').forEach((value) => {
        let name = value ? '[' + value + ']' : '';
        text = `${text}${name}`;
      });
    } else {
      scopeType = '商品：';
      text = '部分商品';
    }

    return `${scopeType}${text}`;
  };

  /***
   * 生效时间
   */
  _buildRangDay = (coupon) => {
    return coupon.get('rangeDayType') === 1
      ? `领取后${coupon.get('effectiveDays')}天内有效`
      : `${moment(coupon.get('startTime')).format(Const.DATE_FORMAT)}至${moment(
          coupon.get('endTime')
        ).format(Const.DATE_FORMAT)}`;
  };

  /**
   * 立即兑换弹窗
   */
  _fetchCoupon = async (pointsCoupon, couponInfo) => {
    const { initCustomerInfo } = this.props.relaxProps;
    if (!WMkit.isLoginOrNotOpen()) {
      msg.emit('loginModal:toggleVisible', {
        callBack: () => {
          initCustomerInfo();
        }
      });
    } else {
      const { isPayPwdValid, showPassword } = this.props.relaxProps;
      const result = await isPayPwdValid();
      if (result) {
        Confirm({
          text: (
            <div style={{ marginLeft: -20, marginRight: -20 }}>
              <div
                className="list center-list"
                style={{ height: 100, padding: 0 }}
              >
                <div className={`item  ${this._buildItemType(couponInfo)}`}>
                  <div className="left" style={{ height: 80, width: 90 }}>
                    <p className="money">
                      ￥
                      <span className="money-text">
                      {couponInfo.get('denomination')}
                    </span>
                    </p>
                    <p className="left-tips">
                      {this._buildFullBuyPrice(couponInfo)}
                    </p>
                  </div>
                  <div className="right" style={{ height: 80, width: 165 }}>
                    <div className="right-info">
                      <div className="right-top">
                        <i>{COUPON_TYPE[couponInfo.get('couponType')]}</i>
                        <p>
                        <span style={{ textAlign: 'left' }}>
                          {' '}
                          {this._buildStorename(couponInfo)}
                        </span>
                        </p>
                      </div>
                      {couponInfo.get('couponType') == 2 && (
                        <div className="range-box" />
                      )}
                      {couponInfo.get('couponType') != 2 && (
                        <div className="range-box">
                          限<span> {this._buildScope(couponInfo)}</span>可用
                        </div>
                      )}
                      <div className="bottom-box" style={{ textAlign: 'left' }}>
                        {this._buildRangDay(couponInfo)}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div>消耗积分：{pointsCoupon.get('points')}积分</div>
            </div>
          ),
          okBtn: '立即兑换',
          cancelBtn: '取消',
          maskClose: false,
          confirmCb: () =>
            showPassword(true, pointsCoupon.get('pointsCouponId'))
        });
      }
    }
  };

  _exchangedEvent = (pointsCoupon) => {
    const { latestPointsCouponInfoList } = this.props.relaxProps;
    const index = latestPointsCouponInfoList.findIndex(
      (info) => info.get('pointsCouponId') == pointsCoupon.get('pointsCouponId')
    );
    if (index > -1) {
      pointsCoupon = pointsCoupon
        .set(
          'exchangeCount',
          latestPointsCouponInfoList.get(index).get('exchangeCount')
        )
        .set(
          'sellOutFlag',
          latestPointsCouponInfoList.get(index).get('sellOutFlag')
        );
    }
    return pointsCoupon;
  };
}
