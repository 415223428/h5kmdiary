import {IOptions, Store} from 'plume2';
import {fromJS} from 'immutable';
import {config} from 'config';
import Swiper from 'swiper/dist/js/swiper.js';
import * as webApi from './webapi';
import PointsMallActor from './actor/points-mall-actor';
import {Alert, Confirm, history, WMkit} from 'wmkit';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new PointsMallActor()];
  }

  /**
   * 初始化用户信息
   */
  initCustomerInfo = async () => {
    const res: any = await webApi.getCustomerInfo();
    if (res && res.code == config.SUCCESS_CODE) {
      this.dispatch('points-mall: initCustomer', fromJS(res.context));
    }
  };

  /**
   * 初始化热门兑换
   */
  initHotExchange = async () => {
    const res: any = await webApi.getHotExchange();
    if (res && res.code == config.SUCCESS_CODE) {
      this.dispatch(
        'points-mall: hotExchange',
        fromJS(res.context.pointsGoodsVOPage.content)
      );
    }
    new Swiper('.swiper-container', {
      slidesPerView: 2.5,
      spaceBetween: 5,
      freeMode: true
    });
  };

  /**
   * 积分商品分类
   */
  initCateList = async () => {
    const res: any = await webApi.getCateList();
    if (res && res.code == config.SUCCESS_CODE) {
      this.dispatch(
        'points-mall: cateList',
        fromJS(res.context.pointsGoodsCateVOList)
      );
    }
  };

  /**
   * 设置排序
   */
  setSort = (type) => {
    let newType = type;
    let newSort = '';
    const sortType = this.state().get('sortType');

    // 是否切换排序类型？
    if (newType !== sortType.get('type')) {
      newSort = 'asc';
    } else {
      // 同一种排序类型，切换升降顺序，我能兑换无顺序
      if (sortType.get('sort') === 'asc') {
        newSort = 'desc';
      } else if (sortType.get('sort') === 'desc') {
        newSort = 'asc';
      }
    }

    this.transaction(() => {
      this.dispatch('points-mall: setSort', { type: newType, sort: newSort });
      this.dispatch('points-mall: showCanExchange', false);
    });
  };

  /**
   * 选择积分商品分类
   * @param cateId
   */
  setCateId = (cateId) => {
    this.dispatch('points-mall: setCateId', cateId);
  };

  /**
   * 选择积分优惠券分类
   * @param cateId
   */
  setPointsCouponListFlag = () => {
    this.dispatch('points-mall: setPointsCouponListFlag');
  };

  /**
   * 展示我能兑换
   */
  showCanExchange = () => {
    this.transaction(() => {
      this.dispatch('points-mall: showCanExchange', true);
      this.dispatch('points-mall: setSort', { type: '', sort: '' });
    });
  };

  /**
   * 立即兑换
   */
  doFetchCoupon = WMkit.onceFunc(async () => {
    //校验输入密码
    let payPassword = this.state().get('payPwd');
    if (payPassword == '') {
      Alert({
        text: '请输入支付密码！'
      });
      return;
    }
    let checkPayPwd = await this.checkCustomerPayPwd(payPassword);
    if (!checkPayPwd) {
      this.dispatch('points-mall:checkPayPwdRes', false);
      await this.getLoginCustomerInfo();
      return;
    } else {
      this.dispatch('points-mall:checkPayPwdRes', true);
    }

    const pointsCouponId = this.state().get('pointsCouponId');
    if (pointsCouponId == '') {
      Alert({
        text: '积分优惠券不存在，请重新兑换'
      });
      return;
    }
    const res: any = await webApi.doFetchCoupon(pointsCouponId);
    if (res && res.code == config.SUCCESS_CODE) {
      Alert({ text: '兑换成功' });
      // 1.关闭支付密码弹窗
      this.showPassword(false, '');
      // 2.重置支付密码
      this.dispatch('points-mall:changePayPwd', '');
      // 3.更新会员积分信息
      this.initCustomerInfo();
      // 4.局部刷新已兑换优惠券的最新数据
      const couponRes: any = await webApi.getCouponById(pointsCouponId);
      if (couponRes.code == config.SUCCESS_CODE) {
        const {
          pointsCouponId,
          exchangeCount,
          sellOutFlag
        } = couponRes.context.pointsCouponVO;
        this.dispatch(
          'points-mall: setNewPointsCouponInfo',
          fromJS({ pointsCouponId, exchangeCount, sellOutFlag })
        );
      }
    } else {
      Alert({ text: res.message });
    }
  }, 1000);

  /**
   * 支付密码弹框显示
   */
  showPassword = (flag: Boolean, pointsCouponId) => {
    this.transaction(() => {
      this.dispatch('points-mall:showPassword', flag);
      this.dispatch('points-mall:setPointsCouponId', pointsCouponId);
    });
  };

  /**
   * 输入密码修改
   * @param payPwd
   */
  changePayPwd = async (payPwd) => {
    this.dispatch('points-mall:changePayPwd', payPwd);
  };

  /**
   * 校验会员支付密码是否可用
   */
  isPayPwdValid = async () => {
    const res = await webApi.isPayPwdValid();
    if (res.code == 'K-000000') {
      return true;
    } else if (res.code == 'K-010206') {
      Confirm({
        text: '您还没有设置支付密码，暂时无法使用积分支付',
        okBtn: '设置支付密码',
        confirmCb: () => history.push('/balance-pay-password')
      });
      return false;
    } else if (res.code == 'K-010207') {
      Alert({
        text: res.message,
        time: 1000
      });
      return false;
    }
  };

  /**
   * 校验输入支付密码
   * @param payPassword
   */
  checkCustomerPayPwd = async (payPassword) => {
    const res = await webApi.checkCustomerPayPwd(payPassword);
    if (res.code == 'K-000000') {
      return true;
    } else if (res.code == 'K-010206') {
      Confirm({
        text: '您还没有设置支付密码，暂时无法使用积分支付',
        okBtn: '设置支付密码',
        confirmCb: () => history.push('/balance-pay-password')
      });
    } else {
      Alert({
        text: res.message,
        time: 1000
      });
      return false;
    }
  };

  /**
   * 获取当前登陆人信息
   */
  getLoginCustomerInfo = async () => {
    const res = (await webApi.getLoginCustomerInfo()) as any;
    if (res.code == 'K-000000') {
      this.dispatch('points-mall:countPayPwdTime', res.context.payErrorTime);
    }
  };
}
