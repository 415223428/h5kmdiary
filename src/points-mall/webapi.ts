import {Fetch} from 'wmkit';

/**
 * 获取会员信息
 */
export const getCustomerInfo = () => {
  return Fetch('/pointsMall/customerInfo');
};

/**
 * 获取热门兑换
 */
export const getHotExchange = () => {
  return Fetch('/pointsMall/hotExchange', {
    method: 'POST',
    body: JSON.stringify({
      pageSize: 10
    })
  });
};

/**
 * 获取积分商品分类信息
 */
export const getCateList = () => {
  return Fetch('/pointsMall/cateList');
};

/**
 * 立即兑换优惠券
 */
export const doFetchCoupon = (pointsCouponId) => {
  return Fetch(`/pointsMall/fetchPointsCoupon/${pointsCouponId}`, {
    method: 'POST'
  });
};

/**
 * 单个查询积分优惠券
 */
export function getCouponById(id) {
  return Fetch(`/pointsMall/coupon/${id}`, {
    method: 'GET'
  });
}

/**
 * 校验输入支付密码
 * @param payPassword
 */
export const checkCustomerPayPwd = (payPassword) => {
  return Fetch<Result<any>>('/checkCustomerPayPwd', {
    method: 'POST',
    body: JSON.stringify({ payPassword: payPassword })
  });
};

/**
 * 校验会员支付密码是否可用
 */
export const isPayPwdValid = () => {
  return Fetch<Result<any>>('/isPayPwdValid', {
    method: 'POST'
  });
};

/**
 * 获取当前登陆人信息
 */
export const getLoginCustomerInfo = () => {
  return Fetch<Result<any>>('/customer/getLoginCustomerInfo', {
    method: 'GET'
  });
};
