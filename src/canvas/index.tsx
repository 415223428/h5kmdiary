import React from 'react';

export default class Canva extends React.Component<any, any> {

  constructor(props) {
    super(props);
    this.state = {
      width: window.screen.width,
      height: window.screen.height
    }
  }


  componentDidMount() {
    let _myCanvasEle = document.getElementById("myCanvas") as any;
    if (_myCanvasEle.getContext) {
      var _ctx2 = _myCanvasEle.getContext("2d"); //获取节点
      // var _ctxW = $("#myCanvas").width(); //获取宽度
      var _ctxW = this.state.width;
      var _pagePd = _ctxW * 0.03; //获取padding的值
      //绘制图片
      var _goodsImg = new Image();
      _goodsImg.src = './img/one.png';
      // _goodsImg.setAttribute("crossOrigin",'Anonymous')
      var _goodsImgW = _ctxW * 0.94;
      var _goodsImgH = _ctxW * 0.97;

      _goodsImg.onload = function () {
        _ctx2.drawImage(this, _pagePd, _pagePd, _goodsImgW, _goodsImgH);
      }
      //绘制推荐行
      var _recRowY = _pagePd + _goodsImgH + 10;
      //绘制头像
      var _userImg = new Image();
       var _userImgW = 20;
      var _userImgH = 20;
      _userImg.onload = function () {
        _ctx2.save(); //保存上一次状态。防止错位
        _ctx2.beginPath();
        //arc(x,y,弧度,顺时针or)
        _ctx2.arc(_pagePd + (_userImgW / 2), _recRowY + (_userImgW / 2), _userImgW / 2, 0, Math.PI * 2, false);
        _ctx2.clip(); //剪切 一旦剪切了某个区域，则所有之后的绘图都会被限制在被剪切的区域内
        _ctx2.drawImage(this, _pagePd, _recRowY, _userImgW, _userImgH);
        _ctx2.restore();
      }
      _userImg.src = "https://wanmi-b2b.oss-cn-shanghai.aliyuncs.com/201810171544181137?x-oss-process=image/resize,m_fixed,limit_0,w_172,h_167";



      _ctx2.fillStyle = '#999';
      _ctx2.font = 'bold 20px arial';
      var _numT = '7';
      var _numTW = _ctx2.measureText(_numT).width;
      var _numTStartX = _pagePd + _userImgW + 20;
      //内容。 X的位置  Y的位置
      _ctx2.fillText(_numT, _numTStartX, _recRowY + 20);
      _ctx2.fillStyle = '#999';
      _ctx2.font = 'bold 20px arial';
      var _recT = '推荐你看';
      var _recTStartX = _numTStartX + _numTW + 20;
      _ctx2.fillText(_recT, _recTStartX, _recRowY + 20);

      //绘制标题文字
      var text = '这是一段标题这是一段标题这是一段标题这是一段标题这是一段标题这是一段标题这是一段标题这' +
        '是一段标题这是一段标题这是一段标题这是一段标题这是一段标题';//这是要绘制的文本
      _ctx2.font = 'bold 15px arial';
      _ctx2.fillStyle = '#666';

      var _titleLastRowY = this.drawText(_ctx2, text, _ctxW, _recRowY + _userImgH + 40);


      //绘制价格文字
      var _priceRowY = _titleLastRowY + 25;
      _ctx2.fillStyle = 'red';
      _ctx2.font = 'bold 15px arial';
      var _priceIconT = '￥';
      var _priceIconW = _ctx2.measureText(_priceIconT).width;
      _ctx2.fillText(_priceIconT, _pagePd, _priceRowY);
      _ctx2.font = 'bold 25px arial';
      var _priceT = '300.00';
      var _priceTW = _ctx2.measureText(_priceT).width;
      _ctx2.fillText(_priceT, _pagePd + _priceIconW, _priceRowY);
      _ctx2.fillStyle = '#999';
      _ctx2.font = 'bold 14px arial';
      var _priceBetweenT = '￥400';
      var _priceBetweenTW = _ctx2.measureText(_priceBetweenT).width;
      var _priceBetweenTStartX = _pagePd + _priceIconW + _priceTW + 50;
      _ctx2.fillText(_priceBetweenT, _priceBetweenTStartX, _priceRowY);
      //绘制价格文字线条
      _ctx2.beginPath();
      _ctx2.moveTo(_priceBetweenTStartX, _priceRowY - 4);
      _ctx2.lineTo(_priceBetweenTStartX + _priceBetweenTW + 5, _priceRowY - 4);
      _ctx2.stroke();
      //绘制图片
      var _codeImg = new Image();
      _codeImg.src = "http://web-img.qmimg.com/qianmicom/u/cms/syy/201705/22163438p3rg.png";
      _codeImg.onload = function () {
        var _codeImgX = _ctxW * 0.75;
        var _codeImgY = _recRowY + 30;
        _ctx2.drawImage(this, _ctxW * 0.75, _codeImgY, _ctxW * 0.2, _ctxW * 0.2);
      }


      // _goodsImg.onload = function(){//图片加载完，再draw 和 toDataURL
        var imgData = _myCanvasEle.toDataURL("image/png");
        document.getElementById("image").setAttribute("src",imgData)
      // };

    } else {
      _myCanvasEle.innerHTML = "unsupport";
    }
  }


  drawText = (canvasContext, str, picW, startY) => {
    var chr = str.split("");//将一个字符串分割成字符串数组
    var temp = "";
    var row = [];
    for (var i = 0; i < chr.length; i++) {
      if (canvasContext.measureText(temp).width < 240) {
        temp += chr[i];
      } else {
        i--; //防止字符丢失
        row.push(temp);
        temp = "";
      }
    }
    row.push(temp);
    //如果数组长度大于2 则截取前两个
    if (row.length > 2) {
      var rowCut = row.slice(0, 2);
      var rowPart = rowCut[1];
      var test = "";
      var empty = [];
      for (var a = 0; a < rowPart.length; a++) {
        if (canvasContext.measureText(test).width < 220) {
          test += rowPart[a];
        }
        else {
          break;
        }
      }
      empty.push(test);
      var group = empty[0] + "..."//这里只显示两行，超出的用...表示
      rowCut.splice(1, 1, group);
      row = rowCut;
    }
    var _strlastRowY = 0;
    for (var j = 0; j < row.length; j++) {
      _strlastRowY = startY + j * 15;
      canvasContext.fillText(row[j], picW * 0.03, _strlastRowY, picW * 0.7);
    }
    return _strlastRowY;
  }

  render() {
    let clientWidth = document.body.clientWidth;
    let clientHeight = document.body.clientHeight;


    return (


      <div style={{width: this.state.width, height: this.state.height, padding: 10}}>
        <button onClick={() => this.canvasTransformPng()}>吧这个转成图片</button>
        <canvas id="myCanvas" width={this.state.width} height={this.state.height} style={{display: "none"}}></canvas>
        <img id="image"/>

      </div>
    )

  }

  canvasTransformPng() {
    let _myCanvasEle = document.getElementById("myCanvas") as any;
    var imgData = _myCanvasEle.toDataURL("image/png");


  }

}