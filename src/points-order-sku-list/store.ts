import { Store } from 'plume2';
import SkuActor from './actor/sku-actor';
import * as webApi from './webapi';
import { Confirm, history } from 'wmkit';

export default class AppStore extends Store {
  bindActor() {
    return [new SkuActor()];
  }

  constructor(props) {
    super(props);
    //debug
    (window as any)._store = this;
  }

  /**
   * 确认订单初始化
   * @returns {Promise<void>}
   */
  getExchangeItem = async (params) => {
    const res = (await webApi.getExchangeItem(params)) as any;
    if (res.code == 'K-000000') {
      const store = res.context.pointsTradeConfirmItem;
      this.dispatch('sku: fetch', {
        sku: store.tradeItem,
        isSelf: store.supplier.isSelf
      });
    } else {
      Confirm({
        text: res.message,
        okBtn: '确定',
        confirmCb: () => history.push('/points-mall')
      });
    }
  };
}
