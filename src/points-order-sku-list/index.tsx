import React, { Component } from 'react';
import { SkuList } from 'wmkit';
import { StoreProvider } from 'plume2';
import AppStore from './store';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class OrderSkuList extends Component<any, any> {
  store: AppStore;

  componentWillMount() {
    const params =
      this.props.location.state && this.props.location.state.params;
    this.store.getExchangeItem(params);
  }

  render() {
    const orderSku = this.store.state().get('orderSku');
    let skuList = Array();
    skuList.push({
      imgUrl: orderSku.pic,
      skuName: orderSku.skuName,
      specDetails: orderSku.specDetails,
      points: orderSku.points,
      num: orderSku.num,
      unit: orderSku.unit,
      pointsGoodsId: orderSku.pointsGoodsId
    });

    return (
      <div style={{ background: '#fafafa' }}>
        <SkuList
          data={skuList}
          isSelf={this.store.state().get('isSelf')}
          commissionStrType={1}
        />
      </div>
    );
  }
}
