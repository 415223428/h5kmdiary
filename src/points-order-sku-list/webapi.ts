import { Fetch } from 'wmkit'

/**
 * 获取订单商品列表
 * @returns {Promise<Result<TResult>>}
 */
export const getExchangeItem = (params) => {
  return Fetch<Result<any>>('/pointsTrade/getExchangeItem', {
    method: 'POST',
    body: JSON.stringify(params)
  });
};