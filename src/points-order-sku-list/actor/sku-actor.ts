import { Action, Actor, IMap } from 'plume2';

export default class SkuActor extends Actor {
  defaultState() {
    return {
      orderSku: {}, //待购买商品
      isSelf: false // 是否是自营
    };
  }

  /**
   * 存储待购买商品
   * @param state
   * @param orderSku
   * @returns {Map<string, V>}
   */
  @Action('sku: fetch')
  fetchOrderSkus(state: IMap, { sku, isSelf }) {
    return state
      .set('orderSku', sku)
      .set('isSelf', isSelf)
  }
}
