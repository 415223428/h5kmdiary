import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';

import { history, OrderWrapper, noop, storage } from 'wmkit';
import { cache } from 'config';

import { operationsQL } from '../ql';

@Relax
export default class OrderBottom extends Component<any, any> {
  props: {
    relaxProps?: {
      operations: any;
      detail: IMap;
      _cancelOrder: Function;
      applyRefund: Function;
      defaultPay: Function;
    };
  };

  static relaxProps = {
    operations: operationsQL,
    detail: 'detail',
    _cancelOrder: noop,
    applyRefund: noop,
    defaultPay: noop
  };

  render() {
    let { operations, detail, defaultPay } = this.props.relaxProps;
    let orderWrapper = OrderWrapper(detail);

    return (
      <div style={{ height: 48 }}>
        <div className="order-button-wrap ">
          {operations == undefined
            ? null
            : operations.available.map((availableButton) => {
                return (
                  <div
                    className={'btn btn-ghost btn-small'}
                    onClick={() => {
                      this._operationButtons(
                        orderWrapper.orderNo(),
                        availableButton
                      );
                    }}
                  >
                    {availableButton}
                  </div>
                );
              })}
        </div>
      </div>
    );
  }

  /**
   * 订单操作按钮event handler
   */
  _operationButtons = async (tid, button) => {
    if (button == '确认收货') {
      history.push(`/points-order-ship-record/${tid}/0`);
    }
  };

  _clearCache = async () => {
    storage('session').del(cache.PAYMENT_ENCLOSES);
    storage('session').del(cache.PAYMENT_REMARK);
    storage('session').del(cache.PAYMENT_TIME);
  };
}
