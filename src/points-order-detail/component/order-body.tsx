import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { IMap, Relax } from 'plume2';

import {
  FormItem,
  FormSelect,
  OrderWrapper,
  history,
  ImageListScroll
} from 'wmkit';

const noneImg = require('../img/none.png');

@Relax
export default class OrderBody extends Component<any, any> {
  props: {
    relaxProps?: {
      detail: IMap;
    };
  };

  static relaxProps = {
    detail: 'detail'
  };

  render() {
    const { detail } = this.props.relaxProps;
    let orderWrapper = OrderWrapper(detail);

    return (
      <div>
        <div className="sku-head">
          <Link to={`/store-main/${orderWrapper.storeId()}`}>
            {orderWrapper.storeName()}
          </Link>
        </div>
        <Link to={`/points-order-detail-skus/${orderWrapper.orderNo()}`}>
          <div className="limit-img">
            <div className="img-content">
              {orderWrapper
                .tradeItems()
                .concat(orderWrapper.gifts())
                .toJS()
                .map((v, k) => {
                  if (k < 4) {
                    return (
                      <img className="img-item" src={v.pic ? v.pic : noneImg} />
                    );
                  }
                })}
            </div>
            <div className="right-context">
              <div className="total-num">
                <div>
                  共{
                    orderWrapper.tradeItems().concat(orderWrapper.gifts()).size
                  }种
                </div>
              </div>
              <i className="iconfont icon-jiantou1" />
            </div>
          </div>
        </Link>
        <div className="mb10">
          <FormSelect
            labelName="付款记录"
            placeholder=""
            selected={{ key: '1', value: orderWrapper.orderPayState() }}
            onPress={() =>
              this._toPayRecord(
                orderWrapper.orderNo(),
                orderWrapper.orderPayState()
              )
            }
          />
          <FormSelect
            labelName="发货记录"
            placeholder=""
            selected={{ key: '1', value: orderWrapper.orderDeliveryState() }}
            onPress={() =>
              this._toShipRecord(
                orderWrapper.orderNo(),
                orderWrapper.orderDeliveryState()
              )
            }
            iconVisible={this._isIconVisible(orderWrapper.orderDeliveryState())}
          />
        </div>
        <div className="mb10 top-border">
          <FormItem
            labelName="订单备注"
            placeholder={orderWrapper.buyerRemark()}
          />
          <FormItem
            labelName="卖家备注"
            placeholder={orderWrapper.sellerRemark()}
          />
          <ImageListScroll
            imageList={orderWrapper.encloses()}
            labelName="订单附件"
          />
        </div>
        <div className="total-price mb10">
          <div className="total-list">
            <span>订单积分</span>
            <span className="price-color">{orderWrapper.points()}</span>
          </div>
        </div>
      </div>
    );
  }

  /**
   * 付款记录页
   */
  _toPayRecord = (tid: string, pay: string) => {
    history.push({
      pathname: `/pay-detail/${tid}`
    });
  };

  _toShipRecord = (id: string, state: string) => {
    if (state == '未发货') {
      return;
    } else {
      history.push(`/points-order-ship-record/${id}/0`);
    }
  };

  /**
   * 发货记录icon是否显示
   * @param state
   * @returns {boolean}
   * @private
   */
  _isIconVisible = (state: string) => {
    if (state === '未发货') {
      return false;
    }
    return true;
  };
}
