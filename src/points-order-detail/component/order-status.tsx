import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';

import { OrderWrapper } from 'wmkit';

@Relax
export default class OrderBaseInfo extends Component<any, any> {
  props: {
    relaxProps?: {
      detail: IMap;
    };
  };

  static relaxProps = {
    detail: 'detail'
  };

  render() {
    const { detail } = this.props.relaxProps;
    let orderWrapper = OrderWrapper(detail);

    return (
      <div className="order-status">
        <div className="status">
          <span>订单状态</span>
          <span className="result">{orderWrapper.orderState()}</span>
        </div>
        <div className="status">
          <span className="text">订单号</span>
          <span className="text">{orderWrapper.orderNo()}</span>
        </div>
        <div className="status">
          <span className="text">下单时间</span>
          <span className="text">{orderWrapper.createTime()}</span>
        </div>
      </div>
    );
  }
}
