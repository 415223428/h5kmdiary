import React, { Component } from 'react';
import { IMap, Relax } from 'plume2';

import { OrderWrapper } from 'wmkit';

@Relax
export default class OrderBuyerInfo extends Component<any, any> {
  props: {
    relaxProps?: {
      detail: IMap;
    };
  };

  static relaxProps = {
    detail: 'detail'
  };

  render() {
    const { detail } = this.props.relaxProps;
    let orderWrapper = OrderWrapper(detail);

    return (
      <div className="mb10">
        <div className="address-info">
          <i className="iconfont icon-dz posit" />
          <div className="address-content">
            <div className="address-detail">
              <div className="name">
                <span>收货人：{orderWrapper.buyerName()}</span>
                <span>{orderWrapper.buyerPhone()}</span>
              </div>
              <p>收货地址：{orderWrapper.buyerAddress()}</p>
            </div>
          </div>
        </div>
        <div className="seperate_line" />
      </div>
    );
  }
}
