import React, { Component } from 'react';
import { StoreProvider } from 'plume2';

import { SkuList, GiftList } from 'wmkit';

import AppStore from '../store';
import { skusQL } from '../ql';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class PointsOrderDetailSkus extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { tid } = this.props.match.params;
    this.store.getTradeSkus(tid);
  }

  render() {
    let skus = this.store.bigQuery(skusQL);
    const selfBuy = this.store.state().get('selfBuy');
    return (
      <div style={{ background: '#fafafa' }}>
        <SkuList
          data={skus}
          rebate={true}
          commissionStrType={1}
          showCommissionStr={selfBuy}
        />
      </div>
    );
  }
}
