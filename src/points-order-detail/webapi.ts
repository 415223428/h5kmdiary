/**
 * Created by chenpeng on 2017/7/25.
 */
import { Fetch } from 'wmkit';

type TResult = { code: string; message: string; context: any };
/**
 * 订单详情
 */
export const fetchOrderDetail = (tid: string) => {
  return Fetch(`/points/trade/${tid}`);
};

/**
 * 获取订单商品清单
 */
export const fetchTradeSkus = (tid: string) => {
  return Fetch(`/trade/goods/${tid}`);
};
