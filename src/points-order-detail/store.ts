import { Store, IOptions } from 'plume2';

import { Alert, history, _, WMkit } from 'wmkit';
import { config, cache } from 'config';

import DetailActor from './actor/detail-actor';
import { fetchOrderDetail } from './webapi';
import * as webapi from '../order-list/webapi';

export default class AppStore extends Store {
  constructor(props: IOptions) {
    super(props);
    if (__DEV__) {
      (window as any)._store = this;
    }
  }

  bindActor() {
    return [new DetailActor()];
  }

  /**
   * 初始化订单详情
   */
  init = async (tid) => {
    const res = await fetchOrderDetail(tid);
    if (res.code == config.SUCCESS_CODE) {
      this.dispatch('detail-actor:init', res.context);
    } else if (res.code == 'K-050100') {
      Alert({
        text: '订单不存在',
        time: 1000,
        cb: () => history.push('/points-order-list')
      });
    }
  };

  /**
   * 查询店铺-小店名称
   * @param distributorId
   * @returns {Promise<void>}
   */
  queryShopInfo = async (distributorId) => {
    const propsRes = (await webapi.queryShopInfo(distributorId)) as any;
    if (propsRes.code == config.SUCCESS_CODE) {
      this.dispatch('detail-actor:shopName', propsRes.context);
    }
  };

  toDelivery = () => {
    history.push('/points-order-ship-record');
  };

  /**
   * 获取商品清单
   */
  getTradeSkus = async (tid: string) => {
    const res = (await fetchOrderDetail(tid)) as any;
    if (res.code == config.SUCCESS_CODE) {
      this.dispatch('detail-actor:setSkus', res.context.tradeItems);
      this.dispatch('detail-actor:setGifts', res.context.gifts);
      let customerId =
        JSON.parse(localStorage.getItem(cache.LOGIN_DATA)).customerId || '';
      if (res.context.inviteeId == customerId) {
        // 订单返利给自己时，为自购
        this.dispatch('detail-actor:selfBuy', true);
      }
    }
  };
}
