import React, { Component } from 'react';
import { StoreProvider } from 'plume2';

import AppStore from './store';
import OrderStatus from './component/order-status';
import OrderBuyerInfo from './component/order-buyer-info';
import OrderBody from './component/order-body';
import OrderBottom from './component/order-bottom';

@StoreProvider(AppStore, { debug: __DEV__ })
export default class OrderDetail extends Component<any, any> {
  store: AppStore;

  componentDidMount() {
    const { tid } = this.props.match.params;
    this.store.init(tid);
  }

  render() {
    return (
      <div style={{ backgroundColor: '#fafafa' }}>
        {/*订单状态*/}
        <OrderStatus />

        {/*收货人信息*/}
        <OrderBuyerInfo />

        {/*订单主体*/}
        <OrderBody />

        {/*操作按钮*/}
        <OrderBottom />
      </div>
    );
  }
}
